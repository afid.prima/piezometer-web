﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PZO_PiezometerOnlineMap.Mapper
{
	public class WLR_Mapper_AWM
	{
        protected SqlParameter p_function = new SqlParameter("@function", SqlDbType.Int);
        protected SqlParameter p_text_param1 = new SqlParameter("@text_param1", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param2 = new SqlParameter("@text_param2", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param3 = new SqlParameter("@text_param3", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param4 = new SqlParameter("@text_param4", SqlDbType.VarChar, 400);
        protected SqlParameter p_text_param5 = new SqlParameter("@text_param5", SqlDbType.VarChar, 300);
        protected SqlParameter p_text_param6 = new SqlParameter("@text_param6", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param7 = new SqlParameter("@text_param7", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param8 = new SqlParameter("@text_param8", SqlDbType.VarChar);
        protected SqlParameter p_text_param9 = new SqlParameter("@text_param9", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param10 = new SqlParameter("@text_param10", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param11 = new SqlParameter("@text_param11", SqlDbType.VarChar, 200);

        protected SqlParameter p_int_param1 = new SqlParameter("@int_param1", SqlDbType.Int);
        protected SqlParameter p_int_param2 = new SqlParameter("@int_param2", SqlDbType.Int);
        protected SqlParameter p_int_param3 = new SqlParameter("@int_param3", SqlDbType.Int);
        protected SqlParameter p_int_param4 = new SqlParameter("@int_param4", SqlDbType.Int);
        protected SqlParameter p_int_param5 = new SqlParameter("@int_param5", SqlDbType.Int);
        protected SqlParameter p_int_param6 = new SqlParameter("@int_param6", SqlDbType.Int);
        protected SqlParameter p_int_param7 = new SqlParameter("@int_param7", SqlDbType.Int);
        protected SqlParameter p_int_param8 = new SqlParameter("@int_param8", SqlDbType.Int);
        protected SqlParameter p_int_param9 = new SqlParameter("@int_param9", SqlDbType.Int);

        protected SqlParameter p_date_param1 = new SqlParameter("@date_param1", SqlDbType.Date);
        protected SqlParameter p_date_param2 = new SqlParameter("@date_param2", SqlDbType.Date);

        protected SqlParameter p_float_param1 = new SqlParameter("@float_param1", SqlDbType.Float);
        protected SqlParameter p_float_param2 = new SqlParameter("@float_param2", SqlDbType.Float);
        protected SqlParameter p_float_param3 = new SqlParameter("@float_param3", SqlDbType.Float);
        protected SqlParameter p_float_param4 = new SqlParameter("@float_param4", SqlDbType.Float);
        protected SqlParameter p_float_param5 = new SqlParameter("@float_param5", SqlDbType.Float);
        protected SqlParameter p_float_param6 = new SqlParameter("@float_param6", SqlDbType.Float);
        protected SqlParameter p_float_param7 = new SqlParameter("@float_param7", SqlDbType.Float);
        protected SqlParameter p_float_param8 = new SqlParameter("@float_param8", SqlDbType.Float);
        protected SqlParameter p_float_param9 = new SqlParameter("@float_param9", SqlDbType.Float);
        protected SqlParameter p_float_param10 = new SqlParameter("@float_param10", SqlDbType.Float);

        public void BulkInsertDataTable(string tableName, DataTable dataTable)
        {
            SqlConnection SqlConnectionObj = new SqlConnection(WLR_ConnectionString.connStringGISAPP.ToString());
            SqlConnectionObj.Open();
            SqlBulkCopy bulkCopy = new SqlBulkCopy(SqlConnectionObj, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null);
            bulkCopy.DestinationTableName = tableName;
            bulkCopy.WriteToServer(dataTable);
            SqlConnectionObj.Close();
        }

        //----------- Normal
        public DataSet Login(string username, string password)
        {
            p_function.Value = 1;
            p_text_param1.Value = username;
            p_text_param2.Value = password;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_text_param1, p_text_param2);
        }

        public DataSet GetCompanyEstate(int userid)
        {
            p_function.Value = 2;
            p_int_param1.Value = userid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_int_param1);
        }

        public DataSet GetParit(int fc, string kebun, int userid)
        {
            p_function.Value = fc;
            p_text_param1.Value = kebun;
            p_int_param1.Value = userid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "WTW_Web", p_function, p_text_param1, p_int_param1);
        }

        public DataSet GetEstateMappingNew()
        {
            p_function.Value = 35;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "WTW_Web", p_function);
        }

        public DataSet ChangeStyle(int userid, int code, decimal size, string color)
        {
            p_function.Value = 6;
            p_int_param1.Value = userid;
            p_int_param2.Value = code;
            p_float_param1.Value = size;
            p_text_param1.Value = color;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "WTW_Web", p_function, p_int_param1, p_int_param2, p_float_param1, p_text_param1);
        }

        public DataSet ChangeStyleDefault(int userid, int code)
        {
            p_function.Value = 7;
            p_int_param1.Value = userid;
            p_int_param2.Value = code;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "WTW_Web", p_function, p_int_param1, p_int_param2);
        }

        public DataSet GetMasterAsset()
        {
            p_function.Value = 3;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function);
        }

        public DataSet GetListUser()
        {
            p_function.Value = 4;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function);
        }

        public DataSet GetListUserDetail(int userid)
        {
            p_function.Value = 5;
            p_int_param1.Value = userid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_int_param1);
        }

        public DataSet GetFilterTambahAsset(int userid, string estcode, string companycode)
        {
            p_function.Value = 6;
            p_int_param1.Value = userid;
            p_text_param1.Value = estcode;
            p_text_param2.Value = companycode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_int_param1, p_text_param1, p_text_param2);
        }

        public DataSet TambahSettingAsset(string estcode, string companycode, int userid, DateTime startdate, DateTime enddate, string updateby)
        {
            p_function.Value = 7;
            p_text_param1.Value = estcode;
            p_text_param2.Value = companycode;
            p_int_param1.Value = userid;
            p_date_param1.Value = startdate;
            p_date_param2.Value = enddate;
            p_int_param7.Value = Int32.Parse(updateby);
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_text_param1, p_text_param2, p_int_param1, p_date_param1, p_date_param2, p_int_param7);
        }

        public DataSet TambahSettingAssetType(int userid, int type, string updateby, string estcode, string companycode)
        {
            p_function.Value = 8;
            p_int_param1.Value = userid;
            p_int_param2.Value = type;
            p_int_param7.Value = Int32.Parse(updateby);
            p_text_param3.Value = estcode;
            p_text_param4.Value = companycode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_int_param1, p_int_param2, p_int_param7, p_text_param3, p_text_param4);
        }

        public DataSet TambahSettingAssetMaster(int userid, int type, string updateby, string estcode, string companycode)
        {
            p_function.Value = 9;
            p_int_param1.Value = userid;
            p_int_param2.Value = type;
            p_int_param7.Value = Int32.Parse(updateby);
            p_text_param3.Value = estcode;
            p_text_param4.Value = companycode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_int_param1, p_int_param2, p_int_param7, p_text_param3, p_text_param4);
        }

        public DataSet DeleteSettingAsset(string estcode, string companycode, int userid, string updateby)
        {
            p_function.Value = 10;
            p_text_param1.Value = estcode;
            p_text_param2.Value = companycode;
            p_int_param1.Value = userid;
            p_int_param7.Value = Int32.Parse(updateby);
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_text_param1, p_text_param2, p_int_param1, p_int_param7);
        }

        public DataSet DeleteSettingAssetType(int userid, int type, string updateby, string estcode, string companycode)
        {
            p_function.Value = 11;
            p_int_param1.Value = userid;
            p_int_param2.Value = type;
            p_int_param7.Value = Int32.Parse(updateby);
            p_text_param3.Value = estcode;
            p_text_param4.Value = companycode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_int_param1, p_int_param2, p_int_param7, p_text_param3, p_text_param4);
        }

        public DataSet DeleteSettingAssetMaster(int userid, int code, string updateby, string estcode, string companycode)
        {
            p_function.Value = 12;
            p_int_param1.Value = userid;
            p_int_param2.Value = code;
            p_int_param7.Value = Int32.Parse(updateby);
            p_text_param3.Value = estcode;
            p_text_param4.Value = companycode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_int_param1, p_int_param2, p_int_param7, p_text_param3, p_text_param4);
        }

        public DataSet GetMasterAWMCondition()
        {
            p_function.Value = 13;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function);
        }

        public DataSet GetStatusEdit(string estcode, string companycode, int userid)
        {
            p_function.Value = 16;
            p_text_param1.Value = estcode;
            p_text_param2.Value = companycode;
            p_int_param1.Value = userid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_text_param1, p_text_param2, p_int_param1);
        }

        public DataSet GetColumnName(string layerName)
        {
            p_function.Value = 17;
            p_text_param1.Value = layerName;
            if (layerName == "") { p_int_param1.Value = null; } else { p_int_param1.Value = layerName; }
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1);
        }

        public DataSet GetMasterCondition()
        {
            p_function.Value = 18;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function);
        }

        public DataSet GetMasterType(string assetType)
        {
            p_function.Value = 19;
            p_text_param1.Value = assetType;
            if (assetType == "") { p_int_param1.Value = null; } else { p_int_param1.Value = assetType; }
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1);
        }

        public DataSet GetMasterMaterial(string assetType)
        {
            p_function.Value = 20;
            p_text_param1.Value = assetType;
            if (assetType == "") { p_int_param1.Value = null; } else { p_int_param1.Value = assetType; }
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1);
        }

        public DataSet GetMasterAWM(string tipe)
        {
            p_function.Value = 17;
            p_text_param1.Value = tipe;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString().ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_text_param1);
        }

        public DataSet GetCoordAwal(string layername, string globalid)
        {
            p_function.Value = 18;
            p_text_param1.Value = layername;
            p_text_param2.Value = globalid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_text_param1, p_text_param2);
        }

        public DataSet GetLayerBaseMap(string area)
        {
            p_function.Value = 43;
            p_text_param1.Value = area;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "WTW_Web", p_function, p_text_param1);
        }

        // Linked database db linked
        public DataSet InsertHistory(string layername, string globalid, int tipe)
        {
            p_function.Value = 1;
            p_text_param1.Value = layername;
            p_text_param2.Value = globalid;
            p_int_param1.Value = tipe;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Lingked", p_function, p_text_param1, p_text_param2, p_int_param1);
        }

        // Linked
        public DataSet GetParitBy(int fc, string value)
        {
            p_function.Value = fc;
            p_text_param1.Value = value;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "WTW_Web_Linked", p_function, p_text_param1);
        }

        // Linked Map
        public DataSet GetSummaryObject(int fc, string area, int status)
        {
            p_function.Value = fc;
            p_text_param1.Value = area;
            p_int_param1.Value = status;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_int_param1);
        }

        public DataSet ToQC(string layername, string globalid, string remarks)
        {
            p_function.Value = 4;
            p_text_param1.Value = layername;
            p_text_param2.Value = globalid;
            p_text_param3.Value = remarks;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_text_param2, p_text_param3);
        }

        public DataSet ToApprove(string layername, string globalid, string remarks)
        {
            p_function.Value = 5;
            p_text_param1.Value = layername;
            p_text_param2.Value = globalid;
            p_text_param3.Value = remarks;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_text_param2, p_text_param3);
        }

        public DataSet ToRejectReSurvey(string layername, string globalid, string remarks)
        {
            p_function.Value = 6;
            p_text_param1.Value = layername;
            p_text_param2.Value = globalid;
            p_text_param3.Value = remarks;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_text_param2, p_text_param3);
        }

        public DataSet ToRejectDelete(string layername, string globalid, string remarks)
        {
            p_function.Value = 7;
            p_text_param1.Value = layername;
            p_text_param2.Value = globalid;
            p_text_param3.Value = remarks;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_text_param2, p_text_param3);
        }

        public DataSet TambahBRD1(string estnr, string block, string type, int condition,
        decimal length, decimal width, int flowdir, int status_object, string createuser,
        string remark, string shape, string companycode, string groupcompany, string surveydate,
        int qc, int tahun, int bulan, int durasi
        )
        {
            p_function.Value = 8;
            p_text_param8.Value = shape;
            p_text_param1.Value = estnr;
            p_text_param2.Value = block;
            p_text_param3.Value = type;
            p_int_param1.Value = condition;
            p_float_param1.Value = length;
            p_float_param2.Value = width;
            p_int_param2.Value = flowdir;
            p_int_param3.Value = status_object;
            p_text_param4.Value = createuser;
            p_text_param5.Value = remark;
            p_text_param6.Value = companycode;
            p_text_param7.Value = groupcompany;
            p_text_param9.Value = surveydate;
            p_int_param4.Value = qc;
            p_int_param5.Value = tahun;
            p_int_param6.Value = bulan;
            p_int_param7.Value = durasi;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param8, p_text_param1, p_text_param2, p_text_param3, p_int_param1, p_float_param1, p_float_param2, p_int_param2, p_int_param3, p_text_param4, p_text_param5, p_text_param6, p_text_param7, p_text_param9, p_int_param4, p_int_param5, p_int_param6, p_int_param7);
        }

        public DataSet EditBRD1(string estnr, string block, string type, int condition,
        decimal length, decimal width, int flowdir, int status_object, string createuser,
        string remark, string shape, string companycode, string groupcompany, string surveydate,
        int qc, int tahun, int bulan, int durasi, string globalid
        )
        {
            p_function.Value = 800;
            p_text_param8.Value = shape;
            p_text_param1.Value = estnr;
            p_text_param2.Value = block;
            p_text_param3.Value = type;
            p_int_param1.Value = condition;
            p_float_param1.Value = length;
            p_float_param2.Value = width;
            p_int_param2.Value = flowdir;
            p_int_param3.Value = status_object;
            p_text_param4.Value = createuser;
            p_text_param5.Value = remark;
            p_text_param6.Value = companycode;
            p_text_param7.Value = groupcompany;
            p_text_param9.Value = surveydate;
            p_int_param4.Value = qc;
            p_int_param5.Value = tahun;
            p_int_param6.Value = bulan;
            p_int_param7.Value = durasi;
            p_text_param11.Value = globalid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param8, p_text_param1, p_text_param2, p_text_param3, p_int_param1, p_float_param1, p_float_param2, p_int_param2, p_int_param3, p_text_param4, p_text_param5, p_text_param6, p_text_param7, p_text_param9, p_int_param4, p_int_param5, p_int_param6, p_int_param7, p_text_param11);
        }

        public DataSet TambahCVT1(string estnr, string block, string type, int condition,
        decimal length, decimal width, int flowdir, int status_object, string createuser,
        string remark, string shape, string companycode, string groupcompany, string surveydate,
        int qc, int tahun, int bulan, int durasi
        )
        {
            p_function.Value = 9;
            p_text_param1.Value = estnr;
            p_text_param2.Value = block;
            p_text_param3.Value = type;
            p_int_param1.Value = condition;
            p_float_param1.Value = length;
            p_float_param2.Value = width;
            p_int_param2.Value = flowdir;
            p_int_param3.Value = status_object;
            p_text_param4.Value = createuser;
            p_text_param5.Value = remark;
            p_text_param8.Value = shape;
            p_text_param6.Value = companycode;
            p_text_param7.Value = groupcompany;
            p_text_param9.Value = surveydate;
            p_int_param4.Value = qc;
            p_int_param5.Value = tahun;
            p_int_param6.Value = bulan;
            p_int_param7.Value = durasi;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_text_param2, p_text_param3, p_int_param1, p_float_param1, p_float_param2, p_int_param2, p_int_param3, p_text_param4, p_text_param5, p_text_param8, p_text_param6, p_text_param7, p_text_param9, p_int_param4, p_int_param5, p_int_param6, p_int_param7);
        }

        public DataSet EditCVT1(string estnr, string block, string type, int condition,
        decimal length, decimal width, int flowdir, int status_object, string createuser,
        string remark, string shape, string companycode, string groupcompany, string surveydate,
        int qc, int tahun, int bulan, int durasi, string globalid
        )
        {
            p_function.Value = 900;
            p_text_param1.Value = estnr;
            p_text_param2.Value = block;
            p_text_param3.Value = type;
            p_int_param1.Value = condition;
            p_float_param1.Value = length;
            p_float_param2.Value = width;
            p_int_param2.Value = flowdir;
            p_int_param3.Value = status_object;
            p_text_param4.Value = createuser;
            p_text_param5.Value = remark;
            p_text_param8.Value = shape;
            p_text_param6.Value = companycode;
            p_text_param7.Value = groupcompany;
            p_text_param9.Value = surveydate;
            p_int_param4.Value = qc;
            p_int_param5.Value = tahun;
            p_int_param6.Value = bulan;
            p_int_param7.Value = durasi;
            p_text_param11.Value = globalid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_text_param2, p_text_param3, p_int_param1, p_float_param1, p_float_param2, p_int_param2, p_int_param3, p_text_param4, p_text_param5, p_text_param8, p_text_param6, p_text_param7, p_text_param9, p_int_param4, p_int_param5, p_int_param6, p_int_param7, p_text_param11);
        }

        public DataSet PlanningToBudget(string layername, string user, string globalid, int tahun)
        {
            p_function.Value = 10;
            p_text_param1.Value = layername;
            p_text_param2.Value = user;
            p_text_param3.Value = globalid;
            p_int_param1.Value = tahun;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_text_param2, p_text_param3, p_int_param1);
        }

        public DataSet EditLokasi(string layername, string user, string globalid, string shape, string estnr, string block)
        {
            p_function.Value = 11;
            p_text_param1.Value = layername;
            p_text_param8.Value = shape;
            p_text_param2.Value = estnr;
            p_text_param3.Value = block;
            p_text_param4.Value = user;
            p_text_param11.Value = globalid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_text_param8, p_text_param2, p_text_param3, p_text_param4, p_text_param11);
        }

        public DataSet TambahWGT1(
            string shape, string groupcompany, string companycode, string estnr, string block,
            string username,
            string remarks, string surveydate, int qc, int status_object,
            int tahun, int bulan, int duration,
            int condition,
            int type, int tot, decimal width, int flowdir
        )
        {
            p_function.Value = 12;
            p_text_param8.Value = shape;
            p_text_param1.Value = groupcompany;
            p_text_param2.Value = companycode;
            p_text_param3.Value = estnr;
            p_text_param4.Value = block;
            p_text_param5.Value = username;
            p_text_param6.Value = remarks;
            p_text_param7.Value = surveydate;
            p_int_param1.Value = qc;
            p_int_param2.Value = status_object;
            p_int_param3.Value = tahun;
            p_int_param4.Value = bulan;
            p_int_param5.Value = duration;
            p_int_param6.Value = condition;

            p_int_param7.Value = type;
            p_int_param8.Value = tot;
            p_float_param1.Value = width;
            p_int_param9.Value = flowdir;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param8, p_text_param1, p_text_param2, p_text_param3, p_text_param4, p_text_param5, p_text_param6, p_text_param7, p_int_param1, p_int_param2, p_int_param3, p_int_param4, p_int_param5, p_int_param6,
                p_int_param7, p_int_param8, p_float_param1, p_int_param9);
        }

        public DataSet EditWGT1(
            string globalid, int condition, string username,
            int tahun, int bulan, int duration, string remark,

            int type, int tot, decimal width, int flowdir
        )
        {
            p_function.Value = 120;
            p_text_param1.Value = globalid;
            p_int_param1.Value = condition;
            p_text_param2.Value = username;
            p_int_param2.Value = tahun;
            p_int_param3.Value = bulan;
            p_int_param4.Value = duration;
            p_text_param3.Value = remark;

            p_int_param5.Value = type;
            p_int_param6.Value = tot;
            p_float_param1.Value = width;
            p_int_param7.Value = flowdir;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_int_param1, p_text_param2, p_int_param2, p_int_param3, p_int_param4, p_text_param3,
                p_int_param5, p_int_param6, p_float_param1, p_int_param7);
        }

        public DataSet TambahPUM1(
            string shape, string groupcompany, string companycode, string estnr, string block,
            string username,
            string remarks, string surveydate, int qc, int status_object,
            int tahun, int bulan, int duration,
            int condition,
            string brand, string model, decimal hp, decimal diameter, decimal tput
        )
        {
            p_function.Value = 13;
            p_text_param8.Value = shape;
            p_text_param1.Value = groupcompany;
            p_text_param2.Value = companycode;
            p_text_param3.Value = estnr;
            p_text_param4.Value = block;
            p_text_param5.Value = username;
            p_text_param6.Value = remarks;
            p_text_param7.Value = surveydate;
            p_int_param1.Value = qc;
            p_int_param2.Value = status_object;
            p_int_param3.Value = tahun;
            p_int_param4.Value = bulan;
            p_int_param5.Value = duration;
            p_int_param6.Value = condition;

            p_text_param9.Value = brand;
            p_text_param10.Value = model;
            p_float_param1.Value = hp;
            p_float_param2.Value = diameter;
            p_float_param3.Value = tput;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param8, p_text_param1, p_text_param2, p_text_param3, p_text_param4, p_text_param5, p_text_param6, p_text_param7, p_int_param1, p_int_param2, p_int_param3, p_int_param4, p_int_param5, p_int_param6,
                p_text_param9, p_text_param10, p_float_param1, p_float_param2, p_float_param3);
        }

        public DataSet EditPUM1(
            string globalid, int condition, string username,
            int tahun, int bulan, int duration, string remark,

            string brand, string model, decimal hp, decimal diameter, decimal tput
        )
        {
            p_function.Value = 130;
            p_text_param1.Value = globalid;
            p_int_param1.Value = condition;
            p_text_param2.Value = username;
            p_int_param2.Value = tahun;
            p_int_param3.Value = bulan;
            p_int_param4.Value = duration;
            p_text_param3.Value = remark;

            p_text_param4.Value = brand;
            p_text_param5.Value = model;
            p_float_param1.Value = hp;
            p_float_param2.Value = diameter;
            p_float_param3.Value = tput;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_int_param1, p_text_param2, p_int_param2, p_int_param3, p_int_param4, p_text_param3,
                p_text_param4, p_text_param5, p_float_param1, p_float_param2, p_float_param3);
        }

        public DataSet TambahDAM1(
            string shape, string groupcompany, string companycode, string estnr, string block,
            string username,
            string remarks, string surveydate, int qc, int status_object,
            int tahun, int bulan, int duration,
            int condition,
            decimal width, int flowdir
        )
        {
            p_function.Value = 14;
            p_text_param8.Value = shape;
            p_text_param1.Value = groupcompany;
            p_text_param2.Value = companycode;
            p_text_param3.Value = estnr;
            p_text_param4.Value = block;
            p_text_param5.Value = username;
            p_text_param6.Value = remarks;
            p_text_param7.Value = surveydate;
            p_int_param1.Value = qc;
            p_int_param2.Value = status_object;
            p_int_param3.Value = tahun;
            p_int_param4.Value = bulan;
            p_int_param5.Value = duration;
            p_int_param6.Value = condition;

            p_float_param1.Value = width;
            p_int_param7.Value = flowdir;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param8, p_text_param1, p_text_param2, p_text_param3, p_text_param4, p_text_param5, p_text_param6, p_text_param7, p_int_param1, p_int_param2, p_int_param3, p_int_param4, p_int_param5, p_int_param6,
                p_float_param1, p_int_param7);
        }

        public DataSet EditDAM1(
            string globalid, int condition, string username,
            int tahun, int bulan, int duration, string remark,

            decimal width, int flowdir
        )
        {
            p_function.Value = 140;
            p_text_param1.Value = globalid;
            p_int_param1.Value = condition;
            p_text_param2.Value = username;
            p_int_param2.Value = tahun;
            p_int_param3.Value = bulan;
            p_int_param4.Value = duration;
            p_text_param3.Value = remark;

            p_float_param1.Value = width;
            p_int_param5.Value = flowdir;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_int_param1, p_text_param2, p_int_param2, p_int_param3, p_int_param4, p_text_param3,
                p_float_param1, p_int_param5);
        }

        public DataSet TambahBUN1(
            string shape, string groupcompany, string companycode, string estnr, string block,
            string username,
            string remarks, string surveydate, int qc, int status_object,
            int tahun, int bulan, int duration,
            int condition,
            int type, decimal spillway, int flowdir
        )
        {
            p_function.Value = 15;
            p_text_param8.Value = shape;
            p_text_param1.Value = groupcompany;
            p_text_param2.Value = companycode;
            p_text_param3.Value = estnr;
            p_text_param4.Value = block;
            p_text_param5.Value = username;
            p_text_param6.Value = remarks;
            p_text_param7.Value = surveydate;
            p_int_param1.Value = qc;
            p_int_param2.Value = status_object;
            p_int_param3.Value = tahun;
            p_int_param4.Value = bulan;
            p_int_param5.Value = duration;
            p_int_param6.Value = condition;

            p_int_param7.Value = type;
            p_float_param1.Value = spillway;
            p_int_param8.Value = flowdir;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param8, p_text_param1, p_text_param2, p_text_param3, p_text_param4, p_text_param5, p_text_param6, p_text_param7, p_int_param1, p_int_param2, p_int_param3, p_int_param4, p_int_param5, p_int_param6,
                p_int_param7, p_float_param1, p_int_param8);
        }

        public DataSet EditBUN1(
            string globalid, int condition, string username,
            int tahun, int bulan, int duration, string remark,

            int type, decimal spillway, int flowdir
        )
        {
            p_function.Value = 140;
            p_text_param1.Value = globalid;
            p_int_param1.Value = condition;
            p_text_param2.Value = username;
            p_int_param2.Value = tahun;
            p_int_param3.Value = bulan;
            p_int_param4.Value = duration;
            p_text_param3.Value = remark;

            p_int_param5.Value = type;
            p_float_param1.Value = spillway;
            p_int_param6.Value = flowdir;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_int_param1, p_text_param2, p_int_param2, p_int_param3, p_int_param4, p_text_param3,
                p_int_param5, p_float_param1, p_int_param6);
        }

        public DataSet TambahZIP1(
            string shape, string groupcompany, string companycode, string estnr, string block,
            string username,
            string remarks, string surveydate, int qc, int status_object,
            int tahun, int bulan, int duration,
            int condition,
            int type, int material, decimal size, int flowdir
        )
        {
            p_function.Value = 16;
            p_text_param8.Value = shape;
            p_text_param1.Value = groupcompany;
            p_text_param2.Value = companycode;
            p_text_param3.Value = estnr;
            p_text_param4.Value = block;
            p_text_param5.Value = username;
            p_text_param6.Value = remarks;
            p_text_param7.Value = surveydate;
            p_int_param1.Value = qc;
            p_int_param2.Value = status_object;
            p_int_param3.Value = tahun;
            p_int_param4.Value = bulan;
            p_int_param5.Value = duration;
            p_int_param6.Value = condition;

            p_int_param7.Value = type;
            p_int_param8.Value = material;
            p_float_param1.Value = size;
            p_int_param9.Value = flowdir;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param8, p_text_param1, p_text_param2, p_text_param3, p_text_param4, p_text_param5, p_text_param6, p_text_param7, p_int_param1, p_int_param2, p_int_param3, p_int_param4, p_int_param5, p_int_param6,
                p_int_param7, p_int_param8, p_float_param1, p_int_param9);
        }



        public DataSet EditZIP1(
            string globalid, int condition, string username,
            int tahun, int bulan, int duration, string remark,

            int type, int material, decimal size, int flowdir
        )
        {
            p_function.Value = 160;
            p_text_param1.Value = globalid;
            p_int_param1.Value = condition;
            p_text_param2.Value = username;
            p_int_param2.Value = tahun;
            p_int_param3.Value = bulan;
            p_int_param4.Value = duration;
            p_text_param3.Value = remark;

            p_int_param5.Value = type;
            p_int_param6.Value = material;
            p_float_param1.Value = size;
            p_int_param7.Value = flowdir;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web_Linked", p_function, p_text_param1, p_int_param1, p_text_param2, p_int_param2, p_int_param3, p_int_param4, p_text_param3,
                p_int_param5, p_int_param6, p_float_param1, p_int_param7);
        }
    }
}