﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace PZO_PiezometerOnlineMap.Mapper
{
    public class PZO_ProjectMapperTambahan
    {
        #region Parameters Declaration

        protected SqlParameter p_function = new SqlParameter("@function", SqlDbType.Int);
        protected SqlParameter p_CompanyCode = new SqlParameter("@CompanyCode", SqlDbType.VarChar, 15);
        protected SqlParameter p_EstCode = new SqlParameter("@EstCode", SqlDbType.VarChar, 10);
        protected SqlParameter p_Division = new SqlParameter("@Division", SqlDbType.VarChar, 10);
        protected SqlParameter p_Block = new SqlParameter("@Block", SqlDbType.VarChar, 255);
        protected SqlParameter p_ProjectTypeName = new SqlParameter("@ProjectTypeName", SqlDbType.VarChar, 50);
        protected SqlParameter p_UserID = new SqlParameter("@userid", SqlDbType.BigInt);
        protected SqlParameter p_Date = new SqlParameter("@Date", SqlDbType.DateTime);
        protected SqlParameter p_PieRecordID = new SqlParameter("@PieRecordID", SqlDbType.VarChar, 10);
        protected SqlParameter p_ID = new SqlParameter("@ID", SqlDbType.Int);

        protected SqlParameter p_LayerDisplayName = new SqlParameter("@LayerDisplayName", SqlDbType.VarChar, 100);
        protected SqlParameter p_LayerURL = new SqlParameter("@LayerURL", SqlDbType.VarChar, 250);
        protected SqlParameter p_LayerURLName = new SqlParameter("@LayerURLName", SqlDbType.VarChar, 100);
        protected SqlParameter p_LayerCategory = new SqlParameter("@LayerCategory", SqlDbType.VarChar, 10);
        protected SqlParameter p_LayerType = new SqlParameter("@LayerType", SqlDbType.Int);
        protected SqlParameter p_LayerDisplay = new SqlParameter("@LayerDisplay", SqlDbType.VarChar, 5);
        protected SqlParameter p_FieldID = new SqlParameter("@FieldID", SqlDbType.Int);

        protected SqlParameter p_StartWeek = new SqlParameter("@StartWeek", SqlDbType.BigInt);
        protected SqlParameter p_EndWeek = new SqlParameter("@EndWeek", SqlDbType.BigInt);
        protected SqlParameter p_KetinggianMin = new SqlParameter("@KetinggianMin", SqlDbType.Int);
        protected SqlParameter p_KetinggianMax = new SqlParameter("@KetinggianMax", SqlDbType.Int);
        protected SqlParameter p_Remarks = new SqlParameter("@Remarks", SqlDbType.VarChar, 10);
        protected SqlParameter p_IdWmArea = new SqlParameter("@IdWmArea", SqlDbType.BigInt);
        protected SqlParameter p_WmaCode = new SqlParameter("@wmacode", SqlDbType.VarChar);
        protected SqlParameter p_raCode = new SqlParameter("@ra_code", SqlDbType.VarChar);
        protected SqlParameter p_year = new SqlParameter("@year", SqlDbType.Int);
        protected SqlParameter p_month = new SqlParameter("@month", SqlDbType.Int);
        protected SqlParameter p_week = new SqlParameter("@week", SqlDbType.Int);
        protected SqlParameter p_minRangeZone = new SqlParameter("@minRangeZone", SqlDbType.Float);
        protected SqlParameter p_maxRangeZone = new SqlParameter("@maxRangeZone", SqlDbType.Float);
        protected SqlParameter p_estCode2 = new SqlParameter("@estCode2", SqlDbType.VarChar, 255);
        protected SqlParameter p_PiezoID = new SqlParameter("@PieRecordID", SqlDbType.VarChar, (255));
        protected SqlParameter p_block = new SqlParameter("@block", SqlDbType.VarChar, (255));
        protected SqlParameter p_Now = new SqlParameter("@Now", SqlDbType.Date);
        protected SqlParameter p_WeekName = new SqlParameter("@WeekName", SqlDbType.VarChar, 255);
        protected SqlParameter p_estCodeZona = new SqlParameter("@estCodeZona", SqlDbType.VarChar, 255);
        protected SqlParameter p_dayparam = new SqlParameter("@dayParam", SqlDbType.Int, 255);
        protected SqlParameter p_weekId = new SqlParameter("@weekId", SqlDbType.Int, 255);
        protected SqlParameter p_IDWEEK = new SqlParameter("@IDWEEK", SqlDbType.Int, 255); // Sama
        protected SqlParameter p_range = new SqlParameter("@range", SqlDbType.Int, 255); // Sama
        protected SqlParameter p_IndicatorID = new SqlParameter("@IndicatorID", SqlDbType.Int, 255); // Sama
        protected SqlParameter p_IDWEEKBefore = new SqlParameter("@IDWEEKBefore", SqlDbType.Int, 255); // Sama
        protected SqlParameter p_wmArea = new SqlParameter("@wmArea", SqlDbType.VarChar, int.MaxValue);
        protected SqlParameter p_idWMarea = new SqlParameter("@IdwmArea", SqlDbType.VarChar, 50);
        protected SqlParameter p_StatusKetinggian = new SqlParameter("@statusKetinggian ", SqlDbType.VarChar, 255);
        protected SqlParameter p_statusPerubahan = new SqlParameter("@statusPerubahan ", SqlDbType.VarChar, 255);
        protected SqlParameter p_multiEstCode = new SqlParameter("@estCode", SqlDbType.VarChar, 255);
        protected SqlParameter p_multipulau = new SqlParameter("@wm", SqlDbType.VarChar, 255);
        protected SqlParameter p_IDWEEKStart = new SqlParameter("@IDWEEKStart", SqlDbType.Int, 255); // Sama
        protected SqlParameter p_IDWEEKEnd = new SqlParameter("@IDWEEKEnd", SqlDbType.Int, 255); // Sama
        protected SqlParameter p_zona = new SqlParameter("@zona", SqlDbType.VarChar, 50);
        protected SqlParameter p_check = new SqlParameter("@checked", SqlDbType.VarChar, 50);
        
        protected SqlParameter p_dateParam = new SqlParameter("@dateParam", SqlDbType.VarChar, 50);


        #endregion

        #region Method

        public DataTable ListLayer(string EstCode)
        {
            p_function.Value = 1;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode).Tables[0];
        }

        public DataSet GetEstateDetail(string EstCode)
        {
            p_function.Value = 2;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode);
        }

        public DataTable ListBlockByEstate(string EstCode)
        {
            p_function.Value = 3;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode).Tables[0];
        }

        public DataTable GetBlockExtent(string EstCode, string Block)
        {
            p_function.Value = 4;
            p_EstCode.Value = EstCode;
            p_Block.Value = Block;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode, p_Block).Tables[0];
        }

        public DataTable GetLayerField(int FieldID)
        {
            p_function.Value = 5;
            p_FieldID.Value = FieldID;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_FieldID).Tables[0];
        }

        public DataTable GetPiezoRecordDetailByPieRecordID(string PieRecordID)
        {
            p_function.Value = 6;
            p_PieRecordID.Value = PieRecordID;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_PieRecordID).Tables[0];
        }

        public DataTable GetPiezoRecordDetailByPieRecordIDWithLimitDate(string PieRecordID, long startDate, long endDate)
        {
            p_function.Value = 20;
            p_PieRecordID.Value = PieRecordID;
            p_StartWeek.Value = startDate;
            p_EndWeek.Value = endDate;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function, p_PieRecordID, p_StartWeek, p_EndWeek).Tables[0];
        }
        
        public DataTable GetPiezoRecordDetailByPieRecordIDWithLimitDate2(string PieRecordID, int week, int month,int year)
        {
            p_function.Value = 25;
            p_PieRecordID.Value = PieRecordID;
            p_week.Value = week;
            p_month.Value = month;
            p_year.Value = year;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function, p_PieRecordID, p_week, p_month, p_year).Tables[0];
        }

        public DataTable GetWeekFromCurrentDate()
        {
            p_function.Value = 7;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }

        public DataTable GetWeekFromID(int ID)
        {
            p_function.Value = 8;
            p_ID.Value = ID;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_ID).Tables[0];
        }

        public string GetZone(string EstCode)
        {
            p_function.Value = 9;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode).Tables[0].Rows[0]["ZONE"].ToString();
        }

        public DataTable GetListWeek()
        {
            p_function.Value = 10;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }

        public DataTable ListLayerType()
        {
            p_function.Value = 104;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IMV_OnlineMap", p_function).Tables[0];
        }

        public DataTable GetWaterDepthIndicator()
        {
            p_function.Value = 4;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("MOB_PZO_ALL_2", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;
        }
        
        public DataTable GetAllUserGis()
        {
            p_function.Value = 26;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("MOB_PZO_ALL_2", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;
        }

        public DataTable GetQueryFilter(string estCode, long startWeek, long endWeek)
        {
            p_function.Value = 5;
            p_EstCode.Value = estCode;
            p_StartWeek.Value = startWeek;
            p_EndWeek.Value = endWeek;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function, p_EstCode, p_StartWeek, p_EndWeek).Tables[0];
        }

        public DataTable GetMappingPiezoRecordIPLAS(string estCode)
        {
            p_function.Value = 6;
            p_EstCode.Value = estCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function, p_EstCode).Tables[0];
        }

        public DataTable GetQueryFilterWMArea(int wmarea, long startWeek, long endWeek)
        {
            //p_function.Value = 16;
            //p_IdWmArea.Value = wmarea;
            //p_StartWeek.Value = startWeek;
            //p_EndWeek.Value = endWeek;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function, p_IdWmArea, p_StartWeek, p_EndWeek).Tables[0];


            p_function = new SqlParameter("@function", SqlDbType.Int);
            p_IdWmArea = new SqlParameter("@IdWmArea", SqlDbType.BigInt);
            p_StartWeek = new SqlParameter("@StartWeek", SqlDbType.BigInt);
            p_EndWeek = new SqlParameter("@EndWeek", SqlDbType.BigInt);
            p_function.Value = 16;
            p_IdWmArea.Value = wmarea;
            p_StartWeek.Value = startWeek;
            p_EndWeek.Value = endWeek;

            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("MOB_PZO_ALL_2", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_IdWmArea);
                command.Parameters.Add(p_StartWeek);
                command.Parameters.Add(p_EndWeek);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;
        }

        public DataTable GetMappingPiezoRecordIPLASWMarean(int wmarea)
        {
            p_function.Value = 17;
            p_IdWmArea.Value = wmarea;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function, p_IdWmArea).Tables[0];
        }

        public DataSet getWMAreaByEstCode(String estCode)
        {
            p_function.Value = 14;
            p_EstCode.Value = estCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function, p_EstCode);
        }

        public DataTable getAllEstateByWMID(String id)
        {
            p_function.Value = 15;
            p_IdWmArea.Value = Convert.ToInt32(id);
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function, p_IdWmArea).Tables[0];
        }
        public DataSet PerbandinganCurahHujan(string zona, string raCode, float minRangeZone, float maxRangeZone, int year, int bulan, string week, string estCode)
        {
            //p_function.Value = 17;
            p_function.Value = 21;
            p_WmaCode.Value = zona;
            p_raCode.Value = raCode;
            p_minRangeZone.Value = minRangeZone;
            p_maxRangeZone.Value = maxRangeZone;
            p_year.Value = year;
            p_month.Value = bulan;
            p_week.Value = week;
            p_estCode2.Value = estCode;
            var ds = new DataSet();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("RPT_GROUPREPORT", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 600 }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_WmaCode);
                command.Parameters.Add(p_raCode);
                command.Parameters.Add(p_minRangeZone);
                command.Parameters.Add(p_maxRangeZone);
                command.Parameters.Add(p_year);
                command.Parameters.Add(p_month);
                command.Parameters.Add(p_week);
                command.Parameters.Add(p_estCode2);
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }

            return ds;

            //p_function.Value = 45;
            //p_function.Value = 17;
            //p_WmaCode.Value = zona;
            //p_raCode.Value = raCode;
            //p_minRangeZone.Value = minRangeZone;
            //p_maxRangeZone.Value = maxRangeZone;
            //p_year.Value = year;
            //p_month.Value = bulan;
            //p_week.Value = week;            

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, , "RPT_GROUPREPORT", p_function, p_WmaCode, p_raCode, p_minRangeZone, p_maxRangeZone, p_year, p_month, p_week);
        }
        public DataTable GetRambuAir(string wmaCode)
        {
            p_function.Value = 19;
            p_WmaCode.Value = wmaCode;
            var ds = new DataSet();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("RPT_GROUPREPORT", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 600 }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_WmaCode);
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }

            return ds.Tables[0];
            //p_function.Value = 19;
            //p_WmaCode.Value = wmaCode;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_WmaCode).Tables[0];
        }

        public DataTable getAllWMArea()
        {
            p_function.Value = 18;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function).Tables[0];
        }

        public DataTable getAllWMAreaEstate()
        {
            p_function.Value = 19;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "MOB_PZO_ALL_2", p_function).Tables[0];
        }

        public DataTable getAllWMAreaZone(int idWMArea)
        {
            p_function.Value = 26;
            p_IdWmArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_IdWmArea).Tables[0];
        }
        public DataTable getRambuAir(int idZona)
        {
            p_function.Value = 27;
            p_IdWmArea.Value = idZona;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_PieRecordID).Tables[0];
        }
        public DataTable GetWeek(int week, int month, int year)
        {
            p_function.Value = 20;

            p_year.Value = year;
            p_month.Value = month;
            p_week.Value = week;
            var ds = new DataSet();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("RPT_GROUPREPORT", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 600 }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_year);
                command.Parameters.Add(p_month);
                command.Parameters.Add(p_week);
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }

            return ds.Tables[0];

            //p_function.Value = 20;
            //p_week.Value = week;
            //p_month.Value = month;
            //p_year.Value = year;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_week, p_month, p_year).Tables[0];
        }
        public DataTable GetExtendCoordinate(string wmaCode)
        {
            p_function.Value = 24;
            p_WmaCode.Value = wmaCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_WmaCode).Tables[0];

        }
        public DataSet GetPerbandinganTMATTMAS2(string wmcode, string racode, int tahun, int bulan, int minggu, float maxX, float minX, string estCode)
        {
            p_function.Value = 21;
            p_WmaCode.Value = wmcode;
            p_raCode.Value = racode;
            p_year.Value = tahun;
            p_month.Value = bulan;
            p_week.Value = minggu;
            p_minRangeZone.Value = minX;
            p_maxRangeZone.Value = maxX;
            p_estCode2.Value = estCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_WmaCode, p_raCode, p_year, p_month, p_week, p_maxRangeZone, p_minRangeZone, p_estCode2);
        }
        public string insertParamForAnalisisTmas(string wmaCode, float minRange, float maxRange, string curahHujan, string rambuAir)
        {
            p_function.Value = 25;
            p_WmaCode.Value = wmaCode;
            p_minRangeZone.Value = minRange;
            p_maxRangeZone.Value = maxRange;
            p_estCode2.Value = curahHujan;
            p_raCode.Value = rambuAir;
            return SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_WmaCode, p_minRangeZone, p_maxRangeZone, p_estCode2, p_raCode).ToString();
        }
        public DataTable getMinMaxValue(string wmaCode)
        {
            p_function.Value = 26;

            p_WmaCode.Value = wmaCode;
            var ds = new DataSet();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("RPT_GROUPREPORT", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_WmaCode);
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                conn.Close();
            }

            return ds.Tables[0];
        }
        public DataTable GetListCurahHujan(int idwmarea)
        {
            p_function.Value = 29;
            p_IdWmArea.Value = idwmarea;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_IdWmArea).Tables[0];
        }
        public DataTable GetZonaByWmArea(int idWmarea)
        {
            p_function.Value = 30;
            p_IdWmArea.Value = idWmarea;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_IdWmArea).Tables[0];
        }
        public DataTable GetAllExtendCoordinate()
        {
            p_function.Value = 1;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT2", p_function).Tables[0];
        }
        //public DataTable newGetGraphByZona(string wmaCode)
        //{
        //    p_function.Value = 39;
        //    p_week.Value = 4;
        //    p_month.Value = 3;
        //    p_year.Value = 2019;
        //    p_IdWmArea.Value = 1;
        //    p_WmaCode.Value = wmaCode;
        //    return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_WmaCode, p_week, p_month, p_year, p_IdWmArea).Tables[0];
        //}

        public DataSet newGetGraphByZona(string wmaCode, int week, int month, int idWmArea, int year)
        {
            p_function = new SqlParameter("@function", SqlDbType.Int);
            p_WmaCode = new SqlParameter("@wmacode", SqlDbType.VarChar, 255);
            p_week = new SqlParameter("@week", SqlDbType.Int);
            p_month = new SqlParameter("@month", SqlDbType.Int);
            p_IdWmArea = new SqlParameter("@IdWmArea", SqlDbType.BigInt);
            p_year = new SqlParameter("@year", SqlDbType.Int);
            p_function.Value = 37;
            p_WmaCode.Value = wmaCode;
            p_week.Value = week;
            p_month.Value = month;
            p_IdWmArea.Value = idWmArea;
            p_year.Value = year;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_week, p_month, p_year, p_IdWmArea, p_WmaCode);
            var ds = new DataSet();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_OnlineMap", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_WmaCode);
                command.Parameters.Add(p_week);
                command.Parameters.Add(p_month);
                command.Parameters.Add(p_IdWmArea);
                command.Parameters.Add(p_year);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                command.Parameters.Clear();
                conn.Close();
            }

            return ds;
        }

        public DataSet GetgraphbyPiezoRecordID(string wmaCode, int week, int month, int idWmArea, int year, string pieRecordID)
        {
            p_function = new SqlParameter("@function", SqlDbType.Int);
            p_WmaCode = new SqlParameter("@wmacode", SqlDbType.VarChar, 255);
            p_week = new SqlParameter("@week", SqlDbType.Int);
            p_month = new SqlParameter("@month", SqlDbType.Int);
            p_IdWmArea = new SqlParameter("@IdWmArea", SqlDbType.BigInt);
            p_year = new SqlParameter("@year", SqlDbType.Int);
            p_PieRecordID = new SqlParameter("@PieRecordID", SqlDbType.VarChar, 255);
            p_function.Value = 36;
            p_WmaCode.Value = wmaCode;
            p_week.Value = week;
            p_month.Value = month;
            p_IdWmArea.Value = idWmArea;
            p_year.Value = year;
            p_PieRecordID.Value = pieRecordID;
            // return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_week, p_month, p_year, p_IdWmArea, p_WmaCode, p_PieRecordID);


            var ds = new DataSet();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_OnlineMap", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_WmaCode);
                command.Parameters.Add(p_week);
                command.Parameters.Add(p_month);
                command.Parameters.Add(p_IdWmArea);
                command.Parameters.Add(p_year);
                command.Parameters.Add(p_PieRecordID);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                command.Parameters.Clear();
                conn.Close();
            }

            return ds;
        }

        public string updateBlock(string wmaCode, string block)
        {
            p_function.Value = 32;
            p_WmaCode.Value = wmaCode;
            p_Block.Value = block;
            return SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_WmaCode, p_Block).ToString();
        }

        public DataSet GetReportAnalisaTMATdanTMASbyZona(string zona, int week, int month, int year, string pzoid)
        {
            p_function = new SqlParameter("@function", SqlDbType.Int);
            p_WmaCode = new SqlParameter("@wmacode", SqlDbType.VarChar, 255);
            p_week = new SqlParameter("@week", SqlDbType.Int);
            p_month = new SqlParameter("@month", SqlDbType.Int);
            p_IdWmArea = new SqlParameter("@IdWmArea", SqlDbType.BigInt);
            p_year = new SqlParameter("@year", SqlDbType.Int);
            p_PieRecordID = new SqlParameter("@PieRecordID", SqlDbType.VarChar, 255);
            p_function.Value = 38;
            p_WmaCode.Value = zona;
            p_week.Value = week;
            p_month.Value = month;
            p_year.Value = year;
            p_PieRecordID.Value = pzoid;
            // return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_week, p_month, p_year, p_IdWmArea, p_WmaCode, p_PieRecordID);


            var ds = new DataSet();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("RPT_GROUPREPORT", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_WmaCode);
                command.Parameters.Add(p_week);
                command.Parameters.Add(p_month);
                command.Parameters.Add(p_year);
                command.Parameters.Add(p_PieRecordID);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                command.Parameters.Clear();
                conn.Close();
            }

            return ds;
        }
        public DataTable GetMasterGraph()
        {
            p_function.Value = 38;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }
        public DataSet GetReportAnalisaTMATdanTMASbyZonaAllBlock(string zona, int week, int month, int year)
        {
            p_function = new SqlParameter("@function", SqlDbType.Int);
            p_WmaCode = new SqlParameter("@wmacode", SqlDbType.VarChar, 255);
            p_week = new SqlParameter("@week", SqlDbType.Int);
            p_month = new SqlParameter("@month", SqlDbType.Int);
            p_IdWmArea = new SqlParameter("@IdWmArea", SqlDbType.BigInt);
            p_year = new SqlParameter("@year", SqlDbType.Int);
            p_PieRecordID = new SqlParameter("@PieRecordID", SqlDbType.VarChar, 255);
            p_function.Value = 50;
            p_WmaCode.Value = zona;
            p_week.Value = week;
            p_month.Value = month;
            p_year.Value = year;
            //p_PieRecordID.Value = pzoid;
            // return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_week, p_month, p_year, p_IdWmArea, p_WmaCode, p_PieRecordID);


            var ds = new DataSet();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("RPT_GROUPREPORT", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_WmaCode);
                command.Parameters.Add(p_week);
                command.Parameters.Add(p_month);
                command.Parameters.Add(p_year);
                //command.Parameters.Add(p_PieRecordID);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                command.Parameters.Clear();
                conn.Close();
            }

            return ds;
        }
        public string getElevasiTanahbyBlock(string block)
        {
            p_function.Value = 51;
            p_block.Value = block;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_block).Tables[0].Rows[0]["elevasiTanah"].ToString();

        }
        public DataSet generateWeeklyReportV2(string estCode, string Now)
        {
            p_function.Value = 23;
            p_EstCode.Value = estCode;
            p_WeekName.Value = Now;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT", p_function, p_EstCode, p_WeekName);

        }
        public DataSet getWeekName()
        {
            p_function.Value = 24;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT", p_function);

        }
        public DataSet GetWeekNameByDate(string dateRequest)
        {
            p_function.Value = 27;
            p_Date.Value = dateRequest;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT", p_function, p_Date);

        }
        public DataSet generateWeeklyReport(string estCode, string Now)
        {
            p_function.Value = 17;
            p_EstCode.Value = estCode;
            p_WeekName.Value = Now;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT", p_function, p_EstCode, p_WeekName);

        }
        public DataTable getZonaByKebun(string estCode)
        {
            p_function.Value = 109;
            p_estCodeZona.Value = estCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_PIEZOMETER", p_function, p_estCodeZona).Tables[0];
        }
        public DataSet getWeekNameForPerubahan(int dayparam, int month)
        {
            p_function.Value = 25;
            p_dayparam.Value = dayparam;
            p_month.Value = month;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT", p_function, p_dayparam, p_month);

        }
        public DataSet getNextPrevWeeknameForPerubahan(int WeekId)
        {
            p_function.Value = 26;
            p_weekId.Value = WeekId;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT", p_function, p_weekId);

        }
        public DataSet getNextPrevWeekPetaPerbandingan(int WeekId)
        {
            p_function.Value = 26;
            p_weekId.Value = WeekId;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT", p_function, p_weekId);

        }
        public DataSet getNextPrevWeekPetaPerbandinganxx(int WeekId)
        {
            p_function.Value = 26;
            p_weekId.Value = WeekId;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT", p_function, p_weekId);

        }
        public DataSet getPencatatanPerubahanPiezometer(string wmArea, string weekId, string estCode, string pulau)
        {
            p_function.Value = 1;
            p_IDWEEK.Value = weekId;
            p_wmArea.Value = wmArea;
            p_multiEstCode.Value = estCode;
            p_multipulau.Value = pulau;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT4", p_function, p_IDWEEK, p_wmArea, p_multiEstCode, p_multipulau);
        }
        public DataSet getPencatatanPerubahanPiezometerKLHK(string wmArea, string weekId, string estCode, string pulau)
        {
            p_function.Value = 2;
            p_IDWEEK.Value = weekId;
            p_wmArea.Value = wmArea;
            p_multiEstCode.Value = estCode;
            p_multipulau.Value = pulau;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT4", p_function, p_IDWEEK, p_wmArea, p_multiEstCode, p_multipulau);
        }
        public DataSet getBlockPerubahanTMAT(int IdWeek, string wmArea, string statusKetinggian, string statusperubahan, string estCode, string pulau)
        {
            p_function.Value = 3;
            p_IDWEEK.Value = IdWeek;
            p_wmArea.Value = wmArea;
            p_StatusKetinggian.Value = statusKetinggian;
            p_statusPerubahan.Value = statusperubahan;
            p_multiEstCode.Value = estCode;
            p_multipulau.Value = pulau;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT4", p_function, p_IDWEEK, p_wmArea, p_StatusKetinggian, p_statusPerubahan, p_multiEstCode, p_multipulau);
        }
        public DataSet getBlockPerubahanTMATKLHK(int IdWeek, string wmArea, string statusKetinggian, string statusperubahan, string estCode, string pulau)
        {
            p_function.Value = 4;
            p_IDWEEK.Value = IdWeek;
            p_wmArea.Value = wmArea;
            p_StatusKetinggian.Value = statusKetinggian;
            p_statusPerubahan.Value = statusperubahan;
            p_multiEstCode.Value = estCode;
            p_multipulau.Value = pulau;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT4", p_function, p_IDWEEK, p_wmArea, p_StatusKetinggian, p_statusPerubahan, p_multiEstCode, p_multipulau);
        }
        public DataSet getBlockZonaForPencatatan(string wmArea, string estCode)
        {
            p_function.Value = 5;
            p_wmArea.Value = wmArea;
            p_multiEstCode.Value = estCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT4", p_function, p_wmArea, p_multiEstCode);
        }
        public DataSet getBlockZonaForPencatatanKLHK(string wmArea, string estCode)
        {
            p_function.Value = 6;
            p_wmArea.Value = wmArea;
            p_multiEstCode.Value = estCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT4", p_function, p_wmArea, p_multiEstCode);
        }
        public DataSet getEstateByWmaArea(string wmArea)
        {
            p_function.Value = 7;
            p_idWMarea.Value = wmArea;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT4", p_function, p_idWMarea);
        }
        public DataSet getMultiWMArea(string wmArea)
        {
            p_function.Value = 8;
            p_idWMarea.Value = wmArea;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT4", p_function, p_idWMarea);
        }
        public DataSet getPencatatanPerubahanPiezometerAll(string wmArea, string weekId, string estCode, string pulau)
        {
            p_function.Value = 9;
            p_IDWEEK.Value = weekId;
            p_wmArea.Value = wmArea;
            p_multiEstCode.Value = estCode;
            p_multipulau.Value = pulau;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT4", p_function, p_IDWEEK, p_wmArea, p_multiEstCode, p_multipulau);
        }
        public DataSet getBlockPerubahanTMATAll(int IdWeek, string wmArea, string statusKetinggian, string statusperubahan, string estCode, string pulau)
        {
            p_function.Value = 10;
            p_IDWEEK.Value = IdWeek;
            p_wmArea.Value = wmArea;
            p_StatusKetinggian.Value = statusKetinggian;
            p_statusPerubahan.Value = statusperubahan;
            p_multiEstCode.Value = estCode;
            p_multipulau.Value = pulau;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT4", p_function, p_IDWEEK, p_wmArea, p_StatusKetinggian, p_statusPerubahan, p_multiEstCode, p_multipulau);
        }
        public DataSet getChartTMAT(int idWeekStart, int idWeekEnd, int idWeek, string wmcode)
        {
            p_function.Value = 1;
            p_IDWEEK.Value = idWeek;
            p_IDWEEKStart.Value = idWeekStart;
            p_IDWEEKEnd.Value = idWeekEnd;
            p_WmaCode.Value = wmcode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT5", p_function, p_IDWEEK, p_IDWEEKStart, p_IDWEEKEnd, p_WmaCode);
        }
        public DataSet cekZonaBalckList(string wmcode)
        {
            p_function.Value = 2;
            p_WmaCode.Value = wmcode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT5", p_function, p_WmaCode);
        }
        public DataSet insertChartTMATPiezo(string wmcode,string estCode,string block,string PieRecordID,string check, string userId)
        {
            p_function.Value = 3;
            p_WmaCode.Value = wmcode;
            p_EstCode.Value = estCode;
            p_Block.Value = block;
            p_PieRecordID.Value = PieRecordID;
            p_check.Value = check;
            p_UserID.Value = userId;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT5", p_function, p_WmaCode, p_EstCode, p_Block, p_PieRecordID, p_check, p_UserID);
        }
        public DataSet getZonaBalckList(string wmcode)
        {
            p_function.Value = 4;
            p_WmaCode.Value = wmcode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT5", p_function, p_WmaCode);
        }
        public DataSet getPetaPerbandinganPetaGo(string pt, int weekId)
        {
            p_function.Value = 2;
            p_CompanyCode.Value = pt;
            p_IDWEEK.Value = weekId;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT6", p_function, p_CompanyCode, p_IDWEEK);

        }
        public DataSet getPetaKondisiPiezoGo(string pt, int weekId,int range, int indicatorID)
        {
            p_function.Value = 3;
            p_CompanyCode.Value = pt;
            p_IDWEEK.Value = weekId;
            p_range.Value = range;
            p_IndicatorID.Value = indicatorID;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_REPORT6", p_function, p_CompanyCode, p_IDWEEK, p_range, p_IndicatorID);
        }
        public DataSet generatePetaPerbandingan(int idWeek, string companyCode)
        {
            p_function.Value = 1;
            p_IDWEEK.Value = idWeek;
            p_CompanyCode.Value = companyCode;
            
            var ds = new DataSet();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_REPORT6", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_IDWEEK);
                command.Parameters.Add(p_CompanyCode);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                command.Parameters.Clear();
                conn.Close();
            }

            return ds;
        }

    }


    #endregion
}
