﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PZO_PiezometerOnlineMap.Mapper
{
    public class PZO_MasterPiezo
    {
        #region Parameters Declaration
        protected SqlParameter p_function = new SqlParameter("@function", SqlDbType.Int);
        protected SqlParameter p_Type = new SqlParameter("@Type", SqlDbType.VarChar, 50);
        protected SqlParameter p_estCode = new SqlParameter("@estCode", SqlDbType.VarChar, 5);
        protected SqlParameter p_IDUnitPemantauan = new SqlParameter("@IDUnitPemantauan", SqlDbType.VarChar, 10);
        protected SqlParameter p_ID = new SqlParameter("@ID", SqlDbType.Int);
        protected SqlParameter p_PieRecordID = new SqlParameter("@PieRecordID", SqlDbType.VarChar, 100);
        protected SqlParameter p_Block1 = new SqlParameter("@Block1", SqlDbType.VarChar, 50);
        protected SqlParameter p_Block2 = new SqlParameter("@Block2", SqlDbType.VarChar, 50);
        protected SqlParameter p_LastUpdate = new SqlParameter("@LastUpdate", SqlDbType.BigInt);
        protected SqlParameter p_IsActive2 = new SqlParameter("@IsActive2", SqlDbType.VarChar, 5);
        protected SqlParameter P_Remarks = new SqlParameter("@Remarks", SqlDbType.VarChar, 300);
        protected SqlParameter p_Remarks2 = new SqlParameter("@Remarks2", SqlDbType.VarChar, 200);
        protected SqlParameter p_IsDefault = new SqlParameter("@IsDefault", SqlDbType.VarChar, 5);
        protected SqlParameter p_Isactive = new SqlParameter("@IsActive", SqlDbType.VarChar, 5);
        protected SqlParameter p_Alias = new SqlParameter("@Alias", SqlDbType.VarChar, 50);
        protected SqlParameter p_UserIDEditor = new SqlParameter("@UserIDEditor", SqlDbType.BigInt);
        protected SqlParameter p_Jenis = new SqlParameter("@Jenis", SqlDbType.VarChar, 50);
        protected SqlParameter p_Date = new SqlParameter("@Date", SqlDbType.BigInt);
        protected SqlParameter p_KodePerubahan = new SqlParameter("@KodePerubahan", SqlDbType.Int);

        #endregion

        public DataSet ListHistoryDAftarPizometer(String type, String estCode)
        {
            p_function.Value = 25;
            p_Type.Value = type;
            p_estCode.Value = estCode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_Type, p_estCode);
        }
        public DataSet ListHistorySummaryPizometer(String estCode)
        {
            p_function.Value = 45;
            p_estCode.Value = estCode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_estCode);
        }
        public DataSet ListPizometerEditHistory(String estCode, String idUnitPemantauan)
        {
            p_function.Value = 47;
            p_estCode.Value = estCode;
            p_IDUnitPemantauan.Value = idUnitPemantauan;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_estCode, p_IDUnitPemantauan);
        }

        public DataSet bindPiezometerEditTMAT(String ID,String IDUnitPemantauan,String PieRecordID,String Block1,String Date,String IsActive2,String Remarks,String Remarks2,String IsDefault,String IsActive, String Alias,String UserIDEditor,String Jenis)
        {
            p_function.Value = 5;
            p_ID.Value = ID;
            p_IDUnitPemantauan.Value = IDUnitPemantauan;
            p_PieRecordID.Value = PieRecordID;
            p_Block1.Value = Block1;
            p_LastUpdate.Value = Date;
            p_IsActive2.Value = IsActive2;
            P_Remarks.Value = Remarks;
            p_Remarks2.Value = Remarks2;
            p_IsDefault.Value = IsDefault;
            p_Isactive.Value = IsActive;
            p_Alias.Value = Alias;
            p_UserIDEditor.Value = UserIDEditor;
            p_Jenis.Value = Jenis;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_IDUnitPemantauan, p_ID, p_Jenis, p_LastUpdate, p_UserIDEditor, p_Alias, p_Remarks2, p_IsActive2, p_IsDefault, p_PieRecordID, P_Remarks, p_function, p_Block1, p_Isactive, p_estCode);
        }

        public DataSet bindPiezometerInsertHistory(String ID,String PieRecordID,String Date,String KodePerubahan,String Block2,String Remarks,String EstCode,String IsActive,String UserIDEditor)
        {
            p_function.Value = 29;
            p_ID.Value = ID;
            p_PieRecordID.Value = PieRecordID;
            p_Date.Value = Date;
            p_KodePerubahan.Value = KodePerubahan;
            p_Block2.Value = Block2;

            P_Remarks.Value = Remarks;
            p_estCode.Value = EstCode;
            p_Isactive.Value = IsActive;
            p_UserIDEditor.Value = UserIDEditor;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_Date, p_UserIDEditor, p_KodePerubahan, p_Block2, p_IsActive2, p_IsDefault, p_PieRecordID, P_Remarks, p_function, p_estCode);
        }
    }
}