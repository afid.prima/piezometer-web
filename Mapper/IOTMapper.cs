﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using PZO_PiezometerOnlineMap.Mapper;


namespace PZO_PiezometerOnlineMap.Mapper
{
    public class IOTMapper
    {
        #region Parameters Declaration

        protected SqlParameter p_function = new SqlParameter("@function", SqlDbType.Int);
        protected SqlParameter p_companycode = new SqlParameter("@companycode", SqlDbType.VarChar);
        protected SqlParameter p_estcode = new SqlParameter("@estcode", SqlDbType.VarChar);
        protected SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar);
        protected SqlParameter p_deviceid2 = new SqlParameter("@deviceid2", SqlDbType.VarChar);
        protected SqlParameter p_recordtime = new SqlParameter("@recordtime", SqlDbType.VarChar);
        protected SqlParameter p_type = new SqlParameter("@type", SqlDbType.VarChar);
        protected SqlParameter p_startingdate = new SqlParameter("@startingdate", SqlDbType.VarChar);
        protected SqlParameter p_endingdate = new SqlParameter("@endingdate", SqlDbType.VarChar);
        protected SqlParameter p_stationid = new SqlParameter("@stationid", SqlDbType.VarChar);
        protected SqlParameter p_division = new SqlParameter("@division", SqlDbType.VarChar);
        protected SqlParameter p_assignname = new SqlParameter("@assignname", SqlDbType.VarChar);
        protected SqlParameter p_devicetype = new SqlParameter("@devicetype", SqlDbType.VarChar);
        protected SqlParameter p_latitude = new SqlParameter("@latitude", SqlDbType.Float);
        protected SqlParameter p_longitude = new SqlParameter("@longitude", SqlDbType.Float);
        protected SqlParameter p_isdefault = new SqlParameter("@isdefault", SqlDbType.VarChar);
        protected SqlParameter p_isactive = new SqlParameter("@isactive", SqlDbType.VarChar);
        protected SqlParameter p_createby = new SqlParameter("@createby", SqlDbType.Int);
        protected SqlParameter p_source = new SqlParameter("@source", SqlDbType.VarChar);
        protected SqlParameter p_remarks = new SqlParameter("@remarks", SqlDbType.VarChar);
        protected SqlParameter p_starthours = new SqlParameter("@startinghours", SqlDbType.VarChar);
        protected SqlParameter p_endhours = new SqlParameter("@endinghours", SqlDbType.VarChar);
        protected SqlParameter p_maintenanceid = new SqlParameter("@maintenanceid", SqlDbType.VarChar);
        protected SqlParameter p_kerusakanoperasional = new SqlParameter("@kerusakanoperasional", SqlDbType.VarChar);
        protected SqlParameter p_kerusakansituasional = new SqlParameter("@kerusakansituasional", SqlDbType.VarChar);
        protected SqlParameter p_idgraph = new SqlParameter("@idgraph", SqlDbType.Int);
        protected SqlParameter p_namagraph = new SqlParameter("@namagraph", SqlDbType.VarChar);
        protected SqlParameter p_watergateid = new SqlParameter("@watergateid", SqlDbType.VarChar);
        protected SqlParameter p_pintudalam = new SqlParameter("@pintudalam", SqlDbType.VarChar);
        protected SqlParameter p_pintuluar = new SqlParameter("@pintuluar", SqlDbType.VarChar);
        protected SqlParameter p_rainfallstation = new SqlParameter("@rainfallstation", SqlDbType.VarChar);
        protected SqlParameter p_iddetailgraph = new SqlParameter("@iddetailgraph", SqlDbType.Int);
        protected SqlParameter p_plantation = new SqlParameter("@plantation", SqlDbType.VarChar);
        protected SqlParameter p_grouping = new SqlParameter("@grouping", SqlDbType.VarChar);
        protected SqlParameter p_groupcompanyname = new SqlParameter("@groupcompanyname", SqlDbType.VarChar);
        protected SqlParameter p_arsiran = new SqlParameter("@arsiran", SqlDbType.Int);
        protected SqlParameter p_curahHujan = new SqlParameter("@curahHujan", SqlDbType.VarChar);
        protected SqlParameter p_adjval = new SqlParameter("@adjval", SqlDbType.Float);
        protected SqlParameter p_chiwindspeed = new SqlParameter("@chiwindspeed", SqlDbType.VarChar);
        protected SqlParameter p_paramint1 = new SqlParameter("@paramInt1", SqlDbType.Int);
        protected SqlParameter p_paramint2 = new SqlParameter("@paramInt2", SqlDbType.Int);
        protected SqlParameter p_param1 = new SqlParameter("@param1", SqlDbType.VarChar);
        protected SqlParameter p_param2 = new SqlParameter("@param2", SqlDbType.VarChar);
        protected SqlParameter p_param3 = new SqlParameter("@param3", SqlDbType.VarChar);
        protected SqlParameter p_param4 = new SqlParameter("@param4", SqlDbType.VarChar);
        protected SqlParameter p_param5 = new SqlParameter("@param5", SqlDbType.VarChar);
        protected SqlParameter p_paramdec1 = new SqlParameter("@paramdec1", SqlDbType.Decimal);
        protected SqlParameter p_paramdec2 = new SqlParameter("@paramdec2", SqlDbType.Decimal);
        protected SqlParameter p_paramdec3 = new SqlParameter("@paramdec3", SqlDbType.Decimal);
        protected SqlParameter p_paramdec4 = new SqlParameter("@paramdec4", SqlDbType.Decimal);



        //untuk keperluan reporting
        protected SqlParameter p_mdlCode = new SqlParameter("@mdlCode", SqlDbType.VarChar);
        protected SqlParameter p_title = new SqlParameter("@title", SqlDbType.VarChar);
        protected SqlParameter p_reportCode = new SqlParameter("@reportId", SqlDbType.VarChar);

        protected SqlParameter p_module = new SqlParameter("@module", SqlDbType.VarChar, 10);
        protected SqlParameter p_reportCode2 = new SqlParameter("@ReportCode", SqlDbType.VarChar, 10);
        protected SqlParameter p_reportName = new SqlParameter("@ReportName", SqlDbType.VarChar, 100);
        protected SqlParameter p_reportCategory = new SqlParameter("@ReportCategory", SqlDbType.VarChar, 10);
        protected SqlParameter p_remarks2 = new SqlParameter("@Remarks", SqlDbType.VarChar, -1);
        protected SqlParameter p_pageId = new SqlParameter("@pageId", SqlDbType.VarChar, 50);
        protected SqlParameter p_typeDownload = new SqlParameter("@typeDownload", SqlDbType.VarChar, 50);
        protected SqlParameter p_status = new SqlParameter("@status", SqlDbType.VarChar, 50);
        protected SqlParameter p_userId = new SqlParameter("@userId", SqlDbType.VarChar, 50);
        protected SqlParameter p_estCode = new SqlParameter("@estCode", SqlDbType.VarChar, 50);
        protected SqlParameter p_estCode2 = new SqlParameter("@estCode2", SqlDbType.VarChar, 10);
        protected SqlParameter p_thn = new SqlParameter("@thn", SqlDbType.Int);
        protected SqlParameter p_bln = new SqlParameter("@bln", SqlDbType.Int);
        protected SqlParameter p_dateParam = new SqlParameter("@dateParam", SqlDbType.VarChar, 50);

        #endregion

        //protected WLR_ConnectionString ConnectionString = new WLR_ConnectionString();

        public DataSet GetListDevice()
        {
            p_function.Value = 1;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetDailyRainfall(string companycode, string estcode, string deviceid, string recordtime)
        {
            p_function.Value = 2;
            p_companycode.Value = companycode;
            p_estcode.Value = estcode;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_companycode, p_estcode, p_deviceid, p_recordtime);
        }
        public DataSet GetSummaryDaily(string companycode, string estcode, string deviceid, string recordtime)
        {
            p_function.Value = 3;
            p_companycode.Value = companycode;
            p_estcode.Value = estcode;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_companycode, p_estcode, p_deviceid, p_recordtime);
        }
        public DataSet GetMonthlyRainfall(string companycode, string estcode, string deviceid, string recordtime)
        {
            p_function.Value = 4;
            p_companycode.Value = companycode;
            p_estcode.Value = estcode;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_companycode, p_estcode, p_deviceid, p_recordtime);
        }
        public DataSet GetDailyTMAT(string companycode, string estcode, string deviceid, string recordtime)
        {
            p_function.Value = 5;
            p_companycode.Value = companycode;
            p_estcode.Value = estcode;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_companycode, p_estcode, p_deviceid, p_recordtime);
        }
        public DataSet GetMonthlyTMAT(string companycode, string estcode, string deviceid, string startingdate, string endingdate)
        {
            p_function.Value = 6;
            p_companycode.Value = companycode;
            p_estcode.Value = estcode;
            p_deviceid.Value = deviceid;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_companycode, p_estcode, p_deviceid, p_startingdate, p_endingdate);
        }
        public DataSet GetDailyTMAS(string companycode, string estcode, string deviceid, string recordtime)
        {
            p_function.Value = 7;
            p_companycode.Value = companycode;
            p_estcode.Value = estcode;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_companycode, p_estcode, p_deviceid, p_recordtime);
        }
        public DataSet GetMonthlyTMAS(string companycode, string estcode, string deviceid, string startingdate, string endingdate, string chiwindspeed)
        {
            p_function.Value = 8;
            p_companycode.Value = companycode;
            p_estcode.Value = estcode;
            p_deviceid.Value = deviceid;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            p_chiwindspeed.Value = chiwindspeed;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_companycode, p_estcode, p_deviceid, p_startingdate, p_endingdate, p_chiwindspeed);
        }
        public DataSet MasterStation()
        {
            p_function.Value = 9;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet StatusDevice()
        {
            p_function.Value = 10;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet ARSDeviceStatus()
        {
            p_function.Value = 11;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet AWLDeviceStatus()
        {
            p_function.Value = 12;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetHoliday()
        {
            p_function.Value = 13;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetCoverage()
        {
            p_function.Value = 14;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet InsertStation(string stationid, string estcode, string division, string assignname, string devicetype, string latitude, string longitude, string isdefault, string isactive, string createby)
        {
            p_function.Value = 15;
            p_stationid.Value = stationid;
            p_estcode.Value = estcode;
            p_division.Value = division;
            p_assignname.Value = assignname;
            p_devicetype.Value = devicetype;
            p_latitude.Value = Decimal.Parse(latitude);
            p_longitude.Value = Decimal.Parse(longitude);
            p_isdefault.Value = isdefault;
            p_isactive.Value = isactive;
            p_createby.Value = Int32.Parse(createby);
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_stationid, p_estcode, p_division, p_assignname, p_devicetype, p_latitude, p_longitude, p_isdefault, p_isactive, p_createby);
        }
        public DataSet DeleteStation(string stationid)
        {
            p_function.Value = 16;
            p_stationid.Value = stationid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_stationid);
        }
        public DataSet ListDeviceMertani(string type, string estcode)
        {
            p_function.Value = 17;
            p_type.Value = type;
            p_estcode.Value = estcode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_type, p_estcode);
        }
        public DataSet ListProjectLayerMap(string estcode)
        {
            p_function.Value = 18;
            p_estcode.Value = estcode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_estcode);
        }
        public DataSet AssignSourceData(string stationid, string deviceid, string source, string startingdate, string endingdate, string createby)
        {
            p_function.Value = 19;
            p_stationid.Value = stationid;
            p_deviceid.Value = deviceid;
            p_source.Value = source;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            p_createby.Value = Int32.Parse(createby);
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_stationid, p_deviceid, p_source, p_startingdate, p_endingdate, p_createby);
        }
        public DataSet AssignedStationRainfall(string stationid, string recordtime)
        {
            p_function.Value = 20;
            p_stationid.Value = stationid;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_stationid, p_recordtime);
        }
        public DataSet AssignedStationTMATTMAS(string stationid, string devicetype, string recordtime)
        {
            p_function.Value = 21;
            p_stationid.Value = stationid;
            p_devicetype.Value = devicetype;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_stationid, p_devicetype, p_recordtime);
        }
        public DataSet DeleteAssingedStationData(string stationid, string startingdate)
        {
            p_function.Value = 22;
            p_stationid.Value = stationid;
            p_startingdate.Value = startingdate;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_stationid, p_startingdate);
        }
        public DataSet AssignedSourceDataHistory(string stationid, string estcode)
        {
            p_function.Value = 23;
            p_stationid.Value = stationid;
            p_estcode.Value = estcode;
            

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_stationid, p_estcode);
        }
        public DataSet addRemarksDevice(string devicetype, string deviceid, string remarks, string createby)
        {
            p_function.Value = 24;
            p_devicetype.Value = devicetype;
            p_deviceid.Value = deviceid;
            p_remarks.Value = remarks;
            p_createby.Value = createby;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_devicetype, p_deviceid, p_remarks, p_createby);
        }
        public DataSet deleteRemarksDevice(string deviceid)
        {
            p_function.Value = 25;
            p_deviceid.Value = deviceid;
           
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_stationid, p_deviceid);
        }
        public DataSet UnregistedMertaniDevice()
        {
            p_function.Value = 26;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetRawData(string type, string deviceid, string starthours, string endhours)
        {
            p_function.Value = 27;
            p_type.Value = type;
            p_deviceid.Value = deviceid;
            p_starthours.Value = starthours;
            p_endhours.Value = endhours;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_type, p_deviceid, p_starthours, p_endhours);
        }
        public DataSet DeleteAllMappingData(string stationid)
        {
            p_function.Value = 28;
            p_stationid.Value = stationid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_stationid);
        }
        public DataSet Get7daysreportTMATTMAS(string startingdate, string endingdate)
        {
            p_function.Value = 29;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_startingdate, p_endingdate);
        }
        public DataSet GetWeekTemp()
        {
            p_function.Value = 30;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet Get7daysreportRainfall(string startingdate, string endingdate)
        {
            p_function.Value = 31;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_startingdate, p_endingdate);
        }
        public DataSet GetManualData()
        {
            p_function.Value = 32;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetDeviceMaintenanceSummary()
        {
            p_function.Value = 33;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetDeviceMaintenanceDetail(string deviceid)
        {
            p_function.Value = 34;
            p_deviceid.Value = deviceid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_deviceid);
        }
        public DataSet addMaintenance(string maintenanceid, string deviceid, string startingdate, string endingdate, string kerusakanoperasional, string kerusakansituasional, string remarks, string createby)
        {
            p_function.Value = 35;
            p_maintenanceid.Value = maintenanceid;
            p_deviceid.Value = deviceid;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            p_kerusakanoperasional.Value = kerusakanoperasional;
            p_kerusakansituasional.Value = kerusakansituasional;
            p_remarks.Value = remarks;
            p_createby.Value = createby;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_maintenanceid, p_deviceid, p_startingdate, p_endingdate, p_kerusakanoperasional, p_kerusakansituasional, p_remarks, p_createby);
        }
        public DataSet deleteMaintenance(string maintenanceid)
        {
            p_function.Value = 36;
            p_maintenanceid.Value = maintenanceid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_maintenanceid);
        }
        public DataSet GetARSAcuraccyReport(string estcode, string startingdate, string endingdate)
        {
            p_function.Value = 37;
            p_estcode.Value = estcode;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_estcode, p_startingdate, p_endingdate);
        }
        public DataSet GetWatergate()
        {
            p_function.Value = 38;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetWatergateData(string deviceid, string startingdate, string endingdate, string estcode, string division)
        {
            p_function.Value = 39;
            p_deviceid.Value = deviceid;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            p_estcode.Value = estcode;
            p_division.Value = division;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_deviceid, p_startingdate, p_endingdate, p_estcode, p_division);
        }
        public DataSet GetGraph()
        {
            p_function.Value = 40;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet InsertGraph(string idgraph, string namagraph, string companycode, string createby)
        {
            p_function.Value = 41;
            p_idgraph.Value = idgraph;
            p_namagraph.Value = namagraph;
            p_companycode.Value = companycode;
            p_createby.Value = createby;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_idgraph, p_namagraph, p_companycode, p_createby);
        }
        public DataSet DeleteGraph(string idgraph)
        {
            p_function.Value = 42;
            p_idgraph.Value = idgraph;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_idgraph);
        }
        public DataSet InsertDetailGraph(string iddetailgraph, string idgraph, string deviceid)
        {
            p_function.Value = 43;
            p_iddetailgraph.Value = iddetailgraph;
            p_idgraph.Value = idgraph;
            p_deviceid.Value = deviceid;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_iddetailgraph, p_idgraph, p_deviceid);
        }
        public DataSet DeleteGraphDetail(string idgraph)
        {
            p_function.Value = 44;
            p_idgraph.Value = idgraph;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_idgraph);
        }
        public DataSet GetMonthlyMonitoringArs(string plantation, string grouping, string groupcompanyname, string companycode, string estcode, string recordtime)
        {
            p_function.Value = 45;
            p_plantation.Value = plantation;
            p_grouping.Value = grouping;
            p_groupcompanyname.Value = groupcompanyname;
            p_companycode.Value = companycode;
            p_estcode.Value = estcode;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_plantation, p_grouping, p_groupcompanyname, p_companycode, p_estcode, p_recordtime);
        }
        public DataSet GetMonthlyMonitoringAwl(string grouping, string estcode, string recordtime, string type)
        {
            p_function.Value = 46;
            p_grouping.Value = grouping;
            p_estcode.Value = estcode;
            p_recordtime.Value = recordtime;
            p_type.Value = type;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_grouping, p_estcode, p_recordtime, p_type);
        }
        public DataSet GetFilterRegion()
        {
            p_function.Value = 47;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }

        public DataSet GetTitleReport(string mdlCode, string title, string ReportCode)
        {
            p_function.Value = 1;
            p_mdlCode.Value = mdlCode;
            p_title.Value = title;
            p_reportCode.Value = ReportCode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "SP_ALL_Report", p_function, p_reportCode, p_mdlCode, p_title);
        }

        //public DataSet GetTitleReportByreportId(string mdlCode, string reportId)
        //{
        //    p_function.Value = 1;
        //    p_mdlCode.Value = mdlCode;
        //    p_reportCode.Value = reportId;
        //    return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "SP_ALL_Report", p_function, p_mdlCode, p_reportCode);
        //}

        public DataSet IOT_TMATHarian(string estCode, string deviceid, string dateParam)
        {

            //SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            //SqlParameter p_recordtime = new SqlParameter("@recordtime", SqlDbType.VarChar, -1);
            //SqlParameter p_text_param5 = new SqlParameter("@text_param5", SqlDbType.VarChar, -1);
            p_function.Value = 1001;
            p_estcode.Value = estCode;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = dateParam;
            //p_estCode.Value = estCode;
            //p_text_param6.Value = thn;
            //p_text_param7.Value = bln;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_Report", p_function, p_estcode, p_deviceid, p_recordtime);

            //var dt = new DataSet();
            //using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            //using (
            //    var command = new SqlCommand("IOT_Report", conn) { CommandType = CommandType.StoredProcedure }
            //)
            //{
            //    conn.Open();
            //    command.Parameters.Add(p_function);
            //    command.Parameters.Add(p_deviceid);
            //    command.Parameters.Add(p_recordtime);
            //    command.CommandTimeout = 0;
            //    var adapter = new SqlDataAdapter(command);
            //    adapter.Fill(dt);
            //    command.Parameters.Clear();
            //    conn.Close();
            //}

            //return dt;

        }

        public DataSet IOT_TMASHarian(string estCode, string deviceid, string dateParam)
        {

            //SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            //SqlParameter p_recordtime = new SqlParameter("@recordtime", SqlDbType.VarChar, -1);
            //SqlParameter p_text_param5 = new SqlParameter("@text_param5", SqlDbType.VarChar, -1);
            p_function.Value = 1002;
            p_estcode.Value = estCode;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = dateParam;
            //p_estCode.Value = estCode;
            //p_text_param6.Value = thn;
            //p_text_param7.Value = bln;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_Report", p_function, p_estcode, p_deviceid, p_recordtime);

            //var dt = new DataSet();
            //using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            //using (
            //    var command = new SqlCommand("IOT_Report", conn) { CommandType = CommandType.StoredProcedure }
            //)
            //{
            //    conn.Open();
            //    command.Parameters.Add(p_function);
            //    command.Parameters.Add(p_deviceid);
            //    command.Parameters.Add(p_recordtime);
            //    command.CommandTimeout = 0;
            //    var adapter = new SqlDataAdapter(command);
            //    adapter.Fill(dt);
            //    command.Parameters.Clear();
            //    conn.Close();
            //}

            //return dt;

        }

        public DataSet IOT_CHHarian(string estCode, string deviceid, string dateParam)
        {

            //SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            //SqlParameter p_recordtime = new SqlParameter("@recordtime", SqlDbType.VarChar, -1);
            //SqlParameter p_text_param5 = new SqlParameter("@text_param5", SqlDbType.VarChar, -1);
            p_function.Value = 1006;
            p_estcode.Value = estCode;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = dateParam;
            //p_estCode.Value = estCode;
            //p_text_param6.Value = thn;
            //p_text_param7.Value = bln;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_Report", p_function, p_estcode, p_deviceid, p_recordtime);

            //var dt = new DataSet();
            //using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            //using (
            //    var command = new SqlCommand("IOT_Report", conn) { CommandType = CommandType.StoredProcedure }
            //)
            //{
            //    conn.Open();
            //    command.Parameters.Add(p_function);
            //    command.Parameters.Add(p_deviceid);
            //    command.Parameters.Add(p_recordtime);
            //    command.CommandTimeout = 0;
            //    var adapter = new SqlDataAdapter(command);
            //    adapter.Fill(dt);
            //    command.Parameters.Clear();
            //    conn.Close();
            //}

            //return dt;

        }

        public DataSet IOT_TMATBulanan(int function,string region, string dateParam, string thisType)
        {

            //SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            //SqlParameter p_recordtime = new SqlParameter("@recordtime", SqlDbType.VarChar, -1);
            //SqlParameter p_text_param5 = new SqlParameter("@text_param5", SqlDbType.VarChar, -1);
            p_function.Value = function;
            p_grouping.Value = region;
            p_recordtime.Value = dateParam;
            p_type.Value = thisType;
            //p_estCode.Value = estCode;
            //p_text_param6.Value = thn;
            //p_text_param7.Value = bln;
            //return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_Report", p_function, p_grouping, p_recordtime, p_type);

            var dt = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("IOT_Report", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_grouping);
                command.Parameters.Add(p_recordtime);
                command.Parameters.Add(p_type);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }

        public DataSet IOT_CHBulanan(int function, string region, string dateParam, string thisType)
        {

            //SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            //SqlParameter p_recordtime = new SqlParameter("@recordtime", SqlDbType.VarChar, -1);
            //SqlParameter p_text_param5 = new SqlParameter("@text_param5", SqlDbType.VarChar, -1);
            p_function.Value = function;
            p_grouping.Value = region;
            p_recordtime.Value = dateParam;
            p_type.Value = thisType;
            //p_estCode.Value = estCode;
            //p_text_param6.Value = thn;
            //p_text_param7.Value = bln;
            //return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_Report", p_function, p_grouping, p_recordtime, p_type);

            var dt = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("IOT_Report", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_grouping);
                command.Parameters.Add(p_recordtime);
                command.Parameters.Add(p_type);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }

        public DataSet IOT_MonitoringPerKebun(int function, string estCode, string dateParam, string thisType)
        {

            //SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            //SqlParameter p_recordtime = new SqlParameter("@recordtime", SqlDbType.VarChar, -1);
            //SqlParameter p_text_param5 = new SqlParameter("@text_param5", SqlDbType.VarChar, -1);
            p_function.Value = function;
            p_estcode.Value = estCode;
            p_recordtime.Value = dateParam;
            //p_type.Value = thisType;
            //return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_Report", p_function, p_grouping, p_recordtime, p_type);

            var dt = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("IOT_Report", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_estcode);
                command.Parameters.Add(p_recordtime);
                //command.Parameters.Add(p_type);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }

        public DataSet IOT_PemantauanWatergate(string estcode, string curahHujan, string deviceid, string deviceid2, string startingdate, string endingdate, int arsiran)
        {

            //SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            //SqlParameter p_recordtime = new SqlParameter("@recordtime", SqlDbType.VarChar, -1);
            //SqlParameter p_text_param5 = new SqlParameter("@text_param5", SqlDbType.VarChar, -1);
            p_function.Value = 1005;
            p_estcode.Value = estcode;
            p_curahHujan.Value = curahHujan;
            p_deviceid.Value = deviceid;
            p_deviceid2.Value = deviceid2;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            p_arsiran.Value = arsiran;
            //    declare @deviceid varchar(200) = 'MTI-7J9JLKNFGIBI,MTI-08OQQB58H3NN'

            //declare @estcode varchar(20) = 'SIP1'

            //declare @division varchar(10) = 'AFD-3'

            //Declare @startingdate date = '2022-08-01'

            //Declare @endingdate date = '2022-08-07'

            //p_estCode.Value = estCode;
            //p_text_param6.Value = thn;
            //p_text_param7.Value = bln;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_Report", p_function, p_estcode, p_curahHujan, p_deviceid, p_deviceid2, p_startingdate, p_endingdate, p_arsiran);

            //var dt = new DataSet();
            //using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            //using (
            //    var command = new SqlCommand("IOT_Report", conn) { CommandType = CommandType.StoredProcedure }
            //)
            //{
            //    conn.Open();
            //    command.Parameters.Add(p_function);
            //    command.Parameters.Add(p_deviceid);
            //    command.Parameters.Add(p_recordtime);
            //    command.CommandTimeout = 0;
            //    var adapter = new SqlDataAdapter(command);
            //    adapter.Fill(dt);
            //    command.Parameters.Clear();
            //    conn.Close();
            //}

            //return dt;

        }


        public DataSet GetDetailSavedGraph(string estcode, string idSavedGraph)
        {
            p_function.Value = 2001;
            p_estcode.Value = estcode;
            p_idgraph.Value = idSavedGraph;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_Report", p_function, p_estcode, p_idgraph);
        }

        public DataSet InsertAdjustedDataWatergate(string deviceid, string recordtime, string adjval)
        {
            p_function.Value = 49;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = recordtime;
            p_adjval.Value = adjval;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_deviceid, p_recordtime, p_adjval);
        }
        public DataSet GetAdjustedDataWatergate(string companycode)
        {
            p_function.Value = 50;
            p_companycode.Value = companycode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_companycode);
        }
        public DataSet GetLanguage()
        {
            p_function.Value = 29;
            p_mdlCode.Value = "IOT";
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "API_WEB4", p_function, p_mdlCode);
        }
        public DataSet GetWaterDepthIndicator()
        {
            p_function.Value = 52;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }

        public DataSet GetAWSSource()
        {
            p_function.Value = 53;            
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetRainfallAWS(string deviceid)
        {
            p_function.Value = 54;
            p_deviceid.Value = deviceid;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_deviceid);
        }
        public DataSet GetCHIWindspeed(string deviceid, string recordtime, string chiwindspeed)
        {
            p_function.Value = 55;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = recordtime;
            p_chiwindspeed.Value = chiwindspeed;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_deviceid, p_recordtime, p_chiwindspeed);
        }
        public DataSet GetCHIPeriod(string deviceid, string startingdate, string endingdate, string chiwindspeed)
        {
            p_function.Value = 56;
            p_deviceid.Value = deviceid;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            p_chiwindspeed.Value = chiwindspeed;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_deviceid, p_startingdate, p_endingdate, p_chiwindspeed);
        }
        public void HMS_InsertLogReport(string pageId, string module, string ReportCode, string ReportName, string ReportCategory, string Remarks, string estCode, string UserID, string dateParam, string typeDownload, string status)
        {
            p_function.Value = 5;
            p_module.Value = module;
            p_reportCode2.Value = ReportCode;
            p_reportName.Value = ReportName;
            p_reportCategory.Value = ReportCategory;
            p_remarks2.Value = Remarks;
            p_estCode2.Value = estCode;
            p_userId.Value = UserID;
            p_dateParam.Value = dateParam;
            p_pageId.Value = pageId;
            p_typeDownload.Value = typeDownload;
            p_status.Value = status;
            SqlHelper.ExecuteNonQuery(WLR_ConnectionString.connStringHMSAPP, CommandType.StoredProcedure, "SP_HMS_Report6", p_function, p_module,
                p_userId, p_reportCode2, p_reportName, p_reportCategory, p_remarks2, p_estCode2, p_dateParam, p_pageId, p_typeDownload, p_status);

        }
        public DataSet IOT_ReportDashboardAWS(int thisFunction, string param1)
        {
            SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            SqlParameter p_recordtime = new SqlParameter("@recordtime", SqlDbType.VarChar, -1);
            //SqlParameter p_text_param5 = new SqlParameter("@text_param5", SqlDbType.VarChar, -1);
            p_function.Value = thisFunction;
            p_deviceid.Value = param1;

            //return SqlHelper.ExecuteDataset(HMS_WLR_ConnectionString.conStrGISAPP, CommandType.StoredProcedure, "API_WEB4", p_function, p_mdlCode);

            var dt = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("IOT", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_deviceid);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }

        public DataSet IOT_PerhitunganKeseimbanganAirPerBulan(int thisFunction, string param1, string param2, string param3, string param4, string param5, string param6)
        {

            SqlParameter p_plantation = new SqlParameter("@plantation", SqlDbType.VarChar, -1);
            SqlParameter p_grouping = new SqlParameter("@grouping", SqlDbType.VarChar, -1);
            SqlParameter p_groupcompanyname = new SqlParameter("@groupcompanyname", SqlDbType.VarChar, -1);
            SqlParameter p_companycode = new SqlParameter("@companycode", SqlDbType.VarChar, -1);
            SqlParameter p_estcode = new SqlParameter("@estcode", SqlDbType.VarChar, -1);
            SqlParameter p_recordtime = new SqlParameter("@recordtime", SqlDbType.VarChar, -1);
            SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            p_function.Value = thisFunction;
            p_plantation.Value = param1;
            p_grouping.Value = param2;
            p_groupcompanyname.Value = param3;
            p_companycode.Value = param4;
            p_estcode.Value = param5;
            p_recordtime.Value = param6;
            //p_deviceid.Value = param1;

            //return SqlHelper.ExecuteDataset(HMS_WLR_ConnectionString.conStrGISAPP, CommandType.StoredProcedure, "API_WEB4", p_function, p_mdlCode);

            var dt = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("IOT", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_plantation);
                command.Parameters.Add(p_grouping);
                command.Parameters.Add(p_groupcompanyname);
                command.Parameters.Add(p_companycode);
                command.Parameters.Add(p_estcode);
                command.Parameters.Add(p_recordtime);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }

        public DataSet IOT_PerhitunganKeseimbanganAirPerDevice(int thisFunction, string param1, string param2)
        {

            SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            SqlParameter p_recordtime = new SqlParameter("@recordtime", SqlDbType.VarChar, -1);
            p_function.Value = thisFunction;
            p_deviceid.Value = param1;
            p_recordtime.Value = param2;
            //p_deviceid.Value = param1;

            //return SqlHelper.ExecuteDataset(HMS_WLR_ConnectionString.conStrGISAPP, CommandType.StoredProcedure, "API_WEB4", p_function, p_mdlCode);

            var dt = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("IOT", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_deviceid);
                command.Parameters.Add(p_recordtime);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }



        public DataSet GetDefisitAirTahunan(string deviceid, string recordtime)
        {
            p_function.Value = 59;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_deviceid, p_recordtime);
        }
        public DataSet GetDefisitAirBulanan(string plantation, string grouping, string groupcompanyname, string companycode, string estcode, string deviceid, string recordtime)
        {
            p_function.Value = 60;
            p_plantation.Value = plantation;
            p_grouping.Value = grouping;
            p_groupcompanyname.Value = groupcompanyname;
            p_companycode.Value = companycode;
            p_estcode.Value = estcode;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_plantation, p_grouping, p_groupcompanyname, p_companycode, p_estcode, p_deviceid, p_recordtime);
        }
        public DataSet GetStatusServer()
        {
            p_function.Value = 108;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringHMSAPP, CommandType.StoredProcedure, "SP_MCS_Web_22", p_function);
        }
        public DataSet GetArsStation4Weeks(string companycode, string endingdate, string arsiran)
        {
            p_function.Value = 61;
            p_companycode.Value = companycode;
            p_endingdate.Value = endingdate;
            p_arsiran.Value = arsiran;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_companycode,p_endingdate,p_arsiran);
        }
        public DataSet IOT_LaporanWMMingguanbyPT(int thisFunction, string param1, string param2, string param3, string param4)
        {

            SqlParameter p_endingdate = new SqlParameter("@endingdate", SqlDbType.VarChar, -1);
            SqlParameter p_arsiran = new SqlParameter("@arsiran", SqlDbType.VarChar, -1);
            SqlParameter p_param4 = new SqlParameter("@param4", SqlDbType.VarChar, -1);
            p_function.Value = thisFunction;
            //p_deviceid.Value = param1;
            //p_recordtime.Value = param2;
            ////p_deviceid.Value = param1;
            p_companycode.Value = param1;
            p_endingdate.Value = param2;
            p_arsiran.Value = param3;
            p_param4.Value = param4;

            //return SqlHelper.ExecuteDataset(HMS_WLR_ConnectionString.conStrGISAPP, CommandType.StoredProcedure, "API_WEB4", p_function, p_mdlCode);

            var dt = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("IOT", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_companycode);
                command.Parameters.Add(p_endingdate);
                command.Parameters.Add(p_arsiran);
                command.Parameters.Add(p_param4);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }
        public DataSet GetStationList()
        {
            p_function.Value = 63;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetAnnualRainfallPattern(string estcode,string recordtime)
        {
            p_function.Value = 64;
            p_estcode.Value = estcode;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_estcode, p_recordtime);
        }
        public DataSet IOT_LaporanBulananCHperJam(int thisFunction, string thn, string bln, string estcode, string deviceid)
        {

            SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            p_function.Value = thisFunction;
            p_thn.Value = thn;
            p_bln.Value = bln;
            p_estCode.Value = estcode;
            p_deviceid.Value = deviceid;

            //exec gis_app..[IOT_Report] @function = 2010, @thn = 2023, @bln = 4, @estCode = 'CEN',@deviceid = 'MTI-7U1CE7K12ZVM'

            //return SqlHelper.ExecuteDataset(HMS_WLR_ConnectionString.conStrGISAPP, CommandType.StoredProcedure, "API_WEB4", p_function, p_mdlCode);

            var dt = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("IOT_Report", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_thn);
                command.Parameters.Add(p_bln);
                command.Parameters.Add(p_estCode);
                command.Parameters.Add(p_deviceid);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }
        public DataSet GetHourlyArsData(string estcode, string deviceid, string paramint1, string paramint2)
        {
            p_function.Value = 65;
            p_estcode.Value = estcode;
            p_deviceid.Value = deviceid;
            p_paramint1.Value = Int32.Parse(paramint1);
            p_paramint2.Value = Int32.Parse(paramint2);
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_estcode, p_deviceid, p_paramint1, p_paramint2);
        }
        public DataSet GetAnnualRainfall(string companycode, string recordtime)
        {
            p_function.Value = 66;
            p_companycode.Value = companycode;
            p_recordtime.Value = recordtime;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_companycode, p_recordtime);
        }

        public DataSet IOT_CHTahunanbyPT(int thisFunction, string param1, string param2)
        {

            SqlParameter p_param1 = new SqlParameter("@recordtime", SqlDbType.VarChar, -1);
            SqlParameter p_param2 = new SqlParameter("@companycode", SqlDbType.VarChar, -1);
            p_function.Value = thisFunction;
            p_param1.Value = param1;
            p_param2.Value = param2;
            var dt = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("IOT_Report", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_param1);
                command.Parameters.Add(p_param2);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }
        public DataSet GetComparisonCHIOTnSAP(string deviceid, string param1, string param2)
        {
            p_function.Value = 67;
            p_deviceid.Value = deviceid;
            p_param1.Value = param1;
            p_param2.Value = param2;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_deviceid, p_param1, p_param2);
        }

        public DataSet IOT_LaporanPerbandinganARSvsSAP(int thisFunction, string thn, string bln, string deviceid)
        {

            SqlParameter p_deviceid = new SqlParameter("@deviceid", SqlDbType.VarChar, -1);
            SqlParameter p_thn = new SqlParameter("@thn", SqlDbType.VarChar, -1);
            SqlParameter p_bln = new SqlParameter("@bln", SqlDbType.VarChar, -1);
            p_function.Value = thisFunction;
            p_thn.Value = thn;
            p_bln.Value = bln;
            p_deviceid.Value = deviceid;

            //exec gis_app..[IOT_Report] @function = 2010, @thn = 2023, @bln = 4, @estCode = 'CEN',@deviceid = 'MTI-7U1CE7K12ZVM'

            //return SqlHelper.ExecuteDataset(HMS_WLR_ConnectionString.conStrGISAPP, CommandType.StoredProcedure, "API_WEB4", p_function, p_mdlCode);

            var dt = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("IOT", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_thn);
                command.Parameters.Add(p_bln);
                command.Parameters.Add(p_deviceid);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }
        public DataSet GetListAWLKLHK()
        {
            p_function.Value = 68;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet InsertSavedReportKLHKSummary(string param1, string param2, string createby)
        {
            p_function.Value = 69;
            p_param1.Value = param1;
            p_param2.Value = param2;
            p_createby.Value = createby;



            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_param1, p_param2, p_createby);
        }
        public DataSet InsertSavedReportKLHKDetail( string paramInt1, string paramInt2, string param3,string param4, string param5)
        {
            p_function.Value = 70;
            p_paramint1.Value = paramInt1;
            p_paramint2.Value = paramInt2;
            p_param3.Value = param3;
            p_param4.Value = param4;
            p_param5.Value = param5;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_paramint1, p_paramint2,p_param3,p_param4,p_param5);
        }
        public DataSet GetSavedKLHKData()
        {
            p_function.Value = 71;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetDataKLHKAWL(string paramInt1, string thn, string bln)
        {
            p_function.Value = 72;
            p_paramint1.Value = paramInt1;
            p_thn.Value = thn;
            p_bln.Value = bln;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_paramint1,p_thn,p_bln);
        }
        public DataSet DeleteSavedKLHKData(string paramInt1)
        {
            p_function.Value = 73;
            p_paramint1.Value = paramInt1;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_paramint1);
        }
        public DataSet GetARSSourceScheduler()
        {
            p_function.Value = 75;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetAWLSourceScheduler()
        {
            p_function.Value = 76;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetAWLHolykellSource()
        {
            p_function.Value = 77;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetAWLHolykellSourceScheduler()
        {
            p_function.Value = 78;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetHolykellCalibration(string deviceid)
        {
            p_function.Value = 79;
            p_deviceid.Value = deviceid;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_deviceid);
        }
        public DataSet GetCalibrationSetInput()
        {
            p_function.Value = 80;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet InsertHolykellCalibration(string calibrationid, string calibrationdate, string deviceid, string paramdec1, string paramdec2, string paramdec3, string paramdec4, string type, string createby, string superviseby, string remarks, string urlfoto)
        {
            p_function.Value = 81;
            p_param1.Value = calibrationid;
            p_recordtime.Value = calibrationdate;
            p_deviceid.Value = deviceid;
            p_paramdec1.Value = paramdec1;
            p_paramdec2.Value = paramdec2;
            p_paramdec3.Value = paramdec3;
            p_paramdec4.Value = paramdec4;
            p_paramint1.Value = type;
            p_createby.Value = createby;
            p_param3.Value = superviseby;
            p_remarks.Value = remarks;
            p_param2.Value = urlfoto;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function, p_param1, p_recordtime,p_deviceid,p_paramdec1, p_paramdec2, p_paramdec3, p_paramdec4,p_paramint1,p_createby, p_param3,p_remarks,p_param2);
        }
        public DataSet GetDeviceHolykell()
        {
            p_function.Value = 86;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT", p_function);
        }
        public DataSet GetDailyTMATHolykell(string deviceid, string recordtime)
        {
            p_function.Value = 82;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = recordtime;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_2", p_function, p_deviceid, p_recordtime);
        }
        public DataSet GetMonthlyTMATHolykell(string deviceid, string startingdate, string endingdate, string companycode, string rainfall)
        {
            p_function.Value = 83;
            p_deviceid.Value = deviceid;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            p_companycode.Value = companycode;
            p_chiwindspeed.Value = rainfall;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_2", p_function, p_deviceid, p_startingdate, p_endingdate, p_companycode, p_chiwindspeed);
        }
        public DataSet GetDailyTMASHolykell(string deviceid, string recordtime)
        {
            p_function.Value = 84;
            p_deviceid.Value = deviceid;
            p_recordtime.Value = recordtime;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_2", p_function, p_deviceid, p_recordtime);
        }
        public DataSet GetMonthlyTMASHolykell(string deviceid, string startingdate, string endingdate, string companycode, string rainfall)
        {
            p_function.Value = 85;
            p_deviceid.Value = deviceid;
            p_startingdate.Value = startingdate;
            p_endingdate.Value = endingdate;
            p_companycode.Value = companycode;
            p_chiwindspeed.Value = rainfall;


            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IOT_2", p_function, p_deviceid, p_startingdate, p_endingdate, p_companycode, p_chiwindspeed);
        }
    }
}