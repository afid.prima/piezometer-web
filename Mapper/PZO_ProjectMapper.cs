﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using PZO_PiezometerOnlineMap.Domain;

namespace PZO_PiezometerOnlineMap.Mapper
{
    public class PZO_ProjectMapper
    {
        #region Parameters Declaration

        protected SqlParameter p_function = new SqlParameter("@function", SqlDbType.Int);
        protected SqlParameter p_CompanyCode = new SqlParameter("@CompanyCode", SqlDbType.VarChar, 8);
        protected SqlParameter p_CompanyCode_KLHK = new SqlParameter("@CompanyCode_KLHK", SqlDbType.VarChar, 50);
        protected SqlParameter p_EstCode = new SqlParameter("@EstCode", SqlDbType.VarChar, -1);
        protected SqlParameter p_Division = new SqlParameter("@Division", SqlDbType.VarChar, 10);
        protected SqlParameter p_Block = new SqlParameter("@Block", SqlDbType.VarChar, 10);
        protected SqlParameter p_ProjectTypeName = new SqlParameter("@ProjectTypeName", SqlDbType.VarChar, 50);
        protected SqlParameter p_UserID = new SqlParameter("@userid", SqlDbType.BigInt);
        protected SqlParameter p_Date = new SqlParameter("@Date", SqlDbType.DateTime);
        protected SqlParameter p_PieRecordID = new SqlParameter("@PieRecordID", SqlDbType.VarChar, 10);
        protected SqlParameter p_ID = new SqlParameter("@ID", SqlDbType.Int);
        protected SqlParameter p_WeekID = new SqlParameter("@WeekID", SqlDbType.Int);
        protected SqlParameter p_Month = new SqlParameter("@Month", SqlDbType.Int);
        protected SqlParameter p_Year = new SqlParameter("@Year", SqlDbType.Int);
        protected SqlParameter p_WeekParam = new SqlParameter("@WeekParam", SqlDbType.VarChar, 10);
        protected SqlParameter p_StartDate = new SqlParameter("@StartDate", SqlDbType.DateTime);
        protected SqlParameter p_StartingDate = new SqlParameter("@StartingDate", SqlDbType.DateTime);
        protected SqlParameter p_EndDate = new SqlParameter("@EndDate", SqlDbType.DateTime);
        protected SqlParameter p_TanggalSurvey = new SqlParameter("@TanggalSurvey", SqlDbType.DateTime);
        protected SqlParameter p_mdlCode = new SqlParameter("@mdlCode", SqlDbType.VarChar);

        protected SqlParameter p_LayerDisplayName = new SqlParameter("@LayerDisplayName", SqlDbType.VarChar, 100);
        protected SqlParameter p_LayerURL = new SqlParameter("@LayerURL", SqlDbType.VarChar, 250);
        protected SqlParameter p_LayerURLName = new SqlParameter("@LayerURLName", SqlDbType.VarChar, 100);
        protected SqlParameter p_LayerCategory = new SqlParameter("@LayerCategory", SqlDbType.VarChar, 10);
        protected SqlParameter p_LayerType = new SqlParameter("@LayerType", SqlDbType.Int);
        protected SqlParameter p_LayerDisplay = new SqlParameter("@LayerDisplay", SqlDbType.VarChar, 5);
        protected SqlParameter p_FieldID = new SqlParameter("@FieldID", SqlDbType.Int);

        protected SqlParameter p_Week = new SqlParameter("@Week", SqlDbType.Int);
        protected SqlParameter p_WeekName = new SqlParameter("@WeekName", SqlDbType.VarChar, 50);
        protected SqlParameter p_MonthName = new SqlParameter("@MonthName", SqlDbType.VarChar, 50);
        protected SqlParameter p_AdjustmentValue = new SqlParameter("@AdjustmentValue", SqlDbType.Int);

        protected SqlParameter p_Keterangan = new SqlParameter("@Keterangan", SqlDbType.VarChar, 100);
        protected SqlParameter p_idWMArea = new SqlParameter("@idWmarea", SqlDbType.Int);
        protected SqlParameter p_wmaCode = new SqlParameter("@WmaCode", SqlDbType.VarChar, 50);
        protected SqlParameter p_uname = new SqlParameter("@UserName", SqlDbType.VarChar, 50);
        protected SqlParameter p_path = new SqlParameter("@Path", SqlDbType.VarChar, -1);
        protected SqlParameter p_fileName = new SqlParameter("@fileName", SqlDbType.VarChar, 100);
        protected SqlParameter p_UploadDate = new SqlParameter("@UploadDate", SqlDbType.VarChar, 20);
        protected SqlParameter p_Referensi = new SqlParameter("@Referensi", SqlDbType.VarChar, 20);
        protected SqlParameter p_periode = new SqlParameter("@Periode", SqlDbType.VarChar, 20);
        protected SqlParameter p_WeekTemp = new SqlParameter("@WeekTemp", SqlDbType.Int);
        protected SqlParameter p_Ketinggian = new SqlParameter("@Ketinggian", SqlDbType.Int);
        protected SqlParameter p_HasilGenerate = new SqlParameter("@HasilGenerate", SqlDbType.Int);
        protected SqlParameter p_KetinggianLogger = new SqlParameter("@KetinggianLogger", SqlDbType.VarChar, 5);

        protected SqlParameter p_Afd = new SqlParameter("@Afd", SqlDbType.VarChar, 10);
        protected SqlParameter p_KodeTMAT = new SqlParameter("@TMAT", SqlDbType.VarChar, 10);
        protected SqlParameter p_Merk = new SqlParameter("@Merk", SqlDbType.VarChar, 50);
        protected SqlParameter p_Type = new SqlParameter("@Type", SqlDbType.VarChar, 50);
        protected SqlParameter p_SN = new SqlParameter("@SerialNumber", SqlDbType.VarChar, 50);
        protected SqlParameter p_Kondisi = new SqlParameter("@Kondisi", SqlDbType.VarChar, 50);
        protected SqlParameter p_Baterai = new SqlParameter("@Baterai", SqlDbType.VarChar, 5);
        protected SqlParameter p_LastCheck = new SqlParameter("@LastCheck", SqlDbType.VarChar, 20);
        protected SqlParameter p_Remark = new SqlParameter("@Remark", SqlDbType.VarChar, -1);

        //update dicki 
        protected SqlParameter p_estCode = new SqlParameter("@estCode", SqlDbType.VarChar);
        protected SqlParameter p_zona = new SqlParameter("@zona", SqlDbType.VarChar, 255); 
        protected SqlParameter p_multipleIdWmArea = new SqlParameter("@multipleIdWmArea", SqlDbType.VarChar);
        protected SqlParameter p_graphName = new SqlParameter("@graphName", SqlDbType.VarChar);
        protected SqlParameter p_wmArea = new SqlParameter("@wmArea", SqlDbType.Int);
        protected SqlParameter p_zonaId = new SqlParameter("@zonaId", SqlDbType.VarChar);
        protected SqlParameter p_createBy = new SqlParameter("@createBy", SqlDbType.Int);
        protected SqlParameter p_updateBy = new SqlParameter("@updateBy", SqlDbType.Int);
        protected SqlParameter p_idGraph = new SqlParameter("@idGraph", SqlDbType.Int);
        protected SqlParameter p_curahHujan = new SqlParameter("@curahHujan", SqlDbType.VarChar);
        protected SqlParameter p_pieRecordID = new SqlParameter("@pieRecordID", SqlDbType.VarChar);
        protected SqlParameter p_startDate = new SqlParameter("@startDate", SqlDbType.VarChar);
        protected SqlParameter p_endDate = new SqlParameter("@endDate", SqlDbType.VarChar); 
        protected SqlParameter p_stationName = new SqlParameter("@stationName", SqlDbType.VarChar);
        protected SqlParameter p_colorField = new SqlParameter("@colorField", SqlDbType.VarChar);
        protected SqlParameter p_lineThickness = new SqlParameter("@lineThickness ", SqlDbType.VarChar); 


        protected SqlParameter p_multiplepieRecordID = new SqlParameter("@multiplepieRecordID", SqlDbType.VarChar);
        protected SqlParameter p_multipleEstCode = new SqlParameter("@multipleEstCode", SqlDbType.VarChar);
        protected SqlParameter p_multipleZona = new SqlParameter("@multipleZona", SqlDbType.VarChar);

        #endregion

        #region Method

        public DataTable ListLayer(string EstCode)
        {
            p_function.Value = 1;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode).Tables[0];
        }

        public DataSet GetEstateDetail(string EstCode)
        {
            p_function.Value = 2;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode);
        }

        public DataTable ListBlockByEstate(string EstCode)
        {
            p_function.Value = 3;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode).Tables[0];
        }

        public DataTable GetBlockExtent(string EstCode, string Block)
        {
            p_function.Value = 4;
            p_EstCode.Value = EstCode;
            p_Block.Value = Block;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode, p_Block).Tables[0];
        }

        public DataTable GetLayerField(int FieldID)
        {
            p_function.Value = 5;
            p_FieldID.Value = FieldID;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_FieldID).Tables[0];
        }

        public DataTable GetPiezoRecordDetailByPieRecordID(string PieRecordID)
        {
            p_function.Value = 6;
            p_PieRecordID.Value = PieRecordID;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_PieRecordID).Tables[0];

            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_OnlineMap", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_PieRecordID);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;
        }

        public DataTable GetWeekFromCurrentDate()
        {
            p_function.Value = 7;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }

        public DataTable GetWeekFromID(int ID)
        {
            p_function.Value = 8;
            p_ID.Value = ID;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_ID).Tables[0];
        }

        public string GetZone(string EstCode)
        {
            p_function.Value = 9;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode).Tables[0].Rows[0]["ZONE"].ToString();
        }

        public DataTable GetListWeek()
        {
            p_function.Value = 10;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }

        public DataTable GetListPiezometer(string EstCode, string WeekID)
        {
            p_function.Value = 11;
            p_EstCode.Value = EstCode;
            p_WeekID.Value = WeekID;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode, p_WeekID).Tables[0];
        }

        public DataTable GetWeekFromParameter(string WeekParam)
        {
            p_function.Value = 12;
            p_WeekParam.Value = WeekParam;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_WeekParam).Tables[0];
        }

        public DataTable GetPreviousWeekFromParameter(int Week, int Month, int Year)
        {
            p_function.Value = 13;
            p_WeekID.Value = Week;
            p_Month.Value = Month;
            p_Year.Value = Year;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_WeekID, p_Month, p_Year).Tables[0];
        }

        public DataTable GetNextWeekFromParameter(int Week, int Month, int Year)
        {
            p_function.Value = 14;
            p_WeekID.Value = Week;
            p_Month.Value = Month;
            p_Year.Value = Year;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_WeekID, p_Month, p_Year).Tables[0];
        }

        public string GetMappedEstate(string EstCode)
        {
            p_function.Value = 15;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode).Tables[0].Rows[0]["EstCode"].ToString();
        }

        public DataTable GetDataPiezometer(string CompanyCode, string EstCode, DateTime StartDate, DateTime EndDate)
        {

            p_function.Value = 16;
            //p_function.Value = 44;
            p_CompanyCode.Value = CompanyCode;
            p_EstCode.Value = EstCode;
            p_StartDate.Value = StartDate;
            p_EndDate.Value = EndDate;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_OnlineMap", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_CompanyCode);
                command.Parameters.Add(p_EstCode);
                command.Parameters.Add(p_StartDate);
                command.Parameters.Add(p_EndDate);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }

        public DataTable GetProgressForDashboard(string CompanyCode, string EstCode)
        {
            p_function.Value = 17;
            p_CompanyCode.Value = CompanyCode;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode).Tables[0];
        }

        public DataTable GetProgressForDashboardPerCompany(string EstCode)
        {
            p_function.Value = 18;
            //p_function.Value = 42;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode).Tables[0];
        }

        public DataTable GetDataProgressForDashboard(string CompanyCode, string EstCode, int AdjustmentValue)
        {
            p_function.Value = 19;
            //p_function.Value = 41;
            p_CompanyCode.Value = CompanyCode;
            p_EstCode.Value = EstCode;
            p_AdjustmentValue.Value = AdjustmentValue;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_AdjustmentValue).Tables[0];
        }

        public DataTable GetDataPengukuranForDashboard(string CompanyCode, string EstCode, int AdjustmentValue, string Keterangan)
        {
            p_function.Value = 20;
            //p_function.Value = 40;
            p_CompanyCode.Value = CompanyCode;
            p_EstCode.Value = EstCode;
            p_AdjustmentValue.Value = AdjustmentValue;
            p_Keterangan.Value = Keterangan;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_AdjustmentValue, p_Keterangan).Tables[0];
        }

        public DataTable ListCompany()
        {
            p_function.Value = 21;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }

        public DataTable GetDataKLHK(string CompanyCode, string EstCode, DateTime StartDate, DateTime EndDate)
        {
            p_function.Value = 22;
            p_CompanyCode.Value = CompanyCode;
            p_EstCode.Value = EstCode;
            p_StartDate.Value = StartDate;
            p_EndDate.Value = EndDate;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_OnlineMap", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_CompanyCode);
                command.Parameters.Add(p_EstCode);
                command.Parameters.Add(p_StartDate);
                command.Parameters.Add(p_EndDate);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }

        public DataTable ListCompanyKLHK()
        {
            p_function.Value = 23;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }

        public DataTable GetDataSumberData()
        {
            p_function.Value = 25;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }

        public DataTable ListLayerType()
        {
            p_function.Value = 104;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "IMV_OnlineMap", p_function).Tables[0];
        }
        public DataTable GetDateByWeekName(string WeekName)
        {
            p_function.Value = 67;
            p_WeekName.Value = WeekName;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_WeekName).Tables[0];
        }
        public DataTable GetListWeekNew()
        {
            p_function.Value = 27;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }
        public DataTable GetMasterZonaByWmArea(int idwmArea)
        {
            p_function.Value = 24;
            p_idWMArea.Value = idwmArea;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }
        public DataTable GetGraphByZona(int idWmaArea, string wmaCode)
        {
            p_function.Value = 29;
            p_idWMArea.Value = idWmaArea;
            p_wmaCode.Value = wmaCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_idWMArea, p_wmaCode).Tables[0];
        }
        public DataTable GetMasterBlock()
        {
            p_function.Value = 31;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }
        public DataTable GetAllExtendCoordinate()
        {
            p_function.Value = 33;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }
        public DataTable GetAllExtendCoordinatePulau()
        {
            p_function.Value = 34;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];

        }
        public DataTable getBlockParameter()
        {
            p_function.Value = 35;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }


        public void SavePathUpload(string uname, string path, string fileName, string uploadDate, string companyCode, string logger, string periode, string year)
        {
            p_function.Value = 67;
            p_uname.Value = uname;
            p_path.Value = path;
            p_fileName.Value = fileName;
            p_UploadDate.Value = uploadDate;
            p_CompanyCode.Value = companyCode;
            p_Block.Value = logger;
            p_periode.Value = periode;
            p_Year.Value = year;
            SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_uname, p_fileName, p_path, p_UploadDate, p_CompanyCode, p_Block, p_periode, p_Year);
        }

        public DataSet ListMonthTemp(string estCode)
        {
            p_function.Value = 68;
            p_EstCode.Value = estCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_EstCode);
        }

        public DataTable GetDataGenerateKLHK(string CompanyCode, string EstCode, string Periode, string Year)
        {
            p_function.Value = 69;
            p_CompanyCode.Value = CompanyCode;
            p_EstCode.Value = EstCode;
            p_periode.Value = Periode;
            p_Year.Value = Year;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_CompanyCode, p_EstCode, p_periode, p_Year);
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_Piezometer", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_CompanyCode);
                command.Parameters.Add(p_EstCode);
                command.Parameters.Add(p_periode);
                command.Parameters.Add(p_Year);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }

        public void UpdateGenerateKLHK(string PieRecordID, string WeekName, int HasilGenerate, int userID)
        {
            p_function.Value = 70;
            p_PieRecordID.Value = PieRecordID;
            p_WeekName.Value = WeekName;
            p_HasilGenerate.Value = HasilGenerate;
            p_UserID.Value = userID;
            SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_PieRecordID, p_WeekName, p_UserID, p_HasilGenerate);
        }

        public void SubmitFinalDataKLHK(string CompanyCode, string Periode, string EstCode, int userID)
        {
            p_function.Value = 71;
            p_CompanyCode.Value = CompanyCode;
            p_periode.Value = Periode;
            p_EstCode.Value = EstCode;
            p_UserID.Value = userID;
            SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_CompanyCode, p_periode, p_UserID, p_EstCode);
        }

        public void SaveDataKLHK(string EstCode, string block, string WeekName, string HasilGenerate, int userID)
        {
            p_function.Value = 72;
            p_EstCode.Value = EstCode;
            p_Block.Value = block;
            p_WeekName.Value = WeekName;
            p_HasilGenerate.Value = HasilGenerate;
            p_UserID.Value = userID;
            SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_EstCode, p_Block, p_WeekName, p_HasilGenerate, p_UserID);
        }

        public DataTable GetDataLastWeek(string CompanyCode, string EstCode, int MonthBefore)
        {
            p_function.Value = 73;
            p_CompanyCode.Value = CompanyCode;
            p_EstCode.Value = EstCode;
            p_periode.Value = MonthBefore;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_Piezometer", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_CompanyCode);
                command.Parameters.Add(p_EstCode);
                command.Parameters.Add(p_periode);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }


        public int GetLastIdGenerate()
        {
            p_function.Value = 74;
            return Convert.ToInt32(SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function).Tables[0].Rows[0]["lastId"]);
        }

        public void DeleteGenerateKLHK(string WeekName, string EstCode)
        {
            p_function.Value = 75;
            p_WeekName.Value = WeekName;
            p_EstCode.Value = EstCode;
            SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_WeekName, p_EstCode);
        }

        public string getStartDateBefore(DateTime StartDate)
        {
            p_function.Value = 76;
            p_StartingDate.Value = StartDate;
            return Convert.ToString(SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_StartingDate).Tables[0].Rows[0]["StartDateBefore"]);
        }
        public string getEndDateBefore(DateTime EndDate)
        {
            p_function.Value = 77;
            p_EndDate.Value = EndDate;
            return Convert.ToString(SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_EndDate).Tables[0].Rows[0]["EndDateBefore"]);
        }

        public string getWeekName(string TanggalSurvey)
        {
            p_function.Value = 78;
            p_TanggalSurvey.Value = TanggalSurvey;
            return Convert.ToString(SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_TanggalSurvey).Tables[0].Rows[0]["WeekName"]);
        }

        public DataTable ListEstate()
        {
            p_function.Value = 79;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function).Tables[0];
        }

        public string GetModuleAccess(int userID)
        {
            p_function.Value = 80;
            p_UserID.Value = userID;
            return Convert.ToString(SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_UserID).Tables[0].Rows[0]["mdlAccCode"]);
        }

        public string GetEstate(string CompanyCode)
        {

            p_function.Value = 81;
            p_CompanyCode.Value = CompanyCode;
            return Convert.ToString(SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_CompanyCode).Tables[0].Rows[0]["EstCode"]);
        }
        public DataSet ListLogger(string CompanyCode)
        {

            p_function.Value = 82;
            p_CompanyCode.Value = CompanyCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_CompanyCode);
        }
        public void SaveDataLoggerKLHK(string CompanyCode, string Tanggal, string Logger, string TMAT, int userID, string Referensi)
        {
            p_function.Value = 83;
            p_CompanyCode.Value = CompanyCode;
            p_Block.Value = Logger;
            p_KetinggianLogger.Value = TMAT;
            p_StartingDate.Value = Tanggal;
            p_UserID.Value = userID;
            p_Referensi.Value = Referensi;
            SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_CompanyCode, p_Block, p_KetinggianLogger, p_StartingDate, p_UserID, p_Referensi);
        }


        public DataTable GetHOBODoc(string CompanyCode, string Periode, string Year)
        {
            p_function.Value = 84;
            p_CompanyCode.Value = CompanyCode;
            p_periode.Value = Periode;
            p_Year.Value = Year;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_Piezometer", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_CompanyCode);
                command.Parameters.Add(p_periode);
                command.Parameters.Add(p_Year);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }

        public DataTable GetExcelDoc(string CompanyCode, string Periode, string Year)
        {
            p_function.Value = 85;
            p_CompanyCode.Value = CompanyCode;
            p_periode.Value = Periode;
            p_Year.Value = Year;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_Piezometer", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_CompanyCode);
                command.Parameters.Add(p_periode);
                command.Parameters.Add(p_Year);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }

        public DataTable GetExcelDetail(string logger, string Periode, string Year)
        {
            p_function.Value = 86;
            p_Block.Value = logger;
            p_periode.Value = Periode;
            p_Year.Value = Year;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_Piezometer", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_Block);
                command.Parameters.Add(p_periode);
                command.Parameters.Add(p_Year);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }
        public DataSet CreatedReport(string CompanyCode, string Periode, string Year)
        {
            p_function.Value = 87;
            p_CompanyCode.Value = CompanyCode;
            p_periode.Value = Periode;
            p_Year.Value = Year;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_CompanyCode, p_periode, p_Year);

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }


        public DataTable CheckNullData(string CompanyCode, string EstCode, string Periode, string Year)
        {
            p_function.Value = 88;
            p_CompanyCode.Value = CompanyCode;
            p_EstCode.Value = EstCode;
            p_periode.Value = Periode;
            p_Year.Value = Year;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_Piezometer", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_CompanyCode);
                command.Parameters.Add(p_EstCode);
                command.Parameters.Add(p_periode);
                command.Parameters.Add(p_Year);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }

        public DataTable CheckDataHasApprovedAll(string CompanyCode, string EstCode, string Periode, string Year)
        {
            p_function.Value = 89;
            p_CompanyCode.Value = CompanyCode;
            p_EstCode.Value = EstCode;
            p_periode.Value = Periode;
            p_Year.Value = Year;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_Piezometer", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_CompanyCode);
                command.Parameters.Add(p_EstCode);
                command.Parameters.Add(p_periode);
                command.Parameters.Add(p_Year);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }

        public DataTable CheckDataThisMonth(string CompanyCode, string EstCode, string Periode, string Year)
        {
            p_function.Value = 90;
            p_CompanyCode.Value = CompanyCode;
            p_EstCode.Value = EstCode;
            p_periode.Value = Periode;
            p_Year.Value = Year;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_Piezometer", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_CompanyCode);
                command.Parameters.Add(p_EstCode);
                command.Parameters.Add(p_periode);
                command.Parameters.Add(p_Year);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }


        public string checkDataHasApproved(string CompanyCode, string EstCode, string Periode, string Year)
        {
            p_function.Value = 91;
            p_CompanyCode.Value = CompanyCode;
            p_EstCode.Value = EstCode;
            p_periode.Value = Periode;
            p_Year.Value = Year;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_Year, p_periode, p_CompanyCode, p_EstCode).Tables[0].Rows[0]["Summary"].ToString();
        }


        //public DataTable GetDataApproved(string CompanyCode, string EstCode, string Periode, string Year)
        //{
        //    p_function.Value = 92;
        //    p_CompanyCode.Value = CompanyCode;
        //    p_EstCode.Value = EstCode;
        //    p_periode.Value = Periode;
        //    p_Year.Value = Year;
        //    //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        //    var dt = new DataTable();
        //    using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
        //    using (
        //        var command = new SqlCommand("PZO_Piezometer", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
        //    )
        //    {
        //        conn.Close();
        //        conn.Open();
        //        command.Parameters.Add(p_function);
        //        command.Parameters.Add(p_CompanyCode);
        //        command.Parameters.Add(p_EstCode);
        //        command.Parameters.Add(p_periode);
        //        command.Parameters.Add(p_Year);
        //        //command.CommandTimeout = 0;
        //        var adapter = new SqlDataAdapter(command);
        //        adapter.Fill(dt);
        //        command.Parameters.Clear();
        //        conn.Close();
        //    }

        //    return dt;

        //    //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        //}


        public DataTable GetDataBlockRehab(string CompanyCode)
        {
            p_function.Value = 92;
            p_CompanyCode.Value = CompanyCode;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_Piezometer", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_CompanyCode);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }


        public DataSet ListAllBlock(string EstCode)
        {
            p_function.Value = 93;
            p_EstCode.Value = EstCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_EstCode);
        }


        public string GetEstateByBlock(string block)
        {

            p_function.Value = 94;
            p_Block.Value = block;
            return Convert.ToString(SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_Block).Tables[0].Rows[0]["EstCode"]);
        }

        public void ClearBlockRehab()
        {
            p_function.Value = 95;
            //SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function);
        }

        public void RemoveBlock(string Estcode, string Block)
        {
            p_function.Value = 96;
            p_EstCode.Value = Estcode;
            p_Block.Value = Block;
            SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_EstCode, p_Block);
        }
        public DataSet ListAllEstate()
        {
            p_function.Value = 97;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function);
        }

        public DataSet ListAfdeling(string Estcode)
        {
            p_function.Value = 98;
            p_EstCode.Value = Estcode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_EstCode);
        }
        public void SaveData(PZO_Domain objpzoDomain)
        {
            p_function.Value = 99;
            p_EstCode.Value = objpzoDomain.EstCode;
            p_Afd.Value = objpzoDomain.Afdeling;
            p_Block.Value = objpzoDomain.Block;
            p_KodeTMAT.Value = objpzoDomain.KodeTMAT;
            p_Merk.Value = objpzoDomain.Merk;
            p_Type.Value = objpzoDomain.Type;
            p_SN.Value = objpzoDomain.SerialNumber;
            p_Kondisi.Value = objpzoDomain.Kondisi;
            p_Baterai.Value = objpzoDomain.Baterai;
            p_LastCheck.Value = objpzoDomain.lastCheck;
            p_Remark.Value = objpzoDomain.Remark;
            SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function,
                p_EstCode, p_Afd, p_Block, p_KodeTMAT, p_Merk, p_Type, p_SN, p_Kondisi, p_Baterai, p_LastCheck, p_Remark);
        }

        public DataTable GetMasterHOBO(string estCode, string afd, string block)
        {
            p_function.Value = 100;
            p_EstCode.Value = estCode;
            p_Afd.Value = afd;
            p_Block.Value = block;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_Piezometer", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_EstCode);
                command.Parameters.Add(p_Afd);
                command.Parameters.Add(p_Block);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_CompanyCode, p_EstCode, p_StartDate, p_EndDate).Tables[0];
        }
        public DataSet ListDataMasterHOBO(string ID)
        {
            p_function.Value = 101;
            p_ID.Value = ID;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_ID);
        }


        public void EditData(PZO_Domain objpzoDomain)
        {
            p_function.Value = 102;
            p_ID.Value = objpzoDomain.ID;
            p_EstCode.Value = objpzoDomain.EstCode;
            p_Afd.Value = objpzoDomain.Afdeling;
            p_Block.Value = objpzoDomain.Block;
            p_KodeTMAT.Value = objpzoDomain.KodeTMAT;
            p_Merk.Value = objpzoDomain.Merk;
            p_Type.Value = objpzoDomain.Type;
            p_SN.Value = objpzoDomain.SerialNumber;
            p_Kondisi.Value = objpzoDomain.Kondisi;
            p_Baterai.Value = objpzoDomain.Baterai;
            p_LastCheck.Value = objpzoDomain.lastCheck;
            p_Remark.Value = objpzoDomain.Remark;
            SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function,
                p_ID, p_EstCode, p_Afd, p_Block, p_KodeTMAT, p_Merk, p_Type, p_SN, p_Kondisi, p_Baterai, p_LastCheck, p_Remark);
        }


        public void DeleteData(PZO_Domain objpzoDomain)
        {
            p_function.Value = 103;
            p_ID.Value = objpzoDomain.ID;
            SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_ID);
        }

        public DataSet ListBlock(string afdeling)
        {
            p_function.Value = 104;
            p_Afd.Value = afdeling;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_Afd);
        }

        public DataSet ListTMAT(string block)
        {
            p_function.Value = 105;
            p_Block.Value = block;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Piezometer", p_function, p_Block);
        }


        public DataTable GenerateDataReport(int AdjustmentValue)
        {
            p_function.Value = 39;
            p_AdjustmentValue.Value = AdjustmentValue;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_AdjustmentValue).Tables[0];
        }

        public DataTable GenerateDataReport(int AdjustmentValue, int userID)
        {
            p_function.Value = 39;
            p_AdjustmentValue.Value = AdjustmentValue;
            p_UserID.Value = userID;
            //return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_AdjustmentValue, p_UserID).Tables[0];

            var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_OnlineMap", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_AdjustmentValue);
                command.Parameters.Add(p_UserID);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }

            return dt;

        }
        public DataSet GetEstCode(string estCode)
        {
            p_function.Value = 40;
            p_EstCode.Value = estCode;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function, p_EstCode);
        }
        public void GenerateDataProgressForDashboard()
        {            
            p_function.Value = 41;           
            //SqlHelper.ExecuteNonQuery(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function);
            //var dt = new DataTable();
            using (var conn = new SqlConnection(PZO_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("PZO_OnlineMap", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);               
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                //adapter.Fill(dt);
                command.Parameters.Clear();
                conn.Close();
            }
            
        }
        public DataSet GetStatusServer()
        {
            p_function.Value = 108;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringHMSAPP, CommandType.StoredProcedure, "SP_MCS_Web_22", p_function);
        }
        public DataSet GetLanguage()
        {
            p_function.Value = 29;
            p_mdlCode.Value = "WLR";
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "API_WEB4", p_function, p_mdlCode);
        }

        //UPDATE DICKI 
        public DataSet GetPiezomenterGraph(string estCode,string zona)
        {
            p_function.Value = 1;
            p_multipleEstCode.Value = estCode;
            p_multipleZona.Value = zona;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT", 
                p_function,
                p_multipleEstCode,
                p_multipleZona
            );
        }

        public DataSet GetCurahHujanGraph(string idWMArea)
        {
            //comment 
            p_function.Value = 2;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_idWMArea
            );
        }

        public DataSet CreateGraphTMAT(string graphName, int wmArea, string zonaId, int createBy, int updateBy)
        {
            p_function.Value = 3;
            p_graphName.Value = graphName;
            p_wmArea.Value = wmArea;
            p_zonaId.Value = zonaId;
            p_createBy.Value = createBy;
            p_updateBy.Value = updateBy;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_graphName,
                p_wmArea,
                p_zonaId,
                p_createBy,
                p_updateBy
            );
        }

        public DataSet CreateGraphTMATDetail(int idGraph, string curahHujan)
        {
            p_function.Value = 4;
            p_idGraph.Value = idGraph;
            p_curahHujan.Value = curahHujan;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_idGraph,
                p_curahHujan
            );
        }

        public DataSet CreateGraphTMATDetailPizo(int idGraph, string pieRecordID,string colorField, int lineThickness)
        {
            p_function.Value = 5;
            p_idGraph.Value = idGraph;
            p_pieRecordID.Value = pieRecordID;
            p_colorField.Value = colorField;
            p_lineThickness.Value = lineThickness;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_idGraph,
                p_pieRecordID,
                p_colorField,
                p_lineThickness
            );
        }

        public DataSet ShowGraphTMAT(string idWMArea)
        {
            p_function.Value = 7;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_idWMArea
            );
        }

        public DataSet showValueGraphTMAT( string pieRecordID, string startDate, string endDate)
        {
            p_function.Value = 6;
            p_pieRecordID.Value = pieRecordID;
            p_startDate.Value = startDate;
            p_endDate.Value = endDate;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_pieRecordID,
                p_startDate,
                p_endDate
            );
        }

        public DataTable getValueCurahHujanGraphTmat(String estCode, String station, string pieRecordID, string startDate, string endDate)
        {
            p_function.Value = 8;
            p_estCode.Value = estCode;
            p_stationName.Value = station;
            p_pieRecordID.Value = pieRecordID;
            p_startDate.Value = startDate;
            p_endDate.Value = endDate;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT", 
                p_function, 
                p_estCode, 
                p_stationName,
                p_pieRecordID,
                p_startDate,
                p_endDate
             ).Tables[0];
        }

        public DataSet ShowGraphTMATById(int idGraph)
        {
            p_function.Value = 9;
            p_idGraph.Value = idGraph;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_idGraph
            );
        }

        public DataSet getPizometerByIdGraph(int idGraph)
        {
            p_function.Value = 10;
            p_idGraph.Value = idGraph;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_idGraph
            );
        }

        public DataSet getCurahHujanByIdGraph(int idGraph)
        {
            p_function.Value = 11;
            p_idGraph.Value = idGraph;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_idGraph
            );
        }

        public DataSet getDeepIndicatorColor()
        {
            p_function.Value = 12;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function
            );
        }

        public DataSet updateGraphTMAT(int idGraph,string graphName, string zonaId, int updateBy)
        {
            p_function.Value = 13;
            p_idGraph.Value = idGraph;
            p_graphName.Value = graphName;
            p_zonaId.Value = zonaId;
            p_updateBy.Value = updateBy;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_idGraph,
                p_graphName,
                p_zonaId,
                p_updateBy
            );
        }
        public DataSet deleteGraphTMAT(int idGraph)
        {
            p_function.Value = 14;
            p_idGraph.Value = idGraph;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_idGraph
            );
        }

        public DataSet getZona(int idWmArea)
        {
            p_function.Value = 15;
            p_wmArea.Value = idWmArea;

            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_wmArea
            );
        }

        public DataSet getEstcode(string zona, int idWmArea)
        {
            p_function.Value = 16;
            p_multipleZona.Value = zona;
            p_wmArea.Value = idWmArea;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_multipleZona,
                p_wmArea
            );
        }

        public DataSet showValueGraphTMATPerDay(string pieRecordID, string startDate, string endDate)
        {
            p_function.Value = 17;
            p_pieRecordID.Value = pieRecordID;
            p_startDate.Value = startDate;
            p_endDate.Value = endDate;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_pieRecordID,
                p_startDate,
                p_endDate
            );
        }

        public DataTable getValueCurahHujanGraphTmatPerDay(String estCode, String station, string startDate, string endDate)
        {
            p_function.Value = 18;
            p_estCode.Value = estCode;
            p_stationName.Value = station;
            p_startDate.Value = startDate;
            p_endDate.Value = endDate;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_Graph_TMAT",
                p_function,
                p_estCode,
                p_stationName,
                p_startDate,
                p_endDate
             ).Tables[0];
        }
        #endregion
    }
}