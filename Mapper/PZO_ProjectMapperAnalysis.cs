﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PZO_PiezometerOnlineMap.Mapper
{
    public class PZO_ProjectMapperAnalysis
    {
        protected SqlParameter p_function = new SqlParameter("@function", SqlDbType.Int);
        protected SqlParameter p_text_param1 = new SqlParameter("@text_param1", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param2 = new SqlParameter("@text_param2", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param3 = new SqlParameter("@text_param3", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param4 = new SqlParameter("@text_param4", SqlDbType.VarChar, 400);
        protected SqlParameter p_text_param5 = new SqlParameter("@text_param5", SqlDbType.VarChar, 300);
        protected SqlParameter p_text_param6 = new SqlParameter("@text_param6", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param7 = new SqlParameter("@text_param7", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param8 = new SqlParameter("@text_param8", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param9 = new SqlParameter("@text_param9", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param10 = new SqlParameter("@text_param10", SqlDbType.VarChar, 200);
        protected SqlParameter p_text_param11 = new SqlParameter("@text_param11", SqlDbType.VarChar, 200);

        protected SqlParameter p_int_param1 = new SqlParameter("@int_param1", SqlDbType.Int);
        protected SqlParameter p_int_param2 = new SqlParameter("@int_param2", SqlDbType.Int);
        protected SqlParameter p_int_param3 = new SqlParameter("@int_param3", SqlDbType.Int);
        protected SqlParameter p_int_param4 = new SqlParameter("@int_param4", SqlDbType.Int);
        protected SqlParameter p_int_param5 = new SqlParameter("@int_param5", SqlDbType.Int);
        protected SqlParameter p_int_param6 = new SqlParameter("@int_param6", SqlDbType.Int);
        protected SqlParameter p_int_param7 = new SqlParameter("@int_param7", SqlDbType.Int);
        protected SqlParameter p_int_param8 = new SqlParameter("@int_param8", SqlDbType.Int);
        protected SqlParameter p_int_param9 = new SqlParameter("@int_param9", SqlDbType.Int);

        protected SqlParameter p_date_param1 = new SqlParameter("@date_param1", SqlDbType.Date);
        protected SqlParameter p_date_param2 = new SqlParameter("@date_param2", SqlDbType.Date);

        protected SqlParameter p_float_param1 = new SqlParameter("@float_param1", SqlDbType.Float);
        protected SqlParameter p_float_param2 = new SqlParameter("@float_param2", SqlDbType.Float);
        protected SqlParameter p_float_param3 = new SqlParameter("@float_param3", SqlDbType.Float);
        protected SqlParameter p_float_param4 = new SqlParameter("@float_param4", SqlDbType.Float);
        protected SqlParameter p_float_param5 = new SqlParameter("@float_param5", SqlDbType.Float);
        protected SqlParameter p_float_param6 = new SqlParameter("@float_param6", SqlDbType.Float);
        protected SqlParameter p_float_param7 = new SqlParameter("@float_param7", SqlDbType.Float);
        protected SqlParameter p_float_param8 = new SqlParameter("@float_param8", SqlDbType.Float);
        protected SqlParameter p_float_param9 = new SqlParameter("@float_param9", SqlDbType.Float);
        protected SqlParameter p_float_param10 = new SqlParameter("@float_param10", SqlDbType.Float);

        public DataSet GetCompanyEstate(int userid)
        {
            p_function.Value = 2;
            p_int_param1.Value = userid;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "AWM_Web", p_function, p_int_param1);
        }

        public DataSet GetLayerBaseMap(string area)
        {
            p_function.Value = 43;
            p_text_param1.Value = area;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "WTW_Web", p_function, p_text_param1);
        }

        // Linked
        public DataSet GetParitBy(int fc, string value)
        {
            p_function.Value = fc;
            p_text_param1.Value = value;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "WTW_Web_Linked", p_function, p_text_param1);
        }

        public DataSet GetParit(int fc, string kebun, int userid)
        {
            p_function.Value = fc;
            p_text_param1.Value = kebun;
            p_int_param1.Value = userid;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "WTW_Web", p_function, p_text_param1, p_int_param1);
        }

        public DataSet GetEstateMappingNew()
        {
            p_function.Value = 35;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "WTW_Web", p_function);
        }

        public DataSet GetKetinggianPiezometerByZona(string companyCode, string zona, int week, int month, int tahun)
        {
            p_function.Value = 1;
            p_text_param1.Value = companyCode;
            p_text_param2.Value = zona;
            p_int_param1.Value = week;
            p_int_param2.Value = month;
            p_int_param3.Value = tahun;
            return SqlHelper.ExecuteDataset(PZO_ConnectionString.connStringGISAPP.ToString(), CommandType.StoredProcedure, "PZO_OnlineMap_4", p_function, p_text_param1, p_text_param2, p_int_param1, p_int_param2, p_int_param3);
        }

        public DataSet SaveZonaAnalysis(int id, string namaZona, int userid,string companyCode, string wmArea, string zona, string pzo, string awl)
        {
            p_function.Value = 2;
            p_int_param1.Value = id;
            p_int_param2.Value = userid;
            p_text_param1.Value = namaZona;
            p_text_param2.Value = companyCode;
            p_text_param3.Value = wmArea;
            p_text_param4.Value = zona;
            p_text_param5.Value = pzo;
            p_text_param6.Value = awl;
            return SqlHelper.ExecuteDataset(
                PZO_ConnectionString.connStringGISAPP.ToString(),
                CommandType.StoredProcedure,
                "PZO_OnlineMap_4", p_function, p_int_param1, p_int_param2, p_text_param1, p_text_param2, p_text_param3, p_text_param4, p_text_param5, p_text_param6
            );

        }

        public DataSet DeleteZonaAnalysis(int id, int userid,string companyCode)
        {
            p_function.Value = 3;
            p_int_param1.Value = id;
            p_int_param2.Value = userid;
            p_text_param1.Value = companyCode;
            return SqlHelper.ExecuteDataset(
                PZO_ConnectionString.connStringGISAPP.ToString(),
                CommandType.StoredProcedure,
                "PZO_OnlineMap_4", p_function, p_int_param1, p_int_param2, p_text_param1
            );
        }

        public DataSet GetAllZonaAnalysis(string companyCode)
        {
            p_function.Value = 4;
            p_text_param1.Value = companyCode;
            return SqlHelper.ExecuteDataset(
                PZO_ConnectionString.connStringGISAPP.ToString(),
                CommandType.StoredProcedure,
                "PZO_OnlineMap_4", p_function, p_text_param1
            );
        }
    }
}