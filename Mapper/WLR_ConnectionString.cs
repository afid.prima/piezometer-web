﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace PZO_PiezometerOnlineMap.Mapper
{
    public class WLR_ConnectionString
    {
        public static string connStringGISAPP
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["conStrGISAPP"].ToString();
            }
        }
        public static string connStringHMSAPP
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["connStringHMSAPP"].ToString();
            }
        }
        public static string connStringGISAPP2
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["conStrGISAPP2"].ToString();
            }
        }
    }
}