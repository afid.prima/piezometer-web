﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace PZO_PiezometerOnlineMap.Mapper
{
    public class WLR_Mapper
    {
        #region Parameters Declaration

        protected SqlParameter p_function = new SqlParameter("@function", SqlDbType.Int);
        protected SqlParameter p_dateParam = new SqlParameter("@dateParam", SqlDbType.VarChar, 10);
        protected SqlParameter p_dateParam2 = new SqlParameter("@dateParam2", SqlDbType.VarChar, 10);
        protected SqlParameter p_dateParam3 = new SqlParameter("@dateParam3", SqlDbType.VarChar, 10);
        protected SqlParameter p_dateParam4 = new SqlParameter("@dateParam4", SqlDbType.VarChar, 10);
        protected SqlParameter p_companyCode = new SqlParameter("@companyCode", SqlDbType.VarChar, 10);
        protected SqlParameter p_estCode = new SqlParameter("@estCode", SqlDbType.VarChar, 10);
        protected SqlParameter p_stationId = new SqlParameter("@stationId", SqlDbType.VarChar, 50);
        protected SqlParameter p_stationName = new SqlParameter("@stationName", SqlDbType.VarChar, 50);
        protected SqlParameter p_measurementId = new SqlParameter("@measurementId", SqlDbType.VarChar, 50);
        protected SqlParameter p_dynamicMeasurement = new SqlParameter("@dynamicMeasurement", SqlDbType.Text);
        protected SqlParameter p_userId = new SqlParameter("@userId", SqlDbType.VarChar, 10);
        protected SqlParameter p_frequency = new SqlParameter("@frequency", SqlDbType.Int);
        protected SqlParameter p_zona = new SqlParameter("@zona", SqlDbType.VarChar, 50);
        protected SqlParameter p_elvTanah = new SqlParameter("@elvTanah", SqlDbType.Float, 50);
        protected SqlParameter p_idWMArea = new SqlParameter("@idWMArea", SqlDbType.VarChar, 50);
        protected SqlParameter p_idGraph = new SqlParameter("@idGraph", SqlDbType.Int);
        protected SqlParameter p_namaGraph = new SqlParameter("@namaGraph", SqlDbType.VarChar, 50);
        protected SqlParameter p_sortField = new SqlParameter("@sortField", SqlDbType.Int);
        protected SqlParameter p_colorField = new SqlParameter("@colorField", SqlDbType.VarChar, 10);
        protected SqlParameter p_namaFileGraph = new SqlParameter("@namaFileGraph", SqlDbType.VarChar, 50);
        protected SqlParameter p_status = new SqlParameter("@status", SqlDbType.VarChar, 10);
        protected SqlParameter p_rambuCurahHujan = new SqlParameter("@rambuCurahHujan", SqlDbType.Text);
        protected SqlParameter p_note = new SqlParameter("@note", SqlDbType.Text);
        protected SqlParameter p_NamaKlasifikasi = new SqlParameter("@namaKlasifikasi", SqlDbType.VarChar, 30);
        protected SqlParameter p_NilaiMinimum = new SqlParameter("@nilaiMinimum", SqlDbType.Float);
        protected SqlParameter P_NilaiMaksimum = new SqlParameter("@nilaiMaksimum", SqlDbType.Float);
        protected SqlParameter p_keteranganGrafik = new SqlParameter("@keteranganGrafik", SqlDbType.Text);
        protected SqlParameter p_measurementDate = new SqlParameter("@measurementDate", SqlDbType.VarChar, 40);
        protected SqlParameter p_inputDate = new SqlParameter("@inputDate", SqlDbType.VarChar, 40);
        protected SqlParameter p_recordStationId = new SqlParameter("@recordStationId", SqlDbType.VarChar, 40);
        protected SqlParameter p_tanggalValidasi = new SqlParameter("@tanggalValidasi", SqlDbType.VarChar, 10);
        protected SqlParameter p_TmasDefaultValue = new SqlParameter("@tmasTarget", SqlDbType.VarChar, 50);
        protected SqlParameter p_startDates = new SqlParameter("@startdates", SqlDbType.VarChar, 10);
        protected SqlParameter p_endDates = new SqlParameter("@enddates", SqlDbType.VarChar, 10);
        protected SqlParameter p_Year = new SqlParameter("@Year", SqlDbType.Int);
        protected SqlParameter p_Month = new SqlParameter("@Month", SqlDbType.Int);
        protected SqlParameter p_Week = new SqlParameter("@Week", SqlDbType.Int);
        protected SqlParameter p_WmaCode = new SqlParameter("@wmacode", SqlDbType.VarChar, 255);
        protected SqlParameter p_graphId = new SqlParameter("@graph", SqlDbType.VarChar, 255);
        protected SqlParameter p_graphSelected = new SqlParameter("@graphSelected", SqlDbType.VarChar, 255);
        protected SqlParameter p_rambuRefLama = new SqlParameter("@RambuRefLama", SqlDbType.VarChar, 9999999);
        protected SqlParameter P_WMA_CodeBaru = new SqlParameter("@WMA_CodeBaru", SqlDbType.VarChar, 255);
        protected SqlParameter P_WMA_CodeLama = new SqlParameter("@WMA_CodeLama", SqlDbType.VarChar, 255);
        protected SqlParameter p_RambuRefBaru = new SqlParameter("@RambuRefBaru", SqlDbType.VarChar, 255);
        protected SqlParameter p_RambuLamaKey = new SqlParameter("@RambuLamaKey", SqlDbType.VarChar, 255);
        protected SqlParameter p_mdlCode = new SqlParameter("@mdlCode", SqlDbType.VarChar);
        protected SqlParameter p_namaSurveyor = new SqlParameter("@namaSurveyor", SqlDbType.VarChar, 255);
        protected SqlParameter p_nikSurveyor = new SqlParameter("@nikSurveyor", SqlDbType.VarChar, 255);
        protected SqlParameter p_createBy = new SqlParameter("@createBy", SqlDbType.Int);
        protected SqlParameter p_fileUploads = new SqlParameter("@fileUpload", SqlDbType.VarChar, 255);
        protected SqlParameter p_urutan = new SqlParameter("@urutan", SqlDbType.Int);
        protected SqlParameter p_namaReport = new SqlParameter("@namaReport", SqlDbType.VarChar, 255);
        protected SqlParameter p_curahHujan = new SqlParameter("@curahHujan", SqlDbType.VarChar, 255);

        protected SqlParameter p_KLHK = new SqlParameter("@KLHK", SqlDbType.VarChar, 50);
        
        protected SqlParameter p_param1 = new SqlParameter("@param1", SqlDbType.VarChar, 255);
        protected SqlParameter p_param2 = new SqlParameter("@param2", SqlDbType.VarChar, 255);

        //update dicki @estCode @


        #endregion

        #region Method

        public DataSet ListCompany()
        {
            p_function.Value = 6;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function);
        }

        public DataSet ListEstate()
        {
            p_function.Value = 5;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function);
        }

        public DataSet GetListWeek()
        {
            p_function.Value = 10;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP2, CommandType.StoredProcedure, "PZO_OnlineMap", p_function);
        }

        public DataSet GetWeekFromCurrentDate()
        {
            p_function.Value = 7;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP2, CommandType.StoredProcedure, "PZO_OnlineMap", p_function);
        }

        public DataTable GetDataDashboardPerCompany(String tglSekarang, String companyCode)
        {
            p_function.Value = 1;
            p_dateParam.Value = tglSekarang;
            p_companyCode.Value = companyCode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_dateParam, p_companyCode).Tables[0];
        }

        public DataTable GetDataDashboardAll(String tglSekarang)
        {
            p_function.Value = 2;
            p_dateParam.Value = tglSekarang;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_dateParam).Tables[0];
        }

        public DataTable GetDataDashboardPerCompanyWeek(String startDate, String endDate, String companyCode)
        {
            p_function.Value = 4;
            p_dateParam.Value = startDate;
            p_dateParam2.Value = endDate;
            p_companyCode.Value = companyCode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_dateParam, p_dateParam2, p_companyCode).Tables[0];
        }

        public DataTable GetDataDashboardAllWeek(String startDate, String endDate)
        {
            p_function.Value = 3;
            p_dateParam.Value = startDate;
            p_dateParam2.Value = endDate;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_dateParam, p_dateParam2).Tables[0];
        }

        public DataTable GetAllStationbyEstateActive(String estCode)
        {
            p_function.Value = 7;
            p_estCode.Value = estCode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_estCode).Tables[0];
        }

        public DataTable GetStationMeasure(String stationId)
        {
            p_function.Value = 8;
            p_stationId.Value = stationId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_stationId).Tables[0];
        }

        public DataTable GetStationImages(String stationId)
        {
            p_function.Value = 9;
            p_stationId.Value = stationId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_stationId).Tables[0];
        }

        public DataTable GetMeasureImages(String stationId)
        {
            p_function.Value = 10;
            p_stationId.Value = stationId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_stationId).Tables[0];
        }

        public int InsertHistoryMeasure(JObject jobj)
        {
            p_function.Value = 11;
            p_stationId.Value = jobj["recordStationId"].ToString();
            p_measurementId.Value = jobj["measurementId"].ToString();
            p_dynamicMeasurement.Value = jobj["dynamicMeasurement"].ToString();
            p_userId.Value = jobj["editBy"].ToString();
            return Convert.ToInt32(SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_stationId, p_measurementId, p_dynamicMeasurement, p_userId).Tables[0].Rows[0]["result"].ToString());
        }

        public int UpdateMeasure(JObject jobj)
        {
            p_function.Value = 12;
            p_measurementId.Value = jobj["measurementId"].ToString();
            p_dynamicMeasurement.Value = jobj["dynamicMeasurement"].ToString();
            p_status.Value = jobj["status"].ToString();
            return Convert.ToInt32(SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_measurementId, p_dynamicMeasurement, p_status).Tables[0].Rows[0]["result"].ToString());
        }

        public int editSurveyorMeasure(JObject jobj)
        {
            p_function.Value = 72;
            p_measurementId.Value = jobj["measurementId"].ToString();
            p_recordStationId.Value = jobj["recordStationId"].ToString();
            p_nikSurveyor.Value = jobj["petugas"].ToString();
            p_urutan.Value = jobj["berbayar"].ToString();
            p_userId.Value = jobj["editBy"].ToString();
            return Convert.ToInt32(SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_measurementId, p_recordStationId, p_nikSurveyor, p_urutan, p_userId).Tables[0].Rows[0]["result"].ToString());
        }


        public DataTable GetHistoryMeasure(String measurementId)
        {
            p_function.Value = 13;
            p_measurementId.Value = measurementId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_measurementId).Tables[0];
        }

        public void UpdateStation(JObject jobj)
        {
            p_function.Value = 14;
            p_stationId.Value = jobj["stationId"].ToString();
            p_stationName.Value = jobj["stationName"].ToString();
            p_zona.Value = jobj["zonaId"].ToString();
            p_frequency.Value = Convert.ToInt32(jobj["frequency"].ToString());
            p_elvTanah.Value = Convert.ToDouble(jobj["elvTanah"].ToString());
            p_status.Value = jobj["status"].ToString();
            p_TmasDefaultValue.Value = jobj["targetWL"].ToString();
            SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_stationId, p_stationName, p_zona, p_elvTanah, p_frequency, p_status, p_TmasDefaultValue);
        }

        public DataTable ListZona(string company)
        {
            p_function.Value = 15;
            p_companyCode.Value = company;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_companyCode).Tables[0];
        }

        public DataSet ListWMArea()
        {
            p_function.Value = 16;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function);
        }

        public DataTable GetMasterZonaByWMArea(string idWMArea)
        {
            p_function.Value = 17;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }

        public DataTable GetRambuAirByWMArea(string idWMArea)
        {
            p_function.Value = 18;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }

        public DataTable GetRataRataCurahHujanByWMArea(string idWMArea)
        {
            p_function.Value = 19;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }

        public DataTable GetRataRataCurahHujanByWMAreaWithMaapingRecord(string idWMArea)
        {
            p_function.Value = 35;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }

        public DataTable InsertGraph(JObject jobj)
        {
            p_function.Value = 20;
            p_namaGraph.Value = jobj["namaGraph"].ToString();
            p_idWMArea.Value = jobj["idWMArea"].ToString();
            p_userId.Value = jobj["createBy"].ToString();
            p_rambuCurahHujan.Value = jobj["curahHujan"].ToString().Replace('[', ' ').Replace(']', ' ').Replace('"', ' ');
            p_keteranganGrafik.Value = jobj["keteranganGrafik"].ToString();
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_namaGraph, p_idWMArea, p_rambuCurahHujan, p_keteranganGrafik, p_userId).Tables[0];
        }

        public void InsertDetailGraph(JObject jobj, int idGraph)
        {
            p_function.Value = 21;
            p_idGraph.Value = idGraph;
            p_stationId.Value = jobj["stationId"].ToString();
            p_sortField.Value = Convert.ToInt32(jobj["sortField"].ToString());
            p_colorField.Value = jobj["colorField"].ToString();
            p_TmasDefaultValue.Value = jobj["TmasDefaultValue"].ToString();
            p_zona.Value = jobj["idZona"].ToString();
            SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idGraph, p_stationId, p_sortField, p_colorField, p_TmasDefaultValue, p_zona);
        }

        public void InsertNoteGraph(JObject jobj, int idGraph)
        {
            p_function.Value = 39;
            p_idGraph.Value = idGraph;
            p_dateParam.Value = jobj["tanggal"].ToString();
            p_note.Value = jobj["note"].ToString();
            p_status.Value = jobj["statusId"].ToString();
            SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idGraph, p_dateParam, p_note, p_status);
        }

        public DataTable UpdatePictureGraph(String nama, int idGraph)
        {
            p_function.Value = 22;
            p_idGraph.Value = idGraph;
            p_namaFileGraph.Value = nama;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idGraph, p_namaFileGraph).Tables[0];
        }

        public DataTable GetRambuByWMAreaForGraph(string idwmArea)
        {
            p_function.Value = 23;
            p_idWMArea.Value = idwmArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }

        public DataTable GetGraphByWMArea(string idwmArea)
        {
            p_function.Value = 24;
            p_idWMArea.Value = idwmArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }

        public void UpdateGraph(JObject jobj)
        {
            p_function.Value = 25;
            p_namaGraph.Value = jobj["namaGraph"].ToString();
            p_userId.Value = jobj["createBy"].ToString();
            p_idGraph.Value = jobj["idGraph"].ToString();
            p_rambuCurahHujan.Value = jobj["curahHujan"].ToString().Replace('[', ' ').Replace(']', ' ').Replace('"', ' ');
            p_keteranganGrafik.Value = jobj["keteranganGrafik"].ToString();
            SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idGraph, p_namaGraph, p_idWMArea, p_rambuCurahHujan, p_keteranganGrafik, p_userId);
        }

        public DataSet ListEstateByWmArea()
        {
            p_function.Value = 26;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function);
        }

        public DataSet listPetugas(string idWMArea)
        {
            p_function.Value = 64;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea);
        }

        public DataSet listPetugasActive(string idWMArea)
        {
            p_function.Value = 69;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea);
        }

        public DataSet UserAcces(string userId)
        {
            p_function.Value = 27;
            p_userId.Value = userId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_userId);
        }

        public DataTable DataMingguanByWmarea(JObject jobj)
        {
            DateTime dend = DateTime.ParseExact(jobj["dateParam2"].ToString(),
                                        "yyyyMMdd",
                                        CultureInfo.InvariantCulture,
                                        DateTimeStyles.None);
            dend = dend.AddDays(1);
            p_function.Value = 30;
            p_dateParam.Value = jobj["dateParam"].ToString();
            p_dateParam2.Value = dend.ToString("yyyyMMdd");
            p_frequency.Value = Convert.ToInt32(jobj["frequency"].ToString());
            p_idWMArea.Value = jobj["idwmarea"].ToString();
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_dateParam, p_dateParam2, p_frequency, p_idWMArea).Tables[0];
        }

        public DataTable DataMingguanByEstate(JObject jobj)
        {
            DateTime dend = DateTime.ParseExact(jobj["dateParam2"].ToString(),
                                        "yyyyMMdd",
                                        CultureInfo.InvariantCulture,
                                        DateTimeStyles.None);
            dend = dend.AddDays(1);
            p_function.Value = 31;
            p_dateParam.Value = jobj["dateParam"].ToString();
            p_dateParam2.Value = dend.ToString("yyyyMMdd");
            p_frequency.Value = Convert.ToInt32(jobj["frequency"].ToString());
            p_estCode.Value = jobj["estCode"].ToString();
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_dateParam, p_dateParam2, p_frequency, p_estCode).Tables[0];
        }

        public DataSet GetMeasureRambuPerWmAreaForMap(String idwmarea, String dateParam, String dateParam2, String dateParam3)
        {
            if (dateParam2 != "")
            {
                DateTime dend = DateTime.ParseExact(dateParam2,
                                            "yyyyMMdd",
                                            CultureInfo.InvariantCulture,
                                            DateTimeStyles.None);
                dend = dend.AddDays(1);
                dateParam2 = dend.ToString("yyyyMMdd");
            }
            p_function.Value = 32;
            p_idWMArea.Value = idwmarea;
            p_dateParam.Value = dateParam;
            p_dateParam2.Value = dateParam2;
            p_dateParam3.Value = dateParam3;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_dateParam, p_dateParam2, p_dateParam3, p_idWMArea);

        }

        public void DeleteGraph(JObject jobj)
        {
            p_function.Value = 33;
            p_idGraph.Value = jobj["idGraph"].ToString();
            SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idGraph);
        }

        public DataTable GetAllStationbyEstate(String estCode)
        {
            p_function.Value = 34;
            p_estCode.Value = estCode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_estCode).Tables[0];
        }

        public DataTable GetAllRambuCurahHujanByWmArea(String idWMArea)
        {
            p_function.Value = 36;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }

        public DataTable GetAllRambuCurahHujanByWmAreaNew(String idWMArea)
        {
            p_function.Value = 47;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }
        public DataTable GetAllRambuCurahHujanByWmAreaNew2(String idWMArea)
        {
            p_function.Value = 48;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }

        public DataTable getValueCurahHujan(String estCode, String station)
        {
            p_function.Value = 37;
            p_estCode.Value = estCode;
            p_stationName.Value = station;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_estCode, p_stationName).Tables[0];
        }

        public DataTable getLocationMeasure(String stationId)
        {
            p_function.Value = 38;
            p_stationId.Value = stationId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_stationId).Tables[0];
        }

        public DataTable getDetailNote(String idGrap)
        {
            p_function.Value = 40;
            p_idGraph.Value = idGrap;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idGraph).Tables[0];
        }

        public DataTable getFotoUdaraByEstate(String estCode)
        {
            p_function.Value = 41;
            p_estCode.Value = estCode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_estCode).Tables[0];
        }
        public void InsertKlasifikasiSetting(JObject jobj, int idGraph)
        {
            p_function.Value = 42;
            p_idGraph.Value = idGraph;
            p_NamaKlasifikasi.Value = jobj["namaKlasifikasi"].ToString();
            p_NilaiMinimum.Value = jobj["nilaiMinimum"].ToString();
            P_NilaiMaksimum.Value = jobj["nilaiMaksimum"].ToString();
            p_colorField.Value = jobj["codeWarna"].ToString();
            p_status.Value = jobj["statusId"].ToString();
            SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idGraph, p_NamaKlasifikasi, p_NilaiMinimum, P_NilaiMaksimum, p_colorField, p_status);

        }
        public DataTable getDetailKlasifikasi(String idGrap)
        {
            p_function.Value = 43;
            p_idGraph.Value = idGrap;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idGraph).Tables[0];
        }
        public string InsertNewMeasure(JObject jobj)
        {
            p_function.Value = 44;
            p_recordStationId.Value = jobj["recordStationId"].ToString();
            p_measurementDate.Value = jobj["tanggal"].ToString();
            p_dynamicMeasurement.Value = jobj["dynamicMeasurement"].ToString();
            p_userId.Value = jobj["inputBy"].ToString();
            return SqlHelper.ExecuteNonQuery(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_recordStationId, p_measurementDate, p_dynamicMeasurement, p_userId).ToString();
        }
        public DataTable GetStationMeasureOptimizm(String stationId)
        {
            p_function.Value = 46;
            p_dynamicMeasurement.Value = stationId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_dynamicMeasurement).Tables[0];
        }
        public DataSet getDataByDate(string date, string recordStationId)
        {
            p_function.Value = 45;
            p_tanggalValidasi.Value = date;
            p_recordStationId.Value = recordStationId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_tanggalValidasi, p_recordStationId);
        }
        public DataTable getDetailKlasifikasiNew(String idGrap)
        {
            p_function.Value = 49;
            p_idGraph.Value = idGrap;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idGraph).Tables[0];
        }
        public DataTable getTmasTarget(string idGrap)
        {
            p_function.Value = 50;
            p_idGraph.Value = idGrap;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idGraph).Tables[0];
        }
        public DataTable GetWmaCodeByWmArea(int idWmArea)
        {
            p_function.Value = 51;
            p_idWMArea.Value = idWmArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }
        public DataTable GetPzoWeekTemp()
        {
            p_function.Value = 27;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_OnlineMap", p_function).Tables[0];
        }
        public DataSet GetWLRChartData(string startDate, string endDate, int wmarea, string graphId)
        {
            p_function.Value = 28;  //Grafik Saja
            p_startDates.Value = startDate;
            p_endDates.Value = endDate;
            p_idWMArea.Value = wmarea;
            p_graphId.Value = graphId;
            var ds = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("RPT_GROUPREPORT", conn) { CommandType = CommandType.StoredProcedure }
            )
            {
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_startDates);
                command.Parameters.Add(p_endDates);
                command.Parameters.Add(p_idWMArea);
                command.Parameters.Add(p_graphId);
                command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                command.Parameters.Clear();
                conn.Close();
            }

            return ds;
        }


        public DataSet GetPerbandinganTMATTMASNew(string wmcode, int year, int month, int week)
        {
            p_function.Value = 30; //Laporan Analisa Perbandingan TMAS (Chart dan Map) OnePage
            p_Year.Value = year;
            p_Month.Value = month;
            p_Week.Value = week;
            p_WmaCode.Value = wmcode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_Year, p_Month, p_Week, p_WmaCode);
        }
        public DataSet GetPerbandinganTMATTMASNewMultiPage(string graph, int year, int month, int week)
        {
            p_function.Value = 31;  //Laporan Analisa Perbandingan TMAS (Chart dan Map) MultiPage
            p_Year.Value = year;
            p_Month.Value = month;
            p_Week.Value = week;
            p_graphId.Value = graph;
            var ds = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("RPT_GROUPREPORT", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_Year);
                command.Parameters.Add(p_Month);
                command.Parameters.Add(p_Week);
                command.Parameters.Add(p_graphId);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                command.Parameters.Clear();
                conn.Close();
            }

            return ds;

            //return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "RPT_GROUPREPORT", p_function, p_Year, p_Month, p_Week, p_graphId);
        }
        public DataTable GetSelectedGraphForReport(int idWmarea)
        {
            p_function.Value = 52;
            p_idWMArea.Value = idWmarea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea).Tables[0];
        }
        public int insertGraphSelectedReport(int idWMArea, string GraphSelected)
        {
            p_function.Value = 53;
            p_idWMArea.Value = idWMArea;
            p_graphSelected.Value = GraphSelected;
            return Convert.ToInt32(SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea, p_graphSelected).Tables[0].Rows[0]["result"].ToString()); ;
        }
        public DataSet GetDataMingguanPivote2d(string startDate, string EndDate, string startDate2, string EndDate2, string idWmArea)
        {
            p_function.Value = 55;
            p_dateParam.Value = startDate2;
            p_dateParam2.Value = EndDate2;
            p_dateParam3.Value = startDate;
            p_dateParam4.Value = EndDate;
            p_idWMArea.Value = idWmArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_dateParam, p_dateParam2, p_dateParam3, p_dateParam4, p_idWMArea);
        }
        public DataTable GetOldNewRambuRef(string RambuRef, string WmaCode)
        {
            p_function.Value = 56;
            p_rambuRefLama.Value = RambuRef;
            P_WMA_CodeBaru.Value = WmaCode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_rambuRefLama, P_WMA_CodeBaru).Tables[0];
        }
        public DataTable GetWmaCodeFromKeyRambu(string rambuLamaKey, string wmaCodeBaru)
        {

            p_function.Value = 57;
            p_RambuLamaKey.Value = rambuLamaKey;
            P_WMA_CodeBaru.Value = wmaCodeBaru;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_RambuLamaKey, P_WMA_CodeBaru).Tables[0];

        }
        public DataTable executeUpdateRambuRef(string RambuLamaKey, string wmaCodeBaru, string rambuRefLama, string rambuRefBaru)
        {
            p_function.Value = 58;
            p_RambuLamaKey.Value = RambuLamaKey;
            P_WMA_CodeBaru.Value = wmaCodeBaru;
            p_RambuRefBaru.Value = rambuRefBaru;
            p_rambuRefLama.Value = rambuRefLama;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_RambuLamaKey, P_WMA_CodeBaru, p_rambuRefLama, p_RambuRefBaru).Tables[0];

        }

        public DataSet ListCompanyCode()
        {
            p_function.Value = 59;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function);
        }
        public DataSet ListCompanyCodePZO()
        {
            p_function.Value = 591;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function);
        }

        public DataSet ListEstCode()
        {
            p_function.Value = 60;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function);
        }

        public DataSet ListZona()
        {
            p_function.Value = 61;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function);
        }

        public DataSet ListWM_Area()
        {
            p_function.Value = 62;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function);
        }

        
        public DataSet getChartFromZona(string zonaId)
        {
            p_function.Value = 63;
            p_zona.Value = zonaId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_zona);
        }

        public DataSet getSettingZona(string zonaAnalysisId)
        {
            SqlParameter p_zonaAnalysisId = new SqlParameter("@zonaAnalysisId", SqlDbType.VarChar, 10);
            p_function.Value = 631;
            p_zonaAnalysisId.Value = zonaAnalysisId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_zonaAnalysisId);
        }
        public DataSet getWeekID(string weekId, string weekIdBefore)
        {
            SqlParameter p_weekId = new SqlParameter("@weekId", SqlDbType.VarChar, 10);
            SqlParameter p_weekIdBefore = new SqlParameter("@weekIdBefore", SqlDbType.VarChar, 10);
            p_function.Value = 632;
            p_weekId.Value = weekId;
            p_weekIdBefore.Value = weekIdBefore;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_weekId, p_weekIdBefore);
        }
        
        public DataSet getSurveyor(string idWMArea,string estCode)
        {
            p_function.Value = 64;
            p_idWMArea.Value = idWMArea;
            p_estCode.Value = estCode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea, p_estCode);
        }

        public DataSet getSurveyorss(string idWMArea)
        {
            p_function.Value = 65;
            p_idWMArea.Value = idWMArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea);
        }

        public DataSet getSurveyorssedit(string idSurveyor)
        {
            p_function.Value = 73;
            p_urutan.Value = int.Parse(idSurveyor);
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_urutan);
        }

        public DataSet SaveNewMeasure(JObject json)
        {
            p_function.Value = 70;
            p_recordStationId.Value = json["RecordStationId"].ToString();
            p_dynamicMeasurement.Value = json["dynamic"].ToString();
            p_userId.Value = json["UserID"].ToString();
            p_measurementDate.Value = json["DateMeasure"].ToString();
            p_nikSurveyor.Value = json["petugas"].ToString();
            p_urutan.Value = json["berbayar"].ToString();
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_recordStationId, p_dynamicMeasurement, p_userId, p_measurementDate, p_nikSurveyor, p_urutan);
        }

        public DataSet InsertImageNewMeasure(JObject json, JObject objHeader, string measurementId)
        {
            p_function.Value = 71;
            p_measurementId.Value = measurementId;
            p_recordStationId.Value = objHeader["RecordStationId"].ToString();
            p_NamaKlasifikasi.Value = json["filename"].ToString();
            p_userId.Value = json["filename"].ToString();
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function,
                p_measurementId, p_recordStationId, p_NamaKlasifikasi, p_userId);
        }

        public DataSet getAllModuleWlr()
        {
            p_function.Value = 74;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function);
        }

        public DataSet getUserEstateAccess(String userId)
        {
            p_function.Value = 76;
            p_userId.Value = userId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_userId);
        }

        public DataSet getUserEstateAccessWMAreaMapping(String userId)
        {
            p_function.Value = 77;
            p_userId.Value = userId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_userId);
        }

        public DataSet showPICWLRWMarea(String idWmArea)
        {
            p_function.Value = 78;
            p_idWMArea.Value = idWmArea;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", p_function, p_idWMArea);
        }


        // Report TMAT & TMAS

        public DataSet LaporanTMATByZona(string dateparam, string wmArea, string KLHK)
        {
            p_function.Value = 3;
            p_dateParam.Value = dateparam;
            p_idWMArea.Value = wmArea;
            p_KLHK.Value = KLHK;
            //return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "ALL_REPORT", p_function, p_dateParam, p_idWMArea);

            var ds = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("ALL_REPORT", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_dateParam);
                command.Parameters.Add(p_idWMArea);
                command.Parameters.Add(p_KLHK);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                command.Parameters.Clear();
                conn.Close();
            }

            return ds;
        }



        public DataSet LaporanTMATByZonaKLHK(string dateparam, string wmArea)
        {
            p_function.Value = 33;
            p_dateParam.Value = dateparam;
            p_idWMArea.Value = wmArea;
            //return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "ALL_REPORT", p_function, p_dateParam, p_idWMArea);

            var ds = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("ALL_REPORT", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_dateParam);
                command.Parameters.Add(p_idWMArea);
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                command.Parameters.Clear();
                conn.Close();
            }

            return ds;
        }

        
        public DataSet LaporanPergerakanTMATByClass(string dateparam, string companyCode, string KLHK)
        {
            p_function.Value = 4;
            p_dateParam.Value = dateparam;
            p_companyCode.Value = companyCode;
            p_KLHK.Value = KLHK;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "ALL_REPORT", p_function, p_dateParam, p_companyCode, p_KLHK);

        }

        public DataSet LaporanPergerakanTMATByClassKLHK(string dateparam, string estCode, string KLHK)
        {
            p_function.Value = 5;
            p_dateParam.Value = dateparam;
            p_estCode.Value = estCode;
            p_KLHK.Value = KLHK;
            //return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "ALL_REPORT", p_function, p_dateParam, p_estCode);

            var ds = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("ALL_REPORT", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_dateParam);
                command.Parameters.Add(p_estCode);
                command.Parameters.Add(p_KLHK);
                
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                command.Parameters.Clear();
                conn.Close();
            }

            return ds;
        }

        //public DataSet LaporanPergerakanTMATKLHK(string dateparam, string wm_area, string companyCode)
        //{
        //    p_function.Value = 6;
        //    p_dateParam.Value = dateparam;
        //    p_WmaCode.Value = wm_area;
        //    p_companyCode.Value = companyCode;
        //    return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "ALL_REPORT", p_function, p_dateParam, p_WmaCode, p_companyCode);

        //}

        public DataSet LaporanPergerakanTMATKLHK(string dateparam, string wm_area, string companyCode, string KLHK)
        {
            //p_function.Value = 1;
            p_function.Value = 13;
            p_dateParam.Value = dateparam;
            p_WmaCode.Value = wm_area;
            p_companyCode.Value = companyCode;
            p_KLHK.Value = KLHK;
            //return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "TMAT_REPORT", p_function, p_dateParam, p_WmaCode, p_companyCode, p_KLHK);
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "ALL_REPORT", p_function, p_dateParam, p_WmaCode, p_companyCode, p_KLHK);

            
            //var ds = new DataSet();
            //using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            //using (var command = new SqlCommand("TMAT_REPORT", conn) { CommandType = CommandType.StoredProcedure })
            //{
            //    conn.Open();
            //    command.Parameters.Add(p_function);
            //    command.Parameters.Add(p_dateParam);
            //    command.Parameters.Add(p_WmaCode);
            //    command.Parameters.Add(p_companyCode);
            //    command.CommandTimeout = 0;
            //    var adapter = new SqlDataAdapter(command);
            //    adapter.Fill(ds);
            //    command.Parameters.Clear();
            //    conn.Close();
            //}

            //return ds;
        }

        public DataSet LaporanPergerakanTMATByKebun(string dateparam, string estCode, string KLHK)
        {
            p_function.Value = 7;
            p_dateParam.Value = dateparam;
            p_estCode.Value = estCode;
            p_KLHK.Value = KLHK;
            //return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "ALL_REPORT", p_function, p_dateParam, p_estCode);

            var ds = new DataSet();
            using (var conn = new SqlConnection(WLR_ConnectionString.connStringGISAPP))
            using (
                var command = new SqlCommand("ALL_REPORT", conn) { CommandType = CommandType.StoredProcedure, CommandTimeout = 0 }
            )
            {
                conn.Close();
                conn.Open();
                command.Parameters.Add(p_function);
                command.Parameters.Add(p_dateParam);
                command.Parameters.Add(p_estCode);
                command.Parameters.Add(p_KLHK);
                
                //command.CommandTimeout = 0;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
                command.Parameters.Clear();
                conn.Close();
            }

            return ds;
        }


        //public void insertLogData( string idZona, string DownloadFolder, string DownloadFileName)
        //{

        //    p_function.Value = 9001;
        //    p_WmaCode.Value = idZona;
        //    p_param1.Value = DownloadFolder;
        //    p_param2.Value = DownloadFileName;
        //    SqlHelper.ExecuteNonQuery(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_REPORT2", p_function, p_WmaCode, p_param1, p_param2);

        //}

        public DataSet insertLogData(string WMACode, string DownloadFolder, string DownloadFileName)
        {
            p_function.Value = 9001;
            p_WmaCode.Value = WMACode;
            p_param1.Value = DownloadFolder;
            p_param2.Value = DownloadFileName;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_REPORT2", p_function, p_WmaCode, p_param1, p_param2);

        }

        public DataSet cekDownloadChartImageZona(string WMACode, string DownloadFolder, string DownloadFileName)
        {
            p_function.Value = 9003;
            p_WmaCode.Value = WMACode;
            p_param1.Value = DownloadFolder;
            p_param2.Value = DownloadFileName;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_REPORT2", p_function, p_WmaCode, p_param1, p_param2);

        }


        public DataTable insertSuerveyor(JObject jobj)
        {
            p_function.Value = 66;
          //  p_status.Value = jobj["StatusActive"].ToString();
            p_namaSurveyor.Value = jobj["Nama"].ToString();
            if (jobj["nik"].ToString() == "") p_nikSurveyor.Value = '-';
            else p_nikSurveyor.Value = jobj["nik"].ToString();
            p_estCode.Value = jobj["estCode"].ToString();
            p_createBy.Value = Int32.Parse(jobj["createBy"].ToString());
            p_fileUploads.Value = jobj["fileUpload"].ToString();

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_status, p_namaSurveyor, p_nikSurveyor, p_estCode, p_createBy, p_fileUploads).Tables[0];
        }

        public DataTable insertSuerveyorHistory(JObject jobj)
        {
            p_function.Value = 75;
        
            p_namaSurveyor.Value = jobj["Nama"].ToString();
            if (jobj["nik"].ToString() == "") p_nikSurveyor.Value = '-';
            else p_nikSurveyor.Value = jobj["nik"].ToString();
            p_estCode.Value = jobj["estCode"].ToString();
            p_createBy.Value = Int32.Parse(jobj["createBy"].ToString());
            p_fileUploads.Value = jobj["fileUpload"].ToString();
         

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_status, p_namaSurveyor, p_nikSurveyor, p_estCode, p_createBy, p_fileUploads).Tables[0];
        }


        public DataTable ceknik(string nik)
        {
            p_function.Value = 67;
            p_nikSurveyor.Value = nik;
          
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_nikSurveyor).Tables[0];
        }

        public DataTable editsurvey(string idSurveyor, string status, string fileUploadedit)
        {
            p_function.Value = 68;
            p_urutan.Value = int.Parse(idSurveyor);
            p_status.Value = int.Parse(status);
            p_fileUploads.Value = fileUploadedit;
          

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_urutan,p_status,p_fileUploads).Tables[0];
        }
        public DataTable editsurveyHistory(string idSurveyor, string status)
        {
            p_function.Value = 68;
            p_urutan.Value = int.Parse(idSurveyor);
            p_status.Value = int.Parse(status);
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
               p_function, p_status, p_namaSurveyor, p_nikSurveyor, p_estCode, p_createBy, p_fileUploads).Tables[0];

        }
   
        public DataTable InsertReportSetting(JObject jobj, String zSelec)
        {

            p_function.Value   =    79;
            p_urutan.Value   =    jobj["idReport"].ToString();
            p_idWMArea.Value   =    jobj["idWMArea"].ToString();
            p_namaReport.Value =    jobj["namaReport"].ToString();
            p_createBy.Value   =    Int32.Parse(jobj["createBy"].ToString());
            p_curahHujan.Value =    jobj["curahHujan"].ToString();
            p_rambuRefLama.Value = zSelec;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_urutan, p_idWMArea, p_namaReport, p_createBy, p_curahHujan, p_rambuRefLama).Tables[0];
        }
        public DataTable getAllWmSettingReport(string idWMArea)
        {
            p_function.Value = 80;
            p_idWMArea.Value = idWMArea;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_idWMArea).Tables[0];
        }
        public DataSet getReportDetail(string idReport, string dateParam, string dateParam2)
        {
            p_function.Value = 81;
            p_urutan.Value = idReport;
            p_dateParam.Value = dateParam;
            p_dateParam2.Value = dateParam2;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_urutan, p_dateParam, p_dateParam2);
        }

        public DataTable DeleteToReportSetting(string idReport, string idWMArea, string createBy)
        {
            p_function.Value = 82;
            p_urutan.Value = idReport;
            p_createBy.Value = createBy;
            p_idWMArea.Value = idWMArea;

            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_urutan, p_createBy, p_idWMArea).Tables[0];
        }
        
        public DataTable SaveReportPemantauanHeader(string uID,string recordPemantauanId,string topik,string judul,string report,string pt)
        {
            p_function.Value = 83;
            p_createBy.Value = uID;
            p_urutan.Value = recordPemantauanId;
            P_WMA_CodeBaru.Value = topik;
            P_WMA_CodeLama.Value = judul;
            p_RambuRefBaru.Value = report;
            p_estCode.Value = pt;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB", 
                p_function, p_createBy, p_urutan, P_WMA_CodeBaru, P_WMA_CodeLama, p_RambuRefBaru, p_estCode).Tables[0];
        }

        public DataTable SaveReportPemantauanDetail(string recordPemantauanId, string detailTopik, string luasDas, string stationId)
        {
            p_function.Value = 84;
            p_urutan.Value = recordPemantauanId;
            P_WMA_CodeBaru.Value = detailTopik;
            P_WMA_CodeLama.Value = luasDas;
            p_RambuRefBaru.Value = stationId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_urutan, P_WMA_CodeBaru, P_WMA_CodeLama, p_RambuRefBaru).Tables[0];
        }

        public DataTable ShowReportPemantauanHeader(string pt)
        {
            p_function.Value = 85;
            p_estCode.Value = pt;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_estCode).Tables[0];
        }

        public DataTable ShowReportPemantauanById(string recordPemantauanId)
        {
            p_function.Value = 86;
            p_urutan.Value = recordPemantauanId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_urutan).Tables[0];
        }

        public DataTable ShowReportPemantauanByIdDataRambu(string recordPemantauanId, string dateParam, string dateParam2)
        {
            p_function.Value = 87;
            p_urutan.Value = recordPemantauanId;
            p_dateParam.Value = dateParam;
            p_dateParam2.Value = dateParam2;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_urutan, p_dateParam, p_dateParam2).Tables[0];
        }

        public DataTable DeletedReportPemantauan(string recordPemantauanId, string uID)
        {
            p_function.Value = 88;
            p_urutan.Value = recordPemantauanId;
            p_createBy.Value = uID;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "WLR_WEB",
                p_function, p_urutan, p_createBy).Tables[0];
        }

        public DataSet GetStatusServer()
        {
            p_function.Value = 108;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringHMSAPP, CommandType.StoredProcedure, "SP_MCS_Web_22", p_function);
        }
       
        //public DataSet GetStatusServer()
        //{
        //    p_function.Value = 108;
        //    return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "SP_MCS_Web_22", p_function);
        //}
        public DataSet GetLanguage()
        {
           p_function.Value = 29;
           p_mdlCode.Value = "WLR";
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "API_WEB4", p_function, p_mdlCode);
        }

      
        #endregion
    }

}