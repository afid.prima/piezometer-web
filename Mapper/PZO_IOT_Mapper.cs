﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PZO_PiezometerOnlineMap.Mapper
{
    public class PZO_IOT_Mapper
    {
        #region Parameters Declaration
        protected SqlParameter p_function = new SqlParameter("@function", SqlDbType.Int);
        protected SqlParameter p_userId = new SqlParameter("@userId", SqlDbType.VarChar);
        protected SqlParameter p_estcode = new SqlParameter("@estcode", SqlDbType.VarChar);
        protected SqlParameter p_piezoId = new SqlParameter("@piezoId", SqlDbType.VarChar);
        protected SqlParameter p_type = new SqlParameter("@type", SqlDbType.VarChar);
        protected SqlParameter p_deviceId = new SqlParameter("@deviceId", SqlDbType.VarChar);
        protected SqlParameter p_calibrasi = new SqlParameter("@calibrasi", SqlDbType.Float);
        protected SqlParameter p_dateParam = new SqlParameter("@dateParam", SqlDbType.VarChar);
        protected SqlParameter p_dateParam2 = new SqlParameter("@dateParam2", SqlDbType.VarChar);
        #endregion

        public DataSet getAllIotTMATByEstate(String estCode)
        {
            p_function.Value = 1;
            p_estcode.Value = estCode;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_IOT_Station", p_function, p_estcode);
        }

        public DataSet getPiezoMeasure(String piezoId, String dateParam, String dateParam2)
        {
            p_function.Value = 2;
            p_piezoId.Value = piezoId;
            p_dateParam.Value = dateParam;
            p_dateParam2.Value = dateParam2;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_IOT_Station", p_function, p_piezoId, p_dateParam, p_dateParam2);
        }

        public DataSet getIOTPiezoMertani(String deviceId, String piezoId, float calibrasi, String dateParam, String dateParam2)
        {
            p_function.Value = 3;
            p_deviceId.Value = deviceId;
            p_piezoId.Value = piezoId;
            p_calibrasi.Value = calibrasi; 
            p_dateParam.Value = dateParam;
            p_dateParam2.Value = dateParam2;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_IOT_Station", p_function, p_deviceId,  p_piezoId, p_calibrasi, p_dateParam, p_dateParam2);
        }

        public DataSet getIOTPiezoHolykel(String deviceId, String piezoId, float calibrasi, String dateParam, String dateParam2)
        {
            p_function.Value = 4;
            p_deviceId.Value = deviceId;
            p_piezoId.Value = piezoId;
            p_calibrasi.Value = calibrasi; 
            p_dateParam.Value = dateParam;
            p_dateParam2.Value = dateParam2;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_IOT_Station", p_function, p_deviceId,  p_piezoId, p_calibrasi, p_dateParam, p_dateParam2);
        }

        public void savePZOIOT(string userId,string estCode, string type, string deviceId, string piezoId, string dateParam, string dateParam2, float calibrasi)
        {
            p_function.Value = 5;
            p_userId.Value = userId;
            p_estcode.Value = estCode;
            p_deviceId.Value = deviceId;
            p_piezoId.Value = piezoId;
            p_type.Value = type;
            p_calibrasi.Value = calibrasi;
            p_dateParam.Value = dateParam;
            p_dateParam2.Value = dateParam2;
            SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_IOT_Station", p_function, p_userId, p_estcode, p_deviceId, p_piezoId, p_type, p_calibrasi, p_dateParam, p_dateParam2);
        }

        public void savePZOIOTHistory(string piezoId)
        {
            p_function.Value = 6;
            p_piezoId.Value = piezoId;
            SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_IOT_Station", p_function, p_piezoId);
        }

        public void deletePZOIOT(string piezoId)
        {
            p_function.Value = 7;
            p_piezoId.Value = piezoId;
            SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_IOT_Station", p_function, p_piezoId);
        }
        
        public DataSet getPZOIOTStation(string piezoId)
        {
            p_function.Value = 8;
            p_piezoId.Value = piezoId;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_IOT_Station", p_function, p_piezoId);
        }

        public DataSet validasiSavePZOIOT(string piezoId, string deviceId, string estCode, string dateParam, string dateParam2)
        {
            p_function.Value = 9;
            p_piezoId.Value = piezoId;
            p_deviceId.Value = deviceId;
            p_estcode.Value = estCode;
            p_dateParam.Value = dateParam;
            p_dateParam2.Value = dateParam2;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_IOT_Station", p_function, p_piezoId, p_deviceId, p_estcode, p_dateParam, p_dateParam2);
        }

        public DataSet getDetailCalibrate(String piezoId, String dateParam)
        {
            p_function.Value = 10;
            p_piezoId.Value = piezoId;
            p_dateParam.Value = dateParam;
            return SqlHelper.ExecuteDataset(WLR_ConnectionString.connStringGISAPP, CommandType.StoredProcedure, "PZO_IOT_Station", p_function, p_piezoId, p_dateParam);
        }
    }
}