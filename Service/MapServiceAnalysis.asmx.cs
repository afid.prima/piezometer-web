﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PZO_PiezometerOnlineMap.Mapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for MapServiceAnalysis
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MapServiceAnalysis : System.Web.Services.WebService
    {
        PZO_ProjectMapperAnalysis mapper = new PZO_ProjectMapperAnalysis();

        protected string dataTableToJson(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetLayerBaseMap(string area)
        {
            string hasil = "";
            DataSet ds = mapper.GetLayerBaseMap(area);
            hasil = dataTableToJson(ds.Tables[0]);
            return hasil;
        }



        [WebMethod]
        public string GetCompanyEstate(string userid)
        {
            string hasil;
            try
            {
                DataSet ds = mapper.GetCompanyEstate(Int32.Parse(userid));
                hasil = dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                hasil = "Error : " + ex.Message;
            }
            return hasil;
        }


        [WebMethod]
        public string[] GetParit(string fc, string kebun, string userid, string estcode, string estnewcode, string companycode)
        {
            string[] hasil = new string[2];
            try
            {
                DataSet ds = mapper.GetParit(Int16.Parse(fc), kebun, Int32.Parse(userid));
                hasil[0] = dataTableToJson(ds.Tables[0]);
                ds = mapper.GetEstateMappingNew();
                hasil[1] = dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                hasil[0] = "Error : " + ex.Message;
            }
            return hasil;
        }

        [WebMethod]
        public string GetParitBy(string fc, string kebun)
        {
            string hasil = "";
            try
            {
                DataSet ds = mapper.GetParitBy(Int16.Parse(fc), kebun);
                hasil = dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                hasil = GetParitBy2(fc, kebun);
            }
            return hasil;
        }

        [WebMethod]
        public string GetParitBy2(string fc, string kebun)
        {
            string hasil = "";
            try
            {
                DataSet ds = mapper.GetParitBy(Int16.Parse(fc), kebun);
                hasil = dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                hasil = "Error : " + ex.Message;
            }
            return hasil;
        }

        [WebMethod]
        public string[] GetKetinggianPiezometerByZona(string companyCode, string zona, int week, int month, int tahun)
        {
            string[] hasil = new string[5];
            DataSet ds = mapper.GetKetinggianPiezometerByZona(companyCode, zona, week, month, tahun);
            hasil[0] = dataTableToJson(ds.Tables[0]);
            hasil[1] = dataTableToJson(ds.Tables[1]);
            hasil[2] = dataTableToJson(ds.Tables[2]);

            return hasil;
        }
        
        [WebMethod]
        public string SaveZonaAnalysis(string obj)
        {
            string hasil = "";
            JObject json = JObject.Parse(obj.ToString());
            int id = int.Parse(json["id"].ToString() == "" ? "0" : json["id"].ToString());
            int userid = int.Parse(json["userid"].ToString());
            String companyCode = json["companyCode"].ToString();
            String wmArea = json["wmArea"].ToString();
            String namaZona = json["namaZona"].ToString();
            String zona = json["zona"].ToString();
            String pzo = "";
            String awl = json["awl"].ToString();

            JArray jArray = JArray.Parse(json["pzo"].ToString());
            for(int i = 0; i < jArray.Count; i++)
            {
                if(i == 0)
                {
                    pzo = jArray[i].ToString();
                }
                else
                {
                    pzo += ","+jArray[i].ToString();
                }
            }

            DataTable dt = mapper.SaveZonaAnalysis(id, namaZona, userid, companyCode, wmArea, zona, pzo, awl).Tables[0];
            return dataTableToJson(dt);
        }

        [WebMethod]
        public string DeleteZonaAnalysis(string obj)
        {
            string hasil = "";
            JObject json = JObject.Parse(obj.ToString());
            int id = int.Parse(json["id"].ToString());
            int userid = int.Parse(json["userid"].ToString());
            String companyCode = json["companyCode"].ToString();

            DataTable dt = mapper.DeleteZonaAnalysis(id, userid, companyCode).Tables[0];
            return dataTableToJson(dt);
        }

        [WebMethod]
        public string GetAllZonaAnalysis(string companyCode)
        {
            DataTable dt = mapper.GetAllZonaAnalysis(companyCode).Tables[0];
            return dataTableToJson(dt);
        }
    }
}
