﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using pass.huruf;
using PASS.Domain;
using PASS.Email;
using PASS.Mapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.Services;
using PZO_PiezometerOnlineMap.Mapper;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebServiceIOT : System.Web.Services.WebService
    {

        private Enkripsi login = new Enkripsi();
        private COR_UserMapper userObjMapper = new COR_UserMapper();
        private COR_userDomain domain = new COR_userDomain();
        private pass.huruf.Enkripsi url = new pass.huruf.Enkripsi();
        private Email email = new Email();
        //Mengambil daftar device
        [WebMethod]
        public string GetListDevice()
        {
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataSet ds = mapper.GetListDevice();
                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }



        }

        //Mengambil data curah hujan harian
        [WebMethod]
        public string GetDailyRainfall(string companycode, string estcode, string deviceid, string recordtime)
        {
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataSet ds = mapper.GetDailyRainfall(companycode, estcode, deviceid, recordtime);
                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }



        }
        //Mengambil summary data curah hujan bulanan
        [WebMethod]
        public string GetSummaryDaily(string companycode, string estcode, string deviceid, string recordtime)
        {
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataSet ds = mapper.GetSummaryDaily(companycode, estcode, deviceid, recordtime);
                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }



        }
        //Mengambil data curah hujan bulanan
        [WebMethod]
        public string GetMonthlyRainfall(string companycode, string estcode, string deviceid, string recordtime)
        {
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataSet ds = mapper.GetMonthlyRainfall(companycode, estcode, deviceid, recordtime);
                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }



        }

        //Mengambil data TMAT harian
        [WebMethod]
        public string GetDailyTMAT(string companycode, string estcode, string deviceid, string recordtime)
        {
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataSet ds = mapper.GetDailyTMAT(companycode, estcode, deviceid, recordtime);
                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }



        }


        //Mengambil Hari Libur
        [WebMethod]
        public string GetHoliday()
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.GetHoliday();
            return dataTableToJson(ds.Tables[0]);
        }

        ////Mengambil data TMAT bulanan
        //[WebMethod]
        //public string GetMonthlyTMAT(string companycode, string estcode, string deviceid, string startingdate, string endingdate, string chiwindspeed)
        //{
        //    try
        //    {
        //        IOTMapper mapper = new IOTMapper();
        //        DataSet ds = mapper.GetMonthlyTMAT(companycode, estcode, deviceid, startingdate, endingdate, chiwindspeed);
        //        return dataTableToJson(ds.Tables[0]);
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }



        //}

        //Mengambil data TMAS harian
        [WebMethod]
        public string GetDailyTMAS(string companycode, string estcode, string deviceid, string recordtime)
        {
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataSet ds = mapper.GetDailyTMAS(companycode, estcode, deviceid, recordtime);
                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }



        }

        //Mengambil data TMAT bulanan
        [WebMethod]
        public string GetMonthlyTMAS(string companycode, string estcode, string deviceid, string startingdate, string endingdate, string chiwindspeed)
        {
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataSet ds = mapper.GetMonthlyTMAS(companycode, estcode, deviceid, startingdate, endingdate, chiwindspeed);
                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }



        }

        //Mengambil Master Station
        [WebMethod]
        public string GetMasterStation()
        {
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataSet ds = mapper.MasterStation();
                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //Mengambil Status Device All
        [WebMethod]
        public string StatusDevice()
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.StatusDevice();
            return dataTableToJson(ds.Tables[0]);
        }
        //Mengambil Status Device ARS
        [WebMethod]
        public string ARSDeviceStatus()
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.ARSDeviceStatus();
            return dataTableToJson(ds.Tables[0]);
        }
        //Mengambil Status Device AWL
        [WebMethod]
        public string AWLDeviceStatus()
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.AWLDeviceStatus();
            return dataTableToJson(ds.Tables[0]);
        }

        //Mengambil Coverage
        [WebMethod]
        public string GetCoverage()
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.GetCoverage();
            return dataTableToJson(ds.Tables[0]);
        }

        //Menambahkan station
        [WebMethod]
        public void InsertStation(string stationid, string estcode, string division, string assignname, string devicetype, string latitude, string longitude, string isdefault, string isactive, string createby)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.InsertStation(stationid, estcode, division, assignname, devicetype, latitude, longitude, isdefault, isactive, createby);

        }
        //Menghapus station
        [WebMethod]
        public void DeleteStation(string stationid)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.DeleteStation(stationid);

        }
        //Mengedit station
        [WebMethod]
        public void EditStation(string stationid, string estcode, string division, string assignname, string devicetype, string latitude, string longitude, string isdefault, string isactive, string createby)
        {


            IOTMapper mapper = new IOTMapper();
            DataSet dd = mapper.DeleteStation(stationid);
            DataSet ds = mapper.InsertStation(stationid, estcode, division, assignname, devicetype, latitude, longitude, isdefault, isactive, createby);

        }

        //List Device pada assign source data 
        [WebMethod]
        public string ListSourceData(string type, string estcode)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.ListDeviceMertani(type, estcode);
            return dataTableToJson(ds.Tables[0]);

        }
        //List Project Layer Map
        [WebMethod]
        public string ListLayerMap(string estcode)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.ListProjectLayerMap(estcode);
            return dataTableToJson(ds.Tables[0]);

        }

        //Assign source data to station
        //[WebMethod]
        //public void AssignSourceData(string stationid, string deviceid, string source, string startingdate, string endingdate, string createby)
        //{


        //    IOTMapper mapper = new IOTMapper();
        //    DataSet ds = mapper.AssignSourceData(stationid, deviceid, source, startingdate, endingdate, createby);

        //}

        //List Assigned Rainfall Data Station
        [WebMethod]
        public string AssignedRainfallData(string stationid, string recordtime)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.AssignedStationRainfall(stationid, recordtime);
            return dataTableToJson(ds.Tables[0]);

        }
        //List Assigned TMAT TMAS Data Station
        [WebMethod]
        public string AssignedTmatTmasData(string stationid, string devicetype, string recordtime)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.AssignedStationTMATTMAS(stationid, devicetype, recordtime);
            return dataTableToJson(ds.Tables[0]);

        }

        //Assign source data 
        //Save Data Adjustment
        [WebMethod]
        public string AssignSourceData(string stationid, string estcode, List<jsonAssign> jsonAssign)
        {
            IOTMapper mapper = new IOTMapper();
            //for (int i = 0; i < jsonAssign.Count; i++)
            //{
            //    DataSet dl = mapper.DeleteAssingedStationData(jsonAssign[i].Station_ID, jsonAssign[i].date);
            //}
            try
            {
                //Creating dummy datatable for testing
                DataTable dt = new DataTable();
                DataColumn dc = new DataColumn("Station_ID", typeof(String));
                dt.Columns.Add(dc);

                dc = new DataColumn("sourceDeviceId", typeof(String));
                dt.Columns.Add(dc);

                dc = new DataColumn("source", typeof(String));
                dt.Columns.Add(dc);

                dc = new DataColumn("date", typeof(String));
                dt.Columns.Add(dc);

                dc = new DataColumn("createBy", typeof(String));
                dt.Columns.Add(dc);

                dc = new DataColumn("updateDate", typeof(String));
                dt.Columns.Add(dc);


                for (int i = 0; i < jsonAssign.Count; i++)
                {


                    DataRow dr = dt.NewRow();


                    dr[0] = jsonAssign[i].Station_ID;
                    dr[1] = jsonAssign[i].sourceDeviceId;
                    dr[2] = jsonAssign[i].source;
                    dr[3] = jsonAssign[i].date;
                    dr[4] = jsonAssign[i].createBy;
                    dr[5] = jsonAssign[i].updateDate;


                    //dr[3] = "coldata4";
                    dt.Rows.Add(dr);//this will add the row at the end of the datatable
                                    //OR
                }




                SqlConnection SqlConnectionObj = new SqlConnection(COR_General.connStringGISAPP);
                SqlConnectionObj.Open();
                SqlBulkCopy bulkCopy = new SqlBulkCopy(SqlConnectionObj, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null);
                bulkCopy.DestinationTableName = "T_IOT_DataMappingStation";
                bulkCopy.WriteToServer(dt);

                SqlConnectionObj.Close();
                DataSet db = mapper.AssignedSourceDataHistory(stationid,estcode);

                return "Data berhasil disimpan";

            }
            catch (Exception ex)
            {
                return ex.Message;

            }


        }

        //Delete Assigned Rainfall Data Station
        [WebMethod]
        public void DeleteAsssignedStationData(string stationid, string startingdate)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.DeleteAssingedStationData(stationid, startingdate);


        }
        //Add remarks to device
        [WebMethod]
        public void addRemarksDevice(string devicetype, string deviceid, string remarks, string createby)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet dd = mapper.deleteRemarksDevice(deviceid);
            DataSet ds = mapper.addRemarksDevice(devicetype, deviceid, remarks, createby);


        }
        //Delete remarks from device
        [WebMethod]
        public void deleteRemarksDevice(string deviceid)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.deleteRemarksDevice(deviceid);


        }
        //Mengambil Status Device yang belum terdaftar
        [WebMethod]
        public string UnregisterdDeviceMertani()
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.UnregistedMertaniDevice();
            return dataTableToJson(ds.Tables[0]);
        }
        //Mengambil RAW Data
        [WebMethod]
        public string GetRawData(string type, string deviceid, string starthours, string endhours)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.GetRawData(type, deviceid, starthours, endhours);
            return dataTableToJson(ds.Tables[0]);

        }
        //Delete All Mapping Data
        [WebMethod]
        public void DeleteAllMappingData(string stationid)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.DeleteAllMappingData(stationid);


        }


        //Mengambil Assigend Rainfall
        [WebMethod]
        public string GetRainfallAssigned(string stationid, string recordtime)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.AssignedStationRainfall(stationid, recordtime);
            return dataTableToJson(ds.Tables[0]);

        }

        ////Get TMAT/TMAS Data assigned from any sources
        //[WebMethod]
        //public string[] GetTMATTMAS(string stationid, string devicetype, string recordtime)
        //{
        //    string[] hasil = new string[2];
        //    try
        //    {
        //        IOTMapper mapper = new IOTMapper();
        //        DataSet ds = mapper.AssignedStationTMATTMAS(stationid, devicetype, recordtime);

        //        hasil[0] = dataTableToJson(ds.Tables[0]);
        //        hasil[1] = dataTableToJson(ds.Tables[1]);
        //        //hasil[2] = dataTableToJson(ds.Tables[2]);


        //    }
        //    catch (Exception ex)
        //    {
        //        hasil[0] = ex.Message;
        //    }
        //    return hasil;
        //}

        //Mengambil Assigned TMAT/TMAS Data
        [WebMethod]
        public string GetTMATTMAS(string stationid, string devicetype, string recordtime)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.AssignedStationTMATTMAS(stationid, devicetype, recordtime);
            return dataTableToJson(ds.Tables[0]);

        }



        //Mengambil data 7 hari terakhir data TMAT/TMAS semua device
        [WebMethod]
        public string Get7DaysReportAllTMATTMAS(string startingdate, string endingdate)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.Get7daysreportTMATTMAS(startingdate, endingdate);
            return dataTableToJson(ds.Tables[0]);

        }

        //Mengambil Week Temp
        [WebMethod]
        public string GetWeekTemp()
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.GetWeekTemp();
            return dataTableToJson(ds.Tables[0]);
        }

        //Mengambil data 7 hari terakhir data rainfall semua device
        [WebMethod]
        public string Get7DaysReportAllRainfall(string startingdate, string endingdate)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.Get7daysreportRainfall(startingdate, endingdate);
            return dataTableToJson(ds.Tables[0]);

        }

        //Get Manual Data from Devices
        [WebMethod]
        public string GetManualData()
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.GetManualData();
            return dataTableToJson(ds.Tables[0]);
        }

        //Get Device Maintenance Summary
        [WebMethod]
        public string GetDeviceMaintenanceSummary()
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.GetDeviceMaintenanceSummary();
            return dataTableToJson(ds.Tables[0]);
        }

        //Get Device Maintenance Detail
        [WebMethod]
        public string GetDeviceMaintenanceDetail(string deviceid)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.GetDeviceMaintenanceDetail(deviceid);
            return dataTableToJson(ds.Tables[0]);
        }

        //Add maintenance detail
        [WebMethod]
        public void addMaintenance(string maintenanceid, string deviceid, string startingdate, string endingdate, string kerusakanoperasional, string kerusakansituasional, string remarks, string createby)
        {

            IOTMapper mapper = new IOTMapper();
            //DataSet dd = mapper.deleteRemarksDevice(deviceid, devicetype);
            DataSet ds = mapper.addMaintenance(maintenanceid, deviceid, startingdate, endingdate, kerusakanoperasional, kerusakansituasional, remarks, createby);


        }

        //Add maintenance detail
        [WebMethod]
        public void updateMaintenance(string maintenanceid, string deviceid, string startingdate, string endingdate, string kerusakanoperasional, string kerusakansituasional, string remarks, string createby)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet dd = mapper.deleteMaintenance(maintenanceid);
            DataSet ds = mapper.addMaintenance(maintenanceid, deviceid, startingdate, endingdate, kerusakanoperasional, kerusakansituasional, remarks, createby);


        }

        //Add maintenance detail
        [WebMethod]
        public void deleteMaintenance(string maintenanceid)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet dd = mapper.deleteMaintenance(maintenanceid);

        }

        //Get accuracy ars
        [WebMethod]
        public string GetARSAccuracyReport(string estcode, string startingdate, string endingdate)
        {

            IOTMapper mapper = new IOTMapper();
           
            DataSet ds = mapper.GetARSAcuraccyReport(estcode, startingdate, endingdate);
            return dataTableToJson(ds.Tables[0]);

        }

        //Get watergate device

        [WebMethod]
        public string[] GetWatergate()
        {
            string[] hasil = new string[2];
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataSet ds = mapper.GetWatergate();

                hasil[0] = dataTableToJson(ds.Tables[0]);
                hasil[1] = dataTableToJson(ds.Tables[1]);


            }
            catch (Exception ex)
            {
                hasil[0] = ex.Message;
            }
            return hasil;
        }

        //Get watergate data
        [WebMethod]
        public string[] GetWatergateData(string deviceid, string startingdate, string endingdate, string estcode, string division)
        {
            string[] hasil = new string[1];
            try
            {
                IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetWatergateData(deviceid, startingdate, endingdate, estcode, division);
                hasil[0] = dataTableToJson(ds.Tables[0]);
                


            }
            catch (Exception ex)
            {
                hasil[0] = ex.Message;
            }
            return hasil;

        }

        //Get watergate saved graph
        [WebMethod]
        public string GetGraph()
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetGraph();
            return dataTableToJson(ds.Tables[0]);

        }

        //Insert graph
        [WebMethod]
        public void InsertGraph(string idgraph, string namagraph, string companycode, string createby)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.InsertGraph(idgraph, namagraph, companycode, createby);
    

        }
        //Delete graph
        [WebMethod]
        public void DeleteGraph(string idgraph)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.DeleteGraph(idgraph);


        }
        //Insert Detail graph
        [WebMethod]
        public void InsertDetailGraph(List<jsonDetailGraph> jsonDetailGraphs)
        {

            IOTMapper mapper = new IOTMapper();
            for (int i = 0; i < jsonDetailGraphs.Count; i++)
            {
                DataSet ds = mapper.InsertDetailGraph(jsonDetailGraphs[i].iddetailgraph, jsonDetailGraphs[i].idgraph, jsonDetailGraphs[i].deviceid);
            }

        }
        //Delete graph
        [WebMethod]
        public void DeleteGraphDetail(string idgraph)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.DeleteGraphDetail(idgraph);


        }
        //Get monthly rainfall
        [WebMethod]
        public string GetMonthlyMonitoringArs(string plantation, string grouping, string groupcompanyname, string companycode, string estcode, string recordtime)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetMonthlyMonitoringArs(plantation, grouping, groupcompanyname, companycode, estcode, recordtime);
            return dataTableToJson(ds.Tables[0]);

        }
        //Get monthly awl
        [WebMethod]
        public string GetMonthlyMonitoringAwl(string grouping, string estcode, string recordtime, string type)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetMonthlyMonitoringAwl(grouping, estcode, recordtime, type);
            return dataTableToJson(ds.Tables[0]);

        }

        //Get filter region
        [WebMethod]
        public string[] GetFilterRegion()
        {
            string[] hasil = new string[4];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetFilterRegion();
            hasil[0] = dataTableToJson(ds.Tables[0]);
            hasil[1] = dataTableToJson(ds.Tables[1]);
            hasil[2] = dataTableToJson(ds.Tables[2]);
            hasil[3] = dataTableToJson(ds.Tables[3]);

            return hasil;

        }

        //Insert adjusted data
        [WebMethod]
        public void InsertAdjustedDataWatergate(string deviceid, string recordtime, string adjval)
        {
            IOTMapper mapper = new IOTMapper();

            DataSet xx = mapper.InsertAdjustedDataWatergate(deviceid, recordtime, adjval);
            
        }

        //Get adjusted data watergate
        [WebMethod]
        public string GetAdjustedDataWatergate(string companycode)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetAdjustedDataWatergate(companycode);
            return dataTableToJson(ds.Tables[0]);

        }

        [WebMethod]
        public string GetLanguage()
        {
            string hasil = "";
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataTable dt = mapper.GetLanguage().Tables[0];
                hasil = dataTableToJson(dt);
            }
            catch (Exception ex)
            {
                hasil = "error " + ex.Message;
            }
            return hasil;
        }
        //Get Water Depth Indicator
        [WebMethod]
        public string GetWaterDepthIndicator()
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetWaterDepthIndicator();
            return dataTableToJson(ds.Tables[0]);

        }
        //Get AWS Source
        [WebMethod]
        public string GetAWSSource()
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetAWSSource();
            return dataTableToJson(ds.Tables[0]);

        }
        //Get Rainfall Condition AWS
        [WebMethod]
        public string[] GetRainfallAWS(string deviceid)
        {
            string[] hasil = new string[2];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetRainfallAWS(deviceid);
            hasil[0] = dataTableToJson(ds.Tables[0]);
            hasil[1] = dataTableToJson(ds.Tables[1]);
            return hasil;
        }
        //Get CH Intensity Windspeed AWS
        [WebMethod]
        public string[] GetCHIWindspeed(string deviceid, string recordtime, string chiwindspeed)
        {
            string[] hasil = new string[1];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetCHIWindspeed(deviceid, recordtime, chiwindspeed);
            hasil[0] = dataTableToJson(ds.Tables[0]);
            
            return hasil;
        }
        //Get CH Intensity Period
        [WebMethod]
        public string[] GetCHIPeriod(string deviceid, string startingdate, string endingdate, string chiwindspeed)
        {
            string[] hasil = new string[1];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetCHIPeriod(deviceid, startingdate, endingdate, chiwindspeed);
            hasil[0] = dataTableToJson(ds.Tables[0]);

            return hasil;
        }

        [WebMethod]
        public string[] GetDefisitAirTahunan(string deviceid, string recordtime)
        {
            string[] hasil = new string[1];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetDefisitAirTahunan(deviceid, recordtime);
            hasil[0] = dataTableToJson(ds.Tables[0]);

            return hasil;
        }
        [WebMethod]
        public string[] GetDefisitAirBulanan(string plantation, string grouping, string groupcompanyname, string companycode, string estcode, string deviceid, string recordtime)
        {
            string[] hasil = new string[1];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetDefisitAirBulanan(plantation, grouping, groupcompanyname, companycode, estcode, deviceid, recordtime);
            hasil[0] = dataTableToJson(ds.Tables[0]);

            return hasil;
        }

        [WebMethod]
        public string[] GetStatusServer()
        {
            string[] hasil = new string[4];
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataSet ds = mapper.GetStatusServer();
                hasil[0] = ds.Tables[0].Rows[0][0].ToString();
                hasil[1] = ds.Tables[1].Rows[0][0].ToString();

                hasil[2] = "";//cpuCounter.NextValue().ToString();
                hasil[3] = "";//ramCounter.NextValue().ToString();				
            }
            catch (Exception ex)
            {
                hasil[0] = "Error " + ex.Message;
            }
            return hasil;
        }

        //reset password

        [WebMethod]
        public string GetEmailDomain(string email)
        {
            COR_GroupMapper mapper = new COR_GroupMapper();
            DataTable dt = mapper.checkDomainUsers(email).Tables[0];

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);

                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public void ForgotPassword(string userEmail)
        {
            int passwordLength = 8;
            int alphaNumericalCharsAllowed = 2;
            String site = "";
            //if (Request.Url.AbsoluteUri.Contains(WebConfigurationManager.AppSettings["localIP"]))
            //{
            //    site = WebConfigurationManager.AppSettings["Site"];
            //}
            //else if (Request.Url.AbsoluteUri.Contains(WebConfigurationManager.AppSettings["publicIP"]))
            //{
            //    site = WebConfigurationManager.AppSettings["PublicSite"];
            //}
            //else
            //{

            //}
            site = WebConfigurationManager.AppSettings["Site"];

            COR_UserMapper mapper = new COR_UserMapper();
            DataTable dt = userObjMapper.GetUserDetailByEmail(userEmail).Tables[0];


            String finalCode = Membership.GeneratePassword(passwordLength, alphaNumericalCharsAllowed);
            String verificationCode = url.enkrips("userregistrationverificationcode_" + finalCode);
            String subject = " Lupa Kata Sandi ?";
            string username = dt.Rows[0]["UserFullName"].ToString();
            String detail = "Kami ingin Menginformasikan Anda Telah Melakukan Lupa Kata Sandi pada Sistem, Berikut Kami Sampaikan Link Untuk Merubah Kata Sandi Anda " +
                            site + "/forgotpassword.aspx?token=" + verificationCode;

            string closing = "Abaikan Pesan ini Jika Anda Tidak Ingin Merubah Password Anda";

            string EmailHeader = "Konfirmasi Lupa Kata Sandi";
            string opening = "Dengan ini Kami Memberi Tahu anda Ada 1 Notifikasi di Aplikasi EntGIS,<br /><br /> Berikut Detailnya";
            string code = "EDEFN001";
            string body = email.PopulateBodyForUserRegistrationVerification(EmailHeader, username, detail, opening, closing, code);

            email.SendHtmlFormattedEmail(null, userEmail, subject, body);
            mapper.DeleteTokenForResetPassword(verificationCode);
            mapper.InsertTokenForResetPassword(userEmail, verificationCode);


        }

        [WebMethod]
        public string CheckDateForgotPassword(string Code)
        {
            COR_userDomain domain = new COR_userDomain();
            COR_UserMapper mapper = new COR_UserMapper();
            domain.Code = Code;


            //domain.userEmail = UserEmail;
            DataTable dt = mapper.CheckDateForgotPassword(domain).Tables[0];

            return dataTableToJson(dt);
        }


        [WebMethod]
        public string ChangePasswordForgot(string Password, string Code)
        {
            COR_userDomain domain = new COR_userDomain();
            COR_UserMapper mapper = new COR_UserMapper();
            Enkripsi encrypt = new Enkripsi();

            domain.userPass = encrypt.enkrips(Password);
            domain.Code = Code;

            try
            {
                mapper.UpdatePasswordForgotPassword(domain);
                return "success";
            }
            catch (Exception ex)
            {
                return "error - " + ex.Message;
            }
        }
        //Get Ars Station 4 Weeks
        [WebMethod]
        public string[] GetArsStation4Weeks(string companycode, string endingdate, string arsiran)
        {
            string[] hasil = new string[2];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetArsStation4Weeks(companycode,endingdate,arsiran);
            hasil[0] = dataTableToJson(ds.Tables[0]);
            hasil[1] = dataTableToJson(ds.Tables[1]);
            return hasil;
        }
        //Get Station List
        [WebMethod]
        public string[] GetStationList()
        {
            string[] hasil = new string[1];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetStationList();
            hasil[0] = dataTableToJson(ds.Tables[0]);
            return hasil;
        }
        //Get Annual Rainfall Pattern
        [WebMethod]
        public string[] GetAnnualRainfall(string estcode, string recordtime)
        {
            string[] hasil = new string[2];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetAnnualRainfallPattern(estcode,recordtime);
            hasil[0] = dataTableToJson(ds.Tables[0]);
            hasil[1] = dataTableToJson(ds.Tables[1]);
            return hasil;
        }
        //Get Hourly ARS Data
        [WebMethod]
        public string[] GetHourlyArsData(string estcode, string deviceid, string paramint1, string paramint2)
        {
            string[] hasil = new string[1];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetHourlyArsData(estcode,deviceid,paramint1,paramint2);
            hasil[0] = dataTableToJson(ds.Tables[0]);
            return hasil;
        }
        //Get Station List
        [WebMethod]
        public string[] GetYearlyRainfall(string companycode, string recordtime)
        {
            string[] hasil = new string[2];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetAnnualRainfall(companycode, recordtime);
            hasil[0] = dataTableToJson(ds.Tables[0]);
            hasil[1] = dataTableToJson(ds.Tables[1]);
            return hasil;
        }

        //Get perbandingan ch IoT dan SAP
        [WebMethod]
        public string GetComparisonOfIotSAP(string deviceid, string thn, string bln)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetComparisonCHIOTnSAP(deviceid,thn, bln);
            return dataTableToJson(ds.Tables[0]);

        }

        [WebMethod]
        public string GetListAWLKLHK()
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetListAWLKLHK();
            return dataTableToJson(ds.Tables[0]);

        }

        //insert data save report KLHK summary
        [WebMethod]
        public void InsertSavedKLHKDataSummary(string param1, string param2, string createby)
        {
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.InsertSavedReportKLHKSummary(param1, param2, createby);

        }
        //insert data save report KLHK detail
        [WebMethod]
        public void InsertSavedKLHKDataDetail(List<jsonDetailKLHK> jsonDetailKLHKs)
        {
            IOTMapper mapper = new IOTMapper();
            for (int i = 0; i < jsonDetailKLHKs.Count; i++)
            {
                DataSet ds = mapper.InsertSavedReportKLHKDetail(jsonDetailKLHKs[i].paramInt1, jsonDetailKLHKs[i].paramInt2, jsonDetailKLHKs[i].param3,jsonDetailKLHKs[i].param4, jsonDetailKLHKs[i].param5);
            }
            

        }
        [WebMethod]
        public string GetSavedKLHKData()
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetSavedKLHKData();
            return dataTableToJson(ds.Tables[0]);

        }
        [WebMethod]
        public string GetAWLKLHKData(string paramInt1,string thn, string bln)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetDataKLHKAWL(paramInt1,thn,bln);
            return dataTableToJson(ds.Tables[0]);

        }
        //delete data saved klhk
        [WebMethod]
        public void DeleteSavedDataKLHK(string paramInt1)
        {
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.DeleteSavedKLHKData(paramInt1);

        }

        //Get ARS from scheduler
        [WebMethod]
        public string GetARSfromScheduler()
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetARSSourceScheduler();
            return dataTableToJson(ds.Tables[0]);

        }
        //Get AWL from scheduler
        [WebMethod]
        public string GetAWLfromScheduler()
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetAWLSourceScheduler();
            return dataTableToJson(ds.Tables[0]);

        }
        //Get AWL from scheduler
        [WebMethod]
        public string GetAWLHolykell()
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetAWLHolykellSource();
            return dataTableToJson(ds.Tables[0]);

        }
        //Get AWL from scheduler
        [WebMethod]
        public string GetAWLHolykellSourceScheduler()
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetAWLHolykellSourceScheduler();
            return dataTableToJson(ds.Tables[0]);

        }
        //Calibration Holykell
        [WebMethod]
        public string GetHolykellCalibration(string deviceid)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetHolykellCalibration(deviceid);
            return dataTableToJson(ds.Tables[0]);

        }
        [WebMethod]
        public string[] GetCalibrationSetInput()
        {

            string[] hasil = new string[2];
            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetCalibrationSetInput();
            hasil[0] = dataTableToJson(ds.Tables[0]);
            hasil[1] = dataTableToJson(ds.Tables[1]);

            return hasil;

        }
        [WebMethod]
        public void InsertHolykellCalibration(string calibrationid, string calibrationdate, string deviceid, string paramdec1, string paramdec2, string paramdec3, string paramdec4, string type, string createby, string param3, string remarks, string urlfoto)
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.InsertHolykellCalibration(calibrationid,calibrationdate,deviceid,paramdec1,paramdec2,paramdec3,paramdec4,type,createby,param3,remarks,urlfoto);

        }
        [WebMethod]
        public string GetDeviceHolykell()
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetDeviceHolykell();
            return dataTableToJson(ds.Tables[0]);

        }
        [WebMethod]
        public string GetDailyTMATHolykell(string deviceid, string recordtime)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetDailyTMATHolykell(deviceid, recordtime);
            return dataTableToJson(ds.Tables[0]);

        }
        [WebMethod]
        public string GetMonthlyTMATHolykell(string deviceid, string startingdate, string endingdate, string companycode, string rainfall)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetMonthlyTMATHolykell(deviceid, startingdate, endingdate, companycode, rainfall);
            return dataTableToJson(ds.Tables[0]);

        }
        [WebMethod]
        public string GetDailyTMASHolykell(string deviceid, string recordtime)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetDailyTMASHolykell(deviceid, recordtime);
            return dataTableToJson(ds.Tables[0]);

        }
        [WebMethod]
        public string GetMonthlyTMASHolykell(string deviceid, string startingdate, string endingdate, string companycode, string rainfall)
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetMonthlyTMASHolykell(deviceid, startingdate, endingdate, companycode, rainfall);
            return dataTableToJson(ds.Tables[0]);

        }
        public string dataTableToJson(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            jsSerializer.MaxJsonLength = Int32.MaxValue;


            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }

            return jsSerializer.Serialize(parentRow);
            


        }


        //Json kirim assigned
        public class jsonAssign
        {
            public string Station_ID { get; set; }
            public string sourceDeviceId { get; set; }
            public string source { get; set; }
            public string date { get; set; }
            public string createBy { get; set; }
            public string updateDate { get; set; }
        }
        public class jsonDetailGraph
        {
            public string iddetailgraph { get; set; }
            public string idgraph { get; set; }
            public string deviceid { get; set; }
        }

        public class jsonDetailKLHK
        {
            public string paramInt1 { get; set; }
            public string paramInt2 { get; set; }
            public string param3 { get; set; }
            public string param4 { get; set; }
            public string param5 { get; set; }
        }
    }
}
