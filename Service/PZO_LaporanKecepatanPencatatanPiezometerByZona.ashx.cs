﻿
using System;
using System.Web;
using System.Data;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using PASS.Domain;
using PASS.Mapper;
using System.Web.Services;
using PZO_PiezometerOnlineMap.Domain;
using PZO_PiezometerOnlineMap.Class;
using PZO_PiezometerOnlineMap.Mapper;
using System.Web.Script.Serialization;
using ClosedXML.Excel;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for PZO_LaporanKecepatanPencatatanPiezometerByZona
    /// </summary>
    public class PZO_LaporanKecepatanPencatatanPiezometerByZona : IHttpHandler
    {
        PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();

        public void ProcessRequest(HttpContext context)
        {
            //var EstCode = context.Request.QueryString["EstCode"];
            DownloadFile2(context, "pdf");
        }

        private void DownloadFile2(HttpContext context, string extention)
        {
            //DataSet dst = mapper.getWeekName();
            var weekName = context.Request.Params["weekName"];
            var EstCode = context.Request.Params["EstCode"];
            //string weekName = dst.Tables[0].Rows[0]["WeekName"].ToString();
            XtraReport report = XtraReport.FromFile(@"" + context.Server.MapPath("..") + "\\ReportFile\\PZO_PencatatanDataByAndroid_2.repx", true);
            DataSet ds = mapper.generateWeeklyReportV2(EstCode, weekName);

            //DataTable dt1 = ds.Tables[0];
            //DataTable dt2 = ds.Tables[1];
            //DataTable dt3 = ds.Tables[2];
            //DataTable dt4 = ds.Tables[3];
            //DataTable dt5 = ds.Tables[4];

            //DetailReportBand detail1 = report.Bands["DetailReport"] as DetailReportBand;
            //DetailReportBand detail2 = report.Bands["DetailReport1"] as DetailReportBand;
            //DetailReportBand detail3 = report.Bands["DetailReport2"] as DetailReportBand;
            //DetailReportBand detail4 = report.Bands["DetailReport3"] as DetailReportBand;
            //DetailBand detailReport = detail1.Bands["Detail1"] as DetailBand;

            //detail1.DataSource = dt1;
            //detail2.DataSource = dt2;
            //detail3.DataSource = dt3;
            //detail4.DataSource = dt4;
            ////report.DataSource = ds;

            //XRTable xrTable1 = detailReport.Controls["xrTable1"] as XRTable;
            //XRTableRow xrTableRow1 = xrTable1.Controls["xrTableRow1"] as XRTableRow;
            //XRTableCell xrTableCell10 = xrTableRow1.Controls["xrTableCell10"] as XRTableCell;
            //xrTableCell10.BeforePrint += xrTableCell10_BeforePrint;

            //GroupHeaderBand gHeader = detail4.Bands["GroupHeader7"] as GroupHeaderBand;
            //DetailBand detailBand = detail4.Bands["Detail4"] as DetailBand;
            //GroupFooterBand gFooter = detail4.Bands["GroupFooter4"] as GroupFooterBand;

            //if (dt4.Rows.Count > 0)
            //{
            //    gFooter.Visible = false;
            //    gHeader.Visible = true;
            //    detailBand.Visible = true;
            //}
            //else
            //{
            //    gFooter.Visible = true;
            //    gHeader.Visible = false;
            //    detailBand.Visible = false;
            //}

            ////DateTime today = DateTime.Now;
            //report.Parameters["estate"].Value = dt5.Rows[0]["NamaKebun"];
            //report.Parameters["company"].Value = dt5.Rows[0]["NamaPT"];
            //report.Parameters["periode"].Value = dt5.Rows[0]["MonthNames"];


            DetailBand detail = report.Bands["Detail"] as DetailBand;
            DetailReportBand detail1 = report.Bands["DetailReport"] as DetailReportBand;
            DetailReportBand detail2 = report.Bands["DetailReport1"] as DetailReportBand;
            DetailReportBand detail3 = report.Bands["DetailReport4"] as DetailReportBand;
            DetailReportBand detail4 = report.Bands["DetailReport3"] as DetailReportBand;

            DetailBand detail1band = detail1.Bands["Detail1"] as DetailBand;
            XRTable xrtable2 = detail1band.Controls["xrTable2"] as XRTable;
            XRTableRow xrTableRow2 = xrtable2.Controls["xrTableRow2"] as XRTableRow;
            XRTableCell xrTableCell6 = xrTableRow2.Controls["xrTableCell6"] as XRTableCell;
            XRTableCell xrTableCell9 = xrTableRow2.Controls["xrTableCell9"] as XRTableCell;
            XRTableCell xrTableCell10 = xrTableRow2.Controls["xrTableCell10"] as XRTableCell;
            //detail.
            detail1.DataSource = ds.Tables[0];
            detail2.DataSource = ds.Tables[5];
            detail4.DataSource = ds.Tables[2];
            detail3.DataSource = ds.Tables[3];
            DataTable dt3 = ds.Tables[3];
            DataTable dt4 = ds.Tables[4];

            xrTableCell10.BeforePrint += xrTableCell10_BeforePrint;
            xrTableCell9.BeforePrint += xrTableCell9_BeforePrint;


            GroupHeaderBand gHeader = detail3.Bands["GroupHeader6"] as GroupHeaderBand;
            DetailBand detailBand = detail3.Bands["Detail5"] as DetailBand;
            GroupFooterBand gFooter = detail3.Bands["GroupFooter6"] as GroupFooterBand;

            if (dt3.Rows.Count > 0)
            {
                gFooter.Visible = false;
                gHeader.Visible = true;
                detailBand.Visible = true;
            }
            else
            {
                gFooter.Visible = true;
                gHeader.Visible = false;
                detailBand.Visible = false;
            }

            report.Parameters["Pt"].Value = dt4.Rows[0]["NamaPT"];
            report.Parameters["KebunX"].Value = dt4.Rows[0]["NamaKebun"];
            report.Parameters["Weekname"].Value = dt4.Rows[0]["MonthNames"];



            foreach (Band band in report.Bands)
            {
                if (band is DetailReportBand)
                {
                    ((DetailReportBand)band).DataMember = "Table1";
                }
            }

            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string[] newWeekName = weekName.Split('W');
            string weekaname2 = "W" + newWeekName[1].ToString().Substring(0, 1) + newWeekName[1].ToString().Substring(1, 2) + newWeekName[1].ToString().Substring(5, 2);
            string filename = weekaname2 + "_" + EstCode + "_RPT1a_LaporanPencatatanPiezometerByZona_" + unixTimestamp.ToString() + ".pdf";
            //W40721
            //W1082021

            //string filename = "RPT_LaporanPencatatanPiezometerByZona_" + EstCode + "_" + WeekName + ".pdf";

            DownloadFile(report, filename, extention, context);
        }


        void xrTableCell10_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            if (dataRow["selisihKetinggian2"].ToString() != null && dataRow["selisihKetinggian2"].ToString() != "")
            {
                int ketinggian = Convert.ToInt32(dataRow["selisihKetinggian2"].ToString());
                if (ketinggian >= 5 && ketinggian < 7)
                {
                    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F50057");
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF00");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian > 7)
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F50057");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian == 7)
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffaa66");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
            }
        }
        void xrTableCell9_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            if (dataRow["ketinggian"].ToString() != null && dataRow["ketinggian"].ToString() != "")
            {
                int ketinggian = Convert.ToInt32(dataRow["ketinggian"].ToString());
                if (ketinggian >= 0 && ketinggian <= 44)
                {
                    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F50057");
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#1950A0");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian >= 45 && ketinggian <= 49)
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#55C4F9");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian >= 50 && ketinggian <= 70)
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#55701F");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian >= 71 && ketinggian <= 75)
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFD53");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian >= 76)
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#990000");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                //else if (ketinggian > 80)
                //{
                //    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#990000");
                //    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                //}
                else
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
            }
        }



        private void DownloadFile(XtraReport report, string namepdf, string extention, HttpContext context)
        {
            string prefixfilenames = DateTime.Now.ToString("ddMMyyyy_HHmm");
            var ext = extention;
            if (ext == "excel")
            {
                ext = "xlsx";
            }

            //string filename = namepdf;

            byte[] bytesInStream = null;
            string fullpath = context.Server.MapPath("..") + "\\Attachment\\" + namepdf;
            //string filepath = context.Server.MapPath("..") + @"\Attachment\" + filename;
            MemoryStream ms = new MemoryStream();
            if (extention.ToLower().Equals("pdf"))
            {
                report.ExportToPdf(fullpath);
            }
            else
            {
                report.ExportToXlsx(fullpath);
            }
            bytesInStream = ms.ToArray();
            ms.Close();

            context.Response.ClearContent();
            context.Response.ClearHeaders();
            context.Response.ContentType = "application/" + extention.ToLower();
            context.Response.AddHeader("content-disposition", "attachment; filename=" + namepdf);
            //context.Response.BinaryWrite(bytesInStream);
            context.Response.TransmitFile(fullpath);
            context.Response.Flush();
            context.Response.Close();
            FileInfo fo = new FileInfo(fullpath);
            if (fo.Exists)
            {
                fo.Delete();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}