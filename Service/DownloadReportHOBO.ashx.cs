﻿
using System;
using System.Web;
using System.Data;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using PASS.Domain;
using PASS.Mapper;
using System.Web.Services;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for DownloadReportHOBO
    /// </summary>
    public class DownloadReportHOBO : IHttpHandler
    {


        public void ProcessRequest(HttpContext context)
        {
            var companycode = context.Request.QueryString["CompanyCode"];
            var Periode = context.Request.QueryString["Periode"];
            var Year = context.Request.QueryString["Year"];
            DownloadFile(companycode, Periode, Year, context, "excel");
        }

        private void DownloadFile(string companycode, string Periode, string Year, HttpContext context, string extention)
        {
            string prefixfilenames = DateTime.Now.ToString("ddMMyyyy_HHmm");
            var ext = extention;
            if (ext == "excel")
            {
                ext = "xlsx";
            }
            if(Periode == "1")
            {
                Periode = "Jan-Mar";
            }
            else if(Periode == "4")
            {
                Periode = "Apr-Jun";
            }
            else if (Periode == "7")
            {
                Periode = "Jul-Sep";
            }
            else if (Periode == "10")
            {
                Periode = "Okt-Des";
            }
            string filename = companycode + "_" + Periode + "_" + Year + ".xlsx";

            byte[] bytesInStream = null;
            string fullpath = context.Server.MapPath("..") + "\\Attachment\\ReportHOBO\\" + filename;
            MemoryStream ms = new MemoryStream();

            bytesInStream = ms.ToArray();
            ms.Close();

            context.Response.ClearContent();
            context.Response.ClearHeaders();
            context.Response.ContentType = "application/" + extention.ToLower();
            context.Response.AddHeader("content-disposition", "attachment; filename=" + filename);
            //context.Response.BinaryWrite(bytesInStream);
            context.Response.TransmitFile(fullpath);
            context.Response.Flush();
            context.Response.Close();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}