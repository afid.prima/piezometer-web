﻿using System;
using System.Data;
using System.Linq;
using System.Web.Services;
using PASS.Domain;
using PASS.Mapper;
using PZO_PiezometerOnlineMap.Class;
using PZO_PiezometerOnlineMap.Mapper;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using System.IO;
using System.Web.Configuration;
using System.Web.Security;
using PZO_PiezometerOnlineMap.Domain;
using ClosedXML.Excel;
using System.Web;
using pass.huruf;
using PASS.Email;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for MapService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MapService : System.Web.Services.WebService
    {

        private pass.huruf.Enkripsi login = new pass.huruf.Enkripsi();
        private COR_UserMapper userObjMapper = new COR_UserMapper();
        private COR_userDomain domain = new COR_userDomain();
        private pass.huruf.Enkripsi url = new pass.huruf.Enkripsi();
     
        private readonly Email email = new Email();

        protected string dataTableToJson(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string[] ListLayer(string EstCode)
        {


            string[] hasil = new string[14];
            try
            {
                PZO_ProjectMapper mapper = new PZO_ProjectMapper();
                DataTable dt = mapper.ListLayer(EstCode);
                int jmlvector = 0;
                if (dt.Rows.Count > 0)
                {
                    hasil[0] = dt.Rows[0]["LayerDisplayName"].ToString();
                    hasil[1] = dt.Rows[0]["LayerCategory"].ToString();
                    hasil[2] = dt.Rows[0]["LayerURL"].ToString();
                    hasil[3] = dt.Rows[0]["LayerURLName"].ToString();
                    hasil[4] = dt.Rows[0]["LayerDisplay"].ToString();
                    hasil[5] = dt.Rows[0]["TypeName"].ToString();
                    hasil[6] = dt.Rows[0]["FieldID"].ToString();

                    hasil[8] = dt.Rows[0]["TreeName"].ToString();
                    hasil[9] = dt.Rows[0]["TreeStatus"].ToString();

                    hasil[10] = dt.Rows[0]["LayerCode"].ToString();
                    hasil[11] = dt.Rows[0]["LayerModule"].ToString();
                    hasil[12] = dt.Rows[0]["LayerStatus"].ToString();
                    hasil[13] = dt.Rows[0]["LayerID"].ToString();

                    if (dt.Rows[0]["LayerCategory"].ToString() == "vector") jmlvector++;

                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        hasil[0] = hasil[0] + ";" + dt.Rows[i]["LayerDisplayName"].ToString();
                        hasil[1] = hasil[1] + ";" + dt.Rows[i]["LayerCategory"].ToString();
                        hasil[2] = hasil[2] + ";" + dt.Rows[i]["LayerURL"].ToString();
                        hasil[3] = hasil[3] + ";" + dt.Rows[i]["LayerURLName"].ToString();
                        hasil[4] = hasil[4] + ";" + dt.Rows[i]["LayerDisplay"].ToString();
                        hasil[5] = hasil[5] + ";" + dt.Rows[i]["TypeName"].ToString();
                        hasil[6] = hasil[6] + ";" + dt.Rows[i]["FieldID"].ToString();

                        hasil[8] = hasil[8] + ";" + dt.Rows[i]["TreeName"].ToString();
                        hasil[9] = hasil[9] + ";" + dt.Rows[i]["TreeStatus"].ToString();

                        hasil[10] = hasil[10] + ";" + dt.Rows[i]["LayerCode"].ToString();
                        hasil[11] = hasil[11] + ";" + dt.Rows[i]["LayerModule"].ToString();
                        hasil[12] = hasil[12] + ";" + dt.Rows[i]["LayerStatus"].ToString();
                        hasil[13] = hasil[13] + ";" + dt.Rows[i]["LayerID"].ToString();

                        if (dt.Rows[i]["LayerCategory"].ToString() == "vector") jmlvector++;
                    }
                    hasil[7] = jmlvector.ToString();
                }

            }
            catch (Exception ex)
            {

            }
            return hasil;
        }


      

        [WebMethod]
        public string[] GetLayerField(string FieldID, string idxLayerFitur, string idxInfo, string url, string firstinfo)
        {
            string[] result = new string[6];
            try
            {
                PZO_ProjectMapper mapper = new PZO_ProjectMapper();
                DataTable dt = mapper.GetLayerField(int.Parse(FieldID));
                result[0] = dt.Rows.Count.ToString();
                if (dt.Rows.Count > 0)
                {
                    result[0] = dt.Rows[0]["FieldName"].ToString();
                    result[4] = dt.Rows[0]["FieldNameAlias"].ToString();
                }
                for (int i = 1; i < dt.Rows.Count; i++)
                {
                    result[0] = result[0] + ";" + dt.Rows[i]["FieldName"].ToString();
                    result[4] = result[4] + ";" + dt.Rows[i]["FieldNameAlias"].ToString();
                }

                result[1] = idxLayerFitur;
                result[2] = idxInfo;
                result[3] = url;
                result[5] = firstinfo;
            }
            catch (Exception ex)
            {
                result[0] = ex.Message.ToString();
            }
            return result;
        }

        [WebMethod]
        public string[] GetFieldLayer(string fieldID, string idxLayerFitur, string idxInfo, string url, string firstinfo)
        {

            string[] hasil = new string[6];
            try
            {
                OLM_OnlineMapper obj = new OLM_OnlineMapper();
                DataSet ds = obj.GetFieldLayer(fieldID);
                hasil[0] = ds.Tables[0].Rows.Count.ToString();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    hasil[0] = ds.Tables[0].Rows[0]["FieldName"].ToString();
                    hasil[4] = ds.Tables[0].Rows[0]["FieldNameAlias"].ToString();
                }
                for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
                {
                    hasil[0] = hasil[0] + ";" + ds.Tables[0].Rows[i]["FieldName"].ToString();
                    hasil[4] = hasil[4] + ";" + ds.Tables[0].Rows[i]["FieldNameAlias"].ToString();
                }

                hasil[1] = idxLayerFitur;
                hasil[2] = idxInfo;
                hasil[3] = url;
                hasil[5] = firstinfo;
            }
            catch (Exception ex)
            {
                hasil[0] = ex.Message.ToString();
            }
            return hasil;
        }

        [WebMethod]
        public string ListEstShortName(string projectid)
        {
            string hasil = "";
            try
            {
                OLM_OnlineMapper obj = new OLM_OnlineMapper();
                DataSet ds = obj.ListEstShortName(projectid);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    hasil = ds.Tables[0].Rows[0]["estShortname"].ToString();

                    for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
                    {
                        hasil = hasil + ";" + ds.Tables[0].Rows[i]["estShortname"].ToString();
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return hasil;
        }

        [WebMethod]
        public string ListBlock(string tableblock)
        {
            string hasil = "";
            try
            {
                OLM_OnlineMapper obj = new OLM_OnlineMapper();
                DataSet ds = obj.ListBlock(tableblock);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    hasil = ds.Tables[0].Rows[0]["Block"].ToString();

                    for (int i = 1; i < ds.Tables[0].Rows.Count; i++)
                    {
                        hasil = hasil + ";" + ds.Tables[0].Rows[i]["Block"].ToString();
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return hasil;
        }

        [WebMethod]
        public string SetWMS(string layerid, string layerurl, string layername)
        {
            string hasil = "";
            try
            {
                OLM_OnlineMapper obj = new OLM_OnlineMapper();
                obj.ChangeLayerID(layerurl, layername, layerid);
            }
            catch (Exception ex)
            {
                hasil = "error";
            }
            return hasil;
        }

        [WebMethod]
        public string[] ListCompany()
        {
            //COR_CompanyMapper mapper = new COR_CompanyMapper();
            //DataView dv = mapper.ListData(1).Tables[0].DefaultView;
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataView dv = mapper.ListCompany().DefaultView;
            DataTable dtListCompany = dv.ToTable(true, "CompanyCode", "CompanyName", "CompanyShortname");

            string[] list = new string[3];
            list[0] = "";
            list[1] = "";
            list[2] = "";
            foreach (DataRow row in dtListCompany.Rows)
            {
                if (list[0] == "")
                    list[0] = row["CompanyCode"].ToString();
                else
                    list[0] += ";" + row["CompanyCode"].ToString();

                if (list[1] == "")
                    list[1] = row["CompanyName"].ToString();
                else
                    list[1] += ";" + row["CompanyName"].ToString();

                if (list[2] == "")
                    list[2] = row["CompanyShortname"].ToString();
                else
                    list[2] += ";" + row["CompanyShortname"].ToString();
            }

            return list;
        }

        [WebMethod]
        public string[] ListCompanyKLHK()
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataView dv = mapper.ListCompanyKLHK().DefaultView;
            DataTable dtListCompany = dv.ToTable(true, "CompanyCode", "CompanyName", "CompanyShortname");

            string[] list = new string[3];
            list[0] = "";
            list[1] = "";
            list[2] = "";
            foreach (DataRow row in dtListCompany.Rows)
            {
                if (list[0] == "")
                    list[0] = row["CompanyCode"].ToString();
                else
                    list[0] += ";" + row["CompanyCode"].ToString();

                if (list[1] == "")
                    list[1] = row["CompanyName"].ToString();
                else
                    list[1] += ";" + row["CompanyName"].ToString();

                if (list[2] == "")
                    list[2] = row["CompanyShortname"].ToString();
                else
                    list[2] += ";" + row["CompanyShortname"].ToString();
            }

            return list;
        }

        [WebMethod]
        public string[] ListEstate(string CompanyCode)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            var q = from myRow in mapper.ListEstate().AsEnumerable()
                    where myRow.Field<string>("CompanyCode").Contains(CompanyCode)
                    select myRow;

            DataView dv = q.CopyToDataTable().DefaultView;
            //DataTable dtListEstate = dv.ToTable(true, "EstNewCode", "NewEstName");
            DataTable dtListEstate = dv.ToTable(true, "estCode", "NewEstName");

            string[] list = new string[2];
            list[0] = "";
            list[1] = "";
            foreach (DataRow row in dtListEstate.Rows)
            {
                if (list[0] == "")
                    list[0] = row["estCode"].ToString();
                else
                    list[0] += ";" + row["estCode"].ToString();

                if (list[1] == "")
                    list[1] = row["NewEstName"].ToString();
                else
                    list[1] += ";" + row["NewEstName"].ToString();
            }

            return list;
        }

        [WebMethod]
        public string ListAllBlock(string EstCode)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.ListAllBlock(EstCode).Tables[0];
            return DataTableToJSON(dt);

        }

        [WebMethod]
        public string ListAllEstate()
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.ListAllEstate().Tables[0];
            return DataTableToJSON(dt);
        }

        [WebMethod]
        public string ListAfdeling(string EstCode)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.ListAfdeling(EstCode).Tables[0];
            return DataTableToJSON(dt);

        }

        [WebMethod]
        public string ListBlockTMAT(string afdeling)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.ListBlock(afdeling).Tables[0];
            return DataTableToJSON(dt);

        }
        [WebMethod]
        public string ListTMAT(string block)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.ListTMAT(block).Tables[0];
            return DataTableToJSON(dt);

        }

        [WebMethod]
        public string ListBlockByEstate(string EstCode)
        {
            string result = "";
            try
            {
                PZO_ProjectMapper mapper = new PZO_ProjectMapper();
                DataTable dt = mapper.ListBlockByEstate(EstCode);
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0]["Block"].ToString();

                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        result = result + ";" + dt.Rows[i]["Block"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return result;
        }

        [WebMethod]
        public string GetBlockExtent(string EstCode, string Block)
        {
            string result = "";
            try
            {
                PZO_ProjectMapper mapper = new PZO_ProjectMapper();
                DataTable dt = mapper.GetBlockExtent(EstCode, Block);
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0]["MinX"].ToString() + ";" + dt.Rows[0]["MinY"].ToString() + ";" + dt.Rows[0]["MaxX"].ToString() + ";" + dt.Rows[0]["MaxY"].ToString();
                }

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return result;
        }

        [WebMethod]
        public string ChangePassword(string UserName, string OldPassword, string NewPassword)
        {
            Language lang = new Language();
            Cryptography crpyto = new Cryptography();
            COR_userDomain domain = new COR_userDomain();
            COR_UserMapper mapper = new COR_UserMapper();

            string oldPass = "", newPass = "";

            if (NewPassword.Length > 6)
            {
                oldPass = crpyto.Encrypt(OldPassword);
                newPass = crpyto.Encrypt(NewPassword);

                DataSet detail = mapper.GetUserDetailByUsername(UserName);
                if (detail.Tables[0].Rows.Count == 1)
                {
                    if (oldPass == detail.Tables[0].Rows[0]["UserPass"].ToString())
                    {
                        domain.userName = UserName;
                        domain.userPass = newPass;
                        mapper.UpdatePassword(domain);

                        return "success";
                    }
                    else
                        return "Wrong input for old password !";
                }
                else
                {
                    return "Username does not exist";//return lang.Translate("COR25039", Session["language"].ToString()); //COR25039
                }
            }
            else
            {
                return "Password length must be > 6 characters";//return lang.Translate("COR25040", Session["language"].ToString()); //COR25040
            }
        }

        [WebMethod]
        public string GetPiezoRecordDetailByPieRecordID(string PieRecordID)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetPiezoRecordDetailByPieRecordID(PieRecordID);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetWeekFromID(string ID)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetWeekFromID(int.Parse(ID));

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetListPiezometer(string EstCode, string WeekID)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetListPiezometer(EstCode, WeekID);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetPreviousWeekFromParameter(string Week, string Month, string Year)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetPreviousWeekFromParameter(int.Parse(Week), int.Parse(Month), int.Parse(Year));

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetNextWeekFromParameter(string Week, string Month, string Year)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetNextWeekFromParameter(int.Parse(Week), int.Parse(Month), int.Parse(Year));

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetDataPiezometer(string CompanyCode, string EstCode, string StartDate, string EndDate)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetDataPiezometer(CompanyCode, EstCode, DateTime.Parse(StartDate), DateTime.Parse(EndDate));

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            jsSerializer.MaxJsonLength = Int32.MaxValue;
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string[] GetStatusServer()
        {
            string[] hasil = new string[4];
            try
            {
                PZO_ProjectMapper mapper = new PZO_ProjectMapper();
                DataSet ds = mapper.GetStatusServer();
                hasil[0] = ds.Tables[0].Rows[0][0].ToString();
                hasil[1] = ds.Tables[1].Rows[0][0].ToString();

                hasil[2] = "";//cpuCounter.NextValue().ToString();
                hasil[3] = "";//ramCounter.NextValue().ToString();				
            }
            catch (Exception ex)
            {
                hasil[0] = "Error " + ex.Message;
            }
            return hasil;
        }

        [WebMethod]
        public string GetLanguage()
        {
            string hasil = "";
            try
            {
                PZO_ProjectMapper mapper = new PZO_ProjectMapper();
                DataTable dt = mapper.GetLanguage().Tables[0];
                hasil = dataTableToJson(dt);
            }
            catch (Exception ex)
            {
                hasil = "error " + ex.Message;
            }
            return hasil;
        }

        [WebMethod]
        public string GetEmailDomain(string email)
        {
            COR_GroupMapper mapper = new COR_GroupMapper();
            DataTable dt = mapper.checkDomainUsers(email).Tables[0];

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);

                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string ForgotPassword(string userEmail)
        {

            try
            {

            int passwordLength = 8;
            int alphaNumericalCharsAllowed = 2;
            string site = "";
            site = WebConfigurationManager.AppSettings["Site"];

            COR_UserMapper mapper = new COR_UserMapper();
            DataTable dt = userObjMapper.GetUserDetailByEmail(userEmail).Tables[0];


            string finalCode = Membership.GeneratePassword(passwordLength, alphaNumericalCharsAllowed);
            string verificationCode = url.enkrips("userregistrationverificationcode_" + finalCode);
            string subject = " Lupa Kata Sandi ?";
            string username = dt.Rows[0]["UserFullName"].ToString();
            string detail = "Kami ingin Menginformasikan Anda Telah Melakukan Lupa Kata Sandi pada Sistem, Berikut Kami Sampaikan Link Untuk Merubah Kata Sandi Anda " +
                            site + "https://app.gis-div.com/pzo/forgotpassword.aspx?token=" + verificationCode;

            string closing = "Abaikan Pesan ini Jika Anda Tidak Ingin Merubah Password Anda";

            string EmailHeader = "Konfirmasi Lupa Kata Sandi";
            string opening = "Dengan ini Kami Memberi Tahu anda Ada 1 Notifikasi di Aplikasi PZO,<br /><br /> Berikut Detailnya";
            string code = "EDEFN001";
            string body = email.PopulateBodyForUserRegistrationVerification(EmailHeader, username, detail, opening, closing, code);

            email.SendHtmlFormattedEmail(null, userEmail, subject, body);
                return "";
                //mapper.DeleteTokenForResetPassword(verificationCode);
            //mapper.InsertTokenForResetPassword(userEmail, verificationCode);


            }
            catch (Exception e)
            {
                return e.Message;

            }

        }

        [WebMethod]
        public string CheckDateForgotPassword(string Code)
        {


            COR_userDomain domain = new COR_userDomain();
            COR_UserMapper mapper = new COR_UserMapper();
            domain.Code = Code;
            

            try
            {
                //domain.userEmail = UserEmail;
                DataTable dt = mapper.CheckDateForgotPassword(domain).Tables[0];

                return dataTableToJson(dt);
            }
            catch (Exception ex)
            {
                return " ";
            }

           
        }


        [WebMethod]
        public string ChangePasswordForgot(string Password, string Code)
        {
            COR_userDomain domain = new COR_userDomain();
            COR_UserMapper mapper = new COR_UserMapper();
            Enkripsi encrypt = new Enkripsi();

            domain.userPass = encrypt.enkrips(Password);
            domain.Code = Code;

            try
            {
                mapper.UpdatePasswordForgotPassword(domain);
                return "success";
            }
            catch (Exception ex)
            {
                return "error - " + ex.Message;
            }
        }


        [WebMethod]
        public string GetDataKLHK(string CompanyCode, string EstCode, string StartDate, string EndDate)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetDataKLHK(CompanyCode, EstCode, DateTime.Parse(StartDate), DateTime.Parse(EndDate));

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetListWeek()
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetListWeek();

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetProgressForDashboard(string CompanyCode, string EstCode)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();

            DataTable dt = new DataTable();
            if (CompanyCode == "")
                dt = mapper.GetProgressForDashboardPerCompany(EstCode);
            else
                dt = mapper.GetProgressForDashboard(CompanyCode, EstCode);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetDataProgressForDashboard(string CompanyCode, string EstCode, int AdjustmentValue)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            //generate dulu datanya
            //mapper.GenerateDataProgressForDashboard();

            DataTable dt = mapper.GetDataProgressForDashboard(CompanyCode, EstCode, AdjustmentValue);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetDataPengukuranForDashboard(string CompanyCode, string EstCode, int AdjustmentValue, string Keterangan)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();

            DataTable dt = mapper.GetDataPengukuranForDashboard(CompanyCode, EstCode, AdjustmentValue, Keterangan);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        //[WebMethod]
        //public string GenerateDataReport(int AdjustmentValue)
        //{
        //    PZO_ProjectMapper mapper = new PZO_ProjectMapper();

        //    DataTable dt = mapper.GenerateDataReport(AdjustmentValue);

        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //    List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        //    Dictionary<string, object> childRow;
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        childRow = new Dictionary<string, object>();
        //        foreach (DataColumn col in dt.Columns)
        //        {
        //            childRow.Add(col.ColumnName, row[col]);
        //        }
        //        parentRow.Add(childRow);
        //    }
        //    return jsSerializer.Serialize(parentRow);
        //}

        [WebMethod]
        public string GenerateDataReport(int AdjustmentValue, int userID)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();

            DataTable dt = mapper.GenerateDataReport(AdjustmentValue, userID);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetDataSumberData()
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();

            DataTable dt = mapper.GetDataSumberData();

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GenerateWeeklyReport(string EstCode, string WeekName)
        {
            PZO_PiezometerMapper mapper = new PZO_PiezometerMapper();
            XtraReport report = XtraReport.FromFile(Server.MapPath("..") + @"\ReportFile\RPT_LaporanPencatatanPiezometer.repx", true);
            DataSet ds = mapper.GetLaporanPencatatanPiezometer_ForManager(EstCode, WeekName);

            //report.DataSource = ds;

            //DateTime today = DateTime.Now;
            ////report.Parameters["printdate"].Value = today.ToString("dd/MM/yyyy HH:mm:ss");

            //int DetailReportBand = 0;
            //foreach (Band band in report.Bands)
            //{
            //    if (band is DetailReportBand)
            //    {
            //        if (DetailReportBand == 0)
            //        {
            //            ((DetailReportBand)band).DataSource = ds.Tables[0];
            //            ((DetailReportBand)band).DataMember = "Table1";
            //        }
            //        else if (DetailReportBand == 1)
            //        {
            //            ((DetailReportBand)band).DataSource = ds.Tables[1];
            //            ((DetailReportBand)band).DataMember = "Table1";
            //        }
            //        else if (DetailReportBand == 2)
            //        {
            //            ((DetailReportBand)band).DataSource = ds.Tables[2];
            //            ((DetailReportBand)band).DataMember = "Table1";
            //        }
            //        DetailReportBand++;
            //    }
            //}

            DataTable dt1 = ds.Tables[0];
            DataTable dt2 = ds.Tables[1];
            DataTable dt3 = ds.Tables[2];
            DataTable dt4 = ds.Tables[3];
            DataTable dt5 = ds.Tables[4];

            DetailReportBand detail1 = report.Bands["DetailReport"] as DetailReportBand;
            DetailBand detailBands1 = detail1.Bands["Detail1"] as DetailBand;

            DetailReportBand detail2 = report.Bands["DetailReport1"] as DetailReportBand;
            DetailReportBand detail3 = report.Bands["DetailReport2"] as DetailReportBand;
            DetailReportBand detail4 = report.Bands["DetailReport3"] as DetailReportBand;

            detail1.DataSource = dt1;
            detail2.DataSource = dt2;
            detail3.DataSource = dt3;
            detail4.DataSource = dt4;
            report.DataSource = ds;

            GroupHeaderBand gHeader = detail4.Bands["GroupHeader5"] as GroupHeaderBand;
            DetailBand detailBand = detail4.Bands["Detail4"] as DetailBand;            
            GroupFooterBand gFooter = detail4.Bands["GroupFooter3"] as GroupFooterBand;

            XRTable xrTable = detailBands1.Controls["XrTable1"] as XRTable;
            XRTableRow xrTableRow1 = xrTable.Controls["XrTableRow1"] as XRTableRow;

            XRTableCell xrTableCell3 = xrTableRow1.Controls["XrTableCell3"] as XRTableCell;


            //if (dt4.Rows.Count > 0)
            //{
            //    gFooter.Visible = false;
            //    gHeader.Visible = true;
            //    detailBand.Visible = true;
            //}
            //else
            //{
            //    gFooter.Visible = true;
            //    gHeader.Visible = false;
            //    detailBand.Visible = false;
            //}

            DateTime today = DateTime.Now;
            report.Parameters["estate"].Value = dt5.Rows[0]["NamaKebun"];
            report.Parameters["company"].Value = dt5.Rows[0]["NamaPT"];
            report.Parameters["periode"].Value = dt5.Rows[0]["MonthNames"];
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string[] newWeekName = WeekName.Split('W');
            string weekaname2 = "W" + newWeekName[1].ToString().Substring(0, 1) + newWeekName[1].ToString().Substring(1, 2) + newWeekName[1].ToString().Substring(5, 2);
            //string filename = weekaname2 + "_" + ds.Tables[6].Rows[0]["EstCodeSAP"] + "_RPT1a_LaporanPencatatanPiezometerByZona_" + unixTimestamp.ToString() + ".pdf";
            xrTableCell3.BeforePrint += xrTableCell3_BeforePrint;

            foreach (Band band in report.Bands)
            {
                if (band is DetailReportBand)
                {
                    ((DetailReportBand)band).DataMember = "Table1";
                }
            }

            string filename = "RPT_LaporanPencatatanPiezometer_" + EstCode + "_" + WeekName + ".pdf";
            string filepath = Server.MapPath("..") + @"\Attachment\" + filename;

            if (File.Exists(filepath))
            {
                File.Delete(filepath);
            }

            report.ExportToPdf(filepath);

            return filename;
        }
        [WebMethod]
        public string GenerateReportPerubahanPencatatanTMAT(string wmArea, string WeekName, string weekLabel, int klhk, string estCode, int flagAllEstate, int flagAllZona, string pulau, int flagAllPulau,  string txtParamPulau, string txtParamKebun, string txtParamZona )
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
             XtraReport report = XtraReport.FromFile(Server.MapPath("..") + @"\ReportFile\PZO_LaporanPencatatanPerubahanTMAT.repx", true);
            //XtraReport report = XtraReport.FromFile(Server.MapPath("..") + @"\ReportFile\PZO_LaporanPencatatanPerubahanTMAT.repx", true);
            DataSet ds = new DataSet();
            DataSet ds2 = new DataSet();
            string klhkLabel = "";
            string blockstring = "";
            if (klhk == 1)
            {
                klhkLabel = " - Klhk";
                ds = mapper.getPencatatanPerubahanPiezometerKLHK(wmArea, WeekName, estCode, pulau);
            }
            else if (klhk == 0)
            {
                klhkLabel = " - Standar";
                ds = mapper.getPencatatanPerubahanPiezometer(wmArea, WeekName, estCode, pulau);
            }
            else if (klhk == 2)
            {
                klhkLabel = " - All";
                ds = mapper.getPencatatanPerubahanPiezometerAll(wmArea, WeekName, estCode, pulau);
            }
            //if (klhk == 1)
            //{
            //    ds = mapper.getPencatatanPerubahanPiezometerKLHK(wmArea, WeekName, estCode,pulau);
            //    klhkLabel = "-KLHK";                
            //}
            //else
            //{
            //    ds = mapper.getPencatatanPerubahanPiezometer(wmArea, WeekName,estCode, pulau);                
            //}

            //DataSet ds = mapper.generateWeeklyReportV2("THP4", "W5072021");

            DetailBand detail = report.Bands["Detail"] as DetailBand;
            DetailReportBand detail1 = report.Bands["DetailReport"] as DetailReportBand;
            DetailReportBand detail2 = report.Bands["DetailReport1"] as DetailReportBand;
            DetailReportBand detail3 = report.Bands["DetailReport2"] as DetailReportBand;
            //DetailBand detailBand3 = detail3.Bands["Detail3"] as DetailBand;

            DetailBand detail1band = detail1.Bands["Detail1"] as DetailBand;
            //detail.
            detail1.DataSource = ds.Tables[1];
            detail2.DataSource = ds.Tables[0];
            //detail3.DataSource = ds2.Tables[0];

            GroupHeaderBand groupHeader = detail1.Bands["GroupHeader1"] as GroupHeaderBand;
            XRTable xrTable2 = detail1band.Controls["xrTable2"] as XRTable;
            XRTableRow xrTableRow3 = xrTable2.Controls["xrTableRow3"] as XRTableRow;
            GroupHeaderBand groupHeader2 = detail3.Bands["GroupHeader2"] as GroupHeaderBand;
            XRLabel xrLabel3 = groupHeader2.Controls["xrLabel3"] as XRLabel;

            XRTableCell xrTableCell12 = xrTableRow3.Controls["xrTableCell12"] as XRTableCell;
            XRTableCell xrTableCell13 = xrTableRow3.Controls["xrTableCell13"] as XRTableCell;
            XRTableCell xrTableCell14 = xrTableRow3.Controls["xrTableCell14"] as XRTableCell;
            XRTableCell xrTableCell15 = xrTableRow3.Controls["xrTableCell15"] as XRTableCell;
            XRTableCell xrTableCell17 = xrTableRow3.Controls["xrTableCell17"] as XRTableCell;
            XRTableCell xrTableCell18 = xrTableRow3.Controls["xrTableCell18"] as XRTableCell;
            //XRTableCell xrTableCell36 = xrTableRow3.Controls["xrTableCell36"] as XRTableCell;

            xrTableCell12.BeforePrint += XRTableCell12BeforePrint;
            xrTableCell13.BeforePrint += xrTableCell13BeforePrint;
            xrTableCell14.BeforePrint += xrTableCell14BeforePrint;
            xrTableCell15.BeforePrint += xrTableCell15BeforePrint;
            xrTableCell17.BeforePrint += xrTableCell17BeforePrint;
            xrTableCell18.BeforePrint += xrTableCell18BeforePrint;
            //xrTableCell36.BeforePrint += xrTableCell36BeforePrint;





            report.Parameters["Periode"].Value = weekLabel+klhkLabel;

            if (flagAllPulau == 1)
            {
                report.Parameters["coverage"].Value = "All Pulau";
            }
            else
            {
                //report.Parameters["coverage"].Value = wmArea + klhkLabel;
                report.Parameters["coverage"].Value = mapper.getMultiWMArea(pulau).Tables[0].Rows[0]["WmAreaName"].ToString();
            }

            if (flagAllZona == 1)
            {
                report.Parameters["Zona"].Value = "All Zona";
            }
            else
            {
                report.Parameters["Zona"].Value = txtParamZona;
            }


            if (flagAllEstate == 1)
            {
                report.Parameters["Estate"].Value = "All Estate";
            }
            else
            {
                report.Parameters["Estate"].Value = txtParamKebun;
            }

            //report.Parameters["KebunX"].Value = dt4.Rows[0]["NamaKebun"];
            //report.Parameters["Weekname"].Value = dt4.Rows[0]["MonthNames"];



            foreach (Band band in report.Bands)
            {
                if (band is DetailReportBand)
                {
                    ((DetailReportBand)band).DataMember = "DataTable1";
                }
            }
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string[] newWeekName = WeekName.Split('W');
            //string weekaname2 = "W" + newWeekName[1].ToString().Substring(0, 1) + newWeekName[1].ToString().Substring(1, 2) + newWeekName[1].ToString().Substring(5, 2);
            //string filename = weekaname2 + "_" + ds.Tables[6].Rows[0]["EstCodeSAP"] + "_RPT1a_LaporanPencatatanPerubahanTMAT_" + unixTimestamp.ToString() + ".pdf";
            string filename = "RPT1b_LaporanPencatatanPerubahanTMAT_" + unixTimestamp.ToString() + ".pdf";
            //W40721
            //W1082021

            string filepath = Server.MapPath("..") + @"\Attachment\" + filename;

            if (File.Exists(filepath))
            {
                File.Delete(filepath);
            }

            report.ExportToPdf(filepath);

            return filename;
        }

        [WebMethod]
        public string GenerateReportPerbandingan(string wmArea, string WeekName, string weekLabel, int klhk, string estCode, int flagAllEstate, int flagAllZona, string pulau, int flagAllPulau, string txtParamPulau, string txtParamKebun, string txtParamZona)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            XtraReport report = XtraReport.FromFile(Server.MapPath("..") + @"\ReportFile\PZO_PetaPerbandingan.repx", true);
            //XtraReport report = XtraReport.FromFile(Server.MapPath("..") + @"\ReportFile\PZO_LaporanPencatatanPerubahanTMAT.repx", true);
            DataSet ds = new DataSet();
            DataSet ds2 = new DataSet();
            string klhkLabel = "";
            string blockstring = "";
            if (klhk == 1)
            {
                klhkLabel = " - Klhk";
                ds = mapper.getPencatatanPerubahanPiezometerKLHK(wmArea, WeekName, estCode, pulau);
            }
            else if (klhk == 0)
            {
                klhkLabel = " - Standar";
                ds = mapper.getPencatatanPerubahanPiezometer(wmArea, WeekName, estCode, pulau);
            }
            else if (klhk == 2)
            {
                klhkLabel = " - All";
                ds = mapper.getPencatatanPerubahanPiezometerAll(wmArea, WeekName, estCode, pulau);
            }
            //if (klhk == 1)
            //{
            //    ds = mapper.getPencatatanPerubahanPiezometerKLHK(wmArea, WeekName, estCode,pulau);
            //    klhkLabel = "-KLHK";                
            //}
            //else
            //{
            //    ds = mapper.getPencatatanPerubahanPiezometer(wmArea, WeekName,estCode, pulau);                
            //}

            //DataSet ds = mapper.generateWeeklyReportV2("THP4", "W5072021");

            DetailBand detail = report.Bands["Detail"] as DetailBand;
            DetailReportBand detail1 = report.Bands["DetailReport"] as DetailReportBand;
            DetailReportBand detail2 = report.Bands["DetailReport1"] as DetailReportBand;
            DetailReportBand detail3 = report.Bands["DetailReport2"] as DetailReportBand;
            //DetailBand detailBand3 = detail3.Bands["Detail3"] as DetailBand;

            DetailBand detail1band = detail1.Bands["Detail1"] as DetailBand;
            //detail.
            detail1.DataSource = ds.Tables[1];
            detail2.DataSource = ds.Tables[0];
            //detail3.DataSource = ds2.Tables[0];

            GroupHeaderBand groupHeader = detail1.Bands["GroupHeader1"] as GroupHeaderBand;
            XRTable xrTable2 = detail1band.Controls["xrTable2"] as XRTable;
            XRTableRow xrTableRow3 = xrTable2.Controls["xrTableRow3"] as XRTableRow;
            GroupHeaderBand groupHeader2 = detail3.Bands["GroupHeader2"] as GroupHeaderBand;
            XRLabel xrLabel3 = groupHeader2.Controls["xrLabel3"] as XRLabel;

            XRTableCell xrTableCell12 = xrTableRow3.Controls["xrTableCell12"] as XRTableCell;
            XRTableCell xrTableCell13 = xrTableRow3.Controls["xrTableCell13"] as XRTableCell;
            XRTableCell xrTableCell14 = xrTableRow3.Controls["xrTableCell14"] as XRTableCell;
            XRTableCell xrTableCell15 = xrTableRow3.Controls["xrTableCell15"] as XRTableCell;
            XRTableCell xrTableCell17 = xrTableRow3.Controls["xrTableCell17"] as XRTableCell;
            XRTableCell xrTableCell18 = xrTableRow3.Controls["xrTableCell18"] as XRTableCell;
            //XRTableCell xrTableCell36 = xrTableRow3.Controls["xrTableCell36"] as XRTableCell;

            xrTableCell12.BeforePrint += XRTableCell12BeforePrint;
            xrTableCell13.BeforePrint += xrTableCell13BeforePrint;
            xrTableCell14.BeforePrint += xrTableCell14BeforePrint;
            xrTableCell15.BeforePrint += xrTableCell15BeforePrint;
            xrTableCell17.BeforePrint += xrTableCell17BeforePrint;
            xrTableCell18.BeforePrint += xrTableCell18BeforePrint;
            //xrTableCell36.BeforePrint += xrTableCell36BeforePrint;





            report.Parameters["Periode"].Value = weekLabel + klhkLabel;

            if (flagAllPulau == 1)
            {
                report.Parameters["coverage"].Value = "All Pulau";
            }
            else
            {
                //report.Parameters["coverage"].Value = wmArea + klhkLabel;
                report.Parameters["coverage"].Value = mapper.getMultiWMArea(pulau).Tables[0].Rows[0]["WmAreaName"].ToString();
            }

            if (flagAllZona == 1)
            {
                report.Parameters["Zona"].Value = "All Zona";
            }
            else
            {
                report.Parameters["Zona"].Value = txtParamZona;
            }


            if (flagAllEstate == 1)
            {
                report.Parameters["Estate"].Value = "All Estate";
            }
            else
            {
                report.Parameters["Estate"].Value = txtParamKebun;
            }

            //report.Parameters["KebunX"].Value = dt4.Rows[0]["NamaKebun"];
            //report.Parameters["Weekname"].Value = dt4.Rows[0]["MonthNames"];



            foreach (Band band in report.Bands)
            {
                if (band is DetailReportBand)
                {
                    ((DetailReportBand)band).DataMember = "DataTable1";
                }
            }
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string[] newWeekName = WeekName.Split('W');
            //string weekaname2 = "W" + newWeekName[1].ToString().Substring(0, 1) + newWeekName[1].ToString().Substring(1, 2) + newWeekName[1].ToString().Substring(5, 2);
            //string filename = weekaname2 + "_" + ds.Tables[6].Rows[0]["EstCodeSAP"] + "_RPT1a_LaporanPencatatanPerubahanTMAT_" + unixTimestamp.ToString() + ".pdf";
            string filename = "RPT1b_LaporanPencatatanPerubahanTMAT_" + unixTimestamp.ToString() + ".pdf";
            //W40721
            //W1082021

            string filepath = Server.MapPath("..") + @"\Attachment\" + filename;

            if (File.Exists(filepath))
            {
                File.Delete(filepath);
            }

            report.ExportToPdf(filepath);

            return filename;
        }

        [WebMethod]
        public string GenerateWeeklyReportV2(string EstCode, string WeekName)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            //XtraReport report = XtraReport.FromFile(Server.MapPath("..") + @"\ReportFile\RPT_LaporanPencatatanPiezometer.repx", true);
            XtraReport report = XtraReport.FromFile(Server.MapPath("..") + @"\ReportFile\PZO_PencatatanDataByAndroid_2.repx", true);
            DataSet ds = mapper.generateWeeklyReportV2(EstCode, WeekName);
            //DataSet ds = mapper.generateWeeklyReportV2("THP4", "W5072021");

            DetailBand detail = report.Bands["Detail"] as DetailBand;
            DetailReportBand detail1 = report.Bands["DetailReport"] as DetailReportBand;
            DetailReportBand detail2 = report.Bands["DetailReport1"] as DetailReportBand;
            //DetailReportBand detail3 = report.Bands["DetailReport2"] as DetailReportBand;
            DetailReportBand detail3 = report.Bands["DetailReport3"] as DetailReportBand;
            DetailReportBand detail4 = report.Bands["DetailReport4"] as DetailReportBand;

            DetailBand detail1band = detail1.Bands["Detail1"] as DetailBand;
            XRTable xrtable2 = detail1band.Controls["xrTable2"] as XRTable;
            XRTableRow xrTableRow2 = xrtable2.Controls["xrTableRow2"] as XRTableRow;
            XRTableCell xrTableCell6 = xrTableRow2.Controls["xrTableCell6"] as XRTableCell;
            XRTableCell xrTableCell9 = xrTableRow2.Controls["xrTableCell9"] as XRTableCell;
            XRTableCell xrTableCell10 = xrTableRow2.Controls["xrTableCell10"] as XRTableCell;
            GroupFooterBand groupFooter6 = detail4.Bands["GroupFooter6"] as GroupFooterBand;
            //detail.
            detail1.DataSource = ds.Tables[0];
            detail2.DataSource = ds.Tables[5];
            detail4.DataSource = ds.Tables[3];
            detail3.DataSource = ds.Tables[2];
            DataTable dt4 = ds.Tables[4];

            xrTableCell10.BeforePrint += xrTableCell10_BeforePrint;
            xrTableCell9.BeforePrint += xrTableCell9_BeforePrint;


            report.Parameters["Pt"].Value = dt4.Rows[0]["NamaPT"];
            report.Parameters["KebunX"].Value = dt4.Rows[0]["NamaKebun"];
            report.Parameters["Weekname"].Value = dt4.Rows[0]["MonthNames"];

            if (ds.Tables[3].Rows.Count > 0)
            {
                groupFooter6.Visible = false;
            }


            foreach (Band band in report.Bands)
            {
                if (band is DetailReportBand)
                {
                    ((DetailReportBand)band).DataMember = "DataTable1";
                }
            }
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string[] newWeekName = WeekName.Split('W');
            string weekaname2 = "W" + newWeekName[1].ToString().Substring(0, 1) + newWeekName[1].ToString().Substring(1, 2) + newWeekName[1].ToString().Substring(5, 2);
            string filename = weekaname2 + "_" + ds.Tables[6].Rows[0]["EstCodeSAP"] + "_RPT1a_LaporanPencatatanPiezometerByZona_" + unixTimestamp.ToString() + ".pdf";
            //W40721
            //W1082021

            string filepath = Server.MapPath("..") + @"\Attachment\" + filename;

            if (File.Exists(filepath))
            {
                File.Delete(filepath);
            }

            report.ExportToPdf(filepath);

            return filename;
        }
        
        [WebMethod]
        public string GetDateByWeekName(string WeekName)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();

            DataTable dt = mapper.GetDateByWeekName(WeekName);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        [WebMethod]
        public string GetMasterZonabyWmArea(int idWmArea)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();

            DataTable dt = mapper.GetMasterZonaByWmArea(idWmArea);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        [WebMethod]
        public string GetGraphByWmAreaCode(int idWmArea, string wmaCode)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetGraphByZona(idWmArea, wmaCode);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod(EnableSession = true)]
        public string SaveUploadPath(string FileName, string Path, string companyCode, string logger, string periode, string year)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            var uname = Session["uID"].ToString();
            var filename = FileName;
            var path = Server.MapPath("~/" + "Attachment/ImportFile/HOBO/" + filename);
            string UploadDate = DateTime.Now.ToString("yyyyMMdd");
            //var PathFile = Path;
            try
            {
                mapper.SavePathUpload(uname, path, filename, UploadDate, companyCode, logger, periode, year);
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod(EnableSession = true)]
        public string SaveUploadPathExcel(string FileName, string Path, string companyCode, string logger, string periode, string year)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            var uname = Session["uID"].ToString();
            var filename = FileName;
            var path = Server.MapPath("~/" + "Attachment/ImportFileExcel/" + filename);
            string UploadDate = DateTime.Now.ToString("yyyyMMdd");
            //var PathFile = Path;
            try
            {
                mapper.SavePathUpload(uname, path, filename, UploadDate, companyCode, logger, periode, year);
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod]
        public string ListMonth(string Estcode)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.ListMonthTemp(Estcode).Tables[0];
            return DataTableToJSON(dt);
        }

        [WebMethod]
        public string ListLogger(string CompanyCode)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.ListLogger(CompanyCode).Tables[0];
            return DataTableToJSON(dt);
        }

        private string DataTableToJSON(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetDataGenerateKLHK(string CompanyCode, string EstCode, string Periode, string Year)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            if (EstCode == "")
            {
                var _estCode = mapper.GetEstate(CompanyCode);
                EstCode = _estCode;
            }

            //string status = mapper.checkDataHasApproved(CompanyCode, EstCode, Periode, Year);
            //if(status == "true")
            //{
            //    DataTable dt = mapper.GetDataApproved(CompanyCode, EstCode, Periode, Year);
            //}
            //else
            //{
            DataTable dt = mapper.GetDataGenerateKLHK(CompanyCode, EstCode, Periode, Year);
            //}


            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetDataBlockRehab(string CompanyCode)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetDataBlockRehab(CompanyCode);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }


        //[WebMethod(EnableSession = true)]
        //public string UpdateGenerateKLHK(string PieRecordID, string WeekName, int HasilGenerate)
        //{
        //    PZO_ProjectMapper mapper = new PZO_ProjectMapper();
        //    var userID = Convert.ToInt32(Session["uID"].ToString());
        //    //var PathFile = Path;
        //    try
        //    {
        //        mapper.UpdateGenerateKLHK(PieRecordID, WeekName, HasilGenerate, userID);
        //        return "success";
        //    }
        //    catch (Exception ex)
        //    {
        //        return ex.Message;
        //    }
        //}
        [WebMethod(EnableSession = true)]
        public string UpdateGenerateKLHK(string dataobject)
        {
            Absent domainBase = new Absent();
            WeekList domainList = new Domain.WeekList();
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            var obj = new JavaScriptSerializer().Deserialize<Absent>(dataobject);
            domainBase.month = obj.month;
            domainBase.year = obj.year;
            var userID = Convert.ToInt32(Session["uID"].ToString());
            var _msg = "";
            //for (int i = 0; i < obj.ListWeek.Count; i++)
            //{
            //    week = obj.ListWeek[i].Date;
            //    mapper.DeleteGenerateKLHK(week, domainBase.estCode);
            //}

            for (int i = 0; i < obj.ListPiezo.Count; i++)
            {


                //dt.Rows.Add(Lastid + i, obj.ListPiezo[i].PieRecordID, obj.ListPiezo[i].HasilGenerate,
                //    obj.ListPiezo[i].Date, userID, CreatedDate, domainBase.month, domainBase.year, 0, null, null
                //    );
                //week = obj.ListPiezo[i].Date;
                try
                {
                    mapper.UpdateGenerateKLHK(obj.ListPiezo[i].PieRecordID, obj.ListPiezo[i].Date, Convert.ToInt32(obj.ListPiezo[i].HasilGenerate), userID);
                    _msg = "success";
                }
                catch (Exception ex)
                {
                    _msg = ex.Message;
                }
            }
            //try
            //{
            //    bulkData.BulkInsertDataTable("T_PZO_GenerateDataKLHK", dt);
            //    return "success";
            //}
            //catch (Exception ex)
            //{
            //    return ex.Message;
            //}
            return _msg;
        }

        [WebMethod(EnableSession = true)]
        public string SubmitFinalDataKLHK(string CompanyCode, string EstCode, string Periode)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            var userID = Convert.ToInt32(Session["uID"].ToString());
            if (EstCode == "")
            {
                var _estCode = mapper.GetEstate(CompanyCode);
                EstCode = _estCode;
            }
            //var PathFile = Path;
            try
            {
                mapper.SubmitFinalDataKLHK(CompanyCode, Periode, EstCode, userID);
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod(EnableSession = true)]
        public int GetUserID()
        {
            var userID = Convert.ToInt32(Session["uID"].ToString());
            return userID;
        }

        [WebMethod]
        public string GetDataLastWeek(string CompanyCode, string EstCode, int Month)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper(); if (EstCode == "")
            {
                var _estCode = mapper.GetEstate(CompanyCode);
                EstCode = _estCode;
            }
            DataTable dt = mapper.GetDataLastWeek(CompanyCode, EstCode, Month);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod(EnableSession = true)]
        public string GenerateKLHK(string dataobject)
        {
            Absent domainBase = new Absent();
            WeekList domainList = new Domain.WeekList();
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            var obj = new JavaScriptSerializer().Deserialize<Absent>(dataobject);
            domainBase.estCode = obj.estCode;
            domainBase.companyCode = obj.companyCode;
            domainBase.month = obj.month;
            domainBase.year = obj.year;

            BulkData bulkData = new BulkData();
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID", typeof(Int64)));
            dt.Columns.Add(new DataColumn("PieRecordID", typeof(string)));
            dt.Columns.Add(new DataColumn("HasilGenerate", typeof(string)));
            dt.Columns.Add(new DataColumn("Date", typeof(string)));
            dt.Columns.Add(new DataColumn("CreatedBy", typeof(string)));
            dt.Columns.Add(new DataColumn("CreatedDate", typeof(string)));
            dt.Columns.Add(new DataColumn("Month", typeof(string)));
            dt.Columns.Add(new DataColumn("Year", typeof(string)));
            dt.Columns.Add(new DataColumn("Status", typeof(string)));
            dt.Columns.Add(new DataColumn("ApprovedBy", typeof(string)));
            dt.Columns.Add(new DataColumn("ApprovedDate", typeof(DateTime)));
            var userID = Convert.ToInt32(Session["uID"].ToString());
            string CreatedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var week = "";
            var Lastid = mapper.GetLastIdGenerate();
            if (domainBase.estCode == "")
            {
                var _estCode = mapper.GetEstate(domainBase.companyCode);
                domainBase.estCode = _estCode;
            }
            for (int i = 0; i < obj.ListWeek.Count; i++)
            {
                week = obj.ListWeek[i].Date;
                mapper.DeleteGenerateKLHK(week, domainBase.estCode);
            }

            for (int i = 0; i < obj.ListPiezo.Count; i++)
            {
                dt.Rows.Add(Lastid + i, obj.ListPiezo[i].PieRecordID, obj.ListPiezo[i].HasilGenerate,
                    obj.ListPiezo[i].Date, userID, CreatedDate, domainBase.month, domainBase.year, 0, null, null
                    );
                week = obj.ListPiezo[i].Date;
            }
            try
            {
                bulkData.BulkInsertDataTable("T_PZO_GenerateDataKLHK", dt);
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        [WebMethod(EnableSession = true)]
        public string GetModuleAccess()
        {
            var userID = Convert.ToInt32(Session["uID"].ToString());
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            var mdlAccCode = mapper.GetModuleAccess(userID);
            return mdlAccCode;
        }


        [WebMethod]
        public string GetDoc(string CompanyCode, string Periode, string Year)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetHOBODoc(CompanyCode, Periode, Year);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetExcelDoc(string CompanyCode, string Periode, string Year)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetExcelDoc(CompanyCode, Periode, Year);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetExcelDetail(string logger, string Year, string Periode)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetExcelDetail(logger, Periode, Year);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }


        [WebMethod]
        public string CheckNullData(string CompanyCode, string EstCode, string Periode, string Year)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            if (EstCode == "")
            {
                var _estCode = mapper.GetEstate(CompanyCode);
                EstCode = _estCode;
            }
            DataTable dt = mapper.CheckNullData(CompanyCode, EstCode, Periode, Year);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }


        [WebMethod]
        public string CheckDataHasApprovedAll(string CompanyCode, string EstCode, string Periode, string Year)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            if (EstCode == "")
            {
                var _estCode = mapper.GetEstate(CompanyCode);
                EstCode = _estCode;
            }
            DataTable dt = mapper.CheckDataHasApprovedAll(CompanyCode, EstCode, Periode, Year);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod(EnableSession = true)]
        public string SaveBlockRehab(string dataobject)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            //var PathFile = Path;
            var obj = new JavaScriptSerializer().Deserialize<Absent>(dataobject);
            BulkData bulkData = new BulkData();
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Block", typeof(string)));
            dt.Columns.Add(new DataColumn("EstCode", typeof(string)));
            dt.Columns.Add(new DataColumn("Status", typeof(string)));
            var estcode = "";
            for (int i = 0; i < obj.ListBlock.Count; i++)
            {
                var _estCode = mapper.GetEstateByBlock(obj.ListBlock[i].Block);
                estcode = _estCode;
                dt.Rows.Add(obj.ListBlock[i].Block, estcode, 1
                    );
            }
            try
            {
                bulkData.BulkInsertDataTable("T_PZO_TEMP_DATAKLHK", dt);
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod(EnableSession = true)]
        public string ClearBlockRehab()
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            //var PathFile = Path;
            try
            {
                mapper.ClearBlockRehab();
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [WebMethod(EnableSession = true)]
        public string RemoveBlock(string EstCode, string Block)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            //var PathFile = Path;
            try
            {
                mapper.RemoveBlock(EstCode, Block);
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }



        [WebMethod]
        public string CheckDataThisMonth(string CompanyCode, string EstCode, string Periode, string Year)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            if (EstCode == "")
            {
                var _estCode = mapper.GetEstate(CompanyCode);
                EstCode = _estCode;
            }
            DataTable dt = mapper.CheckDataThisMonth(CompanyCode, EstCode, Periode, Year);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string DownloadReportHOBO(string CompanyCode, string Periode, string Year)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataSet ds = mapper.CreatedReport(CompanyCode, Periode, Year);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataView dv = ds.Tables[0].DefaultView;
                DataTable dtHeader = dv.ToTable(true, "MonthName", "TanggalData");
                DataTable dtLogger = dv.ToTable(true, "Logger");

                using (var workbook = new XLWorkbook())
                {
                    var worksheet = workbook.Worksheets.Add("Sample Sheet");
                    worksheet.Cell("A1").Value = "Logger";
                    worksheet.Range("A1:A2").Column(1).Merge().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    worksheet.Range("A1:A2").Column(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell("B1").Value = "Bulan";
                    worksheet.Cell("B1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell("B2").Value = "Tanggal";
                    worksheet.Cell("B2").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                    //Writing Columns Name in Excel Sheet
                    int r1 = 3; // Initialize Excel Row Start Position = 4
                    int r2 = 2; // Initialize Excel Row Start Position = 4
                    int r3 = 3; // Initialize Excel Row Start Position = 4
                    int r4 = 3; // Initialize Excel Row Start Position = 4
                    int c1 = 3; // Initialize Data Start Position = 4
                    int c2 = 3;
                    int colstart = 3;
                    var logger = ds.Tables[0].Rows[0]["Logger"].ToString();
                    var _monthname = dtHeader.Rows[0]["MonthName"].ToString();

                    for (int col = 0; col < dtHeader.Rows.Count; col++)
                    {
                        if (_monthname != dtHeader.Rows[col]["MonthName"].ToString())
                        {
                            worksheet.Cell(1, colstart).Value = _monthname;
                            _monthname = dtHeader.Rows[col]["MonthName"].ToString();
                            //worksheet.Range(r1, c1 + col).Merge();
                            // Merge cells for title
                            worksheet.Range(worksheet.Cell(1, colstart), worksheet.Cell(1, c1 + col - 1)).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                            colstart = c1 + col;
                        }
                        worksheet.Cell(r2, c1 + col).Value = dtHeader.Rows[col]["TanggalData"].ToString();

                    }
                    worksheet.Cell(1, colstart).Value = _monthname;
                    worksheet.Range(worksheet.Cell(1, colstart), worksheet.Cell(1, c1 + dtHeader.Rows.Count - 1)).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Columns("3:" + (c1 + dtHeader.Rows.Count)).Width = 4;

                    for (int col = 0; col < ds.Tables[0].Rows.Count; col++)
                    {
                        if (logger != ds.Tables[0].Rows[col]["Logger"].ToString())
                        {
                            logger = ds.Tables[0].Rows[col]["Logger"].ToString();
                            r3 = r3 + 1;
                            r4 = 3;
                        }

                        worksheet.Cell(r3, r4).Value = ds.Tables[0].Rows[col]["TMAT"].ToString();
                        if (ds.Tables[0].Rows[col]["TMAT"].ToString() != "")
                        {
                            if (Convert.ToInt32(ds.Tables[0].Rows[col]["TMAT"].ToString()) >= -40)
                            {
                                worksheet.Cell(r3, r4).Style.Fill.BackgroundColor = XLColor.ForestGreen;
                            }
                            else if (Convert.ToInt32(ds.Tables[0].Rows[col]["TMAT"].ToString()) < -40)
                            {
                                worksheet.Cell(r3, r4).Style.Fill.BackgroundColor = XLColor.PastelRed;
                            }
                            else if (Convert.ToInt32(ds.Tables[0].Rows[col]["TMAT"].ToString()) > 0)
                            {
                                worksheet.Cell(r3, r4).Style.Fill.BackgroundColor = XLColor.BeauBlue;
                            }
                        }




                        worksheet.Cell(r3, c1 + dtHeader.Rows.Count - 1).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        r4++;

                        if (_monthname != ds.Tables[0].Rows[col]["MonthName"].ToString())
                        {
                            _monthname = ds.Tables[0].Rows[col]["MonthName"].ToString();
                            worksheet.Cell(r3, r4 - 1).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        }



                    }

                    for (int col = 0; col < dtLogger.Rows.Count; col++)
                    {
                        if (logger != dtLogger.Rows[col]["Logger"].ToString())
                        {
                            logger = dtLogger.Rows[col]["Logger"].ToString();
                        }
                        worksheet.Cell(r1 + col, 1).Value = dtLogger.Rows[col]["Logger"].ToString();
                        worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(r1 + col, 1)).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(r1 + col, 1)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(r1 + col, 1)).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(3, 1), worksheet.Cell(r1 + col, 1)).Style.Border.RightBorder = XLBorderStyleValues.Thin;

                        worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(r1 + col, 2)).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(r1 + col, 2)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(r1 + col, 2)).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(r1 + col, 2)).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                    }

                    worksheet.Cell(3, 2).Value = "TMAT";

                    worksheet.Range(worksheet.Cell(r3, 1), worksheet.Cell(r3, c1 + dtHeader.Rows.Count - 1)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(3 + dtLogger.Rows.Count - 1, 2)).Merge().Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                    worksheet.Range(worksheet.Cell(3, 2), worksheet.Cell(3 + dtLogger.Rows.Count - 1, 2)).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                    worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(1, c1 + dtHeader.Rows.Count - 1)).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                    worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(1, c1 + dtHeader.Rows.Count - 1)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                    worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(1, c1 + dtHeader.Rows.Count - 1)).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                    worksheet.Range(worksheet.Cell(2, 1), worksheet.Cell(1, c1 + dtHeader.Rows.Count - 1)).Style.Border.RightBorder = XLBorderStyleValues.Thin;

                    worksheet.Cell(1, c1 + dtHeader.Rows.Count).Value = ">-40";
                    worksheet.Cell(1, c1 + dtHeader.Rows.Count).Style.Fill.BackgroundColor = XLColor.ForestGreen;
                    worksheet.Range(worksheet.Cell(1, c1 + dtHeader.Rows.Count), worksheet.Cell(2, c1 + dtHeader.Rows.Count)).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell(1, c1 + dtHeader.Rows.Count).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

                    worksheet.Cell(1, c1 + dtHeader.Rows.Count + 1).Value = "<-40";
                    worksheet.Cell(1, c1 + dtHeader.Rows.Count + 1).Style.Fill.BackgroundColor = XLColor.PastelRed;
                    worksheet.Range(worksheet.Cell(1, c1 + dtHeader.Rows.Count + 1), worksheet.Cell(2, c1 + dtHeader.Rows.Count + 1)).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell(1, c1 + dtHeader.Rows.Count + 1).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

                    worksheet.Cell(1, c1 + dtHeader.Rows.Count + 2).Value = "0";
                    worksheet.Cell(1, c1 + dtHeader.Rows.Count + 2).Style.Fill.BackgroundColor = XLColor.BeauBlue;
                    worksheet.Range(worksheet.Cell(1, c1 + dtHeader.Rows.Count + 2), worksheet.Cell(2, c1 + dtHeader.Rows.Count + 2)).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell(1, c1 + dtHeader.Rows.Count + 2).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

                    worksheet.Cell(1, c1 + dtHeader.Rows.Count + 3).Value = "no data";
                    worksheet.Range(worksheet.Cell(1, c1 + dtHeader.Rows.Count + 3), worksheet.Cell(2, c1 + dtHeader.Rows.Count + 3)).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell(1, c1 + dtHeader.Rows.Count + 3).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

                    for (int col = 0; col < dtLogger.Rows.Count; col++)
                    {
                        //worksheet.Cell(r2, c1 + col).Value = dtHeader.Rows[col]["TanggalData"].ToString();
                        worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count).FormulaA1 = "=COUNTIF(" + "C" + Convert.ToString(3 + col) + ":" + Test(c1 + dtHeader.Rows.Count - 1) + Convert.ToString(3 + col) + ",\"> -40\")-COUNTIF(" + "C" + Convert.ToString(3 + col) + ":" + Test(c1 + dtHeader.Rows.Count - 1) + Convert.ToString(3 + col) + ",\"> -40\")+COUNTIF(" + "C" + Convert.ToString(3 + col) + ":" + Test(c1 + dtHeader.Rows.Count - 1) + Convert.ToString(3 + col) + ",\"> -40\")";
                        worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count + 1).FormulaA1 = "=COUNTIF(" + "C" + Convert.ToString(3 + col) + ":" + Test(c1 + dtHeader.Rows.Count - 1) + Convert.ToString(3 + col) + ",\"< -40\")";
                        worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count + 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count + 2).FormulaA1 = "=COUNTIF(" + "C" + Convert.ToString(3 + col) + ":" + Test(c1 + dtHeader.Rows.Count - 1) + Convert.ToString(3 + col) + ",\"> 0\")";
                        worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count + 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count + 3).FormulaA1 = "=COUNTBLANK(" + "C" + Convert.ToString(3 + col) + ":" + Test(c1 + dtHeader.Rows.Count - 1) + Convert.ToString(3 + col) + ")";
                        worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count + 3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                        worksheet.Range(worksheet.Cell(1 + col, c1 + dtHeader.Rows.Count), worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count + 3)).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(1 + col, c1 + dtHeader.Rows.Count), worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count + 3)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(1 + col, c1 + dtHeader.Rows.Count), worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count + 3)).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(1 + col, c1 + dtHeader.Rows.Count), worksheet.Cell(3 + col, c1 + dtHeader.Rows.Count + 3)).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                    }



                    worksheet.Cell(r3 + 1, 1).Value = ">-40";
                    worksheet.Cell(r3 + 1, 1).Style.Fill.BackgroundColor = XLColor.ForestGreen;
                    worksheet.Range(worksheet.Cell(r3 + 1, 1), worksheet.Cell(r3 + 1, 2)).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell(r3 + 1, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell(r3 + 2, 1).Value = "<-40";
                    worksheet.Cell(r3 + 2, 1).Style.Fill.BackgroundColor = XLColor.PastelRed;
                    worksheet.Range(worksheet.Cell(r3 + 2, 1), worksheet.Cell(r3 + 2, 2)).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell(r3 + 2, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell(r3 + 3, 1).Value = "0";
                    worksheet.Cell(r3 + 3, 1).Style.Fill.BackgroundColor = XLColor.BeauBlue;
                    worksheet.Range(worksheet.Cell(r3 + 3, 1), worksheet.Cell(r3 + 3, 2)).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell(r3 + 3, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell(r3 + 4, 1).Value = "no data";
                    worksheet.Range(worksheet.Cell(r3 + 4, 1), worksheet.Cell(r3 + 4, 2)).Merge().Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    worksheet.Cell(r3 + 4, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                    for (int col = 0; col < dtHeader.Rows.Count; col++)
                    {
                        //worksheet.Cell(r2, c1 + col).Value = dtHeader.Rows[col]["TanggalData"].ToString();
                        worksheet.Cell(r3 + 1, 3 + col).FormulaA1 = "=COUNTIF(" + Test(col + 3) + "3:" + Test(col + 3) + Convert.ToString(r3) + ",\"> -40\")-COUNTIF(" + Test(col + 3) + "3:" + Test(col + 3) + Convert.ToString(r3) + ",\"> 0\")+COUNTIF(" + Test(col + 3) + "3:" + Test(col + 3) + Convert.ToString(r3) + ",\"< -40\")";
                        worksheet.Cell(r3 + 1, 3 + col).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        worksheet.Cell(r3 + 2, 3 + col).FormulaA1 = "=COUNTIF(" + Test(col + 3) + "3:" + Test(col + 3) + Convert.ToString(r3) + ",\"< -40\")";
                        worksheet.Cell(r3 + 2, 3 + col).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        worksheet.Cell(r3 + 3, 3 + col).FormulaA1 = "=COUNTIF(" + Test(col + 3) + "3:" + Test(col + 3) + Convert.ToString(r3) + ",\"> 0\")";
                        worksheet.Cell(r3 + 3, 3 + col).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        worksheet.Cell(r3 + 4, 3 + col).FormulaA1 = "=COUNTBLANK(" + Test(col + 3) + "3:" + Test(col + 3) + Convert.ToString(r3) + ")";
                        worksheet.Cell(r3 + 4, 3 + col).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;

                        worksheet.Range(worksheet.Cell(r3 + 1, 1 + col), worksheet.Cell(r3 + 4, 3 + col)).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(r3 + 1, 1 + col), worksheet.Cell(r3 + 4, 3 + col)).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(r3 + 1, 1 + col), worksheet.Cell(r3 + 4, 3 + col)).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        worksheet.Range(worksheet.Cell(r3 + 1, 1 + col), worksheet.Cell(r3 + 4, 3 + col)).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                    }
                    workbook.CalculateMode = XLCalculateMode.Auto;

                    //for (int row = 0; row < ds.Tables[0].Rows.Count; row++)
                    //{
                    //    d = 0; // Initialize Data Start Position = 4
                    //           // Excel row and column start positions for writing Row=1 and Col=1
                    //    for (int col = 1; col <= ds.Tables[0].Columns.Count; col++)
                    //    {
                    //        worksheet.Cell[r, col].Value = ds.Tables[0].Rows[row][d];
                    //        worksheet.Cell[r, col].AutoFitColumns();
                    //        d++;
                    //    }
                    //    r++;
                    //}

                    string Serverpath = HttpContext.Current.Server.MapPath("../Attachment/ReportHOBO/");

                    if (Periode == "1")
                    {
                        Periode = "Jan-Mar";
                    }
                    else if (Periode == "4")
                    {
                        Periode = "Apr-Jun";
                    }
                    else if (Periode == "7")
                    {
                        Periode = "Jul-Sep";
                    }
                    else if (Periode == "10")
                    {
                        Periode = "Okt-Des";
                    }
                    workbook.SaveAs(Serverpath + CompanyCode + "_" + Periode + "_2020.xlsx");
                }
                return "success";
            }
            else
            {
                return "no data";
            }

        }

        public string Test(int col)
        {
            string columnName = "";
            int mod = 0;

            while (col > 0)
            {
                //char z = (char)(65 + 25);
                //char a = (char)(65 + 0)
                //char b = (char)(65 + 1)
                mod = (col - 1) % 26;

                char _ascii = (char)(65 + mod);
                columnName = _ascii + columnName;
                if (columnName == "Z")
                {
                    col = (col - 1) / 26;
                }
                else
                {
                    col = col / 26;
                }
            }

            return columnName;
        }


        [WebMethod]
        public string SaveHOBO(string dataobject)
        {
            PZO_Domain domainBase = new PZO_Domain();
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();

            var obj = new JavaScriptSerializer().Deserialize<PZO_Domain>(dataobject);
            domainBase.EstCode = obj.EstCode;
            domainBase.Afdeling = obj.Afdeling;
            domainBase.Block = obj.Block;
            domainBase.KodeTMAT = obj.KodeTMAT;
            domainBase.Merk = obj.Merk;
            domainBase.Type = obj.Type;
            domainBase.Kondisi = obj.Kondisi;
            domainBase.SerialNumber = obj.SerialNumber;
            domainBase.Baterai = obj.Baterai;
            domainBase.lastCheck = obj.lastCheck;
            domainBase.Remark = obj.Remark;
            try
            {
                mapper.SaveData(domainBase);
                return "success";
            }
            catch (Exception ex)
            {
                return "error - " + ex.Message;
            }
        }

        [WebMethod]
        public string EditHOBO(string dataobject)
        {
            PZO_Domain domainBase = new PZO_Domain();
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();

            var obj = new JavaScriptSerializer().Deserialize<PZO_Domain>(dataobject);
            domainBase.ID = obj.ID;
            domainBase.EstCode = obj.EstCode;
            domainBase.Afdeling = obj.Afdeling;
            domainBase.Block = obj.Block;
            domainBase.KodeTMAT = obj.KodeTMAT;
            domainBase.Merk = obj.Merk;
            domainBase.Type = obj.Type;
            domainBase.Kondisi = obj.Kondisi;
            domainBase.SerialNumber = obj.SerialNumber;
            domainBase.Baterai = obj.Baterai;
            domainBase.lastCheck = obj.lastCheck;
            domainBase.Remark = obj.Remark;
            try
            {
                mapper.EditData(domainBase);
                return "success";
            }
            catch (Exception ex)
            {
                return "error - " + ex.Message;
            }
        }

        [WebMethod]
        public string DeleteHOBO(string dataobject)
        {
            PZO_Domain domainBase = new PZO_Domain();
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();

            var obj = new JavaScriptSerializer().Deserialize<PZO_Domain>(dataobject);
            domainBase.ID = obj.ID;
            try
            {
                mapper.DeleteData(domainBase);
                return "success";
            }
            catch (Exception ex)
            {
                return "error - " + ex.Message;
            }
        }



        [WebMethod]
        public string GetMasterHOBO(string EstCode, string Afd, string Block)
        {
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            DataTable dt = mapper.GetMasterHOBO(EstCode, Afd, Block);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetMasterHoboData(string ID)
        {
            PZO_Domain domain = new PZO_Domain();
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();

            domain.ID = ID;
            DataTable dt = mapper.ListDataMasterHOBO(ID).Tables[0];

            return DataTableToJSON(dt);
        }
        [WebMethod]
        public string GetWeekName()
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.getWeekName().Tables[0];
            return DataTableToJSON(dt);
        }
        [WebMethod]
        public string GetWeekNameByDate(string dateRequest)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.GetWeekNameByDate(dateRequest).Tables[0];
            return DataTableToJSON(dt);
        }
        [WebMethod]
        public string getZonaByEstate(string estCode)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.getZonaByKebun(estCode);
            return DataTableToJSON(dt);
        }
        [WebMethod]
        public string getWeeknameforPencatatanPerubahan(int dayparam)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            int bulan = DateTime.Now.Month;
            DataTable dt = mapper.getWeekNameForPerubahan(dayparam, bulan).Tables[0];
            return DataTableToJSON(dt);
        }
        [WebMethod]
        public string getNextPrevWeekNameForPencatatanPerubahan(int weekId)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.getNextPrevWeeknameForPerubahan(weekId).Tables[0];
            return DataTableToJSON(dt);
        }
        //[WebMethod]
        //public string GenerateReportPerubahanPencatatanTMAT(string wmArea, string WeekName, string weekLabel, int klhk)
        //{

        //}

        [WebMethod]
        public string getWaterDepthIndicator()
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.GetWaterDepthIndicator();
            return DataTableToJSON(dt);
        }

        [WebMethod]
        public string getNextPrevWeekNamePerbandinganPeta(int weekId)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.getNextPrevWeekPetaPerbandingan(weekId).Tables[0];
            return DataTableToJSON(dt);
        }

        [WebMethod]
        public string getNextPrevWeekNamePerbandinganPetaxx(int weekId)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.getNextPrevWeekPetaPerbandinganxx(weekId).Tables[0];
            return DataTableToJSON(dt);
        }

        [WebMethod]
        public string generatePetaPerbandingan(int weekId, string companyCode)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            int weekX = weekId - 1;
            DataSet dsX = mapper.generatePetaPerbandingan(weekX, companyCode);
            DataSet ds = mapper.generatePetaPerbandingan(weekId, companyCode);

            return "ok";
        }

        [WebMethod]
        public string[] getPetaPerbandinganPetaGo(string pt, int weekId)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataSet ds = mapper.getPetaPerbandinganPetaGo(pt, weekId);
            DataSet ds2 = mapper.getPetaPerbandinganPetaGo(pt, weekId - 1);

            string[] hasil = new string[10];

            hasil[0] = DataTableToJSON(ds2.Tables[0]);
            hasil[1] = DataTableToJSON(ds2.Tables[1]);
            hasil[2] = DataTableToJSON(ds2.Tables[2]);
            hasil[3] = DataTableToJSON(ds2.Tables[3]);

            hasil[4] = DataTableToJSON(ds.Tables[0]);
            hasil[5] = DataTableToJSON(ds.Tables[1]);
            hasil[6] = DataTableToJSON(ds.Tables[2]);
            hasil[7] = DataTableToJSON(ds.Tables[3]);

            hasil[8] = DataTableToJSON(ds2.Tables[4]);
            hasil[9] = DataTableToJSON(ds.Tables[4]);

            return hasil;
        }

        [WebMethod]
        public string[] getPetaKondisiPiezoGo(string pt, int weekId,int range,int indicatorID)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataSet ds = mapper.getPetaKondisiPiezoGo(pt, weekId, range, indicatorID);

            string[] hasil = new string[10];

            hasil[0] = DataTableToJSON(ds.Tables[0]);
            hasil[1] = DataTableToJSON(ds.Tables[1]);
            hasil[2] = DataTableToJSON(ds.Tables[2]);

            return hasil;
        }

        void xrTableCell10_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            if (dataRow["selisihKetinggian2"].ToString() != null && dataRow["selisihKetinggian2"].ToString() != "")
            {
                int ketinggian = Convert.ToInt32(dataRow["selisihKetinggian2"].ToString());
                //if (Convert.ToInt32(dataRow["Ketinggian"]) > 40)
                //{
                    if (dataRow["warnaXY"].ToString().ToUpper() == "MERAH")
                    {

                        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F02626");
                        cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");

                    }
                    else if (dataRow["warnaXY"].ToString().ToUpper() == "BIRU")
                    {
                        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#539ed6");
                        cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");                        
                    }
                    else if (dataRow["warnaXY"].ToString().ToUpper() == "HIJAU")
                    {
                        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                        cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                    else
                    {
                        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                        cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    }
                //if (ketinggian >= 5 && ketinggian < 7)
                //{
                //    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F50057");
                //    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF00");
                //    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                //}
                //else if (ketinggian >= 7)
                //{
                //    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F50057");
                //    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                //}
                //else
                //{
                //    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                //    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                //}
                //}
                //else
                //{
                //    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                //    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                //}
            }
            else
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
            //if (dataRow["selisihKetinggian2"].ToString() != null && dataRow["selisihKetinggian2"].ToString() != "")
            //{
            //    int ketinggian = Convert.ToInt32(dataRow["selisihKetinggian2"].ToString());
            //    if (Convert.ToInt32(dataRow["Ketinggian"]) > 40)
            //    {
            //        if (ketinggian >= 5 && ketinggian < 7)
            //        {
            //            //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F50057");
            //            cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF00");
            //            cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            //        }
            //        else if (ketinggian > 7)
            //        {
            //            cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F50057");
            //            cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            //        }
            //        else if (ketinggian == 7)
            //        {
            //            cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffaa66");
            //            cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            //        }
            //        else
            //        {
            //            cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
            //            cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            //        }
            //    }
            //    else
            //    {
            //        cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
            //        cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            //    }
            //}
        }
        void xrTableCell9_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            if (dataRow["ketinggian"].ToString() != null && dataRow["ketinggian"].ToString() != "")
            {
                int ketinggian = Convert.ToInt32(dataRow["ketinggian"].ToString());
                if (ketinggian < 0)
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian >= 0 && ketinggian <= 44)
                {
                    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F50057");
                    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#1950A0");
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#4672B3");                    
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }              
                else if (ketinggian >= 45 && ketinggian <= 49)
                {
                    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#55701F");
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#00ccf5");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian >= 50 && ketinggian <= 70)
                {
                    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#55701F");
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#55701F");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian >= 71 && ketinggian <= 75)
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFD53");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }               
                else if (ketinggian > 75)
                {
                    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#EC3223");
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F16F65");                    
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
            }
            else
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
        }
        void XRTableCell12BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            DataSet ds = dataRow.DataView.DataViewManager.DataSet;
            DataTable dt = ds.Tables[1];
            if (dataRow["Ket"].ToString().ToLower() == "turun >7 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");

            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 5-7 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 5-10 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
            }

            else if (dataRow["Ket"].ToString().ToLower() == "naik >10cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
            }

        }
        void xrTableCell13BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            DataSet ds = dataRow.DataView.DataViewManager.DataSet;
            DataTable dt = ds.Tables[1];
            if (dataRow["Ket"].ToString().ToLower() == "turun >7 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
                //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#01B0F1");

            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 5-7 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
                //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#01B0F1");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 5-10 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
            }

            else if (dataRow["Ket"].ToString().ToLower() == "naik >10cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
            }

        }
        void xrTableCell14BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            DataSet ds = dataRow.DataView.DataViewManager.DataSet;
            DataTable dt = ds.Tables[1];
            if (dataRow["Ket"].ToString().ToLower() == "turun >7 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FE0002");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");

            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 5-7 cm")
            {
                //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFC000");
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FE0002");

            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 5-10 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");
            }

            else if (dataRow["Ket"].ToString().ToLower() == "naik >10cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");
            }

        }
        void xrTableCell15BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            DataSet ds = dataRow.DataView.DataViewManager.DataSet;
            DataTable dt = ds.Tables[1];
            if (dataRow["Ket"].ToString().ToLower() == "turun >7 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FE0002");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");

            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 5-7 cm")
            {
                //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFC000");
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");

            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 5-10 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
            }

            else if (dataRow["Ket"].ToString().ToLower() == "naik >10cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#0071C1");
            }

        }
        void xrTableCell17BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            DataSet ds = dataRow.DataView.DataViewManager.DataSet;
            DataTable dt = ds.Tables[1];
            if (dataRow["Ket"].ToString().ToLower() == "turun >7 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FE0002");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");

            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 5-7 cm")
            {
                //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFC000");
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FE0002");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 5-10 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");
            }

            else if (dataRow["Ket"].ToString().ToLower() == "naik >10cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");
            }

        }
        void xrTableCell18BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            DataSet ds = dataRow.DataView.DataViewManager.DataSet;
            DataTable dt = ds.Tables[1];
            if (dataRow["Ket"].ToString().ToLower() == "turun >7 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FE0002");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");

            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 5-7 cm")
            {
                //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFC000");
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FE0002");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 5-10 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");
            }

            else if (dataRow["Ket"].ToString().ToLower() == "naik >10cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#92D14F");
            }

        }
        void xrTableCell3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();           
            if (dataRow["ketinggian"].ToString() != null && dataRow["ketinggian"].ToString() != "")
            {
                int ketinggian = Convert.ToInt32(dataRow["ketinggian"].ToString());
                if (ketinggian < 0)
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian >= 0 && ketinggian <= 40)
                {
                    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F50057");
                    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#1950A0");
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#4672B3");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian >= 41 && ketinggian <= 60)
                {
                    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#55701F");
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#889A62");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian >= 60 && ketinggian <= 65)
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFD53");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else if (ketinggian > 65)
                {
                    //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#EC3223");
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#F16F65");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
                else
                {
                    cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                    cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                }
            }
            else
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffffff");
                cell.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
            }
        }
        void xrTableCell36BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            XtraReportBase reportbase = cell.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            DataSet ds = dataRow.DataView.DataViewManager.DataSet;
            DataTable dt = ds.Tables[1];
            if (dataRow["Ket"].ToString().ToLower() == "turun >7 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#e93224");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#2c72ba");
                //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#01B0F1");

            }
            else if (dataRow["Ket"].ToString().ToLower() == "turun 5-7 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#a0ce62");
                //cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#01B0F1");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 0-5 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#2c72ba");
            }
            else if (dataRow["Ket"].ToString().ToLower() == "naik 5-10 cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#2c72ba");
            }

            else if (dataRow["Ket"].ToString().ToLower() == "naik >10cm")
            {
                cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#2c72ba");
            }

        }

    }
}
