﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using PASS.Mapper;
using PASS.Domain;
using PZO_PiezometerOnlineMap.Mapper;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for ImportHandlerExcel
    /// </summary>
    public class ImportHandlerExcel : IHttpHandler
    {
        PZO_ProjectMapper mapper = new PZO_ProjectMapper();

        public void ProcessRequest(HttpContext context)
        {

            var filenames = context.Request.QueryString["filename"];
            var userID = Convert.ToInt32(context.Request.QueryString["userID"]);
            var logger = context.Request.QueryString["logger"];
            try
            {
                var names = "";
                string Compresspath = HttpContext.Current.Server.MapPath("../Attachment/ImportFileExcel/");
                string CompresspathTemp = HttpContext.Current.Server.MapPath("../Attachment/ImportFileExcelTemp/");
                var PostedFile = context.Request.Files[0];
                string FileName = PostedFile.FileName;

                string FileDirectorys = Compresspath + filenames + "." + FileName.Substring(FileName.LastIndexOf('.') + 1);
                if (!Directory.Exists(Compresspath))
                    Directory.CreateDirectory(Compresspath);

                int index = FileName.IndexOf(".");
                if (index > 0)
                    names = FileName.Substring(0, index);
                string FileDirectoryTemp = CompresspathTemp + names + "." + FileName.Substring(FileName.LastIndexOf('.') + 1);
                if (!Directory.Exists(CompresspathTemp))
                    Directory.CreateDirectory(CompresspathTemp);

                //string filemap = "/Excel";
                
                //if (File.Exists(FileDirectorys))
                //{
                //    System.GC.Collect();
                //    System.GC.WaitForPendingFinalizers();
                //    File.Delete(FileDirectorys);
                //}
                PostedFile.SaveAs(FileDirectorys);
                PostedFile.SaveAs(FileDirectoryTemp);
                string status = importDatafromExcel(PostedFile, FileDirectoryTemp, userID, logger);
                context.Response.Write(status);

            }
            catch (Exception)
            {

                throw;
            }
        }

        public string importDatafromExcel(System.Web.HttpPostedFile filename, string fileDirectory, int userID, string blocklogger)
        {
            CultureInfo culture = new CultureInfo("en-US");

            string statusUpload = "";
            //DataTable dt = mapper.getWeekName(DateTime.Parse(startDate), DateTime.Parse(endDate));
            string Serverpath = HttpContext.Current.Server.MapPath("../Attachment/ImportFileExcelTemp/");
            string excelPath = Serverpath + filename.FileName;
            //string sqlTable = "dbo.tTestImport";
            //string myExcelDataQuery = "select Nama, Nim, Kelas from [sheet1$]";    
            string excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=Excel 12.0;";
            OleDbConnection con = new OleDbConnection(excelConnectionString);
            string sqlConnectioString = System.Configuration.ConfigurationManager.ConnectionStrings["conStrGISAPP"].ConnectionString;
            string sqlConnectioString2 = System.Configuration.ConfigurationManager.ConnectionStrings["conStrGISAPP2"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(sqlConnectioString);
            //con.Close();
            //sqlConn.Close();
            //sqlConn.Open();
            try
            {
                //string excelConnectionString = @"provider=microsoft.jet.oledb.4.0;data source=" + excelPath + @"provider=microsoft.jet.oledb.4.0;data source=" + excelPath +
                //                               ";extended properties=" + "\"excel 8.0;hdr=yes;\"";
                System.Data.DataTable excelSheetName = new System.Data.DataTable();
                System.Data.DataTable dtExcel = new System.Data.DataTable();

                con.Open();

                excelSheetName = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string excelquery = "Select* from[" + excelSheetName.Rows[0]["TABLE_NAME"].ToString() + "]";
                OleDbDataAdapter data = new OleDbDataAdapter(excelquery, con);
                data.Fill(dtExcel);
                string json = JsonConvert.SerializeObject(dtExcel, Formatting.Indented);
                string CompanyCode = "";
                var Tahun = "";
                var Bulan = "";
                var Tanggal = "";
                string Logger = "";
                string TMAT = "";
                var Date = "";
                var Referensi = "";
                List<String> listStrLineElements;
                string[] columnNames = dtExcel.Columns.Cast<DataColumn>()
                                                 .Select(x => x.ColumnName)
                                                 .ToArray();

                foreach (DataRow row in dtExcel.Rows)
                {
                    try
                    {
                        for (int i = 0; i < row.ItemArray.Length; i++)
                        {
                            if(blocklogger == row[1].ToString())
                            {
                                CompanyCode = row[0].ToString();
                                Logger = row[1].ToString();
                                Tanggal = row[2].ToString();
                                TMAT = row[3].ToString();
                                Referensi = row[4].ToString();
                            }
                            else
                            {
                                statusUpload = "Check Logger in Filtering and excel file";
                                return statusUpload;
                            }

                        }
                        mapper.SaveDataLoggerKLHK(CompanyCode, Tanggal, Logger, TMAT, userID, Referensi);
                        statusUpload = "Success";
                    }
                    catch (Exception e)
                    {
                        statusUpload = e.ToString();
                        throw e;

                    }
                }

                con.Close();
                File.Delete(fileDirectory);
            }
            catch (Exception e)
            {
                e.Message.ToString();
                statusUpload = e.ToString() + "error";

                con.Close();
                //con.Close();
                //sqlConn.Close();
            }
            return statusUpload;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}