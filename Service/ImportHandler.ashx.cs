﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using PASS.Mapper;
using PASS.Domain;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for ImportHandler
    /// </summary>
    public class ImportHandler : IHttpHandler
    {
        
        public void ProcessRequest(HttpContext context)
        {

            var filename = context.Request.QueryString["filename"];
            var year = context.Request.QueryString["year"];
            var periode = context.Request.QueryString["periode"];
            try
            {
                
                string CompresspathHOBO = HttpContext.Current.Server.MapPath("../attachment/ImportFile/HOBO/");
                string CompresspathZIP = HttpContext.Current.Server.MapPath("../attachment/ImportFile/RAR/");
                var PostedFile = context.Request.Files[0];

                string FileName = PostedFile.FileName;
                string FileDirectoryHOBO = CompresspathHOBO + filename + "." + FileName.Substring(FileName.LastIndexOf('.') + 1);
                string FileDirectoryZIP = CompresspathZIP + filename;
                //string filemap = "/Excel";

                if (!Directory.Exists(CompresspathHOBO))
                    Directory.CreateDirectory(CompresspathHOBO);

                if (!Directory.Exists(CompresspathZIP))
                    Directory.CreateDirectory(CompresspathZIP);

                //if (File.Exists(FileDirectorys))
                //{
                //    System.GC.Collect();
                //    System.GC.WaitForPendingFinalizers();
                //    File.Delete(FileDirectorys);
                //}
                PostedFile.SaveAs(FileDirectoryHOBO);
                PostedFile.SaveAs(FileDirectoryZIP);
                string status = "Success";
                context.Response.Write(status);

            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}