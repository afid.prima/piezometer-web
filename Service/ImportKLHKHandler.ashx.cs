﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using PASS.Mapper;
using PASS.Domain;
using PZO_PiezometerOnlineMap.Mapper;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for ImportKLHKHandler
    /// </summary>
    public class ImportKLHKHandler : IHttpHandler
    {
        PZO_ProjectMapper mapper = new PZO_ProjectMapper();
        public void ProcessRequest(HttpContext context)
        {

            var userID = Convert.ToInt32(context.Request.QueryString["userID"]);
            var Year = 2020;
            try
            {
                var filename = "";
                string Compresspath = HttpContext.Current.Server.MapPath("../Attachment/UploadFileKLHK/");
                var PostedFile = context.Request.Files[0];

                string FileName = PostedFile.FileName;
                int index = FileName.IndexOf(".");
                if (index > 0)
                    filename = FileName.Substring(0, index);
                string FileDirectorys = Compresspath + filename + "." + FileName.Substring(FileName.LastIndexOf('.') + 1);
                //string filemap = "/Excel";

                if (!Directory.Exists(Compresspath))
                    Directory.CreateDirectory(Compresspath);

                //if (File.Exists(FileDirectorys))
                //{
                //    System.GC.Collect();
                //    System.GC.WaitForPendingFinalizers();
                //    File.Delete(FileDirectorys);
                //}
                PostedFile.SaveAs(FileDirectorys);
                string status = importDatafromExcel(PostedFile, userID, Year);
                context.Response.Write(status);

            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public string importDatafromExcel(System.Web.HttpPostedFile filename, int userID, int Year)
        {
            CultureInfo culture = new CultureInfo("en-US");

            string statusUpload = "";
            //DataTable dt = mapper.getWeekName(DateTime.Parse(startDate), DateTime.Parse(endDate));
            string Serverpath = HttpContext.Current.Server.MapPath("../Attachment/UploadFileKLHK/");
            string excelPath = Serverpath + filename.FileName;
            //string sqlTable = "dbo.tTestImport";
            //string myExcelDataQuery = "select Nama, Nim, Kelas from [sheet1$]";    
            string excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=Excel 12.0;";
            OleDbConnection con = new OleDbConnection(excelConnectionString);
            string sqlConnectioString = System.Configuration.ConfigurationManager.ConnectionStrings["conStrGISAPP"].ConnectionString;
            string sqlConnectioString2 = System.Configuration.ConfigurationManager.ConnectionStrings["conStrGISAPP2"].ConnectionString;
            SqlConnection sqlConn = new SqlConnection(sqlConnectioString);
            //con.Close();
            //sqlConn.Close();
            //sqlConn.Open();
            try
            {
                //string excelConnectionString = @"provider=microsoft.jet.oledb.4.0;data source=" + excelPath + @"provider=microsoft.jet.oledb.4.0;data source=" + excelPath +
                //                               ";extended properties=" + "\"excel 8.0;hdr=yes;\"";
                System.Data.DataTable excelSheetName = new System.Data.DataTable();
                System.Data.DataTable dtExcel = new System.Data.DataTable();

                con.Open();

                excelSheetName = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string excelquery = "Select* from[" + excelSheetName.Rows[0]["TABLE_NAME"].ToString() + "]";
                OleDbDataAdapter data = new OleDbDataAdapter(excelquery, con);
                data.Fill(dtExcel);
                string json = JsonConvert.SerializeObject(dtExcel, Formatting.Indented);
                var Estcode = "";
                var Block = "";
                var TanggalSurvey = "";
                var HasilGenerate = "";
                var WeekName = "";
                var Date = "";

                string[] columnNames = dtExcel.Columns.Cast<DataColumn>()
                                                 .Select(x => x.ColumnName)
                                                 .ToArray();

                if (columnNames[0] == "ID Perusahaan")
                {
                    if (columnNames[1] == "EstCode")
                    {
                        if (columnNames[2] == "Kode TPMAT")
                        {
                            if (columnNames[3] == "Nama Petugas Survei")
                            {
                                if (columnNames[4] == "Tanggal Survei")
                                {
                                    if (columnNames[5] == "Tinggi Muka Air Tanah")
                                    {
                                        if (columnNames[6] == "Kode Stasiun Curah Hujan")
                                        {
                                            if (columnNames[7] == "Curah Hujan")
                                            {
                                                if (columnNames[8] == "Ambang Baku Kerusakan Tinggi MAT")
                                                {
                                                    if (columnNames[9] == "Hasil Generate")
                                                    {
                                                        foreach (DataRow row in dtExcel.Rows)
                                                        {
                                                            try
                                                            {
                                                                for (int i = 0; i < row.ItemArray.Length; i++)
                                                                {
                                                                    Estcode = row[1].ToString();
                                                                    Block = row[2].ToString();
                                                                    TanggalSurvey = row[4].ToString();
                                                                    HasilGenerate = row[9].ToString();
                                                                    WeekName = mapper.getWeekName(TanggalSurvey);
                                                                }
                                                                mapper.SaveDataKLHK(Estcode, Block, WeekName, HasilGenerate, userID);
                                                                statusUpload = "berhasil";
                                                            }
                                                            catch (Exception e)
                                                            {
                                                                statusUpload = e.ToString();
                                                                throw e;

                                                            }
                                                        }
                                                        //domain.StationID = "66";
                                                        //domain.sumRainDays = "1.00";
                                                        //domain.sumRainInches = "11.00000";
                                                        //domain.transdate = "2019-01-20";
                                                        //domain.Keterangan = "";
                                                        //mapper.AddDataCurahHujan(domain); //ini buat insert dari mapper ke sp nya
                                                        //return json;
                                                        //File.Delete(excelPath);
                                                    }
                                                    else
                                                    {
                                                        return "Format excel tidak sesuai, mohon periksa column J";
                                                    }
                                                }
                                                else
                                                {
                                                    return "Format excel tidak sesuai, mohon periksa column I";
                                                }
                                            }
                                            else
                                            {
                                                return "Format excel tidak sesuai, mohon periksa column H";
                                            }
                                        }
                                        else
                                        {
                                            return "Format excel tidak sesuai, mohon periksa column G";
                                        }

                                    }
                                    else
                                    {
                                        return "Format excel tidak sesuai, mohon periksa column F";
                                    }
                                }
                                else
                                {
                                    return "Format excel tidak sesuai, mohon periksa column E";
                                }
                            }
                            else
                            {
                                return "Format excel tidak sesuai, mohon periksa D";
                            }
                        }
                        else
                        {
                            return "Format excel tidak sesuai, mohon periksa column C";
                        }
                    }
                    else
                    {
                        return "Format excel tidak sesuai, mohon periksa column B";
                    }
                }
                else
                {
                    return "Format excel tidak sesuai, mohon periksa column A";
                }



                con.Close();
            }
            catch (Exception e)
            {
                e.Message.ToString();
                statusUpload = e.ToString() + "error";

                con.Close();
                //con.Close();
                //sqlConn.Close();
            }
            return statusUpload;
        }
    }
}