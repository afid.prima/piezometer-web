﻿using System;
using System.Data;
using System.Linq;
using System.Web.Services;
using PASS.Domain;
using PASS.Mapper;
using PZO_PiezometerOnlineMap.Class;
using PZO_PiezometerOnlineMap.Mapper;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for MapServiceTambahan
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MapServiceTambahan : System.Web.Services.WebService
    {
        [WebMethod]
        public string GetPiezoRecordDetailByPieRecordID(string PieRecordID, long startWeek, long endWeek)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            JsonConverter converter = new JsonConverter();
            DataTable dt = mapper.GetPiezoRecordDetailByPieRecordIDWithLimitDate(PieRecordID, startWeek, endWeek);
            return converter.ObjectToJSON(dt);
        }

        [WebMethod]
        public string GetPiezoRecordDetailByPieRecordIDWithLimitDate2(string PieRecordID, int week, int month, int year)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            JsonConverter converter = new JsonConverter();
            DataTable dt = mapper.GetPiezoRecordDetailByPieRecordIDWithLimitDate2(PieRecordID, week, month, year);
            return converter.ObjectToJSON(dt);
        }

        [WebMethod]
        public string GetWeekFromID(string ID)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.GetWeekFromID(int.Parse(ID));

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        //[WebMethod]
        //public string GetWaterDepthIndicator()
        //{
        //    JsonConverter converter = new JsonConverter();
        //    PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
        //    DataTable dt = mapper.GetWaterDepthIndicator();
        //    return converter.ObjectToJSON(dt);
        //}

        [WebMethod]
        public string GetQueryFilter(int idwmarea, string estCode, long startWeek, long endWeek, int varMin, int varMax, bool berurut, int nilaiOperator, string jenisOperator)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();

            //ambil semua pengukuran dari tanggal start week awal samapai end week 
            DataTable dt = new DataTable();
            DataTable dtMasterIplas = new DataTable();
            if (idwmarea == 0)
            {
                dt = mapper.GetQueryFilter(estCode, startWeek, endWeek);
                dtMasterIplas = mapper.GetMappingPiezoRecordIPLAS(estCode);
            }
            else
            {
                dt = mapper.GetQueryFilterWMArea(idwmarea, startWeek, endWeek);
                dtMasterIplas = mapper.GetMappingPiezoRecordIPLASWMarean(idwmarea);
            }
            DataTable dtTampung = dt.Clone();

            foreach (DataRow row in dt.Rows)
            {
                int ketinggian = Convert.ToInt32(row["Ketinggian"].ToString());
                if (ketinggian >= varMin && ketinggian <= varMax)
                {
                    dtTampung.Rows.Add(row.ItemArray);
                }
            }

            DataTable dtKirim = new DataTable();
            dtKirim.Columns.Add("PieRecordID");
            dtKirim.Columns.Add("Block");
            dtKirim.Columns.Add("Banyak");

            DataTable dtTemp = dt.Clone();
            DataRow rowX = null;
            int ururt = 0;
            bool samadenggan = false;

            for (int h = 0; h < dtTampung.Rows.Count; h++)
            {
                DataRow row = dtTampung.Rows[h];
                if (rowX == null)
                {
                    dtTemp.Rows.Add(row.ItemArray);
                    ururt++;
                }
                else
                {
                    //jika row sebelum sama denggan row sekarang
                    if (row["PieRecordID"].ToString() == rowX["PieRecordID"].ToString())
                    {
                        //jika beruntun maka cek dahulu apakah row sekarang adalah -1 dari row sebelumnya
                        if (berurut)
                        {
                            int weekIDXm1 = Convert.ToInt32(rowX["ID"].ToString());
                            weekIDXm1 = weekIDXm1 - 1;

                            if (weekIDXm1 == Convert.ToInt32(row["ID"].ToString()))
                            {
                                dtTemp.Rows.Add(row.ItemArray);
                                ururt++;
                            }
                            else
                            {
                                if (jenisOperator == ">")
                                {
                                    //jika nilai urut masih di bawah nilai yang ditentukan maka kembali 1
                                    if (ururt <= nilaiOperator)
                                    {
                                        ururt = 1;
                                    }
                                }
                                else if (jenisOperator == "=")
                                {
                                    //jika nilai urut masih di bawah nilai yang ditentukan maka kembali 1
                                    if (ururt == nilaiOperator)
                                    {
                                        samadenggan = true;
                                    }
                                    if (!samadenggan && ururt != nilaiOperator)
                                    {
                                        ururt = 1;
                                    }
                                }
                                else if (jenisOperator == "<")
                                {
                                    if (ururt >= nilaiOperator)
                                    {
                                        ururt = 1;
                                    }
                                }
                            }
                        }
                        else
                        {
                            //jika tidak beruntun langsung tampung aja
                            dtTemp.Rows.Add(row.ItemArray);
                        }
                    }
                    else
                    {
                        // jika sudah beda row maka 
                        // jika beruntun maka 
                        if (berurut)
                        {
                            if (jenisOperator == ">")
                            {
                                //jika berutur kurang dari nilai yang di input maka hapus piezo id tsb
                                if (ururt <= nilaiOperator)
                                {
                                    DataRow[] drr = dtTemp.Select("PieRecordID='" + rowX["PieRecordID"].ToString() + "' ");
                                    for (int i = 0; i < drr.Length; i++)
                                    {
                                        drr[i].Delete();
                                        dtTemp.AcceptChanges();
                                    }
                                }
                            }
                            else if (jenisOperator == "=")
                            {
                                if (!samadenggan && ururt != nilaiOperator)
                                {
                                    DataRow[] drr = dtTemp.Select("PieRecordID='" + rowX["PieRecordID"].ToString() + "' ");
                                    for (int i = 0; i < drr.Length; i++)
                                    {
                                        drr[i].Delete();
                                        dtTemp.AcceptChanges();
                                    }
                                }
                            }
                            else if (jenisOperator == "<")
                            {
                                //jika berutur kurang dari nilai yang di input maka hapus piezo id tsb
                                if (ururt >= nilaiOperator)
                                {
                                    DataRow[] drr = dtTemp.Select("PieRecordID='" + rowX["PieRecordID"].ToString() + "' ");
                                    for (int i = 0; i < drr.Length; i++)
                                    {
                                        drr[i].Delete();
                                        dtTemp.AcceptChanges();
                                    }
                                }
                            }
                        }
                        dtTemp.Rows.Add(row.ItemArray);
                        samadenggan = false;
                        ururt = 1;
                    }
                }
                rowX = row;

            }

            if (rowX != null)
            {
                //kondisi untuk row terakhir dari dtTampung bandingkan denggan row terakhir dtTemp
                if (berurut)
                {
                    //int weekIDXm1 = Convert.ToInt32(rowX["ID"].ToString());
                    //weekIDXm1 = weekIDXm1 + 1;
                    if (dtTemp.Rows.Count > 0)
                    {

                        if (jenisOperator == ">")
                        {
                            //jika berutur kurang dari nilai yang di input maka hapus piezo id tsb
                            if (ururt < nilaiOperator)
                            {
                                DataRow[] drr = dtTemp.Select("PieRecordID='" + rowX["PieRecordID"].ToString() + "' ");
                                for (int i = 0; i < drr.Length; i++)
                                {
                                    drr[i].Delete();
                                    dtTemp.AcceptChanges();
                                }
                            }
                        }
                        else if (jenisOperator == "=")
                        {
                            if (!samadenggan && ururt != nilaiOperator)
                            {
                                DataRow[] drr = dtTemp.Select("PieRecordID='" + rowX["PieRecordID"].ToString() + "' ");
                                for (int i = 0; i < drr.Length; i++)
                                {
                                    drr[i].Delete();
                                    dtTemp.AcceptChanges();
                                }
                            }
                        }
                        else if (jenisOperator == "<")
                        {
                            if (ururt > nilaiOperator)
                            {
                                DataRow[] drr = dtTemp.Select("PieRecordID='" + rowX["PieRecordID"].ToString() + "' ");
                                for (int i = 0; i < drr.Length; i++)
                                {
                                    drr[i].Delete();
                                    dtTemp.AcceptChanges();
                                }
                            }
                        }
                    }
                }
            }

            var query = from row in dtTemp.AsEnumerable()
                        group row by row.Field<string>("PieRecordID") into PieRecordID
                        orderby PieRecordID.Key
                        select new
                        {
                            Name = PieRecordID.Key,
                            CountOfPieRecordID = PieRecordID.Count()
                        };



            foreach (var PieRecordID in query)
            {
                if (jenisOperator == ">")
                {
                    if (PieRecordID.CountOfPieRecordID > nilaiOperator)
                    {
                        foreach (DataRow row in dtTemp.Rows)
                        {
                            if (row["PieRecordID"].ToString() == PieRecordID.Name)
                            {
                                dtKirim.Rows.Add(row["PieRecordID"], row["Block"], PieRecordID.CountOfPieRecordID);
                                dtKirim = addBlockIplas(dtMasterIplas, dtKirim, row["PieRecordID"].ToString(), PieRecordID.CountOfPieRecordID);
                                break;
                            }
                        }
                    }
                }
                else if (jenisOperator == "<")
                {
                    if (PieRecordID.CountOfPieRecordID < nilaiOperator)
                    {
                        foreach (DataRow row in dtTemp.Rows)
                        {
                            if (row["PieRecordID"].ToString() == PieRecordID.Name)
                            {
                                dtKirim.Rows.Add(row["PieRecordID"], row["Block"], PieRecordID.CountOfPieRecordID);
                                dtKirim = addBlockIplas(dtMasterIplas, dtKirim, row["PieRecordID"].ToString(), PieRecordID.CountOfPieRecordID);
                                break;
                            }
                        }
                    }
                }
                else if (jenisOperator == "=")
                {
                    if (PieRecordID.CountOfPieRecordID == nilaiOperator)
                    {
                        foreach (DataRow row in dtTemp.Rows)
                        {
                            if (row["PieRecordID"].ToString() == PieRecordID.Name)
                            {
                                dtKirim.Rows.Add(row["PieRecordID"], row["Block"], PieRecordID.CountOfPieRecordID);
                                dtKirim = addBlockIplas(dtMasterIplas, dtKirim, row["PieRecordID"].ToString(), PieRecordID.CountOfPieRecordID);
                                break;
                            }
                        }
                    }
                }

            }

            return converter.ObjectToJSON(dtKirim);
        }

        [WebMethod]
        public string GetPalingBanyakQueryFilter(int idwmarea, string estCode, long startWeek, long endWeek, int varMin, int varMax, bool berurut)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();

            //ambil semua pengukuran dari tanggal start week awal samapai end week 
            DataTable dt = new DataTable();
            DataTable dtMasterIplas = new DataTable();
            if (idwmarea == 0)
            {
                dt = mapper.GetQueryFilter(estCode, startWeek, endWeek);
                dtMasterIplas = mapper.GetMappingPiezoRecordIPLAS(estCode);
            }
            else
            {
                dt = mapper.GetQueryFilterWMArea(idwmarea, startWeek, endWeek);
                dtMasterIplas = mapper.GetMappingPiezoRecordIPLASWMarean(idwmarea);
            }
            DataTable dtTampung = dt.Clone();

            foreach (DataRow row in dt.Rows)
            {
                int ketinggian = Convert.ToInt32(row["Ketinggian"].ToString());
                if (ketinggian >= varMin && ketinggian <= varMax)
                {
                    dtTampung.Rows.Add(row.ItemArray);
                }
            }

            DataTable dtKirim = new DataTable();
            dtKirim.Columns.Add("PieRecordID");
            dtKirim.Columns.Add("Block");
            dtKirim.Columns.Add("Banyak");

            DataTable dtTemp = dt.Clone();
            DataRow rowX = null;
            int ururt = 0;
            int ururtX = 1;

            for (int h = 0; h < dtTampung.Rows.Count; h++)
            {
                DataRow row = dtTampung.Rows[h];
                if (rowX == null)
                {
                    dtTemp.Rows.Add(row.ItemArray);
                    ururt++;
                }
                else
                {
                    //jika row sebelum sama denggan row sekarang
                    if (row["PieRecordID"].ToString() == rowX["PieRecordID"].ToString())
                    {
                        //jika beruntun maka cek dahulu apakah row sekarang adalah -1 dari row sebelumnya
                        if (berurut)
                        {
                            int weekIDXm1 = Convert.ToInt32(rowX["ID"].ToString());
                            weekIDXm1 = weekIDXm1 - 1;

                            if (weekIDXm1 == Convert.ToInt32(row["ID"].ToString()))
                            {
                                dtTemp.Rows.Add(row.ItemArray);
                                ururt++;
                            }
                            else
                            {
                                ururt = 1;
                            }
                        }
                        else
                        {
                            //jika tidak beruntun langsung tampung aja
                            dtTemp.Rows.Add(row.ItemArray);
                            ururt++;
                        }
                    }
                    else
                    {
                        if (ururt >= ururtX)
                        {
                            ururtX = ururt;
                        }
                        dtTemp.Rows.Add(row.ItemArray);
                        ururt = 1;
                    }
                }
                rowX = row;

            }

            var query = from row in dtTemp.AsEnumerable()
                        group row by row.Field<string>("PieRecordID") into PieRecordID
                        orderby PieRecordID.Key
                        select new
                        {
                            Name = PieRecordID.Key,
                            CountOfPieRecordID = PieRecordID.Count()
                        };

            foreach (var PieRecordID in query)
            {

                if (PieRecordID.CountOfPieRecordID == ururtX)
                {
                    foreach (DataRow row in dtTemp.Rows)
                    {
                        if (row["PieRecordID"].ToString() == PieRecordID.Name)
                        {
                            dtKirim.Rows.Add(row["PieRecordID"], row["Block"], PieRecordID.CountOfPieRecordID);
                            dtKirim = addBlockIplas(dtMasterIplas, dtKirim, row["PieRecordID"].ToString(), PieRecordID.CountOfPieRecordID);
                            break;
                        }
                    }
                }

            }

            return converter.ObjectToJSON(dtKirim);
        }

        [WebMethod]
        public string GetSeringQueryFilter(int idwmarea, string estCode, long startWeek, long endWeek, int varMin, int varMax, bool berurut)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();

            //ambil semua pengukuran dari tanggal start week awal samapai end week 
            DataTable dt = new DataTable();
            DataTable dtMasterIplas = new DataTable();
            if (idwmarea == 0)
            {
                dt = mapper.GetQueryFilter(estCode, startWeek, endWeek);
                dtMasterIplas = mapper.GetMappingPiezoRecordIPLAS(estCode);
            }
            else
            {
                dt = mapper.GetQueryFilterWMArea(idwmarea, startWeek, endWeek);
                dtMasterIplas = mapper.GetMappingPiezoRecordIPLASWMarean(idwmarea);
            }

            DataTable dtTampung = dt.Clone();

            foreach (DataRow row in dt.Rows)
            {
                int ketinggian = Convert.ToInt32(row["Ketinggian"].ToString());
                if (ketinggian >= varMin && ketinggian <= varMax)
                {
                    dtTampung.Rows.Add(row.ItemArray);
                }
            }

            DataTable dtKirim = new DataTable();
            dtKirim.Columns.Add("PieRecordID");
            dtKirim.Columns.Add("Block");
            dtKirim.Columns.Add("Banyak");

            DataTable dtTemp = dt.Clone();
            DataRow rowX = null;
            int ururt = 0;
            List<int> listUrut = new List<int>();

            for (int h = 0; h < dtTampung.Rows.Count; h++)
            {
                DataRow row = dtTampung.Rows[h];
                if (rowX == null)
                {
                    dtTemp.Rows.Add(row.ItemArray);
                    ururt = 1;
                }
                else
                {
                    //jika row sebelum sama denggan row sekarang
                    if (row["PieRecordID"].ToString() == rowX["PieRecordID"].ToString())
                    {
                        //jika beruntun maka cek dahulu apakah row sekarang adalah -1 dari row sebelumnya
                        if (berurut)
                        {
                            int weekIDXm1 = Convert.ToInt32(rowX["ID"].ToString());
                            weekIDXm1 = weekIDXm1 - 1;

                            if (weekIDXm1 == Convert.ToInt32(row["ID"].ToString()))
                            {
                                ururt++;
                            }
                            else
                            {
                                listUrut.Add(ururt);
                                ururt = 1;
                            }
                        }
                        else
                        {
                            //jika tidak beruntun langsung tampung aja
                            dtTemp.Rows.Add(row.ItemArray);
                        }
                    }
                    else
                    {

                        listUrut.Add(ururt);
                        dtTemp = getMaxBerturutSering(listUrut, dtTemp, rowX);

                        listUrut = new List<int>();
                        dtTemp.Rows.Add(row.ItemArray);
                        ururt = 1;
                    }
                }
                rowX = row;
            }

            if (rowX != null)
            {
                //kondisi untuk row terakhir dari dtTampung bandingkan denggan row terakhir dtTemp
                if (berurut)
                {
                    //int weekIDXm1 = Convert.ToInt32(rowX["ID"].ToString());
                    //weekIDXm1 = weekIDXm1 + 1;
                    if (dtTemp.Rows.Count > 0)
                    {
                        listUrut.Add(ururt);
                        dtTemp = getMaxBerturutSering(listUrut, dtTemp, rowX);
                    }
                }
            }

            var query = from row in dtTemp.AsEnumerable()
                        group row by row.Field<string>("PieRecordID") into PieRecordID
                        orderby PieRecordID.Key
                        select new
                        {
                            Name = PieRecordID.Key,
                            CountOfPieRecordID = PieRecordID.Count()
                        };

            foreach (var PieRecordID in query)
            {
                foreach (DataRow row in dtTemp.Rows)
                {
                    if (row["PieRecordID"].ToString() == PieRecordID.Name)
                    {
                        dtKirim.Rows.Add(row["PieRecordID"], row["Block"], PieRecordID.CountOfPieRecordID);
                        dtKirim = addBlockIplas(dtMasterIplas, dtKirim, row["PieRecordID"].ToString(), PieRecordID.CountOfPieRecordID);
                        break;
                    }
                }
            }

            return converter.ObjectToJSON(dtKirim);
        }

        private DataTable getMaxBerturutSering(List<int> listUrut, DataTable dtTemp, DataRow rowX)
        {
            int nilaiMax = 0;
            foreach (int i in listUrut)
            {
                if (nilaiMax < i)
                {
                    nilaiMax = i;
                }
            }

            for (int i = 1; i < nilaiMax; i++)
            {
                dtTemp.Rows.Add(rowX.ItemArray);
            }
            return dtTemp;
        }

        [WebMethod]
        public string GetWMAreaByEstCode(string estCode)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataSet ds = mapper.getWMAreaByEstCode(estCode);
            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    DataTable dtEstateWM = mapper.getAllEstateByWMID(dt.Rows[0]["idWMArea"].ToString());
                    return converter.ObjectToJSON(dtEstateWM);
                }
            }
            return "";
        }

        [WebMethod]
        public String ListWMArea()
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.getAllWMArea();
            return converter.ObjectToJSON(dt);
        }

        [WebMethod]
        public String ListWMAreaZone(int idWMArea)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.getAllWMAreaZone(idWMArea);
            return converter.ObjectToJSON(dt);
        }

        private DataTable addBlockIplas(DataTable dtMappingPiezoRecordIPLAS, DataTable dtQuery, String PieRecordID, int banyak)
        {
            var q = from row in dtMappingPiezoRecordIPLAS.AsEnumerable()
                    where row.Field<string>("PieRecordID") == PieRecordID
                    select new
                    {
                        PieRecordID = row.Field<string>("PieRecordID"),
                        Block = row.Field<string>("Block")
                    };

            foreach (var item in q)
            {
                DataRow newRow = dtQuery.NewRow();
                newRow["PieRecordID"] = item.PieRecordID;
                newRow["Block"] = item.Block;
                newRow["Banyak"] = banyak;
                dtQuery.Rows.Add(newRow);
            }
            return dtQuery;
        }
        [WebMethod]
        public String ListRambu(string wmaCode)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.GetRambuAir(wmaCode);
            return converter.ObjectToJSON(dt);
        }
        [WebMethod]
        public string ListWeekNumber(int bulan, int tahun, int minggu)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.GetWeek(minggu, bulan, tahun);
            return converter.ObjectToJSON(dt);
        }
        [WebMethod]
        public string GetInitialGraph(string week, int month, int year, string wmaCode, string ra_code, float minRangeZone, float maxRangeZone, string estCode)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataSet ds = mapper.PerbandinganCurahHujan(wmaCode, ra_code, minRangeZone, maxRangeZone, year, month, week, estCode);
            DataTable dt = ds.Tables[0];
            //string[] tempra_code = ra_code.Split(',');
            //for (int i = 0; i < tempra_code.Length; i++)
            //{
            //     ds = mapper.PerbandinganCurahHujan(wmaCode, ra_code, minRangeZone, maxRangeZone, year, month, week);
            //     dt = ds.Tables[0];

            //}
            return converter.ObjectToJSON(dt);
        }
        [WebMethod]
        public string GetExtentCoordinate(string wmaCode)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.GetExtendCoordinate(wmaCode);
            return converter.ObjectToJSON(dt);
        }
        [WebMethod]
        public string insertNewParameter(string wmaCode, float minRange, float maxRange, string curahHujan, string rambuAir)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapperTambahan = new PZO_ProjectMapperTambahan();
            string b = mapperTambahan.insertParamForAnalisisTmas(wmaCode, minRange, maxRange, curahHujan, rambuAir);
            return b;
        }
        [WebMethod]
        public string setParameterInitial(string wmaCode)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapperTambahan = new PZO_ProjectMapperTambahan();
            DataTable dt = mapperTambahan.getMinMaxValue(wmaCode);
            return converter.ObjectToJSON(dt);
        }
        [WebMethod]
        public string ListCurahHujan(int idWmarea)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.GetListCurahHujan(idWmarea);
            return converter.ObjectToJSON(dt);
        }
        [WebMethod]
        public string ListZonaByWmArea(int idWmarea)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataTable dt = mapper.GetZonaByWmArea(idWmarea);
            return converter.ObjectToJSON(dt);

        }
        [WebMethod]
        public string GetMasterRambuAir(string wmaCode, int week, int month, int idWmArea, int year)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataSet ds = mapper.newGetGraphByZona(wmaCode, week, month, idWmArea, year);
            DataTable dtRambuAir = ds.Tables[0];

            List<String> rambuAir = new List<String>();

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dtRambuAir.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtRambuAir.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        [WebMethod]
        public string GetChartTMAS(string wmaCode, int week, int month, int idWmArea, int year)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataSet ds = mapper.newGetGraphByZona(wmaCode, week, month, idWmArea, year);

            DataTable dtCurahHujan = ds.Tables[1];
            List<String> rambuAir = new List<String>();

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dtCurahHujan.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtCurahHujan.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        //public string GetChartTMAS(string wmaCode, int week, int month, int idWmArea, int year) //sp function 38
        //{
        //    PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
        //    DataSet ds = mapper.newGetGraphByZona(wmaCode,week,month,idWmArea,year);
        //    DataTable dtRambuAir = ds.Tables[1];
        //    DataTable dtCurahHujan = ds.Tables[2];
        //    List<String> rambuAir = new List<String>();
        //    foreach (DataRow row in dtRambuAir.Rows)
        //    {
        //        rambuAir.Add(row["stationName"].ToString());
        //    }
        //    for (int i = 0; i < rambuAir.Count; i++)
        //    {
        //        string rambuFix = rambuAir[i];
        //        DataColumnCollection columns = ds.Tables[2].Columns;
        //        if (columns.Contains(rambuFix))
        //        {

        //        }
        //    }
        //    JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
        //    List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
        //    Dictionary<string, object> childRow;
        //    //foreach (DataRow row in dt.Rows)
        //    //{
        //    //    childRow = new Dictionary<string, object>();
        //    //    foreach (DataColumn col in dt.Columns)
        //    //    {
        //    //        childRow.Add(col.ColumnName, row[col]);
        //    //    }
        //    //    parentRow.Add(childRow);
        //    //}
        //    //return jsSerializer.Serialize(parentRow);
        //    return null;
        //}
        [WebMethod]
        public string GetEstateDetail(string estCode)
        {
            string hasil = "";
            try
            {
                JsonConverter converter = new JsonConverter();
                //PZO_PiezometerMapper mapper = new PZO_PiezometerMapper();
                //PZO_PiezometerDomain domain = new PZO_PiezometerDomain();
                //domain.EstCode = estCode;
                PZO_ProjectMapper mapper = new PZO_ProjectMapper();
                DataSet ds = mapper.GetEstateDetail(estCode);
                DataTable dtExtentCoordinate = ds.Tables[0];
                hasil = converter.ObjectToJSON(dtExtentCoordinate);
            }
            catch (Exception ex)
            {
                hasil = ex.Message;
            }
            return hasil;

        }
        [WebMethod]
        public string GetKetinggianByPieRecordId(string wmaCode, int week, int month, int idWmArea, int year, string pieRecordID)
        {
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataSet ds = mapper.GetgraphbyPiezoRecordID(wmaCode, week, month, idWmArea, year, pieRecordID);

            DataTable dtCurahHujan = ds.Tables[0];
            List<String> rambuAir = new List<String>();

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dtCurahHujan.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtCurahHujan.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        [WebMethod]
        public string UpdateBlock(string wmaCode, string block)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapperTambahan = new PZO_ProjectMapperTambahan();
            string b = mapperTambahan.updateBlock(wmaCode, block);
            return b;
        }
        [WebMethod]
        public string getPencatatanPerubahanPiezometer(string weekId, string wmarea, string estCode, string pulau)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataSet ds = mapper.getPencatatanPerubahanPiezometer(weekId, wmarea, estCode, pulau);
            return converter.ObjectToJSON(ds.Tables[0]);
        }
        [WebMethod]
        public string getEstateByWMArea(string wmArea)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataSet ds = mapper.getEstateByWmaArea(wmArea);
            return converter.ObjectToJSON(ds.Tables[0]);
        }
        [WebMethod]
        public string[] getDataPerubahanTMAT(string WMACode, string weekName, int flagKlhk, string estCode, string pulau)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataSet ds = new DataSet();
            string[] hasil = new string[2];
            if (flagKlhk == 1)
            {
                ds = mapper.getPencatatanPerubahanPiezometerKLHK(WMACode, weekName, estCode, pulau);
            }
            else if (flagKlhk == 0)
            {
                ds = mapper.getPencatatanPerubahanPiezometer(WMACode, weekName, estCode, pulau);
            }
            else if (flagKlhk == 2)
            {
                ds = mapper.getPencatatanPerubahanPiezometerAll(WMACode, weekName, estCode, pulau);
            }
            hasil[0] = converter.ObjectToJSON(ds.Tables[1]);
            hasil[1] = converter.ObjectToJSON(ds.Tables[0]);
            return hasil;

                //return converter.ObjectToJSON(ds.Tables[1]);
        }
        [WebMethod]
        public string getBlockPerubahan(int IdWeek, string wmarea, string statusKetinggian, string statusperubahan, int flagKlhk, string estCode, string pulau)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            DataSet ds = new DataSet();
            if (flagKlhk == 1)
            {
                ds = mapper.getBlockPerubahanTMATKLHK(IdWeek, wmarea, statusKetinggian, statusperubahan, estCode, pulau);
            }
            else if (flagKlhk == 0)
            {
                ds = mapper.getBlockPerubahanTMAT(IdWeek, wmarea, statusKetinggian, statusperubahan, estCode, pulau);
            }
            else if (flagKlhk == 2)
            {
                ds = mapper.getBlockPerubahanTMATAll(IdWeek, wmarea, statusKetinggian, statusperubahan, estCode, pulau);
            }

            return converter.ObjectToJSON(ds.Tables[0]);
        }
        [WebMethod]
        public string[] getChartTMAT(int idWeek, int idWeekBefor, string wmcode)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();

            string[] hasil = new string[15];
            int idx = 0;
            for (int i = idWeekBefor; i <= idWeek; i++)
            {

                DataSet ds = mapper.getChartTMAT(idWeekBefor, idWeek, i, wmcode);
                if (idx == 0)
                {
                    hasil[idx] = converter.ObjectToJSON(ds.Tables[0]);
                    idx++;
                }
                if (idx == 1)
                {
                    hasil[idx] = converter.ObjectToJSON(ds.Tables[1]);
                    idx++;
                }
                hasil[idx] = converter.ObjectToJSON(ds.Tables[2]);
                idx++;
            }
            return hasil;
        }

        [WebMethod]
        public string insertChartTMATPiezo(string data)
        {
            JsonConverter converter = new JsonConverter();
            PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
            
            JObject obj1 = JObject.Parse(data.ToString());
            JArray jArray = JArray.Parse(obj1["listPiezo"].ToString());

            DataSet ds = mapper.cekZonaBalckList(obj1["zona"].ToString());

            for(int i = 0; i < jArray.Count; i++)
            {
                JObject obj = JObject.Parse(jArray[i].ToString());
                mapper.insertChartTMATPiezo(obj1["zona"].ToString(), obj["estCode"].ToString(), obj["block"].ToString(), obj["PieRecordID"].ToString(), obj["check"].ToString().ToLower(), obj1["userId"].ToString());
            }


            return converter.ObjectToJSON(ds.Tables[0]);
        }



        //public string GetInitialGraph2(string week, int month, int year, string wmaCode, string ra_code, float minRangeZone, float maxRangeZone)
        //{
        //    JsonConverter converter = new JsonConverter();
        //    PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
        //    DataSet ds = mapper.PerbandinganCurahHujan(wmaCode, ra_code, minRangeZone, maxRangeZone, year, month, week);
        //    DataTable dt = ds.Tables[0];
        //    //string[] tempra_code = ra_code.Split(',');
        //    //for (int i = 0; i < tempra_code.Length; i++)
        //    //{
        //    //     ds = mapper.PerbandinganCurahHujan(wmaCode, ra_code, minRangeZone, maxRangeZone, year, month, week);
        //    //     dt = ds.Tables[0];

        //    //}
        //    return converter.ObjectToJSON(dt);
        //}


        //}
    }
}
