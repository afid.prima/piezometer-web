﻿
using System;
using System.Web;
using System.Data;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using DevExpress.XtraReports.UI;
using PASS.Domain;
using PASS.Mapper;
using System.Web.Services;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for Download
    /// </summary>
    public class Download : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            DownloadTemplate(context);
        }

        private void DownloadTemplate(HttpContext context)
        {
            DownloadFile("excel", context);

            //group footer init

        }
        private void DownloadFile(string extention, HttpContext context)
        {
            string prefixfilenames = DateTime.Now.ToString("ddMMyyyy_HHmm");
            var ext = extention;
            if (ext == "excel")
            {
                ext = "xlsx";
            }

            string filename = "TemplateGenerateKLHK.xlsx";

            byte[] bytesInStream = null;
            string fullpath = context.Server.MapPath("..") + "\\Attachment\\DownloadFile\\" + filename;
            MemoryStream ms = new MemoryStream();

            bytesInStream = ms.ToArray();
            ms.Close();

            context.Response.ClearContent();
            context.Response.ClearHeaders();
            context.Response.ContentType = "application/" + extention.ToLower();
            context.Response.AddHeader("content-disposition", "attachment; filename=" + filename);
            //context.Response.BinaryWrite(bytesInStream);
            context.Response.TransmitFile(fullpath);
            context.Response.Flush();
            context.Response.Close();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}