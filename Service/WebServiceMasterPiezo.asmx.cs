﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using PZO_PiezometerOnlineMap.Mapper;
using Newtonsoft.Json.Linq;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for WebServiceMasterPiezo
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebServiceMasterPiezo : System.Web.Services.WebService
    {

        PZO_MasterPiezo masterPiezo = new PZO_MasterPiezo();
        PZO_IOT_Mapper masterPZOIOT = new PZO_IOT_Mapper();

        [WebMethod]
        public string ListHistoryDAftarPizometer(String estCode,String type)
        {
            DataTable ds = masterPiezo.ListHistoryDAftarPizometer(type, estCode).Tables[0];
            ds.Columns.Add("Distance");

            String a;
            String b;
            String c;
            String d;

            for (int i = 0; i < ds.Rows.Count; i++)
            {
                a = ds.Rows[i]["CoordinateX"].ToString();
                b = ds.Rows[i]["CoordinateY"].ToString();
                d = ds.Rows[i]["Jenis"].ToString();

                if (a != "" && b != "")
                {
                    ds.Rows[i]["Distance"] = Math.Round(1000 * (DistanceTo(Convert.ToDouble(ds.Rows[i]["Latitude"].ToString()), Convert.ToDouble(ds.Rows[i]["Longitude"].ToString()), Convert.ToDouble(ds.Rows[i]["CoordinateY"].ToString()), Convert.ToDouble(ds.Rows[i]["CoordinateX"].ToString()))), 2);
                }
                else
                {
                    ds.Rows[i]["Distance"] = "-";
                }

            }
            return dataTableToJson(ds);
        }

        [WebMethod]
        public string ListHistorySummaryPizometer(String estCode)
        {
            DataTable ds = masterPiezo.ListHistorySummaryPizometer(estCode).Tables[0];
            return dataTableToJson(ds);
        }

        [WebMethod]
        public string ListPizometerEditHistory(String estCode,String idUnitPemantauan)
        {
            DataTable ds = masterPiezo.ListPizometerEditHistory(estCode, idUnitPemantauan).Tables[0];
            return dataTableToJson(ds);
        }

        [WebMethod]
        public string getAllIotTMATByEstate(String estCode) 
        {
            DataTable ds = masterPZOIOT.getAllIotTMATByEstate(estCode).Tables[0];
            return dataTableToJson(ds);
        }

        [WebMethod]
        public string getPiezoMeasure(String piezoId, String dateParam, String dateParam2)
        {
            DataTable ds = masterPZOIOT.getPiezoMeasure(piezoId, dateParam, dateParam2).Tables[0];
            return dataTableToJson(ds);
        }
        [WebMethod]
        public string[] getDetailCalibrate(String piezoId, String dateParam)
        {
            string[] hasil = new string[2];
            DataSet ds = masterPZOIOT.getDetailCalibrate(piezoId, dateParam);
            hasil[0] = dataTableToJson(ds.Tables[0]);
            hasil[1] = dataTableToJson(ds.Tables[1]);
            return hasil;
        }

        [WebMethod]
        public string getIOTPiezo(String deviceId,String piezoId,String type,float calibrasi ,String dateParam, String dateParam2)
        {
            DataTable dt = new DataTable();
            if(type.ToLower() == "mertani")
            {
                dt = masterPZOIOT.getIOTPiezoMertani(deviceId, piezoId, calibrasi, dateParam, dateParam2).Tables[0];
            }else
            {
                dt = masterPZOIOT.getIOTPiezoHolykel(deviceId, piezoId, calibrasi, dateParam, dateParam2).Tables[0];
            }
            return dataTableToJson(dt);
        }

        [WebMethod]
        public string savePZOIOT(String data)
        {
            DataTable dt = new DataTable();
            JObject jObj = JObject.Parse(data.ToString());
            JArray jArr = JArray.Parse(jObj["data"].ToString());

            for (int i = 0; i < jArr.Count; i++)
            {
                JObject jObjX = JObject.Parse(jArr[i].ToString());
                dt = masterPZOIOT.validasiSavePZOIOT(
                    jObjX["piezoId"].ToString(),
                    jObjX["deviceId"].ToString(),
                    jObj["estCode"].ToString(),
                    jObjX["dateParam"].ToString(),
                    jObjX["dateParam2"].ToString()
                ).Tables[0];

                if (dt.Rows.Count > 0)
                {
                    break;
                }
            }

            if (dt.Rows.Count > 0)
            {
                return dataTableToJson(dt);
            }

            masterPZOIOT.deletePZOIOT(jArr[0]["piezoId"].ToString());

            for (int i = 0; i < jArr.Count; i++)
            {
                JObject jObjX = JObject.Parse(jArr[i].ToString());
                masterPZOIOT.savePZOIOT(
                    jObj["userId"].ToString(),
                    jObj["estCode"].ToString(),
                    jObjX["type"].ToString(),
                    jObjX["deviceId"].ToString(),
                    jObjX["piezoId"].ToString(),
                    jObjX["dateParam"].ToString(),
                    jObjX["dateParam2"].ToString(),
                    float.Parse(jObjX["calibrasi"].ToString())
                );
            }

            masterPZOIOT.savePZOIOTHistory(jArr[0]["piezoId"].ToString());
            return dataTableToJson(dt);
        }

        [WebMethod]
        public string getPZOIOTStation(String piezoId)
        {
            DataTable dt = masterPZOIOT.getPZOIOTStation(piezoId).Tables[0];
            return dataTableToJson(dt);
        }

        [WebMethod]
        public string saveDataUnitPemantauaan(String data)
        {
            DataTable dt = new DataTable();
            JObject jObj = JObject.Parse(data.ToString());
            JArray jArr = JArray.Parse(jObj["detailBlockPemantauan"].ToString());

            String idMappingBlockUnitpemantauan = "";
            String UnitPemantauan;
            String KodePerubahan;
            String IsDefault;
            String IsActive;
            String IsActive2;
            String IDUnitPemantauan = "";
            DateTime dateTime;
            DateTime convertedStardate;
            Int32 unixTimestamp;
            String Date;
            String StartDate;

            if (jObj["jenisPiezo"].ToString() != "0")
            {
                if (jObj["isActive"].ToString().ToUpper() == "TRUE")
                {
                    IsActive = "TRUE";

                    if (jObj["isActive"].ToString().ToUpper() != jObj["isActivePrev"].ToString())
                    {
                        KodePerubahan = "1";
                    }

                    IsActive2 = jArr[0]["isActive"].ToString().ToUpper();
                    IsDefault = jArr[0]["isDefault"].ToString().ToUpper();

                    for (int i = 0; i < jArr.Count; i++)
                    {
                        idMappingBlockUnitpemantauan = jArr[i]["idMappingBlockUnitPemantauan"].ToString();
                        UnitPemantauan = jObj["idPemantauaan"].ToString();

                        //RadioButton rdCheck = (RadioButton)dgi.FindControl("rbContenUploadSKTMATColumn3");
                        //RadioButton rdCheckk = (RadioButton)dgi.FindControl("RadioButton1");
                        //CheckBox cboCheck = (CheckBox)dgi.FindControl("cboContenUploadSKTMATColumn2");
                        //Label lblBlock = (Label)dgi.FindControl("lblContenUploadSKTMATColumn1");
                        //CheckBox cboIsActive1 = (CheckBox)dgi.FindControl("cboContenUploadSKTMATColumn6");
                        //CheckBox cboIUnitPemantauan = (CheckBox)dgi.FindControl("cboContenUploadSKTMATColumn8");
                        //CheckBox cboIUnitPemantauan2 = (CheckBox)dgi.FindControl("cboContenUploadSKTMATColumn9");
                        //Label lblStatus = (Label)dgi.FindControl("lblContenUploadSKTMATColumn4");

                        dateTime = DateTime.Now;
                        //objPiezometerDomain.Date = dateTime.ToString();
                        convertedStardate = DateTime.Parse(dateTime.ToString());
                        convertedStardate = convertedStardate.AddHours(-7);

                        if (jArr[i]["isDefault"].ToString().ToUpper() == "TRUE")
                        {
                            IsDefault = "TRUE";
                            if (jArr[i]["block"].ToString().ToUpper() == jObj["block"].ToString().ToUpper())
                            {
                                KodePerubahan = "1";
                            }
                            else
                            {
                                KodePerubahan = "3";
                            }
                        }
                        else
                        {
                            IsDefault = "FALSE";
                        }

                        if (jArr[i]["isActive"].ToString().ToUpper() == "TRUE")
                        {
                            IsActive2 = "TRUE";
                            if (jArr[i]["isActive"].ToString().ToUpper() != jArr[i]["isActive2"].ToString().ToUpper())
                            {
                                KodePerubahan = "4";
                            }
                        }
                        else
                        {
                            IsActive2 = "FALSE";
                            if (jArr[i]["isActive"].ToString().ToUpper() != jArr[i]["isActive2"].ToString().ToUpper())
                            {
                                KodePerubahan = "5";
                            }
                        }

                        if (jArr[i]["lepas"].ToString().ToUpper() == "TRUE")
                        {
                            //if (cboIUnitPemantauan.Checked != cboIUnitPemantauan2.Checked)
                            //{
                            KodePerubahan = "6";
                            //}
                            IDUnitPemantauan = jObj["idPemantauaan"].ToString();
                        }
                        else
                        {
                            //if (cboIUnitPemantauan.Checked != cboIUnitPemantauan2.Checked)
                            //{
                            KodePerubahan = "7";
                            //}
                            IDUnitPemantauan = "";
                        }

                        //ID = idMappingBlockUnitpemantauan;
                        //Jenis = ddlJenis.SelectedValue;
                        //Block1 = lblBlock.Text;
                        //Block2 = txtBlock.Text;
                        //PieRecordID = UnitPemantauan;

                        unixTimestamp = (Int32)(convertedStardate.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        Date = unixTimestamp.ToString() + "000";

                        if (KodePerubahan != "")
                        {
                            masterPiezo.bindPiezometerEditTMAT(idMappingBlockUnitpemantauan, IDUnitPemantauan, jObj["idPemantauaan"].ToString(), jObj["block"].ToString(), Date, IsActive2, jObj["keterangn"].ToString(), jObj["keterangn"].ToString(), IsDefault, IsActive, jObj["alias"].ToString(), jObj["userid"].ToString(), jObj["jenisPiezo"].ToString());
                            masterPiezo.bindPiezometerInsertHistory(idMappingBlockUnitpemantauan, IDUnitPemantauan,Date, KodePerubahan, jObj["block"].ToString(), jObj["keterangn"].ToString(), jObj["estCode"].ToString(), IsActive, jObj["userid"].ToString());
                        }
                    }

                    dateTime = DateTime.Now;
                    convertedStardate = DateTime.Parse(dateTime.ToString());
                    convertedStardate = convertedStardate.AddHours(-7);

                    unixTimestamp = (Int32)(convertedStardate.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    Date = unixTimestamp.ToString() + "000";

                    if (jObj["jenisPiezo"].ToString() != jObj["jenisPiezoPrev"].ToString())
                    {
                        KodePerubahan = "8";
                        masterPiezo.bindPiezometerEditTMAT(idMappingBlockUnitpemantauan, IDUnitPemantauan, jObj["idPemantauaan"].ToString(), jObj["block"].ToString(), Date, IsActive2, jObj["keterangn"].ToString(), jObj["keterangn"].ToString(), IsDefault, IsActive, jObj["alias"].ToString(), jObj["userid"].ToString(), jObj["jenisPiezo"].ToString());
                        masterPiezo.bindPiezometerInsertHistory(idMappingBlockUnitpemantauan, IDUnitPemantauan, Date, KodePerubahan, jObj["block"].ToString(), jObj["keterangn"].ToString(), jObj["estCode"].ToString(), IsActive, jObj["userid"].ToString());
                    }
                    if (jObj["isActive"].ToString().ToUpper() != jObj["isActivePrev"].ToString())
                    {
                        KodePerubahan = "1";
                        masterPiezo.bindPiezometerEditTMAT(idMappingBlockUnitpemantauan, IDUnitPemantauan, jObj["idPemantauaan"].ToString(), jObj["block"].ToString(), Date, IsActive2, jObj["keterangn"].ToString(), jObj["keterangn"].ToString(), IsDefault, IsActive, jObj["alias"].ToString(), jObj["userid"].ToString(), jObj["jenisPiezo"].ToString());
                        masterPiezo.bindPiezometerInsertHistory(idMappingBlockUnitpemantauan, IDUnitPemantauan, Date, KodePerubahan, jObj["block"].ToString(), jObj["keterangn"].ToString(), jObj["estCode"].ToString(), IsActive, jObj["userid"].ToString());
                    }

                }
                else
                {
                    for (int i = 0; i < jArr.Count; i++)
                    {

                        idMappingBlockUnitpemantauan = jArr[i]["idMappingBlockUnitPemantauan"].ToString();
                        //RadioButton rdCheck = (RadioButton)dgi.FindControl("rbContenUploadSKTMATColumn3");
                        //RadioButton rdCheckk = (RadioButton)dgi.FindControl("RadioButton1");
                        //CheckBox cboCheck = (CheckBox)dgi.FindControl("cboContenUploadSKTMATColumn2");
                        //Label lblBlock = (Label)dgi.FindControl("lblContenUploadSKTMATColumn1");
                        //CheckBox cboIsActive1 = (CheckBox)dgi.FindControl("cboContenUploadSKTMATColumn6");
                        //CheckBox cboIUnitPemantauan = (CheckBox)dgi.FindControl("cboContenUploadSKTMATColumn8");
                        //CheckBox cboIUnitPemantauan2 = (CheckBox)dgi.FindControl("cboContenUploadSKTMATColumn9");
                        //Label lblStatus = (Label)dgi.FindControl("lblContenUploadSKTMATColumn4");

                        DateTime now = DateTime.Now;

                        DateTime dateTime1 = DateTime.Now;
                        IsDefault = "TRUE";
                        StartDate = now.ToString();
                        Date = dateTime1.ToString();

                        convertedStardate = DateTime.Parse(StartDate);
                        convertedStardate = convertedStardate.AddHours(-7);

                        DateTime convertedDate = DateTime.Parse(Date);
                        convertedDate = convertedDate.AddHours(-7);

                        unixTimestamp = (Int32)(convertedDate.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        Int32 unixTimestampp = (Int32)(convertedStardate.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        Date = unixTimestamp.ToString() + "000";
                        if (jArr[i]["lepas"].ToString().ToUpper() == "TRUE")
                        {
                            //if (cboIUnitPemantauan.Checked != cboIUnitPemantauan2.Checked)
                            //{
                            KodePerubahan = "6";
                            //}
                            IDUnitPemantauan = jObj["idPemantauaan"].ToString();
                        }
                        else
                        {
                            //if (cboIUnitPemantauan.Checked != cboIUnitPemantauan2.Checked)
                            //{
                            KodePerubahan = "7";
                            //}
                            IDUnitPemantauan = "";
                        }
                        if (jArr[i]["isDefault"].ToString().ToUpper() == "FALSE" && jArr[i]["lepas"].ToString().ToUpper() == "FALSE")
                        {
                            IsDefault = "FALSE";
                        }
                        //objPiezometerDomain.ID = idMappingBlockUnitpemantauan;
                        //objPiezometerDomain.Block1 = lblBlock.Text;
                        //objPiezometerDomain.Date = unixTimestamp.ToString() + "000";
                        //objPiezometerDomain.StartDate = unixTimestampp.ToString() + "000";
                        //objPiezometerDomain.UserIDEditor = jObj["userid"].ToString();
                        //objPiezometerDomain.EstCode = jObj["estCode"].ToString();
                        //objPiezometerDomain.KodePerubahan = "2";
                        //objPiezometerDomain.PieRecordID = txtPemantauan.Text;
                        IsActive = "FALSE";

                        //objPiezometerDomain.Block2 = txtBlock.Text;
                        //objPiezometerDomain.Remarks = txtremarks.Text;
                        //objPiezometerDomain.Remarks2 = txtremarks.Text;
                        IsActive2 = "FALSE";
                        //objPiezometerDomain.Alias = txtAlias.Text;
                        //if (rdCheck.Checked && cboCheck.Checked == false && cboIUnitPemantauan.Checked || rdCheck.Checked == false && cboIUnitPemantauan.Checked == false && cboCheck.Checked)
                        //{
                        masterPiezo.bindPiezometerInsertHistory(idMappingBlockUnitpemantauan, IDUnitPemantauan, Date, KodePerubahan, jObj["block"].ToString(), jObj["keterangn"].ToString(), jObj["estCode"].ToString(), IsActive, jObj["userid"].ToString());
                        //}
                        masterPiezo.bindPiezometerEditTMAT(idMappingBlockUnitpemantauan, IDUnitPemantauan, jObj["idPemantauaan"].ToString(), jObj["block"].ToString(), Date, IsActive2, jObj["keterangn"].ToString(), jObj["keterangn"].ToString(), IsDefault, IsActive, jObj["alias"].ToString(), jObj["userid"].ToString(), jObj["jenisPiezo"].ToString());
                    }
                }
                //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "data()", true);
            }
            return dataTableToJson(dt);
        }

        public static double DistanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;

            }

            return dist;
        }

        public string dataTableToJson(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();

            jsSerializer.MaxJsonLength = Int32.MaxValue;
            
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }

            return jsSerializer.Serialize(parentRow);
        }
    }
}
