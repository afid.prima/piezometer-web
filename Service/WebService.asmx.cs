﻿using System;
using System.Data;
using System.Linq;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using PZO_PiezometerOnlineMap.Mapper;
using PZO_PiezometerOnlineMap.Class;
using PASS.Domain;
using PASS.Mapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Collections;
using pass.huruf;
using System.Web.Configuration;
using System.Web.Security;
using PPB_Project;

namespace PZO_PiezometerOnlineMap.Service
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]

    public class WebService : System.Web.Services.WebService
    {
        private pass.huruf.Enkripsi login = new pass.huruf.Enkripsi();
        private COR_UserMapper userObjMapper = new COR_UserMapper();
        private COR_userDomain domain = new COR_userDomain();
        private pass.huruf.Enkripsi url = new pass.huruf.Enkripsi();
        //private Email email = new Email();


        protected string dataTableToJson(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string[] ListEstate(string CompanyCode)
        {
            COR_EstateMapper mapper = new COR_EstateMapper();
            var q = from myRow in mapper.ListData(1).Tables[0].AsEnumerable()
                    where myRow.Field<string>("CR_Code").Contains(CompanyCode)
                    select myRow;

            DataView dv = q.CopyToDataTable().DefaultView;
            DataTable dtListEstate = dv.ToTable(true, "EstNewCode", "NewEstName");

            string[] list = new string[2];
            list[0] = "";
            list[1] = "";
            foreach (DataRow row in dtListEstate.Rows)
            {
                if (list[0] == "")
                    list[0] = row["EstNewCode"].ToString();
                else
                    list[0] += ";" + row["EstNewCode"].ToString();

                if (list[1] == "")
                    list[1] = row["NewEstName"].ToString();
                else
                    list[1] += ";" + row["NewEstName"].ToString();
            }

            return list;
        }

        [WebMethod]
        public string GetDataDashboardPerCompany(string dateString, string companycode)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetDataDashboardPerCompany(dateString, companycode);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetDataDashboardAll(string dateString)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetDataDashboardAll(dateString);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetDataDashboardAllWeek(string dateString, string dateString2)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetDataDashboardAllWeek(dateString, dateString2);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetDataDashboardPerCompanyWeek(string dateString, string dateString2, string companycode)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetDataDashboardPerCompanyWeek(dateString, dateString2, companycode);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetAllStationbyEstateActive(string estCode)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetAllStationbyEstateActive(estCode);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetAllStationbyEstate(string estCode)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetAllStationbyEstate(estCode);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetStationMeasure(string stationId)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetStationMeasure(stationId);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetStationImages(string stationId)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetStationImages(stationId);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetMeasureImages(string measureId)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetMeasureImages(measureId);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string SaveNewMeasure(string obj)
        {
            //WLR_Mapper mapper = new WLR_Mapper();
            //Helper.SaveImage saveImage = new Helper.SaveImage();
            //JObject json = JObject.Parse(obj.ToString());
            //DataTable dt = mapper.SaveNewMeasure(json).Tables[0];

            //string measurementId = dt.Rows[0]["measurementId"].ToString();

            //JArray jArray = JArray.Parse(json["FotoMeasure"].ToString());
            //for (int i = 0; i < jArray.Count; i++)
            //{
            //    JObject objX = JObject.Parse(jArray[i].ToString());
            //    if (objX["filename"] != null)
            //    {
            //        saveImage.saveImage(objX["content"].ToString(), json["RecordStationId"].ToString(), measurementId, objX["filename"].ToString());
            //        mapper.InsertImageNewMeasure(objX, json, measurementId);
            //    }
            //}
            return "Berhasil";
        }

        [WebMethod]
        public string SaveEditMeasure(string obj)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JObject json = JObject.Parse(obj.ToString());
            int berhasil = mapper.InsertHistoryMeasure(json);
            if (berhasil == 1)
            {
                berhasil = mapper.UpdateMeasure(json);
                if (berhasil == 1)
                {
                    if (json["petugas"] != null)
                    {
                        mapper.editSurveyorMeasure(json);
                    }
                    return "Berhasil";
                }
            }
            return "Gagal";
        }
        [WebMethod]

        public string addMeasure(string obj)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JObject json = JObject.Parse(obj.ToString());
            try
            {
                mapper.InsertNewMeasure(json);
                return "sukses";
            }
            catch (Exception ex)
            {

                throw ex;
                return "error";
            }
            //if (berhasil == 1)
            //{
            //    return "Berhasil";
            //}
            //return "gagal";
        }

        [WebMethod]
        public string GetHistoryMeasure(String measureId)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.GetHistoryMeasure(measureId);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string EditStation(string obj)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JObject json = JObject.Parse(obj.ToString());
            mapper.UpdateStation(json);
            return "Berhasil";
        }

        [WebMethod]
        public string GetMasterZona(string companyCode)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.ListZona(companyCode);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetGraphByWMArea(string wmArea)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetGraphByWMArea(wmArea);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetMasterZonaByWMArea(string wmArea)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetMasterZonaByWMArea(wmArea);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetRambuAirByWMArea(string wmArea)
        {
            WLR_Mapper mapper = new WLR_Mapper();

            DataTable dt = mapper.GetRambuByWMAreaForGraph(wmArea);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetStationMeasureArray(string stationSelected)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JArray JArray = JArray.Parse(stationSelected.ToString());
            JArray JArraySend = new JArray();
            double elvTanah = 0;
            int frequency = 0;
            string daftarStationId = "";
            for (int i = 0; i < JArray.Count; i++)
            {
                daftarStationId = daftarStationId + JArray[i]["stationId"].ToString() + ",";
            }
            DataTable dt = mapper.GetStationMeasureOptimizm(daftarStationId);
            DataTable dtSend = new DataTable();
            dtSend.Columns.Add("measurementId");
            dtSend.Columns.Add("measurementDate");
            dtSend.Columns.Add("Nilai");
            string stationIdx = "";
            for (int j = 0; j < dt.Rows.Count; j++)
            {

                if (stationIdx != "")
                {
                    if (stationIdx != dt.Rows[j]["stationId"].ToString())
                    {

                        JObject jobj = new JObject();

                        for (int i = 0; i < JArray.Count; i++)
                        {
                            if (JArray[i]["stationId"].ToString() == stationIdx)
                            {
                                jobj["stationId"] = JArray[i]["stationId"].ToString();
                                jobj["stationName"] = JArray[i]["stationName"].ToString();
                                jobj["sortField"] = JArray[i]["sortField"].ToString();
                                jobj["color"] = JArray[i]["color"].ToString();
                                break;
                            }
                        }

                        jobj["elevasiTanah"] = elvTanah;
                        jobj["frequency"] = frequency;
                        jobj["measure"] = JsonConvert.SerializeObject(dtSend, Formatting.None);
                        dtSend.Clear();

                        JArraySend.Add(jobj);
                    }
                }

                double nilai = 0;
                DataRow dr = dt.Rows[j];
                if ((dr["dynamicMeasurement"].ToString() != "") && (dr["status"].ToString() == "1" || dr["status"].ToString() == ""))
                {
                    JArray textArray = JArray.Parse(dr["dynamicMeasurement"].ToString());
                    for (int k = 0; k < textArray.Count; k++)
                    {
                        if (Convert.ToInt32(textArray[k]["questionId"].ToString()) == 1)
                        {
                            nilai = Convert.ToDouble(textArray[k]["answer"].ToString());
                        }
                    }

                    dtSend.Rows.Add(dr["measurementId"], dr["measurementDate"], nilai);
                }

                stationIdx = dt.Rows[j]["stationId"].ToString();
                elvTanah = Convert.ToDouble(dt.Rows[j]["elevasiTanah"].ToString());
                frequency = Convert.ToInt16(dt.Rows[j]["frequency"].ToString());
            }

            if (dtSend.Rows.Count > 0)
            {

                JObject jobj1 = new JObject();

                for (int i = 0; i < JArray.Count; i++)
                {
                    if (JArray[i]["stationId"].ToString() == stationIdx)
                    {
                        jobj1["stationId"] = JArray[i]["stationId"].ToString();
                        jobj1["stationName"] = JArray[i]["stationName"].ToString();
                        jobj1["sortField"] = JArray[i]["sortField"].ToString();
                        jobj1["color"] = JArray[i]["color"].ToString();
                        break;
                    }
                }

                jobj1["elevasiTanah"] = elvTanah;
                jobj1["frequency"] = frequency;
                jobj1["measure"] = JsonConvert.SerializeObject(dtSend, Formatting.None);
                dtSend.Clear();

                JArraySend.Add(jobj1);
            }

            //for (int i = 0; i < JArray.Count; i++)
            //{
            //    DataTable dt = mapper.GetStationMeasure(JArray[i]["stationId"].ToString());
            //    DataTable dtSend = new DataTable();
            //    dtSend.Columns.Add("measurementId");
            //    dtSend.Columns.Add("measurementDate");
            //    dtSend.Columns.Add("Nilai");
            //    elvTanah = Convert.ToDouble(dt.Rows[0]["elevasiTanah"].ToString());
            //    frequency = Convert.ToInt16(dt.Rows[0]["frequency"].ToString());
            //    for (int j = 0; j < dt.Rows.Count; j++)
            //    {
            //        double nilai = 0;
            //        DataRow dr = dt.Rows[j];
            //        if ((dr["dynamicMeasurement"].ToString() != "") && (dr["status"].ToString() == "1" || dr["status"].ToString() == ""))
            //        {
            //            JArray textArray = JArray.Parse(dr["dynamicMeasurement"].ToString());
            //            for (int k = 0; k < textArray.Count; k++)
            //            {
            //                if (Convert.ToInt32(textArray[k]["questionId"].ToString()) == 1)
            //                {
            //                    nilai = Convert.ToDouble(textArray[k]["answer"].ToString());
            //                }
            //            }

            //            dtSend.Rows.Add(dr["measurementId"], dr["measurementDate"], nilai);
            //        }
            //    }
            //    JObject jobj = new JObject();
            //    jobj["stationId"] = JArray[i]["stationId"].ToString();
            //    jobj["stationName"] = JArray[i]["stationName"].ToString();
            //    jobj["sortField"] = JArray[i]["sortField"].ToString();
            //    jobj["elevasiTanah"] = elvTanah;
            //    jobj["frequency"] = frequency;
            //    jobj["color"] = JArray[i]["color"].ToString();
            //    jobj["measure"] = JsonConvert.SerializeObject(dtSend, Formatting.None);
            //    JArraySend.Add(jobj);

            //}

            return JArraySend.ToString();
        }

        [WebMethod]
        public string GetRataRataCurahHujanByWMArea(string idWMArea)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = new DataTable();
            if (idWMArea == "4")
            {
                dt = mapper.GetRataRataCurahHujanByWMArea(idWMArea);
            }
            else
            {
                dt = mapper.GetRataRataCurahHujanByWMAreaWithMaapingRecord(idWMArea);
            }

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string InsertGraph(string obj)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JObject json = JObject.Parse(obj.ToString());
            DataTable dtInsert = mapper.InsertGraph(json);
            int idGraph = Convert.ToInt32(dtInsert.Rows[0].ItemArray[0].ToString());

            JArray JArray = JArray.Parse(json["detailGraph"].ToString()).ToObject<JArray>();
            foreach (JObject jobj in JArray.Parse(json["detailGraph"].ToString()).ToObject<JArray>())
            {
                mapper.InsertDetailGraph(jobj, idGraph);
            }

            JArray JArray2 = JArray.Parse(json["tableNote"].ToString()).ToObject<JArray>();
            foreach (JObject jobj in JArray.Parse(json["tableNote"].ToString()).ToObject<JArray>())
            {
                mapper.InsertNoteGraph(jobj, idGraph);
            }
            JArray jArray3 = JArray.Parse(json["tempColor"].ToString()).ToObject<JArray>();
            foreach (JObject jobj in JArray.Parse(json["tempColor"].ToString()).ToObject<JArray>())
            {
                mapper.InsertKlasifikasiSetting(jobj, idGraph);
            }

            JObject objReturn = new JObject();
            objReturn["idwmarea"] = json["idWMArea"];
            objReturn["idGraph"] = idGraph;

            return objReturn.ToString();
        }

        [WebMethod]
        public string UpdateGraph(string obj)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JObject json = JObject.Parse(obj.ToString());
            mapper.UpdateGraph(json);

            JArray JArray = JArray.Parse(json["detailGraph"].ToString()).ToObject<JArray>();
            foreach (JObject jobj in JArray.Parse(json["detailGraph"].ToString()).ToObject<JArray>())
            {
                mapper.InsertDetailGraph(jobj, Convert.ToInt32(json["idGraph"].ToString()));
            }

            JArray JArray2 = JArray.Parse(json["tableNote"].ToString()).ToObject<JArray>();
            foreach (JObject jobj in JArray.Parse(json["tableNote"].ToString()).ToObject<JArray>())
            {
                mapper.InsertNoteGraph(jobj, Convert.ToInt32(json["idGraph"].ToString()));
            }
            JArray jArray3 = JArray.Parse(json["tempColor"].ToString()).ToObject<JArray>();
            foreach (JObject jobj in JArray.Parse(json["tempColor"].ToString()).ToObject<JArray>())
            {

                mapper.InsertKlasifikasiSetting(jobj, Convert.ToInt32(json["idGraph"].ToString()));
            }
            JObject objReturn = new JObject();
            objReturn["idwmarea"] = json["idWMArea"];
            objReturn["idGraph"] = Convert.ToInt32(json["idGraph"].ToString());

            return objReturn.ToString();
        }

        [WebMethod]
        public string DeleteGraph(string obj)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JObject json = JObject.Parse(obj.ToString());
            mapper.DeleteGraph(json);

            JObject objReturn = new JObject();
            objReturn["idwmarea"] = json["idWMArea"];
            objReturn["idGraph"] = json["idGraph"];

            return objReturn.ToString();
        }

        [WebMethod]
        public string DataMingguanByWmarea(string obj)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JObject json = JObject.Parse(obj.ToString());
            DataTable dt = mapper.DataMingguanByWmarea(json);

            JArray JArraySend = new JArray();
            JArray JArrayMeasure = new JArray();

            String idX = "";
            DataRow dr = null;

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                if (dr == null)
                {

                }
                else if (idX != dt.Rows[i]["stationId"].ToString())
                {
                    JObject jobj = new JObject();
                    jobj["stationId"] = dr["stationId"].ToString();
                    jobj["stationName"] = dr["stationName"].ToString();
                    jobj["EstNewCode"] = dr["EstNewCode"].ToString();
                    jobj["Measure"] = JArrayMeasure;

                    JArraySend.Add(jobj);
                    JArrayMeasure = new JArray();

                }
                JObject jmeasure = new JObject();
                jmeasure["pathImage"] = dt.Rows[i]["pathImage"].ToString();
                jmeasure["idMeasureSurvey"] = dt.Rows[i]["idMeasureSurvey"].ToString();
                jmeasure["dynamic"] = dt.Rows[i]["dynamicMeasurement"].ToString();
                jmeasure["id"] = dt.Rows[i]["measurementId"].ToString();
                jmeasure["date"] = dt.Rows[i]["measurementDateFormate"].ToString();
                JArrayMeasure.Add(jmeasure);

                dr = dt.Rows[i];
                idX = dt.Rows[i]["stationId"].ToString();
            }

            if (JArrayMeasure.Count > 0)
            {
                JObject jobj = new JObject();
                jobj["stationId"] = dr["stationId"].ToString();
                jobj["stationName"] = dr["stationName"].ToString();
                jobj["EstNewCode"] = dr["EstNewCode"].ToString();
                jobj["Measure"] = JArrayMeasure;

                JArraySend.Add(jobj);
                JArrayMeasure = new JArray();
            }

            return JArraySend.ToString();
        }

        [WebMethod]
        public string DataMingguanByEstate(string obj)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JObject json = JObject.Parse(obj.ToString());
            DataTable dt = mapper.DataMingguanByEstate(json);

            JArray JArraySend = new JArray();
            JArray JArrayMeasure = new JArray();

            String idX = "";
            DataRow dr = null;

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                if (dr == null)
                {

                }
                else if (idX != dt.Rows[i]["stationId"].ToString())
                {
                    JObject jobj = new JObject();
                    jobj["stationId"] = dr["stationId"].ToString();
                    jobj["stationName"] = dr["stationName"].ToString();
                    jobj["EstNewCode"] = dr["EstNewCode"].ToString();
                    jobj["Measure"] = JArrayMeasure;

                    JArraySend.Add(jobj);
                    JArrayMeasure = new JArray();

                }
                JObject jmeasure = new JObject();
                jmeasure["pathImage"] = dt.Rows[i]["pathImage"].ToString();
                jmeasure["idMeasureSurvey"] = dt.Rows[i]["idMeasureSurvey"].ToString();
                jmeasure["dynamic"] = dt.Rows[i]["dynamicMeasurement"].ToString();
                jmeasure["id"] = dt.Rows[i]["measurementId"].ToString();
                jmeasure["date"] = dt.Rows[i]["measurementDateFormate"].ToString();
                JArrayMeasure.Add(jmeasure);

                dr = dt.Rows[i];
                idX = dt.Rows[i]["stationId"].ToString();
            }

            if (JArrayMeasure.Count > 0)
            {
                JObject jobj = new JObject();
                jobj["stationId"] = dr["stationId"].ToString();
                jobj["stationName"] = dr["stationName"].ToString();
                jobj["EstNewCode"] = dr["EstNewCode"].ToString();
                jobj["Measure"] = JArrayMeasure;

                JArraySend.Add(jobj);
                JArrayMeasure = new JArray();
            }

            return JArraySend.ToString();
        }

        [WebMethod]
        public string getAllRambuCurahHujanByWmArea(string idWMArea)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            //DataTable dt = mapper.GetAllRambuCurahHujanByWmArea(idWMArea);
            DataTable dt = mapper.GetAllRambuCurahHujanByWmAreaNew2(idWMArea);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string getValueCurahHujan(string stationSelected)
        {
            //JArray JArray = JArray.Parse(stationSelected.ToString());
            WLR_Mapper mapper = new WLR_Mapper();
            String[] rambuCh = stationSelected.Split(',');
            DataTable dtOK = new DataTable();

            dtOK.Columns.Add("sumRainInches", typeof(Decimal));
            dtOK.Columns.Add("date", typeof(DateTime));

            foreach (String cH in rambuCh)
            {
                String[] sCH = cH.Split(':');
                DataTable dt = mapper.getValueCurahHujan(sCH[0].Trim(), sCH[1].Trim());
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    //(DateTime.TryParseExact(dt.Rows[i]["sumRainInches"].ToString(), "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out Temp))
                    //DateTime Temp;
                    dtOK.Rows.Add(
                        Convert.ToDecimal(dt.Rows[i]["sumRainInches"].ToString()),
                        //Convert.ToDateTime(dt.Rows[i]["transdate"].ToString())
                        (DateTime.ParseExact(dt.Rows[i]["transdate"].ToString(), "MM/dd/yyyy", null))
                        );
                }
            }

            if (dtOK.Rows.Count > 0)
            {
                var result = dtOK.AsEnumerable()
              .GroupBy(r => r.Field<DateTime>("date"))
              .Select(g =>
              {
                  var row = dtOK.NewRow();

                  row["date"] = g.Key;
                  row["sumRainInches"] = g.Sum(r => r.Field<Decimal>("sumRainInches") / rambuCh.Length);

                  return row;
              }).CopyToDataTable();

                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                Dictionary<string, object> childRow;
                foreach (DataRow row in result.Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in result.Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
                return jsSerializer.Serialize(parentRow);
            }
            else
            {
                return "[]";
            }

        }

        [WebMethod]
        public string GetLocationMeasure(string measureId)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.getLocationMeasure(measureId);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetDetailNote(string idGrap)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.getDetailNote(idGrap);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        [WebMethod]
        public string GetKlasifikasiSetting(string idGrap)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.getDetailKlasifikasi(idGrap);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string GetFotoUdaraByEstate(string estCode)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.getFotoUdaraByEstate(estCode);

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        [WebMethod]
        public string GetDataByDate(string date, string recordStationId)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataSet hasil = mapper.getDataByDate(date, recordStationId);
            int a = hasil.Tables[0].Rows.Count;
            if (a > 0)
            {
                return "1";
            }
            else
            {
                return "0";
            }

        }
        [WebMethod]
        public string GetTmasTargetByIdGraph(string idGraph)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.getTmasTarget(idGraph);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);

            //if (dt.Rows.Count != 0)
            //{
            //    return dt.Rows[0][0].ToString();
            //}
            //else
            //{
            //    return null;
            //}

        }
        [WebMethod]
        public string GetZoneByWmArea(int idWmArea)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.GetWmaCodeByWmArea(idWmArea);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        [WebMethod]
        public string GetPzoWeekTemp()
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.GetPzoWeekTemp();
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        [WebMethod]
        public string insertNewGraphParamater(int idwmArea, string graphSelected)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            int berhasil = mapper.insertGraphSelectedReport(idwmArea, graphSelected);
            if (berhasil == 1)
            {
                if (berhasil == 1)
                {
                    return "Graph Berhasil disimpan sebagai parameter report";
                }
            }
            return "Gagal";

        }
        [WebMethod]
        public string GetSelectedGraphForReport(int idWmarea)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.GetSelectedGraphForReport(idWmarea);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        [WebMethod]
        public string GetOldNewRambuRef(string rambuRef, string WmaCode)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.GetOldNewRambuRef(rambuRef, WmaCode);
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        [WebMethod]
        public string executeUpdateRambuRef(string rambuLamaKey, int urutan, string ZonaBaru)
        {
            ArrayList rambuRefBaruList = new ArrayList();
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt1 = mapper.GetWmaCodeFromKeyRambu(rambuLamaKey, ZonaBaru);
            string wmaCodeLama = dt1.Rows[0]["wma_codeLama"].ToString();
            string rambuRefLama = dt1.Rows[0]["rambuRefLama"].ToString();
            string rambuRefBaru = dt1.Rows[0]["rambuRefBaru"].ToString();
            string splitLeftparenthesis = rambuRefLama.Split('(')[0];
            string[] ArrRamburefLama = splitLeftparenthesis.Split(',');
            string splitLeftparenthesisRambuRefBaru = rambuRefBaru.Split('(')[0];
            string[] ArrRamburefBaru = splitLeftparenthesisRambuRefBaru.Split(',');
            List<String> result2 = new List<string>();
            string fixArrRambuRefLamaList = "";
            string fixArrRambuRefBaruList = "";
            List<String> ArrRamburefBaruList = new List<string>();
            bool sama = false;
            for (int i = 0; i < ArrRamburefLama.Count(); i++)
            {
                if (ArrRamburefLama[i] == rambuLamaKey)
                {
                    var arrRambuRefLamaList = ArrRamburefLama.ToList();
                    arrRambuRefLamaList.RemoveAt(i);
                    fixArrRambuRefLamaList = ConvertStringArrayToStringJoin(arrRambuRefLamaList.ToArray()); /*arrRambuRefLamaList.ToString() + '(' + arrRambuRefLamaList.Count() + ')';*/
                    fixArrRambuRefLamaList = fixArrRambuRefLamaList + '(' + arrRambuRefLamaList.Count + ')';
                    //arrRambuRefLamaList.ToArray();
                }
            }
            int indexAja = 0;
            for (int i = 0; i < ArrRamburefBaru.Count(); i++)
            {
                if (ArrRamburefBaru[i] != rambuLamaKey)
                {
                    result2.Insert(indexAja, ArrRamburefBaru[i]);
                    indexAja++;
                }
            }
            for (int i = 0; i <= result2.Count(); i++)
            {
                if (i == urutan)
                {
                    //ArrRamburefBaruList = ArrRamburefBaru.ToList();
                    result2.Insert(urutan, rambuLamaKey);
                    fixArrRambuRefBaruList = ConvertStringArrayToStringJoin(result2.ToArray());
                    fixArrRambuRefBaruList = fixArrRambuRefBaruList + '(' + result2.Count() + ')';
                    break;
                }

            }
            mapper.executeUpdateRambuRef(rambuLamaKey, ZonaBaru, fixArrRambuRefLamaList, fixArrRambuRefBaruList);
            //string[] ArrRamburefBaru = rambuRefBaru.Split(',');
            return null;

        }

        [WebMethod]
        public string getChartFromZona(string zonaId)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.getChartFromZona(zonaId).Tables[0];

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }


        [WebMethod]
        public string getSettingZona(string zonaAnalysisId)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.getSettingZona(zonaAnalysisId).Tables[0];

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }



        [WebMethod]
        public string getWeekID(string weekId, string weekIdBefore)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.getWeekID(weekId, weekIdBefore).Tables[0];

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }



        static string ConvertStringArrayToString(string[] array)
        {
            // Concatenate all the elements into a StringBuilder.
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append(',');
            }
            return builder.ToString();
        }
        static string ConvertStringArrayToStringJoin(string[] array)
        {
            // Use string Join to concatenate the string elements.
            string result = string.Join(",", array);
            return result;
        }



        //[WebMethod]
        //public void insertLogData(string WMACode, string DownloadFolder, string DownloadFileName)
        //{
        //    ArrayList rambuRefBaruList = new ArrayList();
        //    WLR_Mapper mapper = new WLR_Mapper();
        //    DataTable dt1 = mapper.insertLogData(WMACode, DownloadFolder, DownloadFileName);
        //    return dt1;

        //}


        [WebMethod]
        public string insertLogData(string WMACode, string DownloadFolder, string DownloadFileName)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataSet ds = mapper.insertLogData(WMACode, DownloadFolder, DownloadFileName);

            return null;
        }


        [WebMethod]
        public string cekDownloadChartImageZona(string WMACode, string DownloadFolder, string DownloadFileName)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataSet ds = mapper.cekDownloadChartImageZona(WMACode, DownloadFolder, DownloadFileName);
            int a = ds.Tables[0].Rows.Count;
            if (a > 0)
            {
                return "0";
            }
            else
            {
                return "1";
            }

            //Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject();
            //jObject["tbl1"] = dataTableToJson(ds.Tables[0]);
            //return jObject.ToString();
        }


        [WebMethod]
        public string ceksurveyor(string idWMArea, string estCode)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataSet ds = mapper.getSurveyor(idWMArea, estCode);
            return dataTableToJson(ds.Tables[0]);

            //Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject();
            //jObject["tbl1"] = dataTableToJson(ds.Tables[0]);
            //return jObject.ToString();
        }

        [WebMethod]
        public string ceksurveyorss(string idWMArea)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataSet ds = mapper.getSurveyorss(idWMArea);
            return dataTableToJson(ds.Tables[0]);

            //Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject();
            //jObject["tbl1"] = dataTableToJson(ds.Tables[0]);
            //return jObject.ToString();
        }
        [WebMethod]
        public string ceksurveyorssedit(string idSurveyor)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataSet ds = mapper.getSurveyorssedit(idSurveyor);
            return dataTableToJson(ds.Tables[0]);

            //Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject();
            //jObject["tbl1"] = dataTableToJson(ds.Tables[0]);
            //return jObject.ToString();
        }

        [WebMethod]
        public string ceksurveyorsseditshowdoc(string idSurveyor)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataSet ds = mapper.getSurveyorssedit(idSurveyor);
            return dataTableToJson(ds.Tables[0]);

            //Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject();
            //jObject["tbl1"] = dataTableToJson(ds.Tables[0]);
            //return jObject.ToString();
        }




        [WebMethod]
        public string insertSuerveyor(string obj)
        {
            try
            {
                WLR_Mapper mapper = new WLR_Mapper();
                JObject json = JObject.Parse(obj.ToString());

                DataTable ddt = mapper.ceknik(json["nik"].ToString());


                if (ddt.Rows.Count > 0)
                {
                    return "Data Sudah Ada";
                }
                else
                {
                    DataTable dtInsert = mapper.insertSuerveyor(json);
                    return dataTableToJson(dtInsert);
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }


        [WebMethod]
        public string insertSuerveyorHistory(string obj)
        {
            try
            {
                WLR_Mapper mapper = new WLR_Mapper();
                JObject json = JObject.Parse(obj.ToString());

                DataTable ddt = mapper.ceknik(json["nik"].ToString());


                if (ddt.Rows.Count > 0)
                {
                    return "null";
                }
                else
                {
                    DataTable dtInsert = mapper.insertSuerveyorHistory(json);
                    return dataTableToJson(dtInsert);
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }



        [WebMethod]
        public string EditSurveyor(string obj)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JObject json = JObject.Parse(obj.ToString());
            mapper.editsurvey(json["idSurveyor"].ToString(), json["statusActive"].ToString(), json["fileUploadedit"].ToString());
            return "null";

            //  mapper.editsurveyHistory(json["idSurveyor"].ToString(), json["statusActive"].ToString());

            //  return null;


        }

        [WebMethod]
        public string getListPetugas(int idWmArea)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataSet data = mapper.listPetugas(idWmArea.ToString());
            string jsonDataStr = JsonConvert.SerializeObject(data, Formatting.None);
            JObject jsonData = JObject.Parse(jsonDataStr).ToObject<JObject>();
            return jsonData["Table"].ToString();
        }

        [WebMethod]
        public string getListPetugasActive(int idWmArea)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataSet data = mapper.listPetugasActive(idWmArea.ToString());
            string jsonDataStr = JsonConvert.SerializeObject(data, Formatting.None);
            JObject jsonData = JObject.Parse(jsonDataStr).ToObject<JObject>();
            return jsonData["Table"].ToString();
        }

        [WebMethod]
        public string showPICWLRWMarea(int idWmArea)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataSet data = mapper.showPICWLRWMarea(idWmArea.ToString());
            string jsonDataStr = JsonConvert.SerializeObject(data, Formatting.None);
            JObject jsonData = JObject.Parse(jsonDataStr).ToObject<JObject>();
            return jsonData["Table"].ToString();
        }

        [WebMethod]
        public string[] GetStatusServer()
        {
            string[] hasil = new string[4];
            try
            {
                WLR_Mapper mapper = new WLR_Mapper();
                DataSet ds = mapper.GetStatusServer();
                hasil[0] = ds.Tables[0].Rows[0][0].ToString();
                hasil[1] = ds.Tables[1].Rows[0][0].ToString();

                hasil[2] = "";//cpuCounter.NextValue().ToString();
                hasil[3] = "";//ramCounter.NextValue().ToString();				
            }
            catch (Exception ex)
            {
                hasil[0] = "Error " + ex.Message;
            }
            return hasil;
        }

        //reset password

        [WebMethod]
        public string GetEmailDomain(string email)
        {
            COR_GroupMapper mapper = new COR_GroupMapper();
            DataTable dt = mapper.checkDomainUsers(email).Tables[0];

            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);

                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        [WebMethod]
        public string ForgotPassword(string userEmail)
        {
            //try
            //{
            //    int passwordLength = 8;
            //    int alphaNumericalCharsAllowed = 2;
            //    String site = "";

            //    site = WebConfigurationManager.AppSettings["Site"];

            //    COR_UserMapper mapper = new COR_UserMapper();
            //    DataTable dt = userObjMapper.GetUserDetailByEmail(userEmail).Tables[0];


            //    string finalCode = Membership.GeneratePassword(passwordLength, alphaNumericalCharsAllowed);
            //    string verificationCode = url.enkrips("userregistrationverificationcode_" + finalCode);
            //    string subject = " Lupa Kata Sandi ?";
            //    string username = dt.Rows[0]["UserFullName"].ToString();
            //    string detail = "Kami ingin Menginformasikan Anda Telah Melakukan Lupa Kata Sandi pada Sistem, Berikut Kami Sampaikan Link Untuk Merubah Kata Sandi Anda " +
            //                    site + "/forgotpassword.aspx?token=" + verificationCode;

            //    string closing = "Abaikan Pesan ini Jika Anda Tidak Ingin Merubah Password Anda";

            //    string EmailHeader = "Konfirmasi Lupa Kata Sandi";
            //    string opening = "Dengan ini Kami Memberi Tahu anda Ada 1 Notifikasi di Aplikasi EntGIS,<br /><br /> Berikut Detailnya";
            //    string code = "EDEFN001";
            //    string body = email.PopulateBodyForUserRegistrationVerification(EmailHeader, username, detail, opening, closing, code);

            //    email.SendHtmlFormattedEmail(null, userEmail, subject, body);
            //    return "";
            //    // mapper.DeleteTokenForResetPassword(verificationCode);
            //    //mapper.InsertTokenForResetPassword(userEmail, verificationCode);

            //}
            //catch (Exception e)
            //{
            //    return e.Message;

            //}
            return "";
        }

        [WebMethod]
        public string GetLanguage()
        {
            string hasil = "";
            try
            {
                WLR_Mapper mapper = new WLR_Mapper();
                DataTable dt = mapper.GetLanguage().Tables[0];
                hasil = dataTableToJson(dt);
            }
            catch (Exception ex)
            {
                hasil = "error " + ex.Message;
            }
            return hasil;
        }

        [WebMethod]
        public string InsertReportSetting(string obj)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JObject json = JObject.Parse(obj.ToString());
            JArray jArray = JArray.Parse(json["zonaSelected"].ToString());
            String zSelec = "";
            for (int i = 0; i < jArray.Count(); i++)
            {
                if (i == 0)
                {
                    zSelec = jArray[i].ToString();
                }
                else
                {
                    zSelec += "," + jArray[i].ToString();
                }
            }
            DataTable dtInsert = mapper.InsertReportSetting(json, zSelec);
            return dataTableToJson(dtInsert);
        }

        [WebMethod]
        public string GetAllWmSettingReport(string idWMArea)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.getAllWmSettingReport(idWMArea);
            return dataTableToJson(dt);
        }

        [WebMethod]
        public string GetReportDetail(string idReport, string dateParam, string dateParam2)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataSet ds = mapper.getReportDetail(idReport, dateParam, dateParam2);
            JObject jObject = new JObject();
            jObject["ReportDetail"] = dataTableToJson(ds.Tables[0]);
            jObject["TabelReport"] = dataTableToJson(ds.Tables[1]);
            jObject["TabelReportDetail"] = dataTableToJson(ds.Tables[2]);
            return jObject.ToString();
        }

        [WebMethod]
        public string DeleteToReportSetting(string idReport, string idWMArea, string createBy)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.DeleteToReportSetting(idReport, idWMArea, createBy);
            return dataTableToJson(dt);
        }

        [WebMethod]
        public string SaveReportPemantauan(string obj)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            JObject json = JObject.Parse(obj.ToString());

            String recordPemantauanId = json["recordPemantauanId"].ToString();

            DataTable dt = mapper.SaveReportPemantauanHeader(json["uID"].ToString(), recordPemantauanId, json["topik"].ToString(), json["judul"].ToString(), json["report"].ToString(), json["pt"].ToString());

            recordPemantauanId = dt.Rows[0]["recordPemantauanId"].ToString();

            JArray jArray = JArray.Parse(json["dataTopik"].ToString());
            for (int i = 0; i < jArray.Count; i++)
            {
                JObject objX = JObject.Parse(jArray[i].ToString());
                mapper.SaveReportPemantauanDetail(recordPemantauanId, objX["detailTopik"].ToString(), objX["luasDas"].ToString(), objX["stationId"].ToString());
            }

            return dataTableToJson(dt);
        }

        [WebMethod]
        public string ShowReportPemantauanHeader(string pt)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.ShowReportPemantauanHeader(pt);
            return dataTableToJson(dt);
        }

        [WebMethod]
        public string ShowReportPemantauanById(string recordPemantauanId)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.ShowReportPemantauanById(recordPemantauanId);
            return dataTableToJson(dt);
        }

        [WebMethod]
        public string ShowReportPemantauanByIdDataRambu(string recordPemantauanId, string dateParam, string dateParam2)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.ShowReportPemantauanByIdDataRambu(recordPemantauanId, dateParam, dateParam2);
            return dataTableToJson(dt);
        }

        [WebMethod]
        public string DeletedReportPemantauan(string recordPemantauanId, string uID)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.DeletedReportPemantauan(recordPemantauanId, uID);
            return dataTableToJson(dt);
        }

        [WebMethod]
        public string GetMeasureRambuPerWmAreaForMap(String idwmarea, String dateParam, String dateParam2, String dateParam3)
        {
            WLR_Mapper mapper = new WLR_Mapper();
            DataTable dt = mapper.GetMeasureRambuPerWmAreaForMap(idwmarea, dateParam, dateParam2, dateParam3).Tables[0];
            return dataTableToJson(dt);
        }

        //tambahan TMAT

        //Mengambil Hari Libur
        [WebMethod]
        public string GetHoliday()
        {

            IOTMapper mapper = new IOTMapper();
            DataSet ds = mapper.GetHoliday();
            return dataTableToJson(ds.Tables[0]);
        }


        //Mengambil data TMAT bulanan
        [WebMethod]
        public string GetMonthlyTMAT(string companycode, string estcode, string deviceid, string startingdate, string endingdate)
        {
            try
            {
                IOTMapper mapper = new IOTMapper();
                DataSet ds = mapper.GetMonthlyTMAT(companycode, estcode, deviceid, startingdate, endingdate);
                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }



        }
        

        //Get Water Depth Indicator
        [WebMethod]
        public string IOT_GetWaterDepthIndicator()
        {

            IOTMapper mapper = new IOTMapper();

            DataSet ds = mapper.GetWaterDepthIndicator();
            return dataTableToJson(ds.Tables[0]);

        }

        //Update dicki 
        [WebMethod]
        public string GetZona(string idWmArea)
        {
            try
            {
                if(idWmArea == "")
                {
                    return "Wm area not found";
                }

                int _idWmArea = int.Parse(idWmArea);
                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet ds = mapper.getZona(_idWmArea);

                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public string GetEstcode(string zona,string idWmArea)
        {
            try
            {
                if (zona == "")
                {
                    return "zona not found";
                }

                if (idWmArea == "")
                {
                    return "zona not found";
                }

                int _idWmArea = int.Parse(idWmArea);
                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet ds = mapper.getEstcode(zona, _idWmArea);

                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public string GetPiezomenterGraph(string estCode, string zona)
        {
            try
            {
                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet ds = mapper.GetPiezomenterGraph(estCode, zona);

                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public string GetCurahHujanGraph(string idWMArea)
        {
         
            try
            {
                if (idWMArea == "")
                {
                    return "id idWMArea not found";
                }

                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet ds = mapper.GetCurahHujanGraph(idWMArea);

                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public string CreateGraphTmat(string graphName, string wmArea, string zonaId, string createBy, string updateBy,string pieRecordID, string curahHujan)
        {

            try
            {
                int _wmArea = 0;
                if(wmArea == "")
                {
                    return "Wm area not found";
                }
                _wmArea = int.Parse(wmArea);

                int _createBy = 0;
                if (createBy == "")
                {
                    return "User id not found";
                }
                _createBy = int.Parse(createBy);

                int _updateBy = 0;
                if (updateBy == "")
                {
                    return "User id not found";
                }
                _updateBy = int.Parse(updateBy);

                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet ds = mapper.CreateGraphTMAT(graphName, _wmArea, zonaId, _createBy, _updateBy);

                DataTable graph = ds.Tables[0];
                bool success = false;
                if(graph.Rows.Count > 0)
                {
                    int idGraph = int.Parse(graph.Rows[0]["idGraph"].ToString());

                    JArray _curahHujan = JArray.Parse(curahHujan);

                    for (int k = 0; k < _curahHujan.Count(); k++)
                    {
                        mapper.CreateGraphTMATDetail(idGraph, _curahHujan[k].ToString());
                    }

                    JArray _pieRecordID = JArray.Parse(pieRecordID);

                    for (int k = 0; k < _pieRecordID.Count(); k++)
                    {
                        mapper.CreateGraphTMATDetailPizo(idGraph, _pieRecordID[k]["PieRecordID"].ToString(), _pieRecordID[k]["colorField"].ToString(), int.Parse(_pieRecordID[k]["lineThickness"].ToString()));
                    }
                    
                    success = true;
                }

                JObject message = new JObject();
                message["success"] = success;

                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public string GetAllGraphTMAT(string idWMArea)
        {

            try
            {
                //idWMArea atau wm area 
                if(idWMArea == "")
                {
                    return "WM area not found";
                }

                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet ds = mapper.ShowGraphTMAT(idWMArea);

                return dataTableToJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public string GetValueGraphTMAT(string multiplepieRecordID, string startDate, string endDate)
        {

            try
            {

                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                JArray _multiplepieRecordID = JArray.Parse(multiplepieRecordID);
                JArray arrayGraphValue = new JArray();
                for (int k = 0; k < _multiplepieRecordID.Count(); k++)
                {
                    DataSet ds = mapper.showValueGraphTMAT(_multiplepieRecordID[k].ToString(), startDate, endDate);
                    DataTable GraphValue = ds.Tables[0];

                    JArray value = new JArray();
                    for (int p = 0; p < GraphValue.Rows.Count; p++)
                    {
                        //if (GraphValue.Rows[p]["Ketinggian"].ToString() == "" || GraphValue.Rows[p]["Ketinggian"] == null)
                        //{
                        //    continue;
                        //}

                        JObject objectValue = new JObject();
                        objectValue["ID"] = GraphValue.Rows[p]["ID"].ToString();
                        objectValue["Month"] = GraphValue.Rows[p]["Month"].ToString();
                        objectValue["Year"] = GraphValue.Rows[p]["Year"].ToString();
                        objectValue["Week"] = GraphValue.Rows[p]["Week"].ToString();
                        objectValue["PieRecordID"] = GraphValue.Rows[p]["PieRecordID"].ToString();
                        objectValue["Date"] = GraphValue.Rows[p]["Date"].ToString();
                        objectValue["tglPengukuran"] = GraphValue.Rows[p]["tglPengukuran"].ToString();
                        objectValue["Ketinggian"] = GraphValue.Rows[p]["Ketinggian"].ToString();
                        objectValue["colorBack"] = GraphValue.Rows[p]["colorBack"].ToString();
                        objectValue["IndicatorAliasReport"] = GraphValue.Rows[p]["IndicatorAliasReport"].ToString();
                        //objectValue["colorField"] = GraphValue.Rows[p]["colorField"].ToString();
                        objectValue["sortField"] = 0;
                        objectValue["Division"] = GraphValue.Rows[p]["Division"].ToString();
                        objectValue["estCode"] = GraphValue.Rows[p]["estCode"].ToString();
                        objectValue["Block"] = GraphValue.Rows[p]["Block"].ToString();

                        value.Add(objectValue);
                    }

                    

                    arrayGraphValue.Add(value);
                }

                return arrayGraphValue.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public string GetValueGraphTMATPerDay(string multiplepieRecordID, string startDate, string endDate)
        {

            try
            {

                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                JArray _multiplepieRecordID = JArray.Parse(multiplepieRecordID);
                JArray arrayGraphValue = new JArray();
                for (int k = 0; k < _multiplepieRecordID.Count(); k++)
                {
                    DataSet ds = mapper.showValueGraphTMATPerDay(_multiplepieRecordID[k].ToString(), startDate, endDate);
                    DataTable GraphValue = ds.Tables[0];

                    if(GraphValue.Rows.Count > 0)
                    {
                        JArray value = new JArray();
                        for (int p = 0; p < GraphValue.Rows.Count; p++)
                        {
                            //if (GraphValue.Rows[p]["Ketinggian"].ToString() == "" || GraphValue.Rows[p]["Ketinggian"] == null)
                            //{
                            //    continue;
                            //}

                            JObject objectValue = new JObject();
                            objectValue["PieRecordIDGis"] = GraphValue.Rows[p]["PieRecordIDGis"].ToString();
                            objectValue["PieRecordID"] = GraphValue.Rows[p]["PieRecordID"].ToString();
                            objectValue["Date"] = GraphValue.Rows[p]["Date"].ToString();
                            objectValue["tglPengukuran"] = GraphValue.Rows[p]["tglPengukuran"].ToString();
                            objectValue["Ketinggian"] = GraphValue.Rows[p]["Ketinggian"].ToString();
                            objectValue["colorBack"] = GraphValue.Rows[p]["colorBack"].ToString();
                            objectValue["IndicatorAliasReport"] = GraphValue.Rows[p]["IndicatorAliasReport"].ToString();
                            //objectValue["colorField"] = GraphValue.Rows[p]["colorField"].ToString();
                            objectValue["sortField"] = 0;
                            objectValue["Division"] = GraphValue.Rows[p]["Division"].ToString();
                            objectValue["estCode"] = GraphValue.Rows[p]["estCode"].ToString();
                            objectValue["Block"] = GraphValue.Rows[p]["Block"].ToString();

                            value.Add(objectValue);
                        }
                        arrayGraphValue.Add(value);
                    }

                   
                }

                return arrayGraphValue.ToString();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }

        [WebMethod]
        public string getValueCurahHujanGraphTmat(string stationSelected, string pieRecordId, string startDate, string endDate)
        {
            //JArray JArray = JArray.Parse(stationSelected.ToString());
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            String[] rambuCh = stationSelected.Split(',');
            DataTable dtOK = new DataTable();

            dtOK.Columns.Add("ID", typeof(string));
            dtOK.Columns.Add("Month", typeof(string));
            dtOK.Columns.Add("Year", typeof(string));
            dtOK.Columns.Add("Week", typeof(string));
            dtOK.Columns.Add("sumRainInches", typeof(Decimal));
            dtOK.Columns.Add("date", typeof(string));


            foreach (String cH in rambuCh)
            {
                String[] sCH = cH.Split(':');
                DataTable dt = mapper.getValueCurahHujanGraphTmat(sCH[0].Trim(), sCH[1].Trim(), pieRecordId, startDate, endDate);

                
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    //(DateTime.TryParseExact(dt.Rows[i]["sumRainInches"].ToString(), "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out Temp))
                    //DateTime Temp;
                    decimal sumRainInches = 0;
                    //if (dt.Rows[i]["sumRainInches"].ToString() == null || dt.Rows[i]["sumRainInches"].ToString() == "null" || dt.Rows[i]["sumRainInches"].ToString() == "")
                    //{
                    //    continue;
                    //}
                  
                        
                    sumRainInches = Convert.ToDecimal(dt.Rows[i]["sumRainInches"].ToString());
                  
 
                    dtOK.Rows.Add(
                        dt.Rows[i]["ID"].ToString(),
                        dt.Rows[i]["Month"].ToString(),
                        dt.Rows[i]["Year"].ToString(),
                        dt.Rows[i]["Week"].ToString(),
                        sumRainInches,
                        dt.Rows[i]["transdate"].ToString()
                        );
                }
            }
         
            if (dtOK.Rows.Count > 0)
            {
                var result = dtOK.AsEnumerable()
              .GroupBy(r => r.Field<string>("date"))
              .Select(g =>
              {
                  var row = dtOK.NewRow();

                  row["date"] = g.Key;
                  row["sumRainInches"] = g.Sum(r => r.Field<Decimal>("sumRainInches") / rambuCh.Length);
                  row["ID"] = g.First()["ID"];
                  row["Month"] = g.First()["Month"];
                  row["Year"] = g.First()["Year"];
                  row["Week"] = g.First()["Week"];

                  return row;
              }).CopyToDataTable();

                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                Dictionary<string, object> childRow;
                foreach (DataRow row in result.Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in result.Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
                return jsSerializer.Serialize(parentRow);
            }
            else
            {
                return "[]";
            }

        }

        [WebMethod]
        public string getValueCurahHujanGraphTmatPerDay(string stationSelected, string startDate, string endDate)
        {
            //JArray JArray = JArray.Parse(stationSelected.ToString());
            PZO_ProjectMapper mapper = new PZO_ProjectMapper();
            String[] rambuCh = stationSelected.Split(',');
            DataTable dtOK = new DataTable();

            dtOK.Columns.Add("sumRainInches", typeof(Decimal));
            dtOK.Columns.Add("date", typeof(string));


            foreach (String cH in rambuCh)
            {
                String[] sCH = cH.Split(':');
                DataTable dt = mapper.getValueCurahHujanGraphTmatPerDay(sCH[0].Trim(), sCH[1].Trim(), startDate, endDate);


                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    //(DateTime.TryParseExact(dt.Rows[i]["sumRainInches"].ToString(), "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out Temp))
                    //DateTime Temp;
                    decimal sumRainInches = 0;
                    //if (dt.Rows[i]["sumRainInches"].ToString() == null || dt.Rows[i]["sumRainInches"].ToString() == "null" || dt.Rows[i]["sumRainInches"].ToString() == "")
                    //{
                    //    continue;
                    //}


                    sumRainInches = Convert.ToDecimal(dt.Rows[i]["sumRainInches"].ToString());


                    dtOK.Rows.Add(
                        sumRainInches,
                        dt.Rows[i]["transdate"].ToString()
                        );
                }
            }

            if (dtOK.Rows.Count > 0)
            {
                var result = dtOK.AsEnumerable()
              .GroupBy(r => r.Field<string>("date"))
              .Select(g =>
              {
                  var row = dtOK.NewRow();

                  row["date"] = g.Key;
                  row["sumRainInches"] = g.Sum(r => r.Field<Decimal>("sumRainInches") / rambuCh.Length);

                  return row;
              }).CopyToDataTable();

                JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
                Dictionary<string, object> childRow;
                foreach (DataRow row in result.Rows)
                {
                    childRow = new Dictionary<string, object>();
                    foreach (DataColumn col in result.Columns)
                    {
                        childRow.Add(col.ColumnName, row[col]);
                    }
                    parentRow.Add(childRow);
                }
                return jsSerializer.Serialize(parentRow);
            }
            else
            {
                return "[]";
            }

        }

        [WebMethod]
        public string GetGraphTMATById(string idGraph)
        {

            try
            {
                //idWMArea atau wm area 
                if (idGraph == "")
                {
                    return "id graph not found";
                }

                int _idGraph = int.Parse(idGraph);

                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet ds = mapper.ShowGraphTMATById(_idGraph);

                DataTable Graph = ds.Tables[0];

                JArray ArrayGraph = new JArray();
                for (int p = 0; p < Graph.Rows.Count; p++)
                {
 

                    JObject objectGraph = new JObject(); 
                    objectGraph["idGraph"] = Graph.Rows[p]["idGraph"].ToString();
                    objectGraph["graphName"] = Graph.Rows[p]["graphName"].ToString(); 
                    objectGraph["wmArea"] = Graph.Rows[p]["wmArea"].ToString(); 
                    objectGraph["zonaId"] = Graph.Rows[p]["zonaId"].ToString();
                    objectGraph["createBy"] = Graph.Rows[p]["createBy"].ToString();
                    objectGraph["createDate"] = Graph.Rows[p]["createDate"].ToString();
                    objectGraph["updateBy"] = Graph.Rows[p]["updateBy"].ToString();
                    objectGraph["updateDate"] = Graph.Rows[p]["updateDate"].ToString();
                    objectGraph["status"] = Graph.Rows[p]["status"].ToString();

                    ArrayGraph.Add(objectGraph);
                }

                return ArrayGraph.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public string getDetailPizometer(string idGraph)
        {

            try
            {
                if (idGraph == "")
                {
                    return "id graph not found";
                }

                int _idGraph = int.Parse(idGraph);


                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet dataPizometer = mapper.getPizometerByIdGraph(_idGraph);
                DataTable pizometer = dataPizometer.Tables[0];

                JArray ArrayPizometer = new JArray();
                for (int k = 0; k < pizometer.Rows.Count; k++)
                {
                    JObject ObjectPizometer = new JObject();

                    ObjectPizometer["idGraph"] = pizometer.Rows[k]["idGraph"].ToString();
                    ObjectPizometer["WMA_CODE"] = pizometer.Rows[k]["WMA_CODE"].ToString();
                    ObjectPizometer["NamaZona"] = pizometer.Rows[k]["NamaZona"].ToString();
                    ObjectPizometer["estCode"] = pizometer.Rows[k]["estCode"].ToString();
                    ObjectPizometer["Block"] = pizometer.Rows[k]["Block"].ToString();
                    ObjectPizometer["Division"] = pizometer.Rows[k]["Division"].ToString(); 
                    ObjectPizometer["idGraphDetail"] = pizometer.Rows[k]["idGraphDetail"].ToString();
                    ObjectPizometer["PieRecordID"] = pizometer.Rows[k]["pieRecordID"].ToString();
                    ObjectPizometer["colorField"] = pizometer.Rows[k]["colorField"].ToString();
                    ObjectPizometer["sortField"] = 0;

                    double lineThickness = 3;
                    if (pizometer.Rows[k]["lineThickness"].ToString() != "")
                    {
                        lineThickness = double.Parse(pizometer.Rows[k]["lineThickness"].ToString());
                    }
                    ObjectPizometer["lineThickness"] = lineThickness; 
                    ArrayPizometer.Add(ObjectPizometer);
                }

    

                return ArrayPizometer.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public string getDetailCurahHujan(string idGraph)
        {

            try
            {
                if (idGraph == "")
                {
                    return "id graph not found";
                }

                int _idGraph = int.Parse(idGraph);


                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet dataPizometer = mapper.getCurahHujanByIdGraph(_idGraph);
                DataTable pizometer = dataPizometer.Tables[0];

                JArray ArrayPizometer = new JArray();
                for (int k = 0; k < pizometer.Rows.Count; k++)
                {
                    JObject ObjectPizometer = new JObject();

                    ObjectPizometer["idGraph"] = pizometer.Rows[k]["idGraph"].ToString();
                    ObjectPizometer["idGraphDetail"] = pizometer.Rows[k]["idGraphDetail"].ToString();
                    ObjectPizometer["curahHujan"] = pizometer.Rows[k]["curahHujan"].ToString();
                    ArrayPizometer.Add(ObjectPizometer);
                }



                return ArrayPizometer.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }


        [WebMethod]
        public string getDeepIndicatorColor()
        {

            try
            {

                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet indicatorColor = mapper.getDeepIndicatorColor();
                DataTable color = indicatorColor.Tables[0];


                return dataTableToJson(color);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public string UpdateGraphTmat(string idGraph, string graphName,  string zonaId, string updateBy, string pieRecordID, string curahHujan)
        {

            try
            {
                int _idGraph = 0;
                if (idGraph == "")
                {
                    return "User id not found";
                }
                _idGraph = int.Parse(idGraph);

                int _updateBy = 0;
                if (updateBy == "")
                {
                    return "User id not found";
                }
                _updateBy = int.Parse(updateBy);

                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet ds = mapper.updateGraphTMAT(_idGraph,graphName, zonaId, _updateBy);

                DataTable graph = ds.Tables[0];
                bool success = false;
                if (graph.Rows.Count > 0)
                {

                    JArray _curahHujan = JArray.Parse(curahHujan);

                    for (int k = 0; k < _curahHujan.Count(); k++)
                    {
                        mapper.CreateGraphTMATDetail(_idGraph, _curahHujan[k].ToString());
                    }

                    JArray _pieRecordID = JArray.Parse(pieRecordID);

                    for (int k = 0; k < _pieRecordID.Count(); k++)
                    {
                        mapper.CreateGraphTMATDetailPizo(_idGraph, _pieRecordID[k]["PieRecordID"].ToString(), _pieRecordID[k]["colorField"].ToString(), int.Parse(_pieRecordID[k]["lineThickness"].ToString()));
                    }

                    success = true;
                }

                JObject message = new JObject();
                message["success"] = success;

                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        [WebMethod]
        public string DeleteGraphTmat(string idGraph)
        {

            try
            {
                //comment 
                if(idGraph == "")
                {
                    return "id graph not found";
                }

                int _idGraph = int.Parse(idGraph);

                PZO_ProjectMapper mapper = new PZO_ProjectMapper();

                DataSet graphDataset = mapper.deleteGraphTMAT(_idGraph);
                DataTable graph = graphDataset.Tables[0];


                return dataTableToJson(graph);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

    }


}
