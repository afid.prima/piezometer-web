﻿var global_extentindonesia;

function SetLayerBase() {

    SetBaseCheckbox();
    WebServiceGetLayerBaseMap();
    SetOSM();
    if (localStorage.getItem("pzoolwmvalue").split("*")[1] != "PT.THIP") {
        SetFotoUdara();
        SetVectorBase();
    } else {
        SetFotoUdaraTHIP();
        SetVectorBaseTHIP();
    }

    HandlerMapBase();
}

function SetBaseCheckbox() {
    $('#chkbasemap').prop('checked', false);
    $('#chkbasemap2').prop('checked', false);
    $('#chkbasemap2thip1').prop('checked', false);
    $('#chkbasemap2thip2').prop('checked', false);
    $('#chkbasemap2thip3').prop('checked', false);
    $('#chkbasemap2thip4').prop('checked', false);
    $('#chkboundary').prop('checked', false);
    $('#chkafdeling').prop('checked', false);
    $('#chkblock').prop('checked', false);
    $('#chklanduse').prop('checked', false);
    $('#chkjalan').prop('checked', false);
    $('#chkwater').prop('checked', true);
    $('#chklabelboundary').prop('checked', false);
    $('#chklabelafdeling').prop('checked', false);
    $('#chklabelblock').prop('checked', false);
    $('#chklabel').prop('checked', true);

    $('#chkdeadpalm').prop('checked', false);
    $('#chkoilpalm').prop('checked', false);
    $('#chktph').prop('checked', false);
    $('#chkbasemap22').prop('checked', true);
}

function HandlerMapBase() {
    //Base Layer
    $("#sliderbasemap").slider({ // Slider base
        value: 100,
        slide: function (e, ui) {
            global_baselayermap.setOpacity(ui.value / 100);
        }
    });
    $("#chkbasemap").change(function () { // On Off Layerbase
        if ($(this).is(":checked")) {
            global_baselayermap.setVisible(true);
        }
        else {
            global_baselayermap.setVisible(false);
        }
    });
    $('#selectbasemap').change(function () { //Layer Base Change
        global_baselayermap.setSource(global_sourcelayerbasemap[$(this).val()]);
    });

    //Fotoudara
    if (localStorage.getItem("pzoolwmvalue").split("*")[1] != "PT.THIP") {
        $("#sliderbasemap2").slider({ // Slider base
            value: 100,
            slide: function (e, ui) {
                global_baselayermap2.setOpacity(ui.value / 100);
            }
        });
        $("#chkbasemap2").change(function () { // On Off Layerbase
            if ($(this).is(":checked")) {
                global_baselayermap2.setVisible(true);
            }
            else {
                global_baselayermap2.setVisible(false);
            }
        });
        $('#selectbasemap2').change(function () { //Layer Base Change
            global_baselayermap2.setSource(global_sourcelayerbasemap2[$(this).val()]);
        });

        //Foto udara 2
        $("#sliderbasemap22").slider({ // Slider base
            value: 100,
            slide: function (e, ui) {
                global_baselayermap22.setOpacity(ui.value / 100);
            }
        });
        $("#chkbasemap22").change(function () { // On Off Layerbase
            if ($(this).is(":checked")) {
                global_baselayermap22.setVisible(true);
            }
            else {
                global_baselayermap22.setVisible(false);
            }
        });
        $('#selectbasemap22').change(function () { //Layer Base Change
            global_baselayermap22.setSource(global_sourcelayerbasemap22[$(this).val()]);
        });
    } else {

        $("#sliderbasemap2").slider({ // Slider base
            value: 100,
            slide: function (e, ui) {
                let opac = ui.value / 100;
                globalGroupLayer[$("#selectbasemap2").val()].setVisible(opac);
            }
        });
        $("#chkbasemap2").change(function () { // On Off Layerbase
            for (var i = 0; i < globalGroupLayer.length; i++) {
                if (globalGroupLayer[i].get('title') != $("#selectbasemap22").val()) {
                    globalGroupLayer[i].setVisible(false);
                }
            }

            var chkVisible = $(this).is(":checked");
            globalGroupLayer[$("#selectbasemap2").val()].setVisible(chkVisible);
            console.log("layerTitle" + globalGroupLayer[$("#selectbasemap2").val()].get('title'));
        });
        $('#selectbasemap2').change(function () { //Layer Base Change
            for (var i = 0; i < globalGroupLayer.length; i++) {
                if (globalGroupLayer[i].get('title') != $("#selectbasemap22").val()) {
                    globalGroupLayer[i].setVisible(false);
                }
            }

            var chkVisible = $("#chkbasemap2").is(":checked");
            globalGroupLayer[$("#selectbasemap2").val()].setVisible(chkVisible);
        });

        //Foto udara 2
        $("#sliderbasemap22").slider({ // Slider base
            value: 100,
            slide: function (e, ui) {
                let opac = ui.value / 100;
                globalGroupLayer[$("#selectbasemap22").val()].setVisible(opac);
            }
        });
        $("#chkbasemap22").change(function () { // On Off Layerbase
            for (var i = 0; i < globalGroupLayer.length; i++) {
                if (globalGroupLayer[i].get('title') != $("#selectbasemap2").val()) {
                    globalGroupLayer[i].setVisible(false);
                }
            }

            var chkVisible = $(this).is(":checked");
            globalGroupLayer[$("#selectbasemap22").val()].setVisible(chkVisible);
            console.log("layerTitle" + globalGroupLayer[$("#selectbasemap22").val()].get('title'));
        });
        $('#selectbasemap22').change(function () { //Layer Base Change
            for (var i = 0; i < globalGroupLayer.length; i++) {
                if (globalGroupLayer[i].get('title') != $("#selectbasemap2").val()) {
                    globalGroupLayer[i].setVisible(false);
                }
            }

            var chkVisible = $("#chkbasemap22").is(":checked");
            globalGroupLayer[$("#selectbasemap22").val()].setVisible(chkVisible);
        });
    }

    //Vector
    $("#sliderboundary").slider({ // Slider base
        value: 100,
        slide: function (e, ui) {
            global_layerboundary.setOpacity(ui.value / 100);
        }
    });
    $("#chkboundary").change(function () { // On Off Layerbase
        if ($(this).is(":checked")) {
            global_layerboundary.setVisible(true);
        }
        else {
            global_layerboundary.setVisible(false);
        }
    });

    $("#sliderafdeling").slider({ // Slider base
        value: 100,
        slide: function (e, ui) {
            global_layerafdeling.setOpacity(ui.value / 100);
        }
    });
    $("#chkafdeling").change(function () { // On Off Layerbase
        if ($(this).is(":checked")) {
            global_layerafdeling.setVisible(true);
        }
        else {
            global_layerafdeling.setVisible(false);
        }
    });

    $("#sliderblock").slider({ // Slider base
        value: 100,
        slide: function (e, ui) {
            global_layerblock.setOpacity(ui.value / 100);
        }
    });
    $("#chkblock").change(function () { // On Off Layerbase
        if ($(this).is(":checked")) {
            global_layerblock.setVisible(true);
        }
        else {
            global_layerblock.setVisible(false);
        }
    });

    $("#sliderlanduse").slider({ // Slider base
        value: 100,
        slide: function (e, ui) {
            global_layerlanduse.setOpacity(ui.value / 100);
        }
    });
    $("#chklanduse").change(function () { // On Off Layerbase
        if ($(this).is(":checked")) {
            global_layerlanduse.setVisible(true);
        }
        else {
            global_layerlanduse.setVisible(false);
        }
    });

    $("#sliderjalan").slider({ // Slider base
        value: 100,
        slide: function (e, ui) {
            global_layerjalan.setOpacity(ui.value / 100);
        }
    });
    $("#chkjalan").change(function () { // On Off Layerbase
        if ($(this).is(":checked")) {
            global_layerjalan.setVisible(true);
        }
        else {
            global_layerjalan.setVisible(false);
        }
    });

    if (localStorage.getItem("pzoolwmvalue").split("*")[1] != "PT.THIP") {
        $("#sliderdeadpalm").slider({ // Slider base
            value: 100,
            slide: function (e, ui) {
                global_layerdeadpalm.setOpacity(ui.value / 100);
            }
        });
        $("#chkdeadpalm").change(function () { // On Off Layerbase
            if ($(this).is(":checked")) {
                global_layerdeadpalm.setVisible(true);
            }
            else {
                global_layerdeadpalm.setVisible(false);
            }
        });

        $("#slideroilpalm").slider({ // Slider base
            value: 100,
            slide: function (e, ui) {
                global_layeroilpalm.setOpacity(ui.value / 100);
            }
        });
        $("#chkoilpalm").change(function () { // On Off Layerbase
            if ($(this).is(":checked")) {
                global_layeroilpalm.setVisible(true);
            }
            else {
                global_layeroilpalm.setVisible(false);
            }
        });
    }
    else {
        $("#sliderdeadpalm").slider({ // Slider base
            value: 100,
            slide: function (e, ui) {
                global_layerdeadpalm1.setOpacity(ui.value / 100);
                global_layerdeadpalm2.setOpacity(ui.value / 100);
                global_layerdeadpalm3.setOpacity(ui.value / 100);
                global_layerdeadpalm4.setOpacity(ui.value / 100);
            }
        });
        $("#chkdeadpalm").change(function () { // On Off Layerbase
            if ($(this).is(":checked")) {
                global_layerdeadpalm1.setVisible(true);
                global_layerdeadpalm2.setVisible(true);
                global_layerdeadpalm3.setVisible(true);
                global_layerdeadpalm4.setVisible(true);
            }
            else {
                global_layerdeadpalm1.setVisible(false);
                global_layerdeadpalm2.setVisible(false);
                global_layerdeadpalm3.setVisible(false);
                global_layerdeadpalm4.setVisible(false);
            }
        });

        $("#slideroilpalm").slider({ // Slider base
            value: 100,
            slide: function (e, ui) {
                global_layeroilpalm1.setOpacity(ui.value / 100);
                global_layeroilpalm2.setOpacity(ui.value / 100);
                global_layeroilpalm3.setOpacity(ui.value / 100);
                global_layeroilpalm4.setOpacity(ui.value / 100);
            }
        });
        $("#chkoilpalm").change(function () { // On Off Layerbase
            if ($(this).is(":checked")) {
                global_layeroilpalm1.setVisible(true);
                global_layeroilpalm2.setVisible(true);
                global_layeroilpalm3.setVisible(true);
                global_layeroilpalm4.setVisible(true);
            }
            else {
                global_layeroilpalm1.setVisible(false);
                global_layeroilpalm2.setVisible(false);
                global_layeroilpalm3.setVisible(false);
                global_layeroilpalm4.setVisible(false);
            }
        });
    }

    //$("#slidertph").slider({ // Slider base
    //	value: 100,
    //	slide: function (e, ui) {
    //		global_layertph.setOpacity(ui.value / 100);
    //	}
    //});
    //$("#chktph").change(function () { // On Off Layerbase
    //	if ($(this).is(":checked")) {
    //		global_layertph.setVisible(true);
    //	}
    //	else {
    //		global_layertph.setVisible(false);
    //	}
    //});

    //Label
    $("#chklabelboundary").change(function () { // On Off Layerbase
        if ($(this).is(":checked")) {
            global_layerlabelboundary.setVisible(true);
        }
        else {
            global_layerlabelboundary.setVisible(false);
        }
    });

    $("#chklabelafdeling").change(function () { // On Off Layerbase
        if ($(this).is(":checked")) {
            global_layerlabelafdeling.setVisible(true);
        }
        else {
            global_layerlabelafdeling.setVisible(false);
        }
    });

    $("#chklabelblock").change(function () { // On Off Layerbase
        if ($(this).is(":checked")) {
            global_layerblock.setStyle(StyleFunctionLabelBlockOn);
        }
        else {
            global_layerblock.setStyle(StyleFunctionLabelBlockOff);
        }
    });
}

function SetFotoUdara() {
    let company, namafoto, namafotojson, source;
    global_sourcelayerbasemap2 = [], global_sourcelayerbasemap22 = [];

    WebserviceGetDataWMS(); // Ambil data serice foto udara

    company = localStorage.getItem("pzoolwmvalue").toString().split("*")[1].replace("PT.", "").replace(" ", "").replace("PANPP", "PANP_P").replace("PANPL", "PANP_L").replace("PANPS", "PANP_S").replace("PLDK", "PLD_K").replace("PLDS", "PLD_S").replace("SUMK", "SUM_K").replace("SUMS", "SUM_S");

    //Input Select
    namafoto = "-";
    namafotojson = global_jsonfoto1.filter(d => d.Name == company + "_1");
    if (namafotojson.length > 0) if (namafotojson[0]["Abstract"]["#cdata"].toString().includes("<SPAN>")) namafoto = "FU " + namafotojson[0]["Abstract"]["#cdata"].toString().split("<SPAN>")[1].toString().split("</SPAN>")[0].split("_")[2];
    $("#selectbasemap2").append($('<option>', {
        value: 0,
        text: namafoto,
    }));
    $("#selectbasemap22").append($('<option>', {
        value: 0,
        text: namafoto,
    }));

    namafoto = "-";
    namafotojson = global_jsonfoto2.filter(d => d.Name == company + "_2");
    if (namafotojson.length > 0) if (namafotojson[0]["Abstract"]["#cdata"].toString().includes("<SPAN>")) namafoto = "FU " + namafotojson[0]["Abstract"]["#cdata"].toString().split("<SPAN>")[1].toString().split("</SPAN>")[0].split("_")[2];
    $("#selectbasemap2").append($('<option>', {
        value: 1,
        text: namafoto,
    }));
    $("#selectbasemap22").append($('<option>', {
        value: 1,
        text: namafoto,
    }));

    //Set select ke 2
    $('#selectbasemap22 option').eq(1).prop('selected', true);

    //Input Source
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/aerial_photo/KPN/MapServer/WMSServer",
        params:
        {
            'LAYERS': company + "_1"
        },
        projection: 'EPSG:3857'
    })
    global_sourcelayerbasemap2.push(source);
    global_sourcelayerbasemap22.push(source);

    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/aerial_photo/KPN/MapServer/WMSServer",
        params:
        {
            'LAYERS': company + "_2"
        },
        projection: 'EPSG:3857'
    })
    global_sourcelayerbasemap2.push(source);
    global_sourcelayerbasemap22.push(source);

    //Masukan ke Map
    global_baselayermap22 = new ol.layer.Tile({
        source: global_sourcelayerbasemap22[1]
    });
    map.addLayer(global_baselayermap22);
    global_baselayermap22.setVisible(false);

    global_baselayermap2 = new ol.layer.Tile({
        source: global_sourcelayerbasemap2[0]
    });
    map.addLayer(global_baselayermap2);
    global_baselayermap2.setVisible(false);

    //Set BaseMap
    for (let i = 0; i < global_jsonlayerbasemap.length; i++) {
        //Input Select
        $("#selectbasemap2").append($('<option>', {
            value: i + 2,
            text: global_jsonlayerbasemap[i]["LayerDisplayName"],
        }));
        $("#selectbasemap22").append($('<option>', {
            value: i + 2,
            text: global_jsonlayerbasemap[i]["LayerDisplayName"],
        }));

        //Input Source
        source = new ol.source.TileWMS({
            url: global_jsonlayerbasemap[i]["LayerURL"],
            params:
            {
                'LAYERS': global_jsonlayerbasemap[i]["LayerURLName"]
            },
            projection: 'EPSG:3857'
        })
        global_sourcelayerbasemap2.push(source);
        global_sourcelayerbasemap22.push(source);
    }

    $("#selectbasemap22").val(2);
    global_baselayermap22.setSource(global_sourcelayerbasemap22[$("#selectbasemap22").val()]);
    global_baselayermap22.setVisible(true);

}

function SetFotoUdaraTHIP() {
    let company, namafoto, namafotojson, source;
    global_sourcelayerbasemap2 = [], global_sourcelayerbasemap22 = [];

    WebserviceGetDataWMS(); // Ambil data serice foto udara

    company = localStorage.getItem("pzoolwmvalue").toString().split("*")[1].replace("PT.", "").replace(" ", "").replace("PANPP", "PANP_P").replace("PANPL", "PANP_L").replace("PANPS", "PANP_S").replace("PLDK", "PLD_K").replace("PLDS", "PLD_S").replace("SUMK", "SUM_K").replace("SUMS", "SUM_S");

    //Input Select
    namafoto = "-";
    namafotojson = global_jsonfoto1.filter(d => d.Name.includes(company));
    if (namafotojson.length > 0)
        if (namafotojson[0]["Abstract"]["#cdata"].toString().includes("<SPAN>"))
            namafoto = "FU " + namafotojson[0]["Abstract"]["#cdata"].toString().split("<SPAN>")[1].toString().split("</SPAN>")[0].split("_")[2];
    $("#selectbasemap2").append($('<option>', {
        value: 0,
        text: namafoto,
    }));
    $("#selectbasemap22").append($('<option>', {
        value: 0,
        text: namafoto,
    }));

    namafoto = "-";
    namafotojson = global_jsonfoto2.filter(d => d.Name.includes(company));
    if (namafotojson.length > 0)
        if (namafotojson[0]["Abstract"]["#cdata"].toString().includes("<SPAN>"))
            namafoto = "FU " + namafotojson[0]["Abstract"]["#cdata"].toString().split("<SPAN>")[1].toString().split("</SPAN>")[0].split("_")[2];
    $("#selectbasemap2").append($('<option>', {
        value: 1,
        text: namafoto,
    }));
    $("#selectbasemap22").append($('<option>', {
        value: 1,
        text: namafoto,
    }));

    //Set select ke 2
    $('#selectbasemap22 option').eq(1).prop('selected', true);

    company = "THIP1";
    namafoto = "-";
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/aerial_photo/KPN/MapServer/WMSServer",
        params:
        {
            'LAYERS': company + "_1"
        },
        projection: 'EPSG:3857'
    })
    global_baselayermap2thip1 = new ol.layer.Tile({
        source: source
    });
    //map.addLayer(global_baselayermap2thip1);
    //global_baselayermap2thip1.setVisible(false);

    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/aerial_photo/KPN/MapServer/WMSServer",
        params:
        {
            'LAYERS': company + "_2"
        },
        projection: 'EPSG:3857'
    })
    global_baselayermap22thip1 = new ol.layer.Tile({
        source: source
    });
    //map.addLayer(global_baselayermap22thip1);
    //global_baselayermap22thip1.setVisible(false);

    company = "THIP2";
    namafoto = "-";
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/aerial_photo/KPN/MapServer/WMSServer",
        params:
        {
            'LAYERS': company + "_1"
        },
        projection: 'EPSG:3857'
    })
    global_baselayermap2thip2 = new ol.layer.Tile({
        source: source
    });
    //map.addLayer(global_baselayermap2thip2);
    //global_baselayermap2thip2.setVisible(false);

    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/aerial_photo/KPN/MapServer/WMSServer",
        params:
        {
            'LAYERS': company + "_2"
        },
        projection: 'EPSG:3857'
    })
    global_baselayermap22thip2 = new ol.layer.Tile({
        source: source
    });
    //map.addLayer(global_baselayermap22thip2);
    //global_baselayermap22thip2.setVisible(false);

    company = "THIP3";
    namafoto = "-";
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/aerial_photo/KPN/MapServer/WMSServer",
        params:
        {
            'LAYERS': company + "_1"
        },
        projection: 'EPSG:3857'
    })
    global_baselayermap2thip3 = new ol.layer.Tile({
        source: source
    });
    //map.addLayer(global_baselayermap2thip3);
    //global_baselayermap2thip3.setVisible(false);

    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/aerial_photo/KPN/MapServer/WMSServer",
        params:
        {
            'LAYERS': company + "_2"
        },
        projection: 'EPSG:3857'
    })
    global_baselayermap22thip3 = new ol.layer.Tile({
        source: source
    });
    //map.addLayer(global_baselayermap22thip3);
    //global_baselayermap22thip3.setVisible(false);

    company = "THIP4";
    namafoto = "-";
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/aerial_photo/KPN/MapServer/WMSServer",
        params:
        {
            'LAYERS': company + "_1"
        },
        projection: 'EPSG:3857'
    })
    global_baselayermap2thip4 = new ol.layer.Tile({
        source: source
    });
    //map.addLayer(global_baselayermap2thip4);
    //global_baselayermap2thip4.setVisible(false);

    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/aerial_photo/KPN/MapServer/WMSServer",
        params:
        {
            'LAYERS': company + "_2"
        },
        projection: 'EPSG:3857'
    })
    global_baselayermap22thip4 = new ol.layer.Tile({
        source: source
    });
    //map.addLayer(global_baselayermap22thip4);
    //global_baselayermap22thip4.setVisible(false);

    //Masukan ke Map
    //global_baselayermap22 = new ol.layer.Tile({
    //    source: global_sourcelayerbasemap22[1]
    //});
    //map.addLayer(global_baselayermap22);
    //global_baselayermap22.setVisible(false);

    //global_baselayermap2 = new ol.layer.Tile({
    //    source: global_sourcelayerbasemap2[0]
    //});
    //map.addLayer(global_baselayermap2);
    //global_baselayermap2.setVisible(false);

    globalGroupLayer = [];

    global_baselayermap2 = new ol.layer.Group({
        layers: [global_baselayermap2thip1, global_baselayermap2thip2, global_baselayermap2thip3, global_baselayermap2thip4],
        title: '0'
    });

    global_baselayermap22 = new ol.layer.Group({
        layers: [global_baselayermap22thip1, global_baselayermap22thip2, global_baselayermap22thip3, global_baselayermap22thip4],
        title: '1'
    });

    globalGroupLayer.push(global_baselayermap2);

    map.addLayer(global_baselayermap2);
    global_baselayermap2.setVisible(false);

    globalGroupLayer.push(global_baselayermap22);

    map.addLayer(global_baselayermap22);
    global_baselayermap22.setVisible(false);

    //Set BaseMap
    var dataLayerBasemapDistinct = _.uniq(global_jsonlayerbasemap, x => x.LayerDisplayName);

    let idxSelect = 1;
    let idxWM = 1;
    for (let i = 0; i < dataLayerBasemapDistinct.length; i++) {

        var alayersPref = [];
        var alayersPrefX = [];
        for (let j = 0; j < global_jsonlayerbasemap.length; j++) {
            if (global_jsonlayerbasemap[j]["LayerDisplayName"] == dataLayerBasemapDistinct[i]["LayerDisplayName"]) {

                alayersPref.push(
                    new ol.layer.Tile({
                        source: new ol.source.TileWMS({
                            url: global_jsonlayerbasemap[j]["LayerURL"],
                            params:
                            {
                                'LAYERS': global_jsonlayerbasemap[j]["LayerURLName"]
                            },
                            projection: 'EPSG:3857'
                        })
                    })
                );
                alayersPrefX.push({
                    url: global_jsonlayerbasemap[j]["LayerURL"],
                    LAYERS: global_jsonlayerbasemap[j]["LayerURLName"]
                });
            }
        }

        if (alayersPref.length > 0) {
            dataLayerBasemapDistinct[i]["alayersPrefX"] = alayersPrefX;
            dataLayerBasemapDistinct[i]["alayersPref"] = alayersPref;

            idxSelect++;
            var lGroup2 = new ol.layer.Group({
                layers: alayersPref,
                title: idxSelect
            });
            map.addLayer(lGroup2);
            lGroup2.setVisible(false);
            globalGroupLayer.push(lGroup2);

            //Input Select
            $("#selectbasemap2").append($('<option>', {
                value: idxSelect,
                text: dataLayerBasemapDistinct[i]["LayerDisplayName"],
            }));
            $("#selectbasemap22").append($('<option>', {
                value: idxSelect,
                text: dataLayerBasemapDistinct[i]["LayerDisplayName"],
            }));

            if (dataLayerBasemapDistinct[i]["LayerDisplayName"] == "Basemap WM"){
                idxWM = idxSelect;
            }
        }
    }

    $("#selectbasemap22").val(idxWM);
    globalGroupLayer[$("#selectbasemap22").val()].setVisible(true);

    console.info("dataLayerBasemapDistinct", dataLayerBasemapDistinct);
}

function SetVectorBaseTHIP() {

    //Jalan
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer",
        params: {
            'LAYERS': "RDS2",
            'TILED': true,
            'layerDefs': "{\"RDS2\":\"CompanyCode='PT.THIP'\"}"
        },
        projection: 'EPSG:3857'
    });
    global_layerjalan = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerjalan);
    global_layerjalan.setVisible(false);

    //Boundary
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer",
        params: {
            'LAYERS': "BDY3",
            'TILED': true,
            'layerDefs': "{\"BDY3\":\"CompanyCode='PT.THIP'\"}"
        },
        projection: 'EPSG:3857'
    });
    global_layerboundary = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerboundary);
    global_layerboundary.setVisible(false);

    //Afdeling
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer",
        params: {
            'LAYERS': "DIV3",
            'TILED': true,
            'layerDefs': "{\"BDY3\":\"CompanyCode='PT.THIP'\"}"
        },
        projection: 'EPSG:3857'
    });
    global_layerafdeling = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerafdeling);
    global_layerafdeling.setVisible(false);

    //Block	
    //let tipefilter = "GroupCompanyName+IN+(%27THIP+1%27%2C+%27THIP+2%27%2C+%27THIP+3%27%2C+%27THIP+4%27)";
    let tipefilter = "CompanyCode ='PT.THIP'";

    global_layerblock = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: "https://app.gis-div.com/arcgis/rest/services/pub_waterway/basemap/MapServer/7/query?where=1%3D1+AND+" + tipefilter + "&geometryType=esriGeometryPolygon&outFields=GroupCompanyName%2C+CompanyCode%2C+ESTNR%2CBlock&featureEncoding=esriDefault&f=geojson",
            format: new ol.format.GeoJSON()
        })
    });
    map.addLayer(global_layerblock);
    global_layerblock.setVisible(false);

    global_extentblock = [];
    let vectorSource = global_layerblock.getSource();
    vectorSource.on('change', function () {
        global_layerblock.setStyle(StyleFunctionLabelBlockOn);
        let features = vectorSource.getFeatures();
        let jsonblock = [];
        features.forEach(function (obj) {
            //console.log(obj);
            //console.log(obj.getProperties().Block);
            let sap = global_estatemappingnew.filter(d => d.EstCode == obj.getProperties().ESTNR);
            jsonblock.push(
                {
                    "estcodesap": sap[0]["EstCodeSAP"],
                    "block": obj.getProperties().Block
                }
            );

            //$('#selectblock').append('<option data-content="<span class=\'selectinput\'>' + obj.getProperties().Block + '</span>" value="' + obj.getProperties().Block + '">' + obj.getProperties().Block + '</option>');
            global_extentblock.push(
                {
                    "block": sap[0]["EstCodeSAP"] + " - " + obj.getProperties().Block,
                    "extent": obj.getGeometry().getExtent()
                }
            );
        });

        jsonblock = jsonblock.filter(function (a) {
            var key = a.estcodesap + '|' + a.block;
            if (!this[key]) {
                this[key] = true;
                return true;
            }
        }, Object.create(null)).sort((a, b) => a.estcodesap.localeCompare(b.estcodesap) || a.block.localeCompare(b.block));

        for (let i = 0; i < jsonblock.length; i++) {
            $('#selectblock').append('<option data-content="<span class=\'selectinput\'>' + jsonblock[i]["estcodesap"] + " - " + jsonblock[i]["block"] + '</span>" value="' + jsonblock[i]["estcodesap"] + " - " + jsonblock[i]["block"] + '">' + jsonblock[i]["estcodesap"] + " - " + jsonblock[i]["block"] + '</option>');
        }

        $('#selectblock').selectpicker('refresh');
    });
    
    global_layerblockzona = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: "https://app.gis-div.com/arcgis/rest/services/pub_waterway/basemap/MapServer/7/query?where=1%3D1+AND+" + tipefilter + "&geometryType=esriGeometryPolygon&outFields=GroupCompanyName%2C+CompanyCode%2C+ESTNR%2CBlock&featureEncoding=esriDefault&f=geojson",
            format: new ol.format.GeoJSON()
        })
    });
    map.addLayer(global_layerblockzona);
    global_layerblockzona.setVisible(false);


    global_layerblockManual = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: "https://app.gis-div.com/arcgis/rest/services/pub_waterway/basemap/MapServer/7/query?where=1%3D1+AND+" + tipefilter + "&geometryType=esriGeometryPolygon&outFields=GroupCompanyName%2C+CompanyCode%2C+ESTNR%2CBlock&featureEncoding=esriDefault&f=geojson",
            format: new ol.format.GeoJSON()
        })
    });
    map.addLayer(global_layerblockManual);
    global_layerblockManual.setVisible(false);

    //Landuse
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer",
        params: {
            'LAYERS': "LUS3",
            'TILED': true,
            'layerDefs': "{\"LUS3\":\"CompanyCode='PT.THIP'\"}"
        },
        projection: 'EPSG:3857'
    });
    global_layerlanduse = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerlanduse);
    global_layerlanduse.setVisible(false);

    //Dead Palm
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP1" + "/MapServer/WMSServer",
        params: {
            'LAYERS': "OPL1_D",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layerdeadpalm1 = new ol.layer.Tile({
        source: source
    });

    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP2" + "/MapServer/WMSServer",
        params: {
            'LAYERS': "OPL1_D",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layerdeadpalm2 = new ol.layer.Tile({
        source: source
    });

    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP3" + "/MapServer/WMSServer",
        params: {
            'LAYERS': "OPL1_D",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layerdeadpalm3 = new ol.layer.Tile({
        source: source
    });

    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP4" + "/MapServer/WMSServer",
        params: {
            'LAYERS': "OPL1_D",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layerdeadpalm4 = new ol.layer.Tile({
        source: source
    });

    global_layerdeadpalm = new ol.layer.Group({
        layers: [global_layerdeadpalm1, global_layerdeadpalm2, global_layerdeadpalm3, global_layerdeadpalm4],
        title: '0'
    });
    map.addLayer(global_layerdeadpalm);
    global_layerdeadpalm.setVisible(false);

    //Oil Palm
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP1" + "/MapServer/WMSServer",
        params: {
            'LAYERS': "OPL1",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layeroilpalm1 = new ol.layer.Tile({
        source: source
    });

    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP2" + "/MapServer/WMSServer",
        params: {
            'LAYERS': "OPL1",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layeroilpalm2 = new ol.layer.Tile({
        source: source
    });

    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP3" + "/MapServer/WMSServer",
        params: {
            'LAYERS': "OPL1",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layeroilpalm3 = new ol.layer.Tile({
        source: source
    });

    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP4" + "/MapServer/WMSServer",
        params: {
            'LAYERS': "OPL1",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layeroilpalm4 = new ol.layer.Tile({
        source: source
    });

    global_layeroilpalm = new ol.layer.Group({
        layers: [global_layeroilpalm1, global_layeroilpalm2, global_layeroilpalm3, global_layeroilpalm4],
        title: '0'
    });
    map.addLayer(global_layeroilpalm);
    global_layeroilpalm.setVisible(false);

    ////TPH
    //source = new ol.source.TileWMS({
    //	url: "https://app.gis-div.com/arcgis/services/pub_apiweb/vector/MapServer/WMSServer" + filtertph,
    //	params: {
    //		'LAYERS': "TPH",
    //		'TILED': true
    //	},
    //	projection: 'EPSG:3857'
    //});
    //global_layertph = new ol.layer.Tile({
    //	source: source
    //});
    //map.addLayer(global_layertph);
    //global_layertph.setVisible(false);

    //Label
    //Label Boundary
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer",
        params: {
            'LAYERS': "Anno_BDY3",
            'TILED': true,
            'layerDefs': "{\"Anno_BDY3\":\"CompanyCode='PT.THIP'\"}"
        },
        projection: 'EPSG:3857'
    });
    global_layerlabelboundary = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerlabelboundary);
    global_layerlabelboundary.setVisible(false);

    //Label Afdeling
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer",
        params: {
            'LAYERS': "Anno_DIV3",
            'TILED': true,
            'layerDefs': "{\"Anno_DIV3\":\"CompanyCode='PT.THIP'\"}"
        },
        projection: 'EPSG:3857'
    });
    global_layerlabelafdeling = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerlabelafdeling);
    global_layerlabelafdeling.setVisible(false);

}


function SetVectorBase() {
    let filter = "";
    let filterBDY = "";
    let filterdeadpalm = "", filteroilpalm = "", filtertph;
    let companyapiweb = localStorage.getItem("pzoolwmvalue").split("*")[1].toString().replace("PT.", "").replace(" ", "");
    if (localStorage.getItem("pzoolwmvalue").split("*")[0] == "all") {
        //PT
        if (localStorage.getItem("pzoolwmvalue").split("*")[1].includes("THIP") && localStorage.getItem("pzoolwmvalue").split("*")[1] != "PT.THIP") {
            //THIP			
            filter = "\"GroupCompanyName  ='" + localStorage.getItem("pzoolwmvalue").split("*")[1].toString().replace("PT.", "") + "'\"";
            filtertph = "?layerDefs={\"TPH\":" + "\"GroupCompa ='" + localStorage.getItem("pzoolwmvalue").split("*")[1].toString().replace("PT.", "") + "'\"" + "}";
        }
        else {
            //Non THIP			
            filter = "\"CompanyCode ='" + localStorage.getItem("pzoolwmvalue").split("*")[1] + "'\"";
            filtertph = "?layerDefs={\"TPH\":" + "\"CompanyCod ='" + localStorage.getItem("pzoolwmvalue").split("*")[1].toString().replace("PT.", "") + "'\"" + "}";
        }
        filterBDY = "\"CompanyCode ='" + localStorage.getItem("pzoolwmvalue").split("*")[1].toString().split(" ")[0] + "'\"";
    }
    else {
        //Estate
        filter = "\"ESTNR='" + localStorage.getItem("pzoolwmvalue").split("*")[0] + "'\"";
        filterBDY = "\"ESTNR='" + localStorage.getItem("pzoolwmvalue").split("*")[0] + "'\"";
        filterdeadpalm = "?layerDefs={\"OPL1_D\":" + filter + "}";
        filteroilpalm = "?layerDefs={\"OPL1\":" + filter + "}";
        filtertph = "?layerDefs={\"TPH\":" + "\"GroupCompa ='" + "THIP 1" + "'\"" + "}";
    }

    //Jalan
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer?layerDefs={\"RDS2\":" + filter + "}",
        params: {
            'LAYERS': "RDS2",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layerjalan = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerjalan);
    global_layerjalan.setVisible(false);

    //Boundary
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer?layerDefs={\"BDY3\":" + filterBDY + "}",
        params: {
            'LAYERS': "BDY3",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layerboundary = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerboundary);
    global_layerboundary.setVisible(false);

    //Afdeling
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer?layerDefs={\"DIV3\":" + filter + "}",
        params: {
            'LAYERS': "DIV3",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layerafdeling = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerafdeling);
    global_layerafdeling.setVisible(false);

    //Block	
    let tipefilter = "", valuefilter = "";
    if (localStorage.getItem("pzoolwmvalue").split("*")[0] == "all") {
        //PT
        if (localStorage.getItem("pzoolwmvalue").split("*")[1].includes("THIP") && localStorage.getItem("pzoolwmvalue").split("*")[1] != "PT.THIP") {
            //THIP
            tipefilter = "GroupCompanyName";
            valuefilter = localStorage.getItem("pzoolwmvalue").split("*")[1].toString().replace("PT.", "");
        }
        else {
            //Non THIP
            tipefilter = "CompanyCode";
            valuefilter = localStorage.getItem("pzoolwmvalue").split("*")[1];
        }
    }
    else {
        //Estate
        tipefilter = "ESTNR";
        valuefilter = localStorage.getItem("pzoolwmvalue").split("*")[0];
    }

    global_layerblock = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: "https://app.gis-div.com/arcgis/rest/services/pub_waterway/basemap/MapServer/7/query?where=1%3D1+AND+" + tipefilter + "='" + valuefilter + "'&geometryType=esriGeometryPolygon&outFields=GroupCompanyName%2C+CompanyCode%2C+ESTNR%2CBlock&featureEncoding=esriDefault&f=geojson",
            format: new ol.format.GeoJSON()
        })
    });
    map.addLayer(global_layerblock);
    global_layerblock.setVisible(false);
    global_extentblock = [];
    let vectorSource = global_layerblock.getSource();
    vectorSource.on('change', function () {
        global_layerblock.setStyle(StyleFunctionLabelBlockOn);
        let features = vectorSource.getFeatures();
        let jsonblock = [];
        features.forEach(function (obj) {
            //console.log(obj);
            //console.log(obj.getProperties().Block);
            let sap = global_estatemappingnew.filter(d => d.EstCode == obj.getProperties().ESTNR);
            jsonblock.push(
                {
                    "estcodesap": sap[0]["EstCodeSAP"],
                    "block": obj.getProperties().Block
                }
            );

            //$('#selectblock').append('<option data-content="<span class=\'selectinput\'>' + obj.getProperties().Block + '</span>" value="' + obj.getProperties().Block + '">' + obj.getProperties().Block + '</option>');
            global_extentblock.push(
                {
                    "block": sap[0]["EstCodeSAP"] + " - " + obj.getProperties().Block,
                    "extent": obj.getGeometry().getExtent()
                }
            );
        });

        jsonblock = jsonblock.filter(function (a) {
            var key = a.estcodesap + '|' + a.block;
            if (!this[key]) {
                this[key] = true;
                return true;
            }
        }, Object.create(null)).sort((a, b) => a.estcodesap.localeCompare(b.estcodesap) || a.block.localeCompare(b.block));

        for (let i = 0; i < jsonblock.length; i++) {
            $('#selectblock').append('<option data-content="<span class=\'selectinput\'>' + jsonblock[i]["estcodesap"] + " - " + jsonblock[i]["block"] + '</span>" value="' + jsonblock[i]["estcodesap"] + " - " + jsonblock[i]["block"] + '">' + jsonblock[i]["estcodesap"] + " - " + jsonblock[i]["block"] + '</option>');
        }

        $('#selectblock').selectpicker('refresh');
    });

    global_layerblockzona = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: "https://app.gis-div.com/arcgis/rest/services/pub_waterway/basemap/MapServer/7/query?where=1%3D1+AND+" + tipefilter + "='" + valuefilter + "'&geometryType=esriGeometryPolygon&outFields=GroupCompanyName%2C+CompanyCode%2C+ESTNR%2CBlock&featureEncoding=esriDefault&f=geojson",
            format: new ol.format.GeoJSON()
        })
    });
    map.addLayer(global_layerblockzona);
    global_layerblockzona.setVisible(false);


    global_layerblockManual = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: "https://app.gis-div.com/arcgis/rest/services/pub_waterway/basemap/MapServer/7/query?where=1%3D1+AND+" + tipefilter + "='" + valuefilter + "'&geometryType=esriGeometryPolygon&outFields=GroupCompanyName%2C+CompanyCode%2C+ESTNR%2CBlock&featureEncoding=esriDefault&f=geojson",
            format: new ol.format.GeoJSON()
        })
    });
    map.addLayer(global_layerblockManual);
    global_layerblockManual.setVisible(false);

    //Landuse
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer?layerDefs={\"LUS3\":" + filter + "}",
        params: {
            'LAYERS': "LUS3",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layerlanduse = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerlanduse);
    global_layerlanduse.setVisible(false);

    if (localStorage.getItem("pzoolwmvalue").split("*")[1] != "PT.THIP") {
        //Dead Palm
        source = new ol.source.TileWMS({
            url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + companyapiweb + "/MapServer/WMSServer" + filterdeadpalm,
            params: {
                'LAYERS': "OPL1_D",
                'TILED': true
            },
            projection: 'EPSG:3857'
        });
        global_layerdeadpalm = new ol.layer.Tile({
            source: source
        });
        map.addLayer(global_layerdeadpalm);
        global_layerdeadpalm.setVisible(false);

        //Oil Palm
        source = new ol.source.TileWMS({
            url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + companyapiweb + "/MapServer/WMSServer" + filteroilpalm,
            params: {
                'LAYERS': "OPL1",
                'TILED': true
            },
            projection: 'EPSG:3857'
        });
        global_layeroilpalm = new ol.layer.Tile({
            source: source
        });
        map.addLayer(global_layeroilpalm);
        global_layeroilpalm.setVisible(false);
    }
    else {
        //Dead Palm
        source = new ol.source.TileWMS({
            url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP1" + "/MapServer/WMSServer",
            params: {
                'LAYERS': "OPL1_D",
                'TILED': true
            },
            projection: 'EPSG:3857'
        });
        global_layerdeadpalm1 = new ol.layer.Tile({
            source: source
        });
        map.addLayer(global_layerdeadpalm1);
        global_layerdeadpalm1.setVisible(false);

        source = new ol.source.TileWMS({
            url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP2" + "/MapServer/WMSServer",
            params: {
                'LAYERS': "OPL1_D",
                'TILED': true
            },
            projection: 'EPSG:3857'
        });
        global_layerdeadpalm2 = new ol.layer.Tile({
            source: source
        });
        map.addLayer(global_layerdeadpalm2);
        global_layerdeadpalm2.setVisible(false);

        source = new ol.source.TileWMS({
            url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP3" + "/MapServer/WMSServer",
            params: {
                'LAYERS': "OPL1_D",
                'TILED': true
            },
            projection: 'EPSG:3857'
        });
        global_layerdeadpalm3 = new ol.layer.Tile({
            source: source
        });
        map.addLayer(global_layerdeadpalm3);
        global_layerdeadpalm3.setVisible(false);

        source = new ol.source.TileWMS({
            url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP4" + "/MapServer/WMSServer",
            params: {
                'LAYERS': "OPL1_D",
                'TILED': true
            },
            projection: 'EPSG:3857'
        });
        global_layerdeadpalm4 = new ol.layer.Tile({
            source: source
        });
        map.addLayer(global_layerdeadpalm4);
        global_layerdeadpalm4.setVisible(false);

        //Oil Palm
        source = new ol.source.TileWMS({
            url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP1" + "/MapServer/WMSServer",
            params: {
                'LAYERS': "OPL1",
                'TILED': true
            },
            projection: 'EPSG:3857'
        });
        global_layeroilpalm1 = new ol.layer.Tile({
            source: source
        });
        map.addLayer(global_layeroilpalm1);
        global_layeroilpalm1.setVisible(false);

        source = new ol.source.TileWMS({
            url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP2" + "/MapServer/WMSServer",
            params: {
                'LAYERS': "OPL1",
                'TILED': true
            },
            projection: 'EPSG:3857'
        });
        global_layeroilpalm2 = new ol.layer.Tile({
            source: source
        });
        map.addLayer(global_layeroilpalm2);
        global_layeroilpalm2.setVisible(false);

        source = new ol.source.TileWMS({
            url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP3" + "/MapServer/WMSServer",
            params: {
                'LAYERS': "OPL1",
                'TILED': true
            },
            projection: 'EPSG:3857'
        });
        global_layeroilpalm3 = new ol.layer.Tile({
            source: source
        });
        map.addLayer(global_layeroilpalm3);
        global_layeroilpalm3.setVisible(false);

        source = new ol.source.TileWMS({
            url: "https://app.gis-div.com/arcgis/services/pub_apiweb/" + "THIP4" + "/MapServer/WMSServer",
            params: {
                'LAYERS': "OPL1",
                'TILED': true
            },
            projection: 'EPSG:3857'
        });
        global_layeroilpalm4 = new ol.layer.Tile({
            source: source
        });
        map.addLayer(global_layeroilpalm4);
        global_layeroilpalm4.setVisible(false);
    }

    ////TPH
    //source = new ol.source.TileWMS({
    //	url: "https://app.gis-div.com/arcgis/services/pub_apiweb/vector/MapServer/WMSServer" + filtertph,
    //	params: {
    //		'LAYERS': "TPH",
    //		'TILED': true
    //	},
    //	projection: 'EPSG:3857'
    //});
    //global_layertph = new ol.layer.Tile({
    //	source: source
    //});
    //map.addLayer(global_layertph);
    //global_layertph.setVisible(false);

    //Label
    //Label Boundary
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer?layerDefs={\"Anno_BDY3\":" + filter + "}",
        params: {
            'LAYERS': "Anno_BDY3",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layerlabelboundary = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerlabelboundary);
    global_layerlabelboundary.setVisible(false);

    //Label Afdeling
    source = new ol.source.TileWMS({
        url: "https://app.gis-div.com/arcgis/services/pub_waterway/basemap/MapServer/WMSServer?layerDefs={\"Anno_DIV3\":" + filter + "}",
        params: {
            'LAYERS': "Anno_DIV3",
            'TILED': true
        },
        projection: 'EPSG:3857'
    });
    global_layerlabelafdeling = new ol.layer.Tile({
        source: source
    });
    map.addLayer(global_layerlabelafdeling);
    global_layerlabelafdeling.setVisible(false);

}

function SetOSM() {
    //OSM
    global_sourcelayerbasemap = [];
    global_sourcelayerbasemap.push(
        new ol.source.OSM()
    );
    //Bing
    global_sourcelayerbasemap.push(
        new ol.source.BingMaps({
            key: 'AsSujTcrcX2Vz9PqFuf76YVGnmJJdQrZ0xd9xxna55YvUqqkxzlkcpJdW-fw6DR4',
            imagerySet: 'Aerial'
        })
    );
    global_sourcelayerbasemap.push(
        new ol.source.BingMaps({
            key: 'AsSujTcrcX2Vz9PqFuf76YVGnmJJdQrZ0xd9xxna55YvUqqkxzlkcpJdW-fw6DR4',
            imagerySet: 'Road'
        })
    );

    //Input Select Basemap
    $("#selectbasemap").append($('<option>', {
        value: 0,
        text: "Open Street Map",
    }));
    $("#selectbasemap").append($('<option>', {
        value: 1,
        text: "BING Aerial",
    }));
    $("#selectbasemap").append($('<option>', {
        value: 2,
        text: "BING Road",
    }));

    //Input layer ke map
    global_baselayermap = new ol.layer.Tile({
        source: global_sourcelayerbasemap[0]
    });
    map.addLayer(global_baselayermap);
    global_baselayermap.setVisible(false);
}



function StyleFunctionLabelBlockOn(feature, resolution) {
    let style = new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(1, 43, 54, 0)'
        }),
        stroke: new ol.style.Stroke({
            color: '#f1f736',
            width: 1
        }),
        text: new ol.style.Text({
            font: '13px Calibri,sans-serif',
            fill: new ol.style.Fill({
                color: '#000'
            }),
            stroke: new ol.style.Stroke({
                color: 'yellow',
                width: 4
            }),
            placement: 'point'
        })
    });
    style.getText().setText(feature.get('Block'));
    return style;
}

function StyleFunctionLabelBlockOff(feature, resolution) {
    let index = global_jsonparit.findIndex(d => d.code == feature.get('code_drain'));
    let style = new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(1, 43, 54, 0)'
        }),
        stroke: new ol.style.Stroke({
            color: '#f1f736',
            width: 1
        })
    });
    return style;
}

function WebserviceGetDataWMS() {
    $.ajax({
        type: "get",
        url: "https://app.gis-div.com/arcgis/services/aerial_photo/KPN/MapServer/WMSServer?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities",
        dataType: "xml",
        async: false,
        success: function (data) {

            let json = JSON.parse(xml2json(data, ""));
            global_jsonfoto1 = JSON.parse(xml2json(data, ""))["WMS_Capabilities"]["Capability"]["Layer"]["Layer"][2]["Layer"];
            global_jsonfoto2 = JSON.parse(xml2json(data, ""))["WMS_Capabilities"]["Capability"]["Layer"]["Layer"][1]["Layer"];
        },
        error: function (xhr, status) {
            console.log(status);
        }
    });
}

function WebServiceGetLayerBaseMap() {
    let area = localStorage.getItem("pzoolwmvalue").split("*")[1];
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceAnalysis.asmx/GetLayerBaseMap',
        data: JSON.stringify({
            area: area
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (response) {
            if (response.d.includes("Error : ")) {
                $("#loader").hide();
                Pesan("Informasi Error GetLayerBaseMap", response.d);
            }
            else {
                global_jsonlayerbasemap = JSON.parse(response.d);
                console.log(global_jsonlayerbasemap, "global_jsonlayerbasemap");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#loader").hide();
            alert("Maaf terjadi kesalahan GetLayerBaseMap.");
        }
    });
}
