﻿
function SetLayer() {
    //Hgighlt circle animate
    global_layeranimate = new ol.layer.Tile({
        source: new ol.source.OSM({
            wrapX: false,
        }),
    });
    map.addLayer(global_layeranimate);
    global_layeranimate.setOpacity(0);

    //Set Checkbox	
    $('#chkwater').prop('checked', false);
    //$('#chkasset').prop('checked', true);
    //$('#chkassetsurvey').prop('checked', false);
    //$('#chkassetqc').prop('checked', false);
    $('#chkwaterlabel').prop('checked', false);


    let tipefilter = "", valuefilter = "";
    if (localStorage.getItem("pzoolwmvalue").split("*")[0] == "all") {
        //PT
        if (localStorage.getItem("pzoolwmvalue").split("*")[1].includes("THIP")) {
            //THIP
            tipefilter = "CompanyCode";
            valuefilter = "PT.THIP";
        }
        else {
            //Non THIP
            tipefilter = "CompanyCode";
            valuefilter = localStorage.getItem("pzoolwmvalue").split("*")[1];
        }
    }
    else {
        //Estate
        tipefilter = "ESTNR";
        valuefilter = localStorage.getItem("pzoolwmvalue").split("*")[0];
    }

    //Vector Measure
    global_measuresource = new ol.source.Vector();
    let measurevector = new ol.layer.Vector({
        source: global_measuresource,
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(96, 204, 181, 0.2)',
            }),
            stroke: new ol.style.Stroke({
                color: '#60ccb5',
                width: 3,
            }),
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({
                    color: '#60ccb5',
                }),
            }),
        }),
    });
    map.addLayer(measurevector);
    global_tipemeasure = "LineString";

    //Parit
    //Set Div Parit
    global_layerwater = [];
    let content = '<table style="width:100%;">';
    for (let i = 0; i < global_jsonparit.length; i++) {
        let check = "";
        //if (global_jsonparit[i]["checklist"] == 1) check = 'checked="checked"';
        content += '<tr><td align="center" style="cursor:pointer;" onClick="ChangeStyle(\'' + i + '\',\'' + global_jsonparit[i]["code"] + ' - ' + global_jsonparit[i]["nama"] + '\',\'' + global_jsonparit[i]["size"] + '\',\'' + global_jsonparit[i]["color"] + '\',\'' + global_jsonparit[i]["code"] + '\');"><div id="divstyle' + i + '" style="width:10px; height:' + global_jsonparit[i]["size"] + 'px; background-color:' + global_jsonparit[i]["color"] + ';"></div></td>';
        content += '<td>';
        content += '<input type="checkbox" id="chkwater' + (i) + '" ' + check + '/>';
        content += '<label id="labelwater' + (i) + '" class="labelstyle" for="chkwater' + (i) + '" style="top:4px; position:relative; cursor:pointer;">' + global_jsonparit[i]["nama"] + '</label>';
        content += '</td>';
        content += '<td align="right"><div id="sliderwater' + (i) + '" style="width:40px;"></div></td>';
        content += "</tr>";

        //Set Layer
        global_layerwater.push(
            new ol.layer.Vector({
                source: new ol.source.Vector({
                    url: "https://app.gis-div.com/arcgis/rest/services/pub_waterway/WTR2_edit/MapServer/0/query?where=1%3D1+AND+" + tipefilter + "='" + valuefilter + "'+AND+code_drain=" + global_jsonparit[i]["code"] + "&geometryType=esriGeometryPolygon&outFields=GroupCompanyName%2C+CompanyCode%2C+ESTNR%2CBlock%2Ccode_drain%2CGlobalID&featureEncoding=esriDefault&f=geojson",
                    format: new ol.format.GeoJSON()
                }),
                style: function (feature) {
                    return new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: global_jsonparit[i]["color"],
                            width: global_jsonparit[i]["size"]
                        })
                    });
                }
            })
        );

        map.addLayer(global_layerwater[i]);
        let vectorSource2 = global_layerwater[i].getSource();
        vectorSource2.on('change', function () {
            //if (global_jsonparit[i]["checklist"] == 0)
            global_layerwater[i].setVisible(false);
        });
    }

    content += "</table>";
    $("#divtableparit").html(content);
}

function HandlerMap() {

    $("#btnGo").click(function () {
        setLocalStorage();
        //window.location.href = "WLR_OnlineMapNew.aspx";

        $("#loader").show();
    });

    $("#sliderwater").slider({ // Slider base
        value: 100,
        slide: function (e, ui) {
            for (let i = 0; i < global_layerwater.length; i++) {
                global_layerwater[i].setOpacity(ui.value / 100);
                $("#sliderwater" + i).slider('value', ui.value);
            }
        }
    });

    $("#chkwater").change(function () { // On Off Layerbase
        if ($(this).is(":checked")) {
            for (let i = 0; i < global_layerwater.length; i++) {
                global_layerwater[i].setVisible(true);
                $("#chkwater" + i).prop('checked', true);
            }
        }
        else {
            for (let i = 0; i < global_layerwater.length; i++) {
                global_layerwater[i].setVisible(false);
                $("#chkwater" + i).prop('checked', false);
            }
        }
        SetTableInfo();
    });

    $("#chkwaterlabel").change(function () {
        if ($(this).is(":checked")) {
            for (let i = 0; i < global_layerwater.length; i++) {
                global_layerwater[i].setStyle(StyleFunctionLabelOn);
            }
        }
        else {
            for (let i = 0; i < global_layerwater.length; i++) {
                global_layerwater[i].setStyle(StyleFunctionLabelOff);
            }
        }
    });

    for (let i = 0; i < global_layerwater.length; i++) {
        $("#sliderwater" + i).slider({ // Slider base
            value: 100,
            slide: function (e, ui) {
                global_layerwater[i].setOpacity(ui.value / 100);
            }
        });
        $("#chkwater" + i).change(function () { // On Off Layerbase
            if ($(this).is(":checked")) {
                global_layerwater[i].setVisible(true);
            }
            else {
                global_layerwater[i].setVisible(false);
            }
            SetTableInfo();
        });

        $("#labelwater" + i).bind("contextmenu", function (e) {
            BlinkLayer(i);
            return false;
        });
    }
}

function StyleFunctionLabelOn(feature, resolution) {
    let index = global_jsonparit.findIndex(d => d.code == feature.get('code_drain'));
    let style = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: global_jsonparit[index]["color"],
            width: global_jsonparit[index]["size"]
        }),
        text: new ol.style.Text({
            font: '13px Calibri,sans-serif',
            fill: new ol.style.Fill({
                color: '#000'
            }),
            stroke: new ol.style.Stroke({
                color: '#fff',
                width: 1
            }),
            offsetY: 20,
            offsetX: 20,
            placement: 'line'
        })
    });
    style.getText().setText(feature.get('block'));
    return style;
}

function StyleFunctionLabelOff(feature, resolution) {
    let index = global_jsonparit.findIndex(d => d.code == feature.get('code_drain'));
    let style = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: global_jsonparit[index]["color"],
            width: global_jsonparit[index]["size"]
        })
    });
    return style;
}

function BlinkLayer(index) {
    //global_jsonparit[global_id]["size"] = $("#txtstylesize").val();
    //global_jsonparit[global_id]["color"] = $("#txtstylecolor").val();

    let style1 = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "Blue",
            width: 6
        })
    });
    let style2 = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: "Orange",
            width: 6
        })
    });
    let style3 = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: global_jsonparit[index]["color"],
            width: global_jsonparit[index]["size"]
        })
    });

    let time = 500;
    setTimeout(function () {
        global_layerwater[index].setStyle(style1);
        setTimeout(function () {
            global_layerwater[index].setStyle(style2);
            setTimeout(function () {
                global_layerwater[index].setStyle(style1);
                setTimeout(function () {
                    global_layerwater[index].setStyle(style2);
                    setTimeout(function () {
                        global_layerwater[index].setStyle(style3);
                    }, time);
                }, time);
            }, time);
        }, time);
    }, time);

}

function SetTableInfo() {
    let content = '<table border="1" style="background-color:white; color:black !important; width:100%; font-size:12px; font-family:Calibri;">';
    content += '<tr style="background-color:#315eba;  font-weight:bold; color:white;">';
    content += '<td style="padding:5px;" align="center">Code</td>';
    content += '<td style="padding:5px;">Name</td>';
    content += '<td align="right" style="padding:5px;">Length (M)</td>';
    content += '</tr>';
    let total = 0;
    for (let i = 0; i < global_jsonparitlength.length; i++) {
        //console.log($('#chkwater' + i).is(":checked"),"chk"+i);
        if ($('#chkwater' + i).is(":checked")) {
            content += '<tr style="">';
            content += '<td style="padding:5px;" align="center">' + global_jsonparitlength[i]["code"] + '</td>';
            content += '<td style="padding:5px;">' + global_jsonparitlength[i]["nama"] + '</td>';
            content += '<td align="right" style="padding:5px;">' + global_jsonparitlength[i]["panjang"].toLocaleString('en-US', { minimumFractionDigits: 0, maximumFractionDigits: 0 }) + '</td>';
            content += '</tr>';
            total += global_jsonparitlength[i]["panjang"];
        }
    }
    content += '<tr style="font-weight:bold;">';
    content += '<td colspan="2" align="center" style="padding:5px;">Total</td>';
    content += '<td align="right" style="padding:5px;">' + total.toLocaleString('en-US', { minimumFractionDigits: 0, maximumFractionDigits: 0 }) + '</td>';
    content += '</tr>';

    content += '</table>';
    $("#divtableinfo").html(content);
}



function WebserviceGetParit() {
    let fc;
    let kebun;
    let estcode, estnewcode, companycode;
    if (localStorage.getItem("pzoolwmvalue").split("*")[0] == "all") {
        //PT
        //if (localStorage.getItem("pzoolwmvalue").split("*")[1].includes("THIP")) {
        //	//THIP
        //	fc = 3;
        //	kebun = localStorage.getItem("pzoolwmvalue").split("*")[1].toString().replace("PT.", "");
        //}
        //else {
        //	//Non THIP
        fc = 2;
        kebun = localStorage.getItem("pzoolwmvalue").split("*")[1];
        //}

        //PT
        estcode = "";
        estnewcode = localStorage.getItem("pzoolwmvalue").split("*")[1].replace("PT.", "").replace(" ", "");
        companycode = "";
    } else {
        //Estate
        fc = 4;
        kebun = localStorage.getItem("pzoolwmvalue").split("*")[0];

        //Estate
        estnewcode = localStorage.getItem("pzoolwmvalue").split("*")[2].split(" - ")[1];
        companycode = "";
    }

    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceAnalysis.asmx/GetParit',
        data: JSON.stringify({
            fc: fc,
            kebun: kebun,
            userid: $("#userid").val(),
            estcode: estcode,
            estnewcode: estnewcode,
            companycode: companycode
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (response) {
            if (response.d[0].includes("Error : ")) {
                $("#loader").hide();
                //Pesan("Informasi Error GetParit", response.d[0]);
            }
            else {
                //console.log(response.d);
                global_jsonparit = JSON.parse(response.d[0]);
                global_estatemappingnew = JSON.parse(response.d[1]);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#loader").hide();
            alert("Maaf terjadi kesalahan GetParit.");
        }
    });
}

function WebserviceGetParitBy() {
    let fc;
    let kebun;
    if (localStorage.getItem("pzoolwmvalue").split("*")[0] == "all") {
        //PT
        //if (localStorage.getItem("pzoolwmvalue").split("*")[1].includes("THIP")) {
        //	//THIP
        //	fc = 3;
        //	kebun = localStorage.getItem("pzoolwmvalue").split("*")[1].toString().replace("PT.", "");
        //}
        //else {
        //	//Non THIP
        fc = 2;
        kebun = localStorage.getItem("pzoolwmvalue").split("*")[1];
        //}
    }
    else {
        //Estate
        fc = 4;
        kebun = localStorage.getItem("pzoolwmvalue").split("*")[0];
    }

    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceAnalysis.asmx/GetParitBy',
        data: JSON.stringify({
            fc: fc,
            kebun: kebun
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (response) {
            if (response.d.includes("Error : ")) {
                $("#loader").hide();
                //Pesan("Informasi Error GetParitBy", response.d);
            }
            else {
                global_jsonparitlength = JSON.parse(response.d);
                //console.log(global_jsonparitlength);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#loader").hide();
            alert("Maaf terjadi kesalahan GetParitBy.");
        }
    });
}
