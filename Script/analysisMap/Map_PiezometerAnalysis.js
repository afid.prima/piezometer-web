﻿var ketinggianPiezo = [];
var ketinggianWlr = [];
var fiturDipilih = [];
var btnZoom = false;
var allZonaAnalysis = [];
var selectedZonaAnalysis = {};
function HandlerPiezo() {

    $("#selectFilterWMArea").change(function () {
        ListZonaByWmArea($("#selectFilterWMArea").val());
        GetKetinggianPiezometerByZona();
    });

    $("#selectFilterZona").change(function () {
        GetKetinggianPiezometerByZona();
    })

    $("#selectFilterAWL").change(function () {
        layerIOT.setStyle(styleFeatureIOT);
        if (!layerIOT.getVisible()) {
            layerIOT.setVisible(true);
        }
    })

    $("#selectFilterPzo").change(function () {
        drawselectedPiezo();
    })

    $("#btnZoom").click(function () {
        btnZoom = true;
        zoomZona();
    });

    $("#btnSave").click(function () {
        var obj = {
            id : $("#selectFilterZonaSelected").val(),
            userid: userid,
            companyCode: localStorage.getItem("pzoolwmvalue").split("*")[1],
            wmArea: $("#selectFilterWMArea").val(),
            zona: $("#selectFilterZona").val(), 
            pzo: $("#selectFilterPzo").val(),
            awl: $("#selectFilterAWL").val(), 
            namaZona: $("#namaZona").val()
        }

        if (obj["namaZona"] == "") {
            alert("Mohon Input zona");
            return
        } else if (obj["pzo"].length == 0) {
            alert("Mohon Pilih Piezo");
            return
        } else if (obj["pzo"].length > 4) {
            alert("Piezo tidak boleh lebih dari 4");
            return
        }

        saveZonaAnalysis(obj);
    });

    $("#btnDeleted").click(function () {
        if ($("#selectFilterZonaSelected").val() == 0) {
            alert("Mohon pilih zona analysis");
            return
        }

        var obj = {
            id: $("#selectFilterZonaSelected").val(),
            userid: userid,
            companyCode: localStorage.getItem("pzoolwmvalue").split("*")[1],
            wmArea: $("#selectFilterWMArea").val(),
            zona: $("#selectFilterZona").val(),
            pzo: $("#selectFilterPzo").val(),
            awl: $("#selectFilterAWL").val(),
            namaZona: $("#namaZona").val()
        }

        let confirmAction = confirm("Apakah anda akan hapus zona analysis " + obj["namaZona"]);
        if (confirmAction) {
            deleteZonaAnalysis(obj);
        }
    });

    $("#btnPdf").click(function () {
        var win = window.open('../Content/WLR_DataZonaChart.aspx?zonaAnalysisId=' + $("#selectFilterZonaSelected").val() + '&weekid=' + idweek, '_blank');

    });

    $('#selectFilterZonaSelected').change(function () {
        for (var i = 0; i < allZonaAnalysis.length; i++) {
            if (allZonaAnalysis[i]["zonaAnalysisId"] == $('#selectFilterZonaSelected').val()) {
                selectedZonaAnalysis = allZonaAnalysis[i];
                break;
            }
        }
        
        $("#selectFilterWMArea").val(selectedZonaAnalysis["wmArea"]);
        $("#selectFilterZona").val(selectedZonaAnalysis["zonaId"]);

        $('.selectpicker').selectpicker('refresh');

        GetKetinggianPiezometerByZona();

        $('.selectpicker').selectpicker('refresh');
    })


    layerRambuAirWLR = new ol.layer.Vector({
        name: 'RambuAirWLR',
        source: new ol.source.Vector(), 
        style: function (feature) {
            return styleFeatureRambuAir(feature)
        }
    });

    map.addLayer(layerRambuAirWLR);
    layerRambuAirWLR.setVisible(true);

    layerIOT = new ol.layer.Vector({
        name: 'IOT',
        source: new ol.source.Vector(),
        style: function (feature) {
            return styleFeatureIOT(feature)
        }
    });

    map.addLayer(layerIOT);
    layerIOT.setVisible(true);

    //Select interaction block
    global_selectblock = new ol.interaction.Select({
        style: styleKetinggianPiezometerByZona,
        layers: [global_layerblockzona],
        condition: ol.events.condition.click
    });
    global_selectblock.on('select', function (e) {
        console.info(e.selected[0].getProperties())
        addPieZona(e.selected[0].getProperties());
    });

    map.addInteraction(global_selectblock);
}

function setupPiezoAnalysis() {
    setTimeout(function () {
        console.log("Ini muncul setelah 5 detik.");
    }, 2000);

    GetKetinggianPiezometerByZona();
    getAllZonaAnalysis();
}

function addPieZona(obj) {
    var sfPiezo = $("#selectFilterPzo").val();
    var delPiezo = [];
    for (var i = 0; i < ketinggianPiezo.length; i++) {
        if (obj["Block"] == ketinggianPiezo[i]["Block"]) {
            var hapus = false;
            for (var j = 0; j < sfPiezo.length; j++) {
                if (sfPiezo[j] == ketinggianPiezo[i]["PieRecordID"]) {
                    delPiezo.push(ketinggianPiezo[i]["PieRecordID"]);
                    hapus = true;
                    break;
                }
            }

            if (!hapus) {
                sfPiezo.push(ketinggianPiezo[i]["PieRecordID"]);
            }
        }
    }

    var sOkPiezo = [];
    for (var i = 0; i < sfPiezo.length; i++) {
        var insert = true;
        for (var j = 0; j < delPiezo.length; j++) {
            if (sfPiezo[i] == delPiezo[j]) {
                insert = false;
                break;
            }
        }

        if (insert) {
            sOkPiezo.push(sfPiezo[i]);
        }
    }

    $("#selectFilterPzo").val(sOkPiezo);

    $('.selectpicker').selectpicker('refresh');
    drawselectedPiezo();
}

function drawselectedPiezo() {

    global_layerblockManual.setStyle(styleKetinggianPiezometerManual);

    if (!global_layerblockManual.getVisible()) {
        global_layerblockManual.setVisible(true);
    }
}

function styleKetinggianPiezometerByZona(feature, resolution) {
    let style = null;
    for (var i = 0; i < ketinggianPiezo.length; i++) {
        if (feature.get('ESTNR') == ketinggianPiezo[i]["estCode"]
            && feature.get('Block') == ketinggianPiezo[i]["Block"]
        ) {


            var fillColor = 'rgba(255, 255, 255, 0)';
            var strokeColor = '#D3D3D3'
            //var strokeColor = '#f1f736'
            if (ketinggianPiezo[i]["colorBack"] != null) {
                fillColor = hexToRgba(ketinggianPiezo[i]["colorBack"], 0.7)
            }

            style = new ol.style.Style({
                fill: new ol.style.Fill({
                    color: fillColor
                }),
                stroke: new ol.style.Stroke({
                    color: strokeColor,
                    width: 1
                }),
                text: new ol.style.Text({
                    font: '13px Calibri,sans-serif',
                    fill: new ol.style.Fill({
                        color: '#000'
                    }),
                    text: ketinggianPiezo[i]["PieRecordID"],
                    stroke: new ol.style.Stroke({
                        color: 'yellow',
                        width: 4
                    }),
                    placement: 'point'
                })
            });
        }
    }
    //style.getText().setText(feature.get('Block'));
    return style;
}

function styleKetinggianPiezometerManual(feature, resolution) {
    let style = null;
    var pzoS = $("#selectFilterPzo").val();
    for (var i = 0; i < ketinggianPiezo.length; i++) {
        if (feature.get('ESTNR') == ketinggianPiezo[i]["estCode"] && feature.get('Block') == ketinggianPiezo[i]["Block"]) {
            for (var j = 0; j < pzoS.length; j++) {
                if (ketinggianPiezo[i]["WMA_code"] == $("#selectFilterZona").val() && ketinggianPiezo[i]["PieRecordID"] == pzoS[j]) {

                    var fillColor = 'rgba(222, 213, 213, 0)';
                    var strokeColor = '#0a0a0a'

                    style = new ol.style.Style({
                        fill: new ol.style.Fill({
                            color: fillColor
                        }),
                        stroke: new ol.style.Stroke({
                            color: strokeColor,
                            width: 5
                        })
                    });
                }
            //    break;
            }
        }
    }
    //style.getText().setText(feature.get('Block'));
    return style;
}

function styleFeatureRambuAir(feature) {
    var obj = {};
    var nilai = "";
    var color = "#00FF00";
    var colorF = "#00FF00";
    var bufferRadius = 10;

    nilai = "";
    if (feature.getProperties()["nilai"] != null) {
        nilai = feature.getProperties()["nilai"];
    }

    var styleFeatures = [];

    if (feature.getProperties()["zonaId"] == $("#selectFilterZona").val()) {

        var styleTextPoin = new ol.style.Style({
            image: new ol.style.Circle({
                fill: new ol.style.Fill({
                    color: colorF,
                    width: 4
                }),
                stroke: new ol.style.Stroke({
                    color: color,
                    width: 4
                }),
                radius: 4
            }),
            text: new ol.style.Text({
                textAlign: 'center',
                textBaseline: 'middle',
                font: '11px Verdana',
                offsetX: 0,
                offsetY: 0,
                text: feature.getProperties()["stationName"] ,//+ '\n\n' + nilai,
                fill: new ol.style.Fill({
                    color: '#000000',
                }),
                stroke: new ol.style.Stroke({
                    color: '#ffffff',
                    width: 3
                }),
                placement: 'label',
                overflow: false
            })
        });
        styleFeatures.push(styleTextPoin);
    }
    return styleFeatures;
}

function styleFeatureIOT(feature) {
    var obj = {};
    var nilai = "";
    var color = "#2c22b2";
    var colorF = "#2c22b2";
    var bufferRadius = 10;

    var styleFeatures = [];

    if (feature.getProperties()["stationId"] == $("#selectFilterAWL").val()) {

        var styleTextPoin = new ol.style.Style({
            image: new ol.style.Circle({
                fill: new ol.style.Fill({
                    color: colorF,
                    width: 4
                }),
                stroke: new ol.style.Stroke({
                    color: color,
                    width: 4
                }),
                radius: 4
            }),
            text: new ol.style.Text({
                textAlign: 'center',
                textBaseline: 'middle',
                font: '11px Verdana',
                offsetX: 0,
                offsetY: 0,
                text: feature.getProperties()["stationName"],
                fill: new ol.style.Fill({
                    color: '#000000',
                }),
                stroke: new ol.style.Stroke({
                    color: '#ffffff',
                    width: 3
                }),
                placement: 'label',
                overflow: false
            })
        });
        styleFeatures.push(styleTextPoin);
       
    }

    return styleFeatures;
}

function hexToRgba(hex, alpha) {
    hex = hex.replace('#', ''); // Menghapus tanda # jika ada
    var r = parseInt(hex.substring(0, 2), 16);
    var g = parseInt(hex.substring(2, 4), 16);
    var b = parseInt(hex.substring(4, 6), 16);

    return 'rgba(' + r + ',' + g + ',' + b + ',' + alpha + ')';
}

function hitungJangkauanFiturDipilih(fiturDipilih) {
    var jangkauan = ol.extent.createEmpty();
    fiturDipilih.forEach(function (fitur) {
        ol.extent.extend(jangkauan, fitur.getGeometry().getExtent());
    });
    return jangkauan;
}

function zoomZona() {
    if (fiturDipilih.length > 0) {
        var jangkauanFiturDipilih = hitungJangkauanFiturDipilih(fiturDipilih);
        map.getView().fit(jangkauanFiturDipilih, { padding: [100, 100, 100, 100] }); // Sesuaikan padding sesuai kebutuhan
    }

    if (btnZoom) {
        var allFeature = global_layerblockzona.getSource().getFeatures(); // Dapatkan fitur yang dipilih
        fiturDipilih = [];

        for (var i = 0; i < ketinggianPiezo.length; i++) {
            if (ketinggianPiezo[i]["WMA_code"] == $("#selectFilterZona").val()) {
                for (var j = 0; j < allFeature.length; j++) {
                    if (allFeature[j].getProperties()["Block"] == ketinggianPiezo[i]["Block"]) {
                        fiturDipilih.push(allFeature[j]);
                    }
                }
            }
        }
        btnZoom = false;
        zoomZona()
    }
}

function setupDropDownAnalysisiZona() {
    selectedZonaAnalysis = {};

    $('#selectFilterZonaSelected').find('option').remove();
    $("#selectFilterZonaSelected").append($('<option>', {
        value: 0,
        text: "Pilih Zona Analysis"
    }));

    for (var i = 0; i < allZonaAnalysis.length; i++) {
        if (allZonaAnalysis[i]["selected"] == 1) {
            selectedZonaAnalysis = allZonaAnalysis[i];
        }
        $("#selectFilterZonaSelected").append($('<option>', {
            value: allZonaAnalysis[i]["zonaAnalysisId"],
            text: allZonaAnalysis[i]["zonaAnalysisName"]
        }));
    }

    if (selectedZonaAnalysis["zonaAnalysisId"] != null) {
        $("#selectFilterZonaSelected").val(selectedZonaAnalysis["zonaAnalysisId"]);
    } else {
        $("#selectFilterZonaSelected").val(0);
    }

    $('.selectpicker').selectpicker('refresh');
}

function GetKetinggianPiezometerByZona() {
    $(".loading").show();
    ketinggianPiezo = [];
    ketinggianWlr = [];
    ketinggianAwl = [];
    fiturDipilih = [];
    global_layerblockzona.setVisible(false);
    global_layerblockManual.setVisible(false);
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceAnalysis.asmx/GetKetinggianPiezometerByZona',
        data: '{companyCode: "' + localStorage.getItem("pzoolwmvalue").split("*")[1] + '",zona: "' + $("#selectFilterZona").val() + '",week: ' + week + ',month: ' + month + ',tahun: ' + year +'}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (response) {
            ketinggianPiezo = JSON.parse(response.d[0]);
            ketinggianWlr = JSON.parse(response.d[1]);
            ketinggianAwl = JSON.parse(response.d[2]);

            let vectorSource = global_layerblockzona.getSource();

            global_layerblockzona.setStyle(styleKetinggianPiezometerByZona);

            //var allFeature = global_layerblockzona.getSource().getFeatures(); // Dapatkan fitur yang dipilih
            //fiturDipilih = [];

            //for (var i = 0; i < ketinggianPiezo.length; i++) {
            //    if (ketinggianPiezo[i]["WMA_code"] == $("#selectFilterZona").val()) {
            //        for (var j = 0; j < allFeature.length; j++) {
            //            if (allFeature[j].getProperties()["Block"] == ketinggianPiezo[i]["Block"]) {
            //                fiturDipilih.push(allFeature[j]);
            //            }
            //        }
            //    }
            //}

            //zoomZona();

            if (!global_layerblockzona.getVisible()) {
                global_layerblockzona.setVisible(true);
            }

            //$('#selectFilterPzo').find('option').remove();
            //for (var i = 0; i < ketinggianPiezo.length; i++) {
            //    $("#selectFilterPzo").append($('<option>', {
            //        value: ketinggianPiezo[i]["PieRecordID"],
            //        text: ketinggianPiezo[i]["PieRecordID"]
            //    }));
            //}

            //var vectorSourceX = layerRambuAirWLR.getSource();
            //vectorSourceX.clear();

            //var vectorSourceXX = layerIOT.getSource();
            //vectorSourceXX.clear();

            //markerRambuAirWLR = new ol.source.Vector();
            //markerIOT = new ol.source.Vector();

            //for (var i = 0; i < ketinggianWlr.length; i++) {
            //    var geom_point = new ol.geom.Point([ketinggianWlr[i]['longitude'], ketinggianWlr[i]['latitude']]);
            //    geom_point.transform('EPSG:4326', 'EPSG:3857');

            //    var point_feature = new ol.Feature(geom_point);

            //    point_feature.setId(ketinggianWlr[i]['stationId']);
            //    point_feature.setProperties(ketinggianWlr[i]);
            //    if (ketinggianWlr[i]['row'] == "0") {
            //        markerIOT.addFeature(point_feature);
            //    } else {
            //        markerRambuAirWLR.addFeature(point_feature);
            //    }
            //}

            //layerRambuAirWLR.setSource(markerRambuAirWLR);
            //layerIOT.setSource(markerIOT);
            
            //$('#selectFilterAWL').find('option').remove();
            //$("#selectFilterAWL").append($('<option>', {
            //    value: "",
            //    text: "Pilih AWL"
            //}));

            //var device_id = "";
            //for (var i = 0; i < ketinggianAwl.length; i++) {
            //    if (ketinggianAwl[i]["zona"] == $("#selectFilterZona").val()) {
            //        device_id = ketinggianAwl[i]["device_id"]
            //    }

            //    $("#selectFilterAWL").append($('<option>', {
            //        value: ketinggianAwl[i]["device_id"],
            //        text: ketinggianAwl[i]["alias_name"]
            //    }));
            //}

            //$("#selectFilterAWL").val(device_id);


            //if ($("#selectFilterZonaSelected").val() != 0 && $("#selectFilterZonaSelected").val() != null) {
            //    $("#selectFilterPzo").val(selectedZonaAnalysis["pieRecordID"].split(','));
            //    $("#selectFilterAWL").val(selectedZonaAnalysis["awlDeviceId"]);
            //    $("#namaZona").val(selectedZonaAnalysis["zonaAnalysisName"]);

            //    drawselectedPiezo();
            //} else {
            //    $("#namaZona").val("");
            //}

            $('.selectpicker').selectpicker('refresh');

            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $(".loading").hide();
            alert("Maaf terjadi kesalahan GetKetinggianPiezometerByZona.");
        }
    });
}

function saveZonaAnalysis(objSave) {
    allZonaAnalysis = [];
    $(".loading").show();

    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceAnalysis.asmx/SaveZonaAnalysis',
        data: JSON.stringify({ obj: JSON.stringify(objSave) }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            allZonaAnalysis = JSON.parse(response.d);
            setupDropDownAnalysisiZona();

            $(".loading").hide();
            alert("Berhasil Disimpan");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function saveZonaAnalysis : " + xhr.statusText);


            $('.selectpicker').selectpicker('refresh');
            $(".loading").hide();
        }
    });
}

function deleteZonaAnalysis(objSave) {
    selectedZonaAnalysis = {};
    $(".loading").show();

    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceAnalysis.asmx/DeleteZonaAnalysis',
        data: JSON.stringify({ obj: JSON.stringify(objSave) }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            allZonaAnalysis = JSON.parse(response.d);
            setupDropDownAnalysisiZona();
            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function deleteZonaAnalysis : " + xhr.statusText);

            $(".loading").hide();
        }
    });
}

function getAllZonaAnalysis() {
    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceAnalysis.asmx/GetAllZonaAnalysis',
        data: '{companyCode: "' + localStorage.getItem("pzoolwmvalue").split("*")[1] + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            allZonaAnalysis = JSON.parse(response.d);
            setupDropDownAnalysisiZona();
            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getAllZonaAnalysis : " + xhr.statusText);

            $(".loading").hide();
        }
    });
}