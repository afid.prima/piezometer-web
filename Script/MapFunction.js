﻿//Variable Global

// Tombol Manipulation
//Onclick
function ManipulationComponent() {
    $("#toolsfullscreen").click(function () {
        if (globalisfullscreen) exitfullscreen(); else {
            fullScreen();
        }
    })

    $("#toolsoverview").click(function () {
        if ($("#dialogoverview").dialog("isOpen"))
            $("#dialogoverview").dialog("close");
        else
            $("#dialogoverview").dialog("open");
    })

    $("#toolsdrawing").click(function () {
        if ($("#dialogdrawing").dialog("isOpen"))
            $("#dialogdrawing").dialog("close");
        else
            $("#dialogdrawing").dialog("open");
    })

    $("#toolsmeasurement").click(function () {
        if ($("#dialogmeasurement").dialog("isOpen"))
            $("#dialogmeasurement").dialog("close");
        else
            $("#dialogmeasurement").dialog("open");
    })

    $("#toolslegend").click(function () {
        if ($("#dialoglegend").dialog("isOpen"))
            $("#dialoglegend").dialog("close");
        else
            $("#dialoglegend").dialog("open");
    })

    $("#toolsreport").click(function () {
        $("#divShowReport").html("");
        if ($("#dialogreport").dialog("isOpen"))
            $("#dialogreport").dialog("close");
        else
            $("#dialogreport").dialog("open");
    })

    $("#toolsprevdate").click(function () {
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/GetPreviousWeekFromParameter',
            data: '{Week: "' + globalweek + '", Month: "' + globalmonth + '", Year: "' + globalyear + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var json = $.parseJSON(response.d);
                if (json.length > 0) {
                    var param = "W" + json[0]["Week"] + ("0" + json[0]["Month"]).slice(-2) + json[0]["Year"];
                    window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + globalcompany + "&Estate=" + globalestate + "&IDWEEK=" + param;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.statusText);
            }
        });
        //if (parseInt(globalidweek) - 1 > 0)
        //    window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + globalcompany + "&Estate=" + globalestate + "&IDWEEK=" + (parseInt(globalidweek) - 1);
    })

    $("#toolsnextdate").click(function () {
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/GetNextWeekFromParameter',
            data: '{Week: "' + globalweek + '", Month: "' + globalmonth + '", Year: "' + globalyear + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var json = $.parseJSON(response.d);
                if (json.length > 0) {
                    var param = "W" + json[0]["Week"] + ("0" + json[0]["Month"]).slice(-2) + json[0]["Year"];
                    window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + globalcompany + "&Estate=" + globalestate + "&IDWEEK=" + param;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.statusText);
            }
        });
        //window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + globalcompany + "&Estate=" + globalestate + "&IDWEEK=" + (parseInt(globalidweek) + 1);
    })

    $("#toolshistory").click(function () {
        if ($("#dialogloginhistory").dialog("isOpen"))
            $("#dialogloginhistory").dialog("close");
        else {
            $("#dialogloginhistory").dialog("open");
        }
    })

    $("#btnfilterhistory").click(function () {
        //Webserviceloginhistory();
    })

    $("#toolschangepassword").click(function () {
        if ($("#dialogchangepassword").dialog("isOpen"))
            $("#dialogchangepassword").dialog("close");
        else {
            $("#usernamechange").val(globalusername);
            $("#dialogchangepassword").dialog("open");
        }
    })

    $("#toolsprojectdetail").click(function () {
        if ($("#dialogprojectdetail").dialog("isOpen"))
            $("#dialogprojectdetail").dialog("close");
        else
            $("#dialogprojectdetail").dialog("open");
    })

    $("#selectFilterCompany").change(function () {
        ListEstate($("#selectFilterCompany").val());
    })

    $("#selectFilterYear").change(function () {
        GetListMonth($("#selectFilterYear").val());
    })

    $("#selectFilterMonth").change(function () {
        GetListWeek($("#selectFilterYear").val(), $("#selectFilterMonth").val());
    })

    $("#btnrefresh").click(function () {
        //window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + $("#selectFilterCompany").val() + "&Estate=" + $("#selectFilterEstate").val() + "&IDWEEK=" + $("#selectFilterWeek").val();
        var param = "W" + $("#selectFilterWeek").val() + ("0" + $("#selectFilterMonth").val()).slice(-2) + $("#selectFilterYear").val();
        window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + $("#selectFilterCompany").val() + "&Estate=" + $("#selectFilterEstate").val() + "&IDWEEK=" + param;
    })

    $("#btncancelchange").click(function () {
        $("#dialogchangepassword").dialog("close");
    })

    $("#btnpasswordchange").click(function () {
        var username = globalusername;
        var password = $('#newpassword').val();
        var oldpassword = $('#oldpassword').val();
        var confirmnewpassword = $('#conformnewpassword').val();
        if (password == '' || oldpassword == '' || confirmnewpassword == '') {
            alert("Please insert all data");
        }
        else {
            if (password != confirmnewpassword) {
                alert("New password and confirm new password did not match");
            }
            else {
                ChangePassword(globalusername, oldpassword, password);
            }
        }
    })

    $("#toolsdrag").click(function () {
        ClearGraphic();
        clearMeasure();
        // Deactive Drawing
        for (key in drawControls) {
            var control = drawControls[key];
            control.deactivate();
        }

        for (key in measureControls) {
            var control = measureControls[key];
            control.deactivate();
        }
    })

    $("#toolsextent").click(function () {
        ZoomtoExtent(globalextent.split(",")[0], globalextent.split(",")[1], globalextent.split(",")[2], globalextent.split(",")[3]);
    })

    $("#sidebarlayer").click(function () {
        if ($("#layercontent").dialog("isOpen"))
            $("#layercontent").dialog("close");
        else {
            $("#layercontent").dialog("open");
            $("#filtercontent").dialog("close");
        }
    })

    $("#sidebarfilter").click(function () {
        if ($("#filtercontent").dialog("isOpen"))
            $("#filtercontent").dialog("close");
        else {
            $("#filtercontent").dialog("open");
            $("#layercontent").dialog("close");
        }
    })

    $("#selectFilterKebunForGoTo").change(function () {
        if ($("#selectFilterKebunForGoTo").val() == "") {
            WebServiceListBlock();
        }
        else {
            ListBlockByEstateForGoTo(globalprojectid, $("#selectFilterKebunForGoTo").val(), "");
        }
    })

    $("#buttonzoomto").click(function () {
        GetBlockExtent($("#selectFilterEstate").val(), $("#selectblock").val());
    })

    //Drawing
    $("#drawpoint").click(function () {
        // Deactive Measurement
        for (key in measureControls) {
            var control = measureControls[key];
            control.deactivate();
        }

        for (key in drawControls) {
            var control = drawControls[key];
            if ("point" == key) {
                control.activate();
                // ModeDrawMeasure = true;
            } else {
                control.deactivate();
            }
        }
    });

    $("#drawpolyline").click(function () {
        // Deactive Measurement
        for (key in measureControls) {
            var control = measureControls[key];
            control.deactivate();
        }

        for (key in drawControls) {
            var control = drawControls[key];
            if ("line" == key) {
                control.activate();
                //  ModeDrawMeasure = true;
            } else {
                control.deactivate();
            }
        }
    });

    $("#drawpolygon").click(function () {
        //Deactive Measurement
        for (key in measureControls) {
            var control = measureControls[key];
            control.deactivate();
        }

        for (key in drawControls) {
            var control = drawControls[key];
            if ("polygon" == key) {
                control.activate();
                // ModeDrawMeasure = true;
            } else {
                control.deactivate();
            }
        }
    });

    $("#drawrectangle").click(function () {
        // Deactive Measurement
        for (key in measureControls) {
            var control = measureControls[key];
            control.deactivate();
        }

        for (key in drawControls) {
            var control = drawControls[key];
            if ("box" == key) {
                control.activate();
                // ModeDrawMeasure = true;
            } else {
                control.deactivate();
            }
        }
    });

    $("#Mline").click(function () {
        // Deactive Drawing
        for (key in drawControls) {
            var control = drawControls[key];
            control.deactivate();
        }

        for (key in measureControls) {

            var control = measureControls[key];
            if ("line" == key) {
                control.activate();
                control.setImmediate(true);
                // ModeDrawMeasure = true;
            } else {
                control.deactivate();
            }
        }
    });

    $("#MPolygon").click(function () {
        // Deactive Drawing
        for (key in drawControls) {
            var control = drawControls[key];
            control.deactivate();
        }

        for (key in measureControls) {

            var control = measureControls[key];
            if ("polygon" == key) {
                control.activate();
                control.setImmediate(true);
                //ModeDrawMeasure = true;
            } else {
                control.deactivate();
            }
        }
    });

    $("#selectlistLayer").change(function () {

        try {
            for (var i = 0; i < globaljmlvector; i++) {
                try {
                    globalinfo[i].deactivate();
                }
                catch (err) {
                    break;
                }
            }

            if (globaljmllayer[$("#selectlistLayer")[0].selectedIndex].toString().indexOf(",") > -1) {
                for (var i = 0; i < globaljmllayer[$("#selectlistLayer")[0].selectedIndex].split(',').length; i++) {
                    globalinfo[globaljmllayer[$("#selectlistLayer")[0].selectedIndex].split(',')[i]].activate();
                }
            }
            else {
                globalinfo[globaljmllayer[$("#selectlistLayer")[0].selectedIndex]].activate();
            }

            $("#dialoginfo").dialog("close");
        }
        catch (err) {
            alert("error : " + err.message);
        }
    });

    $("#btnwmscancel").click(function () {
        $("#dialogwms").dialog("close");
    });

    $("#btnwmsok").click(function () {
        var r = confirm("Are you sure change wms url and wms name?");
        if (r == true) {
            WebServiceSetWMS();
        }
    });
}



// Function - Function
//Listener Window
document.addEventListener("mozfullscreenchange", function () {
    resizescreen();
}, false);

document.addEventListener("webkitfullscreenchange", function () {
    resizescreen();
}, false);

function ZoomToPiezometer(long, lat) {
    map.setCenter(new OpenLayers.LonLat(long, lat).transform(
        	new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject()), 20);
    map.zoomToScale(5000);
}

function fullScreen() {
    globalisfullscreen = true;

    var docElm = document.documentElement;
    if (docElm.requestFullscreen) {
        docElm.requestFullscreen();
    }
    else if (docElm.mozRequestFullScreen) {
        docElm.mozRequestFullScreen();
    }
    else if (docElm.webkitRequestFullScreen) {
        docElm.webkitRequestFullScreen();
    }
}

function exitfullscreen() {
    globalisfullscreen = false;

    if (document.exitFullscreen) {
        document.exitFullscreen();
    }
    else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    }
    else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
    }
}

function resizescreen() {
    $("#map").css({ 'height': (($(window).height()) - 40) + 'px' });
    map.updateSize();
}

function SetListLayer() {

    for (var i = 0; i < globallayerDisplay.length; i++) {
        $("#slider" + i).slider({
            value: 100,
            slide: function (e, ui) {
                globallayerfitur[this.id.substring(6, 8)].setOpacity(ui.value / 100);
            }
        });

        $("#chk" + i).change(function () {
            if ($(this).is(":checked")) {
                globallayerfitur[this.id.substring(3, 5)].setVisibility(true);
            }
            else {
                globallayerfitur[this.id.substring(3, 5)].setVisibility(false);
            }
        });
    }

    //Set active layer
    if (globaltreeindex > 0) $("#tablelayer").css({ left: 27, position: 'relative' });
    else $("#tablelayer").css({ left: 2, position: 'relative' });

    globaltreeawal = globaltreeawal.split(",");
    for (var i = 0; i < globaltreeindex; i++) {
        if (globaltreeawal[i] == "off") {
            $(".classTree" + (i + 1) + "").hide();
        }

        $("#imgTree" + (i + 1) + "").click(function () {
            if ($(".classTree" + this.id.substring(7, 10) + "").is(":visible")) {
                $(".classTree" + this.id.substring(7, 10) + "").hide();
                $("#imgTree" + this.id.substring(7, 10) + "").attr("src", "../Images/tree/plus.png");
            }
            else {
                $(".classTree" + this.id.substring(7, 10) + "").show();
                $("#imgTree" + this.id.substring(7, 10) + "").attr("src", "../Images/tree/minus.png");
            }
        });

        $("#chktree" + (i + 1) + "").change(function () {

            if ($("#chktree" + this.id.substring(7, 10) + "").is(':checked')) {

                $(".classTreechk" + this.id.substring(7, 10) + "").prop('checked', true);

                for (var i = 0; i < globallayerDisplayName.length; i++) {
                    if ($("#chk" + i + "").is(".classTreechk" + this.id.substring(7, 10) + "")) {
                        globallayerfitur[i].setVisibility(true);
                    }
                }
            }
            else {
                $(".classTreechk" + this.id.substring(7, 10) + "").prop('checked', false);

                for (var i = 0; i < globallayerDisplayName.length; i++) {
                    if ($("#chk" + i + "").is(".classTreechk" + this.id.substring(7, 10) + "")) {
                        globallayerfitur[i].setVisibility(false);
                    }
                }
            }

        });

    }
}

function ZoomtoPoint(long, lat) {
    map.setCenter(new OpenLayers.LonLat(long, lat).transform(
        	new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject()), 20);
}

function DownloadFile(fileURL, fileName) {
    fileURL = fileURL.replace(/:/g, "_")
    $.ajax({
        url: fileURL,
        type: 'HEAD',
        error: function () {
            alert("File not found!!");
        },
        success: function () {
            if (!window.ActiveXObject) {
                var save = document.createElement('a');
                save.href = fileURL;
                save.target = '_blank';
                save.download = fileName || 'unknown';

                var evt = new MouseEvent('click', {
                    'view': window,
                    'bubbles': true,
                    'cancelable': false
                });
                save.dispatchEvent(evt);

                (window.URL || window.webkitURL).revokeObjectURL(save.href);
            }

            // for IE < 11
            else if (!!window.ActiveXObject && document.execCommand) {
                var _window = window.open(fileURL, '_blank');
                _window.document.close();
                _window.document.execCommand('SaveAs', true, fileName || fileURL)
                _window.close();
            }
        }
    });

}




// Function Map

function ZoomtoExtent(xmin, ymin, xmax, ymax) {
    map.zoomToExtent(new OpenLayers.Bounds(xmin, ymin, xmax, ymax));
}

//Drawing
function AllowPan(element) {

    var stop = !element.checked;
    for (var key in drawControls) {
        drawControls[key].handler.stopDown = stop;
        drawControls[key].handler.stopUp = stop;
    }

}

//Clear Graphic
function ClearGraphic() {
    pointLayer.removeFeatures(pointLayer.features);
    lineLayer.removeFeatures(lineLayer.features);
    polygonLayer.removeFeatures(polygonLayer.features);
    boxLayer.removeFeatures(boxLayer.features);
}

//Measurement
function clearMeasure() {
    var element = document.getElementById('output');
    element.innerHTML = "";
}

function handleMeasurements(event) {
    var geometry = event.geometry;
    var units = event.units;
    var order = event.order;
    var measure = event.measure;
    var element = document.getElementById('output');
    var out = "";
    var satuan = "Ha";
    var ukuran = "0";

    if (units == "km") ukuran = (parseFloat(measure.toFixed(3)) * 100).toFixed(2);
    else
        ukuran = (parseFloat(measure.toFixed(3)) / 10000).toFixed(4);

    if (order == 1) {
        out += "Measure: " + measure.toFixed(3) + " " + units;
    } else {
        out += "Measure: " + ukuran + " " + satuan;
    }
    element.innerHTML = out;
}

function toggleGeodesic(element) {
    for (key in measureControls) {
        var control = measureControls[key];
        control.geodesic = element.checked;
    }
}

function toggleImmediate(element) {
    for (key in measureControls) {
        var control = measureControls[key];
        control.setImmediate(element.checked);
    }
}

function ZoomtoCompartment(minx, miny, maxx, maxy) {

    var bounds = new OpenLayers.Bounds();
    var latlonmin = new Array(2);
    var latlonmax = new Array(2);
    var xmin, ymin, xmax, ymax, zone, southhemi;
    zone = parseFloat(globalrsid.substring(0, globalrsid.length - 1));

    if (globalrsid.slice(-1) == "s" || globalrsid.slice(-1) == "S")
        southhemi = true;
    else
        southhemi = false;

    xmin = parseFloat(minx);
    ymin = parseFloat(miny);
    xmax = parseFloat(maxx);
    ymax = parseFloat(maxy);

    UTMXYToLatLon(xmin, ymin, zone, southhemi, latlonmin);
    UTMXYToLatLon(xmax, ymax, zone, southhemi, latlonmax);
    UTMXYToLatLon(xmin, ymin, zone, southhemi, latlonmin);
    UTMXYToLatLon(xmax, ymax, zone, southhemi, latlonmax);

    map.zoomToExtent(new OpenLayers.Bounds(RadToDeg(latlonmin[1]), RadToDeg(latlonmin[0]), RadToDeg(latlonmax[1]), RadToDeg(latlonmax[0])));
    map.updateSize();
    $("#dialoglaporankerja").dialogExtend("minimize");
}


function SetWMS(layerid, url, name) {
    url = url.replace(/%20/g, " ");
    name = name.replace(/%20/g, " ");
    $("#textwmsurl").val(url);
    $("#textwmsname").val(name);
    globallayerid = layerid;
    $("#dialogwms").dialog("open");
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}



