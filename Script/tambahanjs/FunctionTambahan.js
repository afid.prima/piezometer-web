﻿function setUpChart(data, lamaData) {
	console.info(data);
	var dataPoints = [];
	var dataPoints_CH = [];
	var count = 1;
	var objAxisX = {
		reversed: true,
		title: "Minggu"
	};
	var d = new Date(data[0]["Date"]);
    if (lamaData == 66) {
        d.setDate(d.getDate() - 42)
        //objAxisX = {
        //    reversed: true,
        //    interval: 2,
        //    title: "Minggu"
        //};
    } else if (lamaData == 3) {
		d.setMonth(d.getMonth() - 3);
		objAxisX = {
			reversed: true,
			interval: 2,
			title: "Minggu"
		};
	} else if (lamaData == 6) {
		d.setMonth(d.getMonth() - 6);
		objAxisX = {
			reversed: true,
			interval: 3,
			title: "Minggu"
		};
	} else if (lamaData == 12) {
		d.setMonth(d.getMonth() - 12);
	}
	console.info(d);

	for (var i = 0; i < data.length; i++) {
		if (lamaData == 0) {
			dataPoints.push({
				x: count,
				y: data[i]["Ketinggian"],
				label: data[i]["Week"]
			});
			dataPoints_CH.push({
				x: count,
				y: data[i]["TotalCH"],
				label: data[i]["Week"]
			});
			count++;
		} else {
			if (d.getTime() <= data[i]["Date"]) {
				dataPoints.push({
					x: count,
					y: data[i]["Ketinggian"],
					label: data[i]["Week"]
				});
				dataPoints_CH.push({
					x: count,
					y: data[i]["TotalCH"],
					label: data[i]["Week"]
				});
				count++;
			}
		}
	}
	console.info(dataPoints);
	var chart = new CanvasJS.Chart("divcharthistory", {
		zoomEnabled: true,
		axisXType: "secondary",
		animationEnabled: true,
		axisY: {
			reversed: true,
			title: "Kedalaman Air",
			stripLines: [
                {
                	opacity: .3,
                	color: "#00BFFF",
                	startValue: -10,
                	endValue: 1
                },
                {
                	opacity: .3,
                	color: "#2f74b7",
                	startValue: 1,
                	endValue: 40
                },
                {
                	opacity: .3,
                	color: "#01b0f3",
                	startValue: 40,
                	endValue: 45
                },
                {
                	opacity: .3,
                	color: "#00b04e",
                	startValue: 45,
                	endValue: 60
                },
                {
                	opacity: .3,
                	color: "#fcfe07",
                	startValue: 60,
                	endValue: 65
                },
                {
                	opacity: .3,
                	color: "#fe0000",
                	startValue: 65,
                	endValue: 150
                },
                {
                	opacity: .3,
                	color: "#888888",
                	startValue: 150,
                	endValue: 999
                }
			]
		},
		axisY2: {
			title: "Curah Hujan (mm)"
		},
		axisX: objAxisX,
		data: [
        {
        	type: "column",
        	axisYType: "secondary",
        	name: "Curah Hujan",
        	dataPoints: dataPoints_CH
        },
        {
        	yValueFormatString: "### cm",
        	type: "line",
        	markerSize: 8,
        	markerType: "circle",
        	dataPoints: dataPoints
        }]

	});


	chart.render();

}


function downloadCSV(fileTitle) {
	var headers = {
		est: "ESTNR", // remove commas to avoid errors
		block: "Block",
		piezoid: "PieRecor",
		banyak: "Banyak"
	};

	itemsFormatted = [];

	globalGeoJsonQuery['features'].forEach(function writePoint(features) {
		itemsFormatted.push({
			est: features.properties.ESTNR,
			block: features.properties.Block,
			piezoid: features.properties.PieRecordI,
			banyak: features.properties.Banyak
		});
	});

	exportCSVFile(headers, itemsFormatted, fileTitle); // call the exportCSVFile() function to process the JSON and trigger the download
}

function convertToCSV(objArray) {
	var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
	var str = '';

	for (var i = 0; i < array.length; i++) {
		var line = '';
		for (var index in array[i]) {
			if (line != '') line += ','

			line += array[i][index];
		}

		str += line + '\r\n';
	}

	return str;
}

function exportCSVFile(headers, items, fileTitle) {
	if (headers) {
		items.unshift(headers);
	}

	// Convert Object to JSON
	var jsonObject = JSON.stringify(items);

	var csv = this.convertToCSV(jsonObject);

	var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

	var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
	if (navigator.msSaveBlob) { // IE 10+
		navigator.msSaveBlob(blob, exportedFilenmae);
	} else {
		var link = document.createElement("a");
		if (link.download !== undefined) { // feature detection
			// Browsers that support HTML5 download attribute
			var url = URL.createObjectURL(blob);
			link.setAttribute("href", url);
			link.setAttribute("download", exportedFilenmae);
			link.style.visibility = 'hidden';
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
		}
	}
}
function setUpChartV2(data, indexDiv, block, arrDiv, flagClick, blockName) {

	var content = ""	
	//console.log('block steup chart v2 = ' + block)
	//console.log('arrdiv setup chart v2 = ' + arrDiv)
	//console.log('data length setup chart v2 = ' + data.length)  
	if (flagClick == 1) {
		content = arrDiv[indexDiv];
		var dataPoints = [];
		var dataPoints_CH = [];
		var count = 1;
		var dataKetinggian = [];
		var arrEstName = [];

		var objAxisX = {
			reversed: false,
		};
		var d = new Date(data[0]["Date"]);

		for (var a = 0; a < data.length; a++) { // ini yang buat 4 chart itu ya?
			if (data[a]["PieRecordID"] == block[indexDiv]) {
				dataKetinggian.push(data[a]["Ketinggian"]);
				dataPoints.push({
					x: count,
					y: data[a]["Ketinggian"],
					label: data[a]["WeekName"]
				});
				dataPoints_CH.push({
					x: count,
					y: data[a]["TotalCH"],
					label: data[a]["WeekName"]
				});
				count++;
				if (d.getTime() <= data[a]["Date"]) {
					dataPoints.push({
						x: count,
						y: data[a]["Ketinggian"],
						label: data[a]["WeekName"]
					});
					dataPoints_CH.push({
						x: count,
						y: data[a]["TotalCH"],
						label: data[a]["WeekName"]
					});
					count++;
				}
				var Name = data[a]["estName"] + ' (' + blockName[indexDiv] + ')';
				//console.log('name = ' + Name);
			}
		}
		//console.info(dataPoints);	
		$("#divChartCollection").append(content);
		var chart = new CanvasJS.Chart("tempGraphDiv" + indexDiv % 4, {
			title: {
				text: Name,
				//text: blockName[indexDiv],
				fontWeight: "bolder",
				fontFamily: "Calibri",
				fontSize: 15,
				padding: 1
			},
			zoomEnabled: true,
			axisXType: "secondary",
			animationEnabled: true,
			axisY: {
				minimum: Math.min.apply(null, dataKetinggian) - 10,
				maximum: Math.max.apply(null, dataKetinggian) + 10,
				reversed: true,
				title: "Kedalaman Air",
				stripLines: [
		            {
		            	opacity: .3,
		            	color: "#00BFFF",
		            	startValue: -10,
		            	endValue: 1
		            },
		            {
		            	opacity: .3,
		            	color: "#2f74b7",
		            	startValue: 1,
		            	endValue: 30
		            },
		            {
		            	opacity: .3,
		            	color: "#01b0f3",
		            	startValue: 30,
		            	endValue: 40
		            },
		            {
		            	opacity: .3,
		            	color: "#00b04e",
		            	startValue: 40,
		            	endValue: 50
		            },
		            {
		            	opacity: .3,
		            	color: "#fcfe07",
		            	startValue: 50,
		            	endValue: 60
		            },
		            {
		            	opacity: .3,
		            	color: "#fe0000",
		            	startValue: 60,
		            	endValue: 150
		            },
		            {
		            	opacity: .3,
		            	color: "#888888",
		            	startValue: 150,
		            	endValue: 999
		            }
				]
			},
            axisX: objAxisX,
            height: 320,
			data: [
		    //{
		    //	type: "column",
		    //	axisYType: "secondary",
		    //	name: "Curah Hujan",
		    //	dataPoints: dataPoints_CH
		    //},
		    {
		    	yValueFormatString: "### cm",
		    	type: "line",
		    	markerSize: 8,
		    	markerType: "circle",
		    	dataPoints: dataPoints,
		    	lineColor: "red",
		    	markerColor: "red",
		    }]

		});
		//console.log('data ketinggian = ' + dataKetinggian);
		chart.render();
		$(".loading").hide();
    }
    else {
        //alert("indexdiv " + indexDiv);
        //alert("Block "+block.length);
        for (var i = 0; i < indexDiv; i++) {
           // alert(block[i]);
			content = arrDiv[i];
			//console.log('content index ke ' + i + ' = ' + content);
			//console.log('block index ke ' + i + ' = ' + block[i]);
			//if (block != undefined) {
				//alert('tidak undefined');
			//}
			var dataPoints = [];
			var dataPoints_CH = [];
			var count = 1;
			var dataKetinggian = [];

			var objAxisX = {
				reversed: false,
			};
			var d = new Date(data[0]["Date"]);

			for (var a = 0; a < data.length; a++) {
				if (data[a]["PieRecordID"] == block[i]) {
					dataKetinggian.push(data[a]["Ketinggian"]);
					dataPoints.push({
						x: count,
						y: data[a]["Ketinggian"],
						label: data[a]["WeekName"]
					});
					dataPoints_CH.push({
						x: count,
						y: data[a]["TotalCH"],
						label: data[a]["WeekName"]
					});
					count++;
					if (d.getTime() <= data[a]["Date"]) {
						dataPoints.push({
							x: count,
							y: data[a]["Ketinggian"],
							label: data[a]["WeekName"]
						});
						dataPoints_CH.push({
							x: count,
							y: data[a]["TotalCH"],
							label: data[a]["WeekName"]
						});
						count++;
					}
				}
				var Name = data[a]["estName"] + ' (' + blockName[i] + ')';
			}
			//console.info(dataPoints);	
			$("#divChartCollection").append(content);
			var chart = new CanvasJS.Chart("tempGraphDiv" + i % 4, {
				title: {
					text: Name,
					//text: blockName[i],
					fontWeight: "bolder",
					fontFamily: "Calibri",
					fontSize: 15,
					padding: 1
				},
				zoomEnabled: true,
				axisXType: "secondary",
				animationEnabled: true,
				axisY: {
					minimum: Math.min.apply(null, dataKetinggian) - 10,
					maximum: Math.max.apply(null, dataKetinggian) + 10,
					reversed: true,
					title: "Kedalaman Air",
					stripLines: [
						{
							opacity: .3,
							color: "#00BFFF",
							startValue: -10,
							endValue: 1
						},
						{
							opacity: .3,
							color: "#2f74b7",
							startValue: 1,
							endValue: 30
						},
						{
							opacity: .3,
							color: "#01b0f3",
							startValue: 30,
							endValue: 40
						},
						{
							opacity: .3,
							color: "#00b04e",
							startValue: 40,
							endValue: 50
						},
						{
							opacity: .3,
							color: "#fcfe07",
							startValue: 50,
							endValue: 60
						},
						{
							opacity: .3,
							color: "#fe0000",
							startValue: 60,
							endValue: 150
						},
						{
							opacity: .3,
							color: "#888888",
							startValue: 150,
							endValue: 999
						}
					]
				},
                axisX: objAxisX,
                height: 320,
				data: [
				//{
				//	type: "column",
				//	axisYType: "secondary",
				//	name: "Curah Hujan",
				//	dataPoints: dataPoints_CH
				//},
				{
					yValueFormatString: "### cm",
					type: "line",
					markerSize: 8,
					markerType: "circle",
					dataPoints: dataPoints,
					lineColor: "red",
					markerColor: "red",
				}]

			});
			//console.log('data ketinggian = ' + dataKetinggian);
			chart.render();
			$(".loading").hide();

		}

	}

	//content += "<div class='col-lg-6' style='height: 420px;'>";
	//content += "<div id='tempGraphDiv" + indexDiv + "'>";	
	//content += "</div>";
	//content += "</div>";

	//console.info(data);

}

function setUpChartV3(data, indexDiv, block) {
	var content = ""
	console.log('block steup chart v2 = ' + block)
	console.log('arrdiv setup chart v2 = ' + arrDiv)
	console.log('data length setup chart v2 = ' + data.length)
	for (var i = 0; i < indexDiv; i++) { //ini bay yang buat 4 chart | yang panjang data combobox mana? | block itu bay
		content = arrDiv[i];
		console.log('content index ke ' + i + ' = ' + content);
		console.log('block index ke ' + i + ' = ' + block[i]);
		var dataPoints = [];
		var dataPoints_CH = [];
		var count = 1;
		var dataKetinggian = [];

		var objAxisX = {
            reversed: false,
            labelFontSize: 10,
            titleFontSize: 15
		};
		var d = new Date(data[0]["Date"]);

		for (var a = 0; a < data.length; a++) { // ini yang buat 4 chart itu ya?
			if (data[a]["PieRecordID"] == block[i]) {
				dataKetinggian.push(data[a]["Ketinggian"]);
				dataPoints.push({
					x: count,
					y: data[a]["Ketinggian"],
					label: data[a]["WeekName"]
				});
				dataPoints_CH.push({
					x: count,
					y: data[a]["TotalCH"],
					label: data[a]["WeekName"]
				});
				count++;
				if (d.getTime() <= data[a]["Date"]) {
					dataPoints.push({
						x: count,
						y: data[a]["Ketinggian"],
						label: data[a]["WeekName"]
					});
					dataPoints_CH.push({
						x: count,
						y: data[a]["TotalCH"],
						label: data[a]["WeekName"]
					});
					count++;
				}
			}
		}
		//console.info(dataPoints);	
		$("#divChartCollection").append(content);
		var chart = new CanvasJS.Chart("tempGraphDiv" + i % 4, {
			title: {
				text: block[i],
				fontWeight: "bolder",
				fontFamily: "Calibri",
				fontSize: 15,
				padding: 1
			},
			zoomEnabled: true,
			axisXType: "secondary",
			animationEnabled: true,
			axisY: {
				minimum: Math.min.apply(null, dataKetinggian) - 10,
				maximum: Math.max.apply(null, dataKetinggian) + 10,
				reversed: true,
                title: "Kedalaman Air",
                labelFontSize: 10,
                titleFontSize: 15,
				stripLines: [
		            {
		            	opacity: .3,
		            	color: "#00BFFF",
		            	startValue: -10,
		            	endValue: 1
		            },
		            {
		            	opacity: .3,
		            	color: "#2f74b7",
		            	startValue: 1,
		            	endValue: 30
		            },
		            {
		            	opacity: .3,
		            	color: "#01b0f3",
		            	startValue: 30,
		            	endValue: 40
		            },
		            {
		            	opacity: .3,
		            	color: "#00b04e",
		            	startValue: 40,
		            	endValue: 50
		            },
		            {
		            	opacity: .3,
		            	color: "#fcfe07",
		            	startValue: 50,
		            	endValue: 60
		            },
		            {
		            	opacity: .3,
		            	color: "#fe0000",
		            	startValue: 60,
		            	endValue: 150
		            },
		            {
		            	opacity: .3,
		            	color: "#888888",
		            	startValue: 150,
		            	endValue: 999
                    }

				]
			},
			axisX: objAxisX,
			data: [
		    //{
		    //	type: "column",
		    //	axisYType: "secondary",
		    //	name: "Curah Hujan",
		    //	dataPoints: dataPoints_CH
		    //},
		    {
		    	yValueFormatString: "### cm",
		    	type: "line",
		    	markerSize: 4,
		    	markerType: "circle",
		    	dataPoints: dataPoints,
		    	lineColor: "red",
		    	markerColor: "red",
		    }]

		});
		console.log('data ketinggian = ' + dataKetinggian);
		chart.render();
		$(".loading").hide();
	}
}
