﻿function GetQueryFilter(estCode, varIdWM, startWeek, endWeek, nilaiOperator, jenisOperator, berurut, varMin, varMax) {
    $('#dialogloading').show();
    if (!($('.modal.in').length)) {
        $('.modal-dialogdetail').css({
            top: 0,
            left: 0
        });
    }
    $('#dialogloading').modal({
        backdrop: true,
        show: true
    });
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/GetQueryFilter',
        data: '{'
                + 'estCode: "' + estCode + '",'
                + 'idwmarea: "' + varIdWM + '",'
                + 'startWeek: "' + startWeek + '",'
                + 'endWeek: "' + endWeek + '",'
                + 'nilaiOperator: "' + nilaiOperator + '",'
                + 'jenisOperator: "' + jenisOperator + '",'
                + 'berurut: "' + berurut + '",'
                + 'varMin: "' + varMin + '",'
                + 'varMax: "' + varMax + '"'
             + '}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var jsonGetQuery = $.parseJSON(response.d);
            console.log(jsonGetQuery);
            if (jsonGetQuery.length == 0) {
                alert("Tidak Ada Data");
            }
            var urlCQL = 'http://172.30.1.32:8080/geoserver/OLM/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=OLM:pzo_all_gama&maxFeatures=9999999999999&outputFormat=application/json';
            if (varIdWM != 0){
                urlCQL = 'http://172.30.1.32:8080/geoserver/OLM/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=OLM:';
                for (var i = 0 ; i < listallwmarea.length; i++) {
                    if (listallwmarea[i]["idWMArea"] == varIdWM) {
                        urlCQL += listallwmarea[i]["typeNameGeoServer"] + '&maxFeatures=9999999999999&outputFormat=application/json';
                        break;
                    }
                }
            }else if(varIdWM == 0) {
                urlCQL += '&CQL_FILTER=ESTNR=%27' + estCode + '%27';
            } else {
                alert("Geoserver Belum ada Untuk hal ini ");
                return;
            }
            GetGeoServerByEstate(urlCQL, jsonGetQuery);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetQueryFilter : " + xhr.statusText);
        },
        complete: function () {
            $("#dialogloading").modal('hide');
        }
    });
}

function GetPalingBanyakQueryFilter(estCode, varIdWM, startWeek, endWeek, berurut, varMin, varMax) {
    $('#dialogloading').show();
    if (!($('.modal.in').length)) {
        $('.modal-dialogdetail').css({
            top: 0,
            left: 0
        });
    }
    $('#dialogloading').modal({
        backdrop: true,
        show: true
    });
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/GetPalingBanyakQueryFilter',
        data: '{'
                + 'estCode: "' + estCode + '",'
                + 'idwmarea: "' + varIdWM + '",'
                + 'startWeek: "' + startWeek + '",'
                + 'endWeek: "' + endWeek + '",'
                + 'berurut: "' + berurut + '",'
                + 'varMin: "' + varMin + '",'
                + 'varMax: "' + varMax + '"'
             + '}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var jsonGetQuery = $.parseJSON(response.d);
            console.log(jsonGetQuery);
            if (jsonGetQuery.length == 0) {
                alert("Tidak Ada Data");
            }
            var urlCQL = 'http://172.30.1.32:8080/geoserver/OLM/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=OLM:pzo_all_gama&maxFeatures=9999999999999&outputFormat=application/json';
            if (varIdWM != 0) {
                urlCQL = 'http://172.30.1.32:8080/geoserver/OLM/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=OLM:';
                for (var i = 0 ; i < listallwmarea.length; i++) {
                    if (listallwmarea[i]["idWMArea"] == varIdWM) {
                        urlCQL += listallwmarea[i]["typeNameGeoServer"] + '&maxFeatures=9999999999999&outputFormat=application/json';
                        break;
                    }
                }
            } else if (varIdWM == 0) {
                urlCQL += '&CQL_FILTER=ESTNR=%27' + estCode + '%27';
            } else {
                alert("Geoserver Belum ada Untuk hal ini ");
                return;
            }
            GetGeoServerByEstate(urlCQL, jsonGetQuery);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetPalingBanyakQueryFilter : " + xhr.statusText);
        },
        complete: function () {
            $("#dialogloading").modal('hide');
        }
    });
}

function GetSeringQueryFilter(estCode, varIdWM, startWeek, endWeek, berurut, varMin, varMax) {
    $('#dialogloading').show();
    if (!($('.modal.in').length)) {
        $('.modal-dialogdetail').css({
            top: 0,
            left: 0
        });
    }
    $('#dialogloading').modal({
        backdrop: true,
        show: true
    });
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/GetSeringQueryFilter',
        data: '{'
                + 'estCode: "' + estCode + '",'
                + 'idwmarea: "' + varIdWM + '",'
                + 'startWeek: "' + startWeek + '",'
                + 'endWeek: "' + endWeek + '",'
                + 'berurut: "' + berurut + '",'
                + 'varMin: "' + varMin + '",'
                + 'varMax: "' + varMax + '"'
             + '}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var jsonGetQuery = $.parseJSON(response.d);
            console.log(jsonGetQuery);
            if (jsonGetQuery.length == 0) {
                alert("Tidak Ada Data");
            }
            var urlCQL = 'http://172.30.1.32:8080/geoserver/OLM/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=OLM:pzo_all_gama&maxFeatures=9999999999999&outputFormat=application/json';
            if (varIdWM != 0) {
                urlCQL = 'http://172.30.1.32:8080/geoserver/OLM/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=OLM:';
                for (var i = 0 ; i < listallwmarea.length; i++) {
                    if (listallwmarea[i]["idWMArea"] == varIdWM) {
                        urlCQL += listallwmarea[i]["typeNameGeoServer"] + '&maxFeatures=9999999999999&outputFormat=application/json';
                        break;
                    }
                }
            } else if (varIdWM == 0) {
                urlCQL += '&CQL_FILTER=ESTNR=%27' + estCode + '%27';
            } else {
                alert("Geoserver Belum ada Untuk hal ini ");
                return;
            }
            GetGeoServerByEstate(urlCQL, jsonGetQuery);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetSeringQueryFilter : " + xhr.statusText);
        },
        complete: function () {
            $("#dialogloading").modal('hide');
        }
    });
}

function ShowDetailV2WithLimit(pierecordid, startWeek, endWeek) {
    $('#divloadingdetail').show();
    $('#divcontentdetail').hide();
    if (!($('.modal.in').length)) {
        $('.modal-dialogdetail').css({
            top: 0,
            left: 0
        });
    }
    $('#myDetail').modal({
        backdrop: false,
        show: true
    });
    $('.modal-dialogdetail').draggable({
        handle: ".modal-header"
    });

    console.info(pierecordid);
    console.info(startWeek);
    console.info(endWeek);

    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/GetPiezoRecordDetailByPieRecordID',
        data: "{"
                + "PieRecordID: '" + pierecordid + "',"
                + "startWeek: " + startWeek + ","
                + "endWeek: " + endWeek +
            "}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            if (json == "") {
                var content = "<div class='box-body text-center'><h3 class='box-title'>Tidak Ada Piezo Di Blok Ini</h3></div>"
                $('#divmasterdetail').html(content);
                $('#divtablehistory').html("");
                $('#divloadingdetail').hide();
                $('#divcontentdetail').show();
            } else {
                var imagesrc = "", content = "";
                if (json[0]["ImageLocation"] == "") {
                    imagesrc = "<img style='cursor:pointer;' width='75px' height='75px' src='../Image/Icon/noimage.png' onclick='PreviewImage()'/>";
                }
                else {
                    imagesrc = "<img style='cursor:pointer;' width='75px' height='75px' src='http://172.30.1.122/PZOService/uploads/" + json[0]["estCode"] + "/" + json[0]["ImageLocation"] + "' onclick='PreviewImage()'/>";
                }

                globalimgurl = json[0]["estCode"] + "/" + json[0]["ImageLocation"];
                console.log(json);

                if (json.length > 0) {
                    $('#lblpiezorecordid').text("PiezoRecordID : " + json[0]["PieRecordID"]);

                    content = '<div class="col-md-7"><div class="row">';
                    content += '<div class="col-md-4">PiezoRecordID</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["PieRecordID"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Longitude</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["Longitude"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Latitude</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["Latitude"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Accuracy</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["Accuracy"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Is Active</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["IsActive"] + '</div></div></div>';
                    content += '<div class="col-md-5" style="text-align: center">' + imagesrc + '</div>';
                    $('#divmasterdetail').html(content);

                    content = '<table id="tablehistory" class="table table-striped table-bordered" style="width: 100%">';
                    content += '<thead><tr>';
                    content += '<th>Week</th><th>Tanggal</th><th>Ketinggian</th><th>Kondisi</th><th>Klasifikasi</th>';
                    content += '</tr></thead><tbody>';



                    for (var i = 0; i < json.length; i++) {
                        content += '<tr>';
                        content += "<td align='center'>" + json[i]["Week"] + "</td><td align='center'>" + json[i]["DateUpload2"] + "</td><td align='center'>" + json[i]["Ketinggian"] + "</td><td align='center'>" + json[i]["Kondisi"] + "</td><td align='center'>" + json[i]["Klasifikasi"] + "</td></tr>";
                    }

                    content += '</tbody></table>';
                }

                $('#divtablehistory').html(content);
                $('#tablehistory').DataTable({
                    dom: 'Bfrtip',
                    pageLength: 5,
                    buttons: [
                        'copy', 'csv', 'excel'
                    ],
                    "order": [[1, "desc"]],
                    "pagingType": "full_numbers"
                });
                setUpChart(json, 0);
                $("#allData").click(function () {
                    setUpChart(json, 0);
                });
                $("#thn").click(function () {
                    setUpChart(json, 12);
                });
                $("#6bln").click(function () {
                    setUpChart(json, 6);
                });
                $("#3bln").click(function () {
                    setUpChart(json, 3);
                });
                $('#divloadingdetail').hide();
                $('#divcontentdetail').show();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetPiezoRecordDetailByPieRecordID 2: " + xhr.statusText);
        }
    });
}

function GetGeoServerByEstate(url, jsonGetQuery) {
    $('#dialogloading').show();
    if (!($('.modal.in').length)) {
        $('.modal-dialogdetail').css({
            top: 0,
            left: 0
        });
    }
    $('#dialogloading').modal({
        backdrop: true,
        show: true
    });
    $.ajax({
        dataType: "json",
        url: url,
        success: function (response) {
            console.log(response);
            drawQuery(response, jsonGetQuery);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetGeoServerByEstate : " + xhr.statusText);
        },
        complete: function () {
            $("#dialogloading").modal('hide');
        }
    });
}

function downloadShpWS(nama, estCode, varIdWM) {
    var urlCQL = 'http://172.30.1.32:8080/geoserver/OLM/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=OLM:pzo_all_gama&maxFeatures=9999999&outputFormat=SHAPE-ZIP';
    if (varIdWM != 0) {
        urlCQL = 'http://172.30.1.32:8080/geoserver/OLM/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=OLM:';
        for (var i = 0 ; i < listallwmarea.length; i++) {
            if (listallwmarea[i]["idWMArea"] == varIdWM) {
                urlCQL += listallwmarea[i]["typeNameGeoServer"] + '&maxFeatures=9999999&outputFormat=SHAPE-ZIP';
                break;
            }
        }
    } else if (varIdWM == 0) {
        urlCQL += '&CQL_FILTER=ESTNR=%27' + estCode + '%27';
    }
    $('#dialogloading').show();
    if (!($('.modal.in').length)) {
        $('.modal-dialogdetail').css({
            top: 0,
            left: 0
        });
    }
    $('#dialogloading').modal({
        backdrop: true,
        show: true
    });
    console.info(urlCQL);
    $.ajax({
        type: 'POST',
        url: urlCQL,
        success: function (response) {
            downloadCSV(nama);
            window.location = urlCQL;
            //console.info(response);
            //var zip = new JSZip();
            //zip.file(nama + ".csv", downloadCSV(nama));
            //zip.file(nama + ".zip", response);

            //zip.generateAsync({ type: "nodebuffer", compression: 'DEFLATE' })
            //.then(function (blob) {
            //    saveAs(blob, nama + ".zip");
            //});

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function downloadShpWS : " + xhr.statusText);
        },
        complete: function () {
            $("#dialogloading").modal('hide');
        }
    });

}

function download(data, filename, type) {
    var file = new Blob([data], { type: type });
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function () {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
}