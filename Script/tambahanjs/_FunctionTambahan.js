﻿function setUpChart(data,lamaData) {
    console.info(data);
    var dataPoints = [];
    var dataPoints_CH = [];
    var count = 1;

    var d = new Date(data[0]["Date"]);
    if (lamaData == 3) {
        d.setMonth(d.getMonth() - 3);
    } else if (lamaData == 6) {
        d.setMonth(d.getMonth() - 6);
    } else if (lamaData == 12) {
        d.setMonth(d.getMonth() - 12);
    }
    console.info(d);

    for (var i = 0; i < data.length; i++) {
        if (lamaData == 0) {
            dataPoints.push({
                x: count,
                y: data[i]["Ketinggian"],
                label: data[i]["Week"]
            });
            dataPoints_CH.push({
                x: count,
                y: data[i]["TotalCH"],
                label: data[i]["Week"]
            });
            count++;
        } else {
            if (d.getTime() <= data[i]["Date"]) {
                dataPoints.push({
                    x: count,
                    y: data[i]["Ketinggian"],
                    label: data[i]["Week"]
                });
                dataPoints_CH.push({
                    x: count,
                    y: data[i]["TotalCH"],
                    label: data[i]["Week"]
                });
                count++;
            }
        }
    }
    console.info(dataPoints);
    var chart = new CanvasJS.Chart("divcharthistory", {
        zoomEnabled: true,
        axisXType: "secondary",
        animationEnabled: true, 
        axisY: {
            reversed: true,
            title: "Kedalaman Air",
            stripLines: [
                {
                    opacity: .3,
                    color: "#00BFFF",
                    startValue: -10,
                    endValue: 1
                },
                {
                    opacity: .3,
                    color: "#E36C0A",
                    startValue: 1,
                    endValue: 30
                },
                {
                    opacity: .3,
                    color: "#FFFF00",
                    startValue: 30,
                    endValue: 40
                },
                {
                    opacity: .3,
                    color: "#00B050",
                    startValue: 40,
                    endValue: 60
                },
                {
                    opacity: .3,
                    color: "#FF0000",
                    startValue: 60,
                    endValue: 150
                },
                {
                    opacity: .3,
                    color: "#888888",
                    startValue: 150,
                    endValue: 999
                }
            ]
        },
        axisY2: {
		    title: "Curah Hujan (mm)"
	    },
        axisX: {
            reversed: true,
            title: "Minggu"
        },
        data: [
        {
		    type: "column",
            axisYType: "secondary",
		    name: "Curah Hujan",
		    dataPoints: dataPoints_CH
	    },
        {
            yValueFormatString: "### cm",
            type: "line",
            markerSize: 8,
            markerType: "circle",
            dataPoints: dataPoints
        }]

    });


    chart.render();

}


function downloadCSV(fileTitle) {
    var headers = {
        est: "ESTNR", // remove commas to avoid errors
        block: "Block",
        piezoid: "PieRecor",
        banyak: "Banyak"
    };

    itemsFormatted = [];

    globalGeoJsonQuery['features'].forEach(function writePoint(features) {
        itemsFormatted.push({
            est: features.properties.ESTNR,
            block: features.properties.Block,
            piezoid: features.properties.PieRecordI,
            banyak: features.properties.Banyak
        });
});

    exportCSVFile(headers, itemsFormatted, fileTitle); // call the exportCSVFile() function to process the JSON and trigger the download
}

function convertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','

            line += array[i][index];
        }

        str += line + '\r\n';
    }

    return str;
}

function exportCSVFile(headers, items, fileTitle) {
    if (headers) {
        items.unshift(headers);
    }

    // Convert Object to JSON
    var jsonObject = JSON.stringify(items);

    var csv = this.convertToCSV(jsonObject);

    var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", exportedFilenmae);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}