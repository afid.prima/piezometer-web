﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddEstate',
            saveButton: '#btnSave',
            UpdateButton: '#btnUpdate',
            modalAdd: '#mdlAdd',
            tabEstate: '#tabEstate',
            tableactive: '#tblactiveestate',
            tableinactive: '#tblinactiveestate',
        },
        initialize: function () {
            this.setVars();
            this.setTableActive();
            this.setTableInactive();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $UpdateButton = $(this.options.UpdateButton);
            $modalAdd = $(this.options.modalAdd);
            $tabEstate = $(this.options.tabEstate);
            $tableactive = $(this.options.tableactive);
            $tableinactive = $(this.options.tableinactive);
            _formAdd = this.options.formAdd;
            _tabEstate = this.options.tabEstate;
            _listEstate = [];
            _listEstateCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTableActive: function () {
            //Initialization of main table or data here
            var tableactive = $tableactive.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "class": "center",
                    "targets": 1
                }, {
                    "class": "left",
                    "targets": 2
                }, {
                    "class": "left",
                    "targets": 3
                }, {
                    "class": "center",
                    "targets": 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 7,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 8,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetEstateActive_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblactiveestate").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setTableInactive: function () {
            //Initialization of main table or data here
            var tableinactive = $tableinactive.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "class": "center",
                    "targets": 1
                }, {
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 7,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 8,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetEstateInActive_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblinactiveestate").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $tableactive.on('click', 'a[data-target="#edit"]', this.edit);
                $tableactive.on('click', 'a[data-target="#delete"]', this.delete);

                $tableinactive.on('click', 'a[data-target="#edit"]', this.edit);
                $tableinactive.on('click', 'a[data-target="#delete"]', this.delete);

                $tabEstate.on('click', 'a', this.tabclick);
                $('#SelectTypeEstate').on('change', this.ChangeEstate);
                //this.formvalidation();

                //select event
                $('#selectType').on('change', this.selectchange);
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");
                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 2,
                            required: true,
                            remote: function () {
                                //document.getElementById('errormsg').style.display = 'none';
                                return {
                                    url: "../../webservice/WebService_COR.asmx/GetEstates",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: "{'EstCode' :'" + $('input[name=code]').val() + "'}", 
                                    dataFilter: function (data) {
                                        var msg = JSON.parse(data);
                                        var json = $.parseJSON(msg.d);
                                        if (json.length > 0) {
                                            //{
                                            document.getElementById('lblcode').style.color = '#dd4b39';
                                            document.getElementById('code').style.borderColor = '#dd4b39';
                                            //document.getElementById('help-block').style.display = 'block';
                                            document.getElementById('errormsg').style.display = 'block';
                                            document.all('errormsg').innerHTML = "Code sudah terdaftar pada sistem";
                                        }
                                    }
                                }


                            },
                        },
                        name: {
                            minlength: 1,
                            required: true
                        },
                        shortname: {
                            minlength: 1,
                            required: true
                        },
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                //e.preventDefault();

            },
            tabchange: function (tabId) {


            },
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                document.getElementById('lblcode').style.color = '#333';
                document.getElementById('code').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Estate";
                $('input[name=code]').val('');
                $('input[name=codeHidden]').val('');
                $('input[name=name]').val('');
                $('input[name=shortname]').val('');
                $('input[name=minX]').val('');
                $('input[name=minY]').val('');
                $('input[name=maxX]').val('');
                $('input[name=maxY]').val('');
                $('#SelectRegionName').val('');
                $('#SelectCompanyName').val('');
                $('input[name=firstaddress]').val('');
                $('input[name=secondaddress]').val('');
                $('input[name=thirdaddress]').val('');
                $('input[name=luasdistrik]').val('');
                $('input[name=telpno]').val('');
                $('#Selectstatus').val('');
                $('#SelectTypeEstate').val('');
                $('input[name=newestcode]').val('');
                _setEvents.formvalidation();
            },
            edit: function () {
                document.getElementById('lblcode').style.color = '#333';
                document.getElementById('code').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'inline';
                document.getElementById('btnSave').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Estate";


                var $this = $(this);
                var $row = $(this).closest('tr');
                var EstCode = $row.find('.EstCode').html();
                $modalAdd.modal('toggle');
                _setEvents.formvalidation();

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetEstates',
                    type: "POST",
                    data: "{'EstCode' :'" + EstCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            var Region = obj.CR_Code[obj.CR_Code.length - 1];
                            var Company = obj.CR_Code.slice(0, -1);
                            $('input[name=code]').val(obj.estCode);
                            $('input[name=codeHidden]').val(obj.estCode);
                            $('input[name=name]').val(obj.estName);
                            $('input[name=shortname]').val(obj.estShortname);
                            $('input[name=minX]').val(obj.MinX);
                            $('input[name=minY]').val(obj.MinY);
                            $('input[name=maxX]').val(obj.MaxX);
                            $('input[name=maxY]').val(obj.MaxY);
                            $('#SelectRegionName').val(Region);
                            $('#SelectCompanyName').val(Company);
                            $('input[name=firstaddress]').val(obj.estAddress1);
                            $('input[name=secondaddress]').val(obj.estAddress2);
                            $('input[name=thirdaddress]').val(obj.estAddress3);
                            $('input[name=luasdistrik]').val(obj.estLuasHa);
                            $('input[name=telpno]').val(obj.estTelpNo);
                            $('#Selectstatus').val(obj.estStatus);
                            $('input[name=newestcode]').val(obj.EstNewCode);
                            if (obj.EstNewCode == "" || obj.EstNewCode == null) {
                                $('#SelectTypeEstate').val(0);
                            }
                            else {
                                $('#SelectTypeEstate').val(1);
                            }

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });


                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = {
                    NewestCode: $('input[name=code]').val(), estCode: $('input[name=codeHidden]').val(), estName: $('input[name=name]').val(), RegionCode: $('#SelectRegionName').val(), CR_Code: $('#SelectCompanyName').val(), estStatus: $('#Selectstatus').val(),
                    estShortname: $('input[name=shortname]').val(), estAddress1: $('input[name=firstaddress]').val(), estAddress2: $('input[name=secondaddress]').val(), estAddress3: $('input[name=thirdaddress]').val(), estLuasHa: ($('input[name=luasdistrik]').val() == '' ? 0 : $('input[name=luasdistrik]').val()) ,
                    estTelpNo: $('input[name=telpno]').val(), MinX: $('input[name=minX]').val(), MinY: $('input[name=minY]').val(), MaxX: $('input[name=maxX]').val(), MaxY: $('input[name=maxY]').val()
                };
                //var savetype = 
                _setCustomFunctions.EditEstate(data);
                $modalAdd.modal('hide');

                return false;

            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var estCode = $row.find('.EstCode').html();
                //Set Ajax Submit Here
                var data = { estCode: estCode };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            save: function () {
                console.log($('#Selectstatus').val());
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = {
                        estCode: $('input[name=code]').val(), estName: $('input[name=name]').val(), RegionCode: $('#SelectRegionName').val(), CR_Code: $('#SelectCompanyName').val(), estStatus: $('#Selectstatus').val(),
                        estShortname: $('input[name=shortname]').val(), estAddress1: $('input[name=firstaddress]').val(), estAddress2: $('input[name=secondaddress]').val(), estAddress3: $('input[name=thirdaddress]').val(), estLuasHa: $('input[name=luasdistrik]').val() == "" ? 0 : $('input[name=luasdistrik]').val(),
                        estTelpNo: $('input[name=telpno]').val(), MinX: $('input[name=minX]').val(), MinY: $('input[name=minY]').val(), MaxX: $('input[name=maxX]').val(), MaxY: $('input[name=maxY]').val()
                    };
                    //var savetype = 
                    _setCustomFunctions.saveEstate(data);
                    $modalAdd.modal('hide');

                    return false;
                }
            },
            ChangeEstate: function () {
                var estCode = $('input[name=code]').val();
                var TypeEstate = $('#SelectTypeEstate').val();

                if (TypeEstate == "1") {
                    if (estCode !== "") {
                        _setCustomFunctions.setNewEstCode(estCode);
                    }
                    else {
                        alert("Estate Code Must Be Filled");
                    }
                }


            },
        },
        setCustomFunctions: {
            init: function () {
                this.getRegion();
                this.getCompany();
                this.setNewEstCode("");
            },
            registerTabContent: function (tabId, mdlcode) {
            },
            getRegion: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterRegion',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectRegionName').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectRegionName").append($('<option>', {
                                value: obj.RegionCode,
                                text: obj.RegionCode + ' - ' + obj.RegionName
                            }));
                        })

                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getCompany: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterCompany',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectCompanyName').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectCompanyName").append($('<option>', {
                                value: obj.CompanyCode,
                                text: obj.CompanyCode + ' - ' + obj.CompanyName
                            }));
                        })

                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            setNewEstCode: function (estCode) {
                var _parent = this;
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetNewEstateCode',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: "{'estCode' :'" + estCode + "'}",
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        json.forEach(function (obj) {
                            $('input[name=newestcode]').val(obj.EstNewCode);
                        })

                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            //getMasterModuleGroup: function () {

            //},
            //getMasterModuleType: function () {

            //},
            //getMasterModuleSubType: function (type) {

            //},
            saveEstate: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveEstate',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableactive.DataTable().ajax.reload();
                            $tableinactive.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Estate has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteEstate: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteEstate',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableactive.DataTable().ajax.reload();
                            $tableinactive.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Estate has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditEstate: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditEstate',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $UpdateButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableactive.DataTable().ajax.reload();
                            $tableinactive.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Estate has been Update successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.DeleteEstate(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
})