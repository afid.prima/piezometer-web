﻿$(function () {

    if (localStorage.getItem("storageName") != null) {
        var decrypted = CryptoJS.AES.decrypt(localStorage.getItem("storageName"), "secret message");
        var otherID = decrypted.toString(CryptoJS.enc.Utf8);
    }

    var _dataTable = [];
    var _dataTableExisting = [];
    var _dataTree = [];
    var _dataTreeDesc = [];
    var _dataTreeExisting = [];
    var _dataTrees = "";
    var _defaultProfilePict = "";
    var _dataEmail = [];
    var Page = {
        options: {
            tableUser: '#tblusers',
            tableLastReq: '#tbllastreq',
            tableEmailUser: '#tblemailuser',
            tableDeviceUser: '#tbldeviceuser',
            saveButton: '#btnSave',
            formAdd: '#formAdd',
            modalAdd: '#mdlAdd',
            modalUser: '#mdlUser',
            setAccessButton: '#btnSetAccess',
            setmdlButton: '#btnSet',
            RequestButton: '#btnRequest',
            CancelestButton: '#btnCancelEst',
            RequestmdlButton: '#btnRequestmdl',
            CancelmdlButton: '#btnCancelmdl',
            SaveRequestButton: '#btnSaveRequest',
            saveEmailButton: '#btnSaves',

            profileImage: '#profileImage',
            imageUpload: '#imageUpload'
        },
        initialize: function () {
            this.setTableUserAcc();
            this.setTableLastReq();
            this.setTableEmailUser();
            this.setTableDeviceUser();
            this.setTableModuleOtherUser(otherID);
            this.setVars();
            this.setHideLogo();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $tableUser = $(this.options.tableUser);
            $tableLastReq = $(this.options.tableLastReq);
            $tableEmailUser = $(this.options.tableEmailUser);
            $tableDeviceUser = $(this.options.tableDeviceUser);
            $saveButton = $(this.options.saveButton);
            $modalAdd = $(this.options.modalAdd);
            $modalUser = $(this.options.modalUser);
            $setAccessButton = $(this.options.setAccessButton);
            $setmdlButton = $(this.options.setmdlButton);
            $RequestButton = $(this.options.RequestButton);
            $CancelestButton = $(this.options.CancelestButton);
            $RequestmdlButton = $(this.options.RequestmdlButton);
            $CancelmdlButton = $(this.options.CancelmdlButton);
            $SaveRequestButton = $(this.options.SaveRequestButton);
            $saveEmailButton = $(this.options.saveEmailButton);
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;

            $profileImage = $(this.options.profileImage);
            $imageUpload = $(this.options.imageUpload);
        },
        setTableUserAcc: function () {
            var index = "";
            var table = $('#tblusers').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 100,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 3
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetModule3_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblusers").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);

                    var $row = $(this).closest('tr');

                    _dataTableExisting.push($(aData[3]).next().html());
                }
            });
        },
        setTableLastReq: function () {
            //Initialization of main table or data here
            var table = $('#tbllastreq').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                //"pagingType": "simple_numbers",
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 1,
                    "class": "left"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 2,
                    "class": "left"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 3,
                    "class": "center"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 4,
                    "class": "center",
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserLastRequestAccess_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    //aoData.push({ "name": "roleId", "value": "admin" });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tbllastreq").show();

                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setTableEmailUser: function () {
            //Initialization of main table or data here
            var table = $('#tblemailuser').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                //"pagingType": "simple_numbers",
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 1,
                    "class": "left"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 2,
                    "class": "left"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 4,
                    "class": "center"
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserEmail_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    //aoData.push({ "name": "roleId", "value": "admin" });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblemailuser").show();

                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                    if ($(aData[4])[1].checked == true) {
                        _dataEmail.push(aData[1]);
                    }

                    //_dataEmail.push(aData[1]);
                }
            });
        },
        setTableModuleOtherUser: function (otherID) {
            var index = "";
            var table = $('#tblmoduleuser').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 100,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetModule5_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    var _otheruserid = "";
                    if (otherID != "" && otherID != undefined) {
                        _otheruserid = otherID;
                        otherID = "";
                    }
                    else {
                        _otheruserid = 0
                    }

                    aoData.push({ 'name': 'UserID', 'value': _otheruserid });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblmoduleuser").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);

                    var $row = $(this).closest('tr');

                    _dataTableExisting.push($(aData[3]).next().html());
                }
            });
        },
        setTableDeviceUser: function () {
            //Initialization of main table or data here
            var table = $('#tbldeviceuser').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                //"pagingType": "simple_numbers",
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 1,
                    "class": "left"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 2,
                    "class": "left"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 3,
                    "class": "center"
                }, {
                    "targets": 4,
                    "class": "center"
                }, {
                    "targets": 5,
                    "class": "center",
                    render: function (d) {
                        return moment(d).format("DD-MM-YYYY");
                    }

                }, {
                    "targets": 6,
                    "class": "center"
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserDevice_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ "name": "UserID", "value": otherID });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tbldeviceuser").show();

                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);

                    if ($(aData[7])[0].innerHTML == "blue") {
                        $('td', nRow).css('background-color', 'aqua');
                    }
                    else if ($(aData[7])[0].innerHTML == "red")
                    {
                        $('td', nRow).css('background-color', 'bisque');
                    }
                    $(".userID").attr('id');

                    //_dataEmail.push(aData[1]);
                }
            });
        },
        setHideLogo: function () {
            if ($("#write").length != 0) {
                if (otherID == undefined)
                {
                    if (localStorage.getItem("stylecss") == "MEN_ModuleMenu2")
                    {
                        document.getElementById("logo").style.display = 'none';
						localStorage.removeItem("stylecss");
                    }
                }
                

            }
        },
        setEvents: {
            init: function () {
                $saveButton.on('click', this.save);
                $tableUser.on('click', '.Access', this.ShowAccess);
                $tableUser.on('click', 'a[data-target="#edit"]', this.editAccess);
                $setAccessButton.on('click', this.setAccess);
                $setmdlButton.on('click', this.savemdlAcc);
                $RequestButton.on('click', this.RequestAccess);
                $CancelestButton.on('click', this.CancelEstAccess);
                $RequestmdlButton.on('click', this.RequestmdlAccess);
                $CancelmdlButton.on('click', this.CancelmdlAccess);
                $SaveRequestButton.on('click', this.SaveRequestButton);
                $saveEmailButton.on('click', this.SaveEmailButton);
                $tableLastReq.on('click', 'a[data-target="#reject"]', this.reject);
                $tableEmailUser.on('click', '.EmailMng', this.EmailMng);
                this.formvalidation($formAdd);

                $profileImage.on('click', this.profileImageClicked);
                $imageUpload.on('change', this.imageUploadChanged);
            },
            formvalidation: function (form) {
                //Main Validation
                $.validator.addMethod("check", function (value, element) {

                    return this.optional(element) || $('input[name=Newpassword]').val() == $('input[name=Confirmpassword]').val()

                }, "Kata Sandi Tidak Sama");

                form.validate({
                    rules: {
                        Oldpassword: {
                            required: true,
                            remote: function () {
                                return {

                                    url: "../../webservice/WebService_COR.asmx/CheckPasswordFromProfile",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: JSON.stringify({ UserPass: $('input[name=Oldpassword]').val() }),
                                    dataFilter: function (data) {
                                        var msg = JSON.parse(data);
                                        return msg.d;
                                    }
                                }
                            }
                        },
                        Newpassword: {
                            required: true,
                            minlength: 5

                        },
                        Confirmpassword: {
                            required: true,
                            minlength: 5,
                            check: true
                        }
                    },
                    messages: {
                        Oldpassword: {
                            remote: 'Kata Sandi Salah',
                            required: 'Minimal Kata Sandi 5 Huruf'
                        },
                        Newpassword: {
                            required: 'Minimal Kata Sandi 5 Huruf'
                        },
                        Confirmpassword: {
                            required: 'Minimal Kata Sandi 5 Huruf'
                        },
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
            },
            save: function () {
                //Event for saving data
                var $this = $(this);
                //alert($('input[name=password]').val());
                //Set Ajax Submit Here
                //var data = { CountryCode: $('input[name=password]').val()};
                //var savetype = 
                if ($('input[name=Newpassword]').val() == $('input[name=Confirmpassword]').val()) {
                    _setCustomFunctions.changePassword($('input[name=Newpassword]').val());
                }
                else {
                    alert("Confirm Password not match!");
                }

                return false;

            },
            editAccess: function () {
                var $row = $(this).closest('tr');
                mdlCode = $row.find('.mdlCode').html();
                mdlAccCode = $row.find('.mdlCode_Access').html();
                _setCustomFunctions.getAccess(mdlCode, mdlAccCode);
                $modalUser.modal('toggle');
            },
            ShowAccess: function () {
                $modalAdd.modal('toggle');
                var $row = $(this).closest('tr');
                var mdlCode = $row.find('.mdlCode').html();
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetUserAccess',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: "{'mdlCode' :'" + mdlCode + "'}",
                    success: function (response) {
                        var tables = $('#tblAcc');
                        $("#tblAcc thead").remove();
                        $("#tblAcc tr").remove();
                        var json = $.parseJSON(response.d);
                        tables.append("<thead><tr><th>Code</th><th>Name</th><th>Desc</th></tr></thead>");
                        json.forEach(function (obj) {
                            tables.append("<tr><td>" + obj.mdlAccCode + "</td><td>" + obj.mdlAccDesc + "</td><td>" + obj.mdlAccInfo + "</td></tr>");


                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            setAccess: function () {
                var $row = $(this).closest('tr');
                mdlCode = $('input[name=code]').val();
                mdlAccCode = $('#SelectAccess option:selected').text();
                mdlAccCodes = $('#SelectAccess option:selected').val();
                $('#' + mdlCode).html(mdlAccCode);
                $('#' + mdlCode + "_Access").html(mdlAccCodes);
                $modalUser.modal('hide');
            },
            savemdlAcc: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/CheckRequestIsExist',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == "false") {
                            var datatable = [];
                            var datas = [];
                            var table = $('#tblusers').DataTable();

                            var headers = [];
                            $('#tblusers th').each(function (index, item) {
                                headers[index] = $(item).html();
                            });
                            $('#tblusers tr').has('td').each(function () {
                                var arrayItem = {};
                                $('td', $(this)).each(function (index, item) {
                                    arrayItem[headers[index]] = $(item).html();
                                });
                                datatable.push(arrayItem);
                            });

                            var $row = $(this).closest('tr');
                            _dataTable = [];
                            for (var i = 0; i < datatable.length; i++) {
                                var a = jQuery(datatable[i].Access).text();
                                var lastFive = a.substr(a.length - 4);

                                _dataTable.push(lastFive);
                            }
                            _setCustomFunctions.saveUser(_dataTable, _dataTree);
                        }
                        else {
                            _setCustomFunctions.showPopup('Info', 'Please Cancel Your Request Before');
                        }
                    }
                })

            },
            RequestAccess: function () {
                //var foo = document.getElementById("tvestate");

                //foo.removeAttribute('jstree-checkbox-disabled');
                //foo.classList.remove("jstree-checkbox-disabled");
                document.getElementById('tvestate2').style.display = 'none';
                document.getElementById('tvestate1').style.display = 'inline';
                document.getElementById('btnCancelEst').style.display = 'inline';
                document.getElementById('btnRequest').style.display = 'none';
            },
            CancelEstAccess: function () {
                //var foo = document.getElementById("tvestate");

                //foo.removeAttribute('jstree-checkbox-disabled');
                //foo.classList.remove("jstree-checkbox-disabled");
                document.getElementById('tvestate2').style.display = 'inline';
                document.getElementById('tvestate1').style.display = 'none';
                document.getElementById('btnRequest').style.display = 'inline';
                document.getElementById('btnCancelEst').style.display = 'none';
            },
            RequestmdlAccess: function () {
                document.getElementById('btnCancelmdl').style.display = 'inline';
                document.getElementById('btnRequestmdl').style.display = 'none';
                var datatable = [];
                var datas = [];
                var table = $('#tblusers').DataTable();

                var headers = [];
                $('#tblusers th').each(function (index, item) {
                    headers[index] = $(item).html();
                });
                $('#tblusers tr').has('td').each(function () {
                    var arrayItem = {};
                    $('td', $(this)).each(function (index, item) {
                        arrayItem[headers[index]] = $(item).html();
                    });
                    datatable.push(arrayItem);
                });

                var $row = $(this).closest('tr');
                _dataTable = [];
                for (var i = 0; i < datatable.length; i++) {
                    document.getElementById('btnReject' + i).style.display = 'inline';
                }

            },
            CancelmdlAccess: function () {
                document.getElementById('btnRequestmdl').style.display = 'inline';
                document.getElementById('btnCancelmdl').style.display = 'none';
                var datatable = [];
                var datas = [];
                var table = $('#tblusers').DataTable();

                var headers = [];
                $('#tblusers th').each(function (index, item) {
                    headers[index] = $(item).html();
                });
                $('#tblusers tr').has('td').each(function () {
                    var arrayItem = {};
                    $('td', $(this)).each(function (index, item) {
                        arrayItem[headers[index]] = $(item).html();
                    });
                    datatable.push(arrayItem);
                });

                var $row = $(this).closest('tr');
                _dataTable = [];
                for (var i = 0; i < datatable.length; i++) {
                    document.getElementById('btnReject' + i).style.display = 'none';
                }

            },
            SaveRequestButton: function () {
                //console.info(_dataTree, "");
                var datasw = { Code: "", ListmdlEstAccess: _dataTree };
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/RequestEstAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(datasw) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            _setCustomFunctions.showPopup('Info', 'Request Access has been successfully');
                            document.getElementById('tvestate2').style.display = 'inline';
                            document.getElementById('tvestate1').style.display = 'none';

                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            reject: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var UsersID = $row.find('.UserID').html();
                var TicketsID = $row.find('.TicketID').html();
                var data = { userID: UsersID, TicketID: TicketsID };
                //Set Ajax Submit Here
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data, 'reject', 'confirm', 'Are You Sure Delete This Data?');

                return false;
            },
            checkAll: function (value) {
                var checkboxes = document.querySelectorAll('input[id]');
                var tag = document.getElementsByClassName('EmailMng');
                var table = $('#tblemailuser').DataTable();
                if (value.checked) {
                    _dataEmail = [];
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = true;
                            var row = $(table).parents('tr')[0];
                            var currentId = $(tag[i - 11]).attr('id');
                            _dataEmail.push(currentId);
                            _dataEmail = _dataEmail.filter(function (element) {
                                return element !== undefined;
                            });
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        console.log(i)
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                            _dataEmail = [];
                        }
                    }
                }
                console.log(_dataEmail);
            },
            EmailMng: function () {
                var $row = $(this).closest('tr');
                var EmailCode = $row.find('.EmailCode').html();
                var checkboxes = document.getElementsByTagName('input');
                if ($(this).is(':checked')) {
                    _dataEmail.push(EmailCode);
                }
                else {
                    for (var i = _dataEmail.length - 1; i >= 0; i--) {
                        if (_dataEmail[i] === EmailCode) {
                            _dataEmail.splice(i, 1);
                            // break;       //<-- Uncomment  if only the first term has to be removed
                        }
                    }
                }

            },
            SaveEmailButton: function () {
                //console.info(_dataTree, "");
                var datasw = { ListEmail: _dataEmail };
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/RequestEmail',
                    data: JSON.stringify({ dataobject: JSON.stringify(datasw) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            _setCustomFunctions.showPopup('Info', 'Request Access has been successfully');
                            $tableEmailUser.DataTable().ajax.reload();

                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            profileImageClicked: function () {
                $imageUpload.click();
            },
            imageUploadChanged: function () {
                _setCustomFunctions.fasterPreview(this);
                var _idPict = this.files && this.files.length ? this.files[0].name.split('.')[0] : '';
                var _ext = imageUpload.value.split('.')[1];
                var FileName = _idPict + "." + _ext
                _setCustomFunctions.validateFileUpload(_idPict);
            }
        },
        setCustomFunctions: {
            init: function () {
                this.HideOtherProfile();
                this.getAccess();
                this.getDepartment();
                this.setTree1();
                this.setTree2();
                //this.getDataUser();
                if (otherID != null) {
                    this.setTreeOtherUser(otherID);
                }
            },
            HideOtherProfile: function () {
                if (otherID == null) {
                    document.getElementById('EstOther').style.display = 'none';
                    document.getElementById('MdlOther').style.display = 'none';
                    document.getElementById('ReqAuthentification').style.display = 'inline';
                    document.getElementById('ChangePwd').style.display = 'inline';
                    document.getElementById('EmailMan').style.display = 'inline';
                    document.getElementById("imageUpload").disabled = false;
                    this.getDataUser();
                }
                else {
                    $.ajax({
                        type: 'POST',
                        url: '../../webservice/WebService_COR.asmx/CheckUserID',
                        contentType: 'application/json; charset=utf-8',
                        data: "{'userID' :'" + otherID + "'}",
                        dataType: 'json',
                        success: function (response) {
                            var json = $.parseJSON(response.d);
                            if (json == true) {
                                document.all('lblUserEmail').innerHTML = "";
                                document.all('lblUserOfficeAddress').innerHTML = "";
                                document.all('lblUserFullName').innerHTML = "";
                                document.all('lblUserDepartment').innerHTML = "";
                                document.all('lblUserPhone').innerHTML = "";
                                document.all('lblEstAccess').innerHTML = "";
                                document.all('lblMdlAccess').innerHTML = "";
                                document.all('lblUserGroup').innerHTML = "";
                                document.getElementById('EstOther').style.display = 'inline';
                                document.getElementById('MdlOther').style.display = 'inline';
                                document.getElementById('ReqAuthentification').style.display = 'none';
                                document.getElementById('ChangePwd').style.display = 'none';
                                document.getElementById('EmailMan').style.display = 'none';
                                document.getElementById('UserProfile').style.display = 'none';
                                document.getElementById("imageUpload").disabled = true;
                                _setCustomFunctions.getDataOtherUser(otherID);
                            }
                            else {
                                window.history.back();
                                var getInput = "NoID";
                                localStorage.setItem("ReturnID", getInput);
                            }



                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });
                }
            },
            getDataUser: function () {
                var _parent = this;
                var userID = 10;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetUserDetail',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        json.forEach(function (obj) {
                            //$('#lblUserEmail').val(obj.UserEmail);
                            //document.getElementById("lblUserEmail").value = obj.UserEmail;
                            document.all('lblUserEmail').innerHTML = obj.UserEmail;
                            document.all('lblUserOfficeAddress').innerHTML = obj.UserOfficeAddr;
                            document.all('lblUserFullName').innerHTML = obj.UserFullName;
                            document.all('lblUserDepartment').innerHTML = obj.DeptName;
                            document.all('lblUserPhone').innerHTML = obj.UserHP;
                            document.all('lblEstAccess').innerHTML = obj.estAccess;
                            document.all('lblMdlAccess').innerHTML = obj.mdlAccess;
                            document.all('lblUserGroup').innerHTML = obj.usrGroupDesc;

                            var _url = "attachment//avatar//" + obj.UserAvatar;
                            $.ajax({
                                type: 'POST',
                                url: '../../webservice/WebService_COR.asmx/CheckImageExist',
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                data: "{'url' :'" + _url + "'}",
                                success: function (response) {
                                    if (response.d == "true") {
                                        document.getElementById("profileImage").src = "../../attachment/avatar/" + obj.UserAvatar;

                                        var yourImg = document.getElementById('profileImage');
                                        //var yourImg = document.getElementById('profileImage3');
                                        if (yourImg && yourImg.style) {
                                            yourImg.style.height = '95px';
                                            yourImg.style.width = '95px';
                                        }
                                        var yourImg = document.getElementById('profileImage4');
                                    }
                                    else {
                                        if (obj.UserGender == "Male") {
                                            document.getElementById("profileImage").src = "../../dist/img/avatar5.png"

                                            var yourImg = document.getElementById('profileImage');
                                            //var yourImg = document.getElementById('profileImage3');
                                            if (yourImg && yourImg.style) {
                                                yourImg.style.height = '95px';
                                                yourImg.style.width = '95px';
                                            }
                                            var yourImg = document.getElementById('profileImage4');
                                        }
                                        else {
                                            document.getElementById("profileImage").src = "../../dist/img/avatar2.png"

                                            var yourImg = document.getElementById('profileImage');
                                            //var yourImg = document.getElementById('profileImage3');
                                            if (yourImg && yourImg.style) {
                                                yourImg.style.height = '95px';
                                                yourImg.style.width = '95px';
                                            }
                                            var yourImg = document.getElementById('profileImage4');
                                        }
                                    }

                                }

                            });
                            //document.getElementById("profileImage").src = "../../attachment/avatar/" + obj.UserAvatar
                            //_defaultProfilePict = obj.UserAvatar;
                            //var yourImg = document.getElementById('profileImage');
                            //if (yourImg && yourImg.style) {
                            //    yourImg.style.height = '95px';
                            //    yourImg.style.width = '95px';
                            //}
                            //$('input[name=Oldpassword]').val(obj.UserPass);
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getDataOtherUser: function (otherID) {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetOtherUserDetail',
                    data: { UserID: JSON.stringify(otherID) },
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        json.forEach(function (obj) {

                            document.all('lblUserEmail').innerHTML = obj.UserEmail;
                            document.all('lblUserOfficeAddress').innerHTML = obj.UserOfficeAddr;
                            document.all('lblUserFullName').innerHTML = obj.UserFullName;
                            document.all('lblUserDepartment').innerHTML = obj.DeptName;
                            document.all('lblUserPhone').innerHTML = obj.UserHP;
                            document.all('lblEstAccess').innerHTML = obj.estAccess;
                            document.all('lblMdlAccess').innerHTML = obj.mdlAccess;
                            document.all('lblUserGroup').innerHTML = obj.usrGroupDesc;
                            var _url = "attachment//avatar//" + obj.UserAvatar;
                            $.ajax({
                                type: 'POST',
                                url: '../../webservice/WebService_COR.asmx/CheckImageExist',
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                data: "{'url' :'" + _url + "'}",
                                success: function (response) {
                                    if (response.d == "true") {
                                        document.getElementById("profileImage").src = "../../attachment/avatar/" + obj.UserAvatar;

                                        var yourImg = document.getElementById('profileImage');
                                        //var yourImg = document.getElementById('profileImage3');
                                        if (yourImg && yourImg.style) {
                                            yourImg.style.height = '95px';
                                            yourImg.style.width = '95px';
                                        }
                                        var yourImg = document.getElementById('profileImage4');
                                    }
                                    else {
                                        if (obj.UserGender == "Male") {
                                            document.getElementById("profileImage").src = "../../dist/img/avatar5.png"

                                            var yourImg = document.getElementById('profileImage');
                                            //var yourImg = document.getElementById('profileImage3');
                                            if (yourImg && yourImg.style) {
                                                yourImg.style.height = '95px';
                                                yourImg.style.width = '95px';
                                            }
                                            var yourImg = document.getElementById('profileImage4');
                                        }
                                        else {
                                            document.getElementById("profileImage").src = "../../dist/img/avatar2.png"

                                            var yourImg = document.getElementById('profileImage');
                                            //var yourImg = document.getElementById('profileImage3');
                                            if (yourImg && yourImg.style) {
                                                yourImg.style.height = '95px';
                                                yourImg.style.width = '95px';
                                            }
                                            var yourImg = document.getElementById('profileImage4');
                                        }
                                    }

                                }

                            });
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getAccess: function (mdlCode, mdlAccCode) {
                $('input[name=mdlacccoder]').val(mdlAccCode);
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterAccess',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'mdlCode' :'" + mdlCode + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectAccess').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectAccess").append($('<option>', {
                                value: obj.mdlAccCode,
                                text: obj.mdlAccDesc
                            }));
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
                $('input[name=code]').val(mdlCode);

            },
            getDepartment: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterDepartment',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectDept').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectDept").append($('<option>', {
                                value: obj.DeptCode,
                                text: obj.DeptCode + ' - ' + obj.DeptName
                            }));
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            setTree1: function () {
                var datas = [];
                var datas2 = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetEstAccessUserBySession',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "GET",
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0; i < json.length; i++) {
                            var parent = json[i].parent
                            datas2.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false, checkbox_disabled: true } });
                        }

                        _dataTablebefore = datas2;
                        var $treeview = $("#tvestate2");
                        $('#tvestate2').jstree('destroy');
                        $('#tvestate2').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                "disabled": {
                                    "check_node": true,
                                    "uncheck_node": true
                                },
                                'data':
                                datas2
                            },

                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            var element = document.getElementById("tvestate2");
                            $treeview.jstree('open_all');
                        });

                        $('#tvestate2')
                            // listen for event
                            .on('changed.jstree', function (e, data) {
                                _dataTreeExisting = data.instance.get_bottom_selected();
                            })
                            // create the instance
                            .jstree();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }


                });

            },
            setTree2: function () {
                var datas = [];
                var datas2 = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetEstAccessUserBySession',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "GET",
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0; i < json.length; i++) {
                            var parent = json[i].parent
                            datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }


                        _dataTablebefore = datas;
                        var $treeview = $("#tvestate1");
                        $('#tvestate1').jstree('destroy');
                        $('#tvestate1').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                "disabled": {
                                    "check_node": true,
                                    "uncheck_node": true
                                },
                                'data':
                                datas
                            },

                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                        });

                        $('#tvestate1')
                            // listen for event
                            //var node = $treeview.jstree(true).find('#jstree-last');
                            //alert(node);
                            .on('changed.jstree', function (e, data) {
                                //var i, j, r = [];
                                _dataTree = [];
                                _dataTree = data.instance.get_bottom_selected();

                            })
                            // create the instance
                            .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }


                });

            },
            setTreeOtherUser: function (otherID) {
                var datas = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetEstAccessUser',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: "{'UsersID' :'" + otherID + "'}",
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0 ; i < json.length; i++) {
                            datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false, checkbox_disabled: true } });
                        }
                        var finaldata = "";
                        if (otherID == 0) {
                            finaldata = json;
                        }
                        else {
                            finaldata = datas;
                        }
                        var $treeview = $("#tvestate3");
                        $('#tvestate3').jstree('destroy');
                        $('#tvestate3').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                'data':
                                    finaldata
                            },
                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                            $treeview.jstree('disable_checkbox');
                        });

                        $('#tvestate3')
                        // listen for event
                        .on('changed.jstree', function (e, data) {
                            _dataTree = [];
                            _dataTree = data.instance.get_bottom_selected();
                        })
                        // create the instance
                        .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });


                localStorage.removeItem("storageName");
            },
            changePassword: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/ChangePasswordProfile',
                    data: "{'Password' :'" + data + "'}",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $('input[name=Oldpassword]').val("");
                            $('input[name=Newpassword]').val("");
                            $('input[name=Confirmpassword]').val("");
                            _setCustomFunctions.showPopup('Info', 'Password has been change successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            RejectAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/RejectRequestAccessByUser',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $tableLastReq.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Access has been Reject');
                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveUser: function (_dataTable, _dataTree) {
                var ReqmdlAcc = "";
                var BefmdlAcc = "";
                var Requestmdl = [];
                var Beforemdl = [];
                for (var i in _dataTableExisting) {
                    if (_dataTableExisting[i] !== _dataTable[i]) {
                        ReqmdlAcc = _dataTable[i];
                        BefmdlAcc = _dataTableExisting[i];
                        Requestmdl.push('- ' + ReqmdlAcc);
                        Beforemdl.push('- ' + BefmdlAcc);
                    }
                }

                if (Requestmdl.length == 0 && Beforemdl.length == 0) {
                    var mdlFinal = "";
                }
                else {
                    var mdlFinal = "<label class=bg-green>Module Access </label><br><label style=font-weight:bold>Request : </label><br>" + Requestmdl + "<br><br>" + "<label style=font-weight:bold>Before : </label><br>" + Beforemdl;
                }



                var ReqEstAcc = "";
                var RemEstAcc = "";
                var RequestEst = [];
                var RemoveEst = [];
                for (var i = 0; i < _dataTreeExisting.length; i++) {
                    for (var x = 0; x < _dataTree.length; x++) {
                        if ($.inArray(_dataTree[x], _dataTreeExisting) == -1) {
                            if ($.inArray("- " + _dataTree[x], RequestEst) == -1) {
                                ReqEstAcc = "- " + _dataTree[x];
                                RequestEst.push(ReqEstAcc);
                            }
                        }
                    }
                }

                for (var i = 0; i < _dataTree.length; i++) {
                    for (var x = 0; x < _dataTreeExisting.length; x++) {
                        if ($.inArray(_dataTreeExisting[x], _dataTree) == -1) {
                            if ($.inArray("- " + _dataTreeExisting[x], RemoveEst) == -1) {
                                RemEstAcc = "- " + _dataTreeExisting[x];
                                RemoveEst.push(RemEstAcc);
                            }
                        }
                    }
                }
                if (RequestEst.length == 0 && RemoveEst.length == 0) {
                    var estFinal = "";
                }
                else if (RemoveEst.length == 0 && RequestEst.length == 1) {
                    var estFinal = "<label class=bg-green>Estate Access </label><br><label style=font-weight:bold>Request : </label><br>" + RequestEst;
                }
                else if (RequestEst.length == 0 && RemoveEst.length == 1) {
                    var estFinal = "<label class=bg-green>Estate Access </label><br><label style=font-weight:bold>Remove : </label><br>" + RemoveEst;
                }
                else {
                    var estFinal = "<label class=bg-green>Estate Access </label><br><label style=font-weight:bold>Request : </label><br>" + RequestEst + "<br><br>" + "<label style=font-weight:bold>Remove : </label><br>" + RemoveEst;
                }


                var datas = { UpdatemdlCode: mdlFinal, UpdateEstCode: estFinal, ListmdlEstAccess: _dataTree, ListmdlAccess: _dataTable, ListmdlAccessExisting: _dataTableExisting, ListmdlEstAccessExisting: _dataTreeExisting };

                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/RequestAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(datas) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            _setCustomFunctions.showPopup('Info', 'Request Access has been successfully');
                            document.getElementById('tvestate2').style.display = 'inline';
                            document.getElementById('tvestate1').style.display = 'none';
                            history.go(0); //forward

                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, type, title, content) {


                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                if (type == "reject") {
                                    _setCustomFunctions.RejectAccess(data);
                                }

                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            },

            fasterPreview: function (uploader) {
                if (uploader.files && uploader.files[0]) {
                    $profileImage.attr('src',
                        window.URL.createObjectURL(uploader.files[0]));
                }
            },
            validateFileUpload: function (_idPict) {
                var fuData = document.getElementById('imageUpload');
                var FileUploadPath = fuData.value;

                if (FileUploadPath == '') {
                    alert("Please upload an image");

                } else {
                    var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();



                    if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                        || Extension == "jpeg" || Extension == "jpg") {


                        if (fuData.files && fuData.files[0]) {

                            var size = fuData.files[0].size;

                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#blah').attr('src', e.target.result);
                            }

                            reader.readAsDataURL(fuData.files[0]);
                            this.saveDataUpload(_idPict, Extension);
                            //}
                        }

                    }


                    else {
                        alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
                        this.goToBeforeProfilePict();
                    }
                }
            },
            goToBeforeProfilePict: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetUserDetail',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        json.forEach(function (obj) {
                            document.getElementById("profileImage").src = "../../attachment/avatar/" + obj.UserAvatar
                            var yourImg = document.getElementById('profileImage');
                            if (yourImg && yourImg.style) {
                                yourImg.style.height = '95px';
                                yourImg.style.width = '95px';
                            }
                            history.go(0);
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveDataUpload: function (_idPict, Extension) {
                var msg = document.getElementById('lblUserFullName').innerHTML;
                var _ext = imageUpload.value.split('.')[1];
                var FileName = _idPict + "." + _ext
                var filename = $('#imageUpload').get(0);
                var files = filename.files;
                var data = new FormData();
                for (var i = 0; i < files.length; i++) {
                    data.append(FileName, files[i]);

                }
                $.confirm({
                    title: "Confirm",
                    content: "Are You Sure To Change Profile Picture ?",
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                $.ajax({
                                    type: 'POST',
                                    url: '../../Service/ImportHandler.ashx?w=10&x=10',
                                    data: data,
                                    contentType: false,
                                    processData: false,
                                    success: function (response) {
                                        $.ajax({
                                            type: 'POST',
                                            url: '../../webservice/WebService_COR.asmx/SaveProfilePict',
                                            data: "{'FileName' :'" + msg + "','Extension' :'" + Extension + "'}",
                                            contentType: 'application/json; charset=utf-8',
                                            dataType: 'json',
                                            success: function (response) {
                                                $saveButton.html("Save");
                                                if (response.d == 'success') {
                                                    history.go(0); //forward
                                                } else {
                                                    //_setCustomFunctions.showPopup('Info', 'Failed to save');
                                                }
                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {
                                            }
                                        });
                                    }

                                });
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                _setCustomFunctions.goToBeforeProfilePict();
                            }
                        }
                    }
                });
            }
        }
    }

    Page.initialize();
})

//function SetupUserDetail() {
//    var _dataTable = [];
//    var _dataTableExisting = [];
//    var _dataTree = [];
//    var _dataTreeDesc = [];
//    var _dataTreeExisting = [];
//    var _dataTrees = "";
//    var _defaultProfilePict = "";
//    var _dataEmail = [];
//    var Page = {
//        options: {
//            tableUser: '#tblusers',
//            tableLastReq: '#tbllastreq',
//            tableEmailUser: '#tblemailuser',
//            saveButton: '#btnSave',
//            formAdd: '#formAdd',
//            modalAdd: '#mdlAdd',
//            modalUser: '#mdlUser',
//            setAccessButton: '#btnSetAccess',
//            setmdlButton: '#btnSet',
//            RequestButton: '#btnRequest',
//            CancelestButton: '#btnCancelEst',
//            RequestmdlButton: '#btnRequestmdl',
//            CancelmdlButton: '#btnCancelmdl',
//            SaveRequestButton: '#btnSaveRequest',
//            saveEmailButton: '#btnSaves',

//            profileImage: '#profileImage',
//            imageUpload: '#imageUpload'
//        },
//        initialize: function () {
//            this.setTableUserAcc();
//            this.setTableLastReq();
//            this.setTableEmailUser();
//            this.setVars();
//            this.setCustomFunctions.init();
//            this.setEvents.init();
//        },
//        setVars: function () {
//            $formAdd = $(this.options.formAdd);
//            $tableUser = $(this.options.tableUser);
//            $tableLastReq = $(this.options.tableLastReq);
//            $tableEmailUser = $(this.options.tableEmailUser);
//            $saveButton = $(this.options.saveButton);
//            $modalAdd = $(this.options.modalAdd);
//            $modalUser = $(this.options.modalUser);
//            $setAccessButton = $(this.options.setAccessButton);
//            $setmdlButton = $(this.options.setmdlButton);
//            $RequestButton = $(this.options.RequestButton);
//            $CancelestButton = $(this.options.CancelestButton);
//            $RequestmdlButton = $(this.options.RequestmdlButton);
//            $CancelmdlButton = $(this.options.CancelmdlButton);
//            $SaveRequestButton = $(this.options.SaveRequestButton);
//            $saveEmailButton = $(this.options.saveEmailButton);
//            _setEvents = this.setEvents;
//            _setCustomFunctions = this.setCustomFunctions;

//            $profileImage = $(this.options.profileImage);
//            $imageUpload = $(this.options.imageUpload);
//        },
//        setTableUserAcc: function () {
//            var index = "";
//            var table = $('#tblusers').DataTable({
//                dom: 'Blfrtip',
//                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
//                pageLength: 100,
//                "filter": true,
//                "columnDefs": [{
//                    "searchable": false,
//                    "orderable": false,
//                    "class": "center",
//                    "targets": 0,
//                    width: 5
//                }, {
//                    "searchable": false,
//                    "orderable": false,
//                    "class": "center",
//                    "targets": 1
//                }, {
//                    "searchable": false,
//                    "orderable": false,
//                    "class": "left",
//                    "targets": 2
//                }, {
//                    "searchable": false,
//                    "orderable": false,
//                    "class": "left",
//                    "targets": 3
//                }, {
//                    "searchable": false,
//                    "orderable": false,
//                    "class": "center",
//                    "targets": 4,
//                    width: 10
//                }],
//                "orderClasses": false,
//                "order": [[1, "asc"]],
//                "info": true,
//                //"scrollY": "450px",
//                //"scrollCollapse": true,
//                "bProcessing": true,
//                "bServerSide": true,
//                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetModule3_ServerSideProcessing",
//                "fnServerData": function (sSource, aoData, fnCallback) {
//                    $.ajax({
//                        "dataType": 'json',
//                        "contentType": "application/json; charset=utf-8",
//                        "type": "GET",
//                        "url": sSource,
//                        "data": aoData,
//                        "success": function (msg) {
//                            var json = jQuery.parseJSON(msg.d);
//                            fnCallback(json);
//                            $("#tblusers").show();
//                        },
//                        error: function (xhr, textStatus, error) {
//                            if (typeof console == "object") {
//                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
//                            }
//                        }
//                    });
//                },
//                fnDrawCallback: function () {
//                },
//                fnRowCallback: function (nRow, aData, iDisplayIndex) {
//                    var info = $(this).DataTable().page.info();
//                    var page = info.page;
//                    var length = info.length;
//                    index = (page * length + (iDisplayIndex + 1));
//                    $('td:eq(0)', nRow).html(index);

//                    var $row = $(this).closest('tr');

//                    _dataTableExisting.push($(aData[3]).next().html());
//                }
//            });
//        },
//        setTableLastReq: function () {
//            //Initialization of main table or data here
//            var table = $('#tbllastreq').DataTable({
//                dom: 'Blfrtip',
//                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
//                pageLength: 25,
//                "filter": true,
//                //"pagingType": "simple_numbers",
//                "columnDefs": [{
//                    "searchable": false,
//                    "orderable": false,
//                    "class": "center",
//                    "targets": 0,
//                }, {
//                    "searchable": false,
//                    "orderable": false,
//                    "targets": 1,
//                    "class": "left"
//                }, {
//                    "searchable": false,
//                    "orderable": false,
//                    "targets": 2,
//                    "class": "left"
//                }, {
//                    "searchable": false,
//                    "orderable": false,
//                    "targets": 3,
//                    "class": "center"
//                }, {
//                    "searchable": false,
//                    "orderable": false,
//                    "targets": 4,
//                    "class": "center",
//                    width: 10
//                }],
//                "orderClasses": false,
//                "order": [[1, "asc"]],
//                "info": true,
//                //"scrollY": "450px",
//                //"scrollCollapse": true,
//                "bProcessing": true,
//                "bServerSide": true,
//                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserLastRequestAccess_ServerSideProcessing",
//                "fnServerData": function (sSource, aoData, fnCallback) {
//                    //aoData.push({ "name": "roleId", "value": "admin" });
//                    $.ajax({
//                        "dataType": 'json',
//                        "contentType": "application/json; charset=utf-8",
//                        "type": "GET",
//                        "url": sSource,
//                        "data": aoData,
//                        "success": function (msg) {
//                            var json = jQuery.parseJSON(msg.d);
//                            fnCallback(json);
//                            $("#tbllastreq").show();

//                        },
//                        error: function (xhr, textStatus, error) {
//                            if (typeof console == "object") {
//                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
//                            }
//                        }
//                    });
//                },
//                fnDrawCallback: function () {
//                },
//                fnRowCallback: function (nRow, aData, iDisplayIndex) {
//                    var info = $(this).DataTable().page.info();
//                    var page = info.page;
//                    var length = info.length;
//                    var index = (page * length + (iDisplayIndex + 1));
//                    $('td:eq(0)', nRow).html(index);
//                }
//            });
//        },
//        setTableEmailUser: function () {
//            //Initialization of main table or data here
//            var table = $('#tblemailuser').DataTable({
//                dom: 'Blfrtip',
//                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
//                pageLength: 25,
//                "filter": true,
//                //"pagingType": "simple_numbers",
//                "columnDefs": [{
//                    "searchable": false,
//                    "orderable": false,
//                    "class": "center",
//                    "targets": 0,
//                }, {
//                    "searchable": false,
//                    "orderable": false,
//                    "targets": 1,
//                    "class": "left"
//                }, {
//                    "searchable": false,
//                    "orderable": false,
//                    "targets": 2,
//                    "class": "left"
//                }, {
//                    "searchable": false,
//                    "orderable": false,
//                    "targets": 4,
//                    "class": "center"
//                }],
//                "orderClasses": false,
//                "order": [[1, "asc"]],
//                "info": true,
//                //"scrollY": "450px",
//                //"scrollCollapse": true,
//                "bProcessing": true,
//                "bServerSide": true,
//                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserEmail_ServerSideProcessing",
//                "fnServerData": function (sSource, aoData, fnCallback) {
//                    //aoData.push({ "name": "roleId", "value": "admin" });
//                    $.ajax({
//                        "dataType": 'json',
//                        "contentType": "application/json; charset=utf-8",
//                        "type": "GET",
//                        "url": sSource,
//                        "data": aoData,
//                        "success": function (msg) {
//                            var json = jQuery.parseJSON(msg.d);
//                            fnCallback(json);
//                            $("#tblemailuser").show();

//                        },
//                        error: function (xhr, textStatus, error) {
//                            if (typeof console == "object") {
//                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
//                            }
//                        }
//                    });
//                },
//                fnDrawCallback: function () {
//                },
//                fnRowCallback: function (nRow, aData, iDisplayIndex) {
//                    var info = $(this).DataTable().page.info();
//                    var page = info.page;
//                    var length = info.length;
//                    var index = (page * length + (iDisplayIndex + 1));
//                    $('td:eq(0)', nRow).html(index);
//                    if ($(aData[4])[1].checked == true) {
//                        _dataEmail.push(aData[1]);
//                    }

//                    //_dataEmail.push(aData[1]);
//                }
//            });
//        },
//        setEvents: {
//            init: function () {
//                $saveButton.on('click', this.save);
//                $tableUser.on('click', '.Access', this.ShowAccess);
//                $tableUser.on('click', 'a[data-target="#edit"]', this.editAccess);
//                $setAccessButton.on('click', this.setAccess);
//                $setmdlButton.on('click', this.savemdlAcc);
//                $RequestButton.on('click', this.RequestAccess);
//                $CancelestButton.on('click', this.CancelEstAccess);
//                $RequestmdlButton.on('click', this.RequestmdlAccess);
//                $CancelmdlButton.on('click', this.CancelmdlAccess);
//                $SaveRequestButton.on('click', this.SaveRequestButton);
//                $saveEmailButton.on('click', this.SaveEmailButton);
//                $tableLastReq.on('click', 'a[data-target="#reject"]', this.reject);
//                $tableEmailUser.on('click', '.EmailMng', this.EmailMng);
//                this.formvalidation($formAdd);

//                $profileImage.on('click', this.profileImageClicked);
//                $imageUpload.on('change', this.imageUploadChanged);
//            },
//            formvalidation: function (form) {
//                //Main Validation
//                $.validator.addMethod("check", function (value, element) {

//                    return this.optional(element) || $('input[name=Newpassword]').val() == $('input[name=Confirmpassword]').val()

//                }, "Kata Sandi Tidak Sama");

//                form.validate({
//                    rules: {
//                        Oldpassword: {
//                            required: true,
//                            remote: function () {
//                                return {

//                                    url: "../../webservice/WebService_COR.asmx/CheckPasswordFromProfile",
//                                    type: "POST",
//                                    contentType: "application/json; charset=utf-8",
//                                    dataType: "json",
//                                    data: JSON.stringify({ UserPass: $('input[name=Oldpassword]').val() }),
//                                    dataFilter: function (data) {
//                                        var msg = JSON.parse(data);
//                                        return msg.d;
//                                    }
//                                }
//                            }
//                        },
//                        Newpassword: {
//                            required: true,
//                            minlength: 5

//                        },
//                        Confirmpassword: {
//                            required: true,
//                            minlength: 5,
//                            check: true
//                        }
//                    },
//                    messages: {
//                        Oldpassword: {
//                            remote: 'Kata Sandi Salah',
//                            required: 'Minimal Kata Sandi 5 Huruf'
//                        },
//                        Newpassword: {
//                            required: 'Minimal Kata Sandi 5 Huruf'
//                        },
//                        Confirmpassword: {
//                            required: 'Minimal Kata Sandi 5 Huruf'
//                        },
//                    },
//                    highlight: function (element) {
//                        $(element).closest('.form-group').addClass('has-error');
//                    },
//                    unhighlight: function (element) {
//                        $(element).closest('.form-group').removeClass('has-error');
//                    },
//                    errorElement: 'span',
//                    errorClass: 'help-block',
//                    errorPlacement: function (error, element) {
//                        if (element.parent('.input-group').length) {
//                            error.insertAfter(element.parent());
//                        } else {
//                            error.insertAfter(element);
//                        }
//                    }
//                });
//            },
//            resetformvalidation: function () {
//            },
//            save: function () {
//                //Event for saving data
//                var $this = $(this);
//                //alert($('input[name=password]').val());
//                //Set Ajax Submit Here
//                //var data = { CountryCode: $('input[name=password]').val()};
//                //var savetype = 
//                if ($('input[name=Newpassword]').val() == $('input[name=Confirmpassword]').val()) {
//                    _setCustomFunctions.changePassword($('input[name=Newpassword]').val());
//                }
//                else {
//                    alert("Confirm Password not match!");
//                }

//                return false;

//            },
//            editAccess: function () {
//                var $row = $(this).closest('tr');
//                mdlCode = $row.find('.mdlCode').html();
//                mdlAccCode = $row.find('.mdlCode_Access').html();
//                _setCustomFunctions.getAccess(mdlCode, mdlAccCode);
//                $modalUser.modal('toggle');
//            },
//            ShowAccess: function () {
//                $modalAdd.modal('toggle');
//                var $row = $(this).closest('tr');
//                var mdlCode = $row.find('.mdlCode').html();
//                $.ajax({
//                    type: 'POST',
//                    url: '../../webservice/WebService_COR.asmx/GetUserAccess',
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    data: "{'mdlCode' :'" + mdlCode + "'}",
//                    success: function (response) {
//                        var tables = $('#tblAcc');
//                        $("#tblAcc thead").remove();
//                        $("#tblAcc tr").remove();
//                        var json = $.parseJSON(response.d);
//                        tables.append("<thead><tr><th>Code</th><th>Name</th><th>Desc</th></tr></thead>");
//                        json.forEach(function (obj) {
//                            tables.append("<tr><td>" + obj.mdlAccCode + "</td><td>" + obj.mdlAccDesc + "</td><td>" + obj.mdlAccInfo + "</td></tr>");


//                        })

//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }
//                });
//            },
//            setAccess: function () {
//                var $row = $(this).closest('tr');
//                mdlCode = $('input[name=code]').val();
//                mdlAccCode = $('#SelectAccess option:selected').text();
//                mdlAccCodes = $('#SelectAccess option:selected').val();
//                $('#' + mdlCode).html(mdlAccCode);
//                $('#' + mdlCode + "_Access").html(mdlAccCodes);
//                $modalUser.modal('hide');
//            },
//            savemdlAcc: function () {
//                $.ajax({
//                    type: 'GET',
//                    url: '../../webservice/WebService_COR.asmx/CheckRequestIsExist',
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    success: function (response) {
//                        if (response.d == "false") {
//                            var datatable = [];
//                            var datas = [];
//                            var table = $('#tblusers').DataTable();

//                            var headers = [];
//                            $('#tblusers th').each(function (index, item) {
//                                headers[index] = $(item).html();
//                            });
//                            $('#tblusers tr').has('td').each(function () {
//                                var arrayItem = {};
//                                $('td', $(this)).each(function (index, item) {
//                                    arrayItem[headers[index]] = $(item).html();
//                                });
//                                datatable.push(arrayItem);
//                            });

//                            var $row = $(this).closest('tr');
//                            _dataTable = [];
//                            for (var i = 0; i < datatable.length; i++) {
//                                var a = jQuery(datatable[i].Access).text();
//                                var lastFive = a.substr(a.length - 4);

//                                _dataTable.push(lastFive);
//                            }
//                            _setCustomFunctions.saveUser(_dataTable, _dataTree);
//                        }
//                        else {
//                            _setCustomFunctions.showPopup('Info', 'Please Cancel Your Request Before');
//                        }
//                    }
//                })

//            },
//            RequestAccess: function () {
//                //var foo = document.getElementById("tvestate");

//                //foo.removeAttribute('jstree-checkbox-disabled');
//                //foo.classList.remove("jstree-checkbox-disabled");
//                document.getElementById('tvestate2').style.display = 'none';
//                document.getElementById('tvestate1').style.display = 'inline';
//                document.getElementById('btnCancelEst').style.display = 'inline';
//                document.getElementById('btnRequest').style.display = 'none';
//            },
//            CancelEstAccess: function () {
//                //var foo = document.getElementById("tvestate");

//                //foo.removeAttribute('jstree-checkbox-disabled');
//                //foo.classList.remove("jstree-checkbox-disabled");
//                document.getElementById('tvestate2').style.display = 'inline';
//                document.getElementById('tvestate1').style.display = 'none';
//                document.getElementById('btnRequest').style.display = 'inline';
//                document.getElementById('btnCancelEst').style.display = 'none';
//            },
//            RequestmdlAccess: function () {
//                document.getElementById('btnCancelmdl').style.display = 'inline';
//                document.getElementById('btnRequestmdl').style.display = 'none';
//                var datatable = [];
//                var datas = [];
//                var table = $('#tblusers').DataTable();

//                var headers = [];
//                $('#tblusers th').each(function (index, item) {
//                    headers[index] = $(item).html();
//                });
//                $('#tblusers tr').has('td').each(function () {
//                    var arrayItem = {};
//                    $('td', $(this)).each(function (index, item) {
//                        arrayItem[headers[index]] = $(item).html();
//                    });
//                    datatable.push(arrayItem);
//                });

//                var $row = $(this).closest('tr');
//                _dataTable = [];
//                for (var i = 0; i < datatable.length; i++) {
//                    document.getElementById('btnReject' + i).style.display = 'inline';
//                }

//            },
//            CancelmdlAccess: function () {
//                document.getElementById('btnRequestmdl').style.display = 'inline';
//                document.getElementById('btnCancelmdl').style.display = 'none';
//                var datatable = [];
//                var datas = [];
//                var table = $('#tblusers').DataTable();

//                var headers = [];
//                $('#tblusers th').each(function (index, item) {
//                    headers[index] = $(item).html();
//                });
//                $('#tblusers tr').has('td').each(function () {
//                    var arrayItem = {};
//                    $('td', $(this)).each(function (index, item) {
//                        arrayItem[headers[index]] = $(item).html();
//                    });
//                    datatable.push(arrayItem);
//                });

//                var $row = $(this).closest('tr');
//                _dataTable = [];
//                for (var i = 0; i < datatable.length; i++) {
//                    document.getElementById('btnReject' + i).style.display = 'none';
//                }

//            },
//            SaveRequestButton: function () {
//                //console.info(_dataTree, "");
//                var datasw = { Code: "", ListmdlEstAccess: _dataTree };
//                $.ajax({
//                    type: 'POST',
//                    url: '../../webservice/WebService_COR.asmx/RequestEstAccess',
//                    data: JSON.stringify({ dataobject: JSON.stringify(datasw) }),
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    success: function (response) {
//                        if (response.d == 'success') {
//                            _setCustomFunctions.showPopup('Info', 'Request Access has been successfully');
//                            document.getElementById('tvestate2').style.display = 'inline';
//                            document.getElementById('tvestate1').style.display = 'none';

//                        } else {
//                            _setCustomFunctions.showPopup('Info', 'Failed to save');
//                        }
//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }
//                });
//            },
//            reject: function () {
//                //Event for deleting data
//                var $this = $(this);
//                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
//                var $row = $(this).closest('tr');
//                var UsersID = $row.find('.UserID').html();
//                var TicketsID = $row.find('.TicketID').html();
//                var data = { userID: UsersID, TicketID: TicketsID };
//                //Set Ajax Submit Here
//                //var savetype = 

//                _setCustomFunctions.showConfirmPopup(data, 'reject', 'confirm', 'Are You Sure Delete This Data?');

//                return false;
//            },
//            checkAll: function (value) {
//                var checkboxes = document.getElementsByTagName('input');
//                var tag = document.getElementsByClassName('EmailMng');
//                var table = $('#tblemailuser').DataTable();
//                if (value.checked) {
//                    _dataEmail = [];
//                    for (var i = 0; i < checkboxes.length; i++) {
//                        if (checkboxes[i].type == 'checkbox') {
//                            checkboxes[i].checked = true;
//                            var row = $(table).parents('tr')[0];
//                            var currentId = $(tag[i - 11]).attr('id');
//                            _dataEmail.push(currentId);
//                            _dataEmail = _dataEmail.filter(function (element) {
//                                return element !== undefined;
//                            });
//                        }
//                    }
//                } else {
//                    for (var i = 0; i < checkboxes.length; i++) {
//                        console.log(i)
//                        if (checkboxes[i].type == 'checkbox') {
//                            checkboxes[i].checked = false;
//                            _dataEmail = [];
//                        }
//                    }
//                }
//                console.log(_dataEmail);
//            },
//            EmailMng: function () {
//                var $row = $(this).closest('tr');
//                var EmailCode = $row.find('.EmailCode').html();
//                var checkboxes = document.getElementsByTagName('input');
//                if ($(this).is(':checked')) {
//                    _dataEmail.push(EmailCode);
//                }
//                else {
//                    for (var i = _dataEmail.length - 1; i >= 0; i--) {
//                        if (_dataEmail[i] === EmailCode) {
//                            _dataEmail.splice(i, 1);
//                            // break;       //<-- Uncomment  if only the first term has to be removed
//                        }
//                    }
//                }

//            },
//            SaveEmailButton: function () {
//                //console.info(_dataTree, "");
//                var datasw = { ListEmail: _dataEmail };
//                $.ajax({
//                    type: 'POST',
//                    url: '../../webservice/WebService_COR.asmx/RequestEmail',
//                    data: JSON.stringify({ dataobject: JSON.stringify(datasw) }),
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    success: function (response) {
//                        if (response.d == 'success') {
//                            _setCustomFunctions.showPopup('Info', 'Request Access has been successfully');
//                            $tableEmailUser.DataTable().ajax.reload();

//                        } else {
//                            _setCustomFunctions.showPopup('Info', 'Failed to save');
//                        }
//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }
//                });
//            },
//            profileImageClicked: function () {
//                $imageUpload.click();
//            },
//            imageUploadChanged: function () {
//                _setCustomFunctions.fasterPreview(this);
//                var _idPict = this.files && this.files.length ? this.files[0].name.split('.')[0] : '';
//                var _ext = imageUpload.value.split('.')[1];
//                var FileName = _idPict + "." + _ext
//                _setCustomFunctions.validateFileUpload(_idPict);
//            }
//        },
//        setCustomFunctions: {
//            init: function () {
//                this.getAccess();
//                this.getDepartment();
//                this.setTree1();
//                this.setTree2();
//                this.getDataUser();
//            },
//            getDataUser: function () {
//                var _parent = this;
//                var userID = 10;
//                $.ajax({
//                    type: 'GET',
//                    url: '../../webservice/WebService_COR.asmx/GetUserDetail',
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    success: function (response) {
//                        var json = $.parseJSON(response.d);
//                        json.forEach(function (obj) {
//                            //$('#lblUserEmail').val(obj.UserEmail);
//                            //document.getElementById("lblUserEmail").value = obj.UserEmail;
//                            document.all('lblUserEmail').innerHTML = obj.UserEmail;
//                            document.all('lblUserOfficeAddress').innerHTML = obj.UserOfficeAddr;
//                            document.all('lblUserFullName').innerHTML = obj.UserFullName;
//                            document.all('lblUserDepartment').innerHTML = obj.DeptName;
//                            document.all('lblUserPhone').innerHTML = obj.UserHP;
//                            document.all('lblEstAccess').innerHTML = obj.estAccess;
//                            document.all('lblMdlAccess').innerHTML = obj.mdlAccess; 
//                            document.all('lblUserGroup').innerHTML = obj.usrGroupDesc;

//                            var _url = "attachment//avatar//" + obj.UserAvatar;
//                            $.ajax({
//                                type: 'POST',
//                                url: '../../webservice/WebService_COR.asmx/CheckImageExist',
//                                contentType: 'application/json; charset=utf-8',
//                                dataType: 'json',
//                                data: "{'url' :'" + _url + "'}",
//                                success: function (response) {
//                                    if (response.d == "true") {
//                                        document.getElementById("profileImage").src = "../../attachment/avatar/" + obj.UserAvatar;

//                                        var yourImg = document.getElementById('profileImage');
//                                        //var yourImg = document.getElementById('profileImage3');
//                                        if (yourImg && yourImg.style) {
//                                            yourImg.style.height = '95px';
//                                            yourImg.style.width = '95px';
//                                        }
//                                        var yourImg = document.getElementById('profileImage4');
//                                    }
//                                    else {
//                                        if (obj.UserGender == "Male") {
//                                            document.getElementById("profileImage").src = "../../attachment/avatar/DefaultMale.png"

//                                            var yourImg = document.getElementById('profileImage');
//                                            //var yourImg = document.getElementById('profileImage3');
//                                            if (yourImg && yourImg.style) {
//                                                yourImg.style.height = '95px';
//                                                yourImg.style.width = '95px';
//                                            }
//                                            var yourImg = document.getElementById('profileImage4');
//                                        }
//                                        else {
//                                            document.getElementById("profileImage").src = "../../attachment/avatar/DefaultFemale.png"

//                                            var yourImg = document.getElementById('profileImage');
//                                            //var yourImg = document.getElementById('profileImage3');
//                                            if (yourImg && yourImg.style) {
//                                                yourImg.style.height = '95px';
//                                                yourImg.style.width = '95px';
//                                            }
//                                            var yourImg = document.getElementById('profileImage4');
//                                        }
//                                    }

//                                }

//                            });
//                            //document.getElementById("profileImage").src = "../../attachment/avatar/" + obj.UserAvatar
//                            //_defaultProfilePict = obj.UserAvatar;
//                            //var yourImg = document.getElementById('profileImage');
//                            //if (yourImg && yourImg.style) {
//                            //    yourImg.style.height = '95px';
//                            //    yourImg.style.width = '95px';
//                            //}
//                            //$('input[name=Oldpassword]').val(obj.UserPass);
//                        })

//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }
//                });
//            },
//            getAccess: function (mdlCode, mdlAccCode) {
//                $('input[name=mdlacccoder]').val(mdlAccCode);
//                $.ajax({
//                    type: 'POST',
//                    url: '../../webservice/WebService_COR.asmx/GetMasterAccess',
//                    contentType: 'application/json; charset=utf-8',
//                    data: "{'mdlCode' :'" + mdlCode + "'}",
//                    dataType: 'json',
//                    success: function (response) {
//                        var json = $.parseJSON(response.d);
//                        $('#SelectAccess').find('option').remove();
//                        json.forEach(function (obj) {
//                            $("#SelectAccess").append($('<option>', {
//                                value: obj.mdlAccCode,
//                                text: obj.mdlAccDesc
//                            }));
//                        })

//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }
//                });
//                $('input[name=code]').val(mdlCode);

//            },
//            getDepartment: function () {
//                var _parent = this;
//                $.ajax({
//                    type: 'GET',
//                    url: '../../webservice/WebService_COR.asmx/GetMasterDepartment',
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    success: function (response) {
//                        var json = $.parseJSON(response.d);
//                        $('#SelectDept').find('option').remove();
//                        json.forEach(function (obj) {
//                            $("#SelectDept").append($('<option>', {
//                                value: obj.DeptCode,
//                                text: obj.DeptCode + ' - ' + obj.DeptName
//                            }));
//                        })

//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }
//                });
//            },
//            setTree1: function () {
//                var datas = [];
//                var datas2 = [];
//                var id = 1;
//                $.ajax({
//                    url: '../../webservice/WebService_COR.asmx/GetEstAccessUserBySession',
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    type: "GET",
//                    success: function (response) {
//                        var json = jQuery.parseJSON(response.d);
//                        for (var i = 0 ; i < json.length; i++) {
//                            var parent = json[i].parent
//                            datas2.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false, checkbox_disabled: true } });
//                        }

//                        _dataTablebefore = datas2;
//                        var $treeview = $("#tvestate2");
//                        $('#tvestate2').jstree('destroy');
//                        $('#tvestate2').jstree({
//                            plugins: ['checkbox', 'changed'],
//                            'core': {
//                                "themes": { "icons": false },
//                                expand_selected_onload: true,
//                                "disabled": {
//                                    "check_node": true,
//                                    "uncheck_node": true
//                                },
//                                'data':
//                                    datas2
//                            },

//                        }).bind("loaded.jstree", function (event, data) {
//                            //$(this).jstree("open_all");
//                            var element = document.getElementById("tvestate2");
//                            $treeview.jstree('open_all');
//                        });

//                        $('#tvestate2')
//                        // listen for event
//                        .on('changed.jstree', function (e, data) {
//                            _dataTreeExisting = data.instance.get_bottom_selected();
//                        })
//                        // create the instance
//                        .jstree();

//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }


//                });

//            },
//            setTree2: function () {
//                var datas = [];
//                var datas2 = [];
//                var id = 1;
//                $.ajax({
//                    url: '../../webservice/WebService_COR.asmx/GetEstAccessUserBySession',
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    type: "GET",
//                    success: function (response) {
//                        var json = jQuery.parseJSON(response.d);
//                        for (var i = 0 ; i < json.length; i++) {
//                            var parent = json[i].parent
//                            datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
//                        }


//                        _dataTablebefore = datas;
//                        var $treeview = $("#tvestate1");
//                        $('#tvestate1').jstree('destroy');
//                        $('#tvestate1').jstree({
//                            plugins: ['checkbox', 'changed'],
//                            'core': {
//                                "themes": { "icons": false },
//                                expand_selected_onload: true,
//                                "disabled": {
//                                    "check_node": true,
//                                    "uncheck_node": true
//                                },
//                                'data':
//                                    datas
//                            },

//                        }).bind("loaded.jstree", function (event, data) {
//                            //$(this).jstree("open_all");
//                            $treeview.jstree('open_all');
//                        });

//                        $('#tvestate1')
//                        // listen for event
//                        //var node = $treeview.jstree(true).find('#jstree-last');
//                        //alert(node);
//                        .on('changed.jstree', function (e, data) {
//                            //var i, j, r = [];
//                            _dataTree = [];
//                            _dataTree = data.instance.get_bottom_selected();

//                        })
//                        // create the instance
//                        .jstree();
//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }


//                });

//            },
//            changePassword: function (data) {
//                $.ajax({
//                    type: 'POST',
//                    url: '../../webservice/WebService_COR.asmx/ChangePasswordProfile',
//                    data: "{'Password' :'" + data + "'}",
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    success: function (response) {
//                        $saveButton.html("Save");
//                        if (response.d == 'success') {
//                            $('input[name=Oldpassword]').val("");
//                            $('input[name=Newpassword]').val("");
//                            $('input[name=Confirmpassword]').val("");
//                            _setCustomFunctions.showPopup('Info', 'Password has been change successfully');
//                        } else {
//                            $(_formAdd + " :input").prop("disabled", false);
//                        }
//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }
//                });
//            },
//            RejectAccess: function (data) {
//                $.ajax({
//                    type: 'POST',
//                    url: '../../webservice/WebService_COR.asmx/RejectRequestAccessByUser',
//                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    success: function (response) {
//                        if (response.d == 'success') {
//                            $tableLastReq.DataTable().ajax.reload();
//                            _setCustomFunctions.showPopup('Info', 'Access has been Reject');
//                        } else {
//                            _setCustomFunctions.showPopup('Info', 'Failed to save');
//                        }
//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }
//                });
//            },
//            saveUser: function (_dataTable, _dataTree) {
//                var ReqmdlAcc = "";
//                var BefmdlAcc = "";
//                var Requestmdl = [];
//                var Beforemdl = [];
//                for (var i in _dataTableExisting) {
//                    if (_dataTableExisting[i] !== _dataTable[i]) {
//                        ReqmdlAcc = _dataTable[i];
//                        BefmdlAcc = _dataTableExisting[i];
//                        Requestmdl.push('- ' + ReqmdlAcc);
//                        Beforemdl.push('- ' + BefmdlAcc);
//                    }
//                }

//                if (Requestmdl.length == 0 && Beforemdl.length == 0) {
//                    var mdlFinal = "";
//                }
//                else
//                {
//                    var mdlFinal = "<label class=bg-green>Module Access </label><br><label style=font-weight:bold>Request : </label><br>" + Requestmdl + "<br><br>" + "<label style=font-weight:bold>Before : </label><br>" + Beforemdl;
//                }
                


//                var ReqEstAcc = "";
//                var RemEstAcc = "";
//                var RequestEst = [];
//                var RemoveEst = [];
//                for (var i = 0; i < _dataTreeExisting.length; i++) {
//                    for (var x = 0; x < _dataTree.length; x++) {
//                        if ($.inArray(_dataTree[x], _dataTreeExisting) == -1) {
//                            if ($.inArray("- " + _dataTree[x], RequestEst) == -1)
//                            {
//                                ReqEstAcc = "- " + _dataTree[x];
//                                RequestEst.push(ReqEstAcc);
//                            }
//                        }
//                    }
//                }

//                for (var i = 0; i < _dataTree.length; i++) {
//                    for (var x = 0; x < _dataTreeExisting.length; x++) {
//                        if ($.inArray(_dataTreeExisting[x], _dataTree) == -1) {
//                            if ($.inArray("- " + _dataTreeExisting[x], RemoveEst) == -1) {
//                                RemEstAcc = "- " + _dataTreeExisting[x];
//                                RemoveEst.push(RemEstAcc);
//                            }
//                        }
//                    }
//                }
//                if (RequestEst.length == 0 && RemoveEst.length == 0) {
//                    var estFinal = "";  
//                }
//                else if (RemoveEst.length == 0 && RequestEst.length == 1)
//                {
//                    var estFinal = "<label class=bg-green>Estate Access </label><br><label style=font-weight:bold>Request : </label><br>" + RequestEst;
//                }
//                else if (RequestEst.length == 0 && RemoveEst.length == 1)
//                {
//                    var estFinal = "<label class=bg-green>Estate Access </label><br><label style=font-weight:bold>Remove : </label><br>" + RemoveEst;
//                }
//                else
//                {
//                    var estFinal = "<label class=bg-green>Estate Access </label><br><label style=font-weight:bold>Request : </label><br>" + RequestEst + "<br><br>" + "<label style=font-weight:bold>Remove : </label><br>" + RemoveEst;
//                }
                

//                var datas = { UpdatemdlCode: mdlFinal, UpdateEstCode: estFinal, ListmdlEstAccess: _dataTree, ListmdlAccess: _dataTable, ListmdlAccessExisting: _dataTableExisting, ListmdlEstAccessExisting: _dataTreeExisting };
                
//                $.ajax({
//                    type: 'POST',
//                    url: '../../webservice/WebService_COR.asmx/RequestAccess',
//                    data: JSON.stringify({ dataobject: JSON.stringify(datas) }),
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    success: function (response) {
//                        if (response.d == 'success') {
//                            _setCustomFunctions.showPopup('Info', 'Request Access has been successfully');
//                            document.getElementById('tvestate2').style.display = 'inline';
//                            document.getElementById('tvestate1').style.display = 'none';
//                            history.go(0); //forward

//                        } else {
//                            _setCustomFunctions.showPopup('Info', 'Failed to save');
//                        }
//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }
//                });
//            },
//            showPopup: function (title, content) {
//                $.confirm({
//                    title: title,
//                    content: content,
//                    type: 'blue',
//                    typeAnimated: true,
//                    buttons: {
//                        OK: {
//                            text: 'OK',
//                            btnClass: 'btn-blue',
//                            action: function () {

//                            }
//                        }
//                    }
//                });

//            },
//            showConfirmPopup: function (data, type, title, content) {


//                $.confirm({
//                    title: title,
//                    content: content,
//                    type: 'blue',
//                    typeAnimated: true,
//                    buttons: {
//                        YES: {
//                            text: 'YES',
//                            btnClass: 'btn-red',
//                            action: function () {
//                                if (type == "reject") {
//                                    _setCustomFunctions.RejectAccess(data);
//                                }

//                            }
//                        },
//                        NO: {
//                            text: 'NO',
//                            btnClass: 'btn-blue',
//                            action: function () {
//                                //$modalAdd.modal('toggle');
//                            }
//                        }
//                    }
//                });

//            },

//            fasterPreview: function (uploader) {
//                if (uploader.files && uploader.files[0]) {
//                    $profileImage.attr('src',
//                        window.URL.createObjectURL(uploader.files[0]));
//                }
//            },
//            validateFileUpload: function (_idPict) {
//                var fuData = document.getElementById('imageUpload');
//                var FileUploadPath = fuData.value;

//                if (FileUploadPath == '') {
//                    alert("Please upload an image");

//                } else {
//                    var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();



//                    if (Extension == "gif" || Extension == "png" || Extension == "bmp"
//                        || Extension == "jpeg" || Extension == "jpg") {


//                        if (fuData.files && fuData.files[0]) {

//                            var size = fuData.files[0].size;
                            
//                            var reader = new FileReader();

//                            reader.onload = function (e) {
//                                $('#blah').attr('src', e.target.result);
//                            }

//                            reader.readAsDataURL(fuData.files[0]);
//                            this.saveDataUpload(_idPict, Extension);
//                            //}
//                        }

//                    }


//                    else {
//                        alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
//                        this.goToBeforeProfilePict();
//                    }
//                }
//            },
//            goToBeforeProfilePict: function () {
//                $.ajax({
//                    type: 'GET',
//                    url: '../../webservice/WebService_COR.asmx/GetUserDetail',
//                    contentType: 'application/json; charset=utf-8',
//                    dataType: 'json',
//                    success: function (response) {
//                        var json = $.parseJSON(response.d);
//                        json.forEach(function (obj) {
//                            document.getElementById("profileImage").src = "../../attachment/avatar/" + obj.UserAvatar
//                            var yourImg = document.getElementById('profileImage');
//                            if (yourImg && yourImg.style) {
//                                yourImg.style.height = '95px';
//                                yourImg.style.width = '95px';
//                            }
//                        })

//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
//                    }
//                });
//            },
//            saveDataUpload: function (_idPict, Extension) {
//                var msg = document.getElementById('lblUserFullName').innerHTML;
//                var _ext = imageUpload.value.split('.')[1];
//                var FileName = _idPict + "." + _ext
//                var filename = $('#imageUpload').get(0);
//                var files = filename.files;
//                var data = new FormData();
//                for (var i = 0; i < files.length; i++) {
//                    data.append(FileName, files[i]);

//                }
//                $.confirm({
//                    title: "Confirm",
//                    content: "Are You Sure To Change Profile Picture ?",
//                    type: 'blue',
//                    typeAnimated: true,
//                    buttons: {
//                        YES: {
//                            text: 'YES',
//                            btnClass: 'btn-red',
//                            action: function () {
//                                $.ajax({
//                                    type: 'POST',
//                                    url: '../../Service/ImportHandler.ashx?w=10&x=10',
//                                    data: data,
//                                    contentType: false,
//                                    processData: false,
//                                    success: function (response) {
//                                        $.ajax({
//                                            type: 'POST',
//                                            url: '../../webservice/WebService_COR.asmx/SaveProfilePict',
//                                            data: "{'FileName' :'" + msg + "','Extension' :'" + Extension + "'}",
//                                            contentType: 'application/json; charset=utf-8',
//                                            dataType: 'json',
//                                            success: function (response) {
//                                                $saveButton.html("Save");
//                                                if (response.d == 'success') {
//                                                    history.go(0); //forward
//                                                } else {
//                                                    //_setCustomFunctions.showPopup('Info', 'Failed to save');
//                                                }
//                                            },
//                                            error: function (xhr, ajaxOptions, thrownError) {
//                                            }
//                                        });
//                                    }

//                                });
//                            }
//                        },
//                        NO: {
//                            text: 'NO',
//                            btnClass: 'btn-blue',
//                            action: function () {
//                                _setCustomFunctions.goToBeforeProfilePict();
//                            }
//                        }
//                    }
//                });
//            }
//        }

//    }

//    Page.initialize();
//}

//$("#profileImage").click(function (e) {
//    $("#imageUpload").click();
//});

//function fasterPreview(uploader) {
//    if (uploader.files && uploader.files[0]) {
//        $('#profileImage').attr('src',
//           window.URL.createObjectURL(uploader.files[0]));
//    }
//}

//$("#imageUpload").change(function () {
//    //alert(files[0].size);
//    fasterPreview(this);
//    var _idPict = this.files && this.files.length ? this.files[0].name.split('.')[0] : '';
//    var _ext = imageUpload.value.split('.')[1];
//    var FileName = _idPict + "." + _ext
//    ValidateFileUpload(_idPict);
//});

//function GotoBeforeProfilePict() {
//    $.ajax({
//        type: 'GET',
//        url: '../../webservice/WebService_COR.asmx/GetUserDetail',
//        contentType: 'application/json; charset=utf-8',
//        dataType: 'json',
//        success: function (response) {
//            var json = $.parseJSON(response.d);
//            json.forEach(function (obj) {
//                document.getElementById("profileImage").src = "../../attachment/avatar/" + obj.UserAvatar
//                var yourImg = document.getElementById('profileImage');
//                if (yourImg && yourImg.style) {
//                    yourImg.style.height = '95px';
//                    yourImg.style.width = '95px';
//                }
//            })

//        },
//        error: function (xhr, ajaxOptions, thrownError) {
//        }
//    });
//}

//function ValidateFileUpload(_idPict) {

//    var fuData = document.getElementById('imageUpload');
//    var FileUploadPath = fuData.value;


//    if (FileUploadPath == '') {
//        alert("Please upload an image");

//    } else {
//        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();



//        if (Extension == "gif" || Extension == "png" || Extension == "bmp"
//                    || Extension == "jpeg" || Extension == "jpg") {


//            if (fuData.files && fuData.files[0]) {

//                var size = fuData.files[0].size;

//                //if (size > 9500) {
//                //    alert("Maximum file size exceeds");
//                //    GotoBeforeProfilePict();
//                //    return;
//                //} else {
//                    var reader = new FileReader();

//                    reader.onload = function (e) {
//                        $('#blah').attr('src', e.target.result);
//                    }

//                    reader.readAsDataURL(fuData.files[0]);
//                    saveDataUpload(_idPict, Extension);
//                //}
//            }

//        }


//        else {
//            alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
//            GotoBeforeProfilePict();
//        }
//    }
//}

//function saveDataUpload(_idPict, Extension)
//{
//    var msg = document.getElementById('lblUserFullName').innerHTML;
//    var _ext = imageUpload.value.split('.')[1];
//    var FileName = _idPict + "." + _ext
//    var filename = $('#imageUpload').get(0);
//    var files = filename.files;
//    var data = new FormData();
//    for (var i = 0; i < files.length; i++) {
//        data.append(FileName, files[i]);

//    }
//    $.confirm({
//        title: "Confirm",
//        content: "Are You Sure To Change Profile Picture",
//        type: 'blue',
//        typeAnimated: true,
//        buttons: {
//            YES: {
//                text: 'YES',
//                btnClass: 'btn-red',
//                action: function () {
//                    $.ajax({
//                        type: 'POST',
//                        url: '../../Service/ImportHandler.ashx?w=10&x=10',
//                        data: data,
//                        contentType: false,
//                        processData: false,

//                        success: function (response) {
//                            $.ajax({
//                                type: 'POST',
//                                url: '../../webservice/WebService_COR.asmx/SaveProfilePict',
//                                data: "{'FileName' :'" + msg + "','Extension' :'" + Extension + "'}",
//                                contentType: 'application/json; charset=utf-8',
//                                dataType: 'json',
//                                success: function (response) {
//                                    $saveButton.html("Save");
//                                    if (response.d == 'success') {
//                                        history.go(0); //forward
//                                        _setCustomFunctions.showPopup('Info', 'Profile Pict has been change successfully');
//                                    } else {
//                                        //_setCustomFunctions.showPopup('Info', 'Failed to save');
//                                    }
//                                },
//                                error: function (xhr, ajaxOptions, thrownError) {
//                                }
//                            });
//                        }

//                    });
//                }
//            },
//            NO: {
//                text: 'NO',
//                btnClass: 'btn-blue',
//                action: function () {
//                    GotoBeforeProfilePict();
//                }
//            }
//        }
//    });
//}