﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddKabupaten',
            saveButton: '#btnSave',
            UpdateButton: '#btnUpdate',
            modalAdd: '#mdlAdd',
            tabKabupaten: '#tabKabupaten',
            table: '#tblkabupaten',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $UpdateButton = $(this.options.UpdateButton);
            $modalAdd = $(this.options.modalAdd);
            $tabKabupaten = $(this.options.tabKabupaten);
            $table = $(this.options.table);
            _formAdd = this.options.formAdd;
            _tabKabupaten = this.options.tabKabupaten;
            _listKabupaten = [];
            _listKabupatenCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 3,
                    width: 200
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 4,
                    width: 300
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 5,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 6,
                    width: 10
                },],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetKabupaten_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblkabupaten").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $table.on('click', 'a[data-target="#delete"]', this.delete);

                $tabKabupaten.on('click', 'a', this.tabclick);
                //this.formvalidation();

            },
            formvalidation: function () {
                //Add On For Validation Rule
                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 2,
                            required: true,
                            numeric: true,
                            remote: function () {
                                //document.getElementById('errormsg').style.display = 'none';
                                return {
                                    url: "../../webservice/WebService_COR.asmx/GetKabupatenDetail",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: "{'KabCode' :'" + $('input[name=code]').val() + "'}",
                                    dataFilter: function (data) {
                                        var msg = JSON.parse(data);
                                        var json = $.parseJSON(msg.d);
                                        if (json.length > 0) {
                                            //{
                                            document.getElementById('lblcode').style.color = '#dd4b39';
                                            document.getElementById('code').style.borderColor = '#dd4b39';
                                            //document.getElementById('help-block').style.display = 'block';
                                            document.getElementById('errormsg').style.display = 'block';
                                            document.all('errormsg').innerHTML = "Code sudah terdaftar pada sistem";
                                        }
                                    }
                                }


                            },
                        },
                        name: {
                            minlength: 1,
                            required: true,
                            alpha: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                //e.preventDefault();

            },
            tabchange: function (tabId) {


            },
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                $('#SelectProvDesc').attr("disabled", false);
                $('input[name=code]').prop('readonly', false);
                document.getElementById('lblcode').style.color = '#333';
                document.getElementById('code').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                document.getElementById('errormsg').style.display = 'none';
                document.all('lbl').innerHTML = "Add Kabupaten";

                $('input[name=code]').val('');
                $('input[name=codeHidden]').val('');
                $('input[name=name]').val('');
                $('#SelectProvDesc').val('');
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';

                _setEvents.formvalidation();
            },
            edit: function () {
                document.getElementById('lblcode').style.color = '#333';
                document.getElementById('code').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'inline';
                document.getElementById('btnSave').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Kabupaten";

                $modalAdd.modal('toggle');
                _setEvents.formvalidation();

                var $this = $(this);
                var $row = $(this).closest('tr');
                var KabCode = $row.find('.KabCode').html();
                var KabupatenCode = KabCode.replace(/\s/g, "");

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetKabupatenDetail',
                    type: "POST",
                    data: "{'KabCode' :'" + KabupatenCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=code]').val(obj.KabCode);
                            $('input[name=codeHidden]').val(obj.KabCode);
                            $('input[name=name]').val(obj.KabDesc);
                            $('#SelectProvDesc').val(obj.ProvCode);

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });
                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = { KabCode: $('input[name=codeHidden]').val(), NewKabCode: $('input[name=code]').val(), KabDesc: $('input[name=name]').val(), ProvCode: $('#SelectProvDesc').val() };
                //var savetype = 
                _setCustomFunctions.EditKabupaten(data);

                return false;

            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var KabCode = $row.find('.KabCode').html();
                var KabupatenCode = KabCode.replace(/\s/g, "");
                //Set Ajax Submit Here
                var data = { KabCode: KabupatenCode };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data, 'confirm', 'Are You Sure Delete This Data?');

                return false;
            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { KabCode: $('input[name=code]').val(), KabDesc: $('input[name=name]').val(), ProvCode: $('#SelectProvDesc').val() };
                    //var savetype = 
                    _setCustomFunctions.saveKabupaten(data);

                    return false;
                }
            }
        },
        setCustomFunctions: {
            init: function () {
                //this.getKabupaten();
                this.getMasterProvinsi();
                //this.getMasterModuleType();
            },
            registerTabContent: function (tabId, KabCode) {
            },
            getKabupaten: function () {

            },
            getMasterProvinsi: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterProvince',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectProvDesc').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectProvDesc").append($('<option>', {
                                value: obj.ProvCode,
                                text: obj.ProvCode + ' - ' + obj.ProvDesc
                            }));
                        })

                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            //getMasterModuleType: function () {

            //},
            //getMasterModuleSubType: function (type) {

            //},
            saveKabupaten: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveKabupaten',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            //_setCustomFunctions.getKabupaten();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Kabupaten has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteKabupaten: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteKabupaten',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Kabupaten has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditKabupaten: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditDataKabupaten',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $UpdateButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Kabupaten has been Update successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });
            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.DeleteKabupaten(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
})