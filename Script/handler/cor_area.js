﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddArea',
            addGMMButton: '#btnAddAreaGMM',
            saveButton: '#btnSave',
            saveGMMButton: '#btnSaveGMM',
            UpdateButton: '#btnUpdate',
            UpdateGMMButton: '#btnUpdateGMM',
            modalAdd: '#mdlAdd',
            modalAddGMM: '#mdlAddGMM',
            tabArea: '#tabArea',
            tabAreaGMM: '#tabAreaGMM',
            tableArea: '#tblArea',
            tableAreaPH: '#tbluserPH',
            tableAreaRH: '#tbluserRH',
            tableAreaGEM: '#tbluserGEM',
            tableAreaGMM: '#tblAreaGMM',
        },
        initialize: function () {
            this.setVars();
            this.setTableAreaRO();
            this.setTableAreaPH();
            this.setTableAreaRH();
            this.setTableAreaGEM();
            this.setTableAreaGMM();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $addGMMButton = $(this.options.addGMMButton);
            $saveButton = $(this.options.saveButton);
            $saveGMMButton = $(this.options.saveGMMButton);
            $UpdateButton = $(this.options.UpdateButton);
            $UpdateGMMButton = $(this.options.UpdateGMMButton);
            $modalAdd = $(this.options.modalAdd);
            $modalAddGMM = $(this.options.modalAddGMM);
            $tabArea = $(this.options.tabArea);
            $tableArea = $(this.options.tableArea);
            $tableAreaPH = $(this.options.tableAreaPH);
            $tableAreaRH = $(this.options.tableAreaRH);
            $tableAreaGEM = $(this.options.tableAreaGEM);
            $tableAreaGMM = $(this.options.tableAreaGMM);
            _formAdd = this.options.formAdd;
            _tabArea = this.options.tabArea;
            _listArea = [];
            _listAreaCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTableAreaRO: function () {
            //Initialization of main table or data here
            var tableArea = $tableArea.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 5,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[3, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetArea_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblArea").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setTableAreaPH: function () {
            //Initialization of main table or data here
            var tableAreaPH = $tableAreaPH.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "autoWidth": false,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: "5%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                    width: "10%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2,
                    width: "85%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: "10%"
                }],
                "orderClasses": false,
                "order": [[3, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetAreaPH_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tbluserPH").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setTableAreaRH: function () {
            //Initialization of main table or data here
            var tableAreaRH = $tableAreaRH.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "autoWidth": false,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: "5%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                    width: "10%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2,
                    width: "85%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: "10%"
                }],
                "orderClasses": false,
                "order": [[3, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetAreaRH_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tbluserRH").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setTableAreaGEM: function () {
            //Initialization of main table or data here
            var tableAreaGEM = $tableAreaGEM.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "autoWidth": false,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: "5%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                    width: "10%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2,
                    width: "85%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: "10%"
                }],
                "orderClasses": false,
                "order": [[3, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetAreaGEM_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tbluserGEM").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setTableAreaGMM: function () {
            //Initialization of main table or data here
            var tableAreaGMM = $tableAreaGMM.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "autoWidth": false,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: "5%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                    width: "10%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2,
                    width: "85%"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: "10%"
                }],
                "orderClasses": false,
                "order": [[3, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetAreaGMM_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblAreaGMM").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $addGMMButton.on('click', this.addGMM);
                $saveButton.on('click', this.save);
                $saveGMMButton.on('click', this.saveGMM);
                $UpdateButton.on('click', this.saveEdit);
                $UpdateGMMButton.on('click', this.saveEditGMM);
                $tableArea.on('click', 'a[data-target="#edit"]', this.edit);
                $tableArea.on('click', 'a[data-target="#delete"]', this.delete);
                $tableAreaGMM.on('click', 'a[data-target="#edit"]', this.editGMM);
                $tableAreaGMM.on('click', 'a[data-target="#delete"]', this.deleteGMM);


                $tabArea.on('click', 'a', this.tabclick);
                //this.formvalidation();

                //select event
                $('#selectType').on('change', this.selectchange);
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");

                //Main Validation
                //    $formAdd.validate({
                //        rules: {
                //            code: {
                //                minlength: 1,
                //                required: true,
                //                remote: function () {
                //                    //document.getElementById('errormsg').style.display = 'none';
                //                    return {
                //                        url: "../../webservice/WebService_COR.asmx/GetMasterAreabyAreaCode",
                //                        type: "POST",
                //                        contentType: "application/json; charset=utf-8",
                //                        dataType: "json",
                //                        data: "{'AreaCode' :'" + $('input[name=code]').val() + "'}",
                //                        dataFilter: function (data) {
                //                            var msg = JSON.parse(data);
                //                            var json = $.parseJSON(msg.d);
                //                            if (json.length > 0) {
                //                                //{
                //                                document.getElementById('lblmaincode').style.color = '#dd4b39';
                //                                document.getElementById('description').style.borderColor = '#dd4b39';
                //                                //document.getElementById('help-block').style.display = 'block';
                //                                document.getElementById('errormsg').style.display = 'block';
                //                                document.all('errormsg').innerHTML = "Code sudah terdaftar pada sistem";
                //                            }
                //                        }
                //                    }


                //                },
                //            },
                //            name: {
                //                minlength: 1,
                //                required: true
                //            },
                //            shortname: {
                //                minlength: 1,
                //                required: true
                //            },
                //        },
                //        highlight: function (element) {
                //            $(element).closest('.form-group').addClass('has-error');
                //        },
                //        unhighlight: function (element) {
                //            $(element).closest('.form-group').removeClass('has-error');
                //        },
                //        errorElement: 'span',
                //        errorClass: 'help-block',
                //        errorPlacement: function (error, element) {
                //            if (element.parent('.input-group').length) {
                //                error.insertAfter(element.parent());
                //            } else {
                //                error.insertAfter(element);
                //            }
                //        }
                //    });
            },
            //resetformvalidation: function () {
            //    $('.form-group').removeClass('has-error has-feedback');
            //    $('.form-group').find('span.help-block').hide();
            //    $('.form-group').find('i.form-control-feedback').hide();
            //},
            //tabclick: function () {
            //    //e.preventDefault();

            //},
            //tabchange: function (tabId) {


            //},
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                console.log("ada");
                //Event for adding data
                $modalAdd.modal('toggle');
                //document.getElementById('lblmaincode').style.color = '#333';
                //document.getElementById('maincode').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                //document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Area";


                $('#SelectArea').val("");
                $('#SelectArea').selectpicker('refresh');
                $('#SelectEstate').val("");
                $('#SelectEstate').selectpicker('refresh');
                //$('input[name=maincode]').val('');
                $('input[name=description]').val('');
                $('input[name=level]').val('');
                //_setEvents.formvalidation();
            },
            edit: function () {
                //document.getElementById('lblmaincode').style.color = '#333';
                //document.getElementById('maincode').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                //document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'inline';
                document.getElementById('btnSave').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Area";
                //$("#maincode").prop("disabled", true);

                $modalAdd.modal('toggle');
                _setEvents.formvalidation();

                var $this = $(this);
                var $row = $(this).closest('tr');
                var Id = $row.find('.Id').html();

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetMasterAreaRO',
                    type: "POST",
                    data: "{'Id' :'" + Id + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            //$('#area').val(obj.Area);
                            $('#SelectArea').val(obj.Area);
                            $('#SelectArea').selectpicker('refresh');
                            $('#Id').val(obj.Id);
                            //$('#Selectstatus').val(AreaStatus).attr("selected", "selected");
                            var optArr = obj.EstCode.split(",");
                            //let optArr = [];
                            for (var i = 0; i < data.length; i++) {
                                optArr.push(data[i].Skill.Id);

                            }
                            $('#SelectEstate').val(optArr);
                            $('#SelectEstate').selectpicker('refresh');


                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = {
                    Id: $('input[name=Id]').val(), area: $('#SelectArea option:selected').val(), Estate: $('#SelectEstate').val().toString()
                };
                //var savetype = 
                _setCustomFunctions.EditArea(data);

                return false;

            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var Id = $row.find('.Id').html();
                //Set Ajax Submit Here
                var data = { Id: Id };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(1, data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = {
                        area: $('#SelectArea option:selected').val(), Estate: $('#SelectEstate').val().toString()
                    };
                    //var savetype = 
                    _setCustomFunctions.saveArea(data);
                    $modalAdd.modal('hide');

                    return false;
                }
            },
            addGMM: function () {
                console.log("ada");
                //Event for adding data
                $modalAddGMM.modal('toggle');
                //document.getElementById('lblmaincode').style.color = '#333';
                //document.getElementById('maincode').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                //document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdateGMM').style.display = 'none';
                document.getElementById('btnSaveGMM').style.display = 'inline';
                document.all('lblGMM').innerHTML = "Add Area GMM";


                $('#SelectAreaGMM').val("");
                $('#SelectAreaGMM').selectpicker('refresh');
                $('#SelectEstateGMM').val("");
                $('#SelectEstateGMM').selectpicker('refresh');
            },
            editGMM: function () {
                //document.getElementById('lblmaincode').style.color = '#333';
                //document.getElementById('maincode').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                //document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdateGMM').style.display = 'inline';
                document.getElementById('btnSaveGMM').style.display = 'none';
                document.all('lblGMM').innerHTML = "Edit Area GMM";
                //$("#maincode").prop("disabled", true);

                $modalAddGMM.modal('toggle');
                _setEvents.formvalidation();

                var $this = $(this);
                var $row = $(this).closest('tr');
                var Id = $row.find('.Id').html();

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetMasterAreaGMM',
                    type: "POST",
                    data: "{'Id' :'" + Id + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            //$('#area').val(obj.Area);
                            $('#SelectAreaGMM').val(obj.Area);
                            $('#SelectAreaGMM').selectpicker('refresh');
                            $('#Id').val(obj.Id);
                            //$('#Selectstatus').val(AreaStatus).attr("selected", "selected");
                            var optArr = obj.EstCode.split(",");
                            //let optArr = [];
                            for (var i = 0; i < data.length; i++) {
                                optArr.push(data[i].Skill.Id);

                            }
                            $('#SelectEstateGMM').val(optArr);
                            $('#SelectEstateGMM').selectpicker('refresh');


                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEditGMM: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = {
                    Id: $('input[name=Id]').val(), area: $('#SelectAreaGMM option:selected').val(), Estate: $('#SelectEstateGMM').val().toString()
                };
                //var savetype = 
                _setCustomFunctions.EditAreaGMM(data);

                return false;

            },
            deleteGMM: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var Id = $row.find('.Id').html();
                //Set Ajax Submit Here
                var data = { Id: Id };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(2, data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            saveGMM: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = {
                        area: $('#SelectAreaGMM option:selected').val(), Estate: $('#SelectEstateGMM').val().toString()
                    };
                    //var savetype = 
                    _setCustomFunctions.saveAreaGMM(data);
                    $modalAddGMM.modal('hide');

                    return false;
                }
            }
        },
        setCustomFunctions: {
            init: function () {
                this.getArea();
                this.getAreaGMM();
                this.getEstate();
            },
            registerTabContent: function (tabId, mdlcode) {
            },
            getArea: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/getMasterAreaROHCIS',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectArea').find('option').remove();
                        json.forEach(function (obj) {
                            //if (obj.mainCode == $("#SelectMainJabatan option:selected").val()) {
                            $("#SelectArea").append($('<option>', {
                                value: obj.Nama_OU,
                                text: obj.Nama_OU
                            }));
                            //}
                        })

                        $('#SelectArea').selectpicker('refresh');
                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getAreaGMM: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/getMasterAreaGMMHCIS',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectAreaGMM').find('option').remove();
                        json.forEach(function (obj) {
                            //if (obj.mainCode == $("#SelectMainJabatan option:selected").val()) {
                            $("#SelectAreaGMM").append($('<option>', {
                                value: obj.Nama_OU,
                                text: obj.PomDesc
                            }));
                            //}
                        })

                        $('#SelectArea').selectpicker('refresh');
                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getEstate: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/getMasterEstateCode',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectEstate').find('option').remove();
                        json.forEach(function (obj) {
                            //if (obj.mainCode == $("#SelectMainJabatan option:selected").val()) {
                            $("#SelectEstate").append($('<option>', {
                                value: obj.estcode,
                                text: obj.NewEstName
                            }));
                            //}
                        })

                        $('#SelectEstate').selectpicker('refresh');
                        $('#SelectEstateGMM').find('option').remove();
                        json.forEach(function (obj) {
                            //if (obj.mainCode == $("#SelectMainJabatan option:selected").val()) {
                            $("#SelectEstateGMM").append($('<option>', {
                                value: obj.estcode,
                                text: obj.NewEstName
                            }));
                            //}
                        })

                        $('#SelectEstateGMM').selectpicker('refresh');
                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveArea: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveArea',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableArea.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Area RO has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteArea: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteArea',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableArea.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Area RO has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditArea: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditArea',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $UpdateButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableArea.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Area RO has been Update successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveAreaGMM: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveAreaGMM',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableAreaGMM.DataTable().ajax.reload();
                            $modalAddGMM.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Area GMM has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteAreaGMM: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteAreaGMM',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableAreaGMM.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Area GMM has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditAreaGMM: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditAreaGMM',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $UpdateGMMButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableAreaGMM.DataTable().ajax.reload();
                            $modalAddGMM.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Area GMM has been Update successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (type, data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                if (type == 1) {
                                    _setCustomFunctions.DeleteArea(data);
                                }
                                else {
                                    _setCustomFunctions.DeleteAreaGMM(data);
                                }
                                
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
})