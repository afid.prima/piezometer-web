﻿$(function () {
    var totalRow;
    var _dataTree = [];
    var _globaluserID;
    var Page = {
        options: {
            //formAdd: '#formAdd',
            //addButton: '#btnAddCountry',
            searchButton: '#btnSearch',
            selectFilterPlantation: '#SelectFilterPlantation',
            selectFilterCompany: '#SelectFilterCompany',
            selectFilterEstate: '#SelectFilterEstate',
            selectFilterGroupCompany: '#SelectFilterGroupCompany',
            modalAdd: '#mdlAdd',
            table: '#tbldevice',
            saveButton: '#btnSave',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            //$formAdd = $(this.options.formAdd);
            //$addButton = $(this.options.addButton);
            //$saveButton = $(this.options.saveButton);
            $searchButton = $(this.options.searchButton);
            $SelectFilterCompany = $(this.options.selectFilterCompany);
            $SelectFilterPlantation = $(this.options.selectFilterPlantation);
            $SelectFilterEstate = $(this.options.selectFilterEstate);
            $SelectFilterGroupCompany = $(this.options.selectFilterGroupCompany);
            $modalAdd = $(this.options.modalAdd);
            $table = $(this.options.table);
            $saveButton = $(this.options.saveButton);
            //_formAdd = this.options.formAdd;
            //_tabCountry = this.options.tabCountry;
            //_listCountry = [];
            //_listCountryCode = [];
            //_isEditTabExists = false;
            //_currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'lfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "details-control",
                    "data": null,
                    "defaultContent": '',
                    "targets": 1,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 3
                }, {
                    "searchable": false,
                    "orderable": true,
                    "class": "center",
                    "targets": 5,
                    width: 15
                }, {
                    "searchable": false,
                    "orderable": true,
                    "class": "center",
                    "targets": 6,
                    width: 15
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 7
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 8
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetDevice_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ 'name': 'GroupCompanyCode', 'value': $SelectFilterGroupCompany.val() });
                    aoData.push({ 'name': 'PlantationCode', 'value': $SelectFilterPlantation.val() });
                    aoData.push({ 'name': 'CompanyCode', 'value': $SelectFilterCompany.val() });
                    aoData.push({ 'name': 'EstCode', 'value': $SelectFilterEstate.val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            totalRow = json.recordsTotal;
                            fnCallback(json);
                            $table.show();
                            $(".details-control").on('click', _setEvents.detailmodule);
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });

        },
        setEvents: {
            init: function () {
                $SelectFilterPlantation.on('change', this.onChangePlantation);
                $SelectFilterGroupCompany.on('change', this.onChangeGroupCompany);
                $SelectFilterCompany.on('change', this.onChangeCompany);
                $searchButton.on('click', this.SearchUser);
                $saveButton.on('click', this.save);
            },
            SearchUser: function () {
                $table.DataTable().ajax.reload();
            },
            //ChangePlantation: function () {
            //    $("#tbodyid tr").remove();
            //    $SelectFilterCompany.val('');
            //    var paramID = this.value;
            //    _setCustomFunctions.getGroupCompanyByPlantation(paramID);
            //},
            onChangePlantation: function () {
                var opunit = '';
                if ($SelectFilterPlantation.val() != null) {
                $SelectFilterPlantation.val().forEach(function (opt) {
                    if (opunit === '')
                        opunit += opt;
                    else
                        opunit += ',' + opt;
                })
                }

                _setCustomFunctions.getMasterGroupCompany(opunit);
            },
            onChangeGroupCompany: function () {
                //$("#tbodyid tr").remove();
                //$SelectFilterEstate.val('');
                //var paramID = this.value;
                var opunit = '';
                if ($SelectFilterGroupCompany.val() != null) {
                    $SelectFilterGroupCompany.val().forEach(function (opt) {
                        if (opunit === '')
                            opunit += opt;
                        else
                            opunit += ',' + opt;
                    })
                }
                _setCustomFunctions.getCompanyByGroupCompany(opunit);
            },
            onChangeCompany: function () {
                //$("#tbodyid tr").remove();
                //$SelectFilterEstate.val('');
                //var paramID = this.value;
                var opunit = '';
                if ($SelectFilterCompany.val() != null) {
                    $SelectFilterCompany.val().forEach(function (opt) {
                        if (opunit === '')
                            opunit += opt;
                        else
                            opunit += ',' + opt;
                    })
                }
                _setCustomFunctions.getEstateByCompany(opunit);
            },
            detailmodule: function () {
                var tr = $(this).closest('tr');
                var row = $table.DataTable().row(tr);
                var UserID = tr.find('.UserID').html();

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    _setCustomFunctions.format(row.child, UserID)
                    tr.addClass('shown');
                }
            },
            save: function () {
                _setCustomFunctions.SaveEditUser(_dataTree);
            },
        },
        setCustomFunctions: {
            init: function () {
                this.getPlantation();
                this.getMasterGroupCompany("");
                this.getCompanyByGroupCompany("");
                this.getEstateByCompany("");
                //this.getMasterModuleType();
            },

            SaveEditUser: function (_dataTree) {
                var datasw = { userID: _globaluserID, ListbasemapAccess: _dataTree };
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/UpdateUserBaseMapAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(datasw) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            //$table.DataTable().ajax.reload();
                            $modalAdd.modal('hide');

                            _setCustomFunctions.showPopup('Info', 'User has been saved successfully');
                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getPlantation: function () {
                $SelectFilterPlantation.val('');
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterPlantationActive',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        if (json.length > 0) {
                            $SelectFilterPlantation.find('option').remove();
                            //var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {

                                option = $('<option>', {
                                    value: obj.PLANTATION,
                                    text: obj.PLANTATION
                                });

                                option.appendTo($SelectFilterPlantation);
                            });

                            $SelectFilterPlantation.selectpicker('refresh');
                        }
                    }
                })
            },
            getMasterGroupCompany: function (paramID) {
                //$SelectFilterGroupCompany.val('');
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetGroupCompanyByPlantation',
                    contentType: 'application/json; charset=utf-8',
                    data: "{data: '" + paramID + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        if (json.length > 0) {
                            $SelectFilterGroupCompany.find('option').remove();
                            //var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {

                                option = $('<option>', {
                                    value: obj.GroupCompanyName,
                                    text: obj.GroupCompanyName
                                });

                                option.appendTo($SelectFilterGroupCompany);
                            });

                            $SelectFilterGroupCompany.selectpicker('refresh');
                        }
                    }
                })
            },
            getCompanyByPlantation: function (paramID) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetCompanyByPlantation',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify({ data: paramID }),
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        if (json.length > 0) {
                            $SelectFilterCompany.find('option').remove();
                            //var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {

                                option = $('<option>', {
                                    value: obj.CompanyName,
                                    text: obj.CompanyName
                                });

                                option.appendTo($SelectFilterCompany);
                            });

                            $SelectFilterCompany.selectpicker('refresh');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getCompanyByGroupCompany: function (paramID) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetCompanyByGroupCompany',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify({ data: paramID }),
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        if (json.length > 0) {
                            $SelectFilterCompany.find('option').remove();
                            //var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {

                                option = $('<option>', {
                                    value: obj.CompanyName,
                                    text: obj.CompanyName
                                });

                                option.appendTo($SelectFilterCompany);
                            });

                            $SelectFilterCompany.selectpicker('refresh');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getEstateByCompany: function (paramID) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetEstateByCompany',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify({ data: paramID }),
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        if (json.length > 0) {
                            $SelectFilterEstate.find('option').remove();
                            //var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {

                                option = $('<option>', {
                                    value: obj.estCode,
                                    text: obj.NewEstName
                                });

                                option.appendTo($SelectFilterEstate);
                            });

                            $SelectFilterEstate.selectpicker ('refresh');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            format: function (callback, UserID) {
                var contentHTML;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetDetailMobile_ServerSideProcessing',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: JSON.stringify({ data: UserID }),
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        var thead = '', tbody = '', tbutton = '';
                        actionTableExtendToken = [];
                        actionBaseMapAccess = [];
                        var idZ = "add_" + UserID;
                        var idY = "basemap_" + UserID;
                        var _no;
                        var _length;
                        tbutton += '<div class="col-md-12 pull-right">';
                        tbutton += '<div class="col-md-2 pull-right"><button id=' + idZ + ' type="button" class="btn btn-success pull-right" ><span class="glyphicon glyphicon-plus"></span>&nbsp;Extend Token</button></div>';
                        tbutton += '<div class="col-md-2 pull-right"><button id=' + idY + ' type="button" class="btn btn-success pull-right" ><span class="glyphicon glyphicon-plus"></span>&nbsp;Base Map Access</button></div>';
                        tbutton += '</div><br>';
                        actionTableExtendToken.push(idZ);
                        actionBaseMapAccess.push(idY);
                        thead += "<thead>"
                        thead += '<th style="text-align: center;">No</th>';
                        for (var key in json[0]) {
                            if (key == "androidID") {
                                thead += '<th style="text-align: center;display: none">' + key + '</th>';
                            }
                            else {
                                thead += '<th style="text-align: center;">' + key + '</th>';
                            }

                        }
                        thead += '<th style="text-align: center;"></th>';
                        thead += '<th style="text-align: center;">Unregister</th>';
                        thead += "</thead>"
                        if (json.length != 0) {
                            actionTableUnregister = [];
                            _no = json.length;
                            _length = json.length;
                            $.each(json, function (i, d) {
                                var idX = "edit_" + UserID + _no;
                                tbody += '<tr><td>' + _no + '</td><td class="brand" style="text-align: center;">' + d.brand + '</td><td style="text-align: center;">' + d.manufacturer + '</td><td class="deviceRAM" style="text-align: center;">' + d.deviceRAM + '</td><td class="deviceStorage" style="text-align: center;">' + d.deviceStorage + '</td><td class="EXPIRATION_DATE" style="text-align: center;">' + d.EXPIRATION_DATE + '</td><td class="UserStatus" style="text-align: center;">' + d.UserStatus + '</td><td class="androidID" style="display: none">' + d.androidID + '</td><td id="' + UserID + '"></td>'

                                tbody += '<td><a id=' + idX + ' type="button" class="btn btn-danger btn-xs editDetail" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-trash"></span></a></td>'

                                actionTableUnregister.push(idX);
                                _no--;
                            });
                        }
                        else {
                            thead += '<th>No</th><th>brand</th><th>manufacturer</th><th>deviceRAM</th><th>deviceStorage</th><th>EXPIRATION_DATE</th><th>UserStatus</th><th></th><th>Unregister</th>';
                            tbody += '<tr><td colspan="6" style="text-align: center;">No Data Present</td>';
                        }

                        callback($(tbutton + '<table id="tblDetailDevice_' + UserID + '" class="table table-striped table-bordered" style="width:100%;font-size:9pt;">' + thead + tbody + '</table>')).show();

                        _token = [];
                        for (var i = 0 ; i < actionTableExtendToken.length; i++) {
                            $("#" + actionTableExtendToken[i]).click(function () {
                                _token = [];
                                for (var j = 0; j < _length; j++) {
                                    //_setCustomFunctions.showConfirmPopup('aa', 'Info', 'Are You Sure To Unregistered ?');
                                    if ($($("#tblDetailDevice_" + UserID + " tr").parent()[1].childNodes).find(".dt-checkboxes")[j].checked == true) {
                                        _token.push($("#tblDetailDevice_" + UserID + " tr").parent().siblings().find(".androidID")[j].innerHTML);
                                    }
                                }


                                var datas = { UserID: UserID, ListAndroidID: _token };
                                if (_token.length == 0) {
                                    _setCustomFunctions.showPopup('Info', 'Mohon centang dulu');
                                }
                                else {
                                    _setCustomFunctions.showConfirmPopup(1, datas, 'Info', 'Apakah anda yakin untuk Extend Token ?');
                                }


                            });
                        }

                        for (var i = 0 ; i < actionTableUnregister.length; i++) {
                            $("#" + actionTableUnregister[i]).click(function () {
                                var androidID = $(this).parent().siblings().eq(7).text();
                                var datas = { UserID: UserID, androidID: androidID };
                                _setCustomFunctions.showConfirmPopup(2, datas, 'Info', 'Apakah anda yakin untuk Unregistered ?');
                            });
                        }


                        for (var i = 0 ; i < actionBaseMapAccess.length; i++) {
                            $("#" + actionBaseMapAccess[i]).click(function () {
                                $modalAdd.modal('toggle');
                                _setCustomFunctions.setTree(UserID);
                                _globaluserID = UserID;
                            });
                        }


                        $("#tblDetailDevice_" + UserID).DataTable({
                            dom: 'lfrtip',
                            destroy: true,
                            lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                            pageLength: 25,
                            "filter": true,
                            "columnDefs": [{
                                "searchable": false,
                                "orderable": false,
                                "class": "center",
                                "targets": 0,
                                width: 5
                            }, {
                                "searchable": false,
                                "orderable": false,
                                "targets": 1,
                            }, {
                                "searchable": false,
                                "orderable": false,
                                "class": "left",
                                "targets": 2
                            }, {
                                "searchable": false,
                                "orderable": false,
                                "class": "center",
                                "targets": 5
                            }, {
                                "searchable": false,
                                "orderable": false,
                                "class": "center",
                                "targets": 8,
                                'checkboxes': {
                                    'selectRow': true
                                }
                            }, {
                                "searchable": false,
                                "orderable": false,
                                "class": "center",
                                "targets": 9,
                                width: 5
                            }, ],
                            "orderClasses": false,
                            "order": [[1, "asc"]],
                            "info": true
                        });
                    }
                });

            },
            setTree: function (UsersID) {
                var datas = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetBaseMapAccessUser',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: "{'UsersID' :'" + UsersID + "'}",
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0; i < json.length; i++) {
                            datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }
                        var finaldata = "";
                        if (UsersID == 0) {
                            finaldata = json;
                        }
                        else {
                            finaldata = datas;
                        }
                        var $treeview = $("#tvaccess");
                        $('#tvaccess').jstree('destroy');
                        $('#tvaccess').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                'data':
                                finaldata
                            },
                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                        });

                        $('#tvaccess')
                            // listen for event
                            .on('changed.jstree', function (e, data) {
                                _dataTree = data.instance.get_bottom_selected();
                            })
                            // create the instance
                            .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });



            },
            UnregisterDevice: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/UnregisterDevice',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Device has been Unregistered');
                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            ExtendToken: function (data) {
                var _userID = data.UserID;
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/ExtendToken',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            history.go();
                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (param, data, title, content) {
                if (title == "Info") {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    if (param == 1) {
                                        _setCustomFunctions.ExtendToken(data);
                                    }
                                    else if (param == 2) {
                                        _setCustomFunctions.UnregisterDevice(data);
                                    }

                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            OK: {
                                text: 'OK',
                                btnClass: 'btn-blue',
                                action: function () {
                                    $(_formAdd + " :input").prop("disabled", false);
                                    history.go(0); //forward
                                    //_setCustomFunctions.getModule();
                                    //$modalAdd.modal('hide');
                                }
                            }
                        }
                    });

                }

            }
        }

    }

    Page.initialize();
});