﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            formEdit: '#formEdit',
            formAddmapType: '#formAddmapType',
            addButton: '#btnAddMapgroup',
            addSubMapgroupButton: '#btnAddSubMapgroup',
            saveButton: '#btnSave',
            saveMapTypeButton: '#btnSaveMapType',
            UpdateButton: '#btnUpdate',
            UpdateMapTypeButton: '#btnUpdateMapType',
            modalAdd: '#mdlAdd',
            modalAddMapType: '#mdlAddMapType',
            tabMapgroup: '#tabMapgroup',
            table: '#tblmapgroup',
            tablesubdata: '#tblMapType',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $formEdit = $(this.options.formEdit);
            $formAddmapType = $(this.options.formAddmapType);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $saveMapTypeButton = $(this.options.saveMapTypeButton);
            $UpdateButton = $(this.options.UpdateButton);
            $UpdateMapTypeButton = $(this.options.UpdateMapTypeButton);
            $modalAdd = $(this.options.modalAdd);
            $modalAddMapType = $(this.options.modalAddMapType);
            $tabMapgroup = $(this.options.tabMapgroup);
            $table = $(this.options.table);
            $tablesubdata = $(this.options.tablesubdata);
            _formAdd = this.options.formAdd;
            _formAddmapType = this.options.formAddmapType;
            _tabMapgroup = this.options.tabMapgroup;
            _listMapgroup = [];
            _listMapgroupCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetMapGroup_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblmapgroup").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $saveMapTypeButton.on('click', this.saveMapType);
                $UpdateMapTypeButton.on('click', this.saveEditMapType);
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $table.on('click', 'a[data-target="#delete"]', this.delete);

                $tabMapgroup.on('click', 'a', this.tabclick);
                //this.formvalidation();

                //select event
                $('#selectType').on('change', this.selectchange);
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 1,
                            maxlength: 3,
                            required: true,
                            numeric: true
                        },
                        name: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                $formAddmapType.validate({
                    rules: {
                        maptypeid: {
                            minlength: 1,
                            required: true,
                            numeric: true
                        },
                        maptypename: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                $formEdit.validate({
                    rules: {
                        codeEdit: {
                            minlength: 1,
                            maxlength: 3,
                            required: true,
                            numeric: true
                        },
                        nameEdit: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                $(this).tab('show');
            },
            tabchange: function (tabId) {
                $(_tabMapgroup + ' a[href="#' + tabId + '"]').tab('show');
            },
            tabclose: function () {
                $.confirm({
                    title: 'Confirmation',
                    content: 'You are editing something. Are you sure to close existing edit ? ',
                    buttons: {
                        cancel: function () {

                        },
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                _currentEditTabId = "";
                                _isEditTabExists = false;

                                //there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
                                var tabContentId = $(".closeTab").parent().attr("href");
                                $(".closeTab").parent().parent().remove(); //remove li of tab
                                $(_tabMapgroup + ' a:first').tab('show'); // Select first tab
                                $(tabContentId).remove(); //remove respective tab content
                            }
                        }
                    }
                });
            },
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                document.getElementById('btnSave').style.display = 'inline';
                $('input[name=code]').val('');
                $('input[name=codeHidden]').val('');
                $('input[name=name]').val('');


                _setEvents.formvalidation();
            },
            edit: function () {
                document.getElementById('btnUpdate').style.display = 'inline';

                //$modalAdd.modal('toggle');

                var $this = $(this);
                var $row = $(this).closest('tr');
                var MapGroupCode = $row.find('.MapGroupCode').html();
                var MapGroupName = $row.find('.MapGroupName').html();

                $("#tbodyid").empty();

                var tabId = "edit" + MapGroupCode;
                if ($(_tabMapgroup + ' .nav-tabs a[href="#' + tabId + '"]').length) {
                    _setEvents.tabchange(tabId);
                    _setCustomFunctions.setTableMapType($('input[name=codeEditHidden]').val());
                    $(".addmaptype").on('click', _setEvents.addmaptype);
                    $(".editMap").on('click', _setEvents.saveEdit);
                } else {
                    if (_isEditTabExists) {
                        $.confirm({
                            title: 'Confirmation',
                            content: 'You are editing something. Are you sure to close existing edit ? ',
                            buttons: {
                                GoToTabEditing: {
                                    text: 'Go To Editing Tab',
                                    btnClass: 'btn-orange',
                                    action: function () {
                                        _setEvents.tabchange(_currentEditTabId);
                                        _setCustomFunctions.getMapGroup($('input[name=codeEditHidden]').val());
                                        _setCustomFunctions.setTableMapType($('input[name=codeEditHidden]').val());
                                    }
                                },
                                cancel: function () {

                                },
                                OK: {
                                    text: 'OK',
                                    btnClass: 'btn-blue',
                                    action: function () {
                                        //there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
                                        var tabContentId = _currentEditTabId;
                                        $('#tabMapgroup .nav-tabs a[href="#' + tabContentId + '"]').parent().remove(); //remove li of tab
                                        $('#tabMapgroup a:first').tab('show'); // Select first tab
                                        $('#' + tabContentId).remove(); //remove respective tab content


                                        _isEditTabExists = true;
                                        _currentEditTabId = tabId;
                                        $('#tabMapgroup .nav-tabs').append('<li><a href="#' + tabId + '" data-togle="tab"><button class="close closeTab" type="button" >×</button>Edit - ' + MapGroupName + '</a></li>');
                                        $('#tabMapgroup .tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

                                        _setCustomFunctions.registerTabContent(tabId, MapGroupCode);
                                        _setEvents.tabchange(tabId);
                                        _setCustomFunctions.getMapGroup(MapGroupCode);
                                        _setCustomFunctions.setTableMapType(MapGroupCode);
                                        $(".addmaptype").on('click', _setEvents.addmaptype);
                                        $(".editMap").on('click', _setEvents.saveEdit);
                                        $(".closeTab").on('click', _setEvents.tabclose);
                                    }
                                }
                            }
                        });
                    } else {
                        _isEditTabExists = true;
                        _currentEditTabId = tabId;
                        $('#tabMapgroup .nav-tabs').append('<li><a href="#' + tabId + '" data-togle="tab"><button class="close closeTab" type="button" >×</button>Edit - ' + MapGroupName + '</a></li>');
                        $('#tabMapgroup .tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

                        _setCustomFunctions.registerTabContent(tabId, MapGroupCode);
                        _setEvents.tabchange(tabId);
                        _setCustomFunctions.getMapGroup(MapGroupCode);
                        _setCustomFunctions.setTableMapType(MapGroupCode);
                        $(".addmaptype").on('click', _setEvents.addmaptype);
                        $(".editMap").on('click', _setEvents.saveEdit);
                        $(".closeTab").on('click', _setEvents.tabclose);
                    }
                }


                _setEvents.formvalidation();

                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                if ($formAdd.valid()) {
                    //Event for deleting data
                    var $this = $(this);
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { MapGroupCode: $('input[name=codeEditHidden]').val(), NewMapGroupCode: $('input[name=codeEdit]').val(), MapGroupName: $('input[name=nameEdit]').val() };
                    //var savetype = 
                    _setCustomFunctions.EditMapgroup(data);

                    return false;
                }

            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var MapGroupCode = $row.find('.MapGroupCode').html();
                //Set Ajax Submit Here
                var data = { MapGroupCode: MapGroupCode };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(1, data, 'confirm', 'Are You Sure Delete This Data?');

                return false;

            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { MapGroupCode: $('input[name=code]').val(), MapGroupName: $('input[name=name]').val() };
                    //var savetype = 
                    _setCustomFunctions.saveMapgroup(data);

                    return false;
                }
            },

            saveMapType: function () {
                //Event for deleting data
                if ($formAddmapType.valid()) {
                    var $this = $(this);
                    $("#formAddmapType :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    //Set Ajax Submit Here
                    var data = {
                        MapTypeID: $('input[name=maptypeid]').val(), MapTypeName: $('input[name=maptypename]').val(), MapGroupCode: $('input[name=codeEdit]').val()
                    };
                    _setCustomFunctions.saveMapType(data);

                    return false;
                }


            },
            saveEditMapType: function () {
                //Event for deleting data
                var $this = $(this);
                $("#formAddmapType :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = {
                    NewMapTypeID: $('input[name=maptypeid]').val(), MapTypeID: $('input[name=maptypeidHidden]').val(), MapTypeName: $('input[name=maptypename]').val(), MapGroupCode: $('input[name=codeEdit]').val()
                };
                //var savetype = 
                _setCustomFunctions.EditMapType(data);

                //return false;

            },

            addmaptype: function () {
                document.getElementById('btnUpdateMapType').style.display = 'none';
                document.getElementById('btnSaveMapType').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Map Type";
                //Event for adding data
                //$modalAddSubDataGroup.modal('toggle');
                $modalAddMapType.modal('toggle');
                $('input[name=maptypeid]').val('');
                $('input[name=maptypeidHidden]').val('');
                $('input[name=maptypename]').val('');

                _setEvents.formvalidation();
            },
            editMapType: function () {
                document.getElementById('btnUpdateMapType').style.display = 'inline';
                document.getElementById('btnSaveMapType').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Map Type";
                var $this = $(this);
                var $row = $(this).closest('tr');
                var MapTypeID = $row.find('.maptypeid').html();
                $modalAddMapType.modal('toggle');

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetMapType',
                    type: "POST",
                    data: "{'MapTypeID' :'" + MapTypeID + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=maptypeid]').val(obj.MapTypeID);
                            $('input[name=maptypeidHidden]').val(obj.MapTypeID);
                            $('input[name=maptypename]').val(obj.MapTypeName);
                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

            },

            deleteMapType: function () {
                var $this = $(this);
                var $row = $(this).closest('tr');
                var maptypeid = $row.find('.maptypeid').html();

                var data = { maptypeid: maptypeid };

                _setCustomFunctions.showConfirmPopup(2, data, 'confirm', 'Are You Sure Delete This Data Sub Data?');

                return false;
            },
        },
        setCustomFunctions: {
            init: function () {
            },
            getSubdata: function (MapGroupCode) {
            },
            setTableMapType: function (MapGroupCode) {
                $.ajax({
                    type: "POST",
                    url: "../../webservice/WebService_COR.asmx/GetMapType_ServerSideProcessing",
                    data: JSON.stringify({ data: MapGroupCode }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //for (var i = 0; i < data.length; i++) {
                        var json = $.parseJSON(data.d);
                        var tables = $('#tblmaptype');
                        json.forEach(function (obj) {
                            tables.append("<tr class='maptype'><td class='maptypeid'>" + obj.MapTypeID + "</td><td class='maptypename'>" + obj.MapTypeName + "</td><td align='center'>" + "<a type='button' class='btn btn-primary btn-xs edittype' data-title='EditSubDataGroup' data-toggle='modal'><span class='glyphicon glyphicon-pencil'></span></a>" + "</td><td align='center'>" + "<a type='button' class='btn btn-danger btn-xs deletetype' data-title='DeleteSubDataGroup' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></a>" + "</td></tr>");
                        })
                        //console.log(data.d[i].ipgroup);
                        $(".edittype").on('click', _setEvents.editMapType);
                        $(".deletetype").on('click', _setEvents.deleteMapType);


                        //}
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            },
            registerTabContent: function (tabId, MapGroupCode) {
                $('.divtemplate').clone().appendTo($('#' + tabId));
                $('#' + tabId + " .divtemplate").css('display', 'block');
            },
            getMapGroup: function (MapGroupCode) {
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetMapGroup',
                    type: "POST",
                    data: "{'MapGroupCode' :'" + MapGroupCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=codeEdit]').val(obj.MapGroupCode);
                            $('input[name=codeEditHidden]').val(obj.MapGroupCode);
                            $('input[name=nameEdit]').val(obj.MapGroupName);

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });
            },
            saveMapgroup: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveMapgroup',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Mapgroup has been saved successfully');
                            $modalAdd.modal('hide');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteMapgroup: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteMapgroup',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Mapgroup has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditMapgroup: function (data) {
                var regexNum = /\d/;
                var regexLetter = /[a-zA-z]/;
                if (data.NewMapGroupCode !== "" && data.MapGroupName !== "") {
                    if (regexNum.test(data.NewMapGroupCode)) {
                        $.ajax({
                            type: 'POST',
                            url: '../../webservice/WebService_COR.asmx/EditMapgroup',
                            data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            success: function (response) {
                                $UpdateButton.html("Update");
                                if (response.d == 'success') {
                                    $(_formAdd + " :input").prop("disabled", false);
                                    history.go(0); //forward
                                    _setCustomFunctions.showPopup('Info', 'Mapgroup has been Update successfully');
                                    $modalAdd.modal('hide');
                                } else {
                                    $(_formAdd + " :input").prop("disabled", false);
                                    _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                            }
                        });
                    }
                    else {
                        alert('Check Field');
                    }
                }
                else {
                    alert('Empty Field');
                }

            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }

                        }
                    }
                });
            },
            showConfirmPopup: function (param, data, title, content) {
                if (param == 1) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeleteMapgroup(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeleteMapType(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }


            },
            saveMapType: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/saveMapType',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAddmapType + " :input").prop("disabled", false);
                            history.go(0); //forward
                            _setCustomFunctions.showPopup('Info', 'Map Type has been Save successfully');
                        } else {
                            $(_formAddmapType + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditMapType: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditMapType',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            //history.go(0); //forward
                            var table = document.getElementById("tblmaptype");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAddmapType + " :input").prop("disabled", false);
                            _setCustomFunctions.setTableMapType($('input[name=codeEdit]').val());
                            $modalAddMapType.modal('hide');
                            //EditMapType.preventDefault();
                            _setCustomFunctions.showPopup('Info', 'Map Type has been Update successfully');
                            return false;
                        } else {
                            //$(_formAddmapType + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                            var table = document.getElementById("tblmaptype");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAddmapType + " :input").prop("disabled", false);
                            _setCustomFunctions.setTableMapType($('input[name=codeEdit]').val());
                            $modalAddMapType.modal('hide');
                            //EditMapType.preventDefault();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteMapType: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteMapType',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAddmapType + " :input").prop("disabled", false);
                            //$('#tblSubdatagroup').ajax.reload();
                            //var tables = $('#tblSubdatagroup');
                            //history.go(0); //forward
                            var table = document.getElementById("tblmaptype");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAddmapType + " :input").prop("disabled", false);
                            _setCustomFunctions.setTableMapType($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'MapType has been Delete successfully');
                        } else {
                            $(_formAddmapType + " :input").prop("disabled", false);
                            var table = document.getElementById("tblmaptype");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            _setCustomFunctions.setTableMapType($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
        }

    }

    Page.initialize();
})