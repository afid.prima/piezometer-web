﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddAccess',
            saveButton: '#btnSave',
            UpdateButton: '#btnUpdate',
            modalAdd: '#mdlAdd',
            tabAccess: '#tabAccess',
            tableAccess: '#tblAccess',
        },
        initialize: function () {
            this.setVars();
            this.setTableAccess();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $UpdateButton = $(this.options.UpdateButton);
            $modalAdd = $(this.options.modalAdd);
            $tabAccess = $(this.options.tabAccess);
            $tableAccess = $(this.options.tableAccess);
            _formAdd = this.options.formAdd;
            _tabAccess = this.options.tabAccess;
            _listAccess = [];
            _listAccessCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTableAccess: function () {
            //Initialization of main table or data here
            var tableAccess = $tableAccess.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 5,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[3, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetAccess_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblAccess").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $tableAccess.on('click', 'a[data-target="#edit"]', this.edit);
                $tableAccess.on('click', 'a[data-target="#delete"]', this.delete);


                $tabAccess.on('click', 'a', this.tabclick);
                //this.formvalidation();

                //select event
                $('#selectType').on('change', this.selectchange);
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");

                //Main Validation
                //    $formAdd.validate({
                //        rules: {
                //            code: {
                //                minlength: 1,
                //                required: true,
                //                remote: function () {
                //                    //document.getElementById('errormsg').style.display = 'none';
                //                    return {
                //                        url: "../../webservice/WebService_COR.asmx/GetMasterAccessbyAccessCode",
                //                        type: "POST",
                //                        contentType: "application/json; charset=utf-8",
                //                        dataType: "json",
                //                        data: "{'AccessCode' :'" + $('input[name=code]').val() + "'}",
                //                        dataFilter: function (data) {
                //                            var msg = JSON.parse(data);
                //                            var json = $.parseJSON(msg.d);
                //                            if (json.length > 0) {
                //                                //{
                //                                document.getElementById('lblmaincode').style.color = '#dd4b39';
                //                                document.getElementById('description').style.borderColor = '#dd4b39';
                //                                //document.getElementById('help-block').style.display = 'block';
                //                                document.getElementById('errormsg').style.display = 'block';
                //                                document.all('errormsg').innerHTML = "Code sudah terdaftar pada sistem";
                //                            }
                //                        }
                //                    }


                //                },
                //            },
                //            name: {
                //                minlength: 1,
                //                required: true
                //            },
                //            shortname: {
                //                minlength: 1,
                //                required: true
                //            },
                //        },
                //        highlight: function (element) {
                //            $(element).closest('.form-group').addClass('has-error');
                //        },
                //        unhighlight: function (element) {
                //            $(element).closest('.form-group').removeClass('has-error');
                //        },
                //        errorElement: 'span',
                //        errorClass: 'help-block',
                //        errorPlacement: function (error, element) {
                //            if (element.parent('.input-group').length) {
                //                error.insertAfter(element.parent());
                //            } else {
                //                error.insertAfter(element);
                //            }
                //        }
                //    });
            },
            //resetformvalidation: function () {
            //    $('.form-group').removeClass('has-error has-feedback');
            //    $('.form-group').find('span.help-block').hide();
            //    $('.form-group').find('i.form-control-feedback').hide();
            //},
            //tabclick: function () {
            //    //e.preventDefault();

            //},
            //tabchange: function (tabId) {


            //},
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                console.log("ada");
                //Event for adding data
                $modalAdd.modal('toggle');
                //document.getElementById('lblmaincode').style.color = '#333';
                //document.getElementById('maincode').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                //document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Access";


                $('#SelectJabatan').val("");
                $('#SelectJabatan').selectpicker('refresh');
                $('#SelectmdlAccCode').val("");
                $('#SelectmdlAccCode').selectpicker('refresh');
                //$('input[name=maincode]').val('');
                //_setEvents.formvalidation();
            },
            edit: function () {
                //document.getElementById('lblmaincode').style.color = '#333';
                //document.getElementById('maincode').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                //document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'inline';
                document.getElementById('btnSave').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Access";
                //$("#maincode").prop("disabled", true);

                $modalAdd.modal('toggle');
                _setEvents.formvalidation();

                var $this = $(this);
                var $row = $(this).closest('tr');
                var Id = $row.find('.Id').html();

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetMasterMatriksModuleAccess',
                    type: "POST",
                    data: "{'Id' :'" + Id + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            //$('#Access').val(obj.Access);
                            $('#SelectJabatan').val(obj.mainCode);
                            $('#SelectJabatan').selectpicker('refresh');
                            $('#Id').val(obj.Id);
                            //$('#Selectstatus').val(AccessStatus).attr("selected", "selected");
                            var optArr = obj.mdlAccCode.split(",");
                            //let optArr = [];
                            for (var i = 0; i < data.length; i++) {
                                optArr.push(data[i].Skill.Id);

                            }
                            $('#SelectmdlAccCode').val(optArr);
                            $('#SelectmdlAccCode').selectpicker('refresh');


                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = {
                    Id: $('input[name=Id]').val(), Jabatan: $('#SelectJabatan option:selected').val(), mdlAccCode: $('#SelectmdlAccCode').val().toString()
                };
                //var savetype = 
                _setCustomFunctions.EditAccess(data);

                return false;

            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var Id = $row.find('.Id').html();
                //Set Ajax Submit Here
                var data = { Id: Id };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = {
                        Jabatan: $('#SelectJabatan option:selected').val(), mdlAccCode: $('#SelectmdlAccCode').val().toString()
                    };
                    //var savetype = 
                    _setCustomFunctions.saveAccess(data);
                    $modalAdd.modal('hide');

                    return false;
                }
            }
        },
        setCustomFunctions: {
            init: function () {
                this.getJabatan();
                this.getAccess();
            },
            registerTabContent: function (tabId, mdlcode) {
            },
            getJabatan: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/getListMasterMainJabatan2',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectJabatan').find('option').remove();
                        json.forEach(function (obj) {
                            if (!(obj.Description == "HO")) {
                            $("#SelectJabatan").append($('<option>', {
                                value: obj.mainCode,
                                text: obj.Description
                            }));
                            }
                        })

                        $('#SelectJabatan').selectpicker('refresh');
                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getAccess: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/getMastermdlAccCode',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectmdlAccCode').find('option').remove();
                        json.forEach(function (obj) {
                            //if (obj.mainCode == $("#SelectMainJabatan option:selected").val()) {
                            $("#SelectmdlAccCode").append($('<option>', {
                                value: obj.mdlAccCode,
                                text: obj.mdlAccDesc
                            }));
                            //}
                        })

                        $('#SelectAccess').selectpicker('refresh');
                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveModuleAccessManagement',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableAccess.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Module Access has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteModuleAccessManagement',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableAccess.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Module Access has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditModuleAccessManagement',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $UpdateButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableAccess.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Module Access has been Update successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.DeleteAccess(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
})