﻿var listUser = null;
var listActiveUser = null, listOrdinaryUser = null, listSurveyor = null, listInactiveUser = null, listPendingUser = null, listCloneUser = null;
var chatHub = null;


$(function () {

    document.getElementById("profileshow").onclick = function () { Hide() };

    function Hide() {
        //document.getElementById('write').style.display = 'none';
        var obj = document.getElementById("write");
        if (obj.style.display == "inline")
            obj.style.display = "none";
        else
            obj.style.display = "inline";

        //document.getElementById('write').style.display = 'none';
        var obj = document.getElementById("write2");
        if (obj.style.display == "none")
            obj.style.display = "inline";
        //else
        //    obj.style.display = "none";
        //document.getElementById('write2').style.display = 'inline';
    }

    //document.getElementById('Profile').style.display = 'none';
    var url = window.location.href;
    if (url.indexOf('userdetail') > -1) {
        //document.getElementById('Profile').style.display = 'inline';
        //GetModule();
        //RegisterFormValidation();
        //RegisterResetFormValidation();
        //RegisterOnChangeEvent();

        SetupUserDetail();
    }


    $.ajax({
        url: '../../webservice/WebService_COR.asmx/CheckAccess',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        type: "GET",
        success: function (response) {
            var json = jQuery.parseJSON(response.d);
            if (json == true)
            {
                document.getElementById('btnGoToAdminPage').style.display = 'inline-block';
                //document.getElementById('DSK').style.display = 'inline-block';
                document.getElementById('btnGoToAdminPage').style.display = 'inline-block';
            }
            else
            {
                document.getElementById('btnGoToAdminPage').style.display = 'none';
            }
            
        }
    });


    var _path = window.location.pathname.substring(0, 10);
    var url = location.protocol + "//" + location.host + _path;

    $("#profileImage2").attr("src", url + "/dist/img/loading.gif");
    $("#profileImage3").attr("src", url + "/dist/img/loading.gif");
    $("#profileImage4").attr("src", url + "/dist/img/loading.gif");
    var yourImg1 = document.getElementById('profileImage3');
    if (yourImg1 && yourImg1.style) {
        yourImg1.style.height = '45px';
        yourImg1.style.width = '45px';
    }

    $.ajax({
        type: 'GET',
        url: '../../webservice/WebService_COR.asmx/GetUserDetail',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            json.forEach(function (obj) {
                if (obj.UserAvatar == null || obj.UserAvatar == "") {
                    if (obj.UserGender == "Male") {
                        //build the image url from this
                        var imgurl = url + "/attachment/avatar/DefaultMale.png";
                        //plug it into the jQuery selector
                        $("#profileImage2").attr("src", imgurl);
                        $("#profileImage3").attr("src", imgurl);
                        $("#profileImage4").attr("src", imgurl);
                        //document.getElementById("profileImage2").src = "../../Upload/UploadProfilePictCompress/avatar5.png"
                        //document.getElementById("profileImage3").src = "../../Upload/UploadProfilePictCompress/avatar5.png"
                        //document.getElementById("profileImage4").src = "../../Upload/UploadProfilePictCompress/avatar5.png"

                        var yourImg = document.getElementById('profileImage2');
                        //var yourImg = document.getElementById('profileImage3');
                        if (yourImg && yourImg.style) {
                            yourImg.style.height = '30px';
                            yourImg.style.width = '30px';
                        }
                    }
                    else {
                        var imgurl = url + "/attachment/avatar/DefaultFemale.png";
                        //plug it into the jQuery selector
                        $("#profileImage2").attr("src", imgurl);
                        $("#profileImage3").attr("src", imgurl);
                        $("#profileImage4").attr("src", imgurl);
                        var yourImg = document.getElementById('profileImage2');
                        //var yourImg = document.getElementById('profileImage3');
                        if (yourImg && yourImg.style) {
                            yourImg.style.height = '30px';
                            yourImg.style.width = '30px';
                        }
                        var yourImg1 = document.getElementById('profileImage3');
                        if (yourImg1 && yourImg1.style) {
                            yourImg1.style.height = '45px';
                            yourImg1.style.width = '45px';
                        }
                    }
                }
                else {
                    var _url = "attachment//avatar//" + obj.UserAvatar;
                    $.ajax({
                        type: 'POST',
                        url: '../../webservice/WebService_COR.asmx/CheckImageExist',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: "{'url' :'" + _url + "'}",
                        success: function (response) {
                            if (response.d == "true") {
                                var imgurl = url + "/attachment/avatar/" + obj.UserAvatar;
                                //plug it into the jQuery selector
                                $("#profileImage2").attr("src", imgurl);
                                $("#profileImage3").attr("src", imgurl);
                                $("#profileImage4").attr("src", imgurl);

                                var yourImg = document.getElementById('profileImage2');
                                //var yourImg = document.getElementById('profileImage3');
                                if (yourImg && yourImg.style) {
                                    yourImg.style.height = '30px';
                                    yourImg.style.width = '30px';
                                }
                                var yourImg1 = document.getElementById('profileImage3');
                                if (yourImg1 && yourImg1.style) {
                                    yourImg1.style.height = '45px';
                                    yourImg1.style.width = '45px';
                                }

                            }
                            else {
                                if (obj.UserGender == "Male") {
                                    var imgurl = url + "/attachment/avatar/DefaultMale.png";
                                    //plug it into the jQuery selector
                                    $("#profileImage2").attr("src", imgurl);
                                    $("#profileImage3").attr("src", imgurl);
                                    $("#profileImage4").attr("src", imgurl);
                                    var yourImg = document.getElementById('profileImage2');
                                    //var yourImg = document.getElementById('profileImage3');
                                    if (yourImg && yourImg.style) {
                                        yourImg.style.height = '30px';
                                        yourImg.style.width = '30px';
                                    }
                                    var yourImg1 = document.getElementById('profileImage3');
                                    if (yourImg1 && yourImg1.style) {
                                        yourImg1.style.height = '45px';
                                        yourImg1.style.width = '45px';
                                    }
                                }
                                else {
                                    var imgurl = url + "/attachment/avatar/DefaultFemale.png";
                                    //plug it into the jQuery selector
                                    $("#profileImage2").attr("src", imgurl);
                                    $("#profileImage3").attr("src", imgurl);
                                    $("#profileImage4").attr("src", imgurl);
                                    var yourImg = document.getElementById('profileImage2');
                                    //var yourImg = document.getElementById('profileImage3');
                                    if (yourImg && yourImg.style) {
                                        yourImg.style.height = '30px';
                                        yourImg.style.width = '30px';
                                    }
                                    var yourImg1 = document.getElementById('profileImage3');
                                    if (yourImg1 && yourImg1.style) {
                                        yourImg1.style.height = '45px';
                                        yourImg1.style.width = '45px';
                                    }
                                }
                            }

                        }

                    });
                }

            });

        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });

    ////Set Link Active 
    //var url = window.location.href;
    //// for sidebar menu but not for treeview submenu
    //$('ul.sidebar-menu a').filter(function () {
    //    return this.href == url;
    //}).parent().siblings().removeClass('active').end().addClass('active');
    //// for treeview which is like a submenu
    //$('ul.treeview-menu a').filter(function () {
    //    return this.href == url;
    //}).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active menu-open').end().addClass('active menu-open');

    //// Declare a proxy to reference the hub. 
    //chatHub = $.connection.chatHub;
    //RegisterClientMethods(chatHub);
    //// Start Hub
    //$.connection.hub.start().done(function () {
    //    RegisterEvents(chatHub)
    //});


})

function PushNotif() {
    $.confirm({
        title: 'Broadcast Message',
        boxWidth: '300px',
        useBootstrap: false,
        content: '' +
        '<form action="" class="formName">' +
        '<div class="form-group"><div class="col-md-12">' +
        '<label>Remarks</label>' +
        '<input type="text" id="remarks" placeholder="Remarks" class="name form-control" required />' +
        '</div></div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: 'Submit',
                btnClass: 'btn-blue',
                action: function () {
                    var msg = $("#remarks").val();

                    if (msg.length > 0) {

                        var userName = $('#hdUserName').val();

                        var date = GetCurrentDateTime(new Date());

                        chatHub.server.sendMessageToAll(userName, msg, date);
                        $("#remarks").val('');
                    }
                }
            },
            cancel: function () {
                //close
            },
        }
    });
}

function RegisterEvents(chatHub) {
    var name = '<%= this.UserName %>';

    if (name.length > 0) {
        chatHub.server.connect(name);
    }
}

function RegisterClientMethods(chatHub) {
    // Calls when user successfully logged in
    chatHub.client.onConnected = function (id, userName, allUsers, messages, times) {

        $('#hdId').val(id);
        $('#hdUserName').val(userName);
        $('#spanUser').html(userName);
    }

    // On New User Connected
    chatHub.client.onNewUserConnected = function (id, name, UserImage, loginDate) {
        //AddUser(chatHub, id, name, UserImage, loginDate);
    }


    // On User Disconnected
    chatHub.client.onUserDisconnected = function (id, userName) {

    }

    chatHub.client.messageReceived = function (userName, message, time, userimg) {
        ShowToast('Information', message, 'info');
        //AddMessage(userName, message, time, userimg);
    }


}

function GetCurrentDateTime(now) {

    var localdate = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT");

    return localdate;
}

function SetAccess() {


}

function ShowToast(heading, text, type) {
    if (type == 'info') {
        $.toast({
            text: text,
            heading: heading,
            position: 'top-center',
            bgColor: '#2ecc71',
            hideAfter: true,
            textColor: '#fff',
        });
    }
}
