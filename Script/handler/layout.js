﻿var listUser = null;
var listActiveUser = null, listOrdinaryUser = null, listSurveyor = null, listInactiveUser = null, listPendingUser = null, listCloneUser = null;
var chatHub = null;
var allUser = [];
var _uname = [];
var UsersID = "";
$(function () {
    var _count = 1;
    var PageMaster = {
        optionsMaster: {
            pageMode: '#PageMode',
            listOperatingUnitAccess: '#ListOperatingUnitAccess',
            listCompany: '#ListCompany',
            sessionCompany: '#SessionCompany',
            sessionEstate: '#SessionEstate',
            profileshowButton: '#profileshow',
            selectFilterCompany: '#selectFilterCompany',
            selectFilterEstate: '#selectFilterEstate',
        },
        initializeMaster: function () {
            this.setVarsMaster();
            this.setMenuByGroup();
            this.setCustomFunctionsMaster.init();
            this.setEventsMaster.init();

            if (url.indexOf('alluser') > -1) {
                SetupUser();
            }
            //else if (url.indexOf('userdetail') > -1) {
            //    SetupUserDetail();
            //}
        },
        setMenuByGroup: function () {
            $.ajax({
                type: 'GET',
                url: '../../webservice/WebService_COR.asmx/GetMenuByGroup',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var json = $.parseJSON(response.d);
                    json.forEach(function (obj) {
                        if (json[0].usrGroupCode == "CLE") {
                            if ($pageMode.val() == "admintools") {
                                document.getElementById('2').style.display = 'none';
                                document.getElementById('34').style.display = 'none';
                                document.getElementById('36').style.display = 'none';
                                document.getElementById('38').style.display = 'none';
                                document.getElementById('40').style.display = 'none';


                            }

                        }
                    })
                }
            });
        },
        setVarsMaster: function () {
            $pageMode = $(this.optionsMaster.pageMode);
            $listOperatingUnitAccess = $(this.optionsMaster.listOperatingUnitAccess);
            $listCompany = $(this.optionsMaster.listCompany);
            $sessionCompany = $(this.optionsMaster.sessionCompany);
            $sessionEstate = $(this.optionsMaster.sessionEstate);
            $selectFilterCompany = $(this.optionsMaster.selectFilterCompany);
            $selectFilterEstate = $(this.optionsMaster.selectFilterEstate);
            $profileshowButton = $(this.optionsMaster.profileshowButton);
            url = window.location.href;
            url_location = window.location;
            chatHub = $.connection.chatHub;

            _setCustomFunctionsMaster = this.setCustomFunctionsMaster;
        },
        setEventsMaster: {
            init: function () {
                this.registerClientMethods(chatHub);
                $.connection.hub.start().done(this.registerEvents);
                $selectFilterCompany.change(this.onChangeCompany);
                $selectFilterEstate.change(this.onChangeEstate);
                $profileshowButton.change(this.hiddenLogo);
            },
            registerEvents: function () {
                var name = '<%= this.UserName %>';

                if (name.length > 0) {
                    chatHub.server.connect(name);
                }
            },
            registerClientMethods: function (chatHub) {
                // Calls when user successfully logged in
                chatHub.client.onConnected = function (id, userName, allUsers, messages, times) {

                    $('#hdId').val(id);
                    $('#hdUserName').val(userName);
                    $('#spanUser').html(userName);
                }

                // On New User Connected
                chatHub.client.onNewUserConnected = function (id, name, UserImage, loginDate) {
                    //AddUser(chatHub, id, name, UserImage, loginDate);
                }


                // On User Disconnected
                chatHub.client.onUserDisconnected = function (id, userName) {

                }

                chatHub.client.messageReceived = function (userName, message, time, userimg) {
                    ShowToast('Information', message, 'info');
                    //AddMessage(userName, message, time, userimg);
                }
            },
            onChangeCompany: function () {
                _setCustomFunctionsMaster.getEstate($selectFilterCompany.val());
                _setCustomFunctionsMaster.setSessionCompanyAndEstate($selectFilterCompany.val(), $selectFilterEstate.val());
            },
            onChangeEstate: function () {
                _setCustomFunctionsMaster.setSessionCompanyAndEstate($selectFilterCompany.val(), $selectFilterEstate.val());
                if (_count == 1) {
                    localStorage.setItem("estCode", $selectFilterEstate.val());
                    localStorage.setItem("count", 2);
                }
            },
            hiddenLogo: function () {
                if ($("#write").length != 0) {
                    var obj = document.getElementById("write");
                    if (obj.style.display == "inline") {
                        obj.style.display = "none";
                        document.getElementById("logo").style.display = 'none';
                    }
                    else {
                        obj.style.display = "none";
                        document.getElementById("logo").style.display = 'none';
                    }

                }
            }

        },
        setCustomFunctionsMaster: {
            init: function () {
                this.setupMenuBarClickBehavior();
                if ($pageMode.val() == "mainmenu") {
                    this.getCompany();
                    this.getEstate($selectFilterCompany.val());
                }
            },
            setupMenuBarClickBehavior: function () {
                // for sidebar menu entirely but not cover treeview
                $('ul.sidebar-menu a').filter(function () {
                    return this.href == url_location;
                }).parent().addClass('active');
                //Top bar
                $('ul.navbar-nav a').filter(function () {
                    return this.href == url_location;
                }).parent().addClass('active');

                // for treeview
                $('ul.treeview-menu a').filter(function () {
                    return this.href == url_location;
                }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
            },
            getCompany: function () {
                var json = $.parseJSON($listCompany.val());

                $selectFilterCompany.find('option').remove();
                //$selectFilterCompany.append($('<option>', {
                //    value: "",
                //    text: "All PT"
                //}));
                json.forEach(function (obj) {
                    $selectFilterCompany.append($('<option>', {
                        value: obj.CompanyCode,
                        text: obj.CompanyName
                    }));
                })

                if ($sessionCompany.val() != "") $selectFilterCompany.val($sessionCompany.val());

                $selectFilterCompany.selectpicker('refresh');
            },
            getEstate: function (company) {
                var json = $.parseJSON($listOperatingUnitAccess.val());

                $selectFilterEstate.find('option').remove();
                json.forEach(function (obj) {
                    if (obj.CompanyCode == company || company == "") {
                        $selectFilterEstate.append($('<option>', {
                            value: obj.estCode,
                            text: obj.estName
                        }));
                    }
                })

                if ($sessionEstate.val() != "") $selectFilterEstate.val($sessionEstate.val());
                if (localStorage.getItem("count") == 2) {
                    $selectFilterEstate.val(localStorage.getItem("estCode")).attr('selected', 'selected');
                    localStorage.removeItem("estCode");
                    localStorage.removeItem("count");
                }
                $selectFilterEstate.selectpicker('refresh');


            },
            setSessionCompanyAndEstate: function (company, estate) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SetSessionCompanyAndEstate',
                    data: '{CompanyCode: "' + company + '", EstCode: "' + estate + '"}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        console.log(response.d);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("function SetSessionCompanyAndEstate Company : " + xhr.statusText);
                    }
                });
            },
            directToHMS: function (username, password) {
                //alert(username, password);
                var dataAllPt;
                var dataAllKebun;
                var dataAllAfdeling;
                var EstNewCode;
                var subAfdeling;
                var COMPANYCODE;
                var estCode;
                $.ajax({
                    type: 'POST',
                    url: 'http://172.30.1.122/hmsweb/Service/WebServiceDesktopLogin.asmx/GetLoginDesktop',
                    data: '{UserName: "' + username + '",UserPass:"' + password + '"}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        console.log(response.d);
                        var json = JSON.parse(response.d);
                        let dataAllPt = _.uniq(json, function (x) { return x.COMPANYCODE; });
                        dataAllPt = _.sortBy(dataAllPt, 'COMPANYCODE');
                        let dataAllKebun = _.uniq(json, function (x) { return x.estCode; });
                        dataAllKebun = _.sortBy(dataAllKebun, 'EstNewCode');
                        let dataAllAfdeling = _.sortBy(json, 'afdeling');
                        let dataEstNewCode = _.sortBy(json, 'EstNewCode');

                        localStorage.setItem("dataAllPt", JSON.stringify(dataAllPt));
                        localStorage.setItem("dataAllKebun", JSON.stringify(dataAllKebun));
                        localStorage.setItem("dataAllAfdeling", JSON.stringify(dataAllAfdeling));
                        localStorage.setItem("EstNewCode", JSON.stringify(dataAllPt[0].EstNewCode));
                        localStorage.setItem("afdeling", JSON.stringify(dataAllPt[0].afdeling));
                        localStorage.setItem("company", JSON.stringify(dataAllPt[0].COMPANYCODE));
                        localStorage.setItem("estate", JSON.stringify(dataAllPt[0].estCode));
                        window.location.href = json[0].url;
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("function SetSessionCompanyAndEstate Company : " + xhr.statusText);
                    }
                });
            }
        }

    }

    PageMaster.initializeMaster();

    ////Set Link Active 
    //var url = window.location.href;
    //var url_location = window.location;

    //// for sidebar menu entirely but not cover treeview
    //$('ul.sidebar-menu a').filter(function () {
    //    return this.href == url_location;
    //}).parent().addClass('active');
    ////Top bar
    //$('ul.navbar-nav a').filter(function () {
    //    return this.href == url_location;
    //}).parent().addClass('active');

    //// for treeview
    //$('ul.treeview-menu a').filter(function () {
    //    return this.href == url_location;
    //}).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');

    //// Declare a proxy to reference the hub. 
    //chatHub = $.connection.chatHub;
    //RegisterClientMethods(chatHub);
    //// Start Hub
    //$.connection.hub.start().done(function () {
    //    RegisterEvents(chatHub)
    //});

    //if (url.indexOf('dashboard') > -1) {
    //    console.log('Call Function in cor_dashboard.js');

    //    SetInitDashboard();
    //}
    //else if (url.indexOf('alluser') > -1) {
    //    SetupUser();
    //}

    //else if (url.indexOf('userdetail') > -1) {
    //    SetupUserDetail();
    //}

    //else if (url.indexOf('verificationadmin') > -1) {
    //    SetupVerificationAdmin();
    //}

    $.ajax({
        "dataType": 'json',
        "contentType": "application/json; charset=utf-8",
        "type": "GET",
        "url": "../../webservice/WebService_COR.asmx/AssignUsers_All",
        "success": function (msg) {
            //console.info("msg", msg.d);
            allUser = JSON.parse(msg.d)
        },
        error: function (xhr, textStatus, error) {
            alert("error");
        }
    });

    $.ajax({
        type: 'GET',
        url: '../../webservice/WebService_COR.asmx/GetUserDetail',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            json.forEach(function (obj) {
                if (obj.UserAvatar == null || obj.UserAvatar == "") {
                    if (obj.UserGender == "Male") {
                        document.getElementById("profileImage2").src = "../../dist/img/avatar5.png"
                        document.getElementById("profileImage3").src = "../../dist/img/avatar5.png"
                        document.getElementById("profileImage4").src = "../../dist/img/avatar5.png"
                        var yourImg = document.getElementById('profileImage3');
                        if (yourImg && yourImg.style) {
                            yourImg.style.height = '30px';
                            yourImg.style.width = '30px';
                        }
                    }
                    else {
                        document.getElementById("profileImage2").src = "../../dist/img/avatar2.png"
                        document.getElementById("profileImage3").src = "../../dist/img/avatar2.png"
                        document.getElementById("profileImage4").src = "../../dist/img/avatar2.png"
                        var yourImg = document.getElementById('profileImage3');
                        if (yourImg && yourImg.style) {
                            yourImg.style.height = '30px';
                            yourImg.style.width = '30px';
                        }
                    }
                }
                else {
                    var _url = "attachment//avatar//" + obj.UserAvatar;
                    $.ajax({
                        type: 'POST',
                        url: '../../webservice/WebService_COR.asmx/CheckImageExist',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: "{'url' :'" + _url + "'}",
                        success: function (response) {
                            if (response.d == "true") {
                                document.getElementById("profileImage2").src = "../../attachment/avatar/" + obj.UserAvatar
                                document.getElementById("profileImage3").src = "../../attachment/avatar/" + obj.UserAvatar
                                document.getElementById("profileImage4").src = "../../attachment/avatar/" + obj.UserAvatar
                                var yourImg = document.getElementById('profileImage3');
                                if (yourImg && yourImg.style) {
                                    yourImg.style.height = '30px';
                                    yourImg.style.width = '30px';
                                }
                            }
                            else {
                                if (obj.UserGender == "Male") {
                                    document.getElementById("profileImage2").src = "../../dist/img/avatar5.png"
                                    document.getElementById("profileImage3").src = "../../dist/img/avatar5.png"
                                    document.getElementById("profileImage4").src = "../../dist/img/avatar5.png"
                                    var yourImg = document.getElementById('profileImage3');
                                    if (yourImg && yourImg.style) {
                                        yourImg.style.height = '30px';
                                        yourImg.style.width = '30px';
                                    }
                                }
                                else {
                                    document.getElementById("profileImage2").src = "../../dist/img/avatar2.png"
                                    document.getElementById("profileImage3").src = "../../dist/img/avatar2.png"
                                    document.getElementById("profileImage4").src = "../../dist/img/avatar2.png"
                                    var yourImg = document.getElementById('profileImage3');
                                    if (yourImg && yourImg.style) {
                                        yourImg.style.height = '30px';
                                        yourImg.style.width = '30px';
                                    }
                                }
                            };

                        }

                    });
                }

            })
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
})


$.ajax({
    type: "GET",
    url: '../../webservice/WebService_COR.asmx/GetUserName',
    //data: { UserName: JSON.stringify(username) },
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',

    success: function (response) {
        //var json = $.parseJSON(response.d);
        _uname = [];
        var json = jQuery.parseJSON(response.d);
        for (var i = 0; i < json.length; i++) {
            _uname.push(json[i].Nama);
        }
    },
    error: function (xhr, ajaxOptions, thrownError) {
    }
});





function PushNotif() {
    $.confirm({
        title: 'Broadcast Message',
        boxWidth: '300px',
        useBootstrap: false,
        content: '' +
        '<form action="" class="formName">' +
        '<div class="form-group"><div class="col-md-12">' +
        '<label>Remarks</label>' +
        '<input type="text" id="remarks" placeholder="Remarks" class="name form-control" required />' +
        '</div></div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: 'Submit',
                btnClass: 'btn-blue',
                action: function () {
                    var msg = $("#remarks").val();

                    if (msg.length > 0) {

                        var userName = $('#hdUserName').val();

                        var date = GetCurrentDateTime(new Date());

                        chatHub.server.sendMessageToAll(userName, msg, date);
                        $("#remarks").val('');
                    }
                }
            },
            cancel: function () {
                //close
            },
        }
    });
}

function RegisterEvents(chatHub) {
    var name = '<%= this.UserName %>';

    if (name.length > 0) {
        chatHub.server.connect(name);
    }
}

function RegisterClientMethods(chatHub) {
    // Calls when user successfully logged in
    chatHub.client.onConnected = function (id, userName, allUsers, messages, times) {

        $('#hdId').val(id);
        $('#hdUserName').val(userName);
        $('#spanUser').html(userName);
    }

    // On New User Connected
    chatHub.client.onNewUserConnected = function (id, name, UserImage, loginDate) {
        //AddUser(chatHub, id, name, UserImage, loginDate);
    }


    // On User Disconnected
    chatHub.client.onUserDisconnected = function (id, userName) {

    }

    chatHub.client.messageReceived = function (userName, message, time, userimg) {
        ShowToast('Information', message, 'info');
        //AddMessage(userName, message, time, userimg);
    }


}

function GetCurrentDateTime(now) {

    var localdate = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT");

    return localdate;
}

function ShowToast(heading, text, type) {
    if (type == 'info') {
        $.toast({
            text: text,
            heading: heading,
            position: 'top-center',
            bgColor: '#2ecc71',
            hideAfter: true,
            textColor: '#fff',
        });
    }
}
