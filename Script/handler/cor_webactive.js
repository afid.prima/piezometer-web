﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddWebActive',
            ImportButton: '#btnImportWebActive',
            saveButton: '#btnSave',
            UpdateButton: '#btnUpdate',
            UploadButton: '#BtnUpload',
            modalAdd: '#mdlAdd',
            modalImport: '#mdlImport',
            tabWebActive: '#tabWebActive',
            table: '#tblwebactive',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $ImportButton = $(this.options.ImportButton);
            $saveButton = $(this.options.saveButton);
            $UpdateButton = $(this.options.UpdateButton);
            $UploadButton = $(this.options.UploadButton);
            $modalAdd = $(this.options.modalAdd);
            $modalImport = $(this.options.modalImport);
            $tabWebActive = $(this.options.tabWebActive);
            $table = $(this.options.table);
            _formAdd = this.options.formAdd;
            _tabWebActive = this.options.tabWebActive;
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                },{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2,
                    width: 100
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: 100
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 50
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 5,
                    width: 50
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 6,
                    width: 500
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 7,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 8,
                    width: 5
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetWebActive_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    aoData.push({ 'name': 'mdlcode', 'value': $('#SelectFilterModule').val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblwebactive").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $ImportButton.on('click', this.import);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $UploadButton.on('click', this.upload);
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $table.on('click', 'a[data-target="#delete"]', this.delete);
                $('#SelectModule').on('change', _setCustomFunctions.getPagebymdlCode);
                $('#SelectFilterModule').on('change', this.ChangeModule);

                //this.formvalidation();

                //select event
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 2,
                            required: true
                        },
                        name: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Web Active Page";

                $('input[name=code]').val('');
                $('input[name=codeHidden]').val('');
                $('input[name=name]').val('');
                $('#SelectTipe').val('');
                $('#SelectPage').val('');
                $('#SelectModule').val('');
                _setEvents.formvalidation();
            },
            edit: function () {
                document.getElementById('btnUpdate').style.display = 'inline';
                document.getElementById('btnSave').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Web Active Page";


                $modalAdd.modal('toggle');
                _setEvents.formvalidation();

                var $this = $(this);
                var $row = $(this).closest('tr');
                var AObjectID = $row.find('.AObjectID').html();
                var AObjectName = $row.find('.AObjectName').html();

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetWebActiveObject',
                    type: "POST",
                    data: "{'AObjectID' :'" + AObjectID + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=code]').val(obj.AObjectID);
                            $('input[name=codeHidden]').val(obj.AObjectID);
                            $('input[name=name]').val(obj.AObjectName);
                            $('#SelectTipe').val(obj.TypeID);
                            $('#SelectPage').val(obj.PageID);
                            $('#SelectModule').val(obj.mdlCode);
                            $('#description').val(obj.description);

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });
                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = {
                    AObjectID: $('input[name=codeHidden]').val(), NewAObjectID: $('input[name=code]').val(), AObjectName: $('input[name=name]').val(), mdlCode: $('#SelectModule').val()
                    , TypeID: $('#SelectTipe').val(), PageID: $('#SelectPage').val(), description: $('#description').val()
                };//var savetype = 
                _setCustomFunctions.EditWebActive(data);

                return false;



                //$("#formAdd :input").prop("disabled", true);
                ////Set Ajax Submit Here
                //var data = { CountryCode: $('input[name=codeHidden]').val(), NewCountryCode: $('input[name=code]').val(), CountryName: Count$('input[name=name]').val() };
                ////var savetype = 

                //_setCustomFunctions.EditCountry(data);

                //return false;
            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var AObjectID = $row.find('.AObjectID').html();
                //Set Ajax Submit Here
                var data = { AObjectID: AObjectID };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = {
                        AObjectID: $('input[name=code]').val(), AObjectName: $('input[name=name]').val(), mdlCode: $('#SelectModule').val()
                        , TypeID: $('#SelectTipe').val(), PageID: $('#SelectPage').val(), description: $('#description').val()
                    };
                    //var savetype = 
                    _setCustomFunctions.saveWebActive(data);

                    return false;
                }
            },
            selectchange: function () {
                window.location.reload(true);

            },
            import: function () {
                $modalImport.modal('toggle');
            },
            upload: function () {
                var filename = $('#file').get(0);
                var files = filename.files;
                _setCustomFunctions.UploadData(files);
            },
            ChangeModule: function () {
                $("#tbodyid tr").remove();
                var paramID = this.value;
                _setCustomFunctions.getAccessModuleByModule(paramID);
            },

        },
        setCustomFunctions: {
            init: function () {
                this.getModule();
                this.getType();
                this.getPage();
                this.getFilterModule();
                //this.getMasterModuleGroup();
                //this.getMasterModuleType();
            },
            getModule: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterModuleWebpage',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectModule').find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#SelectModule").append($('<option>', {
                                value: obj.mdlCode,
                                text: obj.mdlCode + " - " + obj.mdlDesc
                            }));
                        })
                    }
                })
            },
            getFilterModule: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterModuleWebpage',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectFilterModule').find('option').remove();
                        $("#SelectFilterModule").append($('<option>', {
                            value: "",
                            text: "All Module"
                        }));
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#SelectFilterModule").append($('<option>', {
                                value: obj.mdlCode,
                                text: obj.mdlCode + " - " + obj.mdlDesc
                            }));
                        })
                    }
                })
            },
            getType: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterTipe',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectTipe').find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#SelectTipe").append($('<option>', {
                                value: obj.TypeID,
                                text: obj.TypeID + " - " + obj.TypeName
                            }));
                        })
                    }
                })
            },
            getPage: function () {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterPage',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectPage').find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#SelectPage").append($('<option>', {
                                value: obj.PageID,
                                text: obj.PageID + " - "
                            }));
                        })
                    }
                })
            },
            getPagebymdlCode: function () {
                var mdlCode = $('#SelectModule').val();
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterPageBymdlCode',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'mdlCode' :'" + mdlCode + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectPage').find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#SelectPage").append($('<option>', {
                                value: obj.PageID,
                                text: obj.PageID + " - "
                            }));
                        })
                    }
                })
            },
            saveWebActive: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveWebActive',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Web Active has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteWebActive: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteWebActive',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Web Active has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditWebActive: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditWebActive',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $UpdateButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Web Active has been Update successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getAccessModuleByModule: function (paramID) {
                $table.DataTable().ajax.reload();
            },
            UploadData: function (files) {
                var data = new FormData();
                for (var i = 0; i < files.length; i++) {
                    data.append(files[i].name, files[i]);

                }
                $.ajax({
                    type: 'POST',
                    url: '../../Service/ImportHandler.ashx',
                    data: data,
                    contentType: false,
                    processData: false,

                    success: function (response) {
                        alert('import data excel sukses');
                        $('.loading').hide();
                        //ADDDataRainfallBulanan(response);
                    }

                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.DeleteWebActive(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
})