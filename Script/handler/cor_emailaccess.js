﻿$(function () {
    var tableAssignUsers;
    var _listAllEmail;
    var _dataEmail = [];
    var _dataEmailRemove = [];
    var objEmail = [];
    var _dataSelect = [];
    var lengthbfr = 0;
    var mdlCode;
    var selected = [];

    var Page = {
        options: {
            formAdd: '#formAdd',
            formAssign: '#formAssign',
            addButton: '#btnAddEmail',
            assignUsersButton: '#btnAssignUser',
            SaveassignUsersButton: '#btnSaveAssignUser',
            CheckedAll: '#select_all',
            saveButton: '#btnSave',
            UpdateButton: '#btnUpdate',
            modalAdd: '#mdlAdd',
            modalAssignUsers: '#mdlAssignUsers',
            tabEmail: '#tabEmail',
            table: '#tblemail',
            tableAssignUsers: '#tblassignUsers',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            //this.setTableAssignUsers();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $formAssign = $(this.options.formAssign);
            $addButton = $(this.options.addButton);
            $assignUsersButton = $(this.options.assignUsersButton);
            $SaveassignUsersButton = $(this.options.SaveassignUsersButton);
            $CheckedAll = $(this.options.CheckedAll);
            $saveButton = $(this.options.saveButton);
            $UpdateButton = $(this.options.UpdateButton);
            $modalAdd = $(this.options.modalAdd);
            $modalAssignUsers = $(this.options.modalAssignUsers);
            $tabEmail = $(this.options.tabEmail);
            $table = $(this.options.table);
            $tableAssignUsers = $(this.options.tableAssignUsers);
            _formAdd = this.options.formAdd;
            _tabEmail = this.options.tabEmail;
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: 150
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 6,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 7,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetEmail_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblemail").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $table.on('click', 'a[data-target="#delete"]', this.delete);
                $assignUsersButton.on('click', this.assignUsers);
                $SaveassignUsersButton.on('click', this.SaveAssignUsers);
                $('#SelectAssignUser').on('change', this.ChangeEmailCode);
                $('#SelectModule').on('change', this.ChangemdlCode);
                $('#SelectMultiModuleAccess').on('change', this.GetDatamdlAccCode);

                //this.formvalidation();

                //select event

            },

            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 2,
                            required: true
                        },
                        name: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Email Access";
                $('input[name=code]').val('');
                $('input[name=codeHidden]').val('');
                $('input[name=name]').val('');
                $('input[name=desc]').val('');
                $('input[name=type]').val('');
                $('#SelectModule').val('');
                $('#SelectType').val('');
                $('#SelectMultiModuleAccess').selectpicker('val', '');
                $('.selectpicker').selectpicker('refresh');

                _setEvents.formvalidation();
            },
            edit: function () {
                document.getElementById('btnUpdate').style.display = 'inline';
                document.getElementById('btnSave').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Email Access";

                $modalAdd.modal('toggle');
                _setEvents.formvalidation();

                var $this = $(this);
                var $row = $(this).closest('tr');
                var EmailCode = $row.find('.EmailCode').html();
                var EmailName = $row.find('.EmailName').html();

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetEmail',
                    type: "POST",
                    data: "{'EmailCode' :'" + EmailCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            var _emailCode = obj.EmailCode;
                            $('input[name=code]').val(obj.EmailCode);
                            $('input[name=codeHidden]').val(obj.EmailCode);
                            $('input[name=name]').val(obj.EmailName);
                            $('input[name=desc]').val(obj.EmailDescription);
                            $('#SelectModule').val(obj.mdlCode);
                            $('#SelectType').val(obj.EmailTypeID);

                            var _dataemailmodule = [];
                            $.ajax({
                                type: "POST",
                                url: '../../webservice/WebService_COR.asmx/GetModuleAccCodeByModule',
                                contentType: 'application/json; charset=utf-8',
                                data: "{mdlCode: '" + obj.mdlCode + "'}",
                                dataType: 'json',
                                success: function (response) {
                                    var json = $.parseJSON(response.d);
                                    $('#SelectMultiModuleAccess').find('option').remove();
                                    json.forEach(function (obj) {
                                        //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                                        $("#SelectMultiModuleAccess").append($('<option>', {
                                            value: obj.mdlAccCode,
                                            text: obj.mdlAccDesc
                                        }));

                                        $.ajax({
                                            url: '../../webservice/WebService_COR.asmx/GetModuleAccCodeByEmailCode',
                                            type: "POST",
                                            data: "{'EmailCode' :'" + _emailCode + "'}",
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            success: function (datas) {
                                                var jsons = $.parseJSON(datas.d);
                                                jsons.forEach(function (objs) {
                                                    _dataemailmodule.push(objs.mdlCode)
                                                });
                                                $('#SelectMultiModuleAccess').selectpicker('val', _dataemailmodule);
                                                $('.selectpicker').selectpicker('refresh');
                                            },
                                            failure: function (msg) {
                                                alert("Something go wrong! ");
                                            }
                                        });
                                    })
                                }
                            });
                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = {
                    NewEmailCode: $('input[name=code]').val(), EmailTypeID: $("#SelectType").val(), mdlCode: $("#SelectModule").val(),
                    EmailName: $('input[name=name]').val(), EmailCode: $('input[name=codeHidden]').val(),
                    EmailDescription: $('input[name=desc]').val()
                }
                    ;
                //var savetype = 
                _setCustomFunctions.EditEmail(data);

                return false;



                //$("#formAdd :input").prop("disabled", true);
                ////Set Ajax Submit Here
                //var data = { CountryCode: $('input[name=codeHidden]').val(), NewCountryCode: $('input[name=code]').val(), CountryName: Count$('input[name=name]').val() };
                ////var savetype = 

                //_setCustomFunctions.EditCountry(data);

                //return false;
            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var EmailCode = $row.find('.EmailCode').html();
                //Set Ajax Submit Here
                var data = { EmailCode: EmailCode };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = {
                        EmailTypeID: $("#SelectType").val(), mdlCode: $("#SelectModule").val(),
                        EmailName: $('input[name=name]').val(), EmailCode: $('input[name=code]').val(),
                        EmailDescription: $('input[name=desc]').val()
                    };
                    //var savetype = 
                    _setCustomFunctions.saveEmail(data);

                    return false;
                }
            },
            assignUsers: function () {
                //Event for adding data
                $modalAssignUsers.modal('toggle');
                _setCustomFunctions.showPopupAssignUser();
                //popUpAssignUser();
                //$(".select_all").on('click', alert(""));
            },
            SaveAssignUsers: function () {
                var selected = tableAssignUsers.rows({ selected: true });
                //console.info("selected", selected);
                var objSelected = [];
                var json = [];
                var id = "";
                console.log(id);
                for (var i = 0; i < selected[0].length; i++) {
                    id = tableAssignUsers.rows({ selected: true }).data()[i][1];
                    objSelected.push(id);
                }
                json = objSelected;
                var data = { EmailCode: $("#SelectAssignUser").val() };
                _setCustomFunctions.SetAssign(data, objSelected);
                $modalAssignUsers.modal('hide');


            },
            ChangeEmailCode: function () {
                var EmailCode = $("#SelectAssignUser").val();

                allUser = [];
                _dataSelect = [];
                _setCustomFunctions.SetAssignByEmailCode(EmailCode);
                //popUpAssignUser();
                _setCustomFunctions.showPopupAssignUser();


            },
            ChangemdlCode: function () {
                var mdlCode = $("#SelectModule").val();

                _setCustomFunctions.getMultiModuleAcc(mdlCode);


            },
            GetDatamdlAccCode: function () {
                //alert($(this).val());
                var brands = $('#SelectMultiModuleAccess option:selected');
                selected = [];
                $(brands).each(function (index, brand) {
                    selected.push($(this).val());
                });

            },
        },
        setCustomFunctions: {
            init: function () {
                this.getModule();
                this.getType();
                this.getAssignUser();
                //this.getMasterModuleGroup();
                //this.getMasterModuleType();
            },
            registerTabContent: function (tabId, mdlcode) {
            },
            getModule: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetModule',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectModule').find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#SelectModule").append($('<option>', {
                                value: obj.mdlCode,
                                text: obj.mdlDesc
                            }));
                        })
                    }
                })
            },
            getMultiModuleAcc: function (mdlCode) {
                $.ajax({
                    type: "POST",
                    url: '../../webservice/WebService_COR.asmx/GetModuleAccCodeByModule',
                    contentType: 'application/json; charset=utf-8',
                    data: "{mdlCode: '" + mdlCode + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectMultiModuleAccess').find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#SelectMultiModuleAccess").append($('<option>', {
                                value: obj.mdlAccCode,
                                text: obj.mdlAccDesc
                            }));

                            $('#SelectMultiModuleAccess').selectpicker('refresh');
                            $('#SelectMultiModuleAccess').change();
                        })
                    }
                })
            },
            getType: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/ListEmailType',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectType').find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#SelectType").append($('<option>', {
                                value: obj.EmailTypeID,
                                text: obj.EmailTypeID + " - " + obj.EmailTypeName
                            }));
                        })
                    }
                })
            },
            getAssignUser: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/ListEmailCode',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectAssignUser').find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#SelectAssignUser").append($('<option>', {
                                value: obj.EmailCode,
                                text: obj.EmailCode + ' - ' + obj.EmailName
                            }));
                        })
                    }
                })
            },
            saveEmail: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveEmail',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Email has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            SetAssign: function (datas, selected) {
                var data = { EmailCode: datas.EmailCode, ListAssignUser: selected };
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SetAssign',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Email has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', response.d);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteEmail: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteEmail',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Email has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditEmail: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditEmail',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $UpdateButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Email has been Update successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            SetAssignByEmailCode: function (EmailCode) {
                var data = { EmailCode: EmailCode };
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "POST",
                    "url": "../../webservice/WebService_COR.asmx/AssignUsers_ByEmailCode",
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    "success": function (msg) {
                        //console.info("msg", msg.d);
                        allUser = JSON.parse(msg.d);
                        for (var i = 0 ; i < allUser.length; i++) {
                            if (allUser[i]["checked"] == "true") {
                                $(i).addClass('selected');
                                //$(nRow).addClass('selected');
                                _dataSelect.push(i);
                            }
                        }
                        //popUpAssignUser();
                        _setCustomFunctions.showPopupAssignUser();


                        //$tableAssignUsers.DataTable().ajax.reload();
                    },
                    error: function (xhr, textStatus, error) {
                        alert("error");
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.DeleteEmail(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            },
            showPopupAssignUser: function () {
                //var table = $('#tblassignUsers').DataTable();
                //table.destroy();
                var tableHtml = "<thead style='font-size:10px'><tr>"
                    + '<th align="left"></th>'
                    + '<th align="left" hidden>UserID</th>'
                    + '<th align="left">Name</th>'
                    + '</thead>';

                for (var i = 0; i < allUser.length; i++) {
                    tableHtml += "<tr>"
                        + "<td align='center'></td>"
                        + "<td align='center' id='id' hidden>" + allUser[i]["UserID"] + "</td>"
                        + "<td align='center'>" + allUser[i]["UserName"] + "</td>"
                        + "</tr>";
                }

                $('#tblassignUsers').html(tableHtml);
                tableAssignUsers = $('#tblassignUsers').DataTable({
                    dom: 'lfrtip',
                    pageLength: 10,
                    destroy: true,
                    columnDefs: [{
                        orderable: false,
                        className: 'select-checkbox',
                        targets: 0
                    }, {
                        orderable: false,
                        className: 'select-checkbox',
                        "class": "center",
                        targets: 2
                    }],
                    select: {
                        style: 'multi',
                        selector: 'td:first-child',
                        rows: 'true'
                    },
                    order: [[1, 'asc']]
                });
                
                _dataSelect = _dataSelect.filter(function (elem, index, self) {
                    return index === self.indexOf(elem);

                });
                for (var x = 0; x < _dataSelect.length; x++) {
                    tableAssignUsers.row(_dataSelect[x]).select();
                    $(x).addClass('selected');
                }


                _dataEmail = [];
                for (var i = 0 ; i < allUser.length; i++) {
                    if (allUser[i]["checked"] == "true") {
                        _dataEmail.push(allUser[i]["UserID"]);
                    }
                }
                $.each(_dataEmail, function (i, el) {
                    if ($.inArray(el, _dataEmail) === undefined) _dataEmail.push(el);
                });
                if (_dataEmail.length != 0 && lengthbfr != _dataEmail.length) {
                    objEmail.push({ EmailCode: $("#SelectAssignUser").val(), ListUser: _dataEmail })
                    lengthbfr = _dataEmail.length;
                }

                tableAssignUsers.on("click", "th.select-checkbox", function () {
                    if ($("th.select-checkbox").hasClass("selected")) {
                        tableAssignUsers.rows().deselect();
                        $("th.select-checkbox").removeClass("selected");
                    } else {
                        tableAssignUsers.rows().select();
                        $("th.select-checkbox").addClass("selected");

                    }
                }).on("select deselect", function () {
                    ("Some selection or deselection going on")
                    var rowData = "";
                    if (tableAssignUsers.rows({ selected: true }).count() !== tableAssignUsers.rows().count()) {
                        $("th.select-checkbox").removeClass("selected");
                        //_dataEmail = [];
                        for (var i = 0 ; i < allUser.length; i++) {
                            if (allUser[i]["checked"] == "false") {
                                $("#" + allUser[i]["UserID"] + "").removeClass("selected");
                            }
                        }
                        if (_dataEmail != "") {
                            var currentId = tableAssignUsers.rows({ selected: true }).count();
                            for (var i = 0 ; i < currentId; i++) {
                                rowData = tableAssignUsers.rows({ selected: true }).data()[i];
                                if ($.inArray(rowData[1], _dataEmail) == -1) {
                                    _dataEmailRemove.push(parseInt(rowData[1]));
                                }
                                //alert(tableAssignUsers.rows({ selected: false }).data()[i]);
                                //if (_dataEmail[i] == parseInt(rowData[1])) {
                                // //delete objEmail[x];
                                //    _dataEmail.splice(i, 1);
                                //    break;
                                //}   

                            }
                            var currentId = tableAssignUsers.rows({ selected: true }).count();
                            for (var i = 0 ; i < currentId; i++) {
                                rowData = tableAssignUsers.rows({ selected: true }).data()[i];
                                //if (_dataEmail[i] == parseInt(rowData[1])) {
                                // //delete objEmail[x];
                                //    _dataEmail.splice(i, 1);
                                //    break;
                                //}   
                                _dataEmail.push(parseInt(rowData[1]));
                            }

                        }
                        else {
                            _dataEmail = [];
                            var currentId = tableAssignUsers.rows({ selected: true }).count();
                            for (var i = 0 ; i < currentId; i++) {
                                rowData = tableAssignUsers.rows({ selected: true }).data()[i];
                                //if (_dataEmail[i] == parseInt(rowData[1])) {
                                //    //delete objEmail[x];
                                //    _dataEmail.splice(i, 1);
                                //    break;
                                //}
                                _dataEmail.push(parseInt(rowData[1]));
                            }
                        }

                        //var currentId = $(".EmailMng").attr('id');
                        //_dataEmail.push(currentId);
                    } else {
                        $("th.select-checkbox").addClass("selected");
                        var currentId = tableAssignUsers.rows({ selected: true }).count();
                        for (var i = 0 ; i < currentId; i++) {
                            rowData = tableAssignUsers.rows({ selected: true }).data()[i];
                            if (_dataEmail[i] == parseInt(rowData[1])) {
                                //delete objEmail[x];
                                _dataEmail.splice(i, 1);
                                break;
                            }
                            _dataEmail.push(parseInt(rowData[1]));
                        }
                    }
                    //var index = objEmail.indexOf($("#SelectAssignUser").val());
                    var ValemailCode = $("#SelectAssignUser").val();
                    //delete objEmail.EmailCode.ValemailCode;
                    for (var x = 0; x < objEmail.length; x++) {
                        if (objEmail[x].EmailCode == ValemailCode) {
                            //delete objEmail[x];
                            objEmail.splice(x, 1);
                            break;
                        }
                    }
                    _dataEmail = _dataEmail.filter(function (elem, index, self) {
                        return index === self.indexOf(elem);

                    });
                    _dataEmailRemove = _dataEmailRemove.filter(function (elem, index, self) {
                        return index === self.indexOf(elem);

                    });
                    for (var i = 0; i < _dataEmail.length; i++) {
                        for (var x = 0; x < _dataEmailRemove.length; x++) {
                            if (_dataEmail[i] == _dataEmailRemove[x]) {
                                _dataEmail.splice(i, 1);
                            }
                        }
                    }

                    objEmail.push({ EmailCode: $("#SelectAssignUser").val(), ListUser: _dataEmail });

                });
            }
        }

    }

    Page.initialize();
})
