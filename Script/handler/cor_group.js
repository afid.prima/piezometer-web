﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddGroup',
            saveButton: '#btnSave',
            UpdateButton: '#btnUpdate',
            AddIPButton: '#btnAddIP',
            modalAdd: '#mdlAdd',
            tabGroup: '#tabGroup',
            table: '#tblgroup',
            tableIP: '#tblIPdetail',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            //this.setTableIP();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $btnAddIP = $(this.options.AddIPButton);
            $UpdateButton = $(this.options.UpdateButton);
            $modalAdd = $(this.options.modalAdd);
            $tabGroup = $(this.options.tabGroup);
            $table = $(this.options.table);
            $tableIP = $(this.options.tableIP);
            _formAdd = this.options.formAdd;
            _tabGroup = this.options.tabGroup;
            _listGroup = [];
            _listGroupCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 3,
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 5,
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 6,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 7,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetGroup_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblgroup").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $btnAddIP.on('click', this.saveIP);
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $table.on('click', 'a[data-target="#delete"]', this.delete);
                $tableIP.on('click', 'a[data-target="#deleteip"]', this.DeleteIP);

                $tabGroup.on('click', 'a', this.tabclick);
                //this.formvalidation();

            },
            formvalidation: function () {
                //Add On For Validation Rule
                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");
                //Main Validation
                $formAdd.validate({
                    rules: {
                        id: {
                            minlength: 1,
                            required: true,
                            remote: function () {
                                //document.getElementById('errormsg').style.display = 'none';
                                return {
                                    url: "../../webservice/WebService_COR.asmx/GetGroupDetail",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: "{'GroupID' :'" + $('input[name=id]').val() + "'}",
                                    dataFilter: function (data) {
                                        var msg = JSON.parse(data);
                                        var json = $.parseJSON(msg.d);
                                        if (json.length > 0) {
                                            //{
                                            document.getElementById('lblcode').style.color = '#dd4b39';
                                            document.getElementById('code').style.borderColor = '#dd4b39';
                                            //document.getElementById('help-block').style.display = 'block';
                                            document.getElementById('errormsg').style.display = 'block';
                                            document.all('errormsg').innerHTML = "Code sudah terdaftar pada sistem";
                                        }
                                    }
                                }


                            },
                        },
                        ipgroup: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });

            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                //e.preventDefault();

            },
            tabchange: function (tabId) {


            },
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                document.getElementById('lblcode').style.color = '#333';
                document.getElementById('code').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                document.getElementById('errormsg').style.display = 'none';
                $('input[name=id]').val("")
                $('input[name=name]').val("")
                $('input[name=desc]').val("")
                $('input[name=domainname]').val("")
                $('input[name=ipgroup]').val("")
                $("#tblIPdetail tr").remove();

                $('#SelectKabDesc').attr("disabled", false);
                $('input[name=code]').prop('readonly', false);
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Group";
                _setCustomFunctions.getIDGroup();

                _setEvents.formvalidation();
            },
            edit: function () {
                document.getElementById('lblcode').style.color = '#333';
                document.getElementById('code').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'inline';
                document.getElementById('btnSave').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Group";
                $("#tblIPdetail tr").remove();
                $modalAdd.modal('toggle');
                _setEvents.formvalidation();

                var $this = $(this);
                var $row = $(this).closest('tr');
                var GroupID = $row.find('.GroupID').html();

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetGroupDetail',
                    type: "POST",
                    data: "{'GroupID' :'" + GroupID + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {

                            $('input[name=id]').val(obj.GroupID);
                            $('input[name=codeHidden]').val(obj.GroupID);
                            $('input[name=name]').val(obj.GroupName);
                            $('input[name=desc]').val(obj.GroupDesc);
                            $('input[name=domainname]').val(obj.GroupDomain);
                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

                $('input[name=id]').val(GroupID);
                $('input[name=codeHidden]').val(GroupID);
                $('input[name=name]').val(GroupName);
                $('input[name=desc]').val(GroupDesc);
                $('input[name=domainname]').val(GroupDomain);
                _setCustomFunctions.SetTableIP(GroupID);

                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = { GroupID: $('input[name=id]').val(), GroupName: $('input[name=name]').val(), GroupDesc: $('input[name=desc]').val(), GroupDomain: $('input[name=domainname]').val() };
                //var savetype = 
                _setCustomFunctions.EditGroup(data);
                $modalAdd.modal('hide');
                //$("#tblIPdetail tr").ajax.reload();
                //$table.DataTable().ajax.reload();

                return false;

            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var GroupID = $row.find('.GroupID').html();
                //Set Ajax Submit Here
                var data = { GroupID: GroupID };
                //var savetype = 

                _setCustomFunctions.DeleteGroup(GroupID);

                return false;
            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", false);
                    var rowCount = $('#tblIPdetail tr').length;
                    //Set Ajax Submit Here
                    var data = { GroupID: $('input[name=id]').val(), GroupName: $('input[name=name]').val(), GroupDesc: $('input[name=desc]').val(), GroupDomain: $('input[name=domainname]').val() };
                    var dataIP = { GroupID: $('input[name=id]').val() };
                    //var savetype = 
                    _setCustomFunctions.saveGroup(data, rowCount);
                    $table.DataTable().ajax.reload();
                    //$('#mdlAdd').modal('hide');
                    $modalAdd.modal('hide');
                    return false;
                }
            },
            saveIP: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { GroupID: $('input[name=id]').val(), GroupIP: $('input[name=ipgroup]').val(), };
                    //var savetype = 
                    _setCustomFunctions.saveIPAddress(data);

                    return false;
                }
            },
            DeleteIP: function () {
                var par = $(this).parent().parent(); //tr
                par.remove();

                return false;

            }
        },
        setCustomFunctions: {
            init: function () {
                //this.getGroup();
                this.getMasterKabupaten();
                this.getIDGroup();
                //this.getMasterModuleType();
            },
            registerTabContent: function (tabId, KecCode) {
            },
            getGroup: function () {

            },
            getMasterKabupaten: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterKabupaten',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectKabDesc').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectKabDesc").append($('<option>', {
                                value: obj.KabCode,
                                text: obj.KabCode + ' - ' + obj.KabDesc
                            }));
                        })

                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            SetTableIP: function (datas) {
                $.ajax({
                    type: "POST",
                    url: "../../webservice/WebService_COR.asmx/GetGroupIP",
                    data: JSON.stringify({ data: datas }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //for (var i = 0; i < data.length; i++) {
                        var json = $.parseJSON(data.d);
                        var tables = $('#tblIPdetail');
                        json.forEach(function (obj) {
                            tables.append("<tr class='ipgroup'><td class='cahyo'>" + obj.IPAddress + "</td><td>" + "<a type='button' class='btn btn-danger btn-xs' data-title='DeleteIP' data-toggle='modal' data-target='#deleteip' ><span class='glyphicon glyphicon-trash'></span></a>" + "</td></tr>");
                        })
                        //console.log(data.d[i].ipgroup);

                        //}
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            },
            getIDGroup: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetNewID',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        //$('#SelectKabDesc').find('option').remove();
                        json.forEach(function (obj) {
                            $('input[name=id]').val($.parseJSON(obj.GroupID));
                            //$("#SelectKabDesc").append($('<option>', {
                            //    value: obj.KabCode,
                            //    text: obj.KabCode + ' - ' + obj.KabDesc
                            //}));
                        })

                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveGroup: function (datas, row_index) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveGroup',
                    data: JSON.stringify({ dataobject: JSON.stringify(datas) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            var $this = $(this);
                            for (var i = 0; i < row_index; i++) {
                                var IPGroup = $('#tblIPdetail')[0].rows[i].innerText;
                                var IP = IPGroup.replace("/t", "");
                                var IPGroups = IP.trim();
                                var data = { GroupID: datas.GroupID, GroupIP: IPGroups, };
                                $.ajax({
                                    type: 'POST',
                                    url: '../../webservice/WebService_COR.asmx/SaveGroupIP',
                                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                                    contentType: 'application/json; charset=utf-8',
                                    dataType: 'json',
                                    success: function (response) {
                                        if (response.d == 'success') {
                                            $(_formAdd + " :input").prop("disabled", false);
                                        } else {
                                            $(_formAdd + " :input").prop("disabled", false);
                                        }
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                    }
                                });
                            }
                            _setCustomFunctions.showPopup('Info', 'Group has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteGroup: function (data) {
                console.log(data);
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteGroup',
                    data: JSON.stringify({ data: data }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'text',
                    success: function (response) {
                        if (response == '{"d":"success"}') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            //_setCustomFunctions.getGroup();
                            _setCustomFunctions.showPopup('Info', 'Group has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditGroup: function (datas) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditGroup',
                    data: JSON.stringify({ dataobject: JSON.stringify(datas) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = [];
                        $UpdateButton.html("Update");
                        if (response.d == 'success') {
                            var $this = $(this);
                            var itArr = [];
                            var rowCount = $('#tblIPdetail tr').length;
                            var tbl2 = $('#tblIPdetail tr').each(function (i) {
                                x = $(this).find('.cahyo');
                                x.each(function () {
                                    var tmp = $(this).text();
                                    itArr.push(tmp);
                                });
                                json = itArr;
                            })
                            var myTableArray = [];
                            //console.log(json);
                            var data = { GroupID: datas.GroupID, ListGroupIP: json };
                            $.ajax({
                                type: 'POST',
                                url: '../../webservice/WebService_COR.asmx/UpdateGroupIP',
                                data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                success: function (response) {
                                    if (response == '{"d":"success"}') {
                                        $(_formAdd + " :input").prop("disabled", false);
                                        $(_formAdd + " :input").prop("disabled", false);
                                    } else {
                                        $(_formAdd + " :input").prop("disabled", false);
                                    }
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                }
                            });

                            _setCustomFunctions.showPopup('Info', 'Group has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                        console.log("sadad");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveIPAddress: function (data) {
                var count = 1;
                var id = $('input[name=id]').val();
                var ipgroup = $('input[name=ipgroup]').val();


                var tables = $('#tblIPdetail');
                tables.append("<tr class='ipgroup'><td class='cahyo'>" + ipgroup + "</td><td>" + "<a type='button' class='btn btn-danger btn-xs' data-title='DeleteIP' data-toggle='modal' data-target='#deleteip' ><span class='glyphicon glyphicon-trash'></span></a>" + "</td></tr>");
                //});
                $(_formAdd + " :input").prop("disabled", false);


            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                                $table.DataTable().ajax.reload();
                            }
                        }
                    }
                });
            }
        }

    }

    Page.initialize();
})