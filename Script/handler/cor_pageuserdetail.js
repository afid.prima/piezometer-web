﻿function SetupUserDetail() {
    //document.getElementById('Profile').style.display = 'inline';
    var _dataTable = [];
    var _dataTableExisting = [];
    var _dataTree = []; 
    var _dataTreeExisting = [];
    var _dataTrees = "";
    var _defaultProfilePict = "";
    var Page = {
        options: {
            tableUser: '#tblusers',
            saveButton: '#btnSave',
            formAdd: '#formAdd',
            modalAdd: '#mdlAdd',
            modalUser: '#mdlUser',
            setAccessButton: '#btnSetAccess',
            setmdlButton: '#btnSet', 
            RequestButton: '#btnRequest',
            SaveRequestButton: '#btnSaveRequest',

        },
        initialize: function () {
            this.setTableUserAcc();
            this.setVars();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $tableUser = $(this.options.tableUser);
            _setEvents = this.setEvents;
            $saveButton = $(this.options.saveButton);
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
            $modalAdd = $(this.options.modalAdd);
            $modalUser = $(this.options.modalUser);
            $setAccessButton = $(this.options.setAccessButton); 
            $setmdlButton = $(this.options.setmdlButton); 
            $RequestButton = $(this.options.RequestButton); 
            $SaveRequestButton = $(this.options.SaveRequestButton);
        },
        setTableUserAcc: function () {
            var index = "";
            var table = $('#tblusers').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 100,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 3
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetModule3_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblusers").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);

                    var $row = $(this).closest('tr');
                    //mdlAccCode = aData[3].find('.' + aData[1] + '_Access').text();
                    //console.log('aaa', $(aData[3]));
                    //console.log('bbb', $(aData[3]).next().html());
                    //console.log('xxx', $(aData[3]).find('div#API').html());

                    _dataTableExisting.push($(aData[3]).next().html());
                }
            });
        },
        
        setEvents: {
            init: function () {
                $saveButton.on('click', this.save);
                $tableUser.on('click', '.Access', this.ShowAccess);
                $tableUser.on('click', 'a[data-target="#edit"]', this.editAccess);
                $setAccessButton.on('click', this.setAccess);
                $setmdlButton.on('click', this.savemdlAcc);
                $RequestButton.on('click', this.RequestAccess); 
                $SaveRequestButton.on('click', this.SaveRequestButton);
                this.formvalidation($formAdd);
            },
            formvalidation: function (form) {
                //Main Validation
                $.validator.addMethod("check", function (value, element) {

                    return this.optional(element) || $('input[name=Newpassword]').val() == $('input[name=Confirmpassword]').val()

                }, "Kata Sandi Tidak Sama");

                form.validate({
                    rules: {
                        Oldpassword: {
                            required: true,
                            remote: function () {
                                return {

                                    url: "../../webservice/WebService_COR.asmx/CheckPasswordFromProfile",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: JSON.stringify({ UserPass: $('input[name=Oldpassword]').val() }),
                                    dataFilter: function (data) {
                                        var msg = JSON.parse(data);
                                        return msg.d;
                                    }
                                }
                            }
                        },
                        Newpassword: {
                            required: true,
                            minlength: 5

                        },
                        Confirmpassword: {
                            required: true,
                            minlength: 5,
                            check: true
                        }
                    },
                    messages: {
                        Oldpassword: {
                            remote: 'Kata Sandi Salah',
                            required: 'Minimal Kata Sandi 5 Huruf'
                        },
                        Newpassword: {
                            required: 'Minimal Kata Sandi 5 Huruf'
                        },
                        Confirmpassword: {
                            required: 'Minimal Kata Sandi 5 Huruf'
                        },
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
            },
            save: function () {
                //Event for saving data
                    var $this = $(this);
                    //alert($('input[name=password]').val());
                    //Set Ajax Submit Here
                    //var data = { CountryCode: $('input[name=password]').val()};
                //var savetype = 
                    if ($('input[name=Newpassword]').val() == $('input[name=Confirmpassword]').val())
                    {
                        _setCustomFunctions.changePassword($('input[name=Newpassword]').val());
                    }
                    else
                    {
                        alert("Confirm Password not match!");
                    }

                    return false;
                
            },
            editAccess: function () {
                var $row = $(this).closest('tr');
                mdlCode = $row.find('.mdlCode').html();
                mdlAccCode = $row.find('.mdlCode_Access').html();
                _setCustomFunctions.getAccess(mdlCode, mdlAccCode);
                $modalUser.modal('toggle');
            },
            ShowAccess: function () {
                $modalAdd.modal('toggle');
                var $row = $(this).closest('tr');
                var mdlCode = $row.find('.mdlCode').html();
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetUserAccess',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: "{'mdlCode' :'" + mdlCode + "'}",
                    success: function (response) {
                        var tables = $('#tblAcc');
                        $("#tblAcc thead").remove();
                        $("#tblAcc tr").remove();
                        var json = $.parseJSON(response.d);
                        tables.append("<thead><tr><th>Code</th><th>Name</th><th>Desc</th></tr></thead>");
                        json.forEach(function (obj) {
                            tables.append("<tr><td>" + obj.mdlAccCode + "</td><td>" + obj.mdlAccDesc + "</td><td>" + obj.mdlAccInfo + "</td></tr>");


                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            setAccess: function () {
                var $row = $(this).closest('tr');
                mdlCode = $('input[name=code]').val();
                mdlAccCode = $('#SelectAccess option:selected').text();
                mdlAccCodes = $('#SelectAccess option:selected').val();
                $('#' + mdlCode).html(mdlAccCode);
                $('#' + mdlCode + "_Access").html(mdlAccCodes);
                $modalUser.modal('hide');
            },
            savemdlAcc: function () {
                var datatable = [];
                var datas = [];
                var table = $('#tblusers').DataTable();

                var headers = [];
                $('#tblusers th').each(function (index, item) {
                    headers[index] = $(item).html();
                });
                $('#tblusers tr').has('td').each(function () {
                    var arrayItem = {};
                    $('td', $(this)).each(function (index, item) {
                        arrayItem[headers[index]] = $(item).html();
                    });
                    datatable.push(arrayItem);
                });

                var $row = $(this).closest('tr');
                _dataTable = [];
                for (var i = 0 ; i < datatable.length; i++) {
                    var a = jQuery(datatable[i].Access).text();
                    var lastFive = a.substr(a.length - 4);

                    _dataTable.push(lastFive);
                }
                _setCustomFunctions.saveUser(_dataTable, _dataTree);
            },
            RequestAccess: function () {
                //var foo = document.getElementById("tvestate");

                //foo.removeAttribute('jstree-checkbox-disabled');
                //foo.classList.remove("jstree-checkbox-disabled");
                document.getElementById('tvestate2').style.display = 'none';
                document.getElementById('tvestate1').style.display = 'inline';
            },
            SaveRequestButton: function () {
                //console.info(_dataTree, "");
                            var datasw = { Code: "", ListmdlEstAccess: _dataTree };
                            $.ajax({
                                type: 'POST',
                                url: '../../webservice/WebService_COR.asmx/RequestEstAccess',
                                data: JSON.stringify({ dataobject: JSON.stringify(datasw) }),
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                success: function (response) {
                                    if (response.d == 'success') {
                                        _setCustomFunctions.showPopup('Info', 'Request Access has been successfully');
                                        document.getElementById('tvestate2').style.display = 'inline';
                                        document.getElementById('tvestate1').style.display = 'none';

                                    } else {
                                        _setCustomFunctions.showPopup('Info', 'Failed to save');
                                    }
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                }
                            });
            }
        },
        setCustomFunctions: {
            init: function () {
                this.getAccess();
                this.getDepartment();
                this.setTree1();
                this.setTree2();
                this.getDataUser();
            },
            getDataUser: function () {
                var _parent = this;
                var userID = 10;
                var _path = window.location.pathname.substring(0, 10);
                var url = location.protocol + "//" + location.host + _path;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetUserDetail',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        json.forEach(function (obj) {
                            //$('#lblUserEmail').val(obj.UserEmail);
                            //document.getElementById("lblUserEmail").value = obj.UserEmail;
                            document.all('lblUserEmail').innerHTML = obj.UserEmail;
                            document.all('lblUserOfficeAddress').innerHTML = obj.UserOfficeAddr;
                            document.all('lblUserFullName').innerHTML = obj.UserFullName;
                            document.all('lblUserDepartment').innerHTML = obj.DeptName;
                            document.all('lblUserPhone').innerHTML = obj.UserHP;
                            document.all('lblEstAccess').innerHTML = obj.estAccess;
                            document.all('lblMdlAccess').innerHTML = obj.mdlAccess; 
                            document.all('lblUserGroup').innerHTML = obj.usrGroupDesc;


                            var _url = "Upload//UploadProfilePictCompress//" + obj.UserAvatar;
                            $.ajax({
                                type: 'POST',
                                url: '../../webservice/WebService_COR.asmx/CheckImageExist',
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                data: "{'url' :'" + _url + "'}",
                                success: function (response) {
                                    if (response.d == "true") {
                                        var imgurl = url + "/Upload/UploadProfilePictCompress/" + obj.UserAvatar;
                                        $("#profileImage1").attr("src", imgurl);
                                        //document.getElementById("profileImage1").src = "../../Upload/UploadProfilePictCompress/" + obj.UserAvatar;

                                        var yourImg = document.getElementById('profileImage1');
                                        //var yourImg = document.getElementById('profileImage3');
                                        if (yourImg && yourImg.style) {
                                            yourImg.style.height = '95px';
                                            yourImg.style.width = '95px';
                                        }
                                    }
                                    else {
                                        if (obj.UserGender == "Male") {
                                            var imgurl = url + "/Upload/UploadProfilePictCompress/" + obj.UserAvatar;
                                            $("#profileImage1").attr("src", imgurl);

                                            var yourImg = document.getElementById('profileImage1');
                                            //var yourImg = document.getElementById('profileImage3');
                                            if (yourImg && yourImg.style) {
                                                yourImg.style.height = '95px';
                                                yourImg.style.width = '95px';
                                            }
                                        }
                                        else {
                                            var imgurl = url + "/Upload/UploadProfilePictCompress/avatar2.png";
                                            $("#profileImage1").attr("src", imgurl);

                                            var yourImg = document.getElementById('profileImage1');
                                            //var yourImg = document.getElementById('profileImage3');
                                            if (yourImg && yourImg.style) {
                                                yourImg.style.height = '95px';
                                                yourImg.style.width = '95px';
                                            }
                                        }
                                    }

                                }

                            });
                            var imgurl = url + "/Upload/UploadProfilePictCompress/" + obj.UserAvatar;
                            $("#profileImage1").attr("src", imgurl);
                            _defaultProfilePict = obj.UserAvatar;
                            var yourImg = document.getElementById('profileImage1');
                            if (yourImg && yourImg.style) {
                                yourImg.style.height = '95px';
                                yourImg.style.width = '95px';
                            }
                            //$('input[name=Oldpassword]').val(obj.UserPass);
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getAccess: function (mdlCode, mdlAccCode) {
                $('input[name=mdlacccoder]').val(mdlAccCode);
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterAccess',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'mdlCode' :'" + mdlCode + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectAccess').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectAccess").append($('<option>', {
                                value: obj.mdlAccCode,
                                text: obj.mdlAccDesc
                            }));
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
                $('input[name=code]').val(mdlCode);

            },
            getDepartment: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterDepartment',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectDept').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectDept").append($('<option>', {
                                value: obj.DeptCode,
                                text: obj.DeptCode + ' - ' + obj.DeptName
                            }));
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            setTree1: function () {
                var datas = [];
                var datas2 = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetEstAccessUserBySession',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "GET",
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0 ; i < json.length; i++) {
                            var parent = json[i].parent
                            datas2.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false, checkbox_disabled: true } });
                        }

                        _dataTablebefore = datas2;
                        var $treeview = $("#tvestate2");
                        $('#tvestate2').jstree('destroy');
                        $('#tvestate2').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                "disabled": {
                                    "check_node": true,
                                    "uncheck_node": true
                                },
                                'data':
                                    datas2
                            },

                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            var element = document.getElementById("tvestate2");
                            $treeview.jstree('open_all');
                        });

                        $('#tvestate2')
                        // listen for event
                        .on('changed.jstree', function (e, data) {
                            _dataTreeExisting = data.instance.get_bottom_selected();
                        })
                        // create the instance
                        .jstree();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }


                });

            },
            setTree2: function () {
                var datas = [];
                var datas2 = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetEstAccessUserBySession',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "GET",
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0 ; i < json.length; i++) {
                            var parent = json[i].parent
                            datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }


                        _dataTablebefore = datas;
                        var $treeview = $("#tvestate1");
                        $('#tvestate1').jstree('destroy');
                        $('#tvestate1').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                "disabled": {
                                    "check_node": true,
                                    "uncheck_node": true
                                },
                                'data':
                                    datas
                            },

                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                        });

                        $('#tvestate1')
                        // listen for event
                        //var node = $treeview.jstree(true).find('#jstree-last');
                        //alert(node);
                        .on('changed.jstree', function (e, data) {
                            //var i, j, r = [];
                            _dataTree = [];
                            _dataTree = data.instance.get_bottom_selected();

                        })
                        // create the instance
                        .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }


                });

            },
            changePassword: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/ChangePasswordProfile',
                    data: "{'Password' :'" + data + "'}",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            history.go(0); //forward
                            //$(_formAdd + " :input").prop("disabled", false);
                            //$table.DataTable().ajax.reload();
                            //$modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Password has been change successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            //_setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveUser: function (_dataTable, _dataTree) {
                var datas = { Text: "", ListmdlEstAccess: _dataTree, ListmdlAccess: _dataTable, ListmdlAccessExisting: _dataTableExisting, ListmdlEstAccessExisting: _dataTreeExisting };
                var objSend = [];
                var objS = [];
                objS.push({ ListmdlAccessExisting: _dataTableExisting });
                objS.push({ ListmdlAccess: _dataTable });
                objSend.push(objS);

                objS = [];
                objS.push({ ListmdlEstAccessExisting: _dataTreeExisting });
                objS.push({ ListmdlEstAccess: _dataTree });
                objSend.push(objS);
                console.info(objSend);
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/RequestAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(objSend) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            _setCustomFunctions.showPopup('Info', 'Request Access has been successfully');
                            document.getElementById('tvestate2').style.display = 'inline';
                            document.getElementById('tvestate1').style.display = 'none';
                            history.go(0); //forward

                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
        }

    }

    Page.initialize();
}
//$("#profileImage1").click(function (e) {
//    $("#imageUpload").click();
//});

(function () {
    document.getElementById("profileImage1").onclick = test_click;
    function test_click() {
        //document.getElementById("test").innerHTML = "HI";
        $("#imageUpload").click();
    }
})();


function fasterPreview(uploader) {
    if (uploader.files && uploader.files[0]) {
        $('#profileImage1').attr('src',
           window.URL.createObjectURL(uploader.files[0]));
    }
}

//$("#imageUpload").change(function () {
//    //alert(files[0].size);

//});


(function () {
    document.getElementById("imageUpload").onchange = test_click2;
    function test_click2() {
        //document.getElementById("test").innerHTML = "HI";
        fasterPreview(this);
        var _idPict = this.files && this.files.length ? this.files[0].name.split('.')[0] : '';
        var _ext = imageUpload.value.split('.')[1];
        var FileName = _idPict + "." + _ext
        ValidateFileUpload(_idPict);
    }
})();

function GotoBeforeProfilePict() {
    $.ajax({
        type: 'GET',
        url: '../../webservice/WebService_COR.asmx/GetUserDetail',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            json.forEach(function (obj) {
                document.getElementById("profileImage1").src = "../../Upload/UploadProfilePictCompress/" + obj.UserAvatar
                var yourImg = document.getElementById('profileImage1');
                if (yourImg && yourImg.style) {
                    yourImg.style.height = '95px';
                    yourImg.style.width = '95px';
                }
            })

        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function ValidateFileUpload(_idPict) {

    var fuData = document.getElementById('imageUpload');
    var FileUploadPath = fuData.value;


    if (FileUploadPath == '') {
        alert("Please upload an image");

    } else {
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();



        if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {


            if (fuData.files && fuData.files[0]) {

                var size = fuData.files[0].size;

                //if (size > 9500) {
                //    alert("Maximum file size exceeds");
                //    GotoBeforeProfilePict();
                //    return;
                //} else {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(fuData.files[0]);
                    saveDataUpload(_idPict, Extension);
                //}
            }

        }


        else {
            alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
            GotoBeforeProfilePict();
        }
    }
}

function saveDataUpload(_idPict, Extension)
{
    var msg = document.getElementById('lblUserFullName').innerHTML;
    var _ext = imageUpload.value.split('.')[1];
    var FileName = _idPict + "." + _ext
    var filename = $('#imageUpload').get(0);
    var files = filename.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(FileName, files[i]);

    }
    $.confirm({
        title: "Confirm",
        content: "Are You Sure To Change Profile Picture",
        type: 'blue',
        typeAnimated: true,
        buttons: {
            YES: {
                text: 'YES',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        type: 'POST',
                        url: '../../Service/ImportHandler.ashx?w=10&x=10',
                        data: data,
                        contentType: false,
                        processData: false,

                        success: function (response) {
                            $.ajax({
                                type: 'POST',
                                url: '../../webservice/WebService_COR.asmx/SaveProfilePict',
                                data: "{'FileName' :'" + msg + "','Extension' :'" + Extension + "'}",
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                success: function (response) {
                                    $saveButton.html("Save");
                                    if (response.d == 'success') {
                                        history.go(0); //forward
                                        _setCustomFunctions.showPopup('Info', 'Profile Pict has been change successfully');
                                    } else {
                                        //_setCustomFunctions.showPopup('Info', 'Failed to save');
                                    }
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                }
                            });
                        }

                    });
                }
            },
            NO: {
                text: 'NO',
                btnClass: 'btn-blue',
                action: function () {
                    GotoBeforeProfilePict();
                }
            }
        }
    });
}