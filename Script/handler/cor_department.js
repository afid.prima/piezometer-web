﻿$(function () {
    var _globalSectionCode;
    var _arrayID = [];
    var _arrayExpired = [];
    var actionTableUpdate = [];
    var actionTableUnregister = [];
    var Page = {
        options: {
            formAdd: '#formAdd',
            formAddSection: '#formAddSection',
            formAddSubSection: '#formAddSubSection',
            addButton: '#btnAddDepartment',
            addSectionButton: '#btnAddSection',
            saveButton: '#btnSave',
            saveSectionButton: '#btnSaveSection',
            saveSubSectionButton: '#btnSaveSubSection',
            UpdateSectionButton: '#btnUpdateSection',
            UpdateSubSectionButton: '#btnUpdateSubSection',
            modalAdd: '#mdlAdd',
            modalAddSection: '#mdlAddSection',
            modalAddSubSection: '#mdlAddSubSection',
            tabDepartment: '#tabDepartment',
            table: '#tbldepartment',
            tablesection: '#tbldepartmentsection',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $formAddSection = $(this.options.formAddSection);
            $formAddSubSection = $(this.options.formAddSubSection);
            $addButton = $(this.options.addButton);
            $addSectionButton = $(this.options.addSectionButton);
            $UpdateSectionButton = $(this.options.UpdateSectionButton);
            $UpdateSubSectionButton = $(this.options.UpdateSubSectionButton);
            $saveButton = $(this.options.saveButton);
            $saveSectionButton = $(this.options.saveSectionButton);
            $saveSubSectionButton = $(this.options.saveSubSectionButton);
            $modalAdd = $(this.options.modalAdd);
            $modalAddSection = $(this.options.modalAddSection);
            $modalAddSubSection = $(this.options.modalAddSubSection);
            $tabDepartment = $(this.options.tabDepartment);
            $table = $(this.options.table);
            $tablesection = $(this.options.tablesection);
            _formAdd = this.options.formAdd;
            _formAddSection = this.options.formAddSection;
            _formAddSubSection = this.options.formAddSubSection;
            _tabDepartment = this.options.tabDepartment;
            _listDepartment = [];
            _listDepartmentCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetDepartment_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tbldepartment").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $addSectionButton.on('click', this.addsection);
                $saveButton.on('click', this.save);
                $saveSectionButton.on('click', this.saveSection);
                $saveSubSectionButton.on('click', this.saveSubSection);
                $UpdateSectionButton.on('click', this.saveEditsection);
                $UpdateSubSectionButton.on('click', this.saveEditSubsection);
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $table.on('click', 'a[data-target="#delete"]', this.delete);

                $tabDepartment.on('click', 'a', this.tabclick);
                //this.formvalidation();

                //select event
                $('#selectType').on('change', this.selectchange);
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 1,
                            maxlength: 3,
                            required: true,
                            alpha: true
                        },
                        name: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });

                $formAddSection.validate({
                    rules: {
                        sectioncode: {
                            minlength: 1,
                            maxlength: 3,
                            required: true
                        },
                        sectionname: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });

                $formAddSubSection.validate({
                    rules: {
                        subsectioncode: {
                            minlength: 1,
                            maxlength: 3,
                            required: true
                        },
                        subsectionname: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                $(this).tab('show');
            },
            tabchange: function (tabId) {
                $(_tabDepartment + ' a[href="#' + tabId + '"]').tab('show');
            },
            tabclose: function () {
                $.confirm({
                    title: 'Confirmation',
                    content: 'You are editing something. Are you sure to close existing edit ? ',
                    buttons: {
                        cancel: function () {

                        },
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                _currentEditTabId = "";
                                _isEditTabExists = false;

                                //there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
                                var tabContentId = $(".closeTab").parent().attr("href");
                                $(".closeTab").parent().parent().remove(); //remove li of tab
                                $(_tabDepartment + ' a:first').tab('show'); // Select first tab
                                $(tabContentId).remove(); //remove respective tab content
                            }
                        }
                    }
                });
            },
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                document.getElementById('btnSave').style.display = 'inline';
                $('input[name=code]').val('');
                $('input[name=codeHidden]').val('');
                $('input[name=name]').val('');

                _setEvents.formvalidation();
            },
            edit: function () {
                document.getElementById('btnUpdate').style.display = 'inline';

                //$modalAdd.modal('toggle');
                //_setEvents.formvalidation();

                var $this = $(this);
                var $row = $(this).closest('tr');
                var DeptCode = $row.find('.DeptCode').html();
                var DeptName = $row.find('.DeptName').html();
                $("#tbodyid").empty();

                var tabId = "edit" + DeptCode;
                if ($(_tabDepartment + ' .nav-tabs a[href="#' + tabId + '"]').length) {
                    _setEvents.tabchange(tabId);
                    _setCustomFunctions.setTableSection($('input[name=codeEditHidden]').val());
                    $(".addnew").on('click', _setEvents.addsection);
                    _setEvents.formvalidation();
                    $(".editDept").on('click', _setEvents.saveEdit);
                } else {
                    if (_isEditTabExists) {
                        $.confirm({
                            title: 'Confirmation',
                            content: 'You are editing something. Are you sure to close existing edit ? ',
                            buttons: {
                                GoToTabEditing: {
                                    text: 'Go To Editing Tab',
                                    btnClass: 'btn-orange',
                                    action: function () {
                                        _setEvents.tabchange(_currentEditTabId);
                                        _setCustomFunctions.getDepartment($('input[name=codeEditHidden]').val());
                                        _setCustomFunctions.setTableSection($('input[name=codeEditHidden]').val());
                                    }
                                },
                                cancel: function () {

                                },
                                OK: {
                                    text: 'OK',
                                    btnClass: 'btn-blue',
                                    action: function () {
                                        //there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
                                        var tabContentId = _currentEditTabId;
                                        $('#tabDepartment .nav-tabs a[href="#' + tabContentId + '"]').parent().remove(); //remove li of tab
                                        $('#tabDepartment a:first').tab('show'); // Select first tab
                                        $('#' + tabContentId).remove(); //remove respective tab content


                                        _isEditTabExists = true;
                                        _currentEditTabId = tabId;
                                        $('#tabDepartment .nav-tabs').append('<li><a href="#' + tabId + '" data-togle="tab"><button class="close closeTab" type="button" >×</button>Edit - ' + DeptName + '</a></li>');
                                        $('#tabDepartment .tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

                                        _setCustomFunctions.registerTabContent(tabId, DeptCode);
                                        _setEvents.tabchange(tabId);
                                        _setCustomFunctions.getDepartment(DeptCode);
                                        _setCustomFunctions.setTableSection(DeptCode);
                                        $(".addnew").on('click', _setEvents.addsection);
                                        _setEvents.formvalidation();
                                        $(".editDept").on('click', _setEvents.saveEdit);
                                        $(".closeTab").on('click', _setEvents.tabclose);
                                    }
                                }
                            }
                        });
                    } else {
                        _isEditTabExists = true;
                        _currentEditTabId = tabId;
                        $('#tabDepartment .nav-tabs').append('<li><a href="#' + tabId + '" data-togle="tab"><button class="close closeTab" type="button" >×</button>Edit - ' + DeptName + '</a></li>');
                        $('#tabDepartment .tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

                        _setCustomFunctions.registerTabContent(tabId, DeptCode);
                        _setEvents.tabchange(tabId);
                        _setCustomFunctions.getDepartment(DeptCode);
                        _setCustomFunctions.setTableSection(DeptCode);
                        $(".addnew").on('click', _setEvents.addsection);
                        _setEvents.formvalidation();
                        $(".editDept").on('click', _setEvents.saveEdit);
                        $(".closeTab").on('click', _setEvents.tabclose);
                    }
                }

                //$(".deletesec").on('click', _setEvents.deletesection);

                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data

                //var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = { DeptCode: $('input[name=codeEditHidden]').val(), NewDeptCode: $('input[name=codeEdit]').val(), DeptName: $('input[name=nameEdit]').val() };
                //var savetype = 
                _setCustomFunctions.EditDepartment(data);
                $modalAdd.modal('hide');

                return false;

            },
            saveEditsection: function () {
                //Event for deleting data
                var $this = $(this);
                $("#formAddSection :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = { SectionCode: $('input[name=sectioncodeHidden]').val(), NewSectionCode: $('input[name=sectioncode]').val(), SectionName: $('input[name=sectionname]').val() };
                //var savetype = 
                _setCustomFunctions.EditSectionDepartment(data);
                //return false;

            },
            saveEditSubsection: function () {
                //Event for deleting data
                var $this = $(this);
                $("#formAddSubSection :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = { SubSectionCode: $('input[name=subsectioncodeHidden]').val(), NewSubSectionCode: $('input[name=subsectioncode]').val(), SubSectionName: $('input[name=subsectionname]').val() };
                //var savetype = 
                _setCustomFunctions.EditSubSection(data);
                //return false;

            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var DeptCode = $row.find('.DeptCode').html();
                //Set Ajax Submit Here
                var data = { DeptCode: DeptCode };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(1, data, '', 'confirm', 'Are You Sure Delete This Data?');

                return false;
            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { DeptCode: $('input[name=code]').val(), DeptName: $('input[name=name]').val() };
                    //var savetype = 
                    _setCustomFunctions.saveDepartment(data);
                    $modalAdd.modal('hide');

                    return false;
                }
            },
            saveSection: function () {
                //Event for saving data
                if ($formAddSection.valid()) {
                    var $this = $(this);
                    $("#formAddSection :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { SectionCode: $('input[name=sectioncode]').val(), SectionName: $('input[name=sectionname]').val(), DeptCode: $('input[name=code]').val() };
                    //var savetype = 
                    _setCustomFunctions.saveSectionDepartment(data);

                    return false;
                }   
            },
            saveSubSection: function () {
                if ($formAddSubSection.valid()) {
                    var $this = $(this);
                    $("#formAddSubSection :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { SectionCode: _globalSectionCode, SubSectionName: $('input[name=subsectionname]').val(), SubSectionCode: $('input[name=subsectioncode]').val() };
                    //var savetype = 
                    _setCustomFunctions.saveSubSection(data);

                    return false;
                }
            },
            addsection: function () {
                document.getElementById('btnUpdateSection').style.display = 'none';
                document.getElementById('btnSaveSection').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Section"
                //Event for adding data
                //$modalAddSection.modal('toggle');codeEditHidden
                var kodeDept = $('input[name=codeEditHidden]').val();
                $modalAddSection.modal('toggle');
                $('input[name=sectioncode]').val('');
                $('input[name=sectioncodeHidden]').val('');
                $('input[name=sectionname]').val('');
                $('input[name=code]').val(kodeDept);

                _setEvents.formvalidation();
            },
            editsection: function () {
                document.getElementById('btnUpdateSection').style.display = 'inline';
                document.getElementById('btnSaveSection').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Section"
                var $this = $(this);
                var $row = $(this).closest('tr');
                var SectionCode = $row.find('.sectioncode').html();
                $modalAddSection.modal('toggle');

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetDepartmentSection',
                    type: "POST",
                    data: "{'SectionCode' :'" + SectionCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=sectioncode]').val(obj.SectionCode);
                            $('input[name=sectioncodeHidden]').val(obj.SectionCode);
                            $('input[name=sectionname]').val(obj.SectionName);
                            $('input[name=code]').val(obj.DeptCode);
                            //$('input[name=module]').val(obj.mdlDesc);

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

            },

            deletesection: function () {
                var $this = $(this);
                var $row = $(this).closest('tr');
                var SectionCode = $row.find('.sectioncode').html();
                var DeptCode = $('input[name=code]').val();
                var data = { SectionCode: SectionCode };

                _setCustomFunctions.showConfirmPopup(2, data, DeptCode, 'confirm', 'Are You Sure Delete This Data Section?');

                return false;
            },
            detailmodule: function () {
                var trx = $(this).closest('tr');
                var rowx = $tablesection.DataTable().row(trx);
                //var UserID = 1979;

                if (rowx.child.isShown()) {
                    // This row is already open - close it
                    rowx.child.hide();
                    trx.removeClass('shown');
                }
                else {
                    // Open this row
                    $.ajax({
                        url: '../../webservice/WebService_COR.asmx/GetMasterID_ServerSideProcessing',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: "POST",
                        data: JSON.stringify({ UserID: UserID }),
                        success: function (response) {
                            var json = $.parseJSON(response.d);
                            // iterate over each element in the array
                            _arrayID = [];
                            for (var i = 0; i < json.length; i++) {
                                // look for the entry with a matching `code` value
                                _arrayID.push(json[i].UserFullName)
                                _arrayExpired.push({ name: json[i].UserFullName, expiredDate: json[i].EXPIRATION_DATE });
                            }

                            _setCustomFunctions.format(row.child, UserID)
                            trx.addClass('shown');
                        }
                    });

                }
            },
        },
        setCustomFunctions: {
            init: function () {
                //this.getCountry();
                //this.getMasterModuleGroup();
                //this.getMasterModuleType();
            },
            setTableSection: function (DeptCode) {
                $.ajax({
                    type: "POST",
                    url: "../../webservice/WebService_COR.asmx/GetDepartmentSection_ServerSideProcessing",
                    data: JSON.stringify({ data: DeptCode }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //for (var i = 0; i < data.length; i++) {
                        var json = $.parseJSON(data.d);
                        var tables = $('#tbldepartmentsection');
                        var _no = 1;
                        json.forEach(function (obj) {
                            tables.append("<tr class='departmentsection'><td></td><td class='sectioncode'>" + obj.SectionCode + "</td><td class='sectionname'>" + obj.SectionName + "</td><td align='center'>" + "<a type='button' class='btn btn-primary btn-xs editsec' data-title='editsection' data-toggle='modal'><span class='glyphicon glyphicon-pencil'></span></a>" + "</td><td align='center'>" + "<a type='button' class='btn btn-danger btn-xs deletesec' data-title='DeleteSection' data-toggle='modal' data-target='#deletesection' ><span class='glyphicon glyphicon-trash'></span></a>" + "</td></tr>");
                            _no++;
                        })

                        tablesection = $('#tbldepartmentsection').DataTable({
                            dom: 'lfrtip',
                            pageLength: 10,
                            columnDefs: [{
                                "searchable": false,
                                "orderable": false,
                                "class": "details-control",
                                "data": null,
                                "defaultContent": '',
                                "targets": 0,
                                width: 5
                            }],
                            order: [[1, 'asc']]
                        });

                        //$(".details-control").on('click', _setEvents.detailmodule);
                        $('#tbldepartmentsection').on('click', 'td.details-control', function () {
                            var tr = $(this).closest('tr');
                            var SectionCode = tr.find('.sectioncode').html();
                            var row = tablesection.row(tr);

                            if (row.child.isShown()) {
                                //This row is already open - close it
                                row.child.hide();
                                tr.removeClass('shown');
                            } else {
                                //Open this row
                                _setCustomFunctions.format(row.child, SectionCode)
                                tr.addClass('shown');
                            }

                        });

                        //console.log(data.d[i].ipgroup);
                        $(".editsec").on('click', _setEvents.editsection);
                        $(".deletesec").on('click', _setEvents.deletesection);

                        //}
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            },
            registerTabContent: function (tabId, DeptCode) {
                $('.divtemplate').clone().appendTo($('#' + tabId));
                $('#' + tabId + " .divtemplate").css('display', 'block');
            },
            getDepartment: function (DeptCode) {
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetDepartment',
                    type: "POST",
                    data: "{'DeptCode' :'" + DeptCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=codeEdit]').val(obj.DeptCode);
                            $('input[name=codeEditHidden]').val(obj.DeptCode);
                            $('input[name=nameEdit]').val(obj.DeptName);


                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });
            },
            saveDepartment: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveDepartment',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Department has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteDepartment: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteDepartment',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Department has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditDepartment: function (data) {
                if (data.NewDeptCode !== "" && data.DeptName !== "") {
                    if (isNaN(data.NewDeptCode)) {
                        var regexNum = /\d/;
                        var regexLetter = /[a-zA-z]/;
                        if (regexLetter.test(data.NewDeptCode)) {
                            if (regexLetter.test(data.DeptName)) {
                                $.ajax({
                                    type: 'POST',
                                    url: '../../webservice/WebService_COR.asmx/EditDepartment',
                                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                                    contentType: 'application/json; charset=utf-8',
                                    dataType: 'json',
                                    success: function (response) {
                                        if (response.d == 'success') {
                                            $(_formAdd + " :input").prop("disabled", false);
                                            $table.DataTable().ajax.reload();
                                            history.go(0); //forward
                                            _setCustomFunctions.showPopup('Info', 'Department has been Update successfully');
                                        } else {
                                            $(_formAdd + " :input").prop("disabled", false);
                                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                                        }
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                    }
                                });
                            }
                            else {
                                alert('Check Field');
                            }

                        }
                        else {
                            alert('Check Field');
                        }

                    }
                    else {
                        alert('Check Field');
                    }
                }
                else {
                    alert('Empty Field');
                }


            },
            saveSectionDepartment: function (data) {
                DeptCode: $('input[name=codeEditHidden]').val();
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveSectionDepartment',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            var table = document.getElementById("tbldepartmentsection");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAddSection + " :input").prop("disabled", false);
                            //history.go(0); //forward
                            $modalAddSection.modal('hide');
                            _setCustomFunctions.setTableSection($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'Department has been saved successfully');
                        } else {
                            $(_formAddSection + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveSubSection: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveSubSection',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $modalAddSubSection.modal('hide');
                            $(_formAddSubSection + " :input").prop("disabled", false);
                            //_setCustomFunctions.setTableSection($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopupNotification(1, 'Info', 'Sub Section has been saved successfully');
                        } else {
                            $(_formAddSubSection + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopupNotification(1, 'Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditSectionDepartment: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditSectionDepartment',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        //$UpdateButton.html("Update");
                        if (response.d == 'success') {
                            //history.go(0); //forward
                            var table = document.getElementById("tbldepartmentsection");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            _setCustomFunctions.setTableSection($('input[name=codeEdit]').val());
                            $(_formAddSection + " :input").prop("disabled", false);
                            $modalAddSection.modal('hide');
                            //history.go(0); //forward
                            _setCustomFunctions.showPopup('Info', 'Section Department has been Update successfully');
                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                            var table = document.getElementById("tbldepartmentsection");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            _setCustomFunctions.setTableSection($('input[name=codeEdit]').val());
                            $(_formAddSection + " :input").prop("disabled", false);
                            $modalAddSection.modal('hide');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditSubSection: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditSubSection',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        //$UpdateButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAddSubSection + " :input").prop("disabled", false);
                            $modalAddSubSection.modal('hide');
                            //history.go(0); //forward
                            _setCustomFunctions.showPopupNotification(1, 'Info', 'Sub Section has been Update successfully');
                        } else {
                            _setCustomFunctions.showPopupNotification(1, 'Info', 'Failed to Edit');
                            $(_formAddSubSection + " :input").prop("disabled", false);
                            $modalAddSubSection.modal('hide');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteSectionDepartment: function (data, DeptCode) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteSectionDepartment',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            //history.go(0); //forward
                            var table = document.getElementById("tbldepartmentsection");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            _setCustomFunctions.setTableSection($('input[name=codeEdit]').val());
                            $(_formAddSection + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Department Section has been Delete successfully');
                        } else {
                            var table = document.getElementById("tbldepartmentsection");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            _setCustomFunctions.setTableSection($('input[name=codeEdit]').val());
                            $(_formAddSection + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteSubSection: function (data, SectioCode) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteSubSection',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            _setCustomFunctions.showPopupNotification(1, 'Info', 'Sub Section has been Delete successfully');
                        } else {
                            _setCustomFunctions.showPopupNotification(1, 'Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            setDetailSubSection: function (SubSectionCode) {
                document.getElementById('btnUpdateSubSection').style.display = 'inline';
                document.getElementById('btnSaveSubSection').style.display = 'none';
                document.all('lblSubSection').innerHTML = "Edit Sub Section";

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetSubSection',
                    type: "POST",
                    data: "{'SubSectionCode' :'" + SubSectionCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('#lblsectionCode ').val(obj.SectionCode);
                            $('input[name=sectioncodeHidden2]').val(obj.SectionCode);
                            $('input[name=subsectioncodeHidden]').val(obj.SubSectionCode);
                            $('input[name=subsectioncode]').val(obj.SubSectionCode);
                            $('input[name=subsectionname]').val(obj.SubSectionName);
                            //$('input[name=module]').val(obj.mdlDesc);

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

            },
            editsection: function () {
                document.getElementById('btnUpdateSection').style.display = 'inline';
                document.getElementById('btnSaveSection').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Section"
                var $this = $(this);
                var $row = $(this).closest('tr');
                var SectionCode = $row.find('.sectioncode').html();
                $modalAddSection.modal('toggle');

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetDepartmentSection',
                    type: "POST",
                    data: "{'SectionCode' :'" + SectionCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=sectioncode]').val(obj.SectionCode);
                            $('input[name=sectioncodeHidden]').val(obj.SectionCode);
                            $('input[name=sectionname]').val(obj.SectionName);
                            $('input[name=code]').val(obj.DeptCode);
                            //$('input[name=module]').val(obj.mdlDesc);

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }

                        }
                    }
                });
            },
            showPopupNotification: function (code,title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                history.go();
                            }

                        }
                    }
                });
            },
            showConfirmPopup: function (param, data, DeptCode, title, content) {
                if (param == 1) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeleteDepartment(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else if (param == 2) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeleteSectionDepartment(data, DeptCode);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else if (param == 3) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeleteSubSection(data, DeptCode);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }



            },
            format: function (callback, SectionCode) {
                var contentHTML;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetSubSection_ServerSideProcessing',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: JSON.stringify({ data: SectionCode }),
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        var thead = '', tbody = '', tbutton = '';
                        actionTableAddSubSection = [];
                        actionTableDelete = [];
                        actionTableEdit = [];
                        var idZ = "add_" + SectionCode;
                        var _no;
                        var _length;
                        tbutton += '<button id=' + idZ + ' type="button" class="btn btn-success pull-right" ><span class="glyphicon glyphicon-plus"></span>&nbsp;Add Sub Section</button><br>';
                        actionTableAddSubSection.push(idZ);
                        thead += "<thead>"
                        thead += '<th style="text-align: center;">No</th>';
                        thead += '<th style="text-align: center;">Sub Section Code</th><th style="text-align: center;">Sub Section Name</th>';
                        thead += '<th style="text-align: center;">Edit</th>';
                        thead += '<th style="text-align: center;">Delete</th>';
                        thead += "</thead>"
                        if (json.length != 0) {
                            actionTableDelete = [];
                            actionTableEdit = [];
                            _no = 1;
                            _length = json.length;
                            for (var i = 0 ; i < json.length; i++) {
                                //arrayKebun = jsonData.ArrayKebun
                                //arrayAplikasi = jsonData.ArrayAplikasi
                                var idY = "editData_" + SectionCode + _no;
                                var idX = "deleteData_" + SectionCode + _no;
                                tbody += '<tr style="text-align: center;"><td>' + _no + '</td><td class="SubSection" >' + json[i].SubSectionCode + '</td><td>' + json[i].SubSectionName + '</td>';

                                tbody += '<td><a id=' + idY + ' type="button" class="btn btn-warning btn-xs editDetail" data-title="Edit" data-toggle="modal" data-target="#edit"><span class="glyphicon glyphicon-pencil"></span></a></td>';
                                tbody += '<td><a id=' + idX + ' type="button" class="btn btn-danger btn-xs deleteDetail" data-title="Delete" data-toggle="modal" data-target="#delete"><span class="glyphicon glyphicon-trash"></span></a></td></tr>';
                                actionTableDelete.push(idX);
                                actionTableEdit.push(idY);
                                _no++;
                            }
                        }
                        else {
                            tbody += '<tr><td colspan="10" style="text-align: center;">No Data Present</td>';
                        }

                        callback($(tbutton + '<table id="tblSubSection_' + SectionCode + '" class="table table-striped table-bordered" style="width:100%;font-size:9pt;">' + thead + tbody + '</table>')).show();


                        for (var i = 0 ; i < actionTableAddSubSection.length; i++) {
                            $("#" + actionTableAddSubSection[i]).click(function () {
                                $modalAddSubSection.modal('toggle');
                                
                                document.getElementById('lblsectionCode').innerHTML = SectionCode;
                                $("#sectioncodeHidden").val(SectionCode)
                                document.getElementById('btnUpdateSubSection').style.display = 'none';
                                _globalSectionCode = SectionCode;
                            });
                        }

                        for (var i = 0 ; i < actionTableEdit.length; i++) {
                            $("#" + actionTableEdit[i]).click(function () {
                                var _SubSectionCode = $(this).parent().siblings().eq(1).text();
                                _setCustomFunctions.setDetailSubSection(_SubSectionCode);
                                $modalAddSubSection.modal('toggle');
                                //_setCustomFunctions.showConfirmPopup(3, datas, '', 'Info', 'Apakah anda yakin untuk Menghapus Data ini ?');
                            });
                        }
                        for (var i = 0 ; i < actionTableDelete.length; i++) {
                            $("#" + actionTableDelete[i]).click(function () {
                                var _SubSectionCode = $(this).parent().siblings().eq(1).text();
                                var datas = { SubSectionCode: _SubSectionCode };
                                _setCustomFunctions.showConfirmPopup(3, datas, '','Info', 'Apakah anda yakin untuk Menghapus Data ini ?');
                            });
                        }

                        if (_length != undefined) {
                            $("#tblSubSection_" + SectionCode).DataTable({
                                dom: 'lfrtip',
                                destroy: true,
                                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                                pageLength: 60,
                                "filter": true,
                                "columnDefs": [{
                                    "searchable": false,
                                    "orderable": false,
                                    "class": "center",
                                    "targets": 0,
                                    width: 5
                                }, {
                                    "searchable": true,
                                    "orderable": true,
                                    "targets": 1,
                                }, {
                                    "searchable": true,
                                    "orderable": true,
                                    "class": "left",
                                    "targets": 2
                                }, {
                                    "searchable": true,
                                    "orderable": true,
                                    "class": "center",
                                    "targets": 5
                                }, {
                                    "searchable": false,
                                    "orderable": false,
                                    "class": "center",
                                    "targets": 7
                                }, {
                                    "searchable": false,
                                    "orderable": false,
                                    "class": "center",
                                    "targets": 11,
                                    'checkboxes': {
                                        'selectRow': true
                                    }
                                }, {
                                    "searchable": false,
                                    "orderable": false,
                                    "class": "center",
                                    "targets": 12,
                                    width: 5
                                }, ],
                                "orderClasses": false,
                                "order": [[0, "asc"]],
                                "info": true
                            });
                        }

                    }
                });

            },
        }

    }

    Page.initialize();
})

function reply_changeSubSection(id) {
    alert(id);
}