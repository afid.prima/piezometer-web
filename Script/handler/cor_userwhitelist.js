﻿$(function () {
    var Page = {
        options: {
            table: '#tbluser',
            selectFilterArea: '#selectFilterArea',
            selectFilterSource: '#selectFilterSource',
            selectFilterLevel: '#selectFilterLevel',
            selectFilterPosisi: '#selectFilterPosisi',
            selectFilterUserData: '#selectFilterUserData',
            selectFilterJabatan: '#selectFilterJabatan',
            btnSearch: '#btnSearch',
            btnsaveFileUploadExcel: '#btnsaveFileUploadExcel',
            modalImportData: '#mdlImportData'
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $table = $(this.options.table);
            $selectFilterArea = $(this.options.selectFilterArea);
            $selectFilterSource = $(this.options.selectFilterSource);
            $selectFilterLevel = $(this.options.selectFilterLevel);
            $selectFilterPosisi = $(this.options.selectFilterPosisi);
            $selectFilterJabatan = $(this.options.selectFilterJabatan);
            $selectFilterUserData = $(this.options.selectFilterUserData);
            $btnSearch = $(this.options.btnSearch);
            $btnsaveFileUploadExcel = $(this.options.btnsaveFileUploadExcel);
            $modalImportData = $(this.options.modalImportData);
            _setCustomFunctions = this.setCustomFunctions;
            _setEvents = this.setEvents;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                //"pagingType": "simple_numbers",
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0
                }, {
                    "class": "center",
                    "targets": 4
                }, {
                    "class": "center",
                    "targets": 9
                }, {
                    "class": "center",
                    "targets": 10
                }, {
                    "class": "center",
                    "targets": 11
                }
                ],
                "orderClasses": false,
                "order": [[1, "asc"]],
                buttons: [
                    {
                        extend: 'copy',
                        text: 'Copy',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    },
                    {
                        extend: 'csvHtml5',
                        text: 'CSV',
                        header: false,
                        exportOptions: {
                            columns: [5]
                        },
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    },
                    {
                        text: 'PDF',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        action: function (e, dt, node, config) {
                            var $this = $(node);
                            $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

                            var comp2 = [], opunit2 = [];
                            if ($selectFilterArea.val() != null) {
                                $selectFilterArea.val().forEach(function (opt) {
                                    comp2.push(opt.split('_')[0]);
                                    opunit2.push(opt.split('_')[1]);
                                })
                            }
                            var level = $selectFilterLevel.val() == null ? "" : $selectFilterLevel.val();
                            var posisi = $selectFilterPosisi.val() == null ? "" : $selectFilterPosisi.val();
                            var jabatan = $selectFilterJabatan.val() == null ? "" : $selectFilterJabatan.val();
                            $.ajax({
                                type: 'POST',
                                url: '../../webservice/WebService_COR.asmx/GenerateReportUserJoinIPLAS_Mapping',
                                data: '{company: "' + comp2 + '", area: "' + opunit2 + '", level: "' + level + '", posisi: "' + posisi + '", jabatan: "' + jabatan + '"}',
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                success: function (response) {
                                    $this.html("PDF");

                                    var content = response.d;
                                    var request = new XMLHttpRequest();
                                    request.open('POST', '../../handlerservice/GetReportHandler.ashx?filename=' + response.d, true);
                                    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                                    request.responseType = 'blob';

                                    request.onload = function () {
                                        // Only handle status code 200
                                        if (request.status === 200) {
                                            // Try to find out the filename from the content disposition `filename` value
                                            var disposition = request.getResponseHeader('content-disposition');
                                            var matches = /"([^"]*)"/.exec(disposition);
                                            var filename = (matches != null && matches[1] ? matches[1] : response.d);

                                            // The actual download
                                            var blob = new Blob([request.response], { type: 'application/pdf' });
                                            var link = document.createElement('a');
                                            link.href = window.URL.createObjectURL(blob);
                                            link.download = filename;

                                            document.body.appendChild(link);

                                            link.click();

                                            document.body.removeChild(link);
                                        }
                                        else {
                                            alert('File not found');
                                        }
                                    };

                                    request.send('content=' + content);
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    console.log(xhr.statusText);
                                }
                            })
                        }
                    },
                    
                    {
                        text: 'Download Template',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        className: 'btn btn-success',
                        action: function (api, node, config) {
                            window.location.href = '../../Service/Download.ashx'
                            //$('mdlImportData').show();
                            //fasterPreview(this);
                            //var _idPict = this.files && this.files.length ? this.files[0].name.split('.')[0] : '';
                            //var _ext = $('#fileUpload')[0].value.split('.')[1];
                            //var FileName = _idPict + "." + _ext
                            //_setCustomFunctions.validateFileUpload(_idPict);

                        }
                    },
                    {
                        text: 'Upload',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        className: 'btn btn-success',
                        action: function (api, node, config) {
                            $modalImportData.modal('toggle');
                            //$('mdlImportData').show();
                            //fasterPreview(this);
                            //var _idPict = this.files && this.files.length ? this.files[0].name.split('.')[0] : '';
                            //var _ext = $('#fileUpload')[0].value.split('.')[1];
                            //var FileName = _idPict + "." + _ext
                            //_setCustomFunctions.validateFileUpload(_idPict);

                        }
                    }
                ],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserWhitelist_Mapping_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    var comp = [], opunit = [];

                    if ($selectFilterArea.val() != null) {
                        $selectFilterArea.val().forEach(function (opt) {
                            comp.push(opt.split('_')[0]);
                            opunit.push(opt.split('_')[1]);
                        })
                    }

                    aoData.push({ "name": "source", "value": $selectFilterSource.val() });
                    aoData.push({ "name": "company", "value": comp });
                    aoData.push({ "name": "area", "value": opunit });
                    aoData.push({ "name": "level", "value": $selectFilterLevel.val() });
                    aoData.push({ "name": "posisi", "value": $selectFilterPosisi.val() });
                    aoData.push({ "name": "jabatan", "value": $selectFilterJabatan.val() });
                    aoData.push({ "name": "usertype", "value": $selectFilterUserData.val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tbluser_mapping").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);

                    if ($(aData[13])[0].innerHTML == "Enterprise") {
                        $('td', nRow).css('background-color', 'bisque');
                    }
                    $(".userID").attr('id');
                }
            });
        },
        setEvents: {
            init: function () {
                $btnSearch.on('click', this.search);
                $btnsaveFileUploadExcel.on('click', this.validateFileUpload);
                $selectFilterArea.on('change', this.onChangeArea);
                $selectFilterLevel.on('change', this.onChangeLevel);
                $selectFilterPosisi.on('change', this.onChangePosisi);
                $table.on('click', '.User', this.ShowOtherProfile);
            },
            ShowOtherProfile: function () {
                var $row = $(this).closest('tr');
                var UserID = $row.find('.userID').html();
                window.location.href = "COR.aspx?bp=userdetail";
                var getInput = UserID;
                var encrypted = CryptoJS.AES.encrypt(UserID, "secret message");
                localStorage.setItem("storageName", encrypted);
                //$.ajax({
                //    type: 'POST',
                //    url: '../../webservice/WebService_COR.asmx/GetOtherProfile',
                //    contentType: 'application/json; charset=utf-8',
                //    dataType: 'json',
                //    data: "{'UserID' :'" + UserID + "'}",
                //    success: function (response) {
                //        var json = $.parseJSON(response.d);
                //        console.log(json)

                //    },
                //    error: function (xhr, ajaxOptions, thrownError) {
                //    }
                //});


            },
            search: function () {
                $table.DataTable().ajax.reload();
            },
            onChangeArea: function () {
                var opunit = '';
                if ($selectFilterArea.val() != null) {
                    $selectFilterArea.val().forEach(function (opt) {
                        if (opunit === '')
                            opunit += opt.split('_')[1];
                        else
                            opunit += ',' + opt.split('_')[1];
                    })
                }

                _setCustomFunctions.getMasterLevel(opunit);
            },
            onChangeLevel: function () {
                var opunit = '';
                var level = '';

                if ($selectFilterArea.val() != null) {
                    $selectFilterArea.val().forEach(function (opt) {
                        if (opunit === '')
                            opunit += opt.split('_')[1];
                        else
                            opunit += ',' + opt.split('_')[1];
                    })
                }

                if ($selectFilterLevel.val() != null) {
                    $selectFilterLevel.val().forEach(function (opt) {
                        if (level === '')
                            level += opt;
                        else
                            level += ',' + opt;
                    })
                }

                _setCustomFunctions.getMasterPosisi(opunit, level);
            },
            onChangePosisi: function () {
                var opunit = '';
                var level = '';
                var posisi = '';

                if ($selectFilterArea.val() != null) {
                    $selectFilterArea.val().forEach(function (opt) {
                        if (opunit === '')
                            opunit += opt.split('_')[1];
                        else
                            opunit += ',' + opt.split('_')[1];
                    })
                }

                if ($selectFilterLevel.val() != null) {
                    $selectFilterLevel.val().forEach(function (opt) {
                        if (level === '')
                            level += opt;
                        else
                            level += ',' + opt;
                    })
                }

                if ($selectFilterPosisi.val() != null) {
                    $selectFilterPosisi.val().forEach(function (opt) {
                        if (posisi === '')
                            posisi += opt;
                        else
                            posisi += ',' + opt;
                    })
                }

                _setCustomFunctions.getMasterJabatan(opunit, level, posisi);
            }, 
            validateFileUpload: function () {
                $(".loading").show();
                _setEvents.fasterPreview(this);
                var idUpload = 'fileUploadExcel';
                var _ext = $('#fileUploadExcel')[0].value.split('.')[1];
                var FileName = idUpload + "." + _ext;
                _setCustomFunctions.validateFileUpload(idUpload);
            },
            fasterPreview: function (uploader) {
                if (uploader.files && uploader.files[0]) {
                    $profileImage.attr('src',
                        window.URL.createObjectURL(uploader.files[0]));
                }
            }
        },
        setCustomFunctions: {
            init: function () {
                this.getMasterArea();
                this.getMasterSource();
                this.getMasterLevel("");
                this.getMasterPosisi("", "");
                this.getMasterJabatan("", "", "");
            },
            getMasterArea: function () {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../../webservice/WebService_COR.asmx/GetMasterArea",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var json = $.parseJSON(data.d);

                        if (json.length > 0) {
                            $selectFilterArea.find('option').remove();
                            var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {
                                if (obj.Company != optgroup) {
                                    optgroup = obj.Company
                                    group = $('<optgroup label="' + optgroup + '" />');
                                }

                                option = $('<option>', {
                                    value: optgroup + "_" + obj.Area,
                                    text: obj.Area
                                });

                                option.attr("data-tokens", optgroup + "," + obj.Area);
                                option.appendTo(group);
                                group.appendTo($selectFilterArea);
                            });

                            $selectFilterArea.selectpicker('refresh');
                        }
                    }
                });
            },
            getMasterSource: function () {
                $selectFilterSource.find('option').remove();
                $selectFilterSource.append($('<option>', {
                    value: 'EUCLID',
                    text: 'EUCLID'
                }));

                $selectFilterSource.selectpicker('refresh');
            },
            getMasterLevel: function (area) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../../webservice/WebService_COR.asmx/GetMasterLevel",
                    data: "{area: '" + area + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var json = $.parseJSON(data.d);

                        $selectFilterLevel.find('option').remove();
                        json.forEach(function (obj) {
                            $selectFilterLevel.append($('<option>', {
                                value: obj.Lvl,
                                text: obj.Lvl
                            }));
                        })

                        $selectFilterLevel.selectpicker('refresh');
                        $selectFilterLevel.change();
                    }
                });
            },
            getMasterPosisi: function (area, level) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../../webservice/WebService_COR.asmx/GetMasterPosisi",
                    data: "{area: '" + area + "', level: '" + level + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var json = $.parseJSON(data.d);

                        $selectFilterPosisi.find('option').remove();
                        json.forEach(function (obj) {
                            $selectFilterPosisi.append($('<option>', {
                                value: obj.Posisi,
                                text: obj.Posisi
                            }));
                        })

                        $selectFilterPosisi.selectpicker('refresh');
                        $selectFilterPosisi.change();
                    }
                });
            },
            getMasterJabatan: function (area, level, posisi) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../../webservice/WebService_COR.asmx/GetMasterJabatan",
                    data: "{area: '" + area + "', level: '" + level + "', posisi: '" + posisi + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var json = $.parseJSON(data.d);

                        $selectFilterJabatan.find('option').remove();
                        json.forEach(function (obj) {
                            $selectFilterJabatan.append($('<option>', {
                                value: obj.Jabatan,
                                text: obj.Jabatan + ' (' + obj.CountPpl + ')'
                            }));
                        })

                        $selectFilterJabatan.selectpicker('refresh');
                        $selectFilterJabatan.change();
                    }
                });
            },
            validateFileUpload: function (_idPict) {
                var fuData = document.getElementById(_idPict);
                var Length = fuData.files.length;
                if (Length < 1) {
                    $.alert({
                        title: 'Alert!',
                        content: 'Please upload an file',
                    });

                } else {
                    var filename = fuData.files[0].name;
                    var FileUploadPath = fuData.value;
                    var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
                    if (fuData.files && fuData.files[0]) {

                        var size = fuData.files[0].size;

                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#blah').attr('src', e.target.result);
                        }

                        reader.readAsDataURL(fuData.files[0]);
                        $.ajax({
                            type: 'POST',
                            url: '../../webservice/WebService_COR.asmx/GetUserID',
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            success: function (response) {
                                userID = response.d;
                                _setCustomFunctions.saveDataUploadExcel(filename, Extension, userID, _idPict);
                            }
                        });
                        //}
                    }
                }
            },
            saveDataUploadExcel: function (filenames, Extension, userID, _idPict) {
                var FileName = userID + "_" + filenames;
                var filename = $('#' + _idPict)[0];
                //const timestampSeconds = Math.round((new Date()).getTime() / 1000);
                //console.log(timestampSeconds);
                var _msg = FileName;
                var files = filename.files;
                var data = new FormData();
                for (var i = 0; i < files.length; i++) {
                    data.append(FileName, files[i]);

                }
                $.ajax({
                    type: 'POST',
                    url: '../../Service/ImportWhitelistHandler.ashx?filename=' + _msg + '&userID=' + userID,
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response == 'Success') {
                            $(".loading").hide();
                            $.alert({
                                title: 'Alert!',
                                content: 'Upload file Excel success',
                            });

                            $modalImportData.modal('hide');
                            history.go(0);
                        } else {
                            alert(response);
                            history.go(0);
                            $(".loading").hide();
                        }
                    }

                });

            }
        }
    }

    Page.initialize();
})


function EventHandler_UserEUCLID() {
    $('#btnSearch').click(function () {
        $('#tbluser').DataTable().ajax.reload();
        $('#tbluser_mapping').DataTable().ajax.reload();
    })

    //Flat green color scheme for iCheck
    $('input[type="radio"].flat-green').iCheck({
        radioClass: 'iradio_flat-green'
    })

    $('input').on('ifChecked', function (event) {
        if ($('#divtbluser').is(":visible")) {
            $('#divtbluser').hide();
            $('#divtbluser_version2').show();
        } else if ($('#divtbluser_version2').is(":visible")) {
            $('#divtbluser').show();
            $('#divtbluser_version2').hide();
        }
    });

    $('#selectFilterArea').change(function () {
        var opunit = '';
        $('#selectFilterArea').val().forEach(function (opt) {
            if (opunit === '')
                opunit += opt.split('_')[1];
            else
                opunit += ',' + opt.split('_')[1];
        })

        GetMasterLevel(opunit);
    })

    $('#selectFilterLevel').change(function () {
        var opunit = '';
        var level = '';

        if ($('#selectFilterArea').val() != null) {
            $('#selectFilterArea').val().forEach(function (opt) {
                if (opunit === '')
                    opunit += opt.split('_')[1];
                else
                    opunit += ',' + opt.split('_')[1];
            })
        }

        if ($('#selectFilterLevel').val() != null) {
            $('#selectFilterLevel').val().forEach(function (opt) {
                if (level === '')
                    level += opt;
                else
                    level += ',' + opt;
            })
        }

        GetMasterPosisi(opunit, level);
    })

    $('#selectFilterPosisi').change(function () {
        var opunit = '';
        var level = '';
        var posisi = '';

        if ($('#selectFilterArea').val() != null) {
            $('#selectFilterArea').val().forEach(function (opt) {
                if (opunit === '')
                    opunit += opt.split('_')[1];
                else
                    opunit += ',' + opt.split('_')[1];
            })
        }

        if ($('#selectFilterLevel').val() != null) {
            $('#selectFilterLevel').val().forEach(function (opt) {
                if (level === '')
                    level += opt;
                else
                    level += ',' + opt;
            })
        }

        if ($('#selectFilterPosisi').val() != null) {
            $('#selectFilterPosisi').val().forEach(function (opt) {
                if (posisi === '')
                    posisi += opt;
                else
                    posisi += ',' + opt;
            })
        }

        GetMasterJabatan(opunit, level, posisi);
    })

}

function GetMasterSource() {
    $('#selectFilterSource').find('option').remove();
    $("#selectFilterSource").append($('<option>', {
        value: 'EUCLID',
        text: 'EUCLID'
    }));
    //$("#selectFilterSource").append($('<option>', {
    //    value: 'IPLAS',
    //    text: 'IPLAS'
    //}));

    $('#selectFilterSource').selectpicker('refresh');
}

function GetMasterArea() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../webservice/WebService_COR.asmx/GetMasterArea",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var json = $.parseJSON(data.d);

            if (json.length > 0) {
                $('#selectFilterArea').find('option').remove();
                var optgroup = "", group = "", option = "";

                json.forEach(function (obj) {
                    if (obj.Company != optgroup) {
                        optgroup = obj.Company
                        group = $('<optgroup label="' + optgroup + '" />');
                    }

                    option = $('<option>', {
                        value: optgroup + "_" + obj.Area,
                        text: obj.Area
                    });

                    option.attr("data-tokens", optgroup + "," + obj.Area);
                    option.appendTo(group);
                    group.appendTo($('#selectFilterArea'));
                });

                $('#selectFilterArea').selectpicker('refresh');
            }


            //$('#selectFilterArea').find('option').remove();
            //json.forEach(function (obj) {
            //    $("#selectFilterArea").append($('<option>', {
            //        value: obj.Area,
            //        text: obj.Area
            //    }));
            //})

            //$('#selectFilterArea').selectpicker('refresh');
        }
    });
}

function GetMasterLevel(area) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../webservice/WebService_COR.asmx/GetMasterLevel",
        data: "{area: '" + area + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var json = $.parseJSON(data.d);

            $('#selectFilterLevel').find('option').remove();
            json.forEach(function (obj) {
                $("#selectFilterLevel").append($('<option>', {
                    value: obj.Lvl,
                    text: obj.Lvl
                }));
            })

            $('#selectFilterLevel').selectpicker('refresh');
            $('#selectFilterLevel').change();
        }
    });
}

function GetMasterPosisi(area, level) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../webservice/WebService_COR.asmx/GetMasterPosisi",
        data: "{area: '" + area + "', level: '" + level + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var json = $.parseJSON(data.d);

            $('#selectFilterPosisi').find('option').remove();
            json.forEach(function (obj) {
                $("#selectFilterPosisi").append($('<option>', {
                    value: obj.Posisi,
                    text: obj.Posisi
                }));
            })

            $('#selectFilterPosisi').selectpicker('refresh');
            $('#selectFilterPosisi').change();
        }
    });
}

function GetMasterJabatan(area, level, posisi) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../webservice/WebService_COR.asmx/GetMasterJabatan",
        data: "{area: '" + area + "', level: '" + level + "', posisi: '" + posisi + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var json = $.parseJSON(data.d);

            $('#selectFilterJabatan').find('option').remove();
            json.forEach(function (obj) {
                $("#selectFilterJabatan").append($('<option>', {
                    value: obj.Jabatan,
                    text: obj.Jabatan + ' (' + obj.CountPpl + ')'
                }));
            })

            $('#selectFilterJabatan').selectpicker('refresh');
        }
    });
}

function GetUserJoinIPLAS() {
    var content = '<table id="tbluser" class="table table-striped table-bordered" style="width:100%;font-size:9pt;">';
    content += '<thead><tr>';
    content += '<th style="text-align:center;">No.</th><th>Area</th><th>Nama</th><th>Email</th><th>Level</th><th>Posisi</th><th>Jabatan</th></tr></thead><tbody></tbody></table>';

    $("#divtbluser").html(content);

    var table = $('#tbluser').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
        pageLength: 25,
        "filter": true,
        //"pagingType": "simple_numbers",
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "class": "center",
            "targets": 0
        }],
        "orderClasses": false,
        "order": [[1, "asc"]],
        buttons: [
            'copy',
            //{
            //    extend: 'csv', title: 'List User GAMA Plantation', messageTop: 'This Information in this table is copyright to GAMA Plantation.',
            //    filename: function () {
            //        var today = new Date();
            //        var dd = today.getDate();
            //        var mm = today.getMonth() + 1; //January is 0!

            //        var yyyy = today.getFullYear();
            //        if (dd < 10) {
            //            dd = '0' + dd;
            //        }
            //        if (mm < 10) {
            //            mm = '0' + mm;
            //        }
            //        var today = yyyy + '/' + mm + '/' + dd;

            //        return today + '_' + 'COR001_LaporanDaftarUserEuclid'
            //    }
            //}, {
            //    extend: 'excel', title: 'List User GAMA Plantation', messageTop: 'This Information in this table is copyright to GAMA Plantation.',
            //    filename: function () {
            //        var today = new Date();
            //        var dd = today.getDate();
            //        var mm = today.getMonth() + 1; //January is 0!

            //        var yyyy = today.getFullYear();
            //        if (dd < 10) {
            //            dd = '0' + dd;
            //        }
            //        if (mm < 10) {
            //            mm = '0' + mm;
            //        }
            //        var today = yyyy + '/' + mm + '/' + dd;

            //        return today + '_' + 'COR001_LaporanDaftarUserEuclid'
            //    }
            //},
            //{
            //    extend: 'pdfHtml5', title: 'List User GAMA Plantation', messageTop: 'This Information in this table is copyright to GAMA Plantation.',
            //    filename: function () {
            //        var today = new Date();
            //        var dd = today.getDate();
            //        var mm = today.getMonth() + 1; //January is 0!

            //        var yyyy = today.getFullYear();
            //        if (dd < 10) {
            //            dd = '0' + dd;
            //        }
            //        if (mm < 10) {
            //            mm = '0' + mm;
            //        }
            //        var today = yyyy + '/' + mm + '/' + dd;

            //        return today + '_' + 'COR001_LaporanDaftarUserEuclid'
            //    }
            //}, 
            'print'
        ],
        "info": true,
        //"scrollY": "450px",
        //"scrollCollapse": true,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserJoinIPLAS_ServerSideProcessing",
        "fnServerData": function (sSource, aoData, fnCallback) {
            aoData.push({ "name": "area", "value": $('#selectFilterArea').val() });
            aoData.push({ "name": "level", "value": $('#selectFilterLevel').val() });
            aoData.push({ "name": "posisi", "value": $('#selectFilterPosisi').val() });
            aoData.push({ "name": "jabatan", "value": $('#selectFilterJabatan').val() });
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success": function (msg) {
                    var json = jQuery.parseJSON(msg.d);
                    fnCallback(json);
                    $("#tbluser").show();
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        },
        fnDrawCallback: function () {
        },
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            var page = info.page;
            var length = info.length;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
        }
    });

    //$(".loading").show();
    //if (listUser == null) {
    //    $.ajax({
    //        type: "POST",
    //        dataType: "json",
    //        url: "../../webservice/WebService_COR.asmx/GetUserJoinIPLAS",
    //        contentType: "application/json; charset=utf-8",
    //        success: function (data) {
    //            $(".loading").hide();

    //            var json = $.parseJSON(data.d);
    //            listUser = json;

    //            var selectedArea = $('#selectFilterArea').val();
    //            var selectedLevel = $('#selectFilterLevel').val();
    //            var selectedPosisi = $('#selectFilterPosisi').val();
    //            var selectedJabatan = $('#selectFilterJabatan').val();

    //            var content = '<table id="tbluser" class="table table-striped table-bordered" style="width:100%;font-size:9pt;">';
    //            content += '<thead><tr>';
    //            content += '<th style="text-align:center;">No.</th><th>Area</th><th>Nama</th><th>Email</th><th>Level</th><th>Posisi</th><th>Jabatan</th><th>Source Data</th></tr></thead><tbody>';

    //            for (var i = 0; i < json.length; i++) {
    //                if ((selectedArea.length == 0 ? 0 : selectedArea.indexOf(listUser[i].Area)) > -1 && (selectedLevel.length == 0 ? 0 : selectedLevel.indexOf(listUser[i].Lvl)) > -1 && (selectedPosisi.length == 0 ? 0 : selectedPosisi.indexOf(listUser[i].Posisi)) > -1 && (selectedJabatan.length == 0 ? 0 : selectedJabatan.indexOf(listUser[i].Jabatan)) > -1) {
    //                    content += '<tr>';
    //                    content += '<td style="text-align:center;"></td>';
    //                    content += '<td>' + listUser[i]["Area"] + '</td>';
    //                    content += '<td>' + listUser[i]["Nama"] + '</td>';
    //                    content += '<td>' + (listUser[i]["Email"] == null ? "" : listUser[i]["Email"]) + '</td>';
    //                    content += '<td>' + listUser[i]["Lvl"] + '</td>';
    //                    content += '<td>' + listUser[i]["Posisi"] + '</td>';
    //                    content += '<td>' + listUser[i]["Jabatan"] + '</td>';
    //                    content += '<td>' + listUser[i]["SourceData"] + '</td>';
    //                    content += '</tr>';
    //                }
    //            }

    //            content += '</tbody>';
    //            content += '<tfoot><tr>';
    //            content += '<th style="text-align:center;"></th><th>Area</th><th>Nama</th><th>Email</th><th>Level</th><th>Posisi</th><th>Jabatan</th><th>Source Data</th></tr></tfoot>';
    //            content += '</table>';
    //            $("#divtbluser").html(content);
    //            var table = $('#tbluser').DataTable({
    //                dom: 'Blfrtip',
    //                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
    //                pageLength: 25,
    //                "columnDefs": [{
    //                    "searchable": false,
    //                    "orderable": false,
    //                    "targets": 0
    //                }],
    //                "order": [[1, 'asc']],
    //                buttons: [
    //                    'copy', { extend: 'csv', title: 'List User GAMA Plantation', messageTop: 'This Information in this table is copyright to GAMA Plantation.' }, { extend: 'excel', title: 'List User GAMA Plantation', messageTop: 'This Information in this table is copyright to GAMA Plantation.' }, 'pdf', 'print'
    //                ],
    //                initComplete: function () {
    //                    this.api().columns().every(function () {
    //                        var column = this;
    //                        var select = $('<select><option value=""></option></select>')
    //                            .appendTo($(column.footer()).empty())
    //                            .on('change', function () {
    //                                var val = $.fn.dataTable.util.escapeRegex(
    //                                    $(this).val()
    //                                );

    //                                column
    //                                    .search(val ? '^' + val + '$' : '', true, false)
    //                                    .draw();
    //                            });

    //                        column.data().unique().sort().each(function (d, j) {
    //                            select.append('<option value="' + d + '">' + d + '</option>')
    //                        });
    //                    });
    //                }
    //            });

    //            table.on('order.dt search.dt', function () {
    //                table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
    //                    cell.innerHTML = i + 1;
    //                });
    //            }).draw();
    //        }
    //    });
    //} else {
    //    var selectedArea = $('#selectFilterArea').val();
    //    var selectedLevel = $('#selectFilterLevel').val();
    //    var selectedPosisi = $('#selectFilterPosisi').val();
    //    var selectedJabatan = $('#selectFilterJabatan').val();

    //    var content = '<table id="tbluser" class="table table-striped table-bordered" style="width:100%;font-size:9pt;">';
    //    content += '<thead><tr>';
    //    content += '<th style="text-align:center;">No.</th><th>Area</th><th>Nama</th><th>Email</th><th>Level</th><th>Posisi</th><th>Jabatan</th><th>Source Data</th></tr></thead><tbody>';

    //    for (var i = 0; i < listUser.length; i++) {
    //        if ((selectedArea.length == 0 ? 0 : selectedArea.indexOf(listUser[i].Area)) > -1 && (selectedLevel.length == 0 ? 0 : selectedLevel.indexOf(listUser[i].Lvl)) > -1 && (selectedPosisi.length == 0 ? 0 : selectedPosisi.indexOf(listUser[i].Posisi)) > -1 && (selectedJabatan.length == 0 ? 0 : selectedJabatan.indexOf(listUser[i].Jabatan)) > -1) {
    //            content += '<tr>';
    //            content += '<td style="text-align:center;"></td>';
    //            content += '<td>' + listUser[i]["Area"] + '</td>';
    //            content += '<td>' + listUser[i]["Nama"] + '</td>';
    //            content += '<td>' + (listUser[i]["Email"] == null ? "" : listUser[i]["Email"]) + '</td>';
    //            content += '<td>' + listUser[i]["Lvl"] + '</td>';
    //            content += '<td>' + listUser[i]["Posisi"] + '</td>';
    //            content += '<td>' + listUser[i]["Jabatan"] + '</td>';
    //            content += '<td>' + listUser[i]["SourceData"] + '</td>';
    //            content += '</tr>';
    //        }
    //    }

    //    content += '</tbody>';
    //    content += '<tfoot><tr>';
    //    content += '<th style="text-align:center;"></th><th>Area</th><th>Nama</th><th>Email</th><th>Level</th><th>Posisi</th><th>Jabatan</th><th>Source Data</th></tr></tfoot>';
    //    content += '</table>';
    //    $("#divtbluser").html(content);
    //    var table = $('#tbluser').DataTable({
    //        dom: 'Blfrtip',
    //        lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
    //        pageLength: 25,
    //        "columnDefs": [{
    //            "searchable": false,
    //            "orderable": false,
    //            "targets": 0
    //        }],
    //        "order": [[1, 'asc']],
    //        buttons: [
    //            'copy', { extend: 'csv', title: 'List User GAMA Plantation', messageTop: 'This Information in this table is copyright to GAMA Plantation.' }, { extend: 'excel', title: 'List User GAMA Plantation', messageTop: 'This Information in this table is copyright to GAMA Plantation.'}, 'pdf', 'print'
    //        ],
    //        initComplete: function () {
    //            this.api().columns().every(function () {
    //                var column = this;
    //                var select = $('<select><option value=""></option></select>')
    //                    .appendTo($(column.footer()).empty())
    //                    .on('change', function () {
    //                        var val = $.fn.dataTable.util.escapeRegex(
    //                            $(this).val()
    //                        );

    //                        column
    //                            .search(val ? '^' + val + '$' : '', true, false)
    //                            .draw();
    //                    });

    //                column.data().unique().sort().each(function (d, j) {
    //                    select.append('<option value="' + d + '">' + d + '</option>')
    //                });
    //            });
    //        }
    //    });

    //    table.on('order.dt search.dt', function () {
    //        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
    //            cell.innerHTML = i + 1;
    //        });
    //    }).draw();

    //    $(".loading").hide();
    //}

}

function GetUserJoinIPLAS_Mapping() {
    var content = '<table id="tbluser_mapping" class="table table-striped table-bordered" style="width:100%;font-size:9pt;">';
    content += '<thead><tr>';
    content += '<th style="text-align:center;" rowspan="2">No.</th><th rowspan="2">Area</th><th rowspan="2">NIK</th><th rowspan="2">Nama</th><th rowspan="2">Gender</th><th rowspan="2">Email</th><th rowspan="2">Level</th><th rowspan="2">Posisi</th><th rowspan="2">Jabatan</th><th style="text-align:center;" colspan="2">Terdaftar pada</th><th rowspan="2">Team Leader</th></tr>';
    content += '<tr><th style="text-align:center;">Enterprise</th><th style="text-align:center;">Mobile</th></tr>'
    content += '</thead><tbody></tbody></table > ';

    $("#divtbluser_version2").html(content);

    var table = $('#tbluser_mapping').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
        pageLength: 25,
        "filter": true,
        //"pagingType": "simple_numbers",
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "class": "center",
            "targets": 0
        }, {
            "class": "center",
            "targets": 4
        }, {
            "class": "center",
            "targets": 9
        }, {
            "class": "center",
            "targets": 10
        }, {
            "class": "center",
            "targets": 11
        }
        ],
        "orderClasses": false,
        "order": [[1, "asc"]],
        buttons: [
            {
                extend: 'copy',
                text: 'Copy',
                className: 'btn btn-success',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                }
            },
            {
                text: 'PDF',
                className: 'btn btn-success',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                },
                action: function (e, dt, node, config) {
                    var $this = $(node);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

                    var comp2 = [], opunit2 = [];
                    $('#selectFilterArea').val().forEach(function (opt) {
                        comp2.push(opt.split('_')[0]);
                        opunit2.push(opt.split('_')[1]);
                    })

                    $.ajax({
                        type: 'POST',
                        url: '../../webservice/WebService_COR.asmx/GenerateReportUserJoinIPLAS_Mapping',
                        data: '{source: "' + $('#selectFilterSource').val() + '", company: "' + comp2 + '", area: "' + opunit2 + '", level: "' + $('#selectFilterLevel').val() + '", posisi: "' + $('#selectFilterPosisi').val() + '", jabatan: "' + $('#selectFilterJabatan').val() + '"}',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (response) {
                            $this.html("PDF");

                            var content = response.d;
                            var request = new XMLHttpRequest();
                            request.open('POST', '../../handlerservice/GetReportHandler.ashx?filename=' + response.d, true);
                            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                            request.responseType = 'blob';

                            request.onload = function () {
                                // Only handle status code 200
                                if (request.status === 200) {
                                    // Try to find out the filename from the content disposition `filename` value
                                    var disposition = request.getResponseHeader('content-disposition');
                                    var matches = /"([^"]*)"/.exec(disposition);
                                    var filename = (matches != null && matches[1] ? matches[1] : response.d);

                                    // The actual download
                                    var blob = new Blob([request.response], { type: 'application/pdf' });
                                    var link = document.createElement('a');
                                    link.href = window.URL.createObjectURL(blob);
                                    link.download = filename;

                                    document.body.appendChild(link);

                                    link.click();

                                    document.body.removeChild(link);
                                }
                                else {
                                    alert('File not found');
                                }
                            };

                            request.send('content=' + content);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.statusText);
                        }
                    })
                }
            }
        ],
        "info": true,
        //"scrollY": "450px",
        //"scrollCollapse": true,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserJoinIPLAS_Mapping_ServerSideProcessing",
        "fnServerData": function (sSource, aoData, fnCallback) {
            var comp = [], opunit = [];

            console.log('length', $('#selectFilterArea').val());
            if ($('#selectFilterArea').val() != null) {
                $('#selectFilterArea').val().forEach(function (opt) {
                    comp.push(opt.split('_')[0]);
                    opunit.push(opt.split('_')[1]);
                })
            }

            aoData.push({ "name": "source", "value": $('#selectFilterSource').val() });
            aoData.push({ "name": "company", "value": comp });
            aoData.push({ "name": "area", "value": opunit });
            aoData.push({ "name": "level", "value": $('#selectFilterLevel').val() });
            aoData.push({ "name": "posisi", "value": $('#selectFilterPosisi').val() });
            aoData.push({ "name": "jabatan", "value": $('#selectFilterJabatan').val() });
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success": function (msg) {
                    var json = jQuery.parseJSON(msg.d);
                    fnCallback(json);
                    $("#tbluser_mapping").show();
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console == "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        },
        fnDrawCallback: function () {
        },
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            var page = info.page;
            var length = info.length;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
        }
    });
}