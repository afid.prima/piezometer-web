﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddattgrade',
            saveButton: '#btnSave',
            UpdateButton: '#btnUpdate',
            modalAdd: '#mdlAdd',
            tabattgrade: '#tabattgrade',
            table: '#tblattgrade',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $UpdateButton = $(this.options.UpdateButton);
            $modalAdd = $(this.options.modalAdd);
            $tabattgrade = $(this.options.tabattgrade);
            $table = $(this.options.table);
            _formAdd = this.options.formAdd;
            _tabattgrade = this.options.tabattgrade;
            _listattgrade = [];
            _listattgradeCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                    width: 100
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 2,
                    width: 100
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: 500
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 5,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetAttGrade_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblattgrade").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $table.on('click', 'a[data-target="#delete"]', this.delete);

                $tabattgrade.on('click', 'a', this.tabclick);
                //this.formvalidation();

                //select event
                $('#SelectChangeLanguage').on('change', this.selectchange);
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 2,
                            maxlength: 2,
                            alpha: true,
                            required: true,
                            remote: function () {
                                //document.getElementById('errormsg').style.display = 'none';
                                return {
                                    url: "../../webservice/WebService_COR.asmx/GetattgradeData",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: "{'attgradeCode' :'" + $('input[name=code]').val() + "'}",
                                    dataFilter: function (data) {
                                        var msg = JSON.parse(data);
                                        var json = $.parseJSON(msg.d);
                                        if (json.length > 0) {
                                            //{
                                            document.getElementById('lblcode').style.color = '#dd4b39';
                                            document.getElementById('code').style.borderColor = '#dd4b39';
                                            //document.getElementById('help-block').style.display = 'block';
                                            document.getElementById('errormsg').style.display = 'block';
                                            document.all('errormsg').innerHTML = "Code sudah terdaftar pada sistem";
                                        }
                                    }
                                }


                            },
                        },
                        name: {
                            minlength: 1,
                            alpha: true,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                //e.preventDefault();

            },
            tabchange: function (tabId) {


            },
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                //document.getElementById('help-block').style.display = 'block';
                //document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';
                document.all('lbl').innerHTML = "Add attgrade";
                $('input[name=code]').val('');
                $('input[name=codeHidden]').val('');
                $('input[name=name]').val('');
                $("#selectGrade").val(0);
                $("#selectAttCode").val(0);
                $("#selectEstCode option:selected").removeAttr("selected");
                $('.selectpicker').selectpicker('refresh');
                _setEvents.formvalidation();

            },
            edit: function () {
                //document.getElementById('help-block').style.display = 'block';
                //document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'inline';
                document.getElementById('btnSave').style.display = 'none';
                document.all('lbl').innerHTML = "Edit attgrade";
                var $this = $(this);
                var $row = $(this).closest('tr');
                var grade = $row.find('.grade').html();
                var attcode = $row.find('.attcode').html();

                $modalAdd.modal('toggle');
                _setEvents.formvalidation();


                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetattgradeData',
                    type: "POST",
                    data: "{'grade' :'" + grade + "','attCode' :'" + attcode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $("#selectGrade").val(obj.grade);
                            $("#selectAttCode").val(obj.attendanceCode);
                            $("#selectEstCode option:selected").removeAttr("selected");
                            $('input[name=codeHidden]').val(obj.grade);
                            $('input[name=attCodeHidden]').val(obj.attendanceCode);
                            if (obj.estCode != 'XX') {
                                $.each(obj.estCode.split(","), function (i, e) {
                                    $("#selectEstCode option[value='" + e + "']").prop("selected", true);
                                });
                            }
                            else {
                                $("#selectEstCode").val(obj.estCode);
                            }

                            $('.selectpicker').selectpicker('refresh');
                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

                //var attgradeName = $row.find('.attgradeName').html();


                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = {
                    GradeBefore: $('input[name=codeHidden]').val(), AttendenceCodeBefore: $('input[name=attCodeHidden]').val(),
                    Grade: $("#selectGrade").val(), AttendenceCode: $("#selectAttCode").val(), ListEstCode: $("#selectEstCode").val()
                };
                //var savetype = 
                _setCustomFunctions.Editattgrade(data);

                return false;



                //$("#formAdd :input").prop("disabled", true);
                ////Set Ajax Submit Here
                //var data = { attgradeCode: $('input[name=codeHidden]').val(), NewattgradeCode: $('input[name=code]').val(), attgradeName: Count$('input[name=name]').val() };
                ////var savetype = 

                //_setCustomFunctions.Editattgrade(data);

                //return false;
            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $this = $(this);
                var $row = $(this).closest('tr');
                var grade = $row.find('.grade').html();
                var attcode = $row.find('.attcode').html();
                //Set Ajax Submit Here
                var data = { GradeBefore: grade, AttendenceCodeBefore: attcode};
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            save: function () {
                //Event for saving data
                console.log('test', $formAdd);
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { Grade: $("#selectGrade").val(), AttendenceCode: $("#selectAttCode").val(), ListEstCode: $("#selectEstCode").val() };
                    //var savetype = 
                    _setCustomFunctions.saveattgrade(data);

                    return false;
                }
            },
            selectchange: function () {
                {

                    var message = '<%=GetGlobalResourceObject("ResourceIDN", "lblAddOrDeleteattgrade") %>';
                    alert(message);
                }
                //alert('<%= GetGlobalResourceObject([ResourceIDN],[lblAddOrDeleteattgrade]) %>');
            }
        },
        setCustomFunctions: {
            init: function () {
                this.getMastergrade();
                this.getMasterattendenceCode();
                this.getMasterEstCode();
                //this.getMasterModuleType();
            },
            registerTabContent: function (tabId, mdlcode) {
            },
            getMastergrade: function () {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterGrade',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var jsonData = JSON.parse(response.d);
                        //$("#selectEstcode").append($('<option>', {
                        //    value: 0,
                        //    text: '-- Choose Estate --',
                        //}));
                        $("#selectGrade").append($('<option>', {
                            value: 0,
                            text: '-- Choose Grade --',
                        }));
                        for (var i = 0; i < jsonData.length; i++) {
                            $("#selectGrade").append($('<option>', {
                                value: jsonData[i]["grade"],
                                text: jsonData[i]["grade"]
                            }));
                        }

                        $('.selectpicker').selectpicker('refresh');
                    }
                });
            },
            getMasterattendenceCode: function () {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterAttendenceCode',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var jsonData = JSON.parse(response.d);
                        //$("#selectEstcode").append($('<option>', {
                        //    value: 0,
                        //    text: '-- Choose Estate --',
                        //}));
                        $("#selectAttCode").append($('<option>', {
                            value: 0,
                            text: '-- Choose Attendence Code --',
                        }));
                        for (var i = 0; i < jsonData.length; i++) {
                            $("#selectAttCode").append($('<option>', {
                                value: jsonData[i]["attendancecode"],
                                text: jsonData[i]["attendanceName"]
                            }));
                        }

                        $('.selectpicker').selectpicker('refresh');
                    }
                });
            },
            getMasterEstCode: function () {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetEstateNew',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var jsonData = JSON.parse(response.d);
                        //$("#selectEstCode").append($('<option>', {
                        //    value: 0,
                        //    text: '-- Choose Estate --',
                        //}));
                        $("#selectEstCode").append($('<option>', {
                            value: 'XX',
                            text: 'All Estate',
                        }));
                        for (var i = 0; i < jsonData.length; i++) {
                            $("#selectEstCode").append($('<option>', {
                                value: jsonData[i]["EstCode"],
                                text: jsonData[i]["NewEstName"]
                            }));
                        }


                        $('.selectpicker').selectpicker('refresh');
                    }
                });
            },
            //getMasterModuleSubType: function (type) {

            //},
            saveattgrade: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveAttGrade',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'attgrade has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            Deleteattgrade: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteAttGrade',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'attgrade has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            Editattgrade: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/Editattgrade',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $UpdateButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'attgrade has been Update successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.Deleteattgrade(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
})


//function Setupattgrade() {

//}