﻿$(function () {
    var _link = "CheckEmail";
    var _links = "";
    var _status = "";
    var Page = {
        options: {
            formRegister: '#formRegister',
            btnVerifikasi: '#Button1',
            modalAdd2: '#mdlAdd2',
        },
        initialize: function () {
            this.setVars();
            this.setEvents.init();
        },
        setVars: function () {
            $formRegister = $(this.options.formRegister);
            $btnVerifikasi = $(this.options.btnVerifikasi);
            $modalAdd2 = $(this.options.modalAdd2);
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setEvents: {
            init: function () {
                $btnVerifikasi.on('click', _setCustomFunctions.Verifikasi);
                //$('#SelectEmail').on('change', _setCustomFunctions.OnchangeHaveEmail);
            },
            formvalidation: function () {
                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                $.validator.addMethod('codExistss', function (value, element) {
                    //value.indexOf('@indo-palm.co.id') < 10 ? $('input[name=uname]').val('') : $('input[name=uname]').val(value.substr(0, value.indexOf('@')))
                    return (value.indexOf('@indo-palm.co.id') > -1 ? true : false);

                }, "Hanya Menggunakan Domain @indo-palm.co.id");

                //Main Validation
                $formRegister.validate({
                    rules: {
                        email2: {
                            required: true,
                            minlength: 2,
                            codExistss: true,
                            email: true,
                            remote: function () {
                                return {

                                    url: "Service/WebService.asmx/CheckEmail",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: JSON.stringify({ UserEmail: $('#email2').val() }),
                                    dataFilter: function (data) {
                                        //var msg = JSON.parse(data);
                                        var json = $.parseJSON(data);
                                        var jsona = json.d;
                                        var jsons = $.parseJSON(jsona);
                                        jsons.forEach(function (obj) {
                                            if (obj.msg == "Enterprise")
                                            {
                                                _links = 'Enterprise';
                                            }
                                            else
                                            {
                                                _links = 'Euclid';
                                            }

                                            $('#username').val(obj.Nama)
                                            _status = obj.Email == "true" ? true : false;
                                        })
                                        return _status;
                                        
                                    }
                                }
                            },

                        }
                    },
                    messages: {
                        email2: {
                            remote: function() {
                                if (_links == 'Enterprise')
                                    return "Email Anda Sudah Terdaftar Pada Sistem, Silahkan Lakukan<a href=default2.aspx target=_blank> Login</label></a>";
                                return "Tidak Ada Email Yang Terdaftar Atas Nama Anda. Silahkan Hubungi Bagian HRD";
                            },
                            required: 'Mohon isi email.'
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
            }
        },
        setCustomFunctions: {
            init: function () {
                this.getModule();
                //this.getMasterModuleGroup();
                //this.getMasterModuleType();
            },
            getModule: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../Service/WebService.asmx/GetMasterModuleWebpage',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectModule').find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#SelectModule").append($('<option>', {
                                value: obj.mdlCode,
                                text: obj.mdlCode + " - " + obj.mdlDesc
                            }));
                        })
                    }
                })
            },
            OnchangeHaveEmail: function(){
                if ($('#SelectEmail').val() == 1)
                {
                    document.getElementById('formemail').style.visibility = 'visible';
                    document.getElementById('btnRegister').style.visibility = 'visible';
                    _setEvents.formvalidation();
                    $('input[name=email2]').val('');
                }
                else
                {
                    document.getElementById('formemail').style.visibility = 'hidden';
                    document.getElementById('btnRegister').style.visibility = 'hidden';
                }
            },
            Test: function () {
            alert("Show Me")},
            showPopup: function (title, content) {
                if (title == "Info")
                {

                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            OK: {
                                text: 'OK',
                                btnClass: 'btn-red',
                                action: function () {
                                    //$('input[name=UserName]').val($('input[name=UserName]').val());
                                }
                            }
                        }
                    });
                }
                else if (title == "Confirm")
                {

                    $.dialog({
                        title: title,
                        content: content
                    });
                }
                else if (title == "Register")
                {
                    $.dialog({
                        title: title,
                        content: content
                    });
                }


            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                //_setCustomFunctions.DeleteWebActive(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                            }
                        }
                    }
                });

            },
            Verifikasi: function () {
                $modalAdd2.modal('hide');

            }
        }

    }

    Page.initialize();
})

function showPopup (title, content) {
    if (title == "Info")
    {

        $.confirm({
            title: title,
            content: content,
            type: 'blue',
            typeAnimated: true,
            buttons: {
                OK: {
                    text: 'OK',
                    btnClass: 'btn-red',
                    action: function () {
                        //$('input[name=UserName]').val($('input[name=UserName]').val());
                    }
                }
            }
        });
    }
    else if (title == "Confirm")
    {

        $.dialog({
            title: title,
            content: content
        });
    }
    else if (title == "Register")
    {
        $.dialog({
            title: title,
            content: content
        });
    }


}
