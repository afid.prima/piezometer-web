﻿function Setup() {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddModule',
            saveButton: '#btnSave',
            saveAndAddModuleAccessButton: '#btnSaveAndAddModuleAccess',
            modalAdd: '#mdlAdd',
            tabModule: '#tabModule',
            table: '#tblmodule',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $saveAndAddModuleAccessButton = $(this.options.saveAndAddModuleAccessButton);
            $modalAdd = $(this.options.modalAdd);
            $tabModule = $(this.options.tabModule);
            $table = $(this.options.table);
            _formAdd = this.options.formAdd;
            _tabModule = this.options.tabModule;
            _listModule = [];
            _listModuleCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "targets": 1,
                    "class": "center",
                    width: 75
                }, {
                    "targets": 4,
                    "class": "center",
                    width: 10
                }, {
                    "targets": 5,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 6,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 7,
                    "class": "center",
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetModule_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblactiveuser").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $saveAndAddModuleAccessButton.on('click', this.save);
                $table.on('click', 'a[data-target="#edit"]', this.edit);

                $tabModule.on('click', 'a', this.tabclick);
                //this.formvalidation();
                $modalAdd.on('hidden.bs.modal', this.resetformvalidation);

                //select event
                $('#selectType').on('change', this.selectchange);
            },
            formvalidation: function () {
                //Add On For Validation Rule
            },
            resetformvalidation: function () {
            },
            tabclick: function () {
                //e.preventDefault();
            },
            tabchange: function (tabId) {
            },
            tabclose: function () {
            },
            selectchange: function () {
                GetMasterModuleSubType($(this).val());
            },
            add: function () {
                //Event for adding data
            },
            edit: function () {
                var $row = $(this).closest('tr');
            },
            delete: function () {
                //Event for deleting data
            },
            save: function () {
                //Event for saving data
            }
        },
        setCustomFunctions: {
            init: function () {
                this.getModule();
                this.getMasterModuleGroup();
                this.getMasterModuleType();
            },
            registerTabContent: function (tabId, mdlcode) {
            },
            getModule: function () {
                
            },
            getMasterModuleGroup: function () {
               
            },
            getMasterModuleType: function () {
                
            },
            getMasterModuleSubType: function (type) {
                
            },
            saveModule: function (data) {
                
            },
            showPopup: function (title, content) {
                
            }
        }

    }

    Page.initialize();
}