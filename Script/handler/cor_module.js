﻿var checktree;
var _data = [];
var actionTable2 = [];
var actionTableDelete = [];
var actionAdd = [];
var _n;
var mdlAccCode;
var callBack;
var _mdlAccCode = [];

$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            formAddmdlAccess: '#formAddmdlAccess',
            formAddsubmdlAccess: '#formAddsubmdlAccess',
            addButton: '#btnAddModule',
            //addmdlAccessButton: '#btnAddModuleAccess',
            saveButton: '#btnSave',
            updateButton: '#btnUpdate',
            saveAndAddModuleAccessButton: '#btnSaveAndAddModuleAccess',
            modalAdd: '#mdlAdd',
            modalAddModuleAccess: '#mdlAddModuleAccess',
            modalAddSubModuleAccess: '#mdlAddSubModuleAccess',
            tabModule: '#tabModule',
            table: '#tblmodule',
            tableModuleAccess: '#tblModuleAccess',
            //mdlacc
            savemdlAccCodeButton: '#btnSavemdlAccess',
            updatemdlAccCodeButton: '#btnUpdatemdlAccess',
            //mdlestacc
            savemdlEstAccCodeButton: '#btnSavemdlEstAccess',
            addSubModuleButton: '#btnAddSubModule',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            //this.setTableModuleEstAccess();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $formAddmdlAccess = $(this.options.formAddmdlAccess);
            $formAddsubmdlAccess = $(this.options.formAddsubmdlAccess);
            $addButton = $(this.options.addButton);
            //$addmdlAccessButton = $(this.options.addmdlAccessButton);
            $saveButton = $(this.options.saveButton);
            $updateButton = $(this.options.updateButton);
            $saveAndAddModuleAccessButton = $(this.options.saveAndAddModuleAccessButton);
            $modalAdd = $(this.options.modalAdd);
            $modalAddModuleAcc = $(this.options.modalAddModuleAccess);
            $modalAddSubModuleAccess = $(this.options.modalAddSubModuleAccess);
            $tabModule = $(this.options.tabModule);
            $table = $(this.options.table);
            $tableModuleAccess = $(this.options.tableModuleAccess);
            //mdlacc
            $savemdlAccCodeButton = $(this.options.savemdlAccCodeButton);
            $updatemdlAccCodeButton = $(this.options.updatemdlAccCodeButton);
            //mdlestacc
            $savemdlEstAccCodeButton = $(this.options.savemdlEstAccCodeButton);
            $addSubModuleButton = $(this.options.addSubModuleButton);
            $test1 = $(this.options.test1);
            _formAdd = this.options.formAdd;
            _formAddmdlAccess = this.options.formAddmdlAccess;
            _formAddsubmdlAccess = this.options.formAddsubmdlAccess;
            _tabModule = this.options.tabModule;
            _listModule = [];
            _listModuleCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;

        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "targets": 1,
                    "class": "center",
                    width: 75
                }, {
                    "targets": 4,
                    "class": "center",
                    width: 10
                }, {
                    "targets": 5,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 6,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 7,
                    "class": "center",
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetModule_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    //aoData.push({ 'name': 'mdlcode', 'value': 'dsdad' })
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblactiveuser").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                //this.setData();
                $addButton.on('click', this.add);
                //$addmdlAccessButton.on('click', this.addmdlAccess);
                $saveButton.on('click', this.save);
                $updateButton.on('click', this.update);
                $saveAndAddModuleAccessButton.on('click', this.save);
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $table.on('click', 'a[data-target="#delete"]', this.delete);

                $tabModule.on('click', 'a', this.tabclick);
                //this.formvalidation();
                $modalAdd.on('hidden.bs.modal', this.resetformvalidation);
                $modalAddModuleAcc.on('hidden.bs.modal', this.resetformvalidation);
                $modalAddSubModuleAccess.on('hidden.bs.modal', this.resetformvalidation);
                //select event
                $('#selectType').on('change', this.selectchange);
                //$('#selectTypeEdit').on('change', this.selectchangeEdit);
                //mdlacc
                $savemdlAccCodeButton.on('click', this.savemdlAccess);
                $updatemdlAccCodeButton.on('click', this.updatemdlAccess);
                //mdlestacc
                $savemdlEstAccCodeButton.on('click', this.savemdlEstAccess);
                //$updatesubmdlAccCodeButton.on('change', this.updatesubmdlAccCodeAccess);
                $addSubModuleButton.on('click', this.test1);
            },
            formvalidation: function () {
                //Add On For Validation Rule
                $.validator.addMethod("alreadyexist", function (value, element) {
                    return _listModuleCode.indexOf(value) == -1;
                }, "Module Code is already exist");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 3,
                            maxlength: 3,
                            required: true,
                            alreadyexist: true
                        },
                        name: {
                            minlength: 1,
                            required: true
                        },
                        group: {
                            required: true
                        },
                        type: {
                            required: true
                        }
                        //subtype: {
                        //    required: true
                        //}
                    },

                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                $formAddmdlAccess.validate({
                    rules: {
                        //code: {
                        //    minlength: 3,
                        //    maxlength: 3,
                        //    required: true,
                        //    alreadyexist: true
                        //},
                        //name: {
                        //    minlength: 1,
                        //    required: true
                        //},
                        //group: {
                        //    required: true
                        //},
                        //type: {
                        //    required: true
                        //}
                        ////subtype: {
                        ////    required: true
                        ////}
                    }, highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                $formAddsubmdlAccess.validate({
                    rules: {
                        subMdlCode: {
                            required: true,
                        },
                        subMdlName: {
                            required: true
                        },
                    }, highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                //e.preventDefault();
                $(this).tab('show');
            },
            tabchange: function (tabId) {
                $(_tabModule + ' a[href="#' + tabId + '"]').tab('show');
            },
            tabclose: function () {
                $.confirm({
                    title: 'Confirmation',
                    content: 'You are editing something. Are you sure to close existing edit ? ',
                    buttons: {
                        cancel: function () {

                        },
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                _currentEditTabId = "";
                                _isEditTabExists = false;

                                //there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
                                var tabContentId = $(".closeTab").parent().attr("href");
                                $(".closeTab").parent().parent().remove(); //remove li of tab
                                $(_tabModule + ' a:first').tab('show'); // Select first tab
                                $(tabContentId).remove(); //remove respective tab content
                            }
                        }
                    }
                });
            },
            selectchange: function () {
                _setCustomFunctions.getMasterModuleSubTypeByTypeCode();
            },
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                _setEvents.formvalidation();

                $('input[name=code]').val('');
                $('input[name=name]').val('');
                $('#selectGroup').val('');
                $('#selectType').val('');
                $("#selectType").val("");
                $('#selectSubType').val('');
                $("#status").attr("checked", false);

            },
            addmdlAccess: function () {
                //Event for adding data
                $modalAddModuleAcc.modal('toggle');
                _setEvents.formvalidation();
            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var mdlcode = $row.find('.mdlCode').html();
                var mdldesc = $row.find('.mdlDesc').html();
                //Set Ajax Submit Here
                var data = { mdlCode: mdlcode };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(1, data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            edit: function () {
                var $row = $(this).closest('tr');
                var mdlcode = $row.find('.mdlCode').html();
                var mdldesc = $row.find('.mdlDesc').html();
                //$("#tbodyid").empty();
                $('input[name=codeEdit]').val('');
                $('input[name=nameEdit]').val('');
                $('#selectGroupEdit').val('');
                $('#selectTypeEdit').val('');
                $("#selectTypeEdit").val("");
                $('#selectSubTypeEdit').val('');
                $("#statusEdit").attr("checked", false);
                //SetData();
                //Event for editing data
                var tabId = "edit" + mdlcode;
                if ($(_tabModule + ' .nav-tabs a[href="#' + tabId + '"]').length) {
                    _setEvents.tabchange(tabId);
                    $(".editmodule").on('click', _setEvents.saveEdit);
                    //$(".check").change(this.checkboxChanged);
                    //$("#demo").treeMultiselect(options);
                    _setCustomFunctions.getModuleData($('input[name=codeEditHidden]').val());
                    //mdlacc
                    //_setCustomFunctions.setTableModuleAccess($('input[name=codeEditHidden]').val());
                    //mdlestacc
                    _setCustomFunctions.setTree($('input[name=codeEditHidden]').val());
                    mdlAccCode = $('input[name=codeEditHidden]').val();
                    _setCustomFunctions.setTableSubModuleAccess(mdlAccCode);
                    //$tableModuleAccess.DataTable().ajax.reload();
                } else {
                    if (_isEditTabExists) {
                        $.confirm({
                            title: 'Confirmation',
                            content: 'You are editing something. Are you sure to close existing edit ? ',
                            buttons: {
                                GoToTabEditing: {
                                    text: 'Go To Editing Tab',
                                    btnClass: 'btn-orange',
                                    action: function () {
                                        _setEvents.tabchange(_currentEditTabId);
                                        _setCustomFunctions.getModuleData($('input[name=codeEditHidden]').val());
                                        //mdlacc
                                        //_setCustomFunctions.setTableModuleAccess($('input[name=codeEditHidden]').val());
                                        //mdlestacc
                                        _setCustomFunctions.setTree($('input[name=codeEditHidden]').val());
                                        mdlAccCode = $('input[name=codeEditHidden]').val();
                                        _setCustomFunctions.setTableSubModuleAccess(mdlAccCode);
                                        //$tableModuleAccess.DataTable().ajax.reload();
                                    }
                                },
                                cancel: function () {

                                },
                                OK: {
                                    text: 'OK',
                                    btnClass: 'btn-blue',
                                    action: function () {
                                        //there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
                                        var tabContentId = _currentEditTabId;
                                        $('#tabModule .nav-tabs a[href="#' + tabContentId + '"]').parent().remove(); //remove li of tab
                                        $('#tabModule a:first').tab('show'); // Select first tab
                                        $('#' + tabContentId).remove(); //remove respective tab content


                                        _isEditTabExists = true;
                                        _currentEditTabId = tabId;
                                        $('#tabModule .nav-tabs').append('<li><a href="#' + tabId + '" data-togle="tab"><button class="close closeTab" type="button" >×</button>Edit - ' + mdldesc + '</a></li>');
                                        $('#tabModule .tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

                                        _setCustomFunctions.registerTabContent(tabId, mdlcode);
                                        _setEvents.tabchange(tabId);
                                        _setCustomFunctions.getModuleData(mdlcode);
                                        $(".editmodule").on('click', _setEvents.saveEdit);
                                        //$("#demo").treeMultiselect(options);
                                        //mdlacc
                                        //_setCustomFunctions.setTableModuleAccess(mdlcode);
                                        $(".addmdlacc").on('click', _setEvents.addsmdlacc);
                                        _setEvents.formvalidation();
                                        $(".addsubmdlacc").on('click', _setEvents.addsubmdlAccCodeAccess);
                                        $(".updatesubmdlacc").on('click', _setEvents.updatesubmdlAccCodeAccess);

                                        //mdlestacc
                                        $(".addmdlestacc").on('click', _setEvents.addsmdlestacc);
                                        _setCustomFunctions.setTree(mdlcode);
                                        //$(".editDept").on('click', _setEvents.saveEdit);
                                        //$(".check").change(this.checkboxChanged);
                                        mdlAccCode = mdlcode;
                                        _setCustomFunctions.setTableSubModuleAccess(mdlAccCode);
                                        $(".closeTab").on('click', _setEvents.tabclose);
                                        //$tableModuleAccess.DataTable().ajax.reload();
                                    }
                                }
                            }
                        });
                    } else {
                        _isEditTabExists = true;
                        _currentEditTabId = tabId;
                        $('#tabModule .nav-tabs').append('<li><a href="#' + tabId + '" data-togle="tab"><button class="close closeTab" type="button" >×</button>Edit - ' + mdldesc + '</a></li>');
                        $('#tabModule .tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

                        _setCustomFunctions.registerTabContent(tabId, mdlcode);
                        _setEvents.tabchange(tabId);
                        _setCustomFunctions.getModuleData(mdlcode);
                        $(".editmodule").on('click', _setEvents.saveEdit);
                        //mdlacc
                        //_setCustomFunctions.setTableModuleAccess(mdlcode);
                        $(".addmdlacc").on('click', _setEvents.addsmdlacc);
                        $(".addmdlestacc").on('click', _setEvents.addsmdlestacc);
                        _setEvents.formvalidation();
                        $(".addsubmdlacc").on('click', _setEvents.addsubmdlAccCodeAccess);
                        $(".updatesubmdlacc").on('click', _setEvents.updatesubmdlAccCodeAccess);

                        //mdlestacc
                        _setCustomFunctions.setTree(mdlcode);
                        //_setEvents.formvalidation();
                        //$(".editDept").on('click', _setEvents.saveEdit);
                        mdlAccCode = mdlcode;
                        _setCustomFunctions.setTableSubModuleAccess(mdlAccCode);
                        $(".closeTab").on('click', _setEvents.tabclose);
                        //$tableModuleAccess.DataTable().ajax.reload();

                        //$("#tall").change(this.checkboxChanged);
                    }
                }


            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { mdlCode: $('input[name=code]').val(), mdlDesc: $('input[name=name]').val(), mdlGroupCode: $('#selectGroup').val(), mdlSubTypeCode: $('#selectSubType').val(), MdlStatus: $('input[name=status]').prop('checked') ? 1 : 0 };
                    //var savetype = 

                    _setCustomFunctions.saveModule(data);

                    return false;
                }
            },
            savemdlAccess: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAddmdlAccess :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { mdlCode: $('input[name=codeModule]').val(), mdlAccCode: $('input[name=mdlAcccode]').val(), mdlAccDesc: $('input[name=mdlAccDesc]').val(), mdlAccInfo: $('input[name=mdlAccInfo]').val() };
                    //var savetype = 

                    _setCustomFunctions.saveModuleAccess(data);

                    return false;
                }
            },
            savemdlEstAccess: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAddmdlAccess :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { mdlCode: $('input[name=codeModule]').val(), EstCode: $('#SelectEstCode').val() };
                    //var savetype = 

                    _setCustomFunctions.saveModuleEstAccess(data);

                    return false;
                }
            },
            updatemdlAccess: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $("#formAddmdlAccess :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { NewMdlCode: $('input[name=mdlAcccode]').val(), mdlAccCode: $('input[name=mdlAcccodeHidden]').val(), mdlCode: $('input[name=codeModule]').val(), mdlAccDesc: $('input[name=mdlAccDesc]').val(), mdlAccInfo: $('input[name=mdlAccInfo]').val() };
                    //var savetype = 

                    _setCustomFunctions.UpdateModuleAccess(data);

                    return false;
                }
            },
            addsubmdlAccCodeAccess: function () {
                //if ($formAddsubmdlAccess.valid()) {
                    var $this = $(this);
                    //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAddmdlAccess :input").prop("disabled", true);
                    _mdlAccCode[0].toString();
                    //Set Ajax Submit Here
                    var data = { subMdlAccCode: $('input[name=subMdlCode]').val(), subMdlName: $('input[name = subMdlName]').val(), subMdlRemarks: $('input[name = subMdlRemarks]').val(), mdlAccCode: _mdlAccCode[0].toString() };
                    //var savetype = 

                    _setCustomFunctions.SavesubModuleAccess(data);

                    return false;
                //}
            },
            updatesubmdlAccCodeAccess: function () {
                //Event for saving data
                if ($formAddsubmdlAccess.valid()) {
                    var $this = $(this);
                    //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAddmdlAccess :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { subMdlAccCode: $('input[name=subMdlcodeHidden]').val(), NewsubMdlAccCode: $('input[name=subMdlCode]').val(), subMdlName: $('input[name = subMdlName]').val(), subMdlRemarks: $('input[name = subMdlRemarks]').val() };
                    //var savetype = 

                    _setCustomFunctions.UpdatesubModuleAccess(data);

                    return false;
                }
            },
            test1: function () {
                //Event for saving data
                alert("sss");
            },
            saveEdit: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { mdlCode: $('input[name=codeEditHidden]').val(), NewMdlCode: $('input[name=codeEdit]').val(), mdlDesc: $('input[name=nameEdit]').val(), mdlGroupCode: $('#selectGroupEdit').val(), mdlSubTypeCode: $('#selectSubTypeEdit').val(), MdlStatus: $('input[name=statusEdit]').prop('checked') ? 1 : 0 };
                    //var savetype = 

                    _setCustomFunctions.updateModule(data);

                    return false;
                }
            },
            addsmdlacc: function () {
                document.getElementById('btnUpdatemdlAccess').style.display = 'none';
                document.getElementById('btnSavemdlAccess').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Module Access";
                //Event for adding data
                //$modalAddSection.modal('toggle');codeEditHidden
                var mdlCode = $('input[name=codeEditHidden]').val();
                $modalAddModuleAcc.modal('toggle');
                $('input[name=mdlAcccode]').val('');
                $('input[name=mdlAcccodeHidden]').val('');
                $('input[name=mdlAccDesc]').val('');
                $('input[name=mdlAccInfo]').val('');
                $('input[name=codeModule]').val(mdlCode);

                _setEvents.formvalidation();
            },
            addsmdlestacc: function () {


                //data
                var data = { mdlCode: $('input[name=codeEditHidden]').val() };
                _setCustomFunctions.saveModuleEstAccess(data, _data);

                //_setEvents.formvalidation();
            },
            editmdlAcc: function () {
                document.getElementById('btnUpdatemdlAccess').style.display = 'inline';
                document.getElementById('btnSavemdlAccess').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Module Access";
                var $row = $(this).closest('tr');
                var mdlacccode = $row.find('.mdlAccCode').html();
                $modalAddModuleAcc.modal('toggle');

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetModuleAccess',
                    type: "POST",
                    data: "{'mdlAccCode' :'" + mdlacccode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=mdlAcccodeHidden]').val(obj.mdlAccCode);
                            $('input[name=mdlAccDesc]').val(obj.mdlAccDesc);
                            $('input[name=mdlAcccode]').val(obj.mdlAccCode);
                            $('input[name=mdlAccInfo]').val(obj.mdlAccInfo);
                            $('input[name=codeModule]').val(obj.mdlCode);
                            //$('input[name=module]').val(obj.mdlDesc);

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

            },
            deletemdlAcc: function () {
                var $row = $(this).closest('tr');
                var mdlAccCode = $row.find('.mdlAccCode').html();
                var data = { mdlAccCode: mdlAccCode };
                _setCustomFunctions.showConfirmPopup(2, data, 'confirm', 'Are You Sure Delete This Data Module Access?');

                return false;
            },
            editsubmdlAcc: function (_submdlCode) {
                document.getElementById('btnUpdatesubmdlAccess').style.display = 'inline';
                document.getElementById('btnSavesubmdlAccess').style.display = 'none';

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetSubModuleAccess',
                    type: "POST",
                    data: "{'subMdlAccCode' :'" + _submdlCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=subMdlCode]').val(obj.subMdlAccCode);
                            $('input[name=subMdlName]').val(obj.subMdlName);

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

            },
            deletesubmdlAcc: function (_submdlCode) {
                var data = { subMdlAccCode: _submdlCode };
                _setCustomFunctions.showConfirmPopup(3, data, 'confirm', 'Are You Sure Delete This Data Module Estate Access?');

                return false;
            },
            detailmodule: function () {
                var tr = $(this).closest('tr');
                var row = $('#tblModuleAccess').DataTable().row(tr);
                //var $row = $(this).closest('tr');
                var mdlAccCode = tr.find('.mdlAccCode').html();

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    _setCustomFunctions.format(row.child, mdlAccCode)
                    tr.addClass('shown');
                }
            },
            closeDetail: function () {
                document.getElementById('plus').style.display = 'inline';
                document.getElementById('minus').style.display = 'none';
                return false;
            },
        },
        setCustomFunctions: {
            init: function () {
                this.getModule();
                this.getMasterModuleGroup();
                this.getMasterModuleType();
                this.getMasterModuleSubType();
                this.getEstCode();
            },
            setTableSubModuleAccess: function (mdlAccCode) {
                //Initialization of main table or data here
                var table = $('#tblModuleAccess').DataTable({
                    dom: 'lfrtip',
                    lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                    pageLength: 25,
                    "filter": true,
                    "columnDefs": [{
                        "searchable": false,
                        "orderable": false,
                        "class": "details-control",
                        "data": null,
                        "defaultContent": '',
                        "targets": 0,
                        width: 5
                    }, {
                        "targets": 1,
                        "class": "center"
                    }, {
                        "targets": 4,
                        "class": "center",
                        width: 10
                    }, {
                        "targets": 5,
                        "class": "center",
                        width: 10
                    }],
                    "orderClasses": false,
                    "order": [[1, "asc"]],
                    "info": true,
                    //"scrollY": "450px",
                    //"scrollCollapse": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "destroy": true,
                    "sAjaxSource": "../../webservice/WebService_COR.asmx/GetModuleAccess_ServerSideProcessing",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        //var _mdlacccode = "";
                        //if (mdlAccCode != "" && mdlAccCode != undefined) {
                        //    _mdlacccode = mdlAccCode;
                        //    otherID = "";
                        //}
                        //else {
                        //    _mdlacccode = ""
                        //}

                        aoData.push({ 'name': 'mdlCode', 'value': mdlAccCode });
                        //aoData.push({ 'name': 'mdlcode', 'value': 'dsdad' })
                        $.ajax({
                            "dataType": 'json',
                            "contentType": "application/json; charset=utf-8",
                            "type": "GET",
                            "url": sSource,
                            "data": aoData,
                            "success": function (msg) {
                                var json = jQuery.parseJSON(msg.d);
                                fnCallback(json);
                                //table.clear().rows.add(json).draw()
                                $("#tblModuleAccess").show();
                                $(".editmdlAcc").on('click', _setEvents.editmdlAcc);
                                $(".deletemdlAcc").on('click', _setEvents.deletemdlAcc);
                                $(".details-control").on('click', _setEvents.detailmodule);
                            },
                            error: function (xhr, textStatus, error) {
                                if (typeof console == "object") {
                                    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                }
                            }
                        });
                    },
                    fnDrawCallback: function () {
                    },
                    fnRowCallback: function (nRow, aData, iDisplayIndex) {
                        var info = $(this).DataTable().page.info();
                        var page = info.page;
                        var length = info.length;
                        var index = (page * length + (iDisplayIndex + 1));
                        //$('td:eq(0)', nRow).html(index);
                    }
                });

            },
            getModuleData: function (mdlCode) {
                this.getMasterModuleSubType();
                var _status = "";
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/getModuleData',
                    type: "POST",
                    data: "{'mdlCode' :'" + mdlCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('#selectTypeEdit').val(obj.mdlTypeCode);
                            if (obj.mdlStatus == 1) {
                                _status = true;
                            }
                            else {
                                _status = false;
                            }
                            $('input[name=codeEdit]').val(obj.mdlCode);
                            $('input[name=codeEditHidden]').val(obj.mdlCode);
                            $('input[name=nameEdit]').val(obj.mdlDesc);
                            $('#selectGroupEdit').val(obj.mdlGroupCode);
                            //$("#status").prop('checked', _status);
                            //$("#statusEdit").attr("checked", true);
                            $('#statusEdit').prop('checked', _status);
                            //$('#selectSubTypeEdit').val(obj.mdlSubTypeCode);
                            SetSubType(obj.mdlSubTypeCode)

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });
            },
            registerTabContent: function (tabId, mdlcode) {
                $('.divtemplate').clone().appendTo($('#' + tabId));
                $('#' + tabId + " .divtemplate").css('display', 'block');
            },
            getModule: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetModule',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        _listModule = $.parseJSON(response.d);

                        _listModuleCode = [];
                        _listModule.forEach(function (obj) {
                            _listModuleCode.push(obj.mdlCode);
                        })
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getMasterModuleGroup: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterModuleGroup',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#selectGroup').find('option').remove();
                        $('#selectGroupEdit').find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#selectGroup").append($('<option>', {
                                value: obj.mdlGroupCode,
                                text: obj.mdlGroupDesc
                            }));
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#selectGroupEdit").append($('<option>', {
                                value: obj.mdlGroupCode,
                                text: obj.mdlGroupDesc
                            }));
                        })
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getMasterModuleType: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterModuleType',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#selectType').find('option').remove();
                        $('#selectTypeEdit').find('option').remove();
                        json.forEach(function (obj) {
                            $("#selectType").append($('<option>', {
                                value: obj.mdlTypeCode,
                                text: obj.mdlTypeCode + ' - ' + obj.mdlTypeName
                            }));
                            $("#selectTypeEdit").append($('<option>', {
                                value: obj.mdlTypeCode,
                                text: obj.mdlTypeCode + ' - ' + obj.mdlTypeName
                            }));
                        })

                        _parent.getMasterModuleSubTypeByTypeCode();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getMasterModuleSubType: function () {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterModuleSubType',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#selectSubType').find('option').remove();
                        $('#selectSubTypeEdit').find('option').remove();
                        json.forEach(function (obj) {
                            $("#selectSubType").append($('<option>', {
                                value: obj.mdlSubTypeCode,
                                text: obj.mdlSubTypeCode + ' - ' + obj.mdlSubTypeName
                            }));
                            $("#selectSubTypeEdit").append($('<option>', {
                                value: obj.mdlSubTypeCode,
                                text: obj.mdlSubTypeCode + ' - ' + obj.mdlSubTypeName
                            }));
                        })
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getMasterModuleSubTypeSetValue: function () {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterModuleSubType',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#selectSubType').find('option').remove();
                        $('#selectSubTypeEdit').find('option').remove();
                        json.forEach(function (obj) {
                            $("#selectSubType").append($('<option>', {
                                value: obj.mdlSubTypeCode,
                                text: obj.mdlSubTypeCode + ' - ' + obj.mdlSubTypeName
                            }));
                            $("#selectSubTypeEdit").append($('<option>', {
                                value: obj.mdlSubTypeCode,
                                text: obj.mdlSubTypeCode + ' - ' + obj.mdlSubTypeName
                            }));
                        })
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getMasterModuleSubTypeByTypeCode: function () {
                var mdlTypeCode = $('#selectType').val();
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterModuleSubTypeByTypeCode',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'mdlTypeCode' :'" + mdlTypeCode + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#selectSubType').find('option').remove();
                        $('#selectSubTypeEdit').find('option').remove();
                        json.forEach(function (obj) {
                            $("#selectSubType").append($('<option>', {
                                value: obj.mdlSubTypeCode,
                                text: obj.mdlSubTypeCode + ' - ' + obj.mdlSubTypeName
                            }));
                            $("#selectSubTypeEdit").append($('<option>', {
                                value: obj.mdlSubTypeCode,
                                text: obj.mdlSubTypeCode + " - " + obj.mdlSubTypeName
                            }));
                        })
                    }
                })
            },
            getEstCode: function () {
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterEstate',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectEstCode').find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $("#SelectEstCode").append($('<option>', {
                                value: obj.estCode,
                                text: obj.estCode + " - " + obj.estName
                            }));
                        })
                    }
                })
            },
            saveModule: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveModule',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            //_setCustomFunctions.getModule();

                            _setCustomFunctions.showPopup('Info', 'Module has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveModuleAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveModuleAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            var table = document.getElementById("tblModuleAccess");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAddmdlAccess + " :input").prop("disabled", false);
                            //history.go(0); //forward
                            $modalAddModuleAcc.modal('hide');
                            _setCustomFunctions.setTableSubModuleAccess($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'Module Access has been saved successfully');
                        } else {
                            var table = document.getElementById("tblModuleAccess");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAddmdlAccess + " :input").prop("disabled", false);
                            //history.go(0); //forward
                            $modalAddModuleAcc.modal('hide');
                            _setCustomFunctions.setTableSubModuleAccess($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveModuleEstAccess: function (datas, selected) {
                var data = { mdlCode: datas.mdlCode, ListEstAccess: selected };
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveModuleEstAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            //history.go(0); //forward
                            $("#tvestate").jstree('destroy');
                            //$('#tvestate').load(document.URL + ' #tvestate');
                            _setCustomFunctions.setTree(datas.mdlCode);
                            //_setCustomFunctions.showPopup('Info', 'Module Estate Access has been saved successfully');
                            //_setCustomFunctions.setTree(datas.mdlCode);
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', response.d);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            SavesubModuleAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/AddsubModuleAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAddsubmdlAccess + " :input").prop("disabled", false);
                            $modalAddSubModuleAccess.modal('hide');
                            $('#tblModuleAccess').DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Sub Module Access has been saved successfully');
                        } else {
                            $(_formAddsubmdlAccess + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            UpdatesubModuleAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/UpdatesubModuleAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAddsubmdlAccess + " :input").prop("disabled", false);
                            $modalAddSubModuleAccess.modal('hide');
                            $('#tblModuleAccess').DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Sub Module Access has been saved successfully');
                        } else {
                            $(_formAddsubmdlAccess + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            UpdateModuleAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/UpdateModuleAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            var table = document.getElementById("tblModuleAccess");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAddmdlAccess + " :input").prop("disabled", false);
                            //history.go(0); //forward
                            $modalAddModuleAcc.modal('hide');
                            //_setCustomFunctions.setTableModuleAccess($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'Module Access has been saved successfully');
                        } else {
                            var table = document.getElementById("tblModuleAccess");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAddmdlAccess + " :input").prop("disabled", false);
                            //history.go(0); //forward
                            $modalAddModuleAcc.modal('hide');
                            //_setCustomFunctions.setTableModuleAccess($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            updateModule: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/UpdateModule',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            _setCustomFunctions.showPopup('Confirm', 'Module has been saved successfully');

                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                if (title == "Info") {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            OK: {
                                text: 'OK',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            OK: {
                                text: 'OK',
                                btnClass: 'btn-blue',
                                action: function () {
                                    $(_formAdd + " :input").prop("disabled", false);
                                    history.go(0); //forward
                                    //_setCustomFunctions.getModule();
                                    //$modalAdd.modal('hide');
                                }
                            }
                        }
                    });

                }

            },
            showConfirmPopup: function (param, data, title, content) {
                if (param == 1) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeleteModule(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else if (param == 2) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeletemdlAcc(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else if (param == 3) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeletesubmdlAcc(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else {
                    alert("Invalid Processing");
                }


            },
            DeleteModule: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteModule',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAddmdlAccess + " :input").prop("disabled", false);
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Module Access has been Delete successfully');
                        } else {
                            $(_formAddmdlAccess + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeletemdlAcc: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteModuleAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            var table = document.getElementById("tblModuleAccess");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAddmdlAccess + " :input").prop("disabled", false);
                            //history.go(0); //forward
                            //_setCustomFunctions.setTableModuleAccess($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'Module Access has been Delete successfully');
                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                            var table = document.getElementById("tblModuleAccess");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAddmdlAccess + " :input").prop("disabled", false);
                            //history.go(0); //forward
                            //_setCustomFunctions.setTableModuleAccess($('input[name=codeEdit]').val());
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeletesubmdlAcc: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteSubModuleAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            _setCustomFunctions.showPopup('Info', 'Sub Module Access has been Delete successfully');
                            $('#tblModuleAccess').DataTable().ajax.reload();
                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                            $('#tblModuleAccess').DataTable().ajax.reload();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            setTree: function (mdlcode) {
                var datas = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetEstAccess',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: "{'mdlCode' :'" + mdlcode + "'}",
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0 ; i < json.length; i++) {
                            datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }
                        $('#tvestate').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                'data':
                                    datas
                            }
                        }).bind("loaded.jstree", function (event, data) {
                            $(this).jstree("open_all");
                        });
                        $('#tvestate')
                        // listen for event
                        .on('changed.jstree', function (e, data) {
                            _data = data.instance.get_bottom_selected()
                        })
                        // create the instance
                        .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("gagal");
                    }
                });



            },
            format: function (callback, mdlAccCode) {
                var contentHTML;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetSubModuleAccess_ServerSideProcessing',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: JSON.stringify({ data: mdlAccCode }),
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        var thead = '', tbody = '', tbutton = '';
                        actionTableAdd = [];
                        var idZ = "add" + mdlAccCode;
                        tbutton += '<button id=' + idZ + ' type="button" class="btn btn-success pull-right" ><span class="glyphicon glyphicon-plus"></span>&nbsp;Add Module</button><br>';
                        actionTableAdd.push(idZ);

                        for (var key in json[0]) {
                            thead += '<th>' + key + '</th>';
                        }
                        thead += '<th>Edit</th>';
                        thead += '<th>Delete</th>';
                        if (json.length != 0) {
                            actionTable2 = [];
                            actionTableDelete = [];
                            $.each(json, function (i, d) {
                                var idX = "edit" + i;
                                var idY = "delete" + i;
                                tbody += '<tr><td class="submdlCode">' + d.subMdlAccCode + '</td><td>' + d.subMdlName + '</td><td class="mdlAccCode">' + d.mdlAccCode + '</td>'
                                tbody += '<td><a id=' + idX + ' type="button" class="btn btn-primary btn-xs editDetail" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></a></td>'
                                tbody += '<td><a id=' + idY + ' type="button" class="btn btn-danger btn-xs deleteDetail" data-title="Delete" data-toggle="modal" data-target="#delete"><span class="glyphicon glyphicon-trash"></span></a></td></tr>';
                                actionTable2.push(idX);
                                actionTableDelete.push(idY);
                            });

                        }
                        else {
                            thead += '<th>Sub mdl Code</th><th>Sub mdl Name</th><th>Edit</th><th>Delete</th>';
                            tbody += '<tr><td colspan="6" style="text-align: center;">No Data Present</td><tr>';
                        }

                        callback($(tbutton + '<table id="tblDetailModuleAccess" class="table table-striped table-bordered" style="width:100%;font-size:9pt;">' + thead + tbody + '</table>')).show();

                        _mdlAccCode = [];
                        for (var i = 0 ; i < actionTableAdd.length; i++) {
                            $("#" + actionTableAdd[i]).click(function () {
                                $modalAddSubModuleAccess.modal('toggle');
                                document.getElementById('btnUpdatesubmdlAccess').style.display = 'none';
                                document.getElementById('btnSavesubmdlAccess').style.display = 'inline';
                                _mdlAccCode.push(mdlAccCode);
                            });
                        }

                        for (var i = 0 ; i < actionTable2.length; i++) {
                            $("#" + actionTable2[i]).click(function () {
                                var $row = $(this).closest('tr');
                                var submdlCode = $row.find('.submdlCode').html();
                                //_setCustomFunctions.EditSubModule(submdlCode);
                                _setEvents.editsubmdlAcc(submdlCode);
                                $modalAddSubModuleAccess.modal('toggle');
                            });
                        }

                        for (var i = 0 ; i < actionTableDelete.length; i++) {
                            $("#" + actionTableDelete[i]).click(function () {
                                var $row = $(this).closest('tr');
                                var submdlCode = $row.find('.submdlCode').html();
                                //_setCustomFunctions.EditSubModule(submdlCode);
                                _setEvents.deletesubmdlAcc(submdlCode);
                            });
                        }

                    }
                });

            },


        }

    }

    Page.initialize();
})

function SetSubType(_mdlSubTypeCode) {
    $.ajax({
        type: 'POST',
        url: '../../webservice/WebService_COR.asmx/GetMasterModuleSubType',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            $('#selectSubTypeEdit').find('option').remove();
            json.forEach(function (obj) {
                $("#selectSubTypeEdit").append($('<option>', {
                    value: obj.mdlSubTypeCode,
                    text: obj.mdlSubTypeCode + ' - ' + obj.mdlSubTypeName
                }));
            })
            $('#selectSubTypeEdit').val(_mdlSubTypeCode);
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

