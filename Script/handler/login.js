﻿$(function () {
    var _emailDefault;
    var Page = {
        options: {
            formLogin: '#formLogin',
            btnLogin: '#btnLogin',
            btnLogin2: '#btnLogin2',
            modalAdd: '#mdlAdd',
            UpdateButton: '#btnUpdate',
        },
        initialize: function () {
            this.setVars();
            this.setEvents.init();
        },
        setVars: function () {
            $formLogin = $(this.options.formLogin);
            $btnLogin = $(this.options.btnLogin);
            $btnLogin2 = $(this.options.btnLogin2);
            $modalAdd = $(this.options.modalAdd);
            $UpdateButton = $(this.options.UpdateButton);
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;

        },
        setEvents: {
            init: function () {
                this.formvalidation($formLogin);
                $UpdateButton.on('click', _setCustomFunctions.ChangePassword);
                $btnLogin2.on('click', _setCustomFunctions.TestByPass);
            },
            formvalidation: function (form) {
                //Main Validation
                form.validate({
                    rules: {
                        username: {
                            required: true,
                            //remote: function () {
                            //    //console.log($('input[name=username]').val());
                            //    return {
                            //        url: "webservice/WebService_COR.asmx/CheckEUCLID",
                            //        type: "POST",
                            //        contentType: "application/json; charset=utf-8",
                            //        dataType: "json",
                            //        data: JSON.stringify({ username: $('input[name=username]').val() }),
                            //        dataFilter: function (data) {
                            //            var msg = JSON.parse(data);
                            //            var json = $.parseJSON(msg.d);
                            //            if (json[0].retunndata == 'false') {
                            //                //_setCustomFunctions.showPopup('Info', 'Name Is Exist in System');
                            //                document.getElementById('password').style.color = '#dd4b39';
                            //                document.getElementById('password').style.borderColor = '#dd4b39';
                            //                document.getElementById('username').style.color = '#dd4b39';
                            //                document.getElementById('username').style.borderColor = '#dd4b39';
                            //                //document.getElementById('help-block').style.display = 'block';
                            //                document.getElementById('errormsg1').style.display = 'block';
                            //                document.getElementById('errormsg2').style.display = 'block';
                            //                //$('.help-block').css('innerHTML', 'Name Is Not Exist in System and Euclid')
                            //                //$('.help-block').css('display', 'block')
                            //                return false;
                            //            }
                            //            else
                            //            {
                            //                //_setEvents.resetformvalidation();
                            //                document.getElementById('password').style.color = '#555555';
                            //                document.getElementById('password').style.borderColor = '#D2D6DE';
                            //                document.getElementById('username').style.color = '#555555';
                            //                document.getElementById('username').style.borderColor = '#D2D6DE';
                            //                return true;

                            //            }
                            //        }
                            //    }
                            //}
                            //remote: function () {
                            //    return {

                            //        url: "webservice/WebService_COR.asmx/CheckUserName",
                            //        type: "POST",
                            //        contentType: "application/json; charset=utf-8",
                            //        dataType: "json",
                            //        data: JSON.stringify({ UserName: $('#username').val() }),
                            //        dataFilter: function (data) {
                            //            var msg = JSON.parse(data);
                            //            return msg.d;
                            //        }
                            //    }
                            //}

                        },
                        password: {
                            required: true,
                            //remote: function () {
                            //    return {

                            //        url: "webservice/WebService_COR.asmx/CheckPassword",
                            //        type: "POST",
                            //        contentType: "application/json; charset=utf-8",
                            //        dataType: "json",
                            //        data: JSON.stringify({ UserPass: $('#password').val(), UserName: $('#username').val() }),
                            //        dataFilter: function (data) {
                            //            var msg = JSON.parse(data);
                            //            return msg.d;
                            //        }
                            //    }
                            //}
                        }
                    },
                    messages: {
                        username: {
                            required: 'Mohon isi username.',
                            //remote: 'User tidak terdaftar pada EUCLID'
                        },
                        password: {
                            required: 'Mohon isi kata sandi.',
                            //remote: 'User tidak terdaftar pada EUCLID'
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            }
        },
        setCustomFunctions: {
            init: function () {
                this.setDefault();
                //this.getMasterModuleType();
            },
            setDefault: function () {
                //document.all('username').innerHTML = '';
                //document.all('password').innerHTML = ''
                document.getElementById('<%=username.ClientID %>').value = "Track";
                document.getElementById('<%=password.ClientID %>').value = "Track";
                //$('#username').val('assa');
                //$('#password').val('assa');
            },
            TestByPass: function () {
                var request = {
                    userName: 'cahyo.adi',
                    password: 'asdfg'
                };
                $.ajax({
                    type: "POST",
                    url: "../../default2.aspx/Bypass_Login",
                    data: JSON.stringify(request),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                    },
                    success: function (result) {
                        alert("We returned: " + result);
                    }
                });
            },
            ChangePassword: function () {
                document.getElementById("checked").src = "image/loading_reset.gif";
                document.getElementById("checked").style.visibility = "visible";
                if ($('input[name=email]').val() != "") {
                    var _email = $('input[name=email]').val();
                    var _emailDomain = 'www.' + $('input[name=email]').val().split('@')[1];

                    $.ajax({
                        type: 'POST',
                        url: 'Service/MapService.asmx/GetEmailDomain',
                        data: "{'email' :'" + _emailDomain + "'}",
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (response) {
                            var json = $.parseJSON(response.d);
                          //  alert(response.d);
                            json.forEach(function (obj) {
                                if (json[0].msg == "true") {
                                    $.ajax({
                                        type: 'POST',
                                        url: 'Service/MapService.asmx/ForgotPassword',
                                        data: JSON.stringify({ userEmail: $('input[name=email]').val() }),
                                        contentType: 'application/json; charset=utf-8',
                                        dataType: 'json',
                                        success: function (response) {
                                           //alert(response.d)
                                            document.getElementById("checked").src = "Image/checked.png";
                                            alert("Periksa email anda untuk melanjutkan proses reset password");

                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                            alert(thrownError);
                                        }
                                    });
                                }
                                else {
                                    alert("Hanya Menggunakan Domain " + json[0].GroupDomain);
                                }

                            })
                        }
                    });


                }
                else {
                    alert("Mohon Isi Field Email");
                }

            },
            OnchangeHaveEmail: function () {
                if ($('#SelectEmail').val() == 1) {
                    document.getElementById('formemail').style.display = 'inline';
                    _setEvents.formvalidation();
                    $('input[name=email2]').val('');
                }
                else {
                    document.getElementById('formemail').style.display = 'none';
                }
            },
            Test: function () {
                $modalAdd.modal('toggle');
                document.getElementById("checked").style.visibility = "hidden";
                $('input[name=email]').val('');
            },
            showPopup: function (title, content) {
                if (title == "Info") {

                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            OK: {
                                text: 'OK',
                                btnClass: 'btn-red',
                                action: function () {
                                    //$('input[name=UserName]').val($('input[name=UserName]').val());
                                }
                            }
                        }
                    });
                }
                else if (title == "Confirm") {

                    $.dialog({
                        title: title,
                        content: content
                    });
                }
                else if (title == "Register") {
                    $.dialog({
                        title: title,
                        content: content
                    });
                }


            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                //_setCustomFunctions.DeleteWebActive(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                            }
                        }
                    }
                });

            },
            Verifikasi: function () {
                $modalAdd2.modal('hide');

            }
        }

    }

    Page.initialize();
})

function tester() {
    alert("Bla");
}