﻿$(function () {
    var Page = {
        options: {
            table: '#tbluser',
            selectFilterArea: '#selectFilterArea',
            selectFilterDivisi: '#selectFilterDivisi',
            selectFilterGang: '#selectFilterGang',
            selectFilterJabatan: '#selectFilterJabatan',
            btnSearch: '#btnSearch'
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $table = $(this.options.table);
            $selectFilterArea = $(this.options.selectFilterArea);
            $selectFilterDivisi = $(this.options.selectFilterDivisi);
            $selectFilterGang = $(this.options.selectFilterGang);
            $selectFilterJabatan = $(this.options.selectFilterJabatan);
            $btnSearch = $(this.options.btnSearch);
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0
                }, {
                    "class": "center",
                    "targets": 4
                }, {
                    "class": "center",
                    "targets": 8
                }

                ],
                "orderClasses": false,
                "order": [[1, "asc"]],
                buttons: [
                    {
                        extend: 'copy',
                        text: 'Copy',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    },
                    {
                        text: 'PDF',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        action: function (e, dt, node, config) {
                            var $this = $(node);
                            $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

                            var opunit = [];
                            if ($selectFilterArea.val() != null) {
                                $selectFilterArea.val().forEach(function (opt) {
                                    opunit.push(opt.split('_')[1]);
                                })
                            }

                            var divisi = $('#selectFilterDivisi').val() == null ? "" : $selectFilterDivisi.val();
                            var gang = $('#selectFilterGang').val() == null ? "" : $selectFilterGang.val();
                            var jabatan = $('#selectFilterJabatan').val() == null ? "" : $selectFilterJabatan.val();
                            $.ajax({
                                type: 'POST',
                                url: '../../webservice/WebService_COR.asmx/GenerateReportUser_IPLAS',
                                data: '{area: "' + opunit + '", divisi: "' + divisi + '", gang: "' + gang + '", jabatan: "' + jabatan + '", filetype: "' + "pdf" + '"}',
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                success: function (response) {
                                    $this.html("PDF");

                                    var content = response.d;
                                    var request = new XMLHttpRequest();
                                    request.open('POST', '../../handlerservice/GetReportHandler.ashx?filename=' + response.d, true);
                                    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                                    request.responseType = 'blob';

                                    request.onload = function () {
                                        // Only handle status code 200
                                        if (request.status === 200) {
                                            // Try to find out the filename from the content disposition `filename` value
                                            var disposition = request.getResponseHeader('content-disposition');
                                            var matches = /"([^"]*)"/.exec(disposition);
                                            var filename = (matches != null && matches[1] ? matches[1] : response.d);

                                            // The actual download
                                            var blob = new Blob([request.response], { type: 'application/pdf' });
                                            var link = document.createElement('a');
                                            link.href = window.URL.createObjectURL(blob);
                                            link.download = filename;

                                            document.body.appendChild(link);

                                            link.click();

                                            document.body.removeChild(link);
                                        }
                                        else {
                                            alert('File not found');
                                        }
                                    };

                                    request.send('content=' + content);
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    console.log(xhr.statusText);
                                }
                            })
                        }
                    },
                    {
                        text: 'Excel',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        action: function (e, dt, node, config) {
                            var $this = $(node);
                            $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

                            var opunit = [];
                            if ($selectFilterArea.val() != null) {
                                $selectFilterArea.val().forEach(function (opt) {
                                    opunit.push(opt.split('_')[1]);
                                })
                            }


                            var divisi = $('#selectFilterDivisi').val() == null ? "" : $selectFilterDivisi.val();
                            var gang = $('#selectFilterGang').val() == null ? "" : $selectFilterGang.val();
                            var jabatan = $('#selectFilterJabatan').val() == null ? "" : $selectFilterJabatan.val();
                            $.ajax({
                                type: 'POST',
                                url: '../../webservice/WebService_COR.asmx/GenerateReportUser_IPLAS',
                                data: '{area: "' + opunit + '", divisi: "' + divisi + '", gang: "' + gang + '", jabatan: "' + jabatan + '", filetype: "' + "excel" + '"}',
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                success: function (response) {
                                    $this.html("Excel");

                                    var content = response.d;
                                    var request = new XMLHttpRequest();
                                    request.open('POST', '../../handlerservice/GetReportHandler.ashx?filename=' + response.d, true);
                                    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                                    request.responseType = 'blob';

                                    request.onload = function () {
                                        // Only handle status code 200
                                        if (request.status === 200) {
                                            // Try to find out the filename from the content disposition `filename` value
                                            var disposition = request.getResponseHeader('content-disposition');
                                            var matches = /"([^"]*)"/.exec(disposition);
                                            var filename = (matches != null && matches[1] ? matches[1] : response.d);

                                            // The actual download
                                            var blob = new Blob([request.response], { type: 'application/pdf' });
                                            var link = document.createElement('a');
                                            link.href = window.URL.createObjectURL(blob);
                                            link.download = filename;

                                            document.body.appendChild(link);

                                            link.click();

                                            document.body.removeChild(link);
                                        }
                                        else {
                                            alert('File not found');
                                        }
                                    };

                                    request.send('content=' + content);
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    console.log(xhr.statusText);
                                }
                            })
                        }
                    }
                ],
                "info": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUser_IPLAS_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    var comp = [], opunit = [];
                    if ($('#selectFilterArea').val() != null) {
                        $selectFilterArea.val().forEach(function (opt) {
                            comp.push(opt.split('_')[0]);
                            opunit.push(opt.split('_')[1]);
                        })
                    }

                    aoData.push({ "name": "company", "value": comp });
                    aoData.push({ "name": "area", "value": opunit });
                    aoData.push({ "name": "gang", "value": $selectFilterGang.val() });
                    aoData.push({ "name": "divisi", "value": $selectFilterDivisi.val() });
                    aoData.push({ "name": "jabatan", "value": $selectFilterJabatan.val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tbluser").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console === "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $btnSearch.on('click', this.search);
                $selectFilterArea.on('change', this.onChangeArea);
                $selectFilterDivisi.on('change', this.onChangeDivisi);
                $selectFilterGang.on('change', this.onChangeGang);
            },
            search: function () {
                $table.DataTable().ajax.reload();
            },
            onChangeArea: function () {
                var opunit = '';
                if ($('#selectFilterArea').val() != null) {
                    $selectFilterArea.val().forEach(function (opt) {
                        if (opunit === '')
                            opunit += opt.split('_')[1];
                        else
                            opunit += ',' + opt.split('_')[1];
                    })
                }

                _setCustomFunctions.getMasterDivisi(opunit);
            },
            onChangeDivisi: function () {
                var opunit = '';
                var divisi = '';
                if ($('#selectFilterArea').val() != null) {
                    $selectFilterArea.val().forEach(function (opt) {
                        if (opunit === '')
                            opunit += opt.split('_')[1];
                        else
                            opunit += ',' + opt.split('_')[1];
                    })
                };
                if ($('#selectFilterDivisi').val() != null) {
                    $selectFilterDivisi.val().forEach(function (opt) {
                        if (divisi === '')
                            divisi += opt;
                        else
                            divisi += ',' + opt;
                    })
                }

                _setCustomFunctions.getMasterGang(opunit, divisi);
            },
            onChangeGang: function () {
                var opunit = '';
                var divisi = '';
                var gang = '';
                if ($('#selectFilterArea').val() != null) {
                    $selectFilterArea.val().forEach(function (opt) {
                        if (opunit === '')
                            opunit += opt.split('_')[1];
                        else
                            opunit += ',' + opt.split('_')[1];
                    })
                };
                
                if ($('#selectFilterDivisi').val() != null) {
                    $selectFilterDivisi.val().forEach(function (opt) {
                        if (divisi === '')
                            divisi += opt;
                        else
                            divisi += ',' + opt;
                    })
                }
                if ($('#selectFilterGang').val() != null) {
                    $selectFilterGang.val().forEach(function (opt) {
                        if (gang === '')
                            gang += opt;
                        else
                            gang += ',' + opt;
                    })
                }
                _setCustomFunctions.getMasterJabatan(opunit, divisi, gang);
            }
        },
        setCustomFunctions: {
            init: function () {
                this.getMasterArea();
                this.getMasterDivisi("");
                this.getMasterGang("", "");
                this.getMasterJabatan("", "", "");
            },
            getMasterArea: function () {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../../webservice/WebService_COR.asmx/GetMasterArea_IPLAS",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var json = $.parseJSON(data.d);

                        if (json.length > 0) {
                            $selectFilterArea.find('option').remove();
                            var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {
                                if (obj.Company !== optgroup) {
                                    optgroup = obj.Company
                                    group = $('<optgroup label="' + optgroup + '" />');
                                }

                                option = $('<option>', {
                                    value: optgroup + "_" + obj.Area,
                                    text: obj.Area
                                });

                                option.attr("data-tokens", optgroup + "," + obj.Area);
                                option.appendTo(group);
                                group.appendTo($selectFilterArea);
                            });

                            $selectFilterArea.selectpicker('refresh');
                        }
                    }
                });
            },
            getMasterDivisi: function (area) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../../webservice/WebService_COR.asmx/GetMasterDivisi_IPLAS",
                    data: "{area: '" + area + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var json = $.parseJSON(data.d);

                        $selectFilterDivisi.find('option').remove();
                        json.forEach(function (obj) {
                            $selectFilterDivisi.append($('<option>', {
                                value: obj.Divisi,
                                text: obj.Divisi
                            }));
                        })

                        $selectFilterDivisi.selectpicker('refresh');
                        $selectFilterDivisi.change();
                    }
                });
            },
            getMasterGang: function (area, divisi) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../../webservice/WebService_COR.asmx/GetMasterGang_IPLAS",
                    data: "{area: '" + area + "', divisi: '" + divisi + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var json = $.parseJSON(data.d);

                        $selectFilterGang.find('option').remove();
                        json.forEach(function (obj) {
                            $selectFilterGang.append($('<option>', {
                                value: obj.Gang,
                                text: obj.Gang
                            }));
                        })

                        $selectFilterGang.selectpicker('refresh');
                        $selectFilterGang.change();
                    }
                });
            },
            getMasterJabatan: function (area, divisi, gang) {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../../webservice/WebService_COR.asmx/GetMasterJabatan_IPLAS",
                    data: "{area: '" + area + "', divisi: '" + divisi + "', gang: '" + gang + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var json = $.parseJSON(data.d);

                        $selectFilterJabatan.find('option').remove();
                        json.forEach(function (obj) {
                            $selectFilterJabatan.append($('<option>', {
                                value: obj.Jabatan,
                                text: obj.Jabatan + ' (' + obj.CountPpl + ')'
                            }));
                        })

                        $selectFilterJabatan.selectpicker('refresh');
                    }
                });
            }
        }
    }

    Page.initialize();
})

function EventHandler_UserIPLAS() {
    $('#btnSearch').click(function () {
        $('#tbluser').DataTable().ajax.reload();
    })

    $('#selectFilterArea').change(function () {
        var opunit = '';
        $('#selectFilterArea').val().forEach(function (opt) {
            if (opunit === '')
                opunit += opt.split('_')[1];
            else
                opunit += ',' + opt.split('_')[1];
        })

        GetMasterDivisi_IPLAS(opunit);
    })

    $('#selectFilterDivisi').change(function () {
        var opunit = '';
        var divisi = '';
        $('#selectFilterArea').val().forEach(function (opt) {
            if (opunit === '')
                opunit += opt.split('_')[1];
            else
                opunit += ',' + opt.split('_')[1];
        })
        $('#selectFilterDivisi').val().forEach(function (opt) {
            if (divisi === '')
                divisi += opt;
            else
                divisi += ',' + opt;
        })
        
        GetMasterGang_IPLAS(opunit, divisi);
    })

    $('#selectFilterGang').change(function () {
        var opunit = '';
        var divisi = '';
        var gang = '';
        $('#selectFilterArea').val().forEach(function (opt) {
            if (opunit === '')
                opunit += opt.split('_')[1];
            else
                opunit += ',' + opt.split('_')[1];
        })
        $('#selectFilterDivisi').val().forEach(function (opt) {
            if (divisi === '')
                divisi += opt;
            else
                divisi += ',' + opt;
        })
        $('#selectFilterGang').val().forEach(function (opt) {
            if (gang === '')
                gang += opt;
            else
                gang += ',' + opt;
        })

        GetMasterJabatan_IPLAS(opunit, divisi, gang);
    })
}

function GetMasterArea_IPLAS() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../webservice/WebService_COR.asmx/GetMasterArea_IPLAS",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var json = $.parseJSON(data.d);

            if (json.length > 0) {
                $('#selectFilterArea').find('option').remove();
                var optgroup = "", group = "", option = "";

                json.forEach(function (obj) {
                    if (obj.Company !== optgroup) {
                        optgroup = obj.Company
                        group = $('<optgroup label="' + optgroup + '" />');
                    }

                    option = $('<option>', {
                        value: optgroup + "_" + obj.Area,
                        text: obj.Area
                    });

                    option.attr("data-tokens", optgroup + "," + obj.Area);
                    option.appendTo(group);
                    group.appendTo($('#selectFilterArea'));
                });

                $('#selectFilterArea').selectpicker('refresh');
            }
        }
    });
}

function GetMasterDivisi_IPLAS(area) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../webservice/WebService_COR.asmx/GetMasterDivisi_IPLAS",
        data: "{area: '" + area  + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var json = $.parseJSON(data.d);

            $('#selectFilterDivisi').find('option').remove();
            json.forEach(function (obj) {
                $("#selectFilterDivisi").append($('<option>', {
                    value: obj.Divisi,
                    text: obj.Divisi
                }));
            })

            $('#selectFilterDivisi').selectpicker('refresh');
            $('#selectFilterDivisi').change();
        }
    });
}

function GetMasterGang_IPLAS(area, divisi) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../webservice/WebService_COR.asmx/GetMasterGang_IPLAS",
        data: "{area: '" + area + "', divisi: '" + divisi + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var json = $.parseJSON(data.d);

            $('#selectFilterGang').find('option').remove();
            json.forEach(function (obj) {
                $("#selectFilterGang").append($('<option>', {
                    value: obj.Gang,
                    text: obj.Gang
                }));
            })

            $('#selectFilterGang').selectpicker('refresh');
            $('#selectFilterGang').change();
        }
    });
}

function GetMasterJabatan_IPLAS(area, divisi, gang) {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../webservice/WebService_COR.asmx/GetMasterJabatan_IPLAS",
        data: "{area: '" + area + "', divisi: '" + divisi+ "', gang: '" + gang + "'}",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var json = $.parseJSON(data.d);

            $('#selectFilterJabatan').find('option').remove();
            json.forEach(function (obj) {
                $("#selectFilterJabatan").append($('<option>', {
                    value: obj.Jabatan,
                    text: obj.Jabatan + ' (' + obj.CountPpl + ')'
                }));
            })

            $('#selectFilterJabatan').selectpicker('refresh');
        }
    });
}

function GetUser_IPLAS() {
    var content = '<table id="tbluser" class="table table-striped table-bordered" style="width:100%;font-size:9pt;">';
    content += '<thead><tr>';
    content += '<th style="text-align:center;">No.</th><th>Area</th><th>NIK</th><th>Nama</th><th>Gender</th><th>Gang</th><th>Divisi</th><th>Jabatan</th><th>Data Collector</th></tr>';
    content += '</thead><tbody></tbody></table > ';

    $("#divtbluser").html(content);

    var table = $('#tbluser').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
        pageLength: 25,
        "filter": true,
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "class": "center",
            "targets": 0
        }, {
            "class": "center",
            "targets": 4
        },{
            "class": "center",
            "targets": 8
        }

        ],
        "orderClasses": false,
        "order": [[1, "asc"]],
        buttons: [
            {
                extend: 'copy',
                text: 'Copy',
                className: 'btn btn-success',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                }
            },
            {
                text: 'PDF',
                className: 'btn btn-success',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                },
                action: function (e, dt, node, config) {
                    var $this = $(node);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

                    var opunit = [];
                    $('#selectFilterArea').val().forEach(function (opt) {
                        opunit.push(opt.split('_')[1]);
                    })

                    var divisi = $('#selectFilterDivisi').val() == null ? "" : $selectFilterDivisi.val();
                    var gang = $('#selectFilterGang').val() == null ? "" : $selectFilterGang.val();
                    var jabatan = $('#selectFilterJabatan').val() == null ? "" : $selectFilterJabatan.val();

                    $.ajax({
                        type: 'POST',
                        url: '../../webservice/WebService_COR.asmx/GenerateReportUser_IPLAS',
                        data: '{area: "' + opunit + '", divisi: "' + divisi + '", gang: "' + gang + '", jabatan: "' + jabatan + '", filetype: "' + "pdf" + '"}',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (response) {
                            $this.html("PDF");

                            var content = response.d;
                            var request = new XMLHttpRequest();
                            request.open('POST', '../../handlerservice/GetReportHandler.ashx?filename=' + response.d, true);
                            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                            request.responseType = 'blob';

                            request.onload = function () {
                                // Only handle status code 200
                                if (request.status === 200) {
                                    // Try to find out the filename from the content disposition `filename` value
                                    var disposition = request.getResponseHeader('content-disposition');
                                    var matches = /"([^"]*)"/.exec(disposition);
                                    var filename = (matches != null && matches[1] ? matches[1] : response.d);

                                    // The actual download
                                    var blob = new Blob([request.response], { type: 'application/pdf' });
                                    var link = document.createElement('a');
                                    link.href = window.URL.createObjectURL(blob);
                                    link.download = filename;

                                    document.body.appendChild(link);

                                    link.click();

                                    document.body.removeChild(link);
                                }
                                else {
                                    alert('File not found');
                                }
                            };

                            request.send('content=' + content);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.statusText);
                        }
                    })
                }
            },
            {
                text: 'Excel',
                className: 'btn btn-success',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                },
                action: function (e, dt, node, config) {
                    var $this = $(node);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

                    var opunit = [];
                    $('#selectFilterArea').val().forEach(function (opt) {
                        opunit.push(opt.split('_')[1]);
                    })

                    var divisi = $('#selectFilterDivisi').val() == null ? "" : $selectFilterDivisi.val();
                    var gang = $('#selectFilterGang').val() == null ? "" : $selectFilterGang.val();
                    var jabatan = $('#selectFilterJabatan').val() == null ? "" : $selectFilterJabatan.val();
                    $.ajax({
                        type: 'POST',
                        url: '../../webservice/WebService_COR.asmx/GenerateReportUser_IPLAS',
                        data: '{area: "' + opunit + '", divisi: "' + divisi + '", gang: "' + gang + '", jabatan: "' + jabatan + '", filetype: "' + "excel" + '"}',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (response) {
                            $this.html("Excel");

                            var content = response.d;
                            var request = new XMLHttpRequest();
                            request.open('POST', '../../handlerservice/GetReportHandler.ashx?filename=' + response.d, true);
                            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                            request.responseType = 'blob';

                            request.onload = function () {
                                // Only handle status code 200
                                if (request.status === 200) {
                                    // Try to find out the filename from the content disposition `filename` value
                                    var disposition = request.getResponseHeader('content-disposition');
                                    var matches = /"([^"]*)"/.exec(disposition);
                                    var filename = (matches != null && matches[1] ? matches[1] : response.d);

                                    // The actual download
                                    var blob = new Blob([request.response], { type: 'application/pdf' });
                                    var link = document.createElement('a');
                                    link.href = window.URL.createObjectURL(blob);
                                    link.download = filename;

                                    document.body.appendChild(link);

                                    link.click();

                                    document.body.removeChild(link);
                                }
                                else {
                                    alert('File not found');
                                }
                            };

                            request.send('content=' + content);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log(xhr.statusText);
                        }
                    })
                }
            }
            //,
            //{
            //    text: 'PDF',
            //    className: 'btn btn-success',
            //    init: function (api, node, config) {
            //        $(node).removeClass('dt-button')
            //    },
            //    action: function (e, dt, node, config) {
            //    }
            //}
        ],
        "info": true,
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUser_IPLAS_ServerSideProcessing",
        "fnServerData": function (sSource, aoData, fnCallback) {
            var comp = [], opunit = [];
            $('#selectFilterArea').val().forEach(function (opt) {
                comp.push(opt.split('_')[0]);
                opunit.push(opt.split('_')[1]);
            })

            aoData.push({ "name": "company", "value": comp });
            aoData.push({ "name": "area", "value": opunit });
            aoData.push({ "name": "gang", "value": $('#selectFilterGang').val() });
            aoData.push({ "name": "divisi", "value": $('#selectFilterDivisi').val() });
            aoData.push({ "name": "jabatan", "value": $('#selectFilterJabatan').val() });
            $.ajax({
                "dataType": 'json',
                "contentType": "application/json; charset=utf-8",
                "type": "GET",
                "url": sSource,
                "data": aoData,
                "success": function (msg) {
                    var json = jQuery.parseJSON(msg.d);
                    fnCallback(json);
                    $("#tbluser").show();
                },
                error: function (xhr, textStatus, error) {
                    if (typeof console === "object") {
                        console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                    }
                }
            });
        },
        fnDrawCallback: function () {
        },
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            var page = info.page;
            var length = info.length;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
        }
    });
}