﻿$(function () {
    var totalRow;
    var _dataSelect = [];
    var lengthbfr = 0;
    var objEmail = [];
    var arrayKebun = [];
    var actionTableUpdate = [];
    var actionTableUnregister = [];
    var arrayKebun = [];
    var arrayApplikasi = [];
    var _aplikasi = [];
    var _existing = [];
    var _arrayID = [];
    var _arrayExpired = [];
    var _app;
    var _paramOnchange = 0;
    var NIK;
    var userID;
    var Page = {
        options: {
            //formAdd: '#formAdd',
            addButton: '#btnSubModule',
            //saveButton: '#btnSave',
            //UpdateButton: '#btnUpdate',
            modalUpdate: '#mdlUpdate',
            table: '#tblUser',
            tableEmail: '#tblemail',
            SelectMultiEstateAccess: '#SelectMultiEstateAccess',
            SelectMultiApplicationAccess: '#SelectMultiApplicationAccess',

        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            //$formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            //$saveButton = $(this.options.saveButton);
            //$UpdateButton = $(this.options.UpdateButton);
            $modalUpdate = $(this.options.modalUpdate);
            //$tabCountry = $(this.options.tabCountry);
            $table = $(this.options.table);
            $tableEmail = $(this.options.tableEmail);
            $SelectMultiEstateAccess = $(this.options.SelectMultiEstateAccess);
            $SelectMultiApplicationAccess = $(this.options.SelectMultiApplicationAccess);
            //_formAdd = this.options.formAdd;
            //_tabCountry = this.options.tabCountry;
            //_listCountry = [];
            //_listCountryCode = [];
            //_isEditTabExists = false;
            //_currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'lfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "details-control",
                    "data": null,
                    "defaultContent": '',
                    "targets": 1,
                    width: 5
                }, {
                    "searchable": true,
                    "orderable": true,
                    "class": "left",
                    "targets": 2,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 3
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 5
                }],
                "orderClasses": false,
                "order": [[0, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserParent_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            totalRow = json.recordsTotal;
                            fnCallback(json);
                            $table.show();
                            $(".details-control").on('click', _setEvents.detailmodule);
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                select: {
                    style: 'multi',
                    selector: 'td:first-child'
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });




        },
        setEvents: {
            init: function () {
                $SelectMultiEstateAccess.on('change', this.GetDatamdlAccCode);
                $SelectMultiApplicationAccess.on('change', this.onChangeMultiApplication);

            },
            saveSubmdlCode: function (UserID) {
                //Set Ajax Submit Here
                var table = document.getElementById("tblemail");
                var row;
                var _array = [];
                var _subModule = [];
                for (var i = 1, row = table.rows[i]; i < table.rows.length ; i++) {
                    //console.log()
                    var $row = $(table).closest('tr');
                    var _a = i;
                    var Package = document.getElementById('tblemail').rows[i].cells[1].innerHTML;
                    var NIK = document.getElementById('tblemail').rows[i].cells[0].innerHTML;
                    var PackageName = Package.innerHTML;
                    if ($("#SelectAppsName" + _a).val() != undefined) {
                        _subModule.push({ NamaPackage: Package, SubModule: $("#SelectAppsName" + _a).val() });
                    }
                    else {
                        _subModule.push({ NamaPackage: Package });
                    }

                }
                var _kebun = [];
                _kebun = $SelectMultiEstateAccess.val();
                _aplikasi = $SelectMultiApplicationAccess.val();

                //_array.push({ NIK: NIK, UserID: UserID, arrayAplikasi: _aplikasi, subModule: _subModule, arrayKebun: _kebun });
                var data = { NIK: NIK, UserID: UserID, arrayAplikasi: _aplikasi, subModule: _subModule, arrayKebun: _kebun };
                console.log(_array)
                //var savetype = 
                _setCustomFunctions.saveSubModule(data);
            },
            detailmodule: function () {
                var tr = $(this).closest('tr');
                var row = $table.DataTable().row(tr);
                var UserID = tr.find('.UserID').html();

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    $.ajax({
                        url: '../../webservice/WebService_COR.asmx/GetMasterID_ServerSideProcessing',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: "POST",
                        data: JSON.stringify({ UserID: UserID }),
                        success: function (response) {
                            var json = $.parseJSON(response.d);
                            // iterate over each element in the array
                            _arrayID = [];
                            for (var i = 0; i < json.length; i++) {
                                // look for the entry with a matching `code` value
                                _arrayID.push(json[i].UserFullName)
                                _arrayExpired.push({name: json[i].UserFullName, expiredDate: json[i].EXPIRATION_DATE});
                            }
                            
                            _setCustomFunctions.format(row.child, UserID)
                            tr.addClass('shown');
                        }
                    });

                }
            },
            onChangeMultiApplication: function () {
                _existing = [];
                var _existingcode = [];
                $SelectMultiApplicationAccess.val()
                var tables = $('#tblemail').DataTable();
                //var table = $('#tblassignUsers').DataTable();
                //tables.destroy();
                //var tableHtml = "<thead style='font-size:10px'><tr>"
                //    + '<th align="left" hidden>NIK</th>'
                //    + '<th align="left">Module Access</th>'
                //    + '<th align="left">Sub Module</th>'
                //    + '</thead>';
                var rowCount = $('#tblemail tr').length;
                for (var i = 1; i < rowCount; i++) {
                    var submdlDesc = $('#tblemail')[0].rows[i].cells[1].innerHTML;
                    var submdlCode = $('#tblemail')[0].rows[i].cells[1].id;
                    _existing.push(submdlDesc);
                    _existingcode.push(submdlCode);
                }
                var _count = rowCount - 1;
                if (_count < $SelectMultiApplicationAccess.val().length) {
                    for (var i = 0; i < $SelectMultiApplicationAccess.val().length ; i++) {

                        $.ajax({
                            type: 'POST',
                            url: '../../webservice/WebService_COR.asmx/GetDataSubModule',
                            contentType: 'application/json; charset=utf-8',
                            data: "{'packageName' :'" + $SelectMultiApplicationAccess.val()[i] + "'}",
                            dataType: 'json',
                            success: function (response) {
                                var _a = i;
                                var _selected = "#SelectAppsName" + _a;
                                var json = $.parseJSON(response.d);
                                var _tempdata = "";
                                var _rowHTML;
                                for (var x = 0; x < _existing.length; x++) {
                                    if ((!_existing.includes(json[0].AppsName) || _tempdata != "") && _tempdata != json[0].AppsName) {
                                        if (json[0].hasSubModuleAccess == 'TRUE') {
                                            _setCustomFunctions.GetSubModule(json[0].PackageName, i, NIK, userID);
                                            _rowHTML = "<tr id='" + json[0].PackageName + "s" + "'><td align='center' id='NIK' hidden>'" + NIK + "'</td><td align='center' id='" + json[0].PackageName + "'>" + json[0].AppsName + "</td><td align='center'><select class='form-control' id='SelectAppsName" + _a + "' name='appsname'></select></td></tr>"
                                            //tables.append();


                                            tables.row.add($(_rowHTML)).draw();
                                        }
                                        else {
                                            //tables.append("");
                                            _rowHTML = "<tr id='" + json[0].PackageName + "s" + "'><td align='center' id='NIK' hidden>'" + NIK + "'</td><td align='center' id='" + json[0].PackageName + "'>" + json[0].AppsName + "</td><td align='center'></td></tr>"
                                            tables.row.add($(_rowHTML)).draw();
                                        }
                                        _tempdata = json[0].AppsName;
                                    }
                                    _paramOnchange = 1;
                                }

                            }
                        });
                    }
                }
                else if (_count > $SelectMultiApplicationAccess.val().length) {
                    //_existing
                    //$SelectMultiApplicationAccess.val()
                    var _removeSubmdl = [];
                    var a = [], diff = [];

                    for (var i = 0; i < _existingcode.length; i++) {
                        a[_existingcode[i]] = true;
                    }

                    for (var i = 0; i < $SelectMultiApplicationAccess.val().length; i++) {
                        if (a[$SelectMultiApplicationAccess.val()[i]]) {
                            delete a[$SelectMultiApplicationAccess.val()[i]];
                        } else {
                            a[$SelectMultiApplicationAccess.val()[i]] = true;
                        }
                    }

                    for (var k in a) {
                        diff.push(k);
                    }

                    var rowid = '#' + diff[0] + "s";
                    tables.row(rowid).remove().draw(false);

                }
            },

        },
        setCustomFunctions: {
            init: function () {

                //this.getDeviceUser();
                //this.getMasterModuleGroup();
                //this.getMasterModuleType();
            },
            setDataApplication: function (data) {
                var _dataemailmodule = [];
                $.ajax({
                    type: "POST",
                    url: '../../webservice/WebService_COR.asmx/GetUserApplicationAccess',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $SelectMultiApplicationAccess.find('option').remove();
                        if (json.length != 0) {
                            json.forEach(function (obj) {
                                //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                                $SelectMultiApplicationAccess.append($('<option>', {
                                    value: obj.PackageName,
                                    text: obj.AppsName
                                }));
                                //var jsons = $.parseJSON(data.arrayApplikasi);
                                //jsons.forEach(function (objs) {
                                _app = data.arrayAplikasi.split(',');
                                //});
                                $SelectMultiApplicationAccess.selectpicker('val', _app);
                                $('.selectpicker2').selectpicker('refresh');
                            })
                        }
                        else {
                            $SelectMultiApplicationAccess.append($('<option>', {
                                value: "",
                                text: "-- No Data --"
                            }));
                        }

                    }
                });
            },
            setDataEstate: function (data) {
                var _dataemailmodule = [];
                $.ajax({
                    type: "POST",
                    url: '../../webservice/WebService_COR.asmx/GetUserEstateAccess',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $SelectMultiEstateAccess.find('option').remove();
                        json.forEach(function (obj) {
                            //$("#selectGroup").append(new Option(obj.mdlGroupCode, obj.mdlGroupDesc));
                            $SelectMultiEstateAccess.append($('<option>', {
                                value: obj.estCode,
                                text: obj.estName
                            }));

                            //var jsons = $.parseJSON(data.arrayApplikasi);
                            //jsons.forEach(function (objs) {
                            var _kebun = data.arrayKebun.split(',');
                            //});
                            $SelectMultiEstateAccess.selectpicker('val', _kebun);
                            $('.selectpicker1').selectpicker('refresh');
                        })
                    }
                });
            },
            setTableSubModule: function (datas) {
                $.ajax({
                    "dataType": 'json',
                    "contentType": "application/json; charset=utf-8",
                    "type": "POST",
                    "url": "../../webservice/WebService_COR.asmx/GetSubModule",
                    data: JSON.stringify({ dataobject: JSON.stringify(datas) }),
                    "success": function (msg) {
                        //console.info("msg", msg.d);
                        allUser = JSON.parse(msg.d);
                        //for (var i = 0 ; i < allUser.length; i++) {
                        //    if (allUser[i]["checked"] == "true") {
                        //        $(i).addClass('selected');
                        //        //$(nRow).addClass('selected');
                        //        _dataSelect.push(i);
                        //    }
                        //}
                        //popUpAssignUser();
                        _setCustomFunctions.DetailTable(datas.UserID, datas.NIK);


                        //$tableAssignUsers.DataTable().ajax.reload();
                    },
                    error: function (xhr, textStatus, error) {
                        alert("error");
                    }
                });
            },
            GetSubModule: function (packageName, i, NIK, UserID) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterSubModule',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'packageName' :'" + packageName + "'}",
                    dataType: 'json',
                    success: function (response) {
                        if (_paramOnchange == 0) {
                            var _a = i + 1;
                        }
                        else {
                            var _a = i;
                        }

                        var _selected = "#SelectAppsName" + _a;
                        var json = $.parseJSON(response.d);
                        $(_selected).find('option').remove();
                        json.forEach(function (obj) {
                            $(_selected).append($('<option>', {
                                value: obj.subMdlAccCode,
                                text: obj.subMdlName
                            }));
                        })
                        if (_paramOnchange != 1) {
                            $.ajax({
                                type: 'POST',
                                url: '../../webservice/WebService_COR.asmx/GetMasterSubModuleUser',
                                contentType: 'application/json; charset=utf-8',
                                data: "{'NIK' :'" + NIK + "','UserID' :'" + UserID + "'}",
                                dataType: 'json',
                                success: function (response) {
                                    var _a = i + 1;
                                    var _selected = "";
                                    var jsonData = $.parseJSON(response.d);
                                    $("#SelectAppsName" + _a).val("");
                                    jsonData.forEach(function (obj) {
                                        if ($("#SelectAppsName" + _a).val() != obj.StringValue && $("#SelectAppsName" + _a).val() != _selected) {
                                            $("#SelectAppsName" + _a).val(obj.StringValue)

                                            _selected = obj.StringValue;
                                        }
                                    });
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                }
                            });
                        }
                        _paramOnchange = 0;
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DetailTable: function (UserID, NIK) {
                //var table = $('#tblassignUsers').DataTable();
                //table.destroy();
                var tableHtml = "<thead style='font-size:10px'><tr>"
                    + '<th align="left" hidden>NIK</th>'
                    + '<th align="left">Module Access</th>'
                    + '<th align="left">Sub Module</th>'
                    + '</thead>';
                for (var i = 0; i < allUser.length; i++) {
                    tableHtml += "<tr id='" + allUser[i]["PackageName"] + "s" + "'>"
                        + "<td align='center' id='NIK' hidden>" + NIK + "</td>"
                        + "<td align='center' id='" + allUser[i]["PackageName"] + "'>" + allUser[i]["AppsName"] + "</td>"
                    //+ "<td align='center'><select class='form-control' id='SelectAppsName" + i + "' name='appsname'></select></td>";
                    //+ "</tr>";
                    if (allUser[i]["hasSubModuleAccess"] == "TRUE") {
                        var _a = i + 1;
                        tableHtml += "<td align='center'><select class='form-control' id='SelectAppsName" + _a + "' name='appsname'></select></td>";
                        //+ "</tr>";
                        this.GetSubModule(allUser[i]["PackageName"], i, NIK, UserID);
                    }
                    else {

                        tableHtml += "<td align='center'></td>";
                        //+ "</tr>";

                    }
                    tableHtml += "</tr>";

                }
                var btnHtml = '<button id="btnSubModule" onclick="_setEvents.saveSubmdlCode(' + UserID + ')" type="button" class="btn btn-success pull-right" style="margin-right: 20px;"><span class="glyphicon glyphicon-plus"></span>Save Sub Module</button><br>';

                $('#divBtnSave').html(btnHtml);
                $('#tblemail').html(tableHtml);
                tableAssignUsers = $('#tblemail').DataTable({
                    dom: 'lfrtip',
                    pageLength: 10,
                    destroy: true,
                    columnDefs: [{
                        orderable: false,
                        targets: 0
                    }],
                    select: {
                        style: 'multi',
                        selector: 'td:first-child',
                        rows: 'true'
                    },
                    order: [[1, 'asc']]
                });
                //$addButton.on('click', _setEvents.saveSubmdlCode(NIK));




            },
            format: function (callback, UserID) {
                var contentHTML;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetSubUser_ServerSideProcessing',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: JSON.stringify({ data: UserID }),
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        var dataChild = [];
                        // iterate over each element in the array
                        for (var i = 0; i < json.length; i++) {
                            // look for the entry with a matching `code` value
                            var jsonTemp = $.parseJSON(json[i].accessJSON);
                            dataChild.push(jsonTemp.Nama);
                        }
                        //console.log(_arrayID);
                        //console.log(dataChild);
                        
                        //console.log(json[0].accessJSON);
                        //console.log(jsonData);
                        var thead = '', tbody = '', tbutton = '';
                        actionTableExtendToken = [];
                        var idZ = "add_" + UserID;
                        var _no;
                        var _length;
                        tbutton += '<button id=' + idZ + ' type="button" class="btn btn-success pull-right" ><span class="glyphicon glyphicon-plus"></span>&nbsp;Extend Token</button><br>';
                        actionTableExtendToken.push(idZ);
                        thead += "<thead>"
                        thead += '<th style="text-align: center;">No</th>';
                        thead += '<th style="text-align: center;">NIK</th><th style="text-align: center;">Nama</th><th style="text-align: center;">Estate</th>';
                        thead += '<th style="text-align: center;">Afdeling</th><th style="text-align: center;">Gang</th><th style="text-align: center;">AndroidID</th>';
                        thead += '<th style="text-align: center;">ExpiredDate</th>';
                        thead += '<th style="text-align: center;display:none">Array Estate</th>';
                        thead += '<th style="text-align: center;display:none">Array Aplikasi</th>';
                        thead += '<th style="text-align: center;">Edit</th>';
                        thead += '<th style="text-align: center;"></th>';
                        thead += '<th style="text-align: center;">Unregister</th>';
                        thead += "</thead>"
                        //var ReqEstAcc = "";
                        //var RequestEst = [];
                        //for (var i = 0; i < dataChild.length; i++) {
                        //    for (var x = 0; x < _arrayID.length; x++) {
                        //        if ($.inArray(_arrayID[x], dataChild) != -1) {
                        //            if ($.inArray(_arrayExpired[x], RequestEst) == -1) {
                        //                ReqEstAcc = _arrayExpired[x].expiredDate;
                        //                RequestEst.push({ name: _arrayExpired[x].name, expiredDate: _arrayExpired[x].expiredDate });
                        //            }
                        //        }
                        //    }
                        //}
                        //console.log(RequestEst);
                        if (json.length != 0) {
                            actionTableUnregister = [];
                            actionTableUpdate = [];
                            arrayKebun = [];
                            arrayApplikasi = [];
                            _no = 1;
                            _length = json.length;
                            for (var i = 0 ; i < json.length; i++) {
                                var jsonData = $.parseJSON(json[i].accessJSON);
                                //arrayKebun = jsonData.ArrayKebun
                                //arrayAplikasi = jsonData.ArrayAplikasi
                                var idX = "edit_" + jsonData.NIK;
                                NIK = jsonData.NIK;
                                var Estate = jsonData.Estate;
                                var idY = "editData_" + UserID + _no;
                                tbody += '<tr><td>' + _no + '</td><td class="NIK" style="text-align: center;">' + jsonData.NIK + '</td><td style="text-align: center;">' + jsonData.Nama + '</td><td class="Estate" style="text-align: center;">' + jsonData.Estate + '</td>';
                                tbody += '<td class="Afdeling" style="text-align: center;">' + jsonData.Afdeling + '</td><td class="Gang" style="text-align: center;">' + jsonData.Gang + '</td><td class="AndroidID" style="text-align: center;">' + json[i].android_id_user + '</td>';
                                
                                tbody += '<td style="text-align: center;">' + (_arrayID.includes(jsonData.Nama) ? _arrayExpired.find(x => x.name == jsonData.Nama).expiredDate : "n/a") + '</td>';
                                
                                tbody += '<td><a id=' + idY + ' type="button" class="btn btn-primary btn-xs editDetail" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></a></td>';

                                tbody += '<td style="text-align: center;display:none">' + jsonData.ArrayKebun + '</td>';
                                tbody += '<td style="text-align: center;display:none">' + jsonData.ArrayAplikasi + '</td>';
                                tbody += '<td id=' + jsonData.UserID + '></td>';

                                tbody += '<td><a id=' + idX + ' type="button" class="btn btn-danger btn-xs editDetail" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-trash"></span></a></td></tr>';
                                arrayKebun.push(jsonData.ArrayKebun);
                                arrayApplikasi.push(jsonData.ArrayAplikasi);
                                actionTableUnregister.push(idX);
                                actionTableUpdate.push(idY);
                                _no++;
                            }
                            //$.each(jsonData, function (i, d) {
                            //    var idX = "edit_" + UserID + _no;
                            //    tbody += '<tr><td>' + _no + '</td><td class="NIK" style="text-align: center;">' + jsonData[0].NIK + '</td><td style="text-align: center;">' + d.Nama + '</td><td class="Estate" style="text-align: center;">' + d.Estate + '</td><td class="Afdeling" style="text-align: center;">' + d.Afdeling + '</td><td class="Gang" style="text-align: center;">' + d.Gang + '</td>'
                            //    tbody += '<td style="display:none">' + d.UserID + '</td><td class="AndroidID" style="text-align: center;">' + d.AndroidID + '</td><td style="text-align: center;">' + d.ArrayKebun + '</td><td class="ArrayAplikasi" style="text-align: center;">' + d.ArrayAplikasi + '</td>'

                            //    tbody += '<td><a id=' + idX + ' type="button" class="btn btn-danger btn-xs editDetail" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-trash"></span></a></td></tr>'

                            //    actionTableUnregister.push(idX);
                            //    _no++;
                            //});
                        }
                        else {
                            //thead += '<th>No</th><th>NIK</th><th>Nama</th><th>Estate</th><th>Afdeling</th><th>Gang</th><th>AndroidID</th><th>ArrayKebun</th><th>ArrayAplikasi</th>';
                            tbody += '<tr><td colspan="10" style="text-align: center;">No Data Present</td>';
                        }

                        callback($(tbutton + '<table id="tblDetailDevice_' + UserID + '" class="table table-striped table-bordered" style="width:100%;font-size:9pt;">' + thead + tbody + '</table>')).show();

                        var _cok = 0;
                        var _token = [];
                        var _nik = [];
                        var _estate = [];
                        var _num;
                        for (var i = 0 ; i < actionTableExtendToken.length; i++) {
                            $("#" + actionTableExtendToken[i]).click(function () {
                                _token = [];
                                for (var j = 0; j < _length; j++) {
                                    //_setCustomFunctions.showConfirmPopup('aa', 'Info', 'Are You Sure To Unregistered ?');
                                   
                                    if ($($("#tblDetailDevice_" + UserID + " tr").parent()[1].childNodes).find(".dt-checkboxes")[j].checked == true) {
                                        if ($("#tblDetailDevice_" + UserID + " tr").parent().siblings().find(".AndroidID")[j].innerHTML != "-")
                                        {
                                            _token.push($("#tblDetailDevice_" + UserID + " tr").parent().siblings().find(".AndroidID")[j].innerHTML);
                                            _nik.push($("#tblDetailDevice_" + UserID + " tr").parent().siblings().find(".NIK")[j].innerHTML);
                                            _estate.push($("#tblDetailDevice_" + UserID + " tr").parent().siblings().find(".Estate")[j].innerHTML);
                                        }
                                        else {
                                            _cok = 1;
                                        }
                                    }
                                }
                                var uniqueNamesNIK = [];
                                var uniqueNamesEstate = [];
                                $.each(_nik, function (i, el) {
                                    if ($.inArray(el, uniqueNamesNIK) === -1) uniqueNamesNIK.push(el);
                                });
                                $.each(_estate, function (i, el) {
                                    if ($.inArray(el, uniqueNamesEstate) === -1) uniqueNamesEstate.push(el);
                                });

                                //var data = { NIK: NIK, UserID: UserID, arrayAplikasi: _aplikasi, subModule: _subModule, arrayKebun: _kebun };
                                var datas = { UserID: UserID, ListAndroidID: _token, ListNIK: uniqueNamesNIK, ListEstate: uniqueNamesEstate };
                                if (_token.length != 0)
                                {
                                    _setCustomFunctions.showConfirmPopup(1, datas, 'Info', 'Are you sure to extend token ?');
                                }
                                else
                                {
                                    if (_cok == 1)
                                    {
                                        _setCustomFunctions.showPopup('Info', 'This user has unregistered Device');
                                    }
                                    else
                                    {
                                        _setCustomFunctions.showPopup('Info', 'Please choose user first');
                                    }
                                    _cok = 0;
                                    
                                }
                                

                            });
                        }

                        for (var i = 0 ; i < actionTableUnregister.length; i++) {
                            $("#" + actionTableUnregister[i]).click(function () {
                                //NIK = $(this).parent().siblings().eq(1).text();
                                var datas = { UserID: UserID, NIK: NIK };
                                _setCustomFunctions.showConfirmPopup(2, datas, 'Info', 'Are you sure to unregistered ?');
                            });
                        }

                        for (var i = 0 ; i < actionTableUpdate.length; i++) {
                            $("#" + actionTableUpdate[i]).click(function () {
                                NIK = $(this).parent().siblings().eq(1).text();
                                var arrayKeb = $(this).parent().siblings().eq(8).text();
                                var arrayApp = $(this).parent().siblings().eq(9).text();
                                userID = UserID;
                                //arrayKebun = arrayKebun.filter(function (elem, index, self) {
                                //    return index === self.indexOf(elem);

                                //});
                                //arrayApplikasi = arrayApplikasi.filter(function (elem, index, self) {
                                //    return index === self.indexOf(elem);

                                //});
                                var datas = { UserID: UserID, NIK: NIK, arrayKebun: arrayKeb, arrayAplikasi: arrayApp };
                                _setCustomFunctions.setDataApplication(datas);
                                _setCustomFunctions.setDataEstate(datas);
                                _setCustomFunctions.setTableSubModule(datas);
                                $modalUpdate.modal('toggle');
                                //_setCustomFunctions.showConfirmPopup(2, datas, 'Info', 'Are You Sure To Unregistered ?');
                            });
                        }
                        if (_length != undefined) {
                            $("#tblDetailDevice_" + UserID).DataTable({
                                dom: 'lfrtip',
                                destroy: true,
                                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                                pageLength: 60,
                                "filter": true,
                                "columnDefs": [{
                                    "searchable": false,
                                    "orderable": false,
                                    "class": "center",
                                    "targets": 0,
                                    width: 5
                                }, {
                                    "searchable": true,
                                    "orderable": true,
                                    "targets": 1,
                                }, {
                                    "searchable": true,
                                    "orderable": true,
                                    "class": "left",
                                    "targets": 2
                                }, {
                                    "searchable": true,
                                    "orderable": true,
                                    "class": "center",
                                    "targets": 5
                                }, {
                                    "searchable": false,
                                    "orderable": false,
                                    "class": "center",
                                    "targets": 7
                                }, {
                                    "searchable": false,
                                    "orderable": false,
                                    "class": "center",
                                    "targets": 11,
                                    'checkboxes': {
                                        'selectRow': true
                                    }
                                }, {
                                    "searchable": false,
                                    "orderable": false,
                                    "class": "center",
                                    "targets": 12,
                                    width: 5
                                }, ],
                                "orderClasses": false,
                                "order": [[0, "asc"]],
                                "info": true
                            });
                        }

                    }
                });

            },
            UnregisterChild: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/UnregisterChild',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Device has been Unregistered');
                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            ExtendToken: function (data) {
                var _userID = data.UserID;
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/ExtendTokenChild',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            //$('#tblDetailDevice_' + _userID).DataTable().ajax.reload();
                            //var tr = $(this).closest('tr');
                            //var row = $table.DataTable().row(tr);
                            //var table = document.getElementById("#tblDetailDevice_" + _userID);
                            ////or use :  var table = document.all.tableid;
                            //for (var i = table.rows.length - 1; i > 0; i--) {
                            //    table.deleteRow(i);
                            //}
                            //_setCustomFunctions.format(row.child, UserID)
                            //_setCustomFunctions.showPopup('Info', 'Module Access has been Delete successfully');
                            history.go();
                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveSubModule: function (data) {
                var _userID = data.UserID;
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/saveSubModule',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            //$('#tblDetailDevice_' + _userID).DataTable().ajax.reload();
                            //var tr = $(this).closest('tr');
                            //var row = $table.DataTable().row(tr);
                            //var table = document.getElementById("#tblDetailDevice_" + _userID);
                            ////or use :  var table = document.all.tableid;
                            //for (var i = table.rows.length - 1; i > 0; i--) {
                            //    table.deleteRow(i);
                            //}
                            //_setCustomFunctions.format(row.child, UserID)
                            //_setCustomFunctions.showPopup('Info', 'Module Access has been Delete successfully');
                            history.go();
                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                                //$table.DataTable().ajax.reload();
                            }
                        }
                    }
                });
            },
            showConfirmPopup: function (param, data, title, content) {
                if (title == "Info") {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    if (param == 1) {
                                        _setCustomFunctions.ExtendToken(data);
                                    }
                                    else if (param == 2) {
                                        _setCustomFunctions.UnregisterChild(data);
                                    }

                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            OK: {
                                text: 'OK',
                                btnClass: 'btn-blue',
                                action: function () {
                                    $(_formAdd + " :input").prop("disabled", false);
                                    history.go(0); //forward
                                    //_setCustomFunctions.getModule();
                                    //$modalAdd.modal('hide');
                                }
                            }
                        }
                    });

                }

            }
        }

    }

    Page.initialize();
});