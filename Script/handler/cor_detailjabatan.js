﻿$(function () {
    var _globalArea;
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddDetailJabatan',
            saveButton: '#btnSave',
            UpdateButton: '#btnUpdate',
            modalAdd: '#mdlAdd',
            tabDetailJabatan: '#tabDetailJabatan',
            tableDetailJabatan: '#tbldetailjabatan',
            selectDetailJabatan: '#SelectDetailJabatan',
        },
        initialize: function () {
            this.setVars();
            this.setTableDetailJabatan();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $UpdateButton = $(this.options.UpdateButton);
            $modalAdd = $(this.options.modalAdd);
            $selectDetailJabatan = $(this.options.selectDetailJabatan);
            //$tabDetailJabatan = $(this.options.tabDetailJabatan);
            $tableDetailJabatan = $(this.options.tableDetailJabatan);
            _formAdd = this.options.formAdd;
            //_tabDetailJabatan = this.options.tabDetailJabatan;
            _listDetailJabatan = [];
            _listDetailJabatanCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTableDetailJabatan: function () {
            //Initialization of main table or data here
            var tableDetailJabatan = $tableDetailJabatan.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 6,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 7,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetDetailJabatan_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tbldetailjabatan").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $tableDetailJabatan.on('click', 'a[data-target="#edit"]', this.edit);
                $tableDetailJabatan.on('click', 'a[data-target="#delete"]', this.delete);


                //$tabDetailJabatan.on('click', 'a', this.tabclick);
                //this.formvalidation();

                //select event
                $('#SelectMainJabatan').on('change', this.selectchange);
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 1,
                            required: true,
                            remote: function () {
                                //document.getElementById('errormsg').style.display = 'none';
                                return {
                                    url: "../../webservice/WebService_COR.asmx/GetMasterDetailJabatanbyDetailJabatanCode",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: "{'DetailJabatanCode' :'" + $('input[name=code]').val() + "'}",
                                    dataFilter: function (data) {
                                        var msg = JSON.parse(data);
                                        var json = $.parseJSON(msg.d);
                                        if (json.length > 0) {
                                            //{
                                            document.getElementById('lblbucode').style.color = '#dd4b39';
                                            document.getElementById('description').style.borderColor = '#dd4b39';
                                            //document.getElementById('help-block').style.display = 'block';
                                            document.getElementById('errormsg').style.display = 'block';
                                            document.all('errormsg').innerHTML = "Code sudah terdaftar pada sistem";
                                        }
                                    }
                                }


                            },
                        },
                        name: {
                            minlength: 1,
                            required: true
                        },
                        shortname: {
                            minlength: 1,
                            required: true
                        },
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                //e.preventDefault();

            },
            tabchange: function (tabId) {


            },
            selectchange: function () {
                _setCustomFunctions.OnchangeMasterArea();
            },
            add: function () {
                console.log("ada");
                //Event for adding data
                $modalAdd.modal('toggle');
                //document.getElementById('lblbucode').style.color = '#333';
                //document.getElementById('bucode').style.borderColor = '#d2d6de';
                //document.getElementById('maincode').style.borderColor = '#d2d6de';
                //document.getElementById('detailcode').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                //document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Detail Jabatan";


                $('#SelectBU').val('');
                //$('#Selectstatus').val(DetailJabatanStatus).attr("selected", "selected");
                $('#SelectMainJabatan').val('');
                $('#SelectDetailJabatan').val('');
                //_setEvents.formvalidation();
            },
            edit: function () {
                //_setCustomFunctions.OnchangeMasterArea();
                //document.getElementById('lblbucode').style.color = '#333';
                //document.getElementById('bucode').style.borderColor = '#d2d6de';
                //document.getElementById('maincode').style.borderColor = '#d2d6de';
                //document.getElementById('detailcode').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                //document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'inline';
                document.getElementById('btnSave').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Detail Jabatan";
                $("#bucode").prop("disabled", true);

                $modalAdd.modal('toggle');
                _setEvents.formvalidation();

                var $this = $(this);
                var $row = $(this).closest('tr');
                var Id = $row.find('.Id').html();
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetMasterDetailJabatan',
                    type: "POST",
                    data: "{'Id' :'" + Id + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('#SelectBU').val(obj.bucode);
                            //$('#Selectstatus').val(DetailJabatanStatus).attr("selected", "selected");
                            $('#SelectMainJabatan').val(obj.maincode);
                            $('#SelectDetailJabatan').val(obj.area);
                            $('#SelectUser').val(obj.UserID);
                            $('#Id').val(obj.Id);
                            $selectDetailJabatan.selectpicker('refresh');

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = {
                    BUCode: $('#SelectBU').val(), maincode: $('#SelectMainJabatan').val(), area: $('#SelectDetailJabatan').val(), id: $('input[name=Id]').val(), UserID: $('#SelectUser').val()
                };
                //var savetype = 
                _setCustomFunctions.EditDetailJabatan(data);

                return false;

            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var Id = $row.find('.Id').html();
                //Set Ajax Submit Here
                var data = { Id: Id };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = {
                        BUCode: $('#SelectBU').val(), maincode: $('#SelectMainJabatan').val(), area: $('#SelectDetailJabatan').val(), UserID: $('#SelectUser').val()
                    };
                    //var savetype = 
                    _setCustomFunctions.saveDetailJabatan(data);
                    $modalAdd.modal('hide');

                    return false;
                }
            }
        },
        setCustomFunctions: {
            init: function () {
                this.getMasterBusinessUnit();
                this.getMasterMainJabatan();
                this.getMasterArea();
                this.getUserTop();
            },
            registerTabContent: function (tabId, mdlcode) {
            },
            getMasterBusinessUnit: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/getListMasterBusinessUnit',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectBU').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectBU").append($('<option>', {
                                value: obj.BUCode,
                                text: obj.BUCode + ' - ' + obj.Description
                            }));
                        })

                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getMasterMainJabatan: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/getListMasterMainJabatan',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectMainJabatan').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectMainJabatan").append($('<option>', {
                                value: obj.mainCode,
                                text: obj.mainCode + ' - ' + obj.Description
                            }));
                        })

                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getMasterArea: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/getMasterAreaJabatan',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        _globalArea = json;
                        $('#SelectDetailJabatan').find('option').remove();
                        json.forEach(function (obj) {
                            //if (obj.mainCode == $("#SelectMainJabatan option:selected").val()) {
                                $("#SelectDetailJabatan").append($('<option>', {
                                    value: obj.Code,
                                    text: obj.Area
                                }));
                            //}
                        })
                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            OnchangeMasterArea: function () {
                var _parent = this;
                //$.ajax({
                //    type: 'GET',
                //    url: '../../webservice/WebService_COR.asmx/getMasterAreaJabatan',
                //    contentType: 'application/json; charset=utf-8',
                //    dataType: 'json',
                //    success: function (response) {
                //        var json = $.parseJSON(response.d);
                        $('#SelectDetailJabatan').find('option').remove();
                        _globalArea.forEach(function (obj) {
                            if (obj.mainCode == $("#SelectMainJabatan option:selected").val()) {
                                $("#SelectDetailJabatan").append($('<option>', {
                                    value: obj.Code,
                                    text: obj.Area
                                }));
                            }
                        })
                $selectDetailJabatan.selectpicker('refresh');
                //        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                //    },
                //    error: function (xhr, ajaxOptions, thrownError) {
                //    }
                //});
            },
            getUserTop: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/getMasterUserTop',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectUser').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectUser").append($('<option>', {
                                value: obj.UserID,
                                text: obj.Nama + obj.noted
                            }));
                        })
                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveDetailJabatan: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveDetailJabatan',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableDetailJabatan.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Detail Jabatan has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteDetailJabatan: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteDetailJabatan',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableDetailJabatan.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Detail Jabatan has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditDetailJabatan: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditDetailJabatan',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $UpdateButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableDetailJabatan.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Detail Jabatan has been Update successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.DeleteDetailJabatan(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
})