﻿//$(function () {
//    var _name = [];



//})

var _name = [];
function SetupUser() {
    var _dataField = [];
    var _dataTable = [];
    var _dataTableAction = [];
    var _dataTableSub = [];
    var _dataTree = [];
    var _dataAfdeling = [];
    var _dataAccess = [];
    var _dataAccess2 = [];
    var _dataAccessPabrik = [];
    var _dataSubSection = [];
    var _listEmail = [];
    var actionTablePermanent = [];
    //var actionTabledateTo = [];
    var ReturnID = localStorage.getItem("ReturnID");
    var _global = "";
    var _companyDomain = "";
    var nameArr = [];
    var nameArr1 = [];
    var _msg;
    var _email;
    var _userid;
    var _useridLogin;
    var _usrGroupCode;
    var globVarPageID;
    var paramini = 0;
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddUser',
            BackButton: '#btnTabUser',
            saveButton: '#btnSave',
            setAccessButton: '#btnSetAccess',
            setAccessObjectButton: '#btnSetAccessObject',
            UpdateButton: '#btnUpdate',
            saveAfdButton: '#btnsaveAfd',
            addAfdButton: '#addAfd',
            editAfdButton: '#addEdit',

            addAccessButton: '#addAccess',
            editAccessButton: '#EditAccess',
            setDurationButton: '#btnSetTimeAccess',
            AddAfdTempButton: '#btnAddAccess',
            saveTimeAccessButton: '#btnSaveTimeAccess',
            cancelTimeAccessButton: '#btnCancelTimeAccess',
            cancelSetTimeAccessButton: '#btnCancelSetTimeAccess',
            clearAccessButton: '#btnClearAccess',
            addAccessPabrikButton: '#addAccessPabrik',
            editAccessPabrikButton: '#EditAccessPabrik',
            tickUpdate: '#tickUpdate',
            modalUser: '#mdlUser',
            modalaccessUser: '#mdlaccessUser',
            modalaccessUserPabrik: '#mdlaccessUserPabrik',
            modaldurationAccess: '#mdldurationAccess',
            modalAdd: '#mdlAdd',
            modalAddAFD: '#mdlUserAFD',
            table: '#tblactiveuser',
            tableOrdinary: '#tblordinaryuser',
            tableSurveyor: '#tblsurveyor',
            tableInactive: '#tblinactiveuser',
            tableUser: '#tblusers',
            tableUserWebObject: '#tbluserwebobject',
            tblData: '#tableAddDuration',
            tblSummaryAccess: '#tblsummaryaccess',
            selectFilterPlantation: '#SelectFilterPlantation',
            selectFilterCompany: '#SelectFilterCompany',
            selectFilterEstate: '#SelectFilterEstate',
            selectFilterGroupCompany: '#SelectFilterGroupCompany',
            selectAccess: '#SelectAccess',
            SelectObjectID: '#SelectObjectID',
            selectDept: '#SelectDept',
            selectmdlCode: '#SelectmdlCode',
            SelectpageID: '#SelectpageID',
            searchButton: '#btnSearch',
            lblemail: '#email',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setTableUserAcc();
            this.setTableUserWeb();
            this.setTableSummaryAccess();
            this.setCustomFunctions.init();
            this.setEvents.init();
            this.getCompanyDomain();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $BackButton = $(this.options.BackButton);
            $saveButton = $(this.options.saveButton);
            $saveAfdButton = $(this.options.saveAfdButton);
            $treeview = $(this.options.treeview);
            $setAccessButton = $(this.options.setAccessButton);
            $setAccessObjectButton = $(this.options.setAccessObjectButton);
            $UpdateButton = $(this.options.UpdateButton);
            $AddAfdButton = $(this.options.addAfdButton);
            $EditAfdButton = $(this.options.editAfdButton);
            $addAccessButton = $(this.options.addAccessButton);
            $editAccessButton = $(this.options.editAccessButton);
            $setDurationButton = $(this.options.setDurationButton);
            $addAfdTempButton = $(this.options.AddAfdTempButton);
            $saveTimeAccessButton = $(this.options.saveTimeAccessButton);
            $cancelTimeAccessButton = $(this.options.cancelTimeAccessButton);
            $cancelSetTimeAccessButton = $(this.options.cancelSetTimeAccessButton);
            $clearAccessButton = $(this.options.clearAccessButton);
            $addAccessPabrikButton = $(this.options.addAccessPabrikButton);
            $editAccessPabrikButton = $(this.options.editAccessPabrikButton);
            $TickUpdate = $(this.options.tickUpdate);
            $modalUser = $(this.options.modalUser);
            $modalaccessUser = $(this.options.modalaccessUser);
            $modalaccessUserPabrik = $(this.options.modalaccessUserPabrik);
            $modaldurationAccess = $(this.options.modaldurationAccess);
            $modalAddAFD = $(this.options.modalAddAFD);
            $table = $(this.options.table);
            $tableOrdinary = $(this.options.tableOrdinary);
            $tableSurveyor = $(this.options.tableSurveyor);
            $tableInactive = $(this.options.tableInactive);
            $tableUser = $(this.options.tableUser);
            $tableUserWebObject = $(this.options.tableUserWebObject);
            $tblData = $(this.options.tblData);
            $tblSummaryAccess = $(this.options.tblSummaryAccess);
            $searchButton = $(this.options.searchButton);
            $lblemail = $(this.options.lblemail);
            $SelectFilterCompany = $(this.options.selectFilterCompany);
            $SelectFilterPlantation = $(this.options.selectFilterPlantation);
            $SelectFilterEstate = $(this.options.selectFilterEstate);
            $SelectFilterGroupCompany = $(this.options.selectFilterGroupCompany);
            $SelectAccess = $(this.options.selectAccess);
            $SelectObjectID = $(this.options.SelectObjectID);
            $SelectDept = $(this.options.selectDept);
            $SelectmdlCode = $(this.options.selectmdlCode);
            $SelectpageID = $(this.options.SelectpageID);
            _formAdd = this.options.formAdd;
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
            //_getCompanyDomain = this.getCompanyDomain;
        },
        getCompanyDomain: function () {
            $.ajax({
                type: 'GET',
                url: '../../webservice/WebService_COR.asmx/GetEmailCompany',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var json = $.parseJSON(response.d);
                    json.forEach(function (obj) {
                        var names = json[0].GroupDomain;
                        nameArr = names.split(',');
                        for (var i = 0; i < nameArr.length; i++) {
                            nameArr1.push(nameArr[i]);
                        }
                        //var res = json[0].GroupDomain.replace("www.", "@");
                        //_companyDomain = res;
                    })

                }
            });
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $('#tblactiveuser').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                //"pagingType": "simple_numbers",
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 5,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 6,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 7,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 8,
                    "class": "center",
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    },
                    {
                        extend: 'excel',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    }],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserActive_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    //aoData.push({ "name": "roleId", "value": "admin" });
                    aoData.push({ 'name': 'GroupCompanyCode', 'value': $SelectFilterGroupCompany.val() });
                    aoData.push({ 'name': 'PlantationCode', 'value': $SelectFilterPlantation.val() });
                    aoData.push({ 'name': 'CompanyCode', 'value': $SelectFilterCompany.val() });
                    aoData.push({ 'name': 'EstCode', 'value': $SelectFilterEstate.val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblactiveuser").show();

                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });

            var table = $('#tblordinaryuser').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                //"pagingType": "simple_numbers",
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0
                }, {
                    "searchable": true,
                    "orderable": true,
                    "targets": 5,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 6,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 7,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 8,
                    "class": "center",
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    },
                    {
                        extend: 'excel',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    }],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserOrdinary_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    //aoData.push({ "name": "roleId", "value": "admin" });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblordinaryuser").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });

            var table = $('#tblsurveyor').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                //"pagingType": "simple_numbers",
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 5,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 6,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 7,
                    "class": "center",
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    },
                    {
                        extend: 'excel',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    }],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserSurveyor_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    //aoData.push({ "name": "roleId", "value": "admin" });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblsurveyor").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });

            var table = $('#tblinactiveuser').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                //"pagingType": "simple_numbers",
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 5,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 6,
                    "class": "center",
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    },
                    {
                        extend: 'excel',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    }],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserInactive_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    //aoData.push({ "name": "roleId", "value": "admin" });

                    aoData.push({ 'name': 'PlantationCode', 'value': $('#SelectFilterPlantation').val() });
                    aoData.push({ 'name': 'GroupCompanyCode', 'value': $('#SelectFilterGroupCompany').val() });
                    aoData.push({ 'name': 'CompanyCode', 'value': $('#SelectFilterCompany').val() });
                    aoData.push({ 'name': 'EstCode', 'value': $('#SelectFilterEstate').val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblinactiveuser").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });

            //////var table = $('#tblpendinguser').DataTable({
            //    dom: 'Blfrtip',
            //    lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
            //    pageLength: 25,
            //    "filter": true,
            //    //"pagingType": "simple_numbers",
            //    "columnDefs": [{
            //        "searchable": false,
            //        "orderable": false,
            //        "class": "center",
            //        "targets": 0
            //    }, {
            //        "searchable": false,
            //        "orderable": false,
            //        "targets": 5,
            //        "class": "center",
            //        width: 10
            //    }, {
            //        "searchable": false,
            //        "orderable": false,
            //        "targets": 6,
            //        "class": "center",
            //        width: 10
            //    }],
            //    "orderClasses": false,
            //    "order": [[1, "asc"]],
            //    "info": true,
            //    //"scrollY": "450px",
            //    //"scrollCollapse": true,
            //    "bProcessing": true,
            //    "bServerSide": true,
            //    "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserPending_ServerSideProcessing",
            //    "fnServerData": function (sSource, aoData, fnCallback) {
            //        //aoData.push({ "name": "roleId", "value": "admin" });
            //        $.ajax({
            //            "dataType": 'json',
            //            "contentType": "application/json; charset=utf-8",
            //            "type": "GET",
            //            "url": sSource,
            //            "data": aoData,
            //            "success": function (msg) {
            //                var json = jQuery.parseJSON(msg.d);
            //                fnCallback(json);
            //                $("#tblinactiveuser").show();
            //            },
            //            error: function (xhr, textStatus, error) {
            //                if (typeof console == "object") {
            //                    console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
            //                }
            //            }
            //        });
            //    },
            //    fnDrawCallback: function () {
            //    },
            //    fnRowCallback: function (nRow, aData, iDisplayIndex) {
            //        var info = $(this).DataTable().page.info();
            //        var page = info.page;
            //        var length = info.length;
            //        var index = (page * length + (iDisplayIndex + 1));
            //        $('td:eq(0)', nRow).html(index);
            //    }
            //});

            var table = $('#tblcloneuser').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                //"pagingType": "simple_numbers",
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 5,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 6,
                    "class": "center",
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    },
                    {
                        extend: 'excel',
                        className: 'btn btn-success',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        }
                    }],
                "info": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserClone_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblcloneuser").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });

        },
        setTableUserAcc: function () {
            var table = $('#tblusers').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 100,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                    width: 400
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 5,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetModule2_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    var _userid = "";
                    if (UsersID != "" && UsersID != undefined) {
                        _userid = UsersID;
                        UsersID = "";
                    }
                    else {
                        _userid = 0
                    }

                    aoData.push({ 'name': 'UserID', 'value': _userid });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblusers").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setTableUserWeb: function () {
            var table = $('#tbluserwebobject').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 100,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                    width: 400
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 5,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 6,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 7,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetSummaryAccess_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    var _userids = "";
                    if (_userid != "" && _userid != undefined) {
                        _userids = _userid;
                    }
                    else {
                        _userids = 0
                    }

                    aoData.push({ 'name': 'UserID', 'value': _userids });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblusers").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setTableSummaryAccess: function () {
            var table = $('#tblsummaryaccess').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 100,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                    width: 400
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 5,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 6,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 7,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetSummaryAccess_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    var _userids = "";
                    if (_userid != "" && _userid != undefined) {
                        _userids = _userid;
                    }
                    else {
                        _userids = 0
                    }

                    aoData.push({ 'name': 'UserID', 'value': _userids });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblusers").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },

        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $setAccessButton.on('click', this.setAccess);
                $setAccessObjectButton.on('click', this.setAccessObject);
                $BackButton.on('click', this.backtohomeuser);
                $AddAfdButton.on('click', this.popUpAfd);
                $EditAfdButton.on('click', this.popUpAfdEdit);
                $saveAfdButton.on('click', this.backtohomeuser);
                $addAccessButton.on('click', this.setAccessUser);
                $editAccessButton.on('click', this.editAccessUser);
                $setDurationButton.on('click', this.addDuration);
                $TickUpdate.on('click', this.activeField);
                $saveTimeAccessButton.on('click', this.saveDuration);
                $cancelTimeAccessButton.on('click', this.cancel);
                $cancelSetTimeAccessButton.on('click', this.cancelSetAccess);
                $clearAccessButton.on('click', this.clearAccess);
                $addAccessPabrikButton.on('click', this.setAccessPabrikUser);
                $editAccessPabrikButton.on('click', this.editAccessPabrikUser);
                //$MultiSubSectionButton.on('click', this.multisubsection);
                //Other Profile
                $table.on('click', '.User', this.ShowOtherProfile);
                $tableSurveyor.on('click', '.User', this.ShowOtherProfile);
                $tableOrdinary.on('click', '.User', this.ShowOtherProfile);
                $tableInactive.on('click', '.User', this.ShowOtherProfile);

                //edit
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $tableSurveyor.on('click', 'a[data-target="#edit"]', this.edit);
                $tableOrdinary.on('click', 'a[data-target="#edit"]', this.edit);
                $tableInactive.on('click', 'a[data-target="#setactive"]', this.setActive);

                //$tableInactive.on('click', 'a[data-target="#edit"]', this.setActive);
                //$tableUser.on('click', 'a[data-target="#accept"]', this.acceptUser);

                //delete
                $table.on('click', 'a[data-target="#delete"]', this.delete);
                $tableSurveyor.on('click', 'a[data-target="#delete"]', this.delete);
                $tableOrdinary.on('click', 'a[data-target="#delete"]', this.delete);
                //$tableInactive.on('click', 'a[data-target="#delete"]', this.deleteUserInactive);


                //tableuser
                $tableUser.on('click', 'a[data-target="#edit"]', this.editUser);
                //$tableUserWebObject.on('click', 'a[data-target="#edit"]', this.setAccessUser);
                //this.formvalidation();

                //select event
                $('#SelectDept').on('change', this.changesection);
                $('#SelectSection').on('change', this.changesubsection);

                //filter
                $SelectFilterPlantation.on('change', this.onChangePlantation);
                $SelectFilterGroupCompany.on('change', this.onChangeGroupCompany);
                $SelectFilterCompany.on('change', this.onChangeCompany);
                $SelectAccess.on('change', this.onChangeAccess);
                $SelectObjectID.on('change', this.onChangeObjectAccess);
                $SelectDept.on('change', this.onChangeDept);
                $SelectmdlCode.on('change', this.onChangemdlCode);
                $SelectpageID.on('change', this.onChangepageID);
                $searchButton.on('click', this.SearchUser);
                $lblemail.on('change', this.test);
            },
            formvalidation: function (_param) {
                //_setCustomFunctions.getEmailUser();
                //Add On For Validation Rule
                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                $.validator.addMethod('codExistss', function (value, element) {
                    //value.indexOf('@indo-palm.co.id') < 10 ? $('input[name=uname]').val('') : $('input[name=uname]').val(value.substr(0, value.indexOf('@')))
                    for (var i = 0; i < nameArr1.length; i++) {
                        if (value.indexOf(nameArr1[i].replace("www.", "@")) > -1) {
                            return true;
                            break;
                        }
                        //return false;

                    }
                    //return (value.indexOf('@gamaplantation.com') > -1 ? true : value.indexOf('@gamacorp.com') > -1 ? true : false);

                }, "Hanya domain @gamaplantation.com dan @gama-corp.com yang berlaku");


                jQuery.validator.addMethod('selectcheck', function (value) {
                    return (value != '0');
                }, "year required");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        SelectDept: {
                            selectcheck: true
                        },
                        name: {
                            remote: function () {
                                return {
                                    url: "../../webservice/WebService_COR.asmx/CheckNamaUser",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: JSON.stringify({ name: $('input[name=name]').val(), userID: $('input[name=userid]').val() }),
                                    dataFilter: function (data) {
                                        var msg = JSON.parse(data);
                                        var json = $.parseJSON(msg.d);
                                        if (json[0].Email == 'false' && json[0].msg == 'Euclid') {
                                            //$('input[name=telp]').val('');
                                            //$('input[name=name]').val('');
                                            //$('input[name=uname]').val('');
                                            //document.getElementById('lblemail').style.color = '#dd4b39';
                                            //document.getElementById('email').style.borderColor = '#dd4b39';
                                            ////document.getElementById('email-error').style.display = 'block';
                                            //document.getElementById('errormsg').style.display = 'block';
                                            ////$('.help-block').css('display', 'block')
                                            ////$('.help-block').css('innerHTML', 'Name Is Not Exist in System and Euclid')
                                            //document.all('errormsg').innerHTML = "Name Is Not Exist in System and Euclid";
                                            //document.all('errormsg1').innerHTML = "Name Is Not Exist in System and Euclid";

                                        }

                                    }
                                }
                            }
                            //_setCustomFunctions.showPopup('Info', 'Name Is Not Exist in System and Euclid');

                        },
                        email: {
                            remote: function () {
                                //document.getElementById('errormsg').style.display = 'none';
                                return {
                                    url: "../../webservice/WebService_COR.asmx/GetEmailUser",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: JSON.stringify({ code: $('input[name=email]').val(), userID: $('input[name=userid]').val() }),
                                    dataFilter: function (data) {
                                        var msg = JSON.parse(data);
                                        var json = $.parseJSON(msg.d);
                                        if (json[0].Email == 'true' && json[0].msg == 'Enterprise') {
                                            //$('#unit').prop('readonly', false);
                                            //$('#group').prop('readonly', false);
                                            //$('#userid').prop('readonly', false);
                                            //$('#name').prop('readonly', false);
                                            //$('#JK').prop('readonly', false);
                                            //$('#dept').prop('readonly', false);
                                            //$('#section').prop('readonly', false);
                                            //$('#jtitle').prop('readonly', false);
                                            //$('#jgrade').prop('readonly', false);
                                            //$('#alkantor').prop('readonly', false);
                                            //$('#telp').prop('readonly', false);
                                            //if (_param == 1 && _email != $('input[name=email]').val())
                                            //{
                                            document.getElementById('lblemail').style.color = '#dd4b39';
                                            document.getElementById('email').style.borderColor = '#dd4b39';
                                            //document.getElementById('help-block').style.display = 'block';
                                            document.getElementById('errormsg').style.display = 'block';
                                            document.all('errormsg').innerHTML = "Name Is Exist in System";

                                            //}
                                            //else
                                            //{
                                            //document.getElementById('lblemail').style.color = '#333';
                                            //document.getElementById('email').style.borderColor = '#d2d6de';
                                            ////document.getElementById('help-block').style.display = 'block';
                                            //document.getElementById('errormsg').style.display = 'none';
                                            //document.all('errormsg').innerHTML = "Name Is Exist in System";

                                            //}

                                            //_setCustomFunctions.showPopup('Info', 'Name Is Exist in System');

                                            _email = $('input[name=email]').val();
                                            //$('.help-block').css('innerHTML', 'Name Is Not Exist in System and Euclid')
                                            //$('.help-block').css('display', 'block')

                                        }
                                        else if (json[0].Email == 'true' && json[0].msg == 'Euclid') {
                                            //$('#unit').prop('readonly', true);
                                            //$('#group').prop('readonly', true);
                                            //$('#userid').prop('readonly', true);
                                            //$('#name').prop('readonly', true);
                                            //$('#JK').prop('readonly', true);
                                            //$('#dept').prop('readonly', true);
                                            //$('#section').prop('readonly', true);
                                            //$('#jtitle').prop('readonly', true);
                                            //$('#jgrade').prop('readonly', true);
                                            //$('#alkantor').prop('readonly', true);
                                            //$('#telp').prop('readonly', true);
                                            _setCustomFunctions.checkUserName();
                                            document.getElementById('errormsg').style.display = 'none';
                                        }
                                        else if (json[0].Email == 'false' && json[0].msg == 'Euclid') {
                                            _setCustomFunctions.showConfirmPopup(2, '', 'confirm', 'Email tidak terdaftar pada EUCLID, apakah anda ingin tetap melanjutkan?');

                                            document.getElementById('lblemail').style.color = '#333';
                                            document.getElementById('email').style.borderColor = '#d2d6de';
                                            //document.getElementById('help-block').style.display = 'block';
                                            document.getElementById('errormsg').style.display = 'none';
                                            document.all('errormsg').innerHTML = " ";
                                            $('input[name=telp]').val('');
                                            $('input[name=name]').val('');
                                            $('input[name=uname]').val('');

                                        }
                                        else {
                                            document.getElementById('lblemail').style.color = '#dd4b39';
                                            document.getElementById('email').style.borderColor = '#dd4b39';
                                            //document.getElementById('email-error').style.display = 'block';
                                            document.getElementById('errormsg').style.display = 'none';
                                            //$('.help-block').css('display', 'block');
                                            document.all('errormsg').innerHTML = "disini";
                                            //$('.help-block').css('innerHTML', 'Name Is Not Exist in System and Euclid');

                                        }

                                        //if (msg.hasOwnProperty('d'))
                                        //    return msg.d;

                                        //else
                                        //    return msg;
                                    }
                                }


                            },
                            minlength: 1,
                            required: true,
                            codExistss: true,
                            email: true,
                            //alreadyexists: true,
                        },

                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                        document.getElementById('errormsg').style.display = 'none';
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                        document.getElementById('errormsg').style.display = 'block';
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                            document.getElementById('errormsg').style.display = 'none';
                        }
                    },
                    messages: {
                        SelectDept: { valueNotEquals: "Please select an item!" }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
                document.getElementById('errormsg').style.display = 'none';
            },
            tabchange: function (tabId) {


            },
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            activeField: function () {
                $('#email').prop('disabled', false);
            },
            addTempAfd: function () {
                //alert(_dataAfdeling);
                _setCustomFunctions.setTree(0);
            },
            popUpAfd: function () {
                if (_dataTree.length != 0) {
                    _setCustomFunctions.setTreeAFD(_dataTree);
                    //alert(_dataTree);
                    $modalAddAFD.modal('toggle');
                }
                else {
                    _setCustomFunctions.showPopup('Info', 'Checked estate first');
                }

            },
            popUpAfdEdit: function () {
                //if (_dataTree.length != 0) {
                _setCustomFunctions.viewTreeAfd(_userid, _dataTree);
                //alert(_dataTree);
                $modalAddAFD.modal('toggle');
                //}
                //else {
                //    _setCustomFunctions.showPopup('Info', 'Checked estate first');
                //}

            },
            setAccessUser: function () {
                _setCustomFunctions.setTreeAccess();
                $modalaccessUser.modal('toggle');
            },
            editAccessUser: function () {
                $tblSummaryAccess.DataTable().ajax.reload();
                _setCustomFunctions.viewTreeAccess(_userid);
                $modalaccessUser.modal('toggle');
            },
            addDuration: function () {
                //_setCustomFunctions.setTreeAccess();
                //alert(_dataAccess);
                _setCustomFunctions.getData(_dataAccess);
                $modaldurationAccess.modal('toggle');
            },
            saveDuration: function () {

                var datatable = [];
                var datatable2 = [];
                var datas = [];
                var datatableWebAccess = [];

                var headers = [];
                //$('#tblDetail th').each(function (index, item) {
                //    headers[index] = $(item).html() == $(item).html();
                //});
                //$('#tblDetail tr').has('td').each(function () {
                //    var arrayItem = {};
                //    var dateFrom,dateTo;
                //    $('td', $(this)).each(function (index, item) {
                //        arrayItem[headers[index]] = $(item).html();
                //    });

                //    datatable.push(arrayItem);

                //});

                $('#tblDetail th').each(function (index, item) {
                    headers[index] = $(item).html() == "Sub Access" ? "Sub" : $(item).html();
                });
                $('#tblDetail tr').has('td').each(function () {
                    var arrayItem = {};
                    $('td', $(this)).each(function (index, item) {
                        arrayItem[headers[index]] = $(item).html();
                    });
                    datatable.push(arrayItem);
                });
                console.log(datatable);
                var no = 1;
                var step = 0;
                var split3 = [];
                for (var i = 0; i < datatable.length; i++) {
                    dateFrom = 'dateFrom_' + i;
                    dateTo = 'dateTo_' + i;
                    //dateFrom = datatable[i].From.substring(datatable[i].From.indexOf("dateFrom_"), datatable[i].From.indexOf('" onchange'));
                    //dateTo = datatable[i].To.substring(datatable[i].To.indexOf("dateTo_"), datatable[i].To.indexOf('" onchange'));
                    split3 = $('#' + dateFrom).val().split(',')
                    if (split3.length == 1) {
                        var myDateFrom = new Date($('#' + dateFrom).val());
                        var myDateTo = new Date($('#' + dateTo).val());
                        if (myDateFrom <= myDateTo) {

                            datatable2.push({ "page": datatable[i].Title, "access": datatable[i].Access, "dateFrom": $('#' + dateFrom).val(), "dateTo": $('#' + dateTo).val() });

                            no++;
                            step++;
                        }
                        else {
                            _setCustomFunctions.showPopup('confirm', 'Nakal ya kamu');
                            break;
                        }
                        if (step == datatable.length) {
                            console.log(datatable2);
                        }
                    }
                    else {
                        var page = datatable[i].Title;
                        var access = datatable[i].Access;
                        for (var x = 0; x < split3.length; x++) {
                            datatable2.push({ "page": page, "access": access, "dateFrom": split3[x], "dateTo": split3[x] });
                        }

                    }
                    //no++;
                }
                console.log(datatable2)
                _dataAccess2 = datatable2;
                $modaldurationAccess.modal('hide');
                $modalaccessUser.modal('hide');

                _setCustomFunctions.AccessByGroupCode();
            },
            cancel: function () {
                $modaldurationAccess.modal('hide');
            },
            cancelSetAccess: function () {
                $modalaccessUser.modal('hide');
            },
            clearAccess: function () {
                var data = { userID: _userid };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(5, data, 'confirm', 'Are You Sure Delete This Data?');
            },
            setAccessPabrikUser: function () {
                _setCustomFunctions.setTreeAccessPabrik();
                $modalaccessUserPabrik.modal('toggle');
            },
            editAccessPabrikUser: function () {
                _setCustomFunctions.viewTreeAccessPabrik(_userid);
                $modalaccessUserPabrik.modal('toggle');
            },
            add: function () {
                //Event for adding data
                $("#gridUser").hide();
                $("#addUser").show();
                _dataAfdeling = [];
                _dataTree = [];
                _dataAccessPabrik = [];
                document.getElementById('tickUpdate').style.display = 'none';

                document.getElementById('addEdit').style.display = 'none';
                document.getElementById('addAfd').style.display = 'inline';


                document.getElementById('addAccess').style.display = 'inline';
                document.getElementById('EditAccess').style.display = 'none';

                document.getElementById('addAccessPabrik').style.display = 'inline';
                document.getElementById('EditAccessPabrik').style.display = 'none';


                _setEvents.resetformvalidation();
                _setEvents.formvalidation(1);
                $('input[name=userid]').val('');
                $('input[name=name]').val('');
                $('input[name=email]').val('');
                $('input[name=uname]').val('');
                $('input[name=dept]').val('');
                $('input[name=unit]').val('');
                $('input[name=jtitle]').val('');
                $('input[name=jgrade]').val('');
                $('input[name=alkantor]').val('');
                $('input[name=telp]').val('');
                $('#SelectGroup').val('');
                $('#SelectJK').val('');
                $('#SelectDept').val('');
                $('#SelectSection').val('');
                //$('#SelectSubSection').val('');
                _setCustomFunctions.setTree(0);
                $tableUser.DataTable().ajax.reload();
                $('#email').prop('readonly', false);
                $('#email').prop('disabled', false);
                document.all('txt').innerHTML = "Add User";
                document.getElementById('lblemail').style.color = '#333';
                document.getElementById('email').style.borderColor = '#d2d6de';
                $("#tblSubSection tr").remove();
                _userid = '';
            },
            edit: function () {
                document.getElementById('addEdit').style.display = 'inline';
                document.getElementById('addAfd').style.display = 'none';

                document.getElementById('EditAccess').style.display = 'inline';
                document.getElementById('addAccess').style.display = 'none';

                document.getElementById('EditAccessPabrik').style.display = 'inline';
                document.getElementById('addAccessPabrik').style.display = 'none';


                _setEvents.resetformvalidation();
                _setEvents.formvalidation(2);
                var index = _listEmail.indexOf($('input[name=email]').val());
                if (index >= 0) {
                    _listEmail.splice(index, 1);
                }
                var $row = $(this).closest('tr');
                UsersID = $row.find('.UserID').html();
                $("#gridUser").hide();
                $("#addUser").show();
                //$('#email').prop('readonly', true);
                //$('#email').prop('disabled', true);
                $('#uname').prop('disabled', true);
                $('#email').prop('disabled', true);
                document.all('txt').innerHTML = "Edit User";

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetUsers',
                    type: "POST",
                    data: "{'usersID' :'" + UsersID + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            _userid = obj.UserID;
                            $('input[name=userid]').val(obj.UserID);
                            $('input[name=name]').val(obj.UserFullName);
                            $('input[name=email]').val(obj.UserEmail);
                            $('input[name=uname]').val(obj.UserName);
                            $('input[name=dept]').val(obj.DeptCode);
                            $('input[name=unit]').val(obj.UserUnit);
                            $('input[name=telp]').val(obj.UserHP);
                            $('input[name=alkantor]').val(obj.UserOfficeAddr);
                            $('input[name=jtitle]').val('');
                            $('input[name=jgrade]').val('');
                            $('#SelectGroup').val(obj.usrGroupCode);
                            $('#SelectJK').val(obj.UserGender);
                            $('#SelectDept').val(obj.DeptCode);
                            $('#SelectSection').val(obj.SectionCode);
                            //$('#SelectSubSection').val(obj.SubSectionCode);
                            $('#isleader').prop('checked', obj.isleader);
                            _setCustomFunctions.multisubsection('ByUserID');
                            $tableUserWebObject.DataTable().ajax.reload();
                            _setCustomFunctions.AccessByGroupCode();
                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

                _setCustomFunctions.setTree(UsersID);
                //setTableUserAcc(UsersID);
                $tableUser.DataTable().ajax.reload();
            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var UsersID = $row.find('.UserID').html();
                //Set Ajax Submit Here
                var data = { userID: UsersID };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(1, data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            deleteUserInactive: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var UsersID = $row.find('.UserID').html();
                //Set Ajax Submit Here
                var data = { userID: UsersID };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(4, data, 'confirm', 'Are You Sure Full Delete This Data?');


                return false;
            },
            setActive: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var UsersID = $row.find('.UserID').html();
                //Set Ajax Submit Here
                var data = { userID: UsersID };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(3, data, 'confirm', 'Are You Sure Set Active This Data?');


                return false;
            },
            save: function () {
                if (_global == 0) {
                    if ($('input[name=userid]').val() == "") {
                        //if ($formAdd.valid()) {
                        //Event for saving data
                        var $this = $(this);
                        //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                        $("#formAdd :input").prop("disabled", false);

                        //Set Ajax Submit Here
                        //var savetype = 
                        var datatable = [];
                        var datatableSubSection = [];
                        var datas = [];
                        var datatableWebAccess = [];

                        var headers = [];
                        $('#tblusers th').each(function (index, item) {
                            headers[index] = $(item).html() == "Sub Access" ? "Sub" : $(item).html();
                        });
                        $('#tblusers tr').has('td').each(function () {
                            var arrayItem = {};
                            $('td', $(this)).each(function (index, item) {
                                arrayItem[headers[index]] = $(item).html();
                            });
                            datatable.push(arrayItem);
                        });

                        var $row = $(this).closest('tr');
                        _dataTable = [];
                        _dataTableSub = [];
                        for (var i = 0; i < datatable.length; i++) {
                            var a = jQuery(datatable[i].Access).text();
                            var lastFive = a.substr(a.length - 4);
                            var b = jQuery(datatable[i].Sub).text();
                            var lastFives = b.substr(b.length - 2);

                            _dataTable.push(lastFive);

                            if (b != "None") {
                                _dataTableSub.push(lastFives);
                            }
                        }


                        var headers = [];
                        $('#tblSubSection th').each(function (index, item) {
                            headers[index] = $(item).html();
                        });
                        $('#tblSubSection tr').has('td').each(function () {
                            var arrayItem = {};
                            $('td', $(this)).each(function (index, item) {
                                arrayItem[headers[index]] = $(item).html();
                            });
                            datatableSubSection.push(arrayItem);
                        });
                        _dataSubSection = [];
                        $('#tblSubSection tr').has('td').each(function () {
                            if ($(this).closest('tr').find('input:checkbox')[0].checked == true) {
                                if ($(this).closest('tr').find('input:checkbox')[1].checked == true) {
                                    _dataSubSection.push({ UserID: $('input[name=userid]').val(), SubSectionCode: $(this).closest('tr').find('input:checkbox')[0].id, isLeader: true });
                                }
                                else {
                                    _dataSubSection.push({ UserID: $('input[name=userid]').val(), SubSectionCode: $(this).closest('tr').find('input:checkbox')[0].id, isLeader: false });
                                }
                            }

                        });


                        console.log(_dataAccess);
                        _setCustomFunctions.saveUser(_dataTable, _dataTree, _dataSubSection, _dataTableSub, _dataAfdeling, _dataAccess2, _dataAccessPabrik);

                        return false;
                        //}
                    }
                    else {
                        //$("#email").rules("remove", "alreadyexist");
                        if ($formAdd.valid()) {
                            //Event for saving data
                            var $this = $(this);
                            //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                            $("#formAdd :input").prop("disabled", false);

                            //Set Ajax Submit Here
                            //var savetype = 
                            var datatable = [];
                            var datas = [];

                            var headers = [];
                            $('#tblusers th').each(function (index, item) {
                                headers[index] = $(item).html() == "Sub Access" ? "Sub" : $(item).html();
                            });
                            $('#tblusers tr').has('td').each(function () {
                                var arrayItem = {};
                                $('td', $(this)).each(function (index, item) {
                                    arrayItem[headers[index]] = $(item).html();
                                });
                                datatable.push(arrayItem);
                            });

                            var $row = $(this).closest('tr');
                            _dataTable = [];
                            _dataTableSub = [];
                            for (var i = 0; i < datatable.length; i++) {
                                var a = jQuery(datatable[i].Access).text();
                                var lastFive = a.substr(a.length - 4);
                                var b = jQuery(datatable[i].Sub).text();
                                var lastFives = b.substr(b.length - 2);

                                _dataTable.push(lastFive);

                                if (b != "None") {
                                    _dataTableSub.push(lastFives);
                                }
                            }


                            var headers = [];
                            //$('#tblSubSection th').each(function (index, item) {
                            //    headers[index] = $(item).html();
                            //});
                            _dataSubSection = [];
                            $('#tblSubSection tr').has('td').each(function () {
                                if ($(this).closest('tr').find('input:checkbox')[0].checked == true) {
                                    if ($(this).closest('tr').find('input:checkbox')[1].checked == true) {
                                        _dataSubSection.push({ UserID: $('input[name=userid]').val(), SubSectionCode: $(this).closest('tr').find('input:checkbox')[0].id, isLeader: true });
                                    }
                                    else {
                                        _dataSubSection.push({ UserID: $('input[name=userid]').val(), SubSectionCode: $(this).closest('tr').find('input:checkbox')[0].id, isLeader: false });
                                    }
                                }

                            });

                            console.log(_dataSubSection);
                            _setCustomFunctions.SaveEditUser(_dataTable, _dataTree, _dataSubSection, _dataTableSub, _dataAfdeling, _dataAccess2, _dataAccessPabrik);

                            return false;
                        }
                    }

                }
                else {
                    _setCustomFunctions.showPopup('confirm', 'Nakal ya kamu');
                }



            },
            backtohomeuser: function () {
                $("#gridUser").show();
                $("#addUser").hide();

            },
            //setActive: function () {
            //    $modalInactive.modal('toggle');
            //},
            acceptUser: function () {
                $modalInactive.modal('toggle');
            },
            editUser: function () {
                var $row = $(this).closest('tr');
                mdlCode = $row.find('.mdlCode').html();
                mdlAccCode = $row.find('.mdlCode_Access').html();
                _setCustomFunctions.getAccess(mdlCode, mdlAccCode);
                //_setCustomFunctions.getSubModuleAccess();
                $modalUser.modal('toggle');
            },

            setAccess: function () {
                var $row = $(this).closest('tr');
                mdlCode = $('input[name=code]').val();
                mdlCode = $('input[name=code]').val();
                mdlAccCode = $('#SelectAccess option:selected').text();
                mdlAccCodes = $('#SelectAccess option:selected').val();

                submdlAccCode = $('#SelectSubModuleAccess option:selected').text();
                submdlAccCodes = $('#SelectSubModuleAccess option:selected').val();

                $('#mdl' + mdlCode).html(mdlAccCode);
                $('#' + mdlCode + "_Access").html(mdlAccCodes);

                if (submdlAccCodes != "0") {
                    $('#Sub' + mdlCode).html(submdlAccCode);
                    $('#' + mdlCode + "_SubAccess").html(submdlAccCodes);
                }
                else {
                    $('#Sub' + mdlCode).html("None");
                    $('#' + mdlCode + "_SubAccess").html("");
                }

                $modalUser.modal('hide');
                //

            },
            setAccessObject: function () {
                var $row = $(this).closest('tr');
                pageID = $('input[name=pageID]').val();
                objectID = $('#SelectObjectID option:selected').text();
                objectName = $('#SelectObjectID option:selected').val();
                description = $('input[name=description]').val();


                $('#mdl' + pageID).html(objectID);
                //$('#' + pageID + "_Access").html(objectID);

                $('#Sub' + pageID).html(description);
                //$('#' + pageID + "_SubAccess").html(description);

                $modalaccessUser.modal('hide');
                //

            },
            changesection: function () {
                _setCustomFunctions.getSection($('#SelectDept').val());
            },
            changesubsection: function () {
                _setCustomFunctions.multisubsection('BySection');
            },
            ShowOtherProfile: function () {
                var $row = $(this).closest('tr');
                var UserID = $row.find('.UserID').html();
                window.location.href = "COR.aspx?bp=userdetail";
                var getInput = UserID;
                var encrypted = CryptoJS.AES.encrypt(UserID, "secret message");
                localStorage.setItem("storageName", encrypted);
                //$.ajax({
                //    type: 'POST',
                //    url: '../../webservice/WebService_COR.asmx/GetOtherProfile',
                //    contentType: 'application/json; charset=utf-8',
                //    dataType: 'json',
                //    data: "{'UserID' :'" + UserID + "'}",
                //    success: function (response) {
                //        var json = $.parseJSON(response.d);
                //        console.log(json)

                //    },
                //    error: function (xhr, ajaxOptions, thrownError) {
                //    }
                //});


            },
            SearchUser: function () {
                $table.DataTable().ajax.reload();
            },
            onChangePlantation: function () {
                var opunit = '';
                if ($SelectFilterPlantation.val() != null) {
                    $SelectFilterPlantation.val().forEach(function (opt) {
                        if (opunit === '')
                            opunit += opt;
                        else
                            opunit += ',' + opt;
                    })
                }

                _setCustomFunctions.getMasterGroupCompany(opunit);
                _setCustomFunctions.getCompanyByPlantation(opunit);
            },
            onChangeGroupCompany: function () {
                //$("#tbodyid tr").remove();
                //$SelectFilterEstate.val('');
                //var paramID = this.value;
                var opunit = '';
                if ($SelectFilterGroupCompany.val() != null) {
                    $SelectFilterGroupCompany.val().forEach(function (opt) {
                        if (opunit === '')
                            opunit += opt;
                        else
                            opunit += ',' + opt;
                    })
                }
                _setCustomFunctions.getCompanyByGroupCompany(opunit);
            },
            onChangeCompany: function () {
                //$("#tbodyid tr").remove();
                //$SelectFilterEstate.val('');
                //var paramID = this.value;
                var opunit = '';
                if ($SelectFilterCompany.val() != null) {
                    $SelectFilterCompany.val().forEach(function (opt) {
                        if (opunit === '')
                            opunit += opt;
                        else
                            opunit += ',' + opt;
                    })
                }
                _setCustomFunctions.getEstateByCompany(opunit);
            },
            onChangeAccess: function () {
                _setCustomFunctions.getSubModuleAccess($('#SelectAccess').val());
            },
            onChangeObjectAccess: function () {
                _setCustomFunctions.getObjectAccess(globVarPageID);
            },
            onChangeDept: function () {
                if ($('#SelectDept').val() != "GIS") {
                    $("#tblSubSection tr").remove();
                }
            },
            onChangemdlCode: function () {
                if ($('#SelectmdlCode').val() != "" || $('#SelectmdlCode').val() != "0") {
                    $.ajax({
                        type: 'POST',
                        url: '../../webservice/WebService_COR.asmx/GetMasterPageID',
                        contentType: 'application/json; charset=utf-8',
                        data: "{'mdlCode' :'" + $("#SelectmdlCode option:selected").val() + "'}",
                        dataType: 'json',
                        success: function (response) {
                            var json = $.parseJSON(response.d);
                            $('#SelectpageID').find('option').remove();
                            $("#SelectpageID").append($('<option>', {
                                value: "0",
                                text: "-- Choose Here --"
                            }));
                            json.forEach(function (obj) {
                                $("#SelectpageID").append($('<option>', {
                                    value: obj.PageID,
                                    text: obj.PageTitle
                                }));
                            });
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });
                }
                else {
                    alert("check module");
                }
            },
            onChangepageID: function () {
                if ($('#SelectmdlCode').val() != "" || $('#SelectmdlCode').val() != "0") {
                    $.ajax({
                        type: 'POST',
                        url: '../../webservice/WebService_COR.asmx/GetMasterObjectID',
                        contentType: 'application/json; charset=utf-8',
                        data: "{'pageID' :'" + $("#SelectpageID option:selected").val() + "'}",
                        dataType: 'json',
                        success: function (response) {
                            var json = $.parseJSON(response.d);
                            $('#SelectObjectID').find('option').remove();
                            $("#SelectObjectID").append($('<option>', {
                                value: "0",
                                text: "-- Choose Here --"
                            }));
                            json.forEach(function (obj) {
                                $("#SelectObjectID").append($('<option>', {
                                    value: obj.PageID,
                                    text: obj.PageTitle
                                }));
                            });
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });
                }
                else {
                    alert("check module");
                }
            },
        },
        setCustomFunctions: {
            init: function () {
                this.getSessionUID();
                this.getGroup();
                this.getAccess();
                this.getDepartment();
                this.getSection('');
                //this.getSubSection('');
                this.setTree(0);
                this.ReturnID();
                this.getPlantation();
                this.getMasterGroupCompany("");
                this.getCompanyByGroupCompany("");
                this.getEstateByCompany("");
                //this.getSubModuleAccess();
                //this.multisubsection(0);
                //this.checkUserName();
            },
            getSessionUID: function () {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetUserID',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        _useridLogin = response.d;
                        $.ajax({
                            type: "POST",
                            url: '../../webservice/WebService_COR.asmx/GetUserGroup',
                            data: "{'userID' :'" + _useridLogin + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var json = $.parseJSON(response.d);
                                json.forEach(function (obj) {
                                    _usrGroupCode = obj.usrGroupCode;
                                });

                            }
                        });
                    }
                });
            },
            multisubsection: function (_by) {
                if (_by == 'ByUserID') {
                    var htmlText;
                    $.ajax({
                        url: '../../webservice/WebService_COR.asmx/GetDataTableSubsection_ServerSideProcessing',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: "POST",
                        data: JSON.stringify({ UserID: $("#userid").val() }),
                        success: function (response) {
                            var json = $.parseJSON(response.d);
                            var tables = $('#tblSubSection');
                            $("#tblSubSection tr").remove();
                            json.forEach(function (obj) {
                                if (obj.HasAccess == 1) {
                                    if (obj.isLeader == 0) {
                                        tables.append("<tr><td>" + obj.SubSectionName + "</td><td><input type='checkbox' checked='checked' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox'></input></td></tr>");
                                    }
                                    else if (obj.isLeader == 1) {
                                        tables.append("<tr><td>" + obj.SubSectionName + "</td><td><input type='checkbox' checked='checked' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox' checked='checked'></input></td></tr>");
                                    }
                                    else if (obj.isLeader == 2) {
                                        tables.append("<tr><td>" + obj.SubSectionName + "</td><td><input type='checkbox' checked='checked' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox'  disabled='disable'></input></td></tr>");
                                    }

                                }
                                else {
                                    if (obj.isLeader == 0) {
                                        tables.append("<tr><td>" + obj.SubSectionName + "</td><td><input type='checkbox' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox'></input></td></tr>");
                                    }
                                    else if (obj.isLeader == 1) {
                                        tables.append("<tr><td>" + obj.SubSectionName + "</td><td><input type='checkbox' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox' checked='checked'></input></td></tr>");
                                    }
                                    else if (obj.isLeader == 2) {
                                        tables.append("<tr><td>" + obj.SubSectionName + "</td><td><input type='checkbox' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox'  disabled='disable'></input></td></tr>");
                                    }
                                }

                            });
                        }
                    });
                }
                else {
                    var htmlText;
                    $.ajax({
                        url: '../../webservice/WebService_COR.asmx/GetDataTableSubsectionBySection_ServerSideProcessing',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: "POST",
                        data: JSON.stringify({ SectionCode: $("#SelectSection").val() }),
                        success: function (response) {
                            var json = $.parseJSON(response.d);
                            var tables = $('#tblSubSection');
                            $("#tblSubSection tr").remove();
                            json.forEach(function (obj) {
                                if (obj.HasAccess == 1) {
                                    if (obj.isLeader == 0) {
                                        tables.append("<tr><td style='width: 300px;'>" + obj.SubSectionName + "</td><td><input type='checkbox' checked='checked' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox'></input></td></tr>");
                                    }
                                    else if (obj.isLeader == 1) {
                                        tables.append("<tr><td style='width: 300px;'>" + obj.SubSectionName + "</td><td><input type='checkbox' checked='checked' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox' checked='checked'></input></td></tr>");
                                    }
                                    else if (obj.isLeader == 2) {
                                        tables.append("<tr><td style='width: 300px;'>" + obj.SubSectionName + "</td><td><input type='checkbox' checked='checked' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox'  disabled='disable'></input></td></tr>");
                                    }

                                }
                                else {
                                    if (obj.isLeader == 0) {
                                        tables.append("<tr><td style='width: 300px;'>" + obj.SubSectionName + "</td><td><input type='checkbox' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox'></input></td></tr>");
                                    }
                                    else if (obj.isLeader == 1) {
                                        tables.append("<tr><td style='width: 300px;'>" + obj.SubSectionName + "</td><td><input type='checkbox' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox' checked='checked'></input></td></tr>");
                                    }
                                    else if (obj.isLeader == 2) {
                                        tables.append("<tr><td style='width: 300px;'>" + obj.SubSectionName + "</td><td><input type='checkbox' id=" + obj.SubSectionCode + "></input></td><td><input type='checkbox'  disabled='disable'></input></td></tr>");
                                    }
                                }

                            });
                        }
                    });
                }


            },
            getPlantation: function () {
                $SelectFilterPlantation.val('');
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterPlantationActive',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        if (json.length > 0) {
                            $SelectFilterPlantation.find('option').remove();
                            //var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {

                                option = $('<option>', {
                                    value: obj.PLANTATION,
                                    text: obj.PLANTATION
                                });

                                option.appendTo($SelectFilterPlantation);
                            });

                            $SelectFilterPlantation.selectpicker('refresh');
                        }
                    }
                })
            },
            getMasterGroupCompany: function (paramID) {
                //$SelectFilterGroupCompany.val('');
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetGroupCompanyByPlantation',
                    contentType: 'application/json; charset=utf-8',
                    data: "{data: '" + paramID + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        if (json.length > 0) {
                            $SelectFilterGroupCompany.find('option').remove();
                            //var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {

                                option = $('<option>', {
                                    value: obj.GroupCompanyName,
                                    text: obj.GroupCompanyName
                                });

                                option.appendTo($SelectFilterGroupCompany);
                            });

                            $SelectFilterGroupCompany.selectpicker('refresh');
                        }
                    }
                })
            },
            getCompanyByPlantation: function (paramID) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetCompanyByPlantation',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify({ data: paramID }),
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        if (json.length > 0) {
                            $SelectFilterCompany.find('option').remove();
                            //var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {

                                option = $('<option>', {
                                    value: obj.CompanyName,
                                    text: obj.CompanyName
                                });

                                option.appendTo($SelectFilterCompany);
                            });

                            $SelectFilterCompany.selectpicker('refresh');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getCompanyByGroupCompany: function (paramID) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetCompanyByGroupCompany',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify({ data: paramID }),
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        if (json.length > 0) {
                            $SelectFilterCompany.find('option').remove();
                            //var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {

                                option = $('<option>', {
                                    value: obj.CompanyName,
                                    text: obj.CompanyName
                                });

                                option.appendTo($SelectFilterCompany);
                            });

                            $SelectFilterCompany.selectpicker('refresh');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getEstateByCompany: function (paramID) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetEstateByCompany',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify({ data: paramID }),
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        if (json.length > 0) {
                            $SelectFilterEstate.find('option').remove();
                            //var optgroup = "", group = "", option = "";

                            json.forEach(function (obj) {

                                option = $('<option>', {
                                    value: obj.estCode,
                                    text: obj.NewEstName
                                });

                                option.appendTo($SelectFilterEstate);
                            });

                            $SelectFilterEstate.selectpicker('refresh');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            ReturnID: function () {
                if (ReturnID == 'NoID') {
                    localStorage.removeItem("ReturnID");
                    _setCustomFunctions.showPopup('confirm', 'ID Not Exist');
                }
            },
            getAccess: function (mdlCode, mdlAccCode) {
                $('input[name=mdlacccoder]').val(mdlAccCode);
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterAccess',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'mdlCode' :'" + mdlCode + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectAccess').find('option').remove();
                        $("#SelectAccess").append($('<option>', {
                            value: "0",
                            text: "-- Choose Here --"
                        }));
                        json.forEach(function (obj) {
                            $("#SelectAccess").append($('<option>', {
                                value: obj.mdlAccCode,
                                text: obj.mdlAccDesc
                            }));
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
                $('input[name=code]').val(mdlCode);
                $('input[name=mdlacccoder]').val(mdlCode);
                this.getSubModuleAccess();
            },
            getAccessObject: function (mdlCode, pageID, pageTitle) {
                //$('input[name=mdlacccoder]').val(mdlAccCode);
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterObjectAccess',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'pageID' :'" + pageID + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectObjectID').find('option').remove();
                        $("#SelectObjectID").append($('<option>', {
                            value: "0",
                            text: "-- Choose Here --"
                        }));
                        json.forEach(function (obj) {
                            $("#SelectObjectID").append($('<option>', {
                                value: obj.AObjectID,
                                text: obj.AObjectName
                            }));
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
                $('input[name=codemdl]').val(mdlCode);
                $('input[name=pagetitle]').val(pageTitle);
                this.getObjectAccess(pageID);
            },
            getSubModuleAccess: function () {
                $('#SelectSubModuleAccess').find('option').remove();
                $('#SelectSubModuleAccess').val("0");
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterSubModuleAccess',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'mdlAccCode' :'" + $('#SelectAccess').val() + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $("#SelectSubModuleAccess").append($('<option>', {
                            value: "0",
                            text: "-- Choose Here --"
                        }));
                        json.forEach(function (obj) {
                            $("#SelectSubModuleAccess").append($('<option>', {
                                value: obj.subMdlAccCode,
                                text: obj.subMdlName
                            }));
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });

            },
            getObjectAccess: function (pageID) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetDeskripsiObjectAccess',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'ObjectID' :'" + $('#SelectObjectID option:selected').val() + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        json.forEach(function (obj) {
                            $('input[name=description]').val(obj.Description)
                            $('input[name=pageID]').val(pageID)
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });

            },
            getGroup: function () {
                var _parent = this;
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterGroup',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectGroup').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectGroup").append($('<option>', {
                                value: obj.usrGroupCode,
                                text: obj.usrGroupCode + ' - ' + obj.usrGroupDesc
                            }));
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getDepartment: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterDepartment',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectDept').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectDept").append($('<option>', {
                                value: obj.DeptCode,
                                text: obj.DeptCode + ' - ' + obj.DeptName
                            }));
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getSection: function (_deptCode) {
                var _parent = this;
                $.ajax({
                    type: "POST",
                    data: "{'DeptCode' :'" + _deptCode + "'}",
                    url: '../../webservice/WebService_COR.asmx/GetMasterDepartmentSectionByDeptCode',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',

                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectSection').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectSection").append($('<option>', {
                                value: obj.SectionCode,
                                text: obj.SectionCode + ' - ' + obj.SectionName
                            }));
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            //getSubSection: function (_sectionCode) {
            //    var _parent = this;
            //    $.ajax({
            //        type: "POST",
            //        data: "{'SectionCode' :'" + _sectionCode + "'}",
            //        url: '../../webservice/WebService_COR.asmx/GetMasterSubSectionBySectionCode',
            //        contentType: 'application/json; charset=utf-8',
            //        dataType: 'json',

            //        success: function (response) {
            //            var json = $.parseJSON(response.d);
            //            $('#SelectSubSection').find('option').remove();
            //            json.forEach(function (obj) {
            //                $("#SelectSubSection").append($('<option>', {
            //                    value: obj.SubSectionCode,
            //                    text: obj.SubSectionCode + ' - ' + obj.SubSectionName
            //                }));
            //            })

            //        },
            //        error: function (xhr, ajaxOptions, thrownError) {
            //        }
            //    });
            //},
            setTree: function (UsersID) {
                var datas = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetEstAccessUser',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: "{'UsersID' :'" + UsersID + "'}",
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0; i < json.length; i++) {
                            datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }
                        var finaldata = "";
                        if (UsersID == 0) {
                            finaldata = json;
                        }
                        else {
                            finaldata = datas;
                        }
                        var $treeview = $("#tvestate");
                        $('#tvestate').jstree('destroy');
                        $('#tvestate').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                'data':
                                    finaldata
                            },
                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                        });

                        $('#tvestate')
                            // listen for event
                            .on('changed.jstree', function (e, data) {
                                _dataTree = data.instance.get_bottom_selected();
                                _setCustomFunctions.viewTreeAfd(_userid, _dataTree);

                            })
                            // create the instance
                            .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });





            },
            viewTreeAfd: function (userID, _listEstate) {
                var dataestate = { UserID: userID, ListEstate: _listEstate };
                var datass = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetAfdAccessUser',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: JSON.stringify({ dataobject: JSON.stringify(dataestate) }),
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0; i < json.length; i++) {
                            datass.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }
                        var finaldata = "";
                        if (userID == 0) {
                            finaldata = json;
                        }
                        else {
                            finaldata = datass;
                        }
                        var $treeview = $("#tvafdeling");
                        $('#tvafdeling').jstree('destroy');
                        $('#tvafdeling').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                'data':
                                    finaldata
                            },
                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                        });

                        $('#tvafdeling')
                            // listen for event
                            .on('changed.jstree', function (e, data) {
                                _dataAfdeling = data.instance.get_bottom_selected();

                            })
                            // create the instance
                            .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            setTreeAFD: function (_listEstate) {
                var dataestate = { Text: "", ListEstate: _listEstate };
                var datas = [];
                var id = 1;

                if (_dataAfdeling.length != 0) {
                    var dataestate = { ListEstate: _dataAfdeling };
                    $.ajax({
                        url: '../../webservice/WebService_COR.asmx/GetTempChecklistEstate',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: "POST",
                        data: JSON.stringify({ dataobject: JSON.stringify(dataestate) }),
                        success: function (response) {
                            var json = jQuery.parseJSON(response.d);

                        }
                    });
                }
                else {
                    $.ajax({
                        url: '../../webservice/WebService_COR.asmx/GetAllAFDEstate',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        type: "POST",
                        data: JSON.stringify({ dataobject: JSON.stringify(dataestate) }),
                        success: function (response) {
                            var json = jQuery.parseJSON(response.d);
                            for (var i = 0; i < json.length; i++) {
                                datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                            }
                            var finaldata = "";
                            if (UsersID == 0) {
                                finaldata = json;
                            }
                            else {
                                finaldata = datas;
                            }
                            var $treeview = $("#tvafdeling");
                            $('#tvafdeling').jstree('destroy');
                            $('#tvafdeling').jstree({
                                plugins: ['checkbox', 'changed'],
                                'core': {
                                    "themes": { "icons": false },
                                    expand_selected_onload: true,
                                    'data':
                                        finaldata
                                },
                            }).bind("loaded.jstree", function (event, data) {
                                //$(this).jstree("open_all");
                                $treeview.jstree('open_all');
                            });

                            $('#tvafdeling')
                                // listen for event
                                .on('changed.jstree', function (e, data) {
                                    _dataAfdeling = data.instance.get_bottom_selected();
                                    //this.setTreeAFD(_dataTree);
                                    //alert(_dataAfdeling);
                                    //$modalAddAFD.modal('toggle');
                                })
                                // create the instance
                                .jstree();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });
                }
            },
            setTreeAccess: function () {
                var datas = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetAllAccess',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    //data: JSON.stringify({ dataobject: JSON.stringify(dataestate) }),
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0; i < json.length; i++) {
                            datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }
                        var finaldata = "";
                        if (UsersID == 0) {
                            finaldata = json;
                        }
                        else {
                            finaldata = datas;
                        }
                        var $treeview = $("#tvaaccess");
                        $('#tvaaccess').jstree('destroy');
                        $('#tvaaccess').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                'data':
                                    finaldata
                            },
                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                            $treeview.jstree().disable_node(this.id);
                        });

                        $('#tvaaccess')
                            // listen for event
                            .on('changed.jstree', function (e, data) {
                                _dataAccess = data.instance.get_bottom_selected();
                                //this.setTreeAFD(_dataTree);
                                //alert(_dataAfdeling);
                                //$modalAddAFD.modal('toggle');
                            })
                            // create the instance
                            .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            viewTreeAccess: function (_userid) {
                var dataestate = { UserID: _userid };
                var datass = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetObjectAccessUser',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: JSON.stringify({ dataobject: JSON.stringify(dataestate) }),
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        paramini = 0;
                        for (var i = 0; i < json.length; i++) {
                            if (json[i].state == 'true') {
                                paramini = 1;
                            }
                            datass.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }
                        var finaldata = "";
                        if (_userid == 0) {
                            finaldata = json;
                        }
                        else {
                            finaldata = datass;
                        }


                        if (paramini != 1) {
                            document.getElementById('btnClearAccess').style.display = 'none';
                            document.getElementById('btnSetTimeAccess').style.display = 'inline';
                        }
                        else {
                            document.getElementById('btnClearAccess').style.display = 'inline';
                            document.getElementById('btnSetTimeAccess').style.display = 'none';
                        }
                        var $treeview = $("#tvaaccess");
                        $('#tvaaccess').jstree('destroy');
                        $('#tvaaccess').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                'data':
                                    finaldata
                            },
                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                            $treeview.jstree().disable_node(this.id);
                        });

                        $('#tvaaccess')
                            // listen for event
                            .on('changed.jstree', function (e, data) {
                                _dataAccess = data.instance.get_bottom_selected();

                            })
                            // create the instance
                            .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            setTreeAccessPabrik: function () {
                var datas = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetAllAccessPabrik',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    //data: JSON.stringify({ dataobject: JSON.stringify(dataestate) }),
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0; i < json.length; i++) {
                            datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }
                        var finaldata = "";
                        if (UsersID == 0) {
                            finaldata = json;
                        }
                        else {
                            finaldata = datas;
                        }
                        var $treeview = $("#tvaaccesspabrik");
                        $('#tvaaccesspabrik').jstree('destroy');
                        $('#tvaaccesspabrik').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                'data':
                                    finaldata
                            },
                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                            $treeview.jstree().disable_node(this.id);
                        });

                        $('#tvaaccesspabrik')
                            // listen for event
                            .on('changed.jstree', function (e, data) {
                                _dataAccessPabrik = data.instance.get_bottom_selected();
                                //this.setTreeAFD(_dataTree);
                                //alert(_dataAfdeling);
                                //$modalAddAFD.modal('toggle');
                            })
                            // create the instance
                            .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            viewTreeAccessPabrik: function (_userid) {
                var dataestate = { UserID: _userid };
                var datass = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetAccessPabrikUser',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: JSON.stringify({ dataobject: JSON.stringify(dataestate) }),
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        paramini = 0;
                        for (var i = 0; i < json.length; i++) {
                            if (json[i].state == 'true') {
                                paramini = 1;
                            }
                            datass.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }
                        var finaldata = "";
                        if (_userid == 0) {
                            finaldata = json;
                        }
                        else {
                            finaldata = datass;
                        }


                        //if (paramini != 1) {
                        //    document.getElementById('btnClearAccessPabrik').style.display = 'none';
                        //    document.getElementById('btnSetTimeAccessPabrik').style.display = 'inline';
                        //}
                        //else {
                        //    document.getElementById('btnClearAccessPabrik').style.display = 'inline';
                        //    document.getElementById('btnSetTimeAccessPabrik').style.display = 'none';
                        //}
                        var $treeview = $("#tvaaccesspabrik");
                        $('#tvaaccesspabrik').jstree('destroy');
                        $('#tvaaccesspabrik').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                'data':
                                    finaldata
                            },
                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                            $treeview.jstree().disable_node(this.id);
                        });

                        $('#tvaaccesspabrik')
                            // listen for event
                            .on('changed.jstree', function (e, data) {
                                _dataAccessPabrik = data.instance.get_bottom_selected();

                            })
                            // create the instance
                            .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            registerTabContent: function (tabId, mdlcode) {
            },

            getData: function () {
                var dataAccess = { ListAccess: _dataAccess };
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/SetTableAccess',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: JSON.stringify({ dataobject: JSON.stringify(dataAccess) }),
                    success: function (response) {
                        var json = JSON.parse(response.d);
                        //$tblData.clear().draw();
                        var no = 1;
                        actionTabledateFrom = [];
                        actionTabledateTo = [];
                        var content = '<font size="1" face="Arial" >';
                        content += '<table id="tblDetail" class="table table-striped table-bordered table-text-center" style="width:100%">';
                        content += '<thead>';
                        content += '<tr>';
                        content += '<th>No</th>';
                        content += '<th>Title</th>';
                        content += '<th>Access</th>';
                        content += '<th>Description</th>';
                        content += '<th>From</th>';
                        content += '<th>To</th>';
                        content += '<th>Duration</th>';
                        content += '<th>Permanent</th>';
                        content += '</tr>';
                        content += '</thead><tbody>';

                        for (var i = 0; i < json.length; i++) {
                            var idX = "permanent_" + i;
                            var idY = "dateFrom_" + i;
                            var idZ = "dateTo_" + i;
                            content += '<tr>';
                            content += '<td>' + no + '</td>';
                            content += '<td>' + json[i].pageTitle + '</td>';
                            content += '<td>' + json[i].aObjectName + '</td>';
                            content += '<td>' + json[i].description + '</td>';
                            content += '<td><input class="form-control dateFrom" id="dateFrom' + '_' + i + '" onchange="javascript:changedateFrom(\'' + idY + '\')" type="text" style="width:auto"></td>';
                            content += '<td><input class="form-control dateTo" id="dateTo' + '_' + i + '" onchange="javascript:changedateTo(\'' + idZ + '\')" type="text" style="width:auto" ></td>';
                            content += '<td><input class="difDate" type="text" id="difDate' + '_' + i + '" style="width: 20px;"></td>';
                            if (json[i].aObjectName == 'Li6') {
                                content += '<td><center><input class="permanent" onclick="javascript:disableDate(\'' + idX + '\')" type="checkbox" id="' + idX + '"></center></td>';
                            }
                            else {
                                content += '<td><center><input class="permanent" onclick="javascript:disableDate(\'' + idX + '\')" type="checkbox" id="' + idX + '" disabled="disabled"></center></td>';
                            }

                            actionTablePermanent.push(idX);
                            //actionTabledateTo.push(idY);
                            no++;
                        }
                        content += '</tbody>';
                        content += '</table></font>';

                        $("#tableAddDuration").html(content);

                        $('.dateFrom').datepicker({
                            multidate: true
                        });
                        $('.dateTo').datepicker({
                            multidate: false
                        });
                        //for (var i = 0 ; i < actionTablePermanent.length; i++) {
                        //    $("#" + actionTablePermanent[i]).click(function () {
                        //        $('#dateTo_' + i).attr('disabled', 'disabled')
                        //    });
                        //}


                        //for (var j = 0 ; j < actionTabledateTo.length; j++) {
                        //    $("#" + actionTabledateTo[j]).change(function () {
                        //        if (actionTabledateTo[i] == '#dateTo_' + i) {
                        //            if ($('#dateFrom_' + i).val() > $('#dateTo_' + i).val()) {
                        //                alert('yeay');
                        //            }
                        //            else {
                        //                alert('ha');
                        //            }
                        //            //NIK = $(this).parent().siblings().eq(1).text();
                        //            //var datas = { UserID: UserID, NIK: NIK };
                        //            //alert('yeay');
                        //        }
                        //    });
                        //}
                        //$tblData.DataTable({
                        //    //dom: 'Blfrtip',
                        //    lengthChange: false,
                        //    paging: false,
                        //    scrollX: true,
                        //    scrollY: "500px"
                        //})

                        $(".loading").hide();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("function getData : " + xhr.statusText);
                        $(".loading").hide();
                    }
                });
            },
            //getEmailUser: function () {
            //    var _parent = this;
            //    $.ajax({
            //        type: 'GET',
            //        url: '../../webservice/WebService_COR.asmx/GetEmailUser',
            //        contentType: 'application/json; charset=utf-8',
            //        dataType: 'json',
            //        success: function (response) {
            //            var json = $.parseJSON(response.d);
            //            json.forEach(function (obj) {
            //                _listEmail.push(obj.UserEmail);
            //            })
            //            console.info(_listEmail,'')
            //        },
            //        error: function (xhr, ajaxOptions, thrownError) {
            //        }
            //    });
            //},
            saveUser: function (_dataTable, _dataTree, _dataSubSection, _dataTableSub, _dataAfdeling, _dataAccess2, _dataAccessPabrik) {
                $.confirm({
                    title: 'Add User',
                    content: 'Apakah anda ingin menyimpan data ini?',
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-blue',
                            action: function () {
                                var email = $('input[name=email]').val();
                                var streetaddress = email.substr(0, email.indexOf('@'));
                                var data = {
                                    userFullName: $('input[name=name]').val(), userUnit: $('input[name=unit]').val(),
                                    usrGroupCode: $('#SelectGroup').val(), UserGender: $('#SelectJK').val(),
                                    DeptCode: $('#SelectDept').val(), UserEmail: $('input[name=email]').val(),
                                    SectionCode: $('#SelectSection').val(), UserOfficeAddr: $('input[name=alkantor]').val(),
                                    UserHP: $('input[name=telp]').val(), UserName: streetaddress, ListmdlAccess: _dataTable,
                                    ListmdlEstAccess: _dataTree, ListSubSection: _dataSubSection, ListSubModule: _dataTableSub,
                                    ListAfdeling: _dataAfdeling, ListAccess2: _dataAccess2, ListAccessPabrik: _dataAccessPabrik
                                };
                                if (_dataTree != "") {
                                    $.ajax({
                                        type: 'POST',
                                        url: '../../webservice/WebService_COR.asmx/SaveUser',
                                        data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                                        contentType: 'application/json; charset=utf-8',
                                        dataType: 'json',
                                        success: function (response) {
                                            $saveButton.html("Save");
                                            if (response.d == 'success') {
                                                $(_formAdd + " :input").prop("disabled", false);
                                                $table.DataTable().ajax.reload();

                                                _setCustomFunctions.showPopup('Info', 'User has been saved successfully');
                                                //
                                                $("#gridUser").show();
                                                $("#addUser").hide();
                                                $table.DataTable().ajax.reload();
                                                $tableOrdinary.DataTable().ajax.reload();
                                                $tableSurveyor.DataTable().ajax.reload();
                                                $tableInactive.DataTable().ajax.reload();
                                            } else {
                                                $(_formAdd + " :input").prop("disabled", false);
                                                _setCustomFunctions.showPopup('Info', 'Failed to save');
                                            }
                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {
                                        }
                                    });
                                }
                                else {
                                    alert("Choose Estate Access First");
                                }
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-red',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            },
            SaveEditUser: function (_dataTable, _dataTree, _dataSubSection, _dataTableSub, _dataAfdeling, _dataAccess2, _dataAccessPabrik) {
                _setCustomFunctions.AccessByGroupCode();
                var data = {
                    userFullName: $('input[name=name]').val(), userUnit: $('input[name=unit]').val(),
                    usrGroupCode: $('#SelectGroup').val(), UserGender: $('#SelectJK').val(),
                    DeptCode: $('#SelectDept').val(), SectionCode: $('#SelectSection').val(),
                    userID: $('input[name=userid]').val(), UserOfficeAddr: $('input[name=alkantor]').val(),
                    UserHP: $('input[name=telp]').val(), UserEmail: $('input[name=email]').val(),
                    //SubSectionCode: $('#SelectSubSection').val(),
                    isLeader: $('#isleader').is(':checked'), ListmdlAccess: _dataTable,
                    ListmdlEstAccess: _dataTree, ListSubSection: _dataSubSection, ListSubModule: _dataTableSub,
                    ListAfdeling: _dataAfdeling, ListAccess2: _dataAccess2, ListAccessPabrik: _dataAccessPabrik
                };
                _userID = $('input[name=userid]').val()
                $.confirm({
                    title: 'Edit User',
                    content: 'Apakah anda yakin untuk menyimpan data ini?',
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-blue',
                            action: function () {
                                $.ajax({
                                    type: 'POST',
                                    url: '../../webservice/WebService_COR.asmx/UpdateUser',
                                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                                    contentType: 'application/json; charset=utf-8',
                                    dataType: 'json',
                                    success: function (response) {
                                        $saveButton.html("Save");
                                        if (response.d == 'success') {
                                            $(_formAdd + " :input").prop("disabled", false);
                                            _dataAccess2 = []
                                            $table.DataTable().ajax.reload();

                                            _setCustomFunctions.showPopup('Info', 'User has been saved successfully');
                                            //
                                            $("#gridUser").show();
                                            $("#addUser").hide();
                                            $table.DataTable().ajax.reload();
                                            $tableOrdinary.DataTable().ajax.reload();
                                            $tableSurveyor.DataTable().ajax.reload();
                                            $tableInactive.DataTable().ajax.reload();
                                            _setCustomFunctions.viewTreeAccess(_userid);
                                        } else {
                                            $(_formAdd + " :input").prop("disabled", false);
                                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                                        }
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                    }
                                });
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-red',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }

                    }
                });

            },
            DeleteUser: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteUser',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $tableOrdinary.DataTable().ajax.reload();
                            $tableSurveyor.DataTable().ajax.reload();
                            $tableInactive.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'User has been Delete successfully');
                            _setCustomFunctions.viewTreeAccess(_userid);
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            SetActiveUser: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SetActiveUser',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $tableOrdinary.DataTable().ajax.reload();
                            $tableSurveyor.DataTable().ajax.reload();
                            $tableInactive.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'User has been set active successfully');
                            _setCustomFunctions.viewTreeAccess(_userid);
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Set Active');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteUserInactive: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteInactiveUser',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $tableOrdinary.DataTable().ajax.reload();
                            $tableSurveyor.DataTable().ajax.reload();
                            $tableInactive.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'User has been Delete successfully');
                            _setCustomFunctions.viewTreeAccess(_userid);
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            ClearDataAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/ClearAccessUser',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $tableOrdinary.DataTable().ajax.reload();
                            $tableSurveyor.DataTable().ajax.reload();
                            $tableInactive.DataTable().ajax.reload();
                            $tableUserWebObject.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'User has been Delete successfully');
                            _setCustomFunctions.viewTreeAccess(_userid);
                            paramini = 0;
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            AccessByGroupCode: function () {
                if (_usrGroupCode == "SUB") {
                    $('#SelectGroup').attr('disabled', 'disabled');
                    $('#SelectJK').attr('disabled', 'disabled');
                    $('#SelectDept').attr('disabled', 'disabled');
                    $('#SelectSection').attr('disabled', 'disabled');
                    $('#tickUpdate').attr('disabled', 'disabled');
                    document.getElementById('tblusers_wrapper').style.display = 'none';
                    document.getElementById('tvestate').style.display = 'none';
                    document.getElementById('SubSection').style.display = 'none';
                    document.getElementById('addEdit').style.display = 'none';
                    document.getElementById('EditAccessPabrik').style.display = 'none';
                    $('#input[name=userid]').prop('disabled', true);
                    $('#name').prop('disabled', true);
                    $('#email').prop('disabled', true);
                    $('#dept').prop('disabled', true);
                    $('#unit').prop('disabled', true);
                    $('#telp').prop('disabled', true);
                    $('#alkantor').prop('disabled', true);
                    $('#jtitle').prop('disabled', true);
                    $('#jgrade').prop('disabled', true);
                }
                else {
                    $('#SelectDept').attr('disabled', false);
                    $('#SelectGroup').attr('disabled', false);
                    $('#SelectJK').attr('disabled', false);
                    $('#SelectSection').attr('disabled', false);
                    $('#input[name=userid]').attr('disabled', false);
                    $('#input[name=name]').attr('disabled', false);
                    $('#input[name=email]').attr('disabled', false);
                    $('#input[name=uname]').attr('disabled', false);
                    $('#input[name=dept]').attr('disabled', false);
                    $('#input[name=unit]').attr('disabled', false);
                    $('#input[name=telp]').attr('disabled', false);
                    $('#input[name=alkantor]').attr('disabled', false);
                    $('#input[name=jtitle]').attr('disabled', false);
                    $('#input[name=jgrade]').attr('disabled', false);
                }
            },
            showConfirmPopup: function (param, data, title, content) {
                if (param == 1) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeleteUser(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else if (param == 2) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //_setEvents.save();
                                    document.getElementById('btnSave').disabled = false;
                                    _global = 0;
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-red',
                                action: function () {
                                    //$('#btnSave').prop('disabled', false);
                                    //$modalAdd.modal('toggle');
                                    document.getElementById('btnSave').disabled = true;
                                    _global = 1;
                                }
                            }
                        }
                    });
                }
                else if (param == 3) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.SetActiveUser(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else if (param == 4) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeleteUserInactive(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else if (param == 5) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.ClearDataAccess(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }


            },
            checkUserName: function () {
                var Email = $('#email').val()

                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetUserData',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'Email' :'" + Email + "'}",
                    dataType: 'json',
                    success: function (response) {
                        document.getElementById('errormsg').style.display = 'none';
                        var json = $.parseJSON(response.d);
                        json.forEach(function (obj) {
                            $('input[name=telp]').val(obj.NoHP);
                            $('input[name=name]').val(obj.Nama);
                            if (obj.Email == "") {
                                var email = $('input[name=email]').val();
                                $('input[name=uname]').val(email.substr(0, email.indexOf('@')));
                            }
                            $('input[name=uname]').val(obj.Email.substr(0, obj.Email.indexOf('@')));

                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
        }

    }

    Page.initialize();
}


//function EditUser(userid) {
//    $.ajax({
//        type: "GET",
//        dataType: "json",
//        url: "../../webservice/WebService_COR.asmx/GetUserDetailWithEnableSession",
//        data: { UserID: JSON.stringify(userid) },
//        contentType: "application/json; charset=utf-8",
//        success: function (response) {
//            console.log(response.d);
//        }
//    });
//}

function ac(_s) {
    $("#name").autocomplete({
        source: function (request, response) {
            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(_uname, function (item) {
                return matcher.test(item);
            }));
        }
    });
};

function test() {
    //alert($('input[name=name]').val());
    var _name = $('input[name=name]').val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../../webservice/WebService_COR.asmx/GetDataFromUserJoinIplas",
        data: { UserName: JSON.stringify(_name) },
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var json = $.parseJSON(response.d);
            json.forEach(function (obj) {
                $('input[name=telp]').val(obj.NoHP);
                if (obj.Email != null) {
                    $('input[name=email]').val(obj.Email);
                }
                else {
                    alert("Email for " + _name + " is Not Register in UserJoinIPLAS");
                }


            });
        }
    });
};


function disableDate(aye) {
    var checkBox = document.getElementById(aye);
    var split = []
    split = aye.split("_");
    //$('#' + aye).val();
    if (checkBox.checked == true) {
        const firstDate = new Date('2000-01-01');
        const secondDate = new Date('9999-01-01');
        $('#dateFrom_' + split[1]).val('2000-01-01');
        $('#dateFrom_' + split[1]).attr('disabled', 'disabled');
        $('#difDate_' + split[1]).attr('disabled', 'disabled');
        $('#difDate_' + split[1]).val('');
        $('#dateTo_' + split[1]).val('9999-01-01');
        $('#dateTo_' + split[1]).attr('disabled', 'disabled');
    } else {
        $('#dateFrom_' + split[1]).attr('disabled', false);
        $('#dateFrom_' + split[1]).datepicker('setDate', null);
        $('#difDate_' + split[1]).attr('disabled', false);
        $('#difDate_' + split[1]).val('');
        $('#dateTo_' + split[1]).attr('disabled', false);
        $('#dateTo_' + split[1]).datepicker('setDate', null);
    }
}

function changedateFrom(ba) {
    var split = []
    var split2 = []
    split = ba.split("_");
    if ($('#' + ba).val().length > 10) {
        $('#dateTo_' + split[1]).attr('disabled', 'disabled');
        $('#dateTo_' + split[1]).val('');
        $('#difDate_' + split[1]).attr('disabled', 'disabled');
        $('#difDate_' + split[1]).val('');
        split2 = $('#' + ba).val().split(",");
        $('#difDate_' + split[1]).val(split2.length);

    }
    else {
        const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        const firstDate = new Date($('#dateFrom_' + split[1]).val());
        const secondDate = new Date($('#dateTo_' + split[1]).val());
        const diffDays = Math.round(Math.round((secondDate - firstDate) / oneDay));
        $('#difDate_' + split[1]).val(diffDays);
    }
    $('#dateFrom_' + split[1]).val();
}

function changedateTo(bca) {
    var split = []
    split = bca.split("_");
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    const firstDate = new Date($('#dateFrom_' + split[1]).val());
    const secondDate = new Date($('#dateTo_' + split[1]).val());
    const diffDays = Math.round(Math.round((secondDate - firstDate) / oneDay));
    $('#dateTo_' + split[1]).val();
    $('#difDate_' + split[1]).val(diffDays);

}