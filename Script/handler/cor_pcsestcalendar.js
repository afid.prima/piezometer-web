﻿$(function () {
    var events;
    var events2;
    var _month;
    var _year;
    var _param = 1;
    var Page = {
        options: {
            formAdd: '#formAddHoliday',
            saveButton: '#btnAddHoliday',
            DeleteButton: '#btnDeleteHoliday',
            UploadButton: '#btnUploadHoliday',
            excelImport: '#excelimport',
            modalAdd: '#mdlAddHoliday',
            modalImport: '#mdlImportHoliday',
        },
        initialize: function () {
            this.setVars();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $saveButton = $(this.options.saveButton);
            $DeleteButton = $(this.options.DeleteButton);
            $UploadButton = $(this.options.UploadButton);
            $excelImport = $(this.options.excelImport);
            $modalAdd = $(this.options.modalAdd);
            $modalImport = $(this.options.modalImport);
            _formAdd = this.options.formAdd;
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setEvents: {
            init: function () {
                $saveButton.on('click', this.save);
                $DeleteButton.on('click', this.delete);
                $UploadButton.on('click', this.upload);
                $('#selectFilterEstate').on('change', this.onchangeEstcode);
                //$excelImport.on('click', this.excelImport);

                //select event
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        totDays: {
                            minlength: 1,
                            required: true
                        },
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            add: function () {
                //Event for adding data
                //var totDays = document.getElementById("totDays").value = 1;
                //totDays.setAttribute("value", "1");
                //$('input#totDays').val("1");
                //$('input[name="totDays"]').val() = "1";
                //$('input[name="totDays"]').val("1");
                //$modalAdd.modal('toggle');
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';

                _setEvents.formvalidation();

                //var totDays = document.getElementById("totDays").value = 0;

            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = {
                        startDate: $('input[name=date]').val(), totDays: $('input[name=totDays]').val(),
                        DayType: $("#selectDayType").val(), ListEstCode: $("#selectEstcode").val(),
                        description :  $("#description").val()};
                    //var savetype = 
                    _setCustomFunctions.saveHoliday(data);
                    console.log(data)
                    return false;
                }
            },
            delete: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { Date: $('input[name=date]').val(), DayType: $("#selectDayType").val() };
                    //var savetype = 
                    _setCustomFunctions.deleteHoliday(data);

                    return false;
                }
            },
            upload: function () {
                var filename2 = $('#excelimport').val();
                var filename = $('#excelimport').get(0);
                var files = filename.files;
                //UploadFile(files)
                _setCustomFunctions.importHoliday(files, "excelimport");
            },
            download: function () {

                _setCustomFunctions.downloadTemplate();
            },
            onchangeEstcode: function () {
                var mon = $('.fc-left')[0].innerText;
                var d = Date.parse(mon.substring(0, mon.length - 5) + "1, 2012");
                var mnth = mon.split(" ")
                if (mnth[0] == 'January') {
                    _month = 1;
                }
                else if (mnth[0] == 'February') {
                    _month = 2;
                }
                else if (mnth[0] == 'March') {
                    _month = 3;
                }
                else if (mnth[0] == 'April') {
                    _month = 4;
                }
                else if (mnth[0] == 'May') {
                    _month = 5;
                }
                else if (mnth[0] == 'June') {
                    _month = 6;
                }
                else if (mnth[0] == 'July') {
                    _month = 7;
                }
                else if (mnth[0] == 'August') {
                    _month = 8;
                }
                else if (mnth[0] == 'September') {
                    _month = 9;
                }
                else if (mnth[0] == 'October') {
                    _month = 10;
                }
                else if (mnth[0] == 'November') {
                    _month = 11;
                }
                else if (mnth[0] == 'Desember') {
                    _month = 12;
                }
                //_month = (new Date(d).getMonth() == 0 ? new Date(d).getMonth() + 1 : new Date(d).getMonth());
                _year = mon.split(" ").pop();
                _setCustomFunctions.getDate(1, $("#selectFilterEstate").val())

            },
        },
        setCustomFunctions: {
            init: function () {
                this.getEstate();
                this.getDate(1, "");
                this.getCalendar();
            },
            registerTabContent: function (tabId, mdlcode) {
            },
            getEstate: function () {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetEstateNew',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var jsonData = JSON.parse(response.d);
                        //$("#selectEstcode").append($('<option>', {
                        //    value: 0,
                        //    text: '-- Choose Estate --',
                        //}));
                        $("#selectFilterEstate").append($('<option>', {
                            value: 0,
                            text: '-- Choose Estate --',
                        }));
                        for (var i = 0; i < jsonData.length; i++) {
                            $("#selectFilterEstate").append($('<option>', {
                                value: jsonData[i]["EstCode"],
                                text: jsonData[i]["NewEstName"]
                            }));
                        }


                        for (var i = 0; i < jsonData.length; i++) {
                            $("#selectEstcode").append($('<option>', {
                                value: jsonData[i]["EstCode"],
                                text: jsonData[i]["NewEstName"]
                            }));
                        }


                        $('.selectpicker').selectpicker('refresh');
                    }
                });
            },
            getDate: function (_param, _estCode) {

                var data;
                var _d = new Date();
                if (_param == 1) {
                    //_month = _d.getMonth() + 1;
                    //_year = _d.getFullYear();
                }


                data = { Month: _month, Year: _year, EstCode: (_estCode == "" ? "" : _estCode) };

                //var oldevents = localStorage.getItem("events");
                //console.log(oldevents)
                //$('#calendarestate').fullCalendar('removeEventSource', events);
                //$('#calendarestate').fullCalendar('refetchEvents');
                $('#calendarestate').fullCalendar('removeEvents');
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetDataHolidayEstate',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    dataType: 'json',
                    success: function (response) {
                        var jsonData = JSON.parse(response.d);
                        //var date = $('#calendarestate').fullCalendar('getDate');
                        //var month_integer = date.getMonth();
                        //alert(month_integer);
                        events = jsonData;
                        $("#calendarestate").fullCalendar('removeEvents');
                        $('#calendarestate').fullCalendar('addEventSource', events);
                        //$('#calendarestate').fullCalendar('refetchEvents');
                        $("#calendarestate").fullCalendar('rerenderEvents');
                    }
                });
            },
            getCalendar: function () {
                $('#calendarestate').fullCalendar({
                    // put your options and callbacks here
                    //customButtons: {
                    //    add_event: {
                    //        text: 'Download Template',
                    //        click: function () {
                    //            _setEvents.download();
                    //        }
                    //    },
                    //    add_event2: {
                    //        text: 'Import dari Excel',
                    //        click: function () {
                    //            $modalImport.modal('toggle');
                    //        }
                    //    }
                    //},
                    header: {
                        left: 'title',
                        //center: 'prev,next today',
                        right: 'prev,next add_event add_event2'
                    },
                    dayClick: function (date, jsEvent, view) {
                        $("#selectEstcode").val(0)
                        $("#description").val("")
                        $("#selectDayType").val()
                        var date = new Date(date._d);
                        //alert(((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear());
                        var dateclicked = ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + date.getFullYear()

                        data = { Date: dateclicked, Estcode: $("#selectFilterEstate").val() };
                        $.ajax({
                            type: 'POST',
                            url: '../../webservice/WebService_COR.asmx/GetDetailDataHolidayEstate',
                            contentType: 'application/json; charset=utf-8',
                            data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                            dataType: 'json',
                            success: function (response) {
                                var jsonData = JSON.parse(response.d);
                                if (jsonData[0].param == "false") {
                                    $('input[name=date]').val(dateclicked);
                                    $("#selectEstcode").val(0);
                                    $("#description").val("");
                                    $("#selectDayType").val("");
                                    document.getElementById('btnDeleteHoliday').style.display = 'none';
                                    document.getElementById('lblTotDays').style.display = 'inline';
                                    document.getElementById('btnAddHoliday').style.display = 'inline';
                                    document.getElementById('lblHoliday').style.display = 'none';
                                    document.getElementById('lblRestday').style.display = 'none';
                                    document.getElementById('lblNormalday').style.display = 'none';
                                    $('.selectpicker').selectpicker('refresh');
                                }
                                else {
                                    $('input[name=date]').val(jsonData[0].Holiday_Date);
                                    $.each(jsonData[0].estCode.split(","), function (i, e) {
                                        $("#selectEstcode option[value='" + e + "']").prop("selected", true);
                                    });
                                    //$("#selectEstcode").val(jsonData[0].estCode);
                                    $("#description").val(jsonData[0].description);
                                    $("#selectDayType").val(jsonData[0].dayType);
                                    document.getElementById('btnDeleteHoliday').style.display = 'inline';
                                    document.getElementById('lblTotDays').style.display = 'none';
                                    document.getElementById('btnAddHoliday').style.display = 'none';
                                    if (jsonData[0].dayType == 1) {
                                        document.getElementById('lblNormalday').style.display = 'inline';
                                        document.getElementById('lblRestday').style.display = 'none';
                                        document.getElementById('lblHoliday').style.display = 'none';
                                    }
                                    else if (jsonData[0].dayType == 2) {
                                        document.getElementById('lblRestday').style.display = 'inline';
                                        document.getElementById('lblNormalday').style.display = 'none';
                                        document.getElementById('lblHoliday').style.display = 'none';
                                    }
                                    else if (jsonData[0].dayType == 3) {
                                        document.getElementById('lblHoliday').style.display = 'inline';
                                        document.getElementById('lblRestday').style.display = 'none';
                                        document.getElementById('lblNormalday').style.display = 'none';
                                    }
                                    else {
                                        document.getElementById('lblHoliday').style.display = 'none';
                                        document.getElementById('lblRestday').style.display = 'none';
                                        document.getElementById('lblNormalday').style.display = 'none';
                                    }
                                    
                                    $('.selectpicker').selectpicker('refresh');
                                }
                            }
                        });

                        $('#totDays').val("1");
                        $modalAdd.modal('toggle');


                    },

                    editable: true,
                    eventLimit: true,
                    eventClick: function (info) {
                        info.jsEvent.preventDefault(); // don't let the browser   navigate

                        if (info.event.url) {
                            window.open(info.event.url);
                        }
                    }
                })
                //var mon = $('.fc-left')[0].innerText;
                //var d = Date.parse(mon.substring(0, mon.length - 5) + "1, 2012");
                //if (!isNaN(d)) {
                //    //return new Date(d).getMonth() + 1;
                //    //alert(new Date(d).getMonth() + 1);
                //    _month = new Date(d).getMonth() + 1;
                //    var _y = new Date();
                //    _year = _y.getFullYear();
                //}
                //return -1;

                $('.fc-prev-button').click(function () {
                    var prevDate = $("#calendarestate").fullCalendar('getDate').toDate();
                    //alert(prevDate);
                    var mon = $('.fc-left')[0].innerText;
                    var d = Date.parse(mon.substring(0, mon.length - 5) + "1, 2012");
                    var mnth = mon.split(" ")
                    if (mnth[0] == 'January') {
                        _month = 1;
                    }
                    else if (mnth[0] == 'February') {
                        _month = 2;
                    }
                    else if (mnth[0] == 'March') {
                        _month = 3;
                    }
                    else if (mnth[0] == 'April') {
                        _month = 4;
                    }
                    else if (mnth[0] == 'May') {
                        _month = 5;
                    }
                    else if (mnth[0] == 'June') {
                        _month = 6;
                    }
                    else if (mnth[0] == 'July') {
                        _month = 7;
                    }
                    else if (mnth[0] == 'August') {
                        _month = 8;
                    }
                    else if (mnth[0] == 'September') {
                        _month = 9;
                    }
                    else if (mnth[0] == 'October') {
                        _month = 10;
                    }
                    else if (mnth[0] == 'November') {
                        _month = 11;
                    }
                    else if (mnth[0] == 'Desember') {
                        _month = 12;
                    }
                    //_month = (new Date(d).getMonth() == 0 ? new Date(d).getMonth() + 1 : new Date(d).getMonth());
                    _year = mon.split(" ").pop();
                    _param = 2;
                    _setCustomFunctions.getDate(_param, $("#selectFilterEstate").val());
                });

                $('.fc-next-button').click(function () {
                    var nextMonthDate = $("#calendarestate").fullCalendar('getDate').toDate();
                    //alert(nextMonthDate);
                    var mon = $('.fc-left')[0].innerText;
                    //var d = Date.parse(mon.substring(0, mon.length - 5) + "1, 2012");
                    var mnth = mon.split(" ")
                    if (mnth[0] == 'January') {
                        _month = 1;
                    }
                    else if (mnth[0] == 'February') {
                        _month = 2;
                    }
                    else if (mnth[0] == 'March') {
                        _month = 3;
                    }
                    else if (mnth[0] == 'April') {
                        _month = 4;
                    }
                    else if (mnth[0] == 'May') {
                        _month = 5;
                    }
                    else if (mnth[0] == 'June') {
                        _month = 6;
                    }
                    else if (mnth[0] == 'July') {
                        _month = 7;
                    }
                    else if (mnth[0] == 'August') {
                        _month = 8;
                    }
                    else if (mnth[0] == 'September') {
                        _month = 9;
                    }
                    else if (mnth[0] == 'October') {
                        _month = 10;
                    }
                    else if (mnth[0] == 'November') {
                        _month = 11;
                    }
                    else if (mnth[0] == 'Desember') {
                        _month = 12;
                    }
                    //_month = new Date(d).getMonth() + 1;
                    _year = mon.split(" ").pop();
                    _param = 2;
                    _setCustomFunctions.getDate(_param, $("#selectFilterEstate").val());
                });
            },
            saveHoliday: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/saveHolidayEstate',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            //$table.DataTable().ajax.reload();
                            _setCustomFunctions.getDate(2, $("#selectFilterEstate").val());
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Hari libur berhasil disimpan');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Gagal Menyimpan Data');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            deleteHoliday: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/deleteHolidayEstate',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            //$table.DataTable().ajax.reload();
                            _setCustomFunctions.getDate(2, $("#selectFilterEstate").val());
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Hari libur berhasil dihapus');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Gagal Menyimpan Data');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            importHoliday: function (files, typeLaporan) {
                var data = new FormData();
                for (var i = 0; i < files.length; i++) {
                    data.append(files[i].name, files[i]);

                }
                $.ajax({
                    type: 'POST',
                    url: '../../Service/ImportHolidayHandler.ashx',
                    data: data,
                    contentType: false,
                    processData: false,

                    success: function (response) {
                        alert('import data excel sukses');
                    }

                });
            },

            downloadTemplate: function () {
                window.location.href = '../../Service/HolidayTemplate.ashx'
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.DeleteCountry(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
});
