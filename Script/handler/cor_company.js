﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddCompany',
            saveButton: '#btnSave',
            UpdateButton: '#btnUpdate',
            modalAdd: '#mdlAdd',
            tabCompany: '#tabCompany',
            tableactive: '#tblactivecompany',
            tableinactive: '#tblinactivecompany',
        },
        initialize: function () {
            this.setVars();
            this.setTableActive();
            this.setTableInactive();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $UpdateButton = $(this.options.UpdateButton);
            $modalAdd = $(this.options.modalAdd);
            $tabCompany = $(this.options.tabCompany);
            $tableactive = $(this.options.tableactive);
            $tableinactive = $(this.options.tableinactive);
            _formAdd = this.options.formAdd;
            _tabCompany = this.options.tabCompany;
            _listCompany = [];
            _listCompanyCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTableActive: function () {
            //Initialization of main table or data here
            var tableactive = $tableactive.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 7,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 8,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetCompanyActive_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblactivecompany").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setTableInactive: function () {
            //Initialization of main table or data here
            var tableinactive = $tableinactive.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 7,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 8,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetCompanyInActive_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblinactivecompany").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $tableactive.on('click', 'a[data-target="#edit"]', this.edit);
                $tableactive.on('click', 'a[data-target="#delete"]', this.delete);

                $tableinactive.on('click', 'a[data-target="#edit"]', this.edit);
                $tableinactive.on('click', 'a[data-target="#delete"]', this.delete);

                $tabCompany.on('click', 'a', this.tabclick);
                //this.formvalidation();

                //select event
                $('#selectType').on('change', this.selectchange);
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 1,
                            required: true,
                                remote: function () {
                                    //document.getElementById('errormsg').style.display = 'none';
                                    return {
                                        url: "../../webservice/WebService_COR.asmx/GetMasterCompanybyCompanyCode",
                                        type: "POST",
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        data: "{'CompanyCode' :'" + $('input[name=code]').val() + "'}",
                                        dataFilter: function (data) {
                                            var msg = JSON.parse(data);
                                            var json = $.parseJSON(msg.d);
                                            if (json.length > 0) {
                                                //{
                                                document.getElementById('lblcode').style.color = '#dd4b39';
                                                document.getElementById('code').style.borderColor = '#dd4b39';
                                                //document.getElementById('help-block').style.display = 'block';
                                                document.getElementById('errormsg').style.display = 'block';
                                                document.all('errormsg').innerHTML = "Code sudah terdaftar pada sistem";
                                            }
                                        }
                                    }


                                },
                        },
                        name: {
                            minlength: 1,
                            required: true
                        },
                        shortname: {
                            minlength: 1,
                            required: true
                        },
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                //e.preventDefault();

            },
            tabchange: function (tabId) {


            },
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                console.log("ada");
                //Event for adding data
                $modalAdd.modal('toggle');
                document.getElementById('lblcode').style.color = '#333';
                document.getElementById('code').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Company";

                $('#SelectRegionName').val('');
                //$('#Selectstatus').val(CompanyStatus).attr("selected", "selected");
                $('#Selectstatus').val('');

                $('input[name=code]').val('');
                $('input[name=codeHidden]').val('');
                $('input[name=name]').val('');
                $('input[name=shortname]').val('');
                $('input[name=desc]').val('');
                $('input[name=minX]').val('');
                $('input[name=minY]').val('');
                $('input[name=maxX]').val('');
                $('input[name=maxY]').val('');
                _setEvents.formvalidation();
            },
            edit: function () {
                document.getElementById('lblcode').style.color = '#333';
                document.getElementById('code').style.borderColor = '#d2d6de';
                //document.getElementById('help-block').style.display = 'block';
                document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnUpdate').style.display = 'inline';
                document.getElementById('btnSave').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Company";

                $modalAdd.modal('toggle');
                _setEvents.formvalidation();

                var $this = $(this);
                var $row = $(this).closest('tr');
                var CompanyCode = $row.find('.CompanyCode').html();
                var CompanyName = $row.find('.CompanyName').html();

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetMasterCompanybyCompanyCode',
                    type: "POST",
                    data: "{'CompanyCode' :'" + CompanyCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('#SelectRegionName').val(obj.RegionCode);
                            //$('#Selectstatus').val(CompanyStatus).attr("selected", "selected");
                            $('#Selectstatus').val(obj.CompanyStatus);

                            $('input[name=code]').val(obj.CompanyCode);
                            $('input[name=codeHidden]').val(obj.CompanyCode);
                            $('input[name=name]').val(obj.CompanyName);
                            $('input[name=shortname]').val(obj.CompanyShortname);
                            $('input[name=desc]').val(obj.CompanyDescription);
                            $('input[name=minX]').val(obj.MinX);
                            $('input[name=minY]').val(obj.MinY);
                            $('input[name=maxX]').val(obj.MaxX);
                            $('input[name=maxY]').val(obj.MaxY);

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = {
                    CompanyCode: $('input[name=codeHidden]').val(), NewCompanyCode: $('input[name=code]').val(), CompanyName: $('input[name=name]').val(), CompanyShortname: $('input[name=shortname]').val(), CompanyStatus: $('#Selectstatus').val()
                    , CompanyDescription: $('input[name=desc]').val(), MinX: $('input[name=minX]').val(), MinY: $('input[name=minY]').val(), MaxX: $('input[name=maxX]').val(), RegionCode: $('#SelectRegionName').val()
                    , MaxY: $('input[name=maxY]').val()
                };
                //var savetype = 
                _setCustomFunctions.EditCompany(data);

                return false;

            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var CompanyCode = $row.find('.CompanyCode').html();
                //Set Ajax Submit Here
                var data = { CompanyCode: CompanyCode };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = {
                        CompanyCode: $('input[name=code]').val(), CompanyName: $('input[name=name]').val(), CompanyDescription: $('input[name=desc]').val(), RegionCode: $('#SelectRegionName').val(), CompanyStatus: $('#Selectstatus').val()
                        , CompanyShortname: $('input[name=shortname]').val(), MinX: $('input[name=minX]').val(), MinY: $('input[name=minY]').val(), MaxX: $('input[name=maxX]').val(), MaxY: $('input[name=maxY]').val()
                    };
                    //var savetype = 
                    _setCustomFunctions.saveCompany(data);
                    $modalAdd.modal('hide');

                    return false;
                }
            }
        },
        setCustomFunctions: {
            init: function () {
                this.getRegion();
                //this.getMasterModuleGroup();
                //this.getMasterModuleType();
            },
            registerTabContent: function (tabId, mdlcode) {
            },
            getRegion: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterRegion',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectRegionName').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectRegionName").append($('<option>', {
                                value: obj.RegionCode,
                                text: obj.RegionCode + ' - ' + obj.RegionName
                            }));
                        })

                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            //getMasterModuleGroup: function () {

            //},
            //getMasterModuleType: function () {

            //},
            //getMasterModuleSubType: function (type) {

            //},
            saveCompany: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveCompany',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableactive.DataTable().ajax.reload();
                            $tableinactive.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Company has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteCompany: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteCompany',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableactive.DataTable().ajax.reload();
                            $tableinactive.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Company has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditCompany: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditCompany',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $UpdateButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableactive.DataTable().ajax.reload();
                            $tableinactive.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'Company has been Update successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.DeleteCompany(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
})