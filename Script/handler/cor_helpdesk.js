﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddHelpDeskType',
            saveButton: '#btnSave',
            UpdateButton: '#btnUpdate',
            modalAdd: '#mdlAdd',
            tabHelpDeskType: '#tabHelpDeskType',
            table: '#tblhelpdesktype',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $UpdateButton = $(this.options.UpdateButton);
            $modalAdd = $(this.options.modalAdd);
            $tabHelpDeskType = $(this.options.tabHelpDeskType);
            $table = $(this.options.table);
            _formAdd = this.options.formAdd;
            _tabHelpDeskType = this.options.tabHelpDeskType;
            _listHelpDeskType = [];
            _listHelpDeskTypeCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 2,
                    width: 150
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetHelpDeskType_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblhelpdesktype").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $table.on('click', 'a[data-target="#delete"]', this.delete);
                $table.on('click', 'a[data-target="#uppriority"]', this.uppriority);
                $table.on('click', 'a[data-target="#downpriority"]', this.downpriority);
                $table.on('click', 'a[data-target="#moveto"]', this.moveto);

                $tabHelpDeskType.on('click', 'a', this.tabclick);
                //this.formvalidation();

                //select event
                $('#selectType').on('change', this.selectchange);
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 2,
                            numeric: true,
                            required: true
                        },
                        name: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                //e.preventDefault();

            },
            tabchange: function (tabId) {


            },
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                $('input[name=code]').val("");
                $('input[name=codeHidden]').val("");
                $('input[name=name]').val("");
                $('input[name=prioritas]').val("");
                document.getElementById('btnUpdate').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Help Desk Type";

                _setEvents.formvalidation();
            },
            edit: function () {
                document.getElementById('btnUpdate').style.display = 'inline';
                document.getElementById('btnSave').style.display = 'none';

                $modalAdd.modal('toggle');
                _setEvents.formvalidation();

                var $this = $(this);
                var $row = $(this).closest('tr');
                //var HelpDeskTypeCode = $row.find('.HelpDeskTypeCode').html();
                var HelpDeskTypeName = $row.find('.HelpDeskTypeName').html();
                document.all('lbl').innerHTML = "Edit Help Desk Type";

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetHelpdeskData',
                    type: "POST",
                    data: "{'HelpDeskTypeName' :'" + HelpDeskTypeName + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=codeHidden]').val(obj.HelpDeskTypeName);
                            $('input[name=name]').val(obj.HelpDeskTypeName);
                            $('input[name=prioritas]').val(obj.HelpDeskTypePriority);
                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });
                //var HelpDeskTypePriority = $row.find('.HelpDeskTypePriority').html();


                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = { HelpDeskTypeName: $('input[name=codeHidden]').val(), NewHelpDeskTypeName: $('input[name=name]').val(), HelpDeskTypePriority: $('input[name=prioritas]').val() };
                //var savetype = 
                _setCustomFunctions.EditHelpDeskType(data);

                return false;



                //$("#formAdd :input").prop("disabled", true);
                ////Set Ajax Submit Here
                //var data = { CountryCode: $('input[name=codeHidden]').val(), NewCountryCode: $('input[name=code]').val(), CountryName: Count$('input[name=name]').val() };
                ////var savetype = 

                //_setCustomFunctions.EditCountry(data);

                //return false;
            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var HelpDeskTypeName = $row.find('.HelpDeskTypeName').html();
                //Set Ajax Submit Here
                var data = { HelpDeskTypeName: HelpDeskTypeName };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            uppriority: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var HelpDeskTypePriority = $row.find('.HelpDeskTypePriority').html();
                var a = parseInt(HelpDeskTypePriority)
                var HelpDeskTypePriorityUp = a + 1;
                var HelpDeskTypeName = $row.find('.HelpDeskTypeName').html();
                //Set Ajax Submit Here
                var data = { HelpDeskTypePriority: HelpDeskTypePriorityUp, HelpDeskTypeName: HelpDeskTypeName };
                //var savetype = 
                console.log("dsadasd");
                _setCustomFunctions.UpPriority(data);


                return false;
            },
            downpriority: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var HelpDeskTypePriority = $row.find('.HelpDeskTypePriority').html();
                var a = parseInt(HelpDeskTypePriority)
                var HelpDeskTypePriorityUp = a - 1;
                var HelpDeskTypeName = $row.find('.HelpDeskTypeName').html();
                //Set Ajax Submit Here
                if (HelpDeskTypePriorityUp == 0 || HelpDeskTypePriority == 0) {
                    alert('Cant Update');
                }
                else {
                    var data = { HelpDeskTypePriority: HelpDeskTypePriorityUp, HelpDeskTypeName: HelpDeskTypeName };

                    _setCustomFunctions.DownPriority(data);
                }



                return false;
            },
            moveto: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var DataPriority = $row.find("td:eq(2) input[type='text']").val();
                var HelpDeskTypeName = $row.find('.HelpDeskTypeName').html();
                //Set Ajax Submit Here
                var data = { HelpDeskTypePriority: DataPriority, HelpDeskTypeName: HelpDeskTypeName };

                _setCustomFunctions.MoveToPriority(data);


                return false;
            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { HelpDeskTypeName: $('input[name=name]').val(), HelpDeskTypePriority: $('input[name=prioritas]').val() };
                    //var savetype = 
                    _setCustomFunctions.saveHelpDeskType(data);

                    return false;
                }
            }
        },
        setCustomFunctions: {
            init: function () {
            },
            registerTabContent: function (tabId, mdlcode) {
            },
            saveHelpDeskType: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveHelpDeskType',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Help Desk Type has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteHelpDeskType: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteHelpDeskType',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Help Desk Type has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditHelpDeskType: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditHelpDeskType',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $UpdateButton.html("Update");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Help Desk Type has been Update successfully');
                            $modalAdd.modal('hide');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            MoveToPriority: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/MovePriority',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            //_setCustomFunctions.showPopup('Info', 'Update Priority has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });

            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.DeleteHelpDeskType(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
})