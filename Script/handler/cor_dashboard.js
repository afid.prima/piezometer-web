﻿$(function () {
    $.ajax({
        type: 'GET',
        url: '../../webservice/WebService_COR.asmx/GetDataDashboard',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            json.forEach(function (obj) {
                //document.all('login').innerHTML = obj.login;
                document.all('euclid').innerHTML = obj.EuclidUser;
                document.all('activeuser').innerHTML = obj.EntUser;
                document.getElementById('dataeuclid').innerHTML = obj.EuclidInEnt;
                document.getElementById('datanotregist').innerHTML = (obj.EuclidUser - obj.EuclidInEnt);
                document.all('datauseractive').innerHTML = obj.ActiveEnt;
                document.all('datauserinactive').innerHTML = obj.InactiveEnt;
                document.all('mobileuser').innerHTML = obj.MobileUser;
                document.all('mobileuseractive').innerHTML = obj.ActiveMobileUser;
                document.all('datacollector').innerHTML = obj.boncengan;
                document.all('notineuclid').innerHTML = obj.EntNotInEuclid;
            })
        }
    });

    $('#detaileuclid').on('click', mdlEuclid);
    $('#detailenterprise').on('click', mdlEnterprise);
    $('#detailmobile').on('click', mdlMobile);

    function mdlEuclid() {
        //alert('test');
        $('#mdlEuclid').modal('toggle');
        setTable(1);
    }

    function mdlEnterprise() {
        //alert('test');
        $('#mdlEnterprise').modal('toggle');
        setTable(2);
    }
    function mdlMobile() {
        //alert('test');
        $('#mdlMobile').modal('toggle');
        setTable(3);
    }

    function setTable(_var) {
        //Initialization of main table or data here
        if (_var = 1) {
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                    width: 400
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetEuclid_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblcountry").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        }

    }
    //$.ajax({
    //    type: 'GET',
    //    url: '../../webservice/WebService_COR.asmx/GetUserActivePerDepartment',
    //    contentType: 'application/json; charset=utf-8',
    //    dataType: 'json',
    //    success: function (response) {
    //        var json = $.parseJSON(response.d);
    //        var content = "";
    //        content += "<div style=background-color:#E65100;color:white> User Per Department</div><br>";
    //        var _no = 1;
    //        var _bgcolor = "";
    //        json.forEach(function (obj) {
    //            if (_no % 2 == 0) {
    //                _bgcolor = 'style=background-color:#FFE0B2';
    //            }
    //            else {
    //                _bgcolor = 'style=background-color:#FFCC80';
    //            }
    //            content += '<div class="col-md-12 contentdropdown"' + _bgcolor + ' > <div class="col-md-10">' + obj.DeptName + '</div> <div class="col-md-2">' + obj.TotalUser + '</div></div>';


    //            _no++;

    //        })
    //        document.getElementById('dataactive').innerHTML = content;
    //    }
    //});

    $.ajax({
        type: 'GET',
        url: '../../webservice/WebService_COR.asmx/GetUserActivePerDepartment',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            //console.info("dataAwal", JSON.stringify(json));
            var array = [];
            var _year;
            var _n = 100;
            var item = [];
            for (i = 0; i < json.length; i++) {
                item.push({ label: json[i].DeptName, y: json[i].TotalUser });
            }
            array.push({
                type: "column",
                dataPoints: item
            });


            var chart = new CanvasJS.Chart("chartContainerUser", {
                data: array,
                culture: "es",
                axisX: {
                    interval: 1,
                    labelAngle: -70
                },
            });
            chart.render();
        }
    });


    $.ajax({
        type: 'GET',
        url: '../../webservice/WebService_COR.asmx/GetChartUserLogin',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            //console.info("dataAwal", JSON.stringify(json));
            var array = [];
            var _year;
            var _n = 2015;
            var item = [];
            for (i = 0; i < json.length; i++) {
                item = [];
                for (j = 0; j <= 12; j++) {
                    if (j == 0) {
                        //if (_n != 2015) {
                            if (json[i].n0 != 0) {
                                item.push({ label: "Jan", y: json[i].n0 });
                            }
                        //}

                    }
                    else if (j == 1) {
                        //if (_n != 2015) {
                            if (json[i].n1 != 0)
                            { item.push({ label: "Feb", y: json[i].n1 }); }
                        //}

                    }
                    else if (j == 2) {
                        //if (_n != 2015) {
                            if (json[i].n2 != 0)
                            { item.push({ label: "Mar", y: json[i].n2 }); }
                        //}

                    }
                    else if (j == 3) {
                        if (_n != 2015) {
                            if (json[i].n3 != 0)
                            { item.push({ label: "Apr", y: json[i].n3 }); }
                        }

                    }
                    else if (j == 4) {
                        //if (_n != 2015) {
                            if (json[i].n4 != 0)
                            { item.push({ label: "May", y: json[i].n4 }); }
                        //}

                    }
                    else if (j == 5) {
                        //if (_n != 2015) {
                            if (json[i].n5 != 0)
                            { item.push({ label: "Jun", y: json[i].n5 }); }
                        //}

                    }
                    else if (j == 6) {
                        //if (_n != 2015) {
                            if (json[i].n6 != 0)
                            { item.push({ label: "Jul", y: json[i].n6 }); }
                        //}

                    }
                    else if (j == 7) {
                        //if (_n != 2015) {
                            if (json[i].n7 != 0)
                            { item.push({ label: "Aug", y: json[i].n7 }); }
                        //}

                    }
                    else if (j == 8) {
                        //if (_n != 2015) {
                            if (json[i].n8 != 0)
                            { item.push({ label: "Sep", y: json[i].n8 }); }
                        //}

                    }
                    else if (j == 9) {
                        //if (_n != 2015) {
                            if (json[i].n9 != 0)
                            { item.push({ label: "Oct", y: json[i].n9 }); }
                        //}
                    }
                    else if (j == 10) {
                        //if (_n != 2015) {
                            if (json[i].n10 != 0)
                            { item.push({ label: "Nov", y: json[i].n10 }); }
                        //}
                    }
                    else if (j == 11) {
                        //if (_n != 2015) {
                            if (json[i].n11 != 0)
                            { item.push({ label: "Dec", y: json[i].n11 }); }
                        //}
                    }
                    else {
                        break;
                    }

                }
                array.push({
                    type: "line",
                    showInLegend: true,
                    dataPoints: item,
                    //legendText: '2019' 
                    legendText: "'" + _n + "'"
                });
                _n++;
            }



            var chart = new CanvasJS.Chart("chartContainer",
{

    axisX: {
        valueFormatString: "MMM",
        labelAngle: -50
    },
    data: array,
    theme: "light2",
});


            chart.render();
        }
    });

    $.ajax({
        type: 'GET',
        url: '../../webservice/WebService_COR.asmx/GetChartUserLoginByDays',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            //console.info("dataAwal", JSON.stringify(json));
            var array = [];
            var _year;
            var _n = 1;
            var item = [];
            for (i = 0; i < json.length; i++) {
                item.push({ label: json[i].Days, y: json[i].TotalLogin });
            }
            array.push({
                type: "line",
                dataPoints: item
            });



            var charts = new CanvasJS.Chart("chartContainerDays",
{

    axisX: {
        valueFormatString: "MMM",
        labelAngle: -50
    },
    data: array,
    theme: "light2",
});


            charts.render();
        }
    });
    //var module_access_chart = new Morris.Bar({
    //    element: 'module-access-chart',
    //    data: [
    //        { y: 'Helpdesk (DSK)', a: 100 },
    //        { y: 'Resource Center (RCM)', a: 75 },
    //        { y: 'Piezometer (PZO)', a: 50 },
    //        { y: 'Water Management (WMN)', a: 75 },
    //        { y: 'Group Report (RPT)', a: 50 },
    //        { y: 'Landbank (BMI)', a: 75 },
    //    ],
    //    xkey: 'y',
    //    ykeys: ['a', 'b'],
    //    labels: ['Series A', 'Series B']
    //});


})