﻿$(function () {
    var url = new URL(window.location.href);
    var Code = url.searchParams.get("token");
    $.ajax({
        url: 'Service/MapService.asmx/CheckDateForgotPassword',
        type: "POST",
        data: "{'Code' :'" + Code + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var json = $.parseJSON(data.d);
            json.forEach(function (obj) {
                if (obj.Status == "false") {
                            var Page = {
                                options: {
                                    formVerification: '#formVerification',
                                    btnChangePassword: '#BtnChangePassword',
                                },
                                initialize: function () {
                                    this.setVars();
                                    this.setEvents.init();
                                },
                                setVars: function () {
                                    $formVerification = $(this.options.formVerification);
                                    $btnVerifikasi = $(this.options.btnChangePassword);
                                    _setEvents = this.setEvents;
                                },
                                setEvents: {
                                    init: function () {
                                        _setEvents.formvalidation();
                                        $btnVerifikasi.on('click', this.ChangePassword);
                                    },
                                    formvalidation: function () {
                                        //Main Validation
                                        $.validator.addMethod("check", function (value, element) {

                                            return this.optional(element) || $('input[name=password]').val() == $('input[name=ulangipassword]').val()

                                        }, "Kata Sandi Tidak Sama");
                                        $formVerification.validate({
                                            rules: {
                                                password: {
                                                    required: true
                                                },
                                                ulangipassword: {
                                                required: true,
                                                check: true
                                        }
                                            },
                                            highlight: function (element) {
                                                $(element).closest('.form-group').addClass('has-error');
                                            },
                                            unhighlight: function (element) {
                                                $(element).closest('.form-group').removeClass('has-error');
                                            },
                                            errorElement: 'span',
                                            errorClass: 'help-block',
                                            errorPlacement: function (error, element) {
                                                if (element.parent('.input-group').length) {
                                                    error.insertAfter(element.parent());
                                                } else {
                                                    error.insertAfter(element);
                                                }
                                            }
                                        });
                                    },
                                    resetformvalidation: function () {
                                    },
                                    ChangePassword: function (e) {
                                            e.preventDefault();
                                            var Password = $('input[name=password]').val();
                                            $.ajax({
                                                type: 'POST',
                                                url: 'Service/MapService.asmx/ChangePasswordForgot',
                                                data: "{'Password' :'" + Password + "','Code' :'" + Code + "'}",
                                                contentType: 'application/json; charset=utf-8',
                                                dataType: 'json',
                                                success: function (response) {
                                                    if (response.d == 'success') {
                                                        document.all('lblNotification').innerHTML = "Password Berhasil Dirubah, Silahkan Lakukan <a target=_blank href=Default.aspx>Login </a><br />";
                                                        document.getElementById('Password').style.display = 'none';
                                                        document.getElementById('UlangiPassword').style.display = 'none';
                                                        document.getElementById('BtnChangePassword').style.display = 'none';
                                                        //document.getElementById('lblDefaultPassword').style.display = 'none';

                                                    } else {

                                                    }
                                                },
                                                error: function (xhr, ajaxOptions, thrownError) {
                                                }
                                            });

                                        },
                                    },
                                    setCustomFunctions: {
                                        init: function () {
                                        },
                                    }

                                }

                                Page.initialize();
                }
                else {
                    if (obj.StatusUser == "1") {

                        document.getElementById('Password').style.display = 'none';
                        document.getElementById('UlangiPassword').style.display = 'none';
                        document.getElementById('BtnChangePassword').style.display = 'none';
                        //document.getElementById('lblDefaultPassword').style.display = 'none';
                        document.all('lblNotification').innerHTML = "Password Telah Di Ubah<br /> Silahkan Lakukan <a target=_blank href=default2.aspx>Login </a><br />";
                    }
                    else {
                        //document.getElementById('lblChangePassword').style.display = 'none';
                        //document.getElementById('BtnChangePassword').style.display = 'none';
                        //document.getElementById('lblDefaultPassword').style.display = 'none';
                        //document.all('lblNotification').innerHTML = "Link ini Sudah Expired, Silahkan Lakukan Kembali <a target=_blank href=register2.aspx>Registrasi</a>";
                    }


                }
            })
        },
        failure: function (msg) {
            alert("Something go wrong! ");
        }
    });
    function Test() {
        alert("test");
    }
})
