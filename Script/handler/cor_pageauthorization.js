﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnSave',
            searchButton: '#btnFind',
            SaveAccessButton: '#btnSaveAccess',
            searchDetailButton: '#btnFindDetail',
            modalAdd: '#mdlAdd',
            modalAdd2: '#mdlAdd2',
            modalAdd3: '#mdlAdd3',
            table: '#tblpageauthorization',
            tableDetail: '#tblpageauthorizationdetail',
        },
        initialize: function () {
            this.setVars();
            this.setCustomFunctions.init();
            this.setEvents.init();
            this.setTable();
            this.setTableDetail();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $tabRegion = $(this.options.tabRegion);
            $searchButton = $(this.options.searchButton);
            $searchDetailButton = $(this.options.searchDetailButton);
            $addButton = $(this.options.addButton);
            $SaveAccessButton = $(this.options.SaveAccessButton);
            $table = $(this.options.table);
            $tableDetail = $(this.options.tableDetail);
            $modalAdd = $(this.options.modalAdd);
            $modalAdd2 = $(this.options.modalAdd2);
            $modalAdd3 = $(this.options.modalAdd3);
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                destroy: true,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                    width: 100
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 2,
                    width: 300
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3,
                    width: 100
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 5,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 6,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                "bProcessing": true,
                "bServerSide": true,
                "bRetrieve": true,
                "bLengthChange": false,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetPageAccessGroup_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {

                    aoData.push({ 'name': 'mdlcode', 'value': $('#SelectModule').val() });
                    aoData.push({ 'name': 'MdlAccCode', 'value': $('#SelectModuleAccess').val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            //if (msg.d !== "")
                            //{
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            //}
                            //else
                            //{
                            //    alert('No Data');
                            //}

                            $("#tblpageauthorization").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });

        },
        setTableDetail: function () {

            //Initialization of main table or data here
            var tableDetail = $tableDetail.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                destroy: true,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 3
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 4
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                "oLanguage": {
                    "sEmptyTable": "The table is really empty now!"
                },
                "bProcessing": true,
                "bServerSide": true,
                "bRetrieve": true,
                "bLengthChange": false,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetPageDetailAccessGroup_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {


                    aoData.push({ 'name': 'PageID', 'value': $('input[name=PageID]').val() });
                    aoData.push({ 'name': 'AObjectID', 'value': $('input[name=activeobject]').val() });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            //if (msg.d !== "")
                            //{
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            //}
                            //else
                            //{
                            //    alert('No Data');
                            //}

                            $("#tblpageauthorization").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });

        },

        setEvents: {
            init: function () {
                $searchButton.on('click', this.edit);
                $searchDetailButton.on('click', this.editDetail);
                $addButton.on('click', this.save);
                $SaveAccessButton.on('click', this.saveAccess);
                $table.on('click', 'a[data-target="#set"]', this.SetAccess);
                $table.on('click', 'a[data-target="#show"]', this.ShowDetailAccess);
                $tableDetail.on('click', 'a[data-target="#setDetail"]', this.ShowDetailWebAccess);

                //select event
                $('#selectType').on('change', this.selectchange);
                $('#SelectModule').on('change', this.ChangeModule);
            },
            edit: function () {
                $table.DataTable().ajax.reload();
            },
            save: function () {

                //Set Ajax Submit Here
                var data = { PageID: $('input[name=code]').val(), Access: $('#SelectAkses').val(), mdlAccCode: $('#SelectModuleAccess').val() };
                //var savetype = 
                _setCustomFunctions.saveAccess(data);
            },
            saveAccess: function () {

                //Set Ajax Submit Here
                var data = { AObjectID: $('input[name=idobj]').val(), Access: $('#SelectIDAkses').val(), mdlAccCode: $('#SelectModuleAccess').val() };
                //var savetype = 
                _setCustomFunctions.saveWebAccess(data);
            },
            delete: function () {
            },
            ChangeModule: function () {
                $("#tbodyid tr").remove();
                $('#SelectModuleAccess').val('');
                var paramID = this.value;
                _setCustomFunctions.getAccessModuleByModule(paramID);
            },
            SetAccess: function () {
                var $this = $(this);
                var $row = $(this).closest('tr');
                var PageID = $row.find('.PageID').html();
                var mdlAccCode = $('#SelectModuleAccess').val();
                _setCustomFunctions.getAccess(PageID, mdlAccCode);
            },
            ShowDetailAccess: function () {
                var $this = $(this);
                var $row = $(this).closest('tr');
                var Access = $row.find('.Access').html();
                if (Access == 'Yes') {
                    var $this = $(this);
                    var $row = $(this).closest('tr');
                    var PageID = $row.find('.PageID').html();
                    $('input[name=PageID]').val(PageID);
                    _setCustomFunctions.getAccessDetail(PageID);
                    $tableDetail.DataTable().ajax.reload();
                }
                else {
                    alert('Anda belum memiliki akses untuk membuka halaman ini');
                }
                //_setCustomFunctions.getAccess(PageID, mdlAccCode);
            },
            ShowDetailWebAccess: function () {

                var $this = $(this);
                var $row = $(this).closest('tr');
                var Access = $row.find('.Access').html();
                var $this = $(this);
                var $row = $(this).closest('tr');
                var AObjectID = $row.find('.AObjectID').html();
                $('input[name=accobj]').val(AObjectID);
                var mdlAccCode = $('#SelectModuleAccess').val();
                _setCustomFunctions.getWebAccessDetail(AObjectID, mdlAccCode);
                //_setCustomFunctions.getAccess(PageID, mdlAccCode);
            },
            editDetail: function () {
                $tableDetail.DataTable().ajax.reload();

                //_setCustomFunctions.getAccess(PageID, mdlAccCode);
            },
        },
        setCustomFunctions: {
            init: function () {
                this.getMasterModule();
                this.getAccessModuleByModule();
            },

            getAccess: function (PageID, mdlAccCode) {
                //console.log(PageID);
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetAccess',
                    type: "POST",
                    data: "{'PageID' :'" + PageID + "','mdlAccCode' :'" + mdlAccCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $modalAdd.modal('toggle');
                            $('input[name=code]').val(obj.PageID);
                            //$('input[name=module]').val(obj.mdlDesc);
                            $('#SelectAkses').val(obj.Access);

                        });
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });
            },
            getMasterModule: function () {
                var _parent = this;
                $('#SelectModule').val('');
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterModule',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectModule').find('option').remove();
                        $("#SelectModule").append($('<option>', {
                            value: "",
                            text: "All Module"
                        }));
                        json.forEach(function (obj) {
                            $("#SelectModule").append($('<option>', {
                                value: obj.mdlCode,
                                text: obj.mdlCode + ' - ' + obj.mdlDesc
                            }));
                        })

                        //_parent.getMasterModuleSubType($("#SelectCountryName").val());
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getAccessModuleByModule: function (paramID) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetAccessModule_ServerSideProcessing',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: JSON.stringify({ data: paramID }),
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectModuleAccess').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectModuleAccess").append($('<option>', {
                                value: obj.mdlAccCode,
                                text: obj.mdlAccCode + ' - ' + obj.mdlAccDesc
                            }));
                        })
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            getAccessDetail: function (PageID) {
                //console.log(PageID);
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetAccessDetail',
                    type: "POST",
                    data: "{'PageID' :'" + PageID + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $modalAdd2.modal('toggle');
                            $('input[name=idpage]').val(obj.PageID);
                            $('input[name=judul]').val(obj.PageTitle);

                        });
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });
            },
            getWebAccessDetail: function (AObjectID, mdlAccCode) {
                //console.log(PageID);
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetWebAccessDetail',
                    type: "POST",
                    data: "{'AObjectID' :'" + AObjectID + "','mdlAccCode' :'" + mdlAccCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $modalAdd3.modal('toggle');
                            $('input[name=idobj]').val(obj.AObjectID);
                            $('#SelectIDAkses').val(obj.Access);

                        });
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });
            },

            //getMasterModuleType: function () {

            //},
            //getMasterModuleSubType: function (type) {

            //},
            saveAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Access has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveWebAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveWebAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $tableDetail.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Access has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });
            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.DeleteRegion(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
})