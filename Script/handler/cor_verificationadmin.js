﻿var _name = [];
function SetupVerificationAdmin() {
    $('#tblusersexisting').wrap('<div style="display:none"/>');
    var _dataTable = [];
    var _dataTableExisting = [];
    var _dataTree = [];
    var _dataTreeExisting = [];
    var _listEmail = [];
    var _uID = "";
    var _userid = "";
    var finaldataEstate = "";
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddUser',
            BackButton: '#btnTabUser',
            saveButton: '#btnSave',
            setAccessButton: '#btnSetAccess',
            UpdateButton: '#btnUpdate',
            modalUser: '#mdlUser',
            modalAdd: '#mdlAdd',
            table: '#tblactiveuser',
            tableUser: '#tblusers', 
            tableUserExisting: '#tblusersexisting',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setTableUserAccExisting();
            this.setTableUserAcc();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $BackButton = $(this.options.BackButton);
            $saveButton = $(this.options.saveButton);
            $setAccessButton = $(this.options.setAccessButton);
            $UpdateButton = $(this.options.UpdateButton);
            $modalUser = $(this.options.modalUser);
            $table = $(this.options.table);
            $tableUser = $(this.options.tableUser); 
            $tableUserExisting = $(this.options.tableUserExisting);
            _formAdd = this.options.formAdd;
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $('#tblactiveuser').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                //"pagingType": "simple_numbers",
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 2,
                    "class": "left"
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 5,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 6,
                    "class": "center",
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "targets": 7,
                    "class": "center",
                    width: 10
                },],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetUserRequestAccess_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    //aoData.push({ "name": "roleId", "value": "admin" });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblactiveuser").show();

                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setTableUserAccExisting: function () {
            var index = "";
            var table = $('#tblusersexisting').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 100,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 3,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetModule5_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    if (UsersID != "" && UsersID != undefined) {
                        _userid = UsersID;
                    }
                    else {
                        _userid = 0
                    }

                    aoData.push({ 'name': 'UserID', 'value': _userid });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblusersexisting").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);

                    if (UsersID != "" && UsersID != undefined) {
                        _dataTableExisting.push($(aData[3]).next().html());
                    }
                    
                    //console.log(_dataTableExisting);
                }
            });
        },
        setTableUserAcc: function () {
            var table = $('#tblusers').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 100,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1,
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 3,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetModule4_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    if (UsersID != "" && UsersID != undefined) {
                        _userid = UsersID;
                    }
                    else {
                        _userid = 0
                    }

                    aoData.push({ 'name': 'UserID', 'value': _userid });
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            fnCallback(json);
                            $("#tblusers").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $setAccessButton.on('click', this.setAccess);
                $BackButton.on('click', this.backtohomeuser);

                //edit
                $table.on('click', 'a[data-target="#approve"]', this.approve);
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $table.on('click', 'a[data-target="#reject"]', this.reject);
                $tableUser.on('click', 'a[data-target="#edit"]', this.editAccess);
                
                //delete
                $table.on('click', 'a[data-target="#delete"]', this.delete);

                //select event
                $('#SelectDept').on('change', this.changesection);
            },
            formvalidation: function () {
                //_setCustomFunctions.getEmailUser();
                //Add On For Validation Rule
                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");


                $.validator.addMethod('codExistss', function (value, element) {
                    //value.indexOf('@indo-palm.co.id') < 10 ? $('input[name=uname]').val('') : $('input[name=uname]').val(value.substr(0, value.indexOf('@')))
                    return (value.indexOf('@indo-palm.co.id') > -1 ? true : false);

                }, "Only @indo-palm.co.id has Accepted");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        name: {
                            minlength: 1,
                            alpha: true,
                            required: true
                        },
                        email: {
                            minlength: 1,
                            required: true,
                            codExistss: true,
                            email: true,
                            //alreadyexists: true,
                            remote: function () {
                                return {
                                    url: "../../webservice/WebService_COR.asmx/GetEmailUser",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: JSON.stringify({ code: $('#email').val(), userID: $('input[name=userid]').val() }),
                                    dataFilter: function (data) {
                                        var msg = JSON.parse(data);
                                        if (msg.d == true) {
                                            $('#unit').prop('readonly', false);
                                            $('#group').prop('readonly', false);
                                            $('#userid').prop('readonly', false);
                                            $('#name').prop('readonly', false);
                                            $('#JK').prop('readonly', false);
                                            $('#dept').prop('readonly', false);
                                            $('#section').prop('readonly', false);
                                            $('#jtitle').prop('readonly', false);
                                            $('#jgrade').prop('readonly', false);
                                            $('#alkantor').prop('readonly', false);
                                            $('#telp').prop('readonly', false);

                                            _setCustomFunctions.checkUserName();
                                        }
                                        else {
                                            $('#unit').prop('readonly', true);
                                            $('#group').prop('readonly', true);
                                            $('#userid').prop('readonly', true);
                                            $('#name').prop('readonly', true);
                                            $('#JK').prop('readonly', true);
                                            $('#dept').prop('readonly', true);
                                            $('#section').prop('readonly', true);
                                            $('#jtitle').prop('readonly', true);
                                            $('#jgrade').prop('readonly', true);
                                            $('#alkantor').prop('readonly', true);
                                            $('#telp').prop('readonly', true);
                                        }

                                        if (msg.hasOwnProperty('d'))
                                            return msg.d;

                                        else
                                            return msg;
                                    }
                                }
                            }
                        },
                        unit: {
                            minlength: 1,
                            required: true
                        },
                        alkantor: {
                            minlength: 1,
                            required: true
                        },
                        telp: {
                            minlength: 1,
                            required: true,
                            numeric: true
                        },
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    },
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                //e.preventDefault();

            },
            tabchange: function (tabId) {


            },
            approve: function () {
                var $row = $(this).closest('tr');
                UsersID = $row.find('.UserID').html();
                TicketID = $row.find('.TicketID').html();
                var data = { userID: UsersID, TicketID: TicketID };
                //$table.DataTable().ajax.reload();
                //setTableUserAcc(UsersID);
                //$('#tblusers').DataTable().ajax.reload();

                _setCustomFunctions.showConfirmPopup(data, 'approve', 'confirm', 'Are You Sure Approve This Data?');
            },
            edit: function () {
                $("#gridUser").hide();
                $("#addUser").show();
                var $row = $(this).closest('tr');
                UsersID = $row.find('.UserID').html();
                _uID = UsersID;

                _setCustomFunctions.setTree(UsersID);
                _setCustomFunctions.setTree2(UsersID);
                $tableUser.DataTable().ajax.reload();
                $tableUserExisting.DataTable().ajax.reload();
                //setTableUserAcc(UsersID);
                //$('#tblusers').DataTable().ajax.reload();

            },
            reject: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var UsersID = $row.find('.UserID').html();
                TicketID = $row.find('.TicketID').html();
                var data = { userID: UsersID, TicketID: TicketID };
                //Set Ajax Submit Here
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data,'reject', 'confirm', 'Are You Sure Delete This Data?');

                return false;
            },
            save: function () {
                //Set Ajax Submit Here
                //var savetype = 
                var datatable = [];
                var datas = [];
                var table = $('#tblusers').DataTable();

                var headers = [];
                $('#tblusers th').each(function (index, item) {
                    headers[index] = $(item).html();
                });
                $('#tblusers tr').has('td').each(function () {
                    var arrayItem = {};
                    $('td', $(this)).each(function (index, item) {
                        arrayItem[headers[index]] = $(item).html();
                    });
                    datatable.push(arrayItem);
                });

                var $row = $(this).closest('tr');
                _dataTable = [];
                for (var i = 0 ; i < datatable.length; i++) {
                    var a = jQuery(datatable[i].Code).text();
                    var lastFive = a.substr(a.length - 4);

                    _dataTable.push(lastFive);
                }
                _setCustomFunctions.saveUser(_dataTable, _dataTree);
            },
            backtohomeuser: function () {
                $("#gridUser").show();
                $("#addUser").hide();

            },
            setAccess: function () {
                var $row = $(this).closest('tr');
                mdlCode = $('input[name=code]').val();
                mdlAccCode = $('#SelectAccess option:selected').text();
                mdlAccCodes = $('#SelectAccess option:selected').val();
                $('#' + mdlCode).html(mdlAccCode);
                $('#' + mdlCode + "_Access").html(mdlAccCodes);
                $modalUser.modal('hide');
                //

            },
            changesection: function () {
                _setCustomFunctions.getSection($('#SelectDept').val());
            },
            editAccess: function () {
                var $row = $(this).closest('tr');
                mdlCode = $row.find('.mdlCode').html();
                mdlAccCode = $row.find('.mdlCode_Access').html();
                _setCustomFunctions.getAccess(mdlCode, mdlAccCode);
                $modalUser.modal('toggle');
            },
        },
        setCustomFunctions: {
            init: function () {
                this.getAccess();
                this.setTree(0);
                //this.checkUserName();
            },
            getAccess: function (mdlCode, mdlAccCode) {
                $('input[name=mdlacccoder]').val(mdlAccCode);
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetMasterAccess',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'mdlCode' :'" + mdlCode + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectAccess').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectAccess").append($('<option>', {
                                value: obj.mdlAccCode,
                                text: obj.mdlAccDesc
                            }));
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });

                $('input[name=code]').val(mdlCode);
                $('input[name=mdlacccoder]').val(mdlCode);
            },
            setTree: function (UsersID) {
                var datas = [];
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetEstAccessUserFromTemp',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: "{'UsersID' :'" + UsersID + "'}",
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0 ; i < json.length; i++) {
                            datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }
                        if (UsersID == "") {
                            finaldata = json;
                        }
                        else {
                            finaldata = datas;
                        }
                        var $treeview = $("#tvestate");
                        $('#tvestate').jstree('destroy');
                        $('#tvestate').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                'data':
                                    finaldata
                            },
                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                        });

                        $('#tvestate')
                        // listen for event
                        .on('changed.jstree', function (e, data) {
                            _dataTree = [];
                            _dataTree = data.instance.get_bottom_selected();
                        })
                        // create the instance
                        .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });



            },
            setTree2: function (UsersID) {
                var datas = [];
                var id = 1;
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetEstAccessUser',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    type: "POST",
                    data: "{'UsersID' :'" + UsersID + "'}",
                    success: function (response) {
                        var json = jQuery.parseJSON(response.d);
                        for (var i = 0 ; i < json.length; i++) {
                            datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                        }
                        var finaldata = "";
                        if (UsersID == "") {
                            finaldata = json;
                        }
                        else {
                            finaldata = datas;
                        }
                        var $treeview = $("#tvestate2");
                        $('#tvestate2').jstree('destroy');
                        $('#tvestate2').jstree({
                            plugins: ['checkbox', 'changed'],
                            'core': {
                                "themes": { "icons": false },
                                expand_selected_onload: true,
                                'data':
                                    finaldata
                            },
                        }).bind("loaded.jstree", function (event, data) {
                            //$(this).jstree("open_all");
                            $treeview.jstree('open_all');
                        });

                        $('#tvestate2')
                        // listen for event
                        .on('changed.jstree', function (e, data) {
                            _dataTreeExisting = data.instance.get_bottom_selected();
                        })
                        // create the instance
                        .jstree();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });



            },
            ApproveAccess: function (data) {
                    $.ajax({
                        type: 'POST',
                        url: '../../webservice/WebService_COR.asmx/SaveRequestAccess',
                        data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        success: function (response) {
                            if (response.d == 'success') {
                                $table.DataTable().ajax.reload();
                                _setCustomFunctions.showPopup('Info', 'Access has been saved successfully');
                            } else {
                                _setCustomFunctions.showPopup('Info', 'Failed to save');
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                        }
                    });
            },
            RejectAccess: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/RejectRequestAccess',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Access has been Reject');
                        } else {
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            saveUser: function (_dataTable, _dataTree) {
                var ReqmdlAcc = "";
                var BefmdlAcc = "";
                var Requestmdl = [];
                var Beforemdl = [];
                //for (var i in _dataTableExisting) {
                //    if (_dataTableExisting[i] !== _dataTable[i]) {
                //        ReqmdlAcc = _dataTable[i];
                //        BefmdlAcc = _dataTableExisting[i];
                //        Requestmdl.push('- ' + ReqmdlAcc);
                //        Beforemdl.push('- ' + BefmdlAcc);
                //    }
                //}
                for (var i = 0; i < _dataTableExisting.length; i++) {
                    for (var x = 0; x < _dataTable.length; x++) {
                        if ($.inArray(_dataTable[x], _dataTableExisting) == -1) {
                            if ($.inArray("- " + _dataTable[x], Requestmdl) == -1) {
                                ReqmdlAcc = "- " + _dataTable[x];
                                Requestmdl.push(ReqmdlAcc);
                            }
                        }
                    }
                }


                for (var i = 0; i < _dataTable.length; i++) {
                    for (var x = 0; x < _dataTableExisting.length; x++) {
                        if ($.inArray(_dataTableExisting[x], _dataTable) == -1) {
                            if ($.inArray("- " + _dataTableExisting[x], Beforemdl) == -1) {
                                BefmdlAcc = "- " + _dataTableExisting[x];
                                Beforemdl.push(BefmdlAcc);
                            }
                        }
                    }
                }


                if (Requestmdl.length == 0 && Beforemdl.length == 0) {
                    var mdlFinal = "";
                }
                else {
                    var mdlFinal = "<label class=bg-green>Module Access </label><br><label style=font-weight:bold>Request : </label><br>" + Requestmdl + "<br><br>" + "<label style=font-weight:bold>Before : </label><br>" + Beforemdl;
                }



                var ReqEstAcc = "";
                var RemEstAcc = "";
                var RequestEst = [];
                var RemoveEst = [];
                for (var i = 0; i < _dataTreeExisting.length; i++) {
                    for (var x = 0; x < _dataTree.length; x++) {
                        if ($.inArray(_dataTree[x], _dataTreeExisting) == -1) {
                            if ($.inArray("- " + _dataTree[x], RequestEst) == -1) {
                                ReqEstAcc = "- " + _dataTree[x];
                                RequestEst.push(ReqEstAcc);
                            }
                        }
                    }
                }

                for (var i = 0; i < _dataTree.length; i++) {
                    for (var x = 0; x < _dataTreeExisting.length; x++) {
                        if ($.inArray(_dataTreeExisting[x], _dataTree) == -1) {
                            if ($.inArray("- " + _dataTreeExisting[x], RemoveEst) == -1) {
                                RemEstAcc = "- " + _dataTreeExisting[x];
                                RemoveEst.push(RemEstAcc);
                            }
                        }
                    }
                }
                if (RequestEst.length == 0 && RemoveEst.length == 0) {
                    var estFinal = "";
                }
                else if (RemoveEst.length == 0 && RequestEst.length == 1) {
                    var estFinal = "<label class=bg-green>Estate Access </label><br><label style=font-weight:bold>Request : </label><br>" + RequestEst;
                }
                else if (RequestEst.length == 0 && RemoveEst.length == 1) {
                    var estFinal = "<label class=bg-green>Estate Access </label><br><label style=font-weight:bold>Remove : </label><br>" + RemoveEst;
                }
                else {
                    var estFinal = "<label class=bg-green>Estate Access </label><br><label style=font-weight:bold>Request : </label><br>" + RequestEst + "<br><br>" + "<label style=font-weight:bold>Remove : </label><br>" + RemoveEst;
                }


                var datas = { userID: _uID, UpdatemdlCode: mdlFinal, UpdateEstCode: estFinal, ListmdlEstAccess: _dataTree, ListmdlAccess: _dataTable, ListmdlAccessExisting: _dataTableExisting, ListmdlEstAccessExisting: _dataTreeExisting };

                        //$(_formAdd + " :input").prop("disabled", false);
                        $.ajax({
                            type: 'POST',
                            url: '../../webservice/WebService_COR.asmx/UpdateRequestAccess',
                            data: JSON.stringify({ dataobject: JSON.stringify(datas) }),
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            success: function (response) {
                                $saveButton.html("Save");
                                if (response.d == 'success') {
                                    $table.DataTable().ajax.reload();
                                    _setCustomFunctions.showPopup('Info', 'User has been saved successfully');

                                } else {
                                    _setCustomFunctions.showPopup('Info', 'Failed to save');
                                    history.go(0); //forward
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                            }
                        });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                history.go(0); //forward

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, type, title, content) {
                

                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                if (type == "reject") {
                                    _setCustomFunctions.RejectAccess(data);
                                }
                                else
                                {
                                    _setCustomFunctions.ApproveAccess(data);
                                }

                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            },
            checkUserName: function () {
                var Email = $('#email').val()

                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/GetUserData',
                    contentType: 'application/json; charset=utf-8',
                    data: "{'Email' :'" + Email + "'}",
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        json.forEach(function (obj) {
                            $('input[name=telp]').val(obj.NoHP);
                            $('input[name=name]').val(obj.Nama);
                            if (obj.Email == "") {
                                var email = $('input[name=email]').val();
                                $('input[name=uname]').val(email.substr(0, email.indexOf('@')));
                            }
                            $('input[name=uname]').val(obj.Email.substr(0, obj.Email.indexOf('@')));

                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
        }

    }

    Page.initialize();
}


function EditUser(userid) {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../../webservice/WebService_COR.asmx/GetUserDetailWithEnableSession",
        data: { UserID: JSON.stringify(userid) },
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            console.log(response.d);
        }
    });
}

function ac(_s) {
    $("#name").autocomplete({
        source: function (request, response) {
            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(_uname, function (item) {
                return matcher.test(item);
            }));
        }
    });
};

function test() {
    //alert($('input[name=name]').val());
    var _name = $('input[name=name]').val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "../../webservice/WebService_COR.asmx/GetDataFromUserJoinIplas",
        data: { UserName: JSON.stringify(_name) },
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var json = $.parseJSON(response.d);
            json.forEach(function (obj) {
                $('input[name=telp]').val(obj.NoHP);
                if (obj.Email != null) {
                    $('input[name=email]').val(obj.Email);
                }
                else {
                    alert("Email for " + _name + " is Not Register in UserJoinIPLAS");
                }


            });
        }
    });
};