﻿$(function () {
    $('#pwd').html('');
    $('#cfmpwd').html('');
    $('#pwd').text("");
    $('#pwd').val("");
    $("#name").prop("disabled", true);
    $("#email").prop("disabled", true);
    $("#category").prop("disabled", true);
    $("#department").prop("disabled", true);
    GetMasterArea();
    GetMasterModule();
    //SetTableAccess();
    GetTableAccess();
    document.getElementById("pwd").value = "";
    var _dataEst = [];
    var _dataMdl = [];
    var _defaultEst = [];
    var _defaultMdl = [];
    var _AccEst = [];
    var _AccMdl = [];
    var _namaUser;
    //var url = new URL(window.location.href);
    //var Code = "bdvffvpgdufkugytwvfgogzkugytzyjv_EfZD0p_";
    var url = new URL(window.location.href);
    var Code = url.searchParams.get("ver");

    var flagGroup1 = 0;
    var flagGroup2 = 0;

    $("#submitBtn").click(function () {
        //alert($('#SelectEstateAcc').val() + ' - ' + $('#SelectmdlAcc').val());
        _AccEst = [];
        _AccMdl = [];

        for (var i = 0; i < $('#SelectEstateAcc').val().length; i++) {
            _AccEst.push($('#SelectEstateAcc').val()[i].split("-")[0].replace(" ", ""));
        };
        
        for (var i = 0; i < $('#SelectmdlAcc').val().length; i++) {
            _AccMdl.push($('#SelectmdlAcc').val()[i].split("-")[0].replace(" ", ""));
        };
        //alert(_AccEst);
        //alert(_AccMdl);

        //const myArray = $('#SelectEstateAcc').val().split(" ")
        $.ajax({
            type: 'POST',
            url: 'webservice/WebService_COR.asmx/SaveAccessUserTemporary',
            data: JSON.stringify({
                userEmail: $("#email").val(),
                password: $("#pwd").val(),
                userCategory: $("#category").val(),
                department: $("#department").val(),
                estAccess: _AccEst.toString(),
                mdlAccess: _AccMdl.toString(),
                estAccessDefault: _defaultEst.toString(),
                mdlAccessDefault: _defaultMdl.toString()

            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            success: function (response) {

                $.ajax({
                    type: 'POST',
                    url: 'webservice/WebService_COR.asmx/RequestApprovalAccess',
                    data: JSON.stringify({ userEmail: "cahyo.adi@kpnplantation.com", userFullName: _namaUser, emailUser: $("#email").val()}),
                    data: JSON.stringify({ userEmail: "gis.support@kpnplantation.com", userFullName: _namaUser, emailUser: ("#email").val()}),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {

                        var _path = window.location.pathname;
                        var urls = location.protocol;
                        var test = window.location.hostname;
                        window.location.href = "/default2.aspx";

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError);
                    }
                });
            }
        });
    });

    $("#SelectmdlAcc").change(function () {
        if (flagGroup2 >= 1) {
            $(".selected").removeClass("selected");
            $('.selectpicker').selectpicker('deselectAll');
            flagGroup2 = 0;
            flagGroup1 = 1;
        }
        if ($(this).parent().hasClass("selected")) {
            if (flagGroup1 > 1) {
                flagGroup1 = 1;
            } else {
                flagGroup1 = 0;
            }
        } else {
            flagGroup1 = flagGroup1 + 1;
        }
    });

    $.ajax({
        url: 'webservice/WebService_COR.asmx/CheckDateVerification',
        type: "POST",
        data: "{'Code' :'" + Code + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var json = $.parseJSON(data.d);
            json.forEach(function (obj) {
                if (obj.Status == "false") {
                    if (Code != null) {

                        $.ajax({
                            url: 'webservice/WebService_COR.asmx/GetAccessUser',
                            type: "POST",
                            data: "{'Code' :'" + Code + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var json = $.parseJSON(data.d);
                                json.forEach(function (obj) {
                                    _dataEst = obj.estAccess.split(",");
                                    _dataMdl = obj.mdlAccess.split(",");
                                    _namaUser = obj.UserFullName;
                                    $('#name').val(obj.UserFullName);
                                    $('#email').val(obj.UserEmail);
                                    $('#category').val(obj.CategoryUser);
                                    $('#hiddencategory').val(obj.CategoryUser);
                                    $('#department').val(obj.Department);
                                    setTableEstate();
                                    setTableModule();
                                })
                            }
                        })
                    }
                    else {
                        alert("Verification Code Has Expired, Please Your verification code has expired, please re-register at Enterprise GIS");
                    }
                }
            })
        }
    });

    function GetTableAccess() {
    }



    function setTableEstate() {
        var json2 = _dataEst;

        var content2 = '<font face="Helvetica" style="font-size:10px;">';
        content2 += '<table id="tblAksesEstate" class="table table-striped table-bordered" style="width:90%;">';
        content2 += '<thead><tr>';
        content2 += '<th style = "text-align: center !important;" > No. </th>';
        content2 += '<th style = "text-align: left !important;" > Plantation </th>';
        content2 += '<th style = "text-align: left !important;" > Region</th>';
        content2 += '<th style = "text-align: left !important;" > Group Company</th>';
        content2 += '<th style = "text-align: left !important;" > Estate</th>';

        content2 += '</tr></thead><tbody>';
        for (var i = 0; i < json2.length; i++) {
            if (json2[i] != "") {
                const myArray2 = json2[i].split("-");
                content2 += '<tr>';
                content2 += '<td style="text-align: center;">' + (i+1) + '</td>';
                content2 += '<td style="text-align: left;">' + myArray2[0] + '</td>';
                content2 += '<td style="text-align: left;">' + myArray2[1] + '</td>';
                content2 += '<td style="text-align: left;">' + myArray2[2] + '</td>';
                content2 += '<td style="text-align: left;">' + myArray2[3] + '</td>';
                content2 += '</tr>';
                _defaultEst.push(myArray2[3]);
            }
            else {

                content2 += '<tr>';
                content2 += '<td style="text-align: center;" colspan="5">' + 'No Access' + '</td>';
                content2 += '<td style="display: none"></td>';
                content2 += '<td style="display: none"></td>';
                content2 += '<td style="display: none"></td>';
                content2 += '<td style="display: none"></td>';
                content2 += '</tr>';

            }
        }
        content2 += '</tbody></table></font>';

        $("#dataTableEstate").html(content2);

        $("#tblAksesEstate").DataTable({
            dom: 'tlip',
            searching: false,
            paging: false,
            info: false,
            bAutoWidth: false,
            aoColumns: [
                { sWidth: '5%' },
                { sWidth: '15%' },
                { sWidth: '25%' },
                { sWidth: '25%' },
                { sWidth: '15%' }
            ]
        }).columns.adjust();

    }
    function setTableModule() {
        var json = _dataMdl;

        var content = '<font face="Helvetica" style="font-size:10px;">';
        content += '<table id="tblAksesModule" class="table table-striped table-bordered" style="width:90%;">';
        content += '<thead><tr>';
        content += '<th style = "text-align: center !important;" > No. </th>';
        content += '<th style = "text-align: left !important;" > Module Code </th>';
        content += '<th style = "text-align: left !important;" > Module Name</th>';
        content += '<th style = "text-align: left !important;" > Access</th>';

        content += '</tr></thead><tbody>';
        for (var i = 0; i < json.length; i++) {
            if (json.length > 0) {
                const myArray = json[i].split("-");
                content += '<tr>';
                content += '<td style="text-align: center;">' + (i + 1) + '</td>';
                content += '<td style="text-align: left;">' + myArray[0] + '</td>';
                content += '<td style="text-align: left;">' + myArray[1] + '</td>';
                content += '<td style="text-align: left;" onclick="javascript:detailAccess(\'' + myArray[0] + '\',\'' + myArray[2] + '\')">' + myArray[3] + '</td>';
                content += '</tr>';
                _defaultMdl.push(myArray[2]);
            }
        }
        content += '</tbody></table></font>';

        $("#dataTableModule").html(content);

        $("#tblAksesModule").DataTable({
            dom: 'tlip',
            searching: false,
            paging: false,
            info: false,
            bAutoWidth: false,
            aoColumns: [
                { sWidth: '5%' },
                { sWidth: '15%' },
                { sWidth: '25%' },
                { sWidth: '15%' }
            ]
        }).columns.adjust();
    }


    function GetMasterModule() {
        var _parent = this;
        $.ajax({
            type: 'GET',
            url: 'webservice/WebService_COR.asmx/getMastermdlAccCode',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var json = $.parseJSON(response.d);
                //$('#SelectmdlAcc').find('option').remove();
                //json.forEach(function (obj) {
                //    $("#SelectmdlAcc").append($('<option>', {
                //        value: obj.mdlAccCode,
                //        text: obj.mdlAccDesc
                //    }));
                //})

                //$('#SelectmdlAcc').selectpicker('refresh');


                if (json.length > 0) {
                    $('#SelectmdlAcc').find('option').remove();
                    var optgroup = "", group = "", option = "";

                    json.forEach(function (obj) {
                        if (obj.mdlDesc != optgroup) {
                            optgroup = obj.Grouping
                            optgroup = obj.mdlDesc
                            group = $('<optgroup label="' + optgroup + '" data-max-options="1"/>');
                        }

                        option = $('<option>', {
                            value: obj.mdlAccDesc,
                            text: obj.mdlAccDesc2
                        });

                        option.attr("data-tokens", optgroup + "," + obj.mdlAccDesc);
                        option.appendTo(group);
                        group.appendTo($('#SelectmdlAcc'));
                    });

                    $('#SelectmdlAcc').selectpicker('refresh');
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    }

    function GetMasterArea() {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "webservice/WebService_COR.asmx/GetEstateNew",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var json = $.parseJSON(data.d);

                if (json.length > 0) {
                    $('#SelectEstateAcc').find('option').remove();
                    var optgroup = "", optgroup2 = "", group = "", option = "";

                    json.forEach(function (obj) {
                        if (obj.NewEstName.includes("Kebun") == true) {
                            if (obj.Grouping != optgroup) {
                                optgroup = obj.Grouping
                                group = $('<optgroup label="' + optgroup + '" />');
                            }

                            option = $('<option>', {
                                value: obj.estCode,
                                text: obj.Estate
                            });

                            option.attr("data-tokens", optgroup + "," + obj.Estate);
                            option.appendTo(group);
                            group.appendTo($('#SelectEstateAcc'));

                        }
                            
                    });

                    $('#SelectEstateAcc').selectpicker('refresh');
                }


                //$('#selectFilterArea').find('option').remove();
                //json.forEach(function (obj) {
                //    $("#selectFilterArea").append($('<option>', {
                //        value: obj.Area,
                //        text: obj.Area
                //    }));
                //})

                //$('#selectFilterArea').selectpicker('refresh');
            }
        });
    }


});
