﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            formAddsubData: '#formAddsubData',
            formEdit: '#formEdit',
            addButton: '#btnAddDatagroup',
            addSubDatagroupButton: '#btnAddSubDatagroup',
            saveButton: '#btnSave',
            saveSubDataButton: '#btnSaveSubDataGroup',
            UpdateButton: '#btnUpdate',
            UpdateSubDataGroupButton: '#btnUpdateSubDataGroup',
            modalAdd: '#mdlAdd',
            modalAddSubDataGroup: '#mdlAddSubDataGroup',
            tabDatagroup: '#tabDatagroup',
            table: '#tbldatagroup',
            tablesubdata: '#tblSubdatagroup',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $formEdit = $(this.options.formEdit);
            $formAddsubData = $(this.options.formAddsubData);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $saveSubDataButton = $(this.options.saveSubDataButton);
            $UpdateButton = $(this.options.UpdateButton);
            $UpdateSubDataGroupButton = $(this.options.UpdateSubDataGroupButton);
            $modalAdd = $(this.options.modalAdd);
            $modalAddSubDataGroup = $(this.options.modalAddSubDataGroup);
            $tabDatagroup = $(this.options.tabDatagroup);
            $table = $(this.options.table);
            $tablesubdata = $(this.options.tablesubdata);
            _formAdd = this.options.formAdd;
            _formAddsubData = this.options.formAddsubData;
            _tabDatagroup = this.options.tabDatagroup;
            _listDatagroup = [];
            _listDatagroupCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 1
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 2,
                    width: 300
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 5,
                    width: 10
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 6,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetDatagroup_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tbldatagroup").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $UpdateButton.on('click', this.saveEdit);
                $saveSubDataButton.on('click', this.saveSubData);
                $UpdateSubDataGroupButton.on('click', this.saveEditSubData);
                $table.on('click', 'a[data-target="#edit"]', this.edit);
                $table.on('click', 'a[data-target="#delete"]', this.delete);

                $tabDatagroup.on('click', 'a', this.tabclick);
                //this.formvalidation();

                //select event
                $('#selectType').on('change', this.selectchange);
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                $.validator.addMethod("numeric", function (value, element) {

                    return this.optional(element) || value == value.match(/^[0-9]+$/);

                }, "Numeric characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 1,
                            maxlength: 3,
                            required: true,
                            numeric: true
                        },
                        name: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                //Main Validation
                $formEdit.validate({
                    rules: {
                        codeEdit: {
                            minlength: 1,
                            maxlength: 3,
                            required: true,
                            numeric: true
                        },
                        nameEdit: {
                            minlength: 1,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },

                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
                $formAddsubData.validate({
                    rules: {
                        subdatagroupcode: {
                            minlength: 1,
                            maxlength: 3,
                            required: true,
                            numeric: true
                        },
                        subdatagroupname: {
                            minlength: 1,
                            required: true
                        },
                        requestmap: {
                            minlength: 1,
                            required: true
                        },
                        areaid: {
                            minlength: 1,
                            required: true
                        },
                        maptypeid: {
                            minlength: 1,
                            maxlength: 3,
                            required: true,
                            numeric: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });


            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                $(this).tab('show');
            },
            tabchange: function (tabId) {
                $(_tabDatagroup + ' a[href="#' + tabId + '"]').tab('show');
            },
            tabclose: function () {
                $.confirm({
                    title: 'Confirmation',
                    content: 'You are editing something. Are you sure to close existing edit ? ',
                    buttons: {
                        cancel: function () {

                        },
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                _currentEditTabId = "";
                                _isEditTabExists = false;

                                //there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
                                var tabContentId = $(".closeTab").parent().attr("href");
                                $(".closeTab").parent().parent().remove(); //remove li of tab
                                $(_tabDatagroup + ' a:first').tab('show'); // Select first tab
                                $(tabContentId).remove(); //remove respective tab content
                            }
                        }
                    }
                });
            },
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                document.getElementById('btnSave').style.display = 'inline';
                $('input[name=code]').val('');
                $('input[name=codeHidden]').val('');
                $('input[name=name]').val('');

                _setEvents.formvalidation();
            },
            edit: function () {
                document.getElementById('btnUpdate').style.display = 'inline';

                _setEvents.formvalidation();
                //$modalAdd.modal('toggle');

                var $this = $(this);
                var $row = $(this).closest('tr');
                var DataGroupCode = $row.find('.DataGroupCode').html();
                var DataGroupName = $row.find('.DataGroupName').html();
                $('#SelectMapTypeID').val('');
                $('#SelectRequestMap').val('');
                $('#SelectAreaID').val('');

                $("#tbodyid").empty();

                var tabId = "edit" + DataGroupCode;
                if ($(_tabDatagroup + ' .nav-tabs a[href="#' + tabId + '"]').length) {
                    _setEvents.tabchange(tabId);
                    _setCustomFunctions.setTableSubData($('input[name=codeEditHidden]').val());
                    $(".addsubdata").on('click', _setEvents.addsubdatagroup);
                    $(".editSub").on('click', _setEvents.saveEdit);
                } else {
                    if (_isEditTabExists) {
                        $.confirm({
                            title: 'Confirmation',
                            content: 'You are editing something. Are you sure to close existing edit ? ',
                            buttons: {
                                GoToTabEditing: {
                                    text: 'Go To Editing Tab',
                                    btnClass: 'btn-orange',
                                    action: function () {
                                        _setEvents.tabchange(_currentEditTabId);
                                        _setCustomFunctions.getDataGroup($('input[name=codeEditHidden]').val());
                                        _setCustomFunctions.setTableSubData($('input[name=codeEditHidden]').val());
                                    }
                                },
                                cancel: function () {

                                },
                                OK: {
                                    text: 'OK',
                                    btnClass: 'btn-blue',
                                    action: function () {
                                        //there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
                                        var tabContentId = _currentEditTabId;
                                        $('#tabDatagroup .nav-tabs a[href="#' + tabContentId + '"]').parent().remove(); //remove li of tab
                                        $('#tabDatagroup a:first').tab('show'); // Select first tab
                                        $('#' + tabContentId).remove(); //remove respective tab content


                                        _isEditTabExists = true;
                                        _currentEditTabId = tabId;
                                        $('#tabDatagroup .nav-tabs').append('<li><a href="#' + tabId + '" data-togle="tab"><button class="close closeTab" type="button" >×</button>Edit - ' + DataGroupName + '</a></li>');
                                        $('#tabDatagroup .tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

                                        _setCustomFunctions.registerTabContent(tabId, DataGroupCode);
                                        _setEvents.tabchange(tabId);
                                        _setCustomFunctions.getDataGroup(DataGroupCode);
                                        _setCustomFunctions.setTableSubData(DataGroupCode);
                                        $(".addsubdata").on('click', _setEvents.addsubdatagroup);
                                        //$(".editSub").on('click', _setEvents.saveEdit);
                                    }
                                }
                            }
                        });
                    } else {
                        _isEditTabExists = true;
                        _currentEditTabId = tabId;
                        $('#tabDatagroup .nav-tabs').append('<li><a href="#' + tabId + '" data-togle="tab"><button class="close closeTab" type="button" >×</button>Edit - ' + DataGroupName + '</a></li>');
                        $('#tabDatagroup .tab-content').append('<div class="tab-pane" id="' + tabId + '"></div>');

                        _setCustomFunctions.registerTabContent(tabId, DataGroupCode);
                        _setEvents.tabchange(tabId);
                        _setCustomFunctions.getDataGroup(DataGroupCode);
                        _setCustomFunctions.setTableSubData(DataGroupCode);
                        $(".addsubdata").on('click', _setEvents.addsubdatagroup);
                        //$(".editSub").on('click', _setEvents.saveEdit);
                    }
                }
                $(".closeTab").on('click', _setEvents.tabclose);



                //Event for deleting data
                //Set Ajax Submit Here
            },
            saveEdit: function () {
                //Event for deleting data
                if ($formEdit.valid()) {
                    var $this = $(this);
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { DataGroupCode: $('input[name=codeEditHidden]').val(), NewDataGroupCode: $('input[name=codeEdit]').val(), DataGroupName: $('input[name=nameEdit]').val(), DefaultRequestMap: $('#SelectDefaultRequestEdit').val(), DefaultPayment: $('#SelectDefaultPembayaranEdit').val() };
                    //var savetype = 
                    _setCustomFunctions.EditDatagroup(data);

                    return false;
                }

            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $row = $(this).closest('tr');
                var DataGroupCode = $row.find('.DataGroupCode').html();
                //Set Ajax Submit Here
                var data = { DataGroupCode: DataGroupCode };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(1, data, 'confirm', 'Are You Sure Delete This Data?');

                return false;
            },
            save: function () {
                //Event for saving data
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { DataGroupCode: $('input[name=code]').val(), DataGroupName: $('input[name=name]').val(), DefaultRequestMap: $('#SelectDefaultRequest').val(), DefaultPayment: $('#SelectDefaultPembayaran').val() };
                    //var savetype = 
                    _setCustomFunctions.saveDatagroup(data);

                    return false;
                }
            },

            saveSubData: function () {
                //Event for deleting data
                if ($formAddsubData.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    //Set Ajax Submit Here
                    var data = {
                        SubCode: $('input[name=subdatagroupcode]').val(), SubName: $('input[name=subdatagroupname]').val(), DataGroupCode: $('input[name=codeEdit]').val(), RequestMap: $('#SelectRequestMap').val(),
                        AreaID: $('#SelectAreaID').val(), MapTypeID: $('#SelectMapTypeID').val()
                    };
                    _setCustomFunctions.SaveSubData(data);

                    return false;
                }


            },
            saveEditSubData: function () {
                //Event for deleting data
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                $("#formAdd :input").prop("disabled", true);

                //Set Ajax Submit Here
                var data = {
                    SubCode: $('input[name=subdatagroupcodeHidden]').val(), NewSubCode: $('input[name=subdatagroupcode]').val(), SubName: $('input[name=subdatagroupname]').val(), RequestMap: $('#SelectRequestMap').val(),
                    AreaID: $('#SelectAreaID').val(), MapTypeID: $('#SelectMapTypeID').val()
                };
                //var savetype = 
                _setCustomFunctions.EditSubData(data);

                //return false;

            },

            addsubdatagroup: function () {
                document.getElementById('btnUpdateSubDataGroup').style.display = 'none';
                document.getElementById('btnSaveSubDataGroup').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Sub Data Group"
                //Event for adding data
                //$modalAddSubDataGroup.modal('toggle');
                $modalAddSubDataGroup.modal('toggle');
                $('input[name=subdatagroupcode]').val('');
                $('input[name=subdatagroupcodeHidden]').val('');
                $('input[name=subdatagroupname]').val('');
                $('#SelectMapTypeID').val('');
                $('#SelectRequestMap').val('');
                $('#SelectAreaID').val('');

                _setEvents.formvalidation();
            },
            editsubdatagroup: function () {
                document.getElementById('btnUpdateSubDataGroup').style.display = 'inline';
                document.getElementById('btnSaveSubDataGroup').style.display = 'none';
                document.all('lbl').innerHTML = "Edit Sub Data Group"
                var $this = $(this);
                var $row = $(this).closest('tr');
                var SubCode = $row.find('.subdatacode').html();
                $modalAddSubDataGroup.modal('toggle');
                $('input[name=subdatagroupcode]').val('');
                $('input[name=subdatagroupcodeHidden]').val('');
                $('input[name=subdatagroupname]').val('');
                $('#SelectMapTypeID').val('');
                $('#SelectRequestMap').val('');
                $('#SelectAreaID').val('');

                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetSubDataGroup',
                    type: "POST",
                    data: "{'SubCode' :'" + SubCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=subdatagroupcode]').val(obj.SubCode);
                            $('input[name=subdatagroupcodeHidden]').val(obj.SubCode);
                            $('input[name=subdatagroupname]').val(obj.SubName);
                            //$('input[name=requestmap]').val(obj.RequestMap);
                            $('input[name=areaid]').val(obj.AreaID);
                            $('#SelectMapTypeID').val(obj.MapTypeID)
                            //$('input[name=module]').val(obj.mdlDesc);
                            $('#SelectAreaID').val(obj.AreaID)
                            $('#SelectRequestMap').val(obj.RequestMap)

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });

            },

            deletesubdatagroup: function () {
                var $this = $(this);
                var $row = $(this).closest('tr');
                var subcode = $row.find('.subdatacode').html();

                var data = { subcode: subcode };

                _setCustomFunctions.showConfirmPopup(2, data, 'confirm', 'Are You Sure Delete This Data Sub Data?');

                return false;
            },
        },
        setCustomFunctions: {
            init: function () {
                this.getMapTypeID();
            },
            getMapTypeID: function () {
                var _parent = this;
                $.ajax({
                    type: 'GET',
                    url: '../../webservice/WebService_COR.asmx/GetMasterMapTypeID',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var json = $.parseJSON(response.d);
                        $('#SelectMapTypeID').find('option').remove();
                        json.forEach(function (obj) {
                            $("#SelectMapTypeID").append($('<option>', {
                                value: obj.MapTypeID,
                                text: obj.MapTypeID + ' - ' + obj.MapTypeName
                            }));
                        })

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });

            },
            getSubdata: function (DataGroupCode) {
            },
            setTableSubData: function (DataGroupCode) {
                $.ajax({
                    type: "POST",
                    url: "../../webservice/WebService_COR.asmx/GetSubDataGroup_ServerSideProcessing",
                    data: JSON.stringify({ data: DataGroupCode }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //for (var i = 0; i < data.length; i++) {
                        var json = $.parseJSON(data.d);
                        var tables = $('#tblSubdatagroup');
                        json.forEach(function (obj) {
                            tables.append("<tr class='subdatagroup'><td class='subdatacode'>" + obj.SubCode + "</td><td class='subdataname'>" + obj.SubName + "</td><td class='maptype'>" + obj.MapTypeName + "</td><td class='requestmap'>" + obj.RequestMapDesc + "</td><td class='area'>" + obj.AreaDesc + "</td><td align='center'>" + "<a type='button' class='btn btn-primary btn-xs editsub' data-title='EditSubDataGroup' data-toggle='modal'><span class='glyphicon glyphicon-pencil'></span></a>" + "</td><td align='center'>" + "<a type='button' class='btn btn-danger btn-xs deletesub' data-title='DeleteSubDataGroup' data-toggle='modal' ><span class='glyphicon glyphicon-trash'></span></a>" + "</td></tr>");
                        })
                        //console.log(data.d[i].ipgroup);

                        $(".editsub").on('click', _setEvents.editsubdatagroup);
                        $(".deletesub").on('click', _setEvents.deletesubdatagroup);

                        //}
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            },
            registerTabContent: function (tabId, DataGroupCode) {
                $('.divtemplate').clone().appendTo($('#' + tabId));
                $('#' + tabId + " .divtemplate").css('display', 'block');
            },
            getDataGroup: function (DataGroupCode) {
                $.ajax({
                    url: '../../webservice/WebService_COR.asmx/GetDataGroup',
                    type: "POST",
                    data: "{'DataGroupCode' :'" + DataGroupCode + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = $.parseJSON(data.d);
                        json.forEach(function (obj) {
                            $('input[name=codeEdit]').val(obj.DataGroupCode);
                            $('input[name=codeEditHidden]').val(obj.DataGroupCode);
                            $('input[name=nameEdit]').val(obj.DataGroupName);
                            //$('input[name=module]').val(obj.mdlDesc);
                            $('#SelectDefaultRequestEdit').val(obj.DefaultRequestMap);
                            $('#SelectDefaultPembayaranEdit').val(obj.DefaultPayment);

                        })
                    },
                    failure: function (msg) {
                        alert("Something go wrong! ");
                    }
                });
            },
            saveDatagroup: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveDatagroup',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Datagroup has been saved successfully');
                            $modalAdd.modal('hide');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteDatagroup: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteDatagroup',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'Datagroup has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditDatagroup: function (data) {
                var regexNum = /\d/;
                var regexLetter = /[a-zA-z]/;
                if (data.NewDataGroupCode !== "" && data.DataGroupName !== "" && data.DefaultRequestMap !== "" && data.DefaultPayment !== "") {
                    if (regexNum.test(data.NewDataGroupCode)) {
                        $.ajax({
                            type: 'POST',
                            url: '../../webservice/WebService_COR.asmx/EditDatagroup',
                            data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            success: function (response) {
                                $UpdateButton.html("Update");
                                if (response.d == 'success') {
                                    $(_formAdd + " :input").prop("disabled", false);
                                    history.go(0); //forward
                                    _setCustomFunctions.showPopup('Info', 'Datagroup has been Update successfully');
                                    $modalAdd.modal('hide');
                                } else {
                                    $(_formAdd + " :input").prop("disabled", false);
                                    _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                            }
                        });
                    }
                    else {
                        alert('Check Field');
                    }
                }
                else {
                    alert('Empty Field');
                }

            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }

                        }
                    }
                });
            },
            showConfirmPopup: function (param, data, title, content) {
                if (param == 1) {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeleteDatagroup(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }
                else {
                    $.confirm({
                        title: title,
                        content: content,
                        type: 'blue',
                        typeAnimated: true,
                        buttons: {
                            YES: {
                                text: 'YES',
                                btnClass: 'btn-red',
                                action: function () {
                                    _setCustomFunctions.DeleteSubData(data);
                                }
                            },
                            NO: {
                                text: 'NO',
                                btnClass: 'btn-blue',
                                action: function () {
                                    //$modalAdd.modal('toggle');
                                }
                            }
                        }
                    });
                }


            },
            SaveSubData: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveSubData',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {

                            var table = document.getElementById("tblSubdatagroup");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAddsubData + " :input").prop("disabled", false);
                            $modalAddSubDataGroup.modal('hide');
                            _setCustomFunctions.setTableSubData($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'SubData has been Save successfully');
                        } else {
                            var table = document.getElementById("tblSubdatagroup");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $modalAddSubDataGroup.modal('hide');
                            _setCustomFunctions.setTableSubData($('input[name=codeEdit]').val());
                            $(_formAddsubData + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            EditSubData: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/EditSubData',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $modalAddSubDataGroup.modal('hide');
                            var table = document.getElementById("tblSubdatagroup");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            _setCustomFunctions.setTableSubData($('input[name=codeEdit]').val());
                            $(_formAddsubData + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'SubData has been Update successfully');
                        } else {
                            $(_formAddsubData + " :input").prop("disabled", false);
                            var table = document.getElementById("tblSubdatagroup");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $modalAddSubDataGroup.modal('hide');
                            _setCustomFunctions.setTableSubData($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'Failed to Edit');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteSubData: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteSubData',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            //$('#tblSubdatagroup').ajax.reload();
                            //var tables = $('#tblSubdatagroup');
                            var table = document.getElementById("tblSubdatagroup");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $modalAddSubDataGroup.modal('hide');
                            _setCustomFunctions.setTableSubData($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'SubData has been Delete successfully');
                        } else {
                            //$(_formAdd + " :input").prop("disabled", false);
                            var table = document.getElementById("tblSubdatagroup");
                            //or use :  var table = document.all.tableid;
                            for (var i = table.rows.length - 1; i > 0; i--) {
                                table.deleteRow(i);
                            }
                            $(_formAdd + " :input").prop("disabled", false);
                            $modalAddSubDataGroup.modal('hide');
                            _setCustomFunctions.setTableSubData($('input[name=codeEdit]').val());
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
        }

    }

    Page.initialize();
})