﻿var listUser = null;
var listActiveUser = null, listOrdinaryUser = null, listSurveyor = null, listInactiveUser = null, listPendingUser = null, listCloneUser = null;
var chatHub = null;
var allUser = [];
var _uname = [];
var UsersID = ""; 
$(function () { 
    //Set Link Active 
    var url = window.location.href;
    var url_location = window.location;

    // for sidebar menu entirely but not cover treeview
    $('ul.sidebar-menu a').filter(function () {
        return this.href == url_location;
    }).parent().addClass('active');
    //Top bar
    $('ul.navbar-nav a').filter(function () {
        return this.href == url_location;
    }).parent().addClass('active');

    // for treeview
    $('ul.treeview-menu a').filter(function () {
        return this.href == url_location;
    }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');

    // Declare a proxy to reference the hub. 
    chatHub = $.connection.chatHub;
    RegisterClientMethods(chatHub);
    // Start Hub
    $.connection.hub.start().done(function () {
        RegisterEvents(chatHub)
    });
    
    if (url.indexOf('dashboard') > -1) {
        console.log('Call Function in cor_dashboard.js');

        SetInitDashboard();
    } 
    else if (url.indexOf('alluser') > -1) {
        SetupUser();
    }

    else if (url.indexOf('userdetail') > -1) {
        SetupUserDetail();
    }

    else if (url.indexOf('verificationadmin') > -1) {
        SetupVerificationAdmin();
    }




    $.ajax({
        "dataType": 'json',
        "contentType": "application/json; charset=utf-8",
        "type": "GET",
        "url": "../../webservice/WebService_COR.asmx/AssignUsers_All",
        "success": function (msg) {
            //console.info("msg", msg.d);
            allUser = JSON.parse(msg.d)
        },
        error: function (xhr, textStatus, error) {
            alert("error");
        }
    });


    $.ajax({
        type: 'GET',
        url: '../../webservice/WebService_COR.asmx/GetUserDetail',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            json.forEach(function (obj) {
                if (obj.UserAvatar == null || obj.UserAvatar == "") {
                    if (obj.UserGender == "Male") {
                        document.getElementById("profileImage2").src = "../../dist/img/avatar5.png"
                        document.getElementById("profileImage3").src = "../../dist/img/avatar5.png"
                        document.getElementById("profileImage4").src = "../../dist/img/avatar5.png"
                        var yourImg = document.getElementById('profileImage3');
                        if (yourImg && yourImg.style) {
                            yourImg.style.height = '30px';
                            yourImg.style.width = '30px';
                        }
                    }
                    else {
                        document.getElementById("profileImage2").src = "../../dist/img/avatar2.png"
                        document.getElementById("profileImage3").src = "../../dist/img/avatar2.png"
                        document.getElementById("profileImage4").src = "../../dist/img/avatar2.png"
                        var yourImg = document.getElementById('profileImage3');
                        if (yourImg && yourImg.style) {
                            yourImg.style.height = '30px';
                            yourImg.style.width = '30px';
                        }
                    }
                }
                else {
                    var _url = "Upload//UploadProfilePictCompress//" + obj.UserAvatar;
                    $.ajax({
                        type: 'POST',
                        url: '../../webservice/WebService_COR.asmx/CheckImageExist',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: "{'url' :'" + _url + "'}",
                        success: function (response) {
                            if (response.d == "true") {
                                document.getElementById("profileImage2").src = "../../Upload/UploadProfilePictCompress/" + obj.UserAvatar
                                document.getElementById("profileImage3").src = "../../Upload/UploadProfilePictCompress/" + obj.UserAvatar
                                document.getElementById("profileImage4").src = "../../Upload/UploadProfilePictCompress/" + obj.UserAvatar
                                var yourImg = document.getElementById('profileImage3');
                                if (yourImg && yourImg.style) {
                                    yourImg.style.height = '30px';
                                    yourImg.style.width = '30px';
                                }
                            }
                            else {
                                if (obj.UserGender == "Male") {
                                    document.getElementById("profileImage2").src = "../../dist/img/avatar5.png"
                                    document.getElementById("profileImage3").src = "../../dist/img/avatar5.png"
                                    document.getElementById("profileImage4").src = "../../dist/img/avatar5.png"
                                    var yourImg = document.getElementById('profileImage3');
                                    if (yourImg && yourImg.style) {
                                        yourImg.style.height = '30px';
                                        yourImg.style.width = '30px';
                                    }
                                }
                                else {
                                    document.getElementById("profileImage2").src = "../../dist/img/avatar2.png"
                                    document.getElementById("profileImage3").src = "../../dist/img/avatar2.png"
                                    document.getElementById("profileImage4").src = "../../dist/img/avatar2.png"
                                    var yourImg = document.getElementById('profileImage3');
                                    if (yourImg && yourImg.style) {
                                        yourImg.style.height = '30px';
                                        yourImg.style.width = '30px';
                                    }
                                }
                            };

                        }

                    });
                }

            })
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
})


$.ajax({
    type: "GET",
    url: '../../webservice/WebService_COR.asmx/GetUserName',
    //data: { UserName: JSON.stringify(username) },
    contentType: 'application/json; charset=utf-8',
    dataType: 'json',

    success: function (response) {
        //var json = $.parseJSON(response.d);
        _uname = [];
        var json = jQuery.parseJSON(response.d);
        for (var i = 0 ; i < json.length; i++) {
            _uname.push(json[i].Nama);
        }
    },
    error: function (xhr, ajaxOptions, thrownError) {
    }
});





function PushNotif() {
    $.confirm({
        title: 'Broadcast Message',
        boxWidth: '300px',
        useBootstrap: false,
        content: '' +
        '<form action="" class="formName">' +
        '<div class="form-group"><div class="col-md-12">' +
        '<label>Remarks</label>' +
        '<input type="text" id="remarks" placeholder="Remarks" class="name form-control" required />' +
        '</div></div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: 'Submit',
                btnClass: 'btn-blue',
                action: function () {
                    var msg = $("#remarks").val();

                    if (msg.length > 0) {

                        var userName = $('#hdUserName').val();

                        var date = GetCurrentDateTime(new Date());

                        chatHub.server.sendMessageToAll(userName, msg, date);
                        $("#remarks").val('');
                    }
                }
            },
            cancel: function () {
                //close
            },
        }
    });
}

function RegisterEvents(chatHub) {
    var name = '<%= this.UserName %>';

    if (name.length > 0) {
        chatHub.server.connect(name);
    }
}

function RegisterClientMethods(chatHub) {
    // Calls when user successfully logged in
    chatHub.client.onConnected = function (id, userName, allUsers, messages, times) {

        $('#hdId').val(id);
        $('#hdUserName').val(userName);
        $('#spanUser').html(userName);
    }

    // On New User Connected
    chatHub.client.onNewUserConnected = function (id, name, UserImage, loginDate) {
        //AddUser(chatHub, id, name, UserImage, loginDate);
    }


    // On User Disconnected
    chatHub.client.onUserDisconnected = function (id, userName) {

    }

    chatHub.client.messageReceived = function (userName, message, time, userimg) {
        ShowToast('Information', message, 'info');
        //AddMessage(userName, message, time, userimg);
    }


}

function GetCurrentDateTime(now) {

    var localdate = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT");

    return localdate;
}

function ShowToast(heading, text, type) {
    if (type == 'info') {
        $.toast({
            text: text,
            heading: heading,
            position: 'top-center',
            bgColor: '#2ecc71',
            hideAfter: true,
            textColor: '#fff',
        });
    }
}
