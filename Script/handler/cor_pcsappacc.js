﻿$(function () {
    var Page = {
        options: {
            formAdd: '#formAdd',
            addButton: '#btnAddappacc',
            saveButton: '#btnSave',
            modalAdd: '#mdlAdd',
            tabappAcc: '#tabappAcc',
            table: '#tblappacc',
            selectArea: '#selectArea',
            selectDepartment: '#selectDepartment',
        },
        initialize: function () {
            this.setVars();
            this.setTable();
            this.setCustomFunctions.init();
            this.setEvents.init();
        },
        setVars: function () {
            $formAdd = $(this.options.formAdd);
            $addButton = $(this.options.addButton);
            $saveButton = $(this.options.saveButton);
            $modalAdd = $(this.options.modalAdd);
            $tabappacc = $(this.options.tabappAcc);
            $table = $(this.options.table);
            $selectArea = $(this.options.selectArea);
            $selectDepartment = $(this.options.selectDepartment);
            _formAdd = this.options.formAdd;
            _tabappAcc = this.options.$tabappacc;
            _listattday = [];
            _listattdayCode = [];
            _isEditTabExists = false;
            _currentEditTabId = "";
            _setEvents = this.setEvents;
            _setCustomFunctions = this.setCustomFunctions;
        },
        setTable: function () {
            //Initialization of main table or data here
            var table = $table.DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 25,
                "filter": true,
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 0,
                    width: 5
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 1,
                    width: 250
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 2,
                    width: 500
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 3,
                    width: 500
                }, {
                    "searchable": false,
                    "orderable": false,
                    "class": "left",
                    "targets": 4,
                    width: 500
                },{
                    "searchable": false,
                    "orderable": false,
                    "class": "center",
                    "targets": 5,
                    width: 10
                }],
                "orderClasses": false,
                "order": [[1, "asc"]],
                "info": true,
                //"scrollY": "450px",
                //"scrollCollapse": true,
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "../../webservice/WebService_COR.asmx/GetAppAcc_ServerSideProcessing",
                "fnServerData": function (sSource, aoData, fnCallback) {
                    $.ajax({
                        "dataType": 'json',
                        "contentType": "application/json; charset=utf-8",
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": function (msg) {
                            var json = jQuery.parseJSON(msg.d);
                            console.log();
                            fnCallback(json);
                            $("#tblappacc").show();
                        },
                        error: function (xhr, textStatus, error) {
                            if (typeof console == "object") {
                                console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                            }
                        }
                    });
                },
                fnDrawCallback: function () {
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    $('td:eq(0)', nRow).html(index);
                }
            });
        },
        setEvents: {
            init: function () {
                $addButton.on('click', this.add);
                $saveButton.on('click', this.save);
                $table.on('click', 'a[data-target="#delete"]', this.delete);

                $tabappacc.on('click', 'a', this.tabclick);
                //this.formvalidation();

                //select event
                $selectArea.on('change', this.onChangeArea);
                $selectDepartment.on('change', this.onChangeDepartment);
            },
            formvalidation: function () {
                //Add On For Validation Rule

                $.validator.addMethod("alpha", function (value, element) {

                    return this.optional(element) || value == value.match(/^[a-zA-Z, '']+$/);

                }, "Alphabetic characters only please");

                //Main Validation
                $formAdd.validate({
                    rules: {
                        code: {
                            minlength: 2,
                            maxlength: 2,
                            alpha: true,
                            required: true,
                            remote: function () {
                                //document.getElementById('errormsg').style.display = 'none';
                                return {
                                    url: "../../webservice/WebService_COR.asmx/GetattdayData",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    data: "{'attendence dayCode' :'" + $('input[name=code]').val() + "'}",
                                    dataFilter: function (data) {
                                        var msg = JSON.parse(data);
                                        var json = $.parseJSON(msg.d);
                                        if (json.length > 0) {
                                            //{
                                            document.getElementById('lblcode').style.color = '#dd4b39';
                                            document.getElementById('code').style.borderColor = '#dd4b39';
                                            //document.getElementById('help-block').style.display = 'block';
                                            document.getElementById('errormsg').style.display = 'block';
                                            document.all('errormsg').innerHTML = "Code sudah terdaftar pada sistem";
                                        }
                                    }
                                }


                            },
                        },
                        name: {
                            minlength: 1,
                            alpha: true,
                            required: true
                        }
                    },
                    highlight: function (element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function (element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function (error, element) {
                        if (element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            },
            resetformvalidation: function () {
                $('.form-group').removeClass('has-error has-feedback');
                $('.form-group').find('span.help-block').hide();
                $('.form-group').find('i.form-control-feedback').hide();
            },
            tabclick: function () {
                //e.preventDefault();

            },
            tabchange: function (tabId) {


            },
            onChangeDepartment: function () {
                _setCustomFunctions.getMasterUserApprovalAccess($("#selectDepartment").val(), $("#selectArea").val());
            },
            onChangeArea: function () {
                _setCustomFunctions.getMasterUserApprovalAccess($("#selectDepartment").val(), $("#selectArea").val());
            },
            //selectchange: function () {
            //    GetMasterModuleSubType($(this).val());
            //},
            add: function () {
                //Event for adding data
                $modalAdd.modal('toggle');
                //document.getElementById('help-block').style.display = 'block';
                //document.getElementById('errormsg').style.display = 'none';
                document.getElementById('btnSave').style.display = 'inline';
                document.all('lbl').innerHTML = "Add Approval Access";
                $("#selectUser").val(0);
                $('.selectpicker').selectpicker('refresh');
                _setEvents.formvalidation();

            },
            delete: function () {
                //Event for deleting data
                var $this = $(this);
                //$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                var $this = $(this);
                var $row = $(this).closest('tr');
                var userID = $row.find('.userID').html();
                //Set Ajax Submit Here
                var data = { userID: userID };
                //var savetype = 

                _setCustomFunctions.showConfirmPopup(data, 'confirm', 'Are You Sure Delete This Data?');


                return false;
            },
            save: function () {
                //Event for saving data
                console.log('test', $formAdd);
                if ($formAdd.valid()) {
                    var $this = $(this);
                    $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
                    $("#formAdd :input").prop("disabled", true);

                    //Set Ajax Submit Here
                    var data = { ListUser: $("#selectUser").val()};
                    //var savetype = 
                    _setCustomFunctions.saveappAcc(data);

                    return false;
                }
            },
        },
        setCustomFunctions: {
            init: function () {
                this.getMasterAreaEuclid();
                this.getMasterDepartmentEuclid();
                //this.getMasterModuleType();
                _setCustomFunctions.getMasterUserApprovalAccess('', '');
            },
            registerTabContent: function (tabId, mdlcode) {
            },
            getMasterUserApprovalAccess: function (Department, Area) {
                $(".loading").show();
                $(".loader").show();
                var filter = { DeptCode: Department, area: Area };
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/MasterUserApprovalAccess',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({ dataobject: JSON.stringify(filter) }),
                    dataType: 'json',
                    success: function (response) {
                        var jsonData = JSON.parse(response.d);
                        $('#selectUser').find('option').remove();
                        $('.selectpicker').selectpicker('refresh');
                 
                        for (var i = 0; i < jsonData.length; i++) {
                            $("#selectUser").append($('<option>', {
                                value: jsonData[i]["UserID"],
                                text: jsonData[i]["UserFullName"]
                            }));
                        }

                        $('.selectpicker').selectpicker('refresh');
                        //$(".loader").hide();
                    }
                });
            },
            getMasterAreaEuclid: function () {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/MasterAreaEuclid',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var jsonData = JSON.parse(response.d);
                        $("#selectArea").append($('<option>', {
                            value: "",
                            text: "ALL"
                        }));
                        for (var i = 0; i < jsonData.length; i++) {
                            $("#selectArea").append($('<option>', {
                                value: jsonData[i]["Area"],
                                text: jsonData[i]["Area"]
                            }));
                        }

                        $('.selectpicker').selectpicker('refresh');
                    }
                });
            },
            getMasterDepartmentEuclid: function () {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/MasterDepartmentEuclid',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var jsonData = JSON.parse(response.d);
                        $("#selectDepartment").append($('<option>', {
                            value: "",
                            text: "ALL"
                        }));
                        for (var i = 0; i < jsonData.length; i++) {
                            $("#selectDepartment").append($('<option>', {
                                value: jsonData[i]["Department"],
                                text: jsonData[i]["Department"]
                            }));
                        }

                        $('.selectpicker').selectpicker('refresh');
                    }
                });
            },
            saveappAcc: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/SaveAppAcc',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $saveButton.html("Save");
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            $modalAdd.modal('hide');
                            _setCustomFunctions.showPopup('Info', 'approval access has been saved successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            DeleteappAcc: function (data) {
                $.ajax({
                    type: 'POST',
                    url: '../../webservice/WebService_COR.asmx/DeleteappAcc',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $(_formAdd + " :input").prop("disabled", false);
                            $table.DataTable().ajax.reload();
                            _setCustomFunctions.showPopup('Info', 'approval access has been Delete successfully');
                        } else {
                            $(_formAdd + " :input").prop("disabled", false);
                            _setCustomFunctions.showPopup('Info', 'Failed to Delete');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            showPopup: function (title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        OK: {
                            text: 'OK',
                            btnClass: 'btn-blue',
                            action: function () {

                            }
                        }
                    }
                });

            },
            showConfirmPopup: function (data, title, content) {
                $.confirm({
                    title: title,
                    content: content,
                    type: 'blue',
                    typeAnimated: true,
                    buttons: {
                        YES: {
                            text: 'YES',
                            btnClass: 'btn-red',
                            action: function () {
                                _setCustomFunctions.DeleteappAcc(data);
                            }
                        },
                        NO: {
                            text: 'NO',
                            btnClass: 'btn-blue',
                            action: function () {
                                //$modalAdd.modal('toggle');
                            }
                        }
                    }
                });

            }
        }

    }

    Page.initialize();
})

