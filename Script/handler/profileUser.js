﻿$(function () {
    var userId;
    GetMasterModule();
    GetMasterArea();
    document.getElementById("loaderscreen").style.display = "inline";
    getDataUser();
    $("#reqBtn").click(function () {
        //alert($('#SelectEstateAcc').val() + ' - ' + $('#SelectmdlAcc').val());
        _AccEst = [];
        _AccMdl = [];

        for (var i = 0; i < $('#SelectEstateAcc').val().length; i++) {
            _AccEst.push($('#SelectEstateAcc').val()[i].split("-")[0].replace(" ", ""));
        };

        for (var i = 0; i < $('#SelectmdlAcc').val().length; i++) {
            _AccMdl.push($('#SelectmdlAcc').val()[i].split("-")[0].replace(" ", ""));
        };
        alert(_AccEst);
        alert(_AccMdl);
        $.ajax({
            type: 'POST',
            url: 'webservice/WebService_COR.asmx/ReqAccessUser',
            data: JSON.stringify({
                estAccess: _AccEst.toString(),
                mdlAccess: _AccMdl.toString()

            }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            success: function (response) {
                alert("Request has been sent");
                $("#SelectEstateAcc option:selected").removeAttr("selected");
                $("#SelectmdlAcc option:selected").removeAttr("selected");
                $("#SelectEstateAcc option:selected").prop("selected", false);
                $("#SelectmdlAcc option:selected").prop("selected", false);
                $('#SelectEstateAcc').selectpicker('refresh');
                $('#SelectmdlAcc').selectpicker('refresh');
            }
        });
    });

});
$("#downloadBtn").click(function () {

    var dt = new Date();
    let shortMonth = dt.toLocaleString('en-us', { month: 'short' }); /* Jun */
    var dates = `${dt.getDate().toString().padStart(2, '0')} ${shortMonth} ${dt.getFullYear().toString().padStart(4, '0')} ${dt.getHours().toString().padStart(2, '0')}:${dt.getMinutes().toString().padStart(2, '0')}:${dt.getSeconds().toString().padStart(2, '0')}`

    const javaScriptRelease = Date.parse(dates);

    var nmpdf = dateFormatReport(dt) + "_" + "ENT1" + "_" + $("#namaLengkap").text() + "_Laporan Summary Profile" + "_" + javaScriptRelease;
    var win = window.open('webservice/DownloadPDFHandler.ashx?userId=' + userId + '&fileRepX=ENT_ProfileUser' + '&namaFile=' + nmpdf, '_blank');
    win.focus();
});
$("#updBtn").click(function () {
    $.ajax({
        type: 'POST',
        url: 'webservice/WebService_COR.asmx/CheckPassword',
        data: JSON.stringify({
            Password: $("#oldPass").val()

        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (response) {
            var json = JSON.parse(response.d);
            if (json[0]['returnData'] == 1) {
                if ($('#newPass').val() == $('#conPass').val()) {
                    $.ajax({
                        type: 'POST',
                        url: 'webservice/WebService_COR.asmx/ChangePasswordProfile',
                        data: JSON.stringify({
                            Password: $("#newPass").val()

                        }),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        async: false,
                        success: function (response) {
                            alert("Request has been sent");
                        }
                    });
                }
                else {
                    alert("Password & Confirm Password Doesn't Match");

                }
            }
            else {
                alert("Incorrect Old Password");

            }

        }
    });

});


function dateFormatReport(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = parseInt(yy.toString().substring(2, 4)) + '' + mm + '' + dd;
}

function GetMasterModule() {
    var _parent = this;
    $.ajax({
        type: 'GET',
        url: 'webservice/WebService_COR.asmx/getMastermdlAccCode',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            //$('#SelectmdlAcc').find('option').remove();
            //json.forEach(function (obj) {
            //    $("#SelectmdlAcc").append($('<option>', {
            //        value: obj.mdlAccCode,
            //        text: obj.mdlAccDesc
            //    }));
            //})

            //$('#SelectmdlAcc').selectpicker('refresh');


            if (json.length > 0) {
                $('#SelectmdlAcc').find('option').remove();
                var optgroup = "", group = "", option = "";

                json.forEach(function (obj) {
                    if (obj.mdlDesc != optgroup) {
                        optgroup = obj.Grouping
                        optgroup = obj.mdlDesc
                        group = $('<optgroup label="' + optgroup + '" data-max-options="1"/>');
                    }

                    option = $('<option>', {
                        value: obj.mdlAccDesc,
                        text: obj.mdlAccDesc2
                    });

                    option.attr("data-tokens", optgroup + "," + obj.mdlAccDesc);
                    option.appendTo(group);
                    group.appendTo($('#SelectmdlAcc'));
                });

                $('#SelectmdlAcc').selectpicker('refresh');
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function GetMasterArea() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "webservice/WebService_COR.asmx/GetEstateNew",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            var json = $.parseJSON(data.d);

            if (json.length > 0) {
                $('#SelectEstateAcc').find('option').remove();
                var optgroup = "", optgroup2 = "", group = "", option = "";

                json.forEach(function (obj) {
                    if (obj.NewEstName.includes("Kebun") == true) {
                        if (obj.Grouping != optgroup) {
                            optgroup = obj.Grouping
                            group = $('<optgroup label="' + optgroup + '" />');
                        }

                        option = $('<option>', {
                            value: obj.estCode,
                            text: obj.Estate
                        });

                        option.attr("data-tokens", optgroup + "," + obj.Estate);
                        option.appendTo(group);
                        group.appendTo($('#SelectEstateAcc'));

                    }

                });

                $('#SelectEstateAcc').selectpicker('refresh');
            }


            //$('#selectFilterArea').find('option').remove();
            //json.forEach(function (obj) {
            //    $("#selectFilterArea").append($('<option>', {
            //        value: obj.Area,
            //        text: obj.Area
            //    }));
            //})

            //$('#selectFilterArea').selectpicker('refresh');
        }
    });
}


function getDataUser() {

    $.ajax({
        type: 'GET',
        url: 'webservice/WebService_COR.asmx/GetDataUser',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = JSON.parse(response.d);
            var mainInformation = JSON.parse(json['mainInformation']);
            var moduleAccess = JSON.parse(json['moduleAccess']);
            var estateAccess = JSON.parse(json['estateAccess']);
            var token = JSON.parse(json['token']);
            var deviceIn = JSON.parse(json['deviceIn']);
            var lastDownloadReport = JSON.parse(json['lastDownloadReport']);
            var lastLoginAndroid = JSON.parse(json['lastLoginAndroid']);
            var lastLoginWEB = JSON.parse(json['lastLoginWEB']);
            var lastUploadData = JSON.parse(json['lastUploadData']);
            var historyDownload = JSON.parse(json['historyDownload']);
            var historyUpload = JSON.parse(json['historyUpload']);
            var userCategory = JSON.parse(json['userCategory']);

            //mainInformation
            var userFullName = document.getElementById('userFullName');
            userFullName.innerHTML = mainInformation[0]["UserFullName"];

            //var emailAva = document.getElementById('UserEmail');
            //emailAva.innerHTML = mainInformation[0]["UserEmail"];
            userId = mainInformation[0]["UserID"];
            $('#emailAva').text(mainInformation[0]["UserEmail"]);
            $('#namaLengkap').text(mainInformation[0]["UserFullName"]);
            $('#department').text(mainInformation[0]["DeptName"]);
            $('#email').text(mainInformation[0]["UserEmail"]);
            $('#section').text(mainInformation[0]["SectionCode"]);
            $('#username').text(mainInformation[0]["UserName"]);
            $('#userCategory').text(userCategory[0]["CategoryUser"]);
            $('#telp').text(mainInformation[0]["UserHP"]);
            $('#kelompok').text(mainInformation[0]["usrGroupCode"]);
            $('#joinDate').text(mainInformation[0]["Mulai_Bekerja"]);

            //Activity&Log
            $('#lastLoginWEB').text((lastLoginWEB.length == 0 ? "" : lastLoginWEB[0]["LoginDate"]));
            $('#lastDwnlod').text((lastDownloadReport.length == 0 ? "" : lastDownloadReport[0]["reportName"]));
            $('#lastLoginAdmApps').text((lastLoginAndroid.length == 0 ? "" : lastLoginAndroid[0]["LoginDate"]));
            $('#lastUplod').text((lastUploadData.length == 0 ? "" : lastUploadData[0]["createDate"]));

            if (mainInformation[0]["UserAvatar"] == null || mainInformation[0]["UserAvatar"] == "") {
                var _url = "..//attachment//avatar//DefaultPhoto.png";
                $("#profileImage4").attr("src", _url);
                document.getElementById("loaderscreen").style.display = "none";

            }
            else {
                var _url = "..//attachment//avatar//" + mainInformation[0]["UserAvatar"];
                $("#profileImage4").attr("src", _url);

                setTableEstate(estateAccess);
                setTableModule(moduleAccess);
                setTableAsset(deviceIn);
                setTableToken(token);
                setTableHistoryDownload(historyDownload);
                setTableHistoryUpload(historyUpload);

                document.getElementById("loaderscreen").style.display = "none";
            }
        }
    });
}

function setTableModule(_dataMdl) {
    var json = _dataMdl;

    var content = '<font face="Sans-serif">';
    content += '<table id="tblAksesModule" class="table table-striped table-bordered" style="width:100%;font-size:12px;font-family:arial;">';
    content += '<thead><tr>';
    content += '<th style = "text-align: center !important;" > No. </th>';
    content += '<th style = "text-align: left !important;" > Module Code </th>';
    content += '<th style = "text-align: left !important;" > Module Name</th>';
    content += '<th style = "text-align: left !important;" > Access</th>';

    content += '</tr></thead><tbody>';
    for (var i = 0; i < json.length; i++) {
        if (json.length > 0) {
            content += '<tr>';
            content += '<td style="text-align: center;">' + (i + 1) + '</td>';
            content += '<td style="text-align: left;">' + json[i]["mdlacccode"] + '</td>';
            content += '<td style="text-align: left;">' + json[i]["mdlDesc"] + '</td>';
            content += '<td style="text-align: left;">' + json[i]["MdlDescription"] + '</td>';
            //content += '<td style="text-align: left;" onclick="javascript:detailAccess(\'' + json[i]["mdlacccode"] + '\',\'' + json[i]["MdlDescription"] + '\')">' + json[i]["MdlDescription"] + '</td>';
            content += '</tr>';
        }
    }
    content += '</tbody></table></font>';

    $("#dataTableModule").html(content);

    $("#tblAksesModule").DataTable({
        dom: 'ftlip',
        paging: false,
        info: false,
        bAutoWidth: false,
        "lengthChange": false,
        "pageLength": 5,
        "filter": true,
        "searching": true,
        aoColumns: [
            { sWidth: '5%' },
            { sWidth: '15%' },
            { sWidth: '25%' },
            { sWidth: '25%' }
        ]
    }).columns.adjust();
}
function setTableEstate(_dataEst) {
    var json2 = _dataEst;

    var content2 = '<font face="Sans-serif">';
    content2 += '<table id="tblAksesEstate" class="table table-striped table-bordered" style="width:100%;font-size:11px;font-family:arial;">';
    content2 += '<thead><tr>';
    content2 += '<th style = "text-align: center !important;" > No. </th>';
    content2 += '<th style = "text-align: left !important;" > Plantation </th>';
    content2 += '<th style = "text-align: left !important;" > Grouping</th>';
    content2 += '<th style = "text-align: left !important;" > Group Company Name</th>';
    content2 += '<th style = "text-align: left !important;" > Company Code</th>';
    content2 += '<th style = "text-align: left !important;" > EstCode</th>';
    content2 += '<th style = "text-align: left !important;" > EstNewCode</th>';
    content2 += '<th style = "text-align: left !important;" > EstCodeSAP</th>';
    content2 += '<th style = "text-align: left !important;" > Estate Name</th>';

    content2 += '</tr></thead><tbody>';
    for (var i = 0; i < json2.length; i++) {
        if (json2[i] != "") {
            content2 += '<tr>';
            content2 += '<td style="text-align: center;">' + (i + 1) + '</td>';
            content2 += '<td style="text-align: left;">' + json2[i]["Plantation"] + '</td>';
            content2 += '<td style="text-align: left;">' + json2[i]["Grouping"] + '</td>';
            content2 += '<td style="text-align: left;">' + json2[i]["GroupCompanyName"] + '</td>';
            content2 += '<td style="text-align: left;">' + json2[i]["CompanyCode"] + '</td>';
            content2 += '<td style="text-align: left;">' + json2[i]["EstCode"] + '</td>';
            content2 += '<td style="text-align: left;">' + json2[i]["EstNewCode"] + '</td>';
            content2 += '<td style="text-align: left;">' + json2[i]["EstCodeSAP"] + '</td>';
            content2 += '<td style="text-align: left;">' + json2[i]["NewEstName"] + '</td>';
            content2 += '</tr>';
        }
        else {

            content2 += '<tr>';
            content2 += '<td style="text-align: center;" colspan="8">' + 'No Access' + '</td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '</tr>';

        }
    }
    content2 += '</tbody></table></font>';

    $("#dataTableEstate").html(content2);

    $("#tblAksesEstate").DataTable({
        dom: 'ftlip',
        paging: false,
        info: false,
        bAutoWidth: false,
        "lengthChange": false,
        "pageLength": 5,
        "filter": true,
        "searching": true,
        aoColumns: [
            { sWidth: '5%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' }
        ]
    }).columns.adjust();

}
function setTableAsset(_dataAsset) {
    var json3 = _dataAsset;

    var content2 = '<font face="Sans-serif">';
    content2 += '<table id="tblAksesAsset" class="table table-striped table-bordered" style="width:100%;font-size:9px">';
    content2 += '<thead><tr>';
    content2 += '<th style = "text-align: center !important;" > No. </th>';
    content2 += '<th style = "text-align: left !important;" > Nama Device </th>';
    content2 += '<th style = "text-align: left !important;" > Asset No</th>';
    content2 += '<th style = "text-align: left !important;" > SIM </th>';
    content2 += '<th style = "text-align: left !important;" > IMEI </th>';
    content2 += '<th style = "text-align: left !important;" > Date Asset Buy</th>';
    content2 += '<th style = "text-align: left !important;" > Project Name</th>';
    content2 += '<th style = "text-align: left !important;" > Condition</th>';
    content2 += '<th style = "text-align: left !important;" > Asset Status</th>';
    content2 += '<th style = "text-align: left !important;" > Tgl Opname</th>';

    content2 += '</tr></thead><tbody>';
    for (var i = 0; i < json3.length; i++) {
        if (json3[i] != "") {
            content2 += '<tr>';
            content2 += '<td style="text-align: center;">' + (i + 1) + '</td>';
            content2 += '<td style="text-align: left;">' + json3[i]["deviceName"] + '</td>';
            content2 += '<td style="text-align: left;">' + json3[i]["AssetNo"] + '</td>';
            content2 += '<td style="text-align: left;">' + json3[i]["sim"] + '</td>';
            content2 += '<td style="text-align: left;">' + json3[i]["imei"] + '</td>';
            content2 += '<td style="text-align: left;">' + json3[i]["dateAssetBuy"] + '</td>';
            content2 += '<td style="text-align: left;">' + json3[i]["projectName"] + '</td>';
            content2 += '<td style="text-align: left;">' + json3[i]["assetCondition"] + '</td>';
            content2 += '<td style="text-align: left;">' + json3[i]["assetStatus"] + '</td>';
            content2 += '<td style="text-align: left;">' + json3[i]["tglOpname"] + '</td>';
            content2 += '</tr>';
        }
        else {

            content2 += '<tr>';
            content2 += '<td style="text-align: center;" colspan="8">' + 'No Access' + '</td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '<td style="display: none"></td>';
            content2 += '</tr>';

        }
    }
    content2 += '</tbody></table></font>';

    $("#dataTableAsset").html(content2);

    $("#tblAksesAsset").DataTable({
        dom: 'ftlip',
        paging: true,
        info: false,
        bAutoWidth: false,
        "lengthChange": false,
        "pageLength": 1,
        "filter": true,
        "searching": true,
        aoColumns: [
            { sWidth: '5%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' },
            { sWidth: '10%' }
        ]
    }).columns.adjust();

}
function setTableToken(_dataToken) {
    var json4 = _dataToken;
    var content = '<font face="Sans-serif">';
    content += '<table id="tblAksesToken" class="table table-striped table-bordered" style="width:100%;font-size:9px">';
    content += '<thead><tr>';
    content += '<th style = "text-align: center !important;" > No. </th>';
    content += '<th style = "text-align: left !important;" > Expired Date </th>';
    content += '<th style = "text-align: left !important;" > TokenId</th>';
    content += '<th style = "text-align: left !important;" > AndroidId</th>';
    content += '<th style = "text-align: left !important;" > Register Date</th>';

    content += '</tr></thead><tbody>';
    for (var i = 0; i < json4.length; i++) {
        if (json4.length > 0) {
            content += '<tr>';
            content += '<td style="text-align: center;">' + (i + 1) + '</td>';
            content += '<td style="text-align: left;">' + json4[i]["EXPIRATION_DATE"] + '</td>';
            content += '<td style="text-align: left;">' + json4[i]["android_id"] + '</td>';
            content += '<td style="text-align: left;">' + json4[i]["android_id"] + '</td>';
            content += '<td style="text-align: left;">' + json4[i]["dateActivation"] + '</td>';
            //content += '<td style="text-align: left;" onclick="javascript:detailAccess(\'' + json[i]["mdlacccode"] + '\',\'' + json[i]["MdlDescription"] + '\')">' + json[i]["MdlDescription"] + '</td>';
            content += '</tr>';
        }
    }
    content += '</tbody></table></font>';

    $("#dataTableToken").html(content);

    $("#tblAksesToken").DataTable({
        dom: 'ftlip',
        paging: true,
        info: false,
        bAutoWidth: false,
        "lengthChange": false,
        "pageLength": 2,
        "filter": true,
        "searching": true,
        aoColumns: [
            { sWidth: '5%' },
            { sWidth: '15%' },
            { sWidth: '25%' },
            { sWidth: '25%' },
            { sWidth: '25%' }
        ]
    }).columns.adjust();
}
function setTableHistoryDownload(_dataDwnld) {
    var json5 = _dataDwnld;

    var content = '<font face="Sans-serif">';
    content += '<table id="tblAksesDwnlod" class="table table-striped table-bordered" style="width:100%;font-size:9px">';
    content += '<thead><tr>';
    content += '<th style = "text-align: center !important;" > No. </th>';
    content += '<th style = "text-align: left !important;" > Module Code </th>';
    content += '<th style = "text-align: left !important;" > Report Name</th>';
    content += '<th style = "text-align: left !important;" > Total</th>';

    content += '</tr></thead><tbody>';
    for (var i = 0; i < json5.length; i++) {
        if (json5.length > 0) {
            content += '<tr>';
            content += '<td style="text-align: center;">' + (i + 1) + '</td>';
            content += '<td style="text-align: left;">' + json5[i]["module"] + '</td>';
            content += '<td style="text-align: left;">' + json5[i]["reportName"] + '</td>';
            content += '<td style="text-align: left;">' + json5[i]["total"] + '</td>';
            //content += '<td style="text-align: left;" onclick="javascript:detailAccess(\'' + json[i]["mdlacccode"] + '\',\'' + json[i]["MdlDescription"] + '\')">' + json[i]["MdlDescription"] + '</td>';
            content += '</tr>';
        }
    }
    content += '</tbody></table></font>';

    $("#dataTableDwnlod").html(content);

    $("#tblAksesDwnlod").DataTable({
        dom: 'ftlip',
        paging: true,
        info: false,
        bAutoWidth: false,
        "lengthChange": false,
        "pageLength": 10,
        "filter": true,
        "searching": true,
        aoColumns: [
            { sWidth: '5%' },
            { sWidth: '5%' },
            { sWidth: '25%' },
            { sWidth: '25%' }
        ]
    }).columns.adjust();
}
function setTableHistoryUpload(_dataUpld) {
    var json = _dataUpld;

    var content = '<font face="Sans-serif">';
    content += '<table id="tblAksesUplod" class="table table-striped table-bordered" style="width:100%;font-size:9px">';
    content += '<thead><tr>';
    content += '<th style = "text-align: center !important;" > No. </th>';
    content += '<th style = "text-align: left !important;" > Module Code </th>';
    content += '<th style = "text-align: left !important;" > Data Type</th>';
    content += '<th style = "text-align: left !important;" > Total</th>';

    content += '</tr></thead><tbody>';
    for (var i = 0; i < json.length; i++) {
        if (json.length > 0) {
            content += '<tr>';
            content += '<td style="text-align: center;">' + (i + 1) + '</td>';
            content += '<td style="text-align: left;">' + json[i]["mdlacccode"] + '</td>';
            content += '<td style="text-align: left;">' + json[i]["uploadType"] + '</td>';
            content += '<td style="text-align: left;">' + json[i]["countData"] + '</td>';
            //content += '<td style="text-align: left;" onclick="javascript:detailAccess(\'' + json[i]["mdlacccode"] + '\',\'' + json[i]["MdlDescription"] + '\')">' + json[i]["MdlDescription"] + '</td>';
            content += '</tr>';
        }
    }
    content += '</tbody></table></font>';

    $("#dataTableUplod").html(content);

    $("#tblAksesUplod").DataTable({
        dom: 'ftlip',
        paging: true,
        info: false,
        bAutoWidth: false,
        "lengthChange": false,
        "pageLength": 5,
        "filter": true,
        "searching": true,
        aoColumns: [
            { sWidth: '5%' },
            { sWidth: '5%' },
            { sWidth: '25%' },
            { sWidth: '25%' }
        ]
    }).columns.adjust();
}

function detailAccess(mdlCode, yourAcc) {
    mdlCode = mdlCode.replace(" ", "");
    $.ajax({
        url: 'webservice/WebService_COR.asmx/GetListModuleAccessByModule',
        type: "POST",
        data: "{'mdlCode' :'" + mdlCode + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var json = $.parseJSON(data.d);
            SetTableAccess(json, yourAcc);
        }
    })
};
function SetTableAccess(_dataMdl, yourAcc) {

    $("#mdlUser").modal('toggle');
    var json = _dataMdl;

    var content = '<font face="Helvetica" style="font-size:10px;">';
    content += '<table id="tblAksesSubModule" class="table table-striped table-bordered" style="width:100%; font-family: Gill Sans, Gill Sans MT, Calibri, Trebuchet MS, sans-serif;font-size: 12px;">';
    content += '<thead><tr>';
    content += '<th style = "text-align: left !important;" > No. </th>';
    content += '<th style = "text-align: left !important;" > Module Name</th>';
    content += '<th style = "text-align: left !important;" > Module Access</th>';
    content += '<th style = "text-align: left !important;" > Description</th>';

    content += '</tr></thead><tbody>';
    for (var i = 0; i < json.length; i++) {
        if (json.length > 0) {
            if (yourAcc == json[i].mdlAccCode) {
                content += '<tr>';
                content += '<td style="text-align: left;"><b>' + (i + 1) + '</b></td>';
                content += '<td style="text-align: left;"><b>' + json[i].mdlDesc + '</b></td>';
                content += '<td style="text-align: left;"><b>' + json[i].mdlAccDesc + '</b></td>';
                content += '<td style="text-align: left;"><b>' + json[i].mdlAccInfo + '</b></td>';
                content += '</tr>';
            }
            else {
                content += '<tr>';
                content += '<td style="text-align: left;">' + (i + 1) + '</td>';
                content += '<td style="text-align: left;">' + json[i].mdlDesc + '</td>';
                content += '<td style="text-align: left;">' + json[i].mdlAccDesc + '</td>';
                content += '<td style="text-align: left;">' + json[i].mdlAccInfo + '</td>';
                content += '</tr>';
            }
        }
    }
    content += '</tbody></table></font>';

    $("#tableDetailAccess").html(content);

    $("#tblAksesSubModule").DataTable({
        dom: 'tlip',
        searching: false,
        paging: false,
        info: false
    }).columns.adjust();
};