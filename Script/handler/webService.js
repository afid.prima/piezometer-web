﻿
$(function () {
    $.ajax({
        type: 'GET',
        url: '../../webservice/WebService_COR.asmx/GetUserDetail',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            json.forEach(function (obj) {
                if (obj.UserAvatar == null || obj.UserAvatar == "") {
                    var _url = "..//attachment//avatar///DefaultPhoto.png";
                    $("#profileImage4").attr("src", _url);

                }
                else {
                    var _url = "..//attachment//avatar//" + obj.UserAvatar;
                    $("#profileImage4").attr("src", _url);

                }
                $('#NamaDepan').text(obj.UserFullName);
                $('#fullName').text(obj.UserFullName);
                $('#userName').text(obj.UserName);
                $('#email').text(obj.UserEmail);
                $('#jabatan').text(obj.Jabatan);
                $('#category').text(obj.CategoryUser);
            });
        }
    });
});