﻿$(function () {
    var url = new URL(window.location.href);
    var Code = url.searchParams.get("ver");
    $.ajax({
        url: 'webservice/WebService_COR.asmx/CheckDateVerification',
        type: "POST",
        data: "{'Code' :'" + Code + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var json = $.parseJSON(data.d);
            json.forEach(function (obj) {
                if (obj.Status == "false") {
                    if (Code != null) {
                        $.ajax({
                            url: 'webservice/WebService_COR.asmx/CheckVerification',
                            type: "POST",
                            data: "{'Code' :'" + Code + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                if (data.d == "success") {
                                    //document.all('lblNotification').innerHTML = "Verifikasi Berhasil, Silahkan Ubah Password Anda";
                                    
                                    $.ajax({
                                        url: 'webservice/WebService_COR.asmx/GetAccessUser',
                                        type: "POST",
                                        data: "{'Code' :'" + Code + "'}",
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (data) {
                                            var json = $.parseJSON(data.d);
                                            json.forEach(function (obj) {
                                                document.all('mdlAcc').innerHTML = obj.mdlCode;
                                                document.all('EstAcc').innerHTML = obj.estCode;

                                                var _dataTable = [];
                                                var _dataTree = [];
                                                var currentTab = 0; // Current tab is set to be the first tab (0)
                                                var Page = {
                                                    options: {
                                                        formVerification: '#formVerification',
                                                        tableUser: '#tblusers',
                                                        modalAdd: '#mdlAdd',
                                                        modalAdd2: '#mdlAdd2',
                                                        modalUser: '#mdlUser',
                                                        setAccessButton: '#btnSetAccess',
                                                        BtnChangePassword: '#BtnChangePassword',
                                                        SavemdlBtn: '#SavemdlBtn',
                                                        SaveEstBtn: '#SaveEstBtn',

                                                    },
                                                    initialize: function () {
                                                        this.setVars();
                                                        this.setEvents.init();
                                                        this.setCustomFunctions.init();
                                                        this.setTableUserAcc(0);
                                                    },
                                                    setVars: function () {
                                                        $formVerification = $(this.options.formVerification);
                                                        _setEvents = this.setEvents;
                                                        _setCustomFunctions = this.setCustomFunctions;
                                                        $modalAdd = $(this.options.modalAdd);
                                                        $modalAdd2 = $(this.options.modalAdd2);
                                                        $modalUser = $(this.options.modalUser);
                                                        $tableUser = $(this.options.tableUser);
                                                        $setAccessButton = $(this.options.setAccessButton);
                                                        $BtnChangePassword = $(this.options.BtnChangePassword);
                                                        $SavemdlBtn = $(this.options.SavemdlBtn);
                                                        $SaveEstBtn = $(this.options.SaveEstBtn);
                                                    },
                                                    setTableUserAcc: function (UsersID) {
                                                        var table = $('#tblusers').DataTable({
                                                            dom: 'lfrtip',
                                                            lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                                                            pageLength: 100,
                                                            "filter": true,
                                                            "columnDefs": [{
                                                                "searchable": false,
                                                                "orderable": false,
                                                                "class": "center",
                                                                "targets": 0,
                                                                width: 5
                                                            }, {
                                                                "searchable": false,
                                                                "orderable": false,
                                                                "class": "center",
                                                                "targets": 1,
                                                                width: 400
                                                            }, {
                                                                "searchable": false,
                                                                "orderable": false,
                                                                "class": "left",
                                                                "targets": 2
                                                            }, {
                                                                "searchable": false,
                                                                "orderable": false,
                                                                "class": "center",
                                                                "targets": 3,
                                                                width: 10
                                                            }, {
                                                                "searchable": false,
                                                                "orderable": false,
                                                                "class": "center",
                                                                "targets": 4,
                                                                width: 10
                                                            }],
                                                            "orderClasses": false,
                                                            "order": [[1, "asc"]],
                                                            "info": true,
                                                            //"scrollY": "450px",
                                                            //"scrollCollapse": true,
                                                            "bProcessing": true,
                                                            "bServerSide": true,
                                                            "sAjaxSource": "webservice/WebService_COR.asmx/GetModule2_ServerSideProcessing",
                                                            "fnServerData": function (sSource, aoData, fnCallback) {
                                                                var _userid = "";
                                                                if (UsersID != "" && UsersID != undefined) {
                                                                    _userid = UsersID;
                                                                    UsersID = "";
                                                                }
                                                                else {
                                                                    _userid = 0
                                                                }

                                                                aoData.push({ 'name': 'UserID', 'value': _userid });
                                                                $.ajax({
                                                                    "dataType": 'json',
                                                                    "contentType": "application/json; charset=utf-8",
                                                                    "type": "GET",
                                                                    "url": sSource,
                                                                    "data": aoData,
                                                                    "success": function (msg) {
                                                                        var json = jQuery.parseJSON(msg.d);
                                                                        fnCallback(json);
                                                                        $("#tblusers").show();
                                                                    },
                                                                    error: function (xhr, textStatus, error) {
                                                                        if (typeof console == "object") {
                                                                            console.log(xhr.status + "," + xhr.responseText + "," + textStatus + "," + error);
                                                                        }
                                                                    }
                                                                });
                                                            },
                                                            fnDrawCallback: function () {
                                                            },
                                                            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                                                                var info = $(this).DataTable().page.info();
                                                                var page = info.page;
                                                                var length = info.length;
                                                                var index = (page * length + (iDisplayIndex + 1));
                                                                $('td:eq(0)', nRow).html(index);
                                                            }
                                                        });
                                                    },
                                                    setEvents: {
                                                        init: function () {
                                                            _setEvents.formvalidation();
                                                            //_setEvents.showTab(currentTab); // Display the current tab
                                                            $tableUser.on('click', 'a[data-target="#edit"]', this.editUser);
                                                            $setAccessButton.on('click', this.setAccess);
                                                            $BtnChangePassword.on('click', this.ChangePassword);
                                                            $SavemdlBtn.on('click', this.savemdlAcc);
                                                            $SaveEstBtn.on('click', this.saveEstAcc);
                                                        },
                                                        formvalidation: function () {
                                                            //Main Validation
                                                            $formVerification.validate({
                                                                rules: {
                                                                    password: {
                                                                        required: true,

                                                                    }
                                                                },
                                                                messages: {
                                                                    password: {
                                                                        required: 'Mohon isi Kata Sandi Baru.'
                                                                    }
                                                                },
                                                                highlight: function (element) {
                                                                    $(element).closest('.form-group').addClass('has-error');
                                                                },
                                                                unhighlight: function (element) {
                                                                    $(element).closest('.form-group').removeClass('has-error');
                                                                },
                                                                errorElement: 'span',
                                                                errorClass: 'help-block',
                                                                errorPlacement: function (error, element) {
                                                                    if (element.parent('.input-group').length) {
                                                                        error.insertAfter(element.parent());
                                                                    } else {
                                                                        error.insertAfter(element);
                                                                    }
                                                                }
                                                            });
                                                        },
                                                        resetformvalidation: function () {
                                                        },
                                                        savemdlAcc: function () {
                                                            var datatable = [];
                                                            var datas = [];
                                                            var table = $('#tblusers').DataTable();

                                                            var headers = [];
                                                            $('#tblusers th').each(function (index, item) {
                                                                headers[index] = $(item).html();
                                                            });
                                                            $('#tblusers tr').has('td').each(function () {
                                                                var arrayItem = {};
                                                                $('td', $(this)).each(function (index, item) {
                                                                    arrayItem[headers[index]] = $(item).html();
                                                                });
                                                                datatable.push(arrayItem);
                                                            });

                                                            var $row = $(this).closest('tr');
                                                            _dataTable = [];
                                                            for (var i = 0 ; i < datatable.length; i++) {
                                                                var a = jQuery(datatable[i].Access).text();
                                                                var lastFive = a.substr(a.length - 4);

                                                                _dataTable.push(lastFive);
                                                            }
                                                            //console.info(_dataTable, '')
                                                            $modalAdd.modal('hide');
                                                        },
                                                        saveEstAcc: function () {
                                                            //console.info(_dataTree, '')
                                                            $modalAdd2.modal('hide');
                                                        },
                                                        ChangePassword: function (e) {
                                                            //if (!$('#check').is(':checked')) {
                                                            //    alert('Mohon Di Ceklist');
                                                            //}
                                                            //else {
                                                            e.preventDefault();
                                                            var Password = $('input[name=password]').val();
                                                            $.ajax({
                                                                type: 'POST',
                                                                url: 'webservice/WebService_COR.asmx/ChangePasswordRegister',
                                                                data: "{'Password' :'" + Password + "','Code' :'" + Code + "'}",
                                                                contentType: 'application/json; charset=utf-8',
                                                                dataType: 'json',
                                                                success: function (response) {
                                                                    if (response.d == 'success') {
                                                                        document.all('lblNotification').innerHTML = "Password Berhasil Di Ubah, Silahkan Lakukan <a target=_blank href=default2.aspx>Login </a><br />";
                                                                        alert("Password Berhasil Di Ubah, Silahkan Lakukan Login");
                                                                        //var _path = window.location.pathname.substring(0, 10);
                                                                        //var urls = location.protocol + "//" + location.host + _path;
                                                                        
                                                                        var _path = window.location.pathname;
                                                                        var urls = location.protocol;
                                                                        var test = window.location.hostname;
                                                                        window.location.href = urls + test + _path;
                                                                    } else {
                                                                        document.all('lblNotification').innerHTML = "Password Gagal Di Ubah, Silahkan Lakukan <a target=_blank href=default2.aspx>Login </a><br />";

                                                                    }
                                                                },
                                                                error: function (xhr, ajaxOptions, thrownError) {
                                                                }
                                                            });
                                                            //}

                                                        },
                                                        editUser: function () {
                                                            var $row = $(this).closest('tr');
                                                            mdlCode = $row.find('.mdlCode').html();
                                                            mdlAccCode = $row.find('.mdlCode_Access').html();
                                                            _setCustomFunctions.getAccess(mdlCode, mdlAccCode);
                                                            $modalUser.modal('toggle');
                                                        },
                                                        setAccess: function () {
                                                            var $row = $(this).closest('tr');
                                                            mdlCode = $('input[name=code]').val();
                                                            mdlAccCode = $('#SelectAccess option:selected').text();
                                                            mdlAccCodes = $('#SelectAccess option:selected').val();
                                                            $('#' + mdlCode).html(mdlAccCode);
                                                            $('#' + mdlCode + "_Access").html(mdlAccCodes);
                                                            $modalUser.modal('hide');
                                                            //

                                                        },
                                                    },
                                                    setCustomFunctions: {
                                                        init: function () {
                                                            this.setTree(0);
                                                        },
                                                        Test: function () {
                                                            $modalAdd.modal('toggle');
                                                        },
                                                        Test1: function () {
                                                            $modalAdd2.modal('toggle');
                                                        },
                                                        getAccess: function (mdlCode, mdlAccCode) {
                                                            $('input[name=mdlacccoder]').val(mdlAccCode);
                                                            $.ajax({
                                                                type: 'POST',
                                                                url: 'webservice/WebService_COR.asmx/GetMasterAccess',
                                                                contentType: 'application/json; charset=utf-8',
                                                                data: "{'mdlCode' :'" + mdlCode + "'}",
                                                                dataType: 'json',
                                                                success: function (response) {
                                                                    var json = $.parseJSON(response.d);
                                                                    $('#SelectAccess').find('option').remove();
                                                                    json.forEach(function (obj) {
                                                                        $("#SelectAccess").append($('<option>', {
                                                                            value: obj.mdlAccCode,
                                                                            text: obj.mdlAccDesc
                                                                        }));
                                                                    })

                                                                },
                                                                error: function (xhr, ajaxOptions, thrownError) {
                                                                }
                                                            });

                                                            $('input[name=code]').val(mdlCode);
                                                            $('input[name=mdlacccoder]').val(mdlCode);
                                                        },

                                                        setTree: function (UsersID) {
                                                            var datas = [];
                                                            var id = 1;
                                                            $.ajax({
                                                                url: 'webservice/WebService_COR.asmx/GetEstAccessUser',
                                                                contentType: 'application/json; charset=utf-8',
                                                                dataType: 'json',
                                                                type: "POST",
                                                                data: "{'UsersID' :'" + UsersID + "'}",
                                                                success: function (response) {
                                                                    var json = jQuery.parseJSON(response.d);
                                                                    for (var i = 0 ; i < json.length; i++) {
                                                                        datas.push({ text: json[i].text, id: json[i].id, parent: json[i].parent, state: { selected: json[i].state == 'true' ? true : false } });
                                                                    }
                                                                    var finaldata = "";
                                                                    if (UsersID == "") {
                                                                        finaldata = json;
                                                                    }
                                                                    else {
                                                                        finaldata = datas;
                                                                    }
                                                                    var $treeview = $("#tvestate");
                                                                    $('#tvestate').jstree('destroy');
                                                                    $('#tvestate').jstree({
                                                                        plugins: ['checkbox', 'changed'],
                                                                        'core': {
                                                                            "themes": { "icons": false },
                                                                            expand_selected_onload: true,
                                                                            'data':
                                                                                finaldata
                                                                        },
                                                                    }).bind("loaded.jstree", function (event, data) {
                                                                        //$(this).jstree("open_all");
                                                                        $treeview.jstree('open_all');
                                                                    });

                                                                    $('#tvestate')
                                                                    // listen for event
                                                                    .on('changed.jstree', function (e, data) {
                                                                        var i, j, r = [];
                                                                        for (i = 0, j = data.selected.length; i < j; i++) {
                                                                            r.push(data.instance.get_node(data.selected[i]).text);
                                                                        }
                                                                        _dataTree = r
                                                                    })
                                                                    // create the instance
                                                                    .jstree();
                                                                },
                                                                error: function (xhr, ajaxOptions, thrownError) {
                                                                }
                                                            });

                                                        },
                                                    }

                                                }

                                                Page.initialize();



                                            })

                                        }
                                    });
                                }
                            },
                            failure: function (msg) {
                                alert("Something go wrong! ");
                            }
                        });
                    }
                    else
                    {
                        document.getElementById('first').style.display = 'none';
                        document.getElementById('password').style.display = 'none';
                        document.getElementById('nextBtn').style.display = 'none';
                        document.getElementById('step').style.display = 'none';
                        document.all('lblNotification').innerHTML = "Link Salah, Silahkan Cek Email Untuk Melihat Link Verifikasi Anda";
                    }

                }
                else {
                    if (obj.StatusUser == "1")
                    {
                        document.getElementById('first').style.display = 'none';
                        document.getElementById('password').style.display = 'none';
                        document.getElementById('nextBtn').style.display = 'none';
                        document.getElementById('step').style.display = 'none';
                        document.all('lblNotification').innerHTML = "Anda Telah Terdaftar Pada Sistem, Silahkan Lakukan <a target=_blank href=default2.aspx>Login </a><br />&nbsp;";
                    }
                    else
                    {
                        document.all('lblNotification').innerHTML = "Link ini Sudah Expired, Silahkan Lakukan Kembali <a target=_blank href=register2.aspx>Registrasi</a>&nbsp;";
                    }
                    
                   
                }
            })
        },
        failure: function (msg) {
            alert("Something go wrong! ");
        }
    });
    function Test() {
        alert("test");
    }
})
