﻿function Main() {
    OpenLayers.ProxyHost = "../Handler/proxy.ashx?url=";
    map = new OpenLayers.Map('map', {
        controls: [
            new OpenLayers.Control.Navigation({
                dragPanOptions: {
                    enableKinetic: true
                }
            }),
            new OpenLayers.Control.PanZoomBar(),
            new OpenLayers.Control.ScaleLine(),
            new OpenLayers.Control.Scale(),
            new OpenLayers.Control.MousePosition(),
            new OpenLayers.Control.KeyboardDefaults()
        ],
        numZoomLevels: parseInt(globalnumzoom),
        projection: "EPSG:4326",
        displayProjection: "EPSG:4326"
    });

    globallayerfitur = new Array(globallayerDisplayName.length);
    globalidxblock = 0;
    globalidxinfo = 0;
    globaltreeindex = 0;
    globaltreeawal = "";
    globalfirstinfo = "";
    globalindexlayer = 0;
    globaljmllayer = new Array(globaljmlvector);
    var contentListLayer = "<table style='font-size:13px; font-family:Calibri'>";
    var contentLanduseLayer = "<table style='font-size:13px; font-family:Calibri'>";
    var contentOilPalmLayer = "<table style='font-size:13px; font-family:Calibri'>";

    var month;
    if (globaldate.split('/')[0][0] == '0') month = globaldate.split('/')[0][1];
    else month = globaldate.split('/')[0];
    for (var i = 0; i < globallayerDisplayName.length; i++) {
        if (globallayerCategory[i] == "base") {
            globallayerfitur[i] = new OpenLayers.Layer.WMS(
                globallayerDisplayName[i],
                globallayerURL[i], {
                    layers: "WMS_FROM_ARCGIS:" + globallayerURLName[i],
                    format: 'image/png'
                }, {
                    transitionEffect: 'resize',
                    tileSize: new OpenLayers.Size(256, 256)
                }
            );
        } else {
            var visible;
            if (globallayerDisplay[i] == "off") visible = false;
            else visible = true;

            if (globallayerCategory[i] == "label") {
                globallayerfitur[i] = new OpenLayers.Layer.WMS(
                    globallayerDisplayName[i],
                    globallayerURL[i], {
                        layers: globallayerURLName[i],
                        transparent: true
                    }, {
                        visibility: visible,
                        isBaseLayer: false,
                        transitionEffect: 'resize',
                        singleTile: true,
                        ratio: 1
                    }
                );
            } else {
                var layername = "";
                var baselayer = false;

                if (globallayerDisplayName[i] == "Info") {
                    layername = "PPB:PZOCALC";

                    globallayerfitur[i] = new OpenLayers.Layer.WMS(
                    globallayerDisplayName[i],
                    globallayerURL[i], {
                        viewparams: "PZOYEAR:" + globalyear + ";ESTNR:" + globalmappedestate + ";PZOMONTH:" + globalmonth + ";PZOWEEK:" + globalweek,
                        layers: layername,
                        transparent: true
                    }, {
                        visibility: visible,
                        isBaseLayer: baselayer,
                        singleTile: true,
                        buffer: 0,
                        displayOutsideMaxExtent: true,
                        ratio: 1,
                        transitionEffect: 'resize'
                    }
                    );
                }
                else {
                    if (globallayerDisplayName[i] == "Block") {
                        layername = "PZO:pzo_block_week";
                        baselayer = true;
                    }
                    else if (globallayerDisplayName[i] == "Piezometer")
                        layername = "PZO:PZO_Point_Week";

                    globallayerfitur[i] = new OpenLayers.Layer.WMS(
                        globallayerDisplayName[i],
                        globallayerURL[i], {
                            viewparams: "years:" + globalyear + ";estate:" + globalmappedestate + ";months:" + globalmonth + ";weeks:" + globalweek,
                            layers: layername,
                            transparent: true
                        }, {
                            visibility: visible,
                            isBaseLayer: baselayer,
                            singleTile: true,
                            buffer: 0,
                            displayOutsideMaxExtent: true,
                            ratio: 1,
                            transitionEffect: 'resize'
                        }
                    );
                }
            }
        }

        map.addLayers([globallayerfitur[i]]);

        //LayerContent
        if (globallayerCategory[i] == "vector") {
            if (globallayerDisplayName[i] == "Piezometer") {
                if (i == 0) {
                    if (globaltreename[i] != "") {
                        $("#selectlistLayer").append(new Option(globaltreename[i], globaltreename[i]));
                        globaljmllayer[globalindexlayer] = "d";
                        globalindexlayer++;
                    } else {
                        $("#selectlistLayer").append(new Option(globallayerDisplayName[i], globallayerDisplayName[i]));
                        globaljmllayer[globalindexlayer] = "e";
                        globalindexlayer++;
                    }
                } else {

                    if (globaltreename[i] != "" && globaltreename[i] != globaltreename[i - 1]) {
                        $("#selectlistLayer").append(new Option(globaltreename[i], globaltreename[i]));
                        if (globalfirstinfo == "") globalfirstinfo = "true";
                        else globalfirstinfo = "false";
                        globaljmllayer[globalindexlayer] = "b";
                        globalindexlayer++;
                    } else if (globaltreename[i] == "") {
                        $("#selectlistLayer").append(new Option(globallayerDisplayName[i], globallayerDisplayName[i]));
                        if (globalfirstinfo == "") globalfirstinfo = "true";
                        else globalfirstinfo = "false";
                        globaljmllayer[globalindexlayer] = "c";
                        globalindexlayer++;
                    }
                }
            }
        }
        //Content        

        if (i == 0) {
            contentListLayer += "<tr> <td></td> <td colspan='5'>" + globallayerTypeName[i] + "</td></tr>";

            if (globaltreename[i] != "") {
                globaltreeindex++;
                if (globaltreestatus[i] == "off") {
                    globaltreeawal = "off";
                    if (globallayercode[i] != "") {
                        contentListLayer += "<tr> <td><img id='imgTree" + globaltreeindex + "' alt='lrp' src='../Images/tree/plus.png' height='24' width='24' style='cursor:pointer;' /></td> <td><input type='checkbox' id='chktree" + globaltreeindex + "' /></td> <td colspan='4' style='cursor:pointer; color:blue;' onclick=LaporanKerja('" + globallayercode[i] + "','" + globallayermodule[i] + "','');>" + globaltreename[i] + "</td> </tr>";
                    } else {
                        contentListLayer += "<tr> <td><img id='imgTree" + globaltreeindex + "' alt='lrp' src='../Images/tree/plus.png' height='24' width='24' style='cursor:pointer;' /></td> <td><input type='checkbox' id='chktree" + globaltreeindex + "' /></td> <td colspan='4'>" + globaltreename[i] + "</td> </tr>";
                    }
                } else {
                    globaltreeawal = "on";
                    if (globallayercode[i] != "") {
                        contentListLayer += "<tr> <td><img id='imgTree" + globaltreeindex + "' alt='lrp' src='../Images/tree/minus.png' height='24' width='24' style='cursor:pointer;' /></td> <td><input type='checkbox' id='chktree" + globaltreeindex + "' /></td> <td colspan='4' style='cursor:pointer; color:blue;' onclick=LaporanKerja('" + globallayercode[i] + "','" + globallayermodule[i] + "','');>" + globaltreename[i] + "</td> </tr>";
                    } else {
                        contentListLayer += "<tr> <td><img id='imgTree" + globaltreeindex + "' alt='lrp' src='../Images/tree/minus.png' height='24' width='24' style='cursor:pointer;' /></td> <td><input type='checkbox' id='chktree" + globaltreeindex + "' /></td> <td colspan='4'>" + globaltreename[i] + "</td> </tr>";
                    }
                }
            }
        } else {
            if (globallayerTypeName[i] != globallayerTypeName[i - 1]) {
                contentListLayer += "<tr> <td></td> <td colspan='5'>" + globallayerTypeName[i] + "</td></tr>";
            }

            if (globaltreename[i] != "" && globaltreename[i] != globaltreename[i - 1]) {
                globaltreeindex++;
                if (globaltreestatus[i] == "off") {

                    if (globaltreeawal != "") {
                        globaltreeawal = globaltreeawal + ",off";
                    } else {
                        globaltreeawal = "off";
                    }
                    if (globallayercode[i] != "") {
                        contentListLayer += "<tr> <td><img id='imgTree" + globaltreeindex + "' alt='lrp' src='../Images/tree/plus.png' height='24' width='24' style='cursor:pointer;' /></td> <td><input type='checkbox' id='chktree" + globaltreeindex + "' /></td> <td colspan='4' style='cursor:pointer; color:blue;' onclick=LaporanKerja('" + globallayercode[i] + "','" + globallayermodule[i] + "','');>" + globaltreename[i] + "</td> </tr>";
                    } else {
                        contentListLayer += "<tr> <td><img id='imgTree" + globaltreeindex + "' alt='lrp' src='../Images/tree/plus.png' height='24' width='24' style='cursor:pointer;' /></td> <td><input type='checkbox' id='chktree" + globaltreeindex + "' /></td> <td colspan='4'>" + globaltreename[i] + "</td> </tr>";
                    }
                } else {

                    if (globaltreeawal != "") {
                        globaltreeawal = globaltreeawal + ",on";
                    } else {
                        globaltreeawal = "on";
                    }

                    if (globallayercode[i] != "") {
                        contentListLayer += "<tr> <td><img id='imgTree" + globaltreeindex + "' alt='lrp' src='../Images/tree/minus.png' height='24' width='24' style='cursor:pointer;' /></td> <td><input type='checkbox' id='chktree" + globaltreeindex + "' /></td> <td colspan='4' style='cursor:pointer; color:blue;' onclick=LaporanKerja('" + globallayercode[i] + "','" + globallayermodule[i] + "','');>" + globaltreename[i] + "</td> </tr>";
                    } else {
                        contentListLayer += "<tr> <td><img id='imgTree" + globaltreeindex + "' alt='lrp' src='../Images/tree/minus.png' height='24' width='24' style='cursor:pointer;' /></td> <td><input type='checkbox' id='chktree" + globaltreeindex + "' /></td> <td colspan='4'>" + globaltreename[i] + "</td> </tr>";
                    }
                }
            }
        }

        if (globallayerDisplay[i] == "on") {
            if (globaltreename[i] != "") {
                contentListLayer += "<tr  class='classTree" + globaltreeindex + "'> <td></td><td></td><td></td> <td><input type='checkbox' id='chk" + i + "'  checked='checked' class='classTreechk" + globaltreeindex + "' /></td>";
            } else {
                if (globallayerDisplayName[i] == "Info")
                    contentListLayer += "<tr> <td></td> <td><input type='checkbox' id='chk" + i + "'  checked='checked' style='display:none;'/></td>";
                else
                    contentListLayer += "<tr> <td></td> <td><input type='checkbox' id='chk" + i + "'  checked='checked'/></td>";
            }
        } else {
            if (globaltreename[i] != "") {
                contentListLayer += "<tr  class='classTree" + globaltreeindex + "'> <td></td><td></td><td></td> <td><input type='checkbox' id='chk" + i + "' class='classTreechk" + globaltreeindex + "' /></td>";
            } else {
                if (globallayerDisplayName[i] == "Info")
                    contentListLayer += "<tr> <td></td> <td><input type='checkbox' id='chk" + i + "' style='display:none;' /></td>";
                else
                    contentListLayer += "<tr> <td></td> <td><input type='checkbox' id='chk" + i + "'  /></td>";
            }
        }

        if (globaltreename[i] == "") {
            if (globallayercode[i] != "") {
                contentListLayer += "<td style='width:180px; cursor:pointer; color:blue;' colspan='3' onclick=LaporanKerja('" + globallayercode[i] + "','" + globallayermodule[i] + "','" + escape(globallayerstatus[i]) + "');>" + globallayerDisplayName[i] + "</td><td><div id=slider" + i + " style='width:60px;'></div></td></tr>";
            } else {
                //  ..       globallayerURL[i],
                ////        { layers: globallayerURLName[i]

                if (globallayerCategory[i] == "base") {
                    if (globalusergroup == "ADM" || globalusergroup == "STE")
                        contentListLayer += "<td style='width:180px;' colspan='3'><a style='cursor:pointer; color:blue;' onclick=SetWMS('" + globallayerid[i] + "','" + globallayerURL[i].replace(/ /g, "%20") + "','" + globallayerURLName[i].replace(/ /g, "%20") + "');> " + globallayerDisplayName[i] + "</a></td><td><div id=slider" + i + " style='width:60px;'></div></td></tr>";
                    else
                        contentListLayer += "<td style='width:180px;' colspan='3'>" + globallayerDisplayName[i] + "</td><td><div id=slider" + i + " style='width:60px;'></div></td></tr>";
                } else {
                    if (globallayerDisplayName[i] == "Piezometer")
                        contentListLayer += "<td style='width:180px;' colspan='3'><a style='cursor:pointer; color:blue;' onclick='GetListPiezometer();'> " + globallayerDisplayName[i] + "</a></td><td><div id=slider" + i + " style='width:60px;'></div></td></tr>";
                    else if (globallayerDisplayName[i] == "Info")
                        contentListLayer += "<td style='width:180px; display:none;' colspan='3'>" + globallayerDisplayName[i] + "</td><td><div id=slider" + i + " style='width:60px; display:none;'></div></td></tr>";
                    else
                        contentListLayer += "<td style='width:180px;' colspan='3'>" + globallayerDisplayName[i] + "</td><td><div id=slider" + i + " style='width:60px;'></div></td></tr>";
                }
            }
        } else {
            if (globallayercode[i] != "") {
                contentListLayer += "<td style='width:180px; cursor:pointer; color:blue;' onclick=LaporanKerja('" + globallayercode[i] + "','" + globallayermodule[i] + "','" + escape(globallayerstatus[i]) + "');>" + globallayerDisplayName[i] + "</td><td><div id=slider" + i + " style='width:60px;'></div></td></tr>";
            } else {
                contentListLayer += "<td style='width:180px'>" + globallayerDisplayName[i] + "</td><td><div id=slider" + i + " style='width:60px;'></div></td></tr>";
            }
        }

        //Info
        globalinfo = new Array(globaljmlvector);
        if (globallayerCategory[i] == "vector") {
            if (globaljmllayer[globalindexlayer - 1] == "a" || globaljmllayer[globalindexlayer - 1] == "b" || globaljmllayer[globalindexlayer - 1] == "c" || globaljmllayer[globalindexlayer - 1] == "d" || globaljmllayer[globalindexlayer - 1] == "e") {
                globaljmllayer[globalindexlayer - 1] = globalidxinfo;
            } else {

                globaljmllayer[globalindexlayer - 1] = globaljmllayer[globalindexlayer - 1] + "," + globalidxinfo;
            }

            GetLayerField(globallayerfieldID[i], i, globalidxinfo, globallayerURL[i], globalfirstinfo);
            globalidxinfo++;
        }
    }

    contentLanduseLayer += "</table>";
    contentOilPalmLayer += "</table>";
    $("#divlistlayer").html(contentListLayer);
    $("#divlanduselayer").html(contentLanduseLayer);
    $("#divoilpalmlayer").html(contentOilPalmLayer);
    //globallayerfitur[1].setVisibility(true);
    SetListLayer();
    //Controll
    //Overview
    var nyc = new OpenLayers.Bounds(
        118.45713, 0.79415, 118.68561, 0.88547
    );
    var options = {
        div: document.getElementById("dialogoverview"),
        mapOptions: {
            projection: new OpenLayers.Projection("EPSG:4326"),
            minResolution: 0.001
        }
    };
    map.addControl(new OpenLayers.Control.OverviewMap(options));

    //Zoom History
    var nav = new OpenLayers.Control.NavigationHistory();
    map.addControl(nav);
    zoomprevious = new OpenLayers.Control.Panel({
        div: document.getElementById("spanzoomprev")
    });
    zoomprevious.addControls([nav.previous]);
    map.addControl(zoomprevious);
    zoomnext = new OpenLayers.Control.Panel({
        div: document.getElementById("spanzoomnext")
    });
    zoomnext.addControls([nav.next]);
    map.addControl(zoomnext);

    //Drawing  
    //polygon
    var sketchSymbolizersDrawingPolygon = {
        "Polygon": {
            strokeWidth: 2,
            strokeOpacity: 1,
            strokeColor: "#E32222",
            fillColor: "#D3E0AD",
            fillOpacity: 0.3
        }
    };
    var stylePolygon = new OpenLayers.Style();
    stylePolygon.addRules([
        new OpenLayers.Rule({
            symbolizer: sketchSymbolizersDrawingPolygon
        })
    ]);
    var styleMapPolygon = new OpenLayers.StyleMap({
        "default": stylePolygon
    });

    //Line
    var sketchSymbolizersDrawingLine = {
        "Line": {
            strokeWidth: 2,
            strokeOpacity: 1,
            strokeColor: "#E32222"
        }
    };
    var styleLine = new OpenLayers.Style();
    styleLine.addRules([
        new OpenLayers.Rule({
            symbolizer: sketchSymbolizersDrawingLine
        })
    ]);
    var styleMapLine = new OpenLayers.StyleMap({
        "default": styleLine
    });

    //Point
    var sketchSymbolizersDrawingPoint = {
        "Point": {
            strokeWidth: 2,
            strokeOpacity: 1,
            strokeColor: "#E32222",
            fillColor: "#D3E0AD",
            fillOpacity: 0.3
        }
    };
    var stylePoint = new OpenLayers.Style();
    stylePoint.addRules([
        new OpenLayers.Rule({
            symbolizer: sketchSymbolizersDrawingPoint
        })
    ]);
    var styleMapPoint = new OpenLayers.StyleMap({
        "default": stylePoint
    });

    //List Vector Drawing
    pointLayer = new OpenLayers.Layer.Vector("Point Layer", {
        'displayInLayerSwitcher': false,
        styleMap: styleMapPoint
    });
    lineLayer = new OpenLayers.Layer.Vector("Line Layer", {
        'displayInLayerSwitcher': false,
        styleMap: styleMapLine
    });
    polygonLayer = new OpenLayers.Layer.Vector("Polygon Layer", {
        'displayInLayerSwitcher': false,
        styleMap: styleMapPolygon
    });
    boxLayer = new OpenLayers.Layer.Vector("Box layer", {
        'displayInLayerSwitcher': false,
        styleMap: styleMapPolygon
    });

    map.addLayers([pointLayer, lineLayer, polygonLayer, boxLayer]);

    drawControls = {
        point: new OpenLayers.Control.DrawFeature(pointLayer,
            OpenLayers.Handler.Point),
        line: new OpenLayers.Control.DrawFeature(lineLayer,
            OpenLayers.Handler.Path),
        polygon: new OpenLayers.Control.DrawFeature(polygonLayer,
            OpenLayers.Handler.Polygon),
        box: new OpenLayers.Control.DrawFeature(boxLayer,
            OpenLayers.Handler.RegularPolygon, {
                handlerOptions: {
                    sides: 4,
                    irregular: true
                }
            }
        )
    };

    for (var key in drawControls) {
        map.addControl(drawControls[key]);
    }

    //Measurement
    // style the sketch fancy
    var sketchSymbolizers = {
        "Point": {
            pointRadius: 4,
            graphicName: "square",
            fillColor: "white",
            fillOpacity: 1,
            strokeWidth: 1,
            strokeOpacity: 1,
            strokeColor: "#333333"
        },
        "Line": {
            strokeWidth: 3,
            strokeOpacity: 1,
            strokeColor: "#EFB135",
            strokeDashstyle: "dash"
        },
        "Polygon": {
            strokeWidth: 2,
            strokeOpacity: 1,
            strokeColor: "#EFB135",
            fillColor: "EAF7D0",
            fillOpacity: 0.3
        }
    };

    var style = new OpenLayers.Style();
    style.addRules([
        new OpenLayers.Rule({
            symbolizer: sketchSymbolizers
        })
    ]);
    var styleMap = new OpenLayers.StyleMap({
        "default": style
    });

    // allow testing of specific renderers via "?renderer=Canvas", etc
    var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
    renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

    measureControls = {
        line: new OpenLayers.Control.Measure(
            OpenLayers.Handler.Path, {
                persist: true,
                handlerOptions: {
                    layerOptions: {
                        renderers: renderer,
                        styleMap: styleMap
                    }
                }
            }
        ),
        polygon: new OpenLayers.Control.Measure(
            OpenLayers.Handler.Polygon, {
                persist: true,
                handlerOptions: {
                    layerOptions: {
                        renderers: renderer,
                        styleMap: styleMap
                    }
                }
            }
        )

    };

    var control;
    for (var key in measureControls) {
        control = measureControls[key];
        control.events.on({
            "measure": handleMeasurements,
            "measurepartial": handleMeasurements
        });
        map.addControl(control);
    }

    var extent = "";
    if (globalcurrentextent == "")
        extent = globalextent.split(",");
    else
        extent = globalcurrentextent.split(",");
    map.zoomToExtent(new OpenLayers.Bounds(parseFloat(extent[0]), parseFloat(extent[1]), parseFloat(extent[2]), parseFloat(extent[3])));
}