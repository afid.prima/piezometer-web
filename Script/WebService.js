﻿function ListLayer(estcode) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListLayer',
        data: '{EstCode: "' + estcode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            globallayerid = response.d[13].split(";");
            globallayerDisplayName = response.d[0].split(";");
            globallayerCategory = response.d[1].split(";");
            globallayerURL = response.d[2].split(";");
            globallayerURLName = response.d[3].split(";");
            globallayerDisplay = response.d[4].split(";");
            globallayerTypeName = response.d[5].split(";");
            globallayerfieldID = response.d[6].split(";");
            globaljmlvector = response.d[7].split(";");
            globaltreename = response.d[8].split(";");
            globaltreestatus = response.d[9].split(";");

            globallayercode = response.d[10].split(";");
            globallayermodule = response.d[11].split(";");
            globallayerstatus = response.d[12].split(";");

            Main();

            for (var i = 0; i < globallayerDisplayName.length; i++) {
                if (globallayerCategory[i] == "base") {
                    if (globallayerDisplay[i] == "off")
                        globallayerfitur[i].setVisibility(false);
                    else if (globallayerDisplay[i] == "on")
                        globallayerfitur[i].setVisibility(true);
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListLayer : " + xhr.statusText);
        }
    });
}

function GetBlockExtent(estcode, block) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetBlockExtent',
        data: '{EstCode: "' + estcode + '", Block: "' + block + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var bounds = new OpenLayers.Bounds();
            var latlonmin = new Array(2);
            var latlonmax = new Array(2);
            var xmin, ymin, xmax, ymax, zone, southhemi;
            zone = parseFloat(globalrsid.substring(0, globalrsid.length - 1));
            if (globalrsid.slice(-1) == "s" || globalrsid.slice(-1) == "S")
                southhemi = true;
            else
                southhemi = false;

            xmin = parseFloat(response.d.split(";")[0]);
            ymin = parseFloat(response.d.split(";")[1]);
            xmax = parseFloat(response.d.split(";")[2]);
            ymax = parseFloat(response.d.split(";")[3]);

            UTMXYToLatLon(xmin, ymin, zone, southhemi, latlonmin);
            UTMXYToLatLon(xmax, ymax, zone, southhemi, latlonmax);
            UTMXYToLatLon(xmin, ymin, zone, southhemi, latlonmin);
            UTMXYToLatLon(xmax, ymax, zone, southhemi, latlonmax);

            ZoomtoExtent(RadToDeg(latlonmin[1]), RadToDeg(latlonmin[0]), RadToDeg(latlonmax[1]), RadToDeg(latlonmax[0]));
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetBlockExtent : " + xhr.statusText);
        }
    });
}

function GetLayerField(fieldID, idxLayerFitur, idxInfo, url, firstinfo) {
    $(function () {
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/GetLayerField',
            data: "{FieldID: '" + fieldID + "',idxLayerFitur: '" + idxLayerFitur + "',idxInfo: '" + idxInfo + "',url: '" + url + "',firstinfo: '" + firstinfo + "'}",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var field = response.d[0].split(";");
                var idxlayer = response.d[1];
                var idxInfo = response.d[2];
                var url = response.d[3];
                var fieldAlias = response.d[4].split(";");
                var firstinfo = response.d[5];
                //INFOs
                globalinfo[idxInfo] = new OpenLayers.Control.WMSGetFeatureInfo({
                    url: url,
                    title: 'Identify features by clicking',
                    infoFormat: 'application/vnd.ogc.gml',
                    layers: [globallayerfitur[idxlayer]],
                    queryVisible: true,
                    eventListeners: {
                        beforegetfeatureinfo: function (event) {
                            this.vendorParams = { viewparams: "years:" + globalyear + ";estate:" + globalmappedestate + ";months:" + globalmonth + ";weeks:" + globalweek };
                        },
                        getfeatureinfo: function (event) {
                            $(function () {
                                try {
                                    if (event.features.length) {
                                        GetPiezoRecordDetailByPieRecordID(event.features[0].attributes["PieRecordID"]);
                                    }
                                } catch (err) {
                                    alert(err);
                                }


                            })
                        }
                    }, error: function (xhr, ajaxOptions, thrownError) {
                        alert("function GetLayerField : " + xhr.statusText);
                    }
                });


                map.addControl(globalinfo[idxInfo]);

                if (firstinfo == "true") {
                    globalinfo[idxInfo].activate();
                }
                else {
                    globalinfo[idxInfo].deactivate();
                }

            }
        });
    });
}

function WebServiceSetWMS() {
    $("#dialogloading").dialog("open");
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/SetWMS',
        data: '{layerid: "' + globallayerid + '",layerurl: "' + $("#textwmsurl").val() + '",layername: "' + $("#textwmsname").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {

            if (response.d != "error") {
                alert("WMS url and wms name has been changed !!");
                window.location.reload();
            }
            else alert("WMS url and wms name not changed !!");
            $("#dialogloading").dialog("close");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function WebServiceSetWMS : " + xhr.statusText);
        }
    });
}

function ListCompany() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListCompany',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataLength = response.d[0].split(";").length;

            $('#selectFilterCompany').find('option').remove();
            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterCompany").append($('<option>', {
                    value: dataValue[i],
                    text: dataText[i]
                }));
            }

            $("#selectFilterCompany").val(globalcompany).change();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListCompany : " + xhr.statusText);
        }
    });
}

function ListEstate(companycode) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListEstate',
        data: '{CompanyCode: "' + companycode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataLength = response.d[0].split(";").length;

            $('#selectFilterEstate').find('option').remove();
            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterEstate").append($('<option>', {
                    value: dataValue[i],
                    text: dataText[i]
                }));
            }

            if (globalisfirstload == true) {
                $("#selectFilterEstate").val(globalestate);
                $('#lbloperatingunit').html($('#selectFilterEstate option:selected').text());
                //$('#lbldate').html($.datepicker.formatDate('dd M yy', new Date(globaldate)));
                $('#lbldate').html("Week " + globalweek + ", " + globalmonthnamealias + " " + globalyear);
                globalisfirstload = false;
            }
            else
                $("#selectFilterEstate").val(dataValue[0]);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListEstate : " + xhr.statusText);
        }
    });
}

function ListBlockByEstate(estcode) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListBlockByEstate',
        data: '{EstCode: "' + estcode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            $('#selectblock').find('option').remove();
            for (var i = 0; i < response.d.split(";").length; i++) {
                $("#selectblock").append(new Option(response.d.split(";")[i], response.d.split(";")[i]));
            }
            $("#selectblock").combobox();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListEstate : " + xhr.statusText);
        }
    });
}

function ChangePassword(username, oldpassword, newpassword) {
    $("#dialogloading").dialog("open");
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ChangePassword',
        data: "{UserName: '" + username + "', OldPassword:'" + oldpassword + "', NewPassword:'" + newpassword + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response != null && response.d != null) {
                if (response.d == "success") {
                    alert("Password has been changed");
                    $('#newpassword').val("");
                    $('#oldpassword').val("");
                    $('#conformnewpassword').val("");
                    $("#dialogchangepassword").dialog("close");
                    $("#dialogloading").dialog("close");
                }
                else {
                    alert(response.d);
                    $("#dialogloading").dialog("close");
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ChangePassword : " + xhr.statusText);
            $("#dialogloading").dialog("close");
        }
    });
}

function GetPiezoRecordDetailByPieRecordID(pierecordid) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetPiezoRecordDetailByPieRecordID',
        data: "{PieRecordID: '" + pierecordid + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            var imagesrc = "";
            if (json[0]["ImageLocation"] == "") {
                imagesrc = "<img style='cursor:pointer;' width='75px' height='75px' src='../Image/Icon/noimage.png' onclick='PreviewImage()'/>";
            }
            else {
                imagesrc = "<img style='cursor:pointer;' width='75px' height='75px' src='http://172.30.1.122/PZOService/uploads/" + globalmappedestate + "/" + json[0]["ImageLocation"] + "' onclick='PreviewImage()'/>";
            }

            globalimgurl = json[0]["ImageLocation"];
            var contentinfo = "<br/><div style='position:absolute; top:50px;right:50px;width:70px;height:80px;'>" + imagesrc + "</div>"
                                    + "<div style='width:400px;font-family:Calibri; font-size:13px; background-color:#42A867'>" + json[0]["PieRecordID"] + "</div>"
                                    + "<table style='width:400px;font-family:Calibri; font-size:13px;'>"
                                    + "<tr><td style='width:75px;'>Longitude</td><td> : " + json[0]["Longitude"] + "</td></tr>"
                                    + "<tr><td>Latitude</td><td> : " + json[0]["Latitude"] + "</td></tr>"
                                    + "<tr><td>Accuracy</td><td> : " + json[0]["Accuracy"] + "</td></tr>"
                                    + "<tr><td>IsActive</td><td> : " + json[0]["IsActive"] + "</td></tr>"
                                    + "</table><br/>"
                                    + "<div style='width:400px;font-family:Calibri; font-size:13px; background-color:#42A867'>Data History</div>"
                                    + "<br/><table width='100%' cellpadding='3' cellspacing='0' border='1' style='font-family:Calibri; font-size:13px;' >"
                                    + "<tr bgcolor='#42A867'><th>Week</th><th>Tanggal</th><th>Ketinggian</th><th>Kondisi</th><th>Klasifikasi</th></tr>";

            for (var i = 0; i < json.length; i++) {
                if (i % 2 == 0)
                    contentinfo += "<tr bgcolor='#D1FFD9'>";
                else
                    contentinfo += "<tr bgcolor='#ABF0A1'>";

                contentinfo += "<td align='center'>" + json[i]["Week"] + "</td><td align='center'>" + json[i]["DateUpload"] + "</td><td align='center'>" + json[i]["Ketinggian"] + "</td><td align='center'>" + json[i]["Kondisi"] + "</td><td align='center'>" + json[i]["Klasifikasi"] + "</td></tr>";
            }

            contentinfo += "</table><br/>";
            $("#divinfo").html(contentinfo);
            $("#dialoginfo").dialog("open");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetPiezoRecordDetailByPieRecordID : " + xhr.statusText);
        }
    });
}

function GetListPiezometer() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetListPiezometer',
        data: '{EstCode: "' + globalmappedestate + '",WeekID: "' + globalidweek + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            if (json.length > 0) {
                contenttable = "<table border='1' id='tablesurveypiezometer' style='font-family:Calibri; font-size:12px;'  CellSpacing='0' CellPadding='4'>";
                contenttable += "<thead><tr bgcolor='#42A867'>";
                contenttable += "<td  align='center' width='20'><font color='#FFFFFF'>No</font></td>";
                contenttable += "<td  align='center' width='100'><font color='#FFFFFF'>PieRecordID</font></td>";
                contenttable += "<td  align='center' width='100'><font color='#FFFFFF'>Block</font></td>";
                contenttable += "<td  align='center' width='100'><font color='#FFFFFF'>Accuracy (m)</font></td>";
                contenttable += "<td  align='center' width='100'><font color='#FFFFFF'>Ketinggian (cm)</font></td>";
                contenttable += "<td  align='center' width='100'><font color='#FFFFFF'>Kondisi</font></td>";
                contenttable += "<td  align='center' width='100'><font color='#FFFFFF'>Tipe PVC</font></td>";
                contenttable += "<td  align='center' width='100'><font color='#FFFFFF'>Tanggal Piezometer</font></td>";
                contenttable += "<td  align='center' width='100'><font color='#FFFFFF'>UserName Piezometer</font></td>";
                contenttable += "<td  align='center' width='100'><font color='#FFFFFF'>UserName Upload</font></td>";
                contenttable += "<td  align='center' width='100'><font color='#FFFFFF'>Source</font></td>";
                contenttable += "<td  align='center' width='40'><font color='#FFFFFF'>Detail</font></td>";
                contenttable += "<td  align='center' width='40'><font color='#FFFFFF'>Go To</font></td>";
                contenttable += "</tr></thead><tbody>";
                for (var i = 0; i < json.length; i++) {
                    if (i % 2 == 0)
                        contenttable += "<tr bgcolor='#D1FFD9'>";
                    else
                        contenttable += "<tr bgcolor='#ABF0A1'>";
                    contenttable += "<td align='center'>" + (i + 1) + "</td>";
                    contenttable += "<td align='left'>" + json[i]["PieRecordID"] + "</td>";
                    contenttable += "<td align='left'>" + json[i]["Block"] + "</td>";
                    contenttable += "<td align='left'>" + json[i]["Accuracy"] + "</td>";
                    if (json[i]["Ketinggian"] == null)
                        contenttable += "<td align='left'>" + "" + "</td>";
                    else
                        contenttable += "<td align='left'>" + json[i]["Ketinggian"] + "</td>";
                    if (json[i]["Kondisi"] == null)
                        contenttable += "<td align='left'>" + "" + "</td>";
                    else
                        contenttable += "<td align='left'>" + json[i]["Kondisi"] + "</td>";
                    contenttable += "<td align='left'>" + json[i]["Type"] + "</td>";
                    contenttable += "<td align='left'>" + json[i]["DateEditor"] + "</td>";
                    contenttable += "<td align='left'>" + json[i]["UserNameEditor"] + "</td>";
                    if (json[i]["UserNameUpload"] == null)
                        contenttable += "<td align='left'>" + "" + "</td>";
                    else
                        contenttable += "<td align='left'>" + json[i]["UserNameUpload"] + "</td>";
                    contenttable += "<td align='left'>" + json[i]["SOURCE"] + "</td>";
                    contenttable += "<td align='center'><img src='../Image/toolbar/detail.png' id='go1' hight=20 width=20 style='cursor:pointer;' onClick=GetPiezoRecordDetailByPieRecordID('" + json[i]["PieRecordID"] + "'); /></td>";
                    contenttable += "<td align='center'><img src='../Image/toolbar/go.png' id='go1' hight=20 width=20 style='cursor:pointer;' onClick=ZoomToPiezometer('" + json[i]["Longitude"] + "','" + json[i]["Latitude"] + "'); /></td>";
                    contenttable += "</tr>";
                }
            }

            contenttable += "</tbody></table> ";
            $("#surveypiezometer").html(contenttable);
            $('#tablesurveypiezometer').DataTable();
            $("#dialogsurveypiezometer").dialog("open");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetListPiezometer : " + xhr.statusText);
        }
    });
}

//image preview
function PreviewImage() {
    var img = new Image();
    var bcgDiv = document.getElementById("divBackground");
    var imgDiv = document.getElementById("divImage");
    var imgFull = document.getElementById("imgFull");
    var imgLoader = document.getElementById("imgLoader");
    imgLoader.style.display = "block";
    img.onload = function () {
        imgFull.src = img.src;
        imgFull.style.display = "block";
        imgLoader.style.display = "none";
    };

    if (globalimgurl == "") {
        img.src = "../Image/Icon/noimage.png";
    }
    else {
        img.src = "http://172.30.1.122/PZOService/uploads/" + globalmappedestate + "/" + globalimgurl;
    }

    var width = document.body.clientWidth;
    if (document.body.clientHeight > document.body.scrollHeight) {
        bcgDiv.style.height = document.body.clientHeight + "px";
    }
    else {
        bcgDiv.style.height = document.body.scrollHeight + "px";
    }
    imgDiv.style.left = (width - 650) / 2 + "px";
    imgDiv.style.top = "20px";
    bcgDiv.style.width = "100%";

    bcgDiv.style.display = "block";
    imgDiv.style.display = "block";
    return false;
}
function HidePreview() {
    var bcgDiv = document.getElementById("divBackground");
    var imgDiv = document.getElementById("divImage");
    var imgFull = document.getElementById("imgFull");
    if (bcgDiv != null) {
        bcgDiv.style.display = "none";
        imgDiv.style.display = "none";
        imgFull.style.display = "none";
    }
}

