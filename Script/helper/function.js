﻿function formMeasureEdit(fotoMeasure) {
    var objMeasure = fotoMeasure[0];

    var value = getValueTinggiMukaAir(JSON.parse(objMeasure["dynamicMeasurement"]));
    var valueHis = getValueTinggiMukaAir(JSON.parse(objMeasure["dynamicMeasurementHis"]));

    var content = '<div class="form-horizontal">';

    content += "<div class='row'><div class='col-md-12' align='center'><div class='row'>";
    var actionImg = [];
    for (var i = 0; i < fotoMeasure.length; i++) {
        if (fotoMeasure.length == 3) {
            content += "<div class='col-md-4'>";
        } else if (fotoMeasure.length == 2) {
            content += "<div class='col-md-6'>";
        } else {
            content += "<div>";
        }
        var pathfoto = globalUrlImages + fotoMeasure[i]["pathImage"];
        if (fotoMeasure[i]["pathImage"] == null) {
            content += "<img src='../Image/Icon/no-photo.png' id='previewEdit' style='width:100px;heigth:100px;margin-left:10px'><br>";
            content += '<div class="form-group">';
            content += '<label for="inputEmail3" class="col-sm-3 control-label">Edit Foto</label>'
            content += '<div class="col-sm-7">';
            content += '<input type="file" accept="image/*" class="form-control" id="ImagesMeasureDataEdit">';
            content += '</div>';
            content += '</div>';
        } else {
            var id = "zoom" + i;
            content += "<a class='fancyboxMeasure' href='" + pathfoto + "' rel='gallery' data-fancybox='images'><img id='" + id + "' src='" + pathfoto + "' style='width:100px;heigth:100px' ></a>";
            actionImg.push(id);
        }
        content += "</div>";
    }
    content += "</div></div>";
    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Tanggal</label>'
    content += '<div class="col-sm-7">';
    content += '';
    content += '</div>';
    content += '</div>';

    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Tanggal Input</label>'
    content += '<div class="col-sm-7">';
    var dateI = objMeasure["inputDate"];
    if (objMeasure["inputDateHistory"] != null) {
        dateI = objMeasure["inputDateHistory"];
    }
    content += '<label class="control-label">' + dateFormat3(new Date(parseInt(dateI.replace('/Date(', '').replace(')/', '')))) + '</label>';
    content += '</div>';
    content += '</div>';

    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Input By</label>';
    content += '<div class="col-sm-7">';
    var uFullname = objMeasure["UserFullName"];
    if (objMeasure["UserFullNameHistory"] != null) {
        uFullname = objMeasure["UserFullNameHistory"];
    }
    content += '<label class="control-label">' + uFullname + '</label>';
    content += '</div>';
    content += '</div>';

    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Nilai</label>'
    content += '<div class="col-sm-6">';
    content += '<input type="number" class="form-control" id="formNilai" value="' + value["nilai"] + '">';
    content += '</div>';
    content += '<div class="col-sm-1">';
    content += '<a href="#" onclick="valueDefault(' + valueHis["nilai"] + ',formNilai)">default</a>';
    content += '</div>';
    content += '<div class="col-sm-1">';
    content += '</div>';
    content += '</div>';

    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Kondisi</label>'
    content += '<div class="col-sm-6">';
    content += '<select id="formKondisi" class="form-control selectpicker" data-live-search="true" style="width:100%;">';
    for (var i = 0; i < masterkondisi.length; i++) {
        content += '<option value="' + masterkondisi[i]["valueId"] + '">' + masterkondisi[i]["valueName"] + '</option>';
    }
    content += '</select>';
    content += '</div>';
    content += '<div class="col-sm-1">';
    content += '<a href="#" onclick="valueDefault(' + value["kondisiId"] + ',formKondisi)">default</a>';
    content += '</div>';
    content += '<div class="col-sm-1">';
    content += '</div>';
    content += '</div>';

    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Remarks</label>'
    content += '<div class="col-sm-6">';
    content += '<input type="text" class="form-control" id="formRemarks" value="' + value["remarks"] + '">';
    content += '</div>';
    content += '<div class="col-sm-1">';
    content += '<a href="#" onclick="valueDefault(' + value["remarks"] + ',formRemarks)">default</a>';
    content += '</div>';
    content += '<div class="col-sm-1">';
    content += '</div>';
    content += '</div>';

    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Status</label>'
    content += '<div class="col-sm-6">';
    if (objMeasure["status"] == 2) {
        content += '<label><input type="radio" class="form-control flat-blue" name="measureActive" value="1"> Aktif </label>';
        content += '<label class="pull-right"><input type="radio" class="form-control flat-blue" name="measureActive"  value="2" checked> Tidak Aktif </label>';
    } else {
        content += '<label><input type="radio" class="form-control flat-blue" name="measureActive" value="1" checked> Aktif </label>';
        content += '<label class="pull-right"><input type="radio" class="form-control flat-blue" name="measureActive"  value="2"> Tidak Aktif </label>';
    }
    content += '</div>';
    content += '<div class="col-sm-1">';
    content += '</div>';
    content += '</div>';


    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Petugas</label>'
    content += '<div class="col-sm-6">';
    content += '<select id="selectPetugas" class="form-control selectpicker" data-live-search="true" data-size="5" style="width:100%;" disble>';
    for (var i = 0; i < listPetugas.length; i++) {
        content += '<option value="' + listPetugas[i]["idSurveyor"] + '">' + listPetugas[i]["namaSurveyor"] + '</option>';
    }
    content += '</select>';
    content += '</div>';
    content += '<div class="col-sm-1">';
    content += '<a href="#" onclick="valueDefault(' + objMeasure.idSurveyor + ',selectPetugas)">default</a>';
    content += '</div>';
    content += '<div class="col-sm-1">';
    content += '</div>';
    content += '</div>';

    
    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Insentif</label>'
    content += '<div class="col-sm-6">';
    if (objMeasure.pay == 1) {
        content += '<label><input type="radio" class="form-control flat-blue" name="berbayar" value="1" checked> Ya </label>';
        content += '<label class="pull-right"><input type="radio" class="form-control flat-blue" name="berbayar"  value="0" > Tidak </label>';
    } else {
        content += '<label><input type="radio" class="form-control flat-blue" name="berbayar" value="1" > Ya </label>';
        content += '<label class="pull-right"><input type="radio" class="form-control flat-blue" name="berbayar"  value="0" checked> Tidak </label>';
    }
    content += '</div>';
    content += '<div class="col-sm-1">';
    content += '</div>';
    content += '</div>';

    content += '</div>';

    content += '<div class="box-footer">';
    content += '<div class="col-sm-4"><button type="submit" class="btn btn-primary" id="formHistory">History</button></div>';
    content += '<div class="col-sm-4" align="center"><button type="submit" class="btn btn-primary" id="formMap">Map</button></div>';
    if (userAccess[0]["mdlAccCode"] == "WLR4" || (userAccess[0]["mdlAccCode"] == "WLR6" && objMeasure.UserID == $("#uID").val())) {
        content += '<div class="col-sm-4"><button type="submit" class="btn btn-primary pull-right" id="formSubmit">Save</button></div>';
    }
    content += '</div></div>';

    $("#formMeasure").html(content);


    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    })

    $('.selectpicker').selectpicker('refresh');

    for (var i = 0; i < actionImg.length; i++) {
        $("#" + actionImg[i]).elevateZoom({
            tint: true,
            tintColour: '#F90', tintOpacity: 0.5,
            cursor: 'pointer',
            imageCrossfade: true,
            scrollZoom: true
        });
    }

    $(".fancyboxMeasure")
        .attr('rel', 'gallery')
        .fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            nextEffect: 'none',
            prevEffect: 'none',
            padding: 0,
            margin: [20, 60, 20, 60]
        });

    $('#formKondisi').val(value["kondisiId"]).change();
    $('#selectPetugas').val(objMeasure.idSurveyor).change();

    $("#formMeasure")
        .dialog({
            "title": objMeasure["stationName"],
            "width": 550,
            "height": 600
        });

    $("#formHistory").click(function () {
        historyMeasure(objMeasure["measurementId"]);
    });

    $("#formMap").click(function () {
        getLocationMeasure(objMeasure);
    });

    $("#formSubmit").click(function () {
        var dynamicM = [];
        var dynamicMobj = {};
        dynamicMobj["questionId"] = 1;
        dynamicMobj["answer"] = $("#formNilai").val();
        dynamicM.push(dynamicMobj);

        dynamicMobj = {};
        dynamicMobj["questionId"] = 5;
        dynamicMobj["answer"] = $("#formRemarks").val();
        dynamicM.push(dynamicMobj);

        dynamicMobj = {};
        dynamicMobj["questionId"] = 6;
        dynamicMobj["answer"] = $("#formKondisi").val();
        dynamicM.push(dynamicMobj);

        var objSend = {};
        objSend["measurementId"] = objMeasure["measurementId"];
        objSend["recordStationId"] = objMeasure["stationId"];
        objSend["editBy"] = $('#uID').val();
        objSend["status"] = $("input:radio[name=measureActive]:checked").val();
        objSend["dynamicMeasurement"] = dynamicM;
        objSend["surveyor"] = dynamicM;

        objSend["petugas"] = $("#selectPetugas").val();
        objSend["berbayar"] = $("input:radio[name=berbayar]:checked").val();

        if ($("#formNilai").val() == "") {
            alert("Mohon Input Nilai")
            return;
        }

        console.info("updateMeasure", JSON.stringify(objSend));
        saveEditMeasure(objSend);
    });
    $("#ImagesMeasureDataEdit").change(function () {
        var preview = document.querySelector('img');
        var file = document.getElementById('ImagesMeasureDataEdit').files[0];
        console.log('file = ' + file);
        var reader = new FileReader();

        reader.onload = function (e) {
            //preview.src = reader.result;
            document.getElementById("previewEdit").src = e.target.result;
        }

        if (file) {
            // alert('ada file');
            reader.readAsDataURL(file);

        } else {
            alert('tidak ada file');
            preview.src = "";
        }

    });
}


function saveEditMeasure(objEdit) {
    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/SaveEditMeasure',
        data: JSON.stringify({ obj: JSON.stringify(objEdit) }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            $("#formMeasure").dialog("close");
            $(".loading").hide();
            var url = new URL(window.location.href);
            var param = url.searchParams.get("WLR");
            if (param == "WLR_DataRambuAir") {
                getDataRambuAir();
            } else if (param == "WLR_DataZona") {
                GetStationMeasureArray();
            } else if (param == "WLR_DataMingguan") {
                setData(false);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function SaveEditMeasure : " + xhr.statusText);
        }
    });
}


function editMeasure(measureId) {
    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/GetMeasureImages',
        data: '{measureId: "' + measureId + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            formMeasureEdit(JSON.parse(response.d));
            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getMeasureImages : " + xhr.statusText);
        }
    });
}



// histrory measure
function showHistoryMeasure(dataHistory) {
    var content = "<div class='row'><div class='col-md-12' align='center'>";
    content = '<table id="tableMeasureHistory" class="table table-striped table-bordered table-text-center" style="width:100%">';
    content += '<thead><tr>';
    content += '<th>Edit By</th><th>Edit Date</th><th>TMAS</th><th>Kondisi</th><th>Remarks</th></tr></thead><tbody>';
    for (var i = 0; i < dataHistory.length; i++) {
        var value = getValueTinggiMukaAir(JSON.parse(dataHistory[i]["dynamicMeasurement"]));
        content += '<tr>';
        content += '<td>' + dataHistory[i]["UserName"] + '</td>';
        content += '<td>' + dateFormat(new Date(parseInt(dataHistory[i]["inputDate"].replace('/Date(', '').replace(')/', '')))) + '</td>';
        content += '<td>' + value["nilai"] + '</td>';
        content += '<td>' + value["kondisi"] + '</td>';
        content += '<td>' + value["remarks"] + '</td>';
        content += '</tr>';
    }
    content += '</tbody></table>';
    content += "</div></div>";

    $("#formMeasureHistory").html(content);
    $('#tableMeasureHistory').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
        pageLength: 10,
        order: [[0, 'desc']],
        buttons: [
            'copy',
            {
                extend: 'csvHtml5',
                title: 'RPZO002_DataKLHK',
                fieldBoundary: '',
                header: false,
                footer: false
            },
            {
                extend: 'excelHtml5',
                title: 'RPZO002_DataKLHK'
            }, {
                extend: 'pdfHtml5',
                title: 'RPZO002_DataKLHK'
            }, 'print'
        ]
    });

    $("#formMeasureHistory")
        .dialog({
            "title": "History Perubahan Data Pengukuran Tanggal " + dateFormat(new Date(parseInt(dataHistory[0]["measurementDate"].replace('/Date(', '').replace(')/', '')))),
            "width": 550,
            "height": 350
        });
}

function historyMeasure(measureId) {
    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/GetHistoryMeasure',
        data: '{measureId: "' + measureId + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (JSON.parse(response.d).length > 0) {
                showHistoryMeasure(JSON.parse(response.d));
            } else {
                alert("Pengukuran Ini Tidak Pernah Di Ubah");
            }
            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetHistoryMeasure : " + xhr.statusText);
        }
    });
}

function dialogMap(data, dataMap) {
    $('.modal-dialogmap').css({
        top: 0,
        left: 230
    });

    $('#myMap').modal({
        backdrop: true,
        show: true
    });

    $('.modal-dialogmap').draggable({
        handle: ".modal-header"
    });
    $('#myMap').modal('show');
    var headHtml = "( Map ) ";
    $("#myMapTitle").html(headHtml);

    var htmlReturned = "<div id='map' class='map' style='width: 920px; height: 580px;'></div>"
        + "<div id='scale-line' class='scale-line' style='top:25px'></div>"
        + "<div id='mouse-position' class='mouse-position' style='top:25px'></div>";
    $("#divObjectMap").html(htmlReturned);
    $("#divObjectMap").css({
        width: 900,
        height: 600
    });
    var objlatlon = data[0];
    var mapProjection = 'EPSG:4326';
    var scaleLineControl = new ol.control.ScaleLine({ units: 'metric', className: 'ol-scale-line', target: document.getElementById('scale-line') });
    var mousePositionControl = new ol.control.MousePosition({
        coordinateFormat: ol.coordinate.createStringXY(8),
        projection: mapProjection,
        target: document.getElementById('mouse-position'),
        undefinedHTML: '&nbsp;'
    });
    var overviewMapControl = new ol.control.OverviewMap({
        className: 'ol-overviewmap ol-custom-overviewmap',
        collapseLabel: '\u00BB',
        label: '\u00AB',
        collapsed: true
    });
    var zoomSliderControl = new ol.control.ZoomSlider();

    var viewMap = new ol.View({
        projection: mapProjection,
        center: [objlatlon["longitude"], objlatlon["latitude"]],
        zoom: 13,
    });


    var objBlock = {};
    var objFoto = {};
    var objLabelBlock = {};

    for (var i = 0; i < dataMap.length; i++) {
        if (dataMap[i]["LayerDisplay"] == "on") {
            if (dataMap[i]["LayerCategory"] == "base") {
                objFoto = dataMap[i];
            } else {
                if (dataMap[i]["LayerDisplayName"] == "Block") {
                    objBlock = dataMap[i];
                } else if (dataMap[i]["LayerDisplayName"] == "Label Block") {
                    objLabelBlock = dataMap[i];
                }
            }
        }
    }


    var layerFotoUdara = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url: objFoto["LayerURL"],
            params: {
                'LAYERS': objFoto["LayerURLName"]
            }
        })
    });


    var layerBlock = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            url: objBlock["LayerURL"],
            params: {
                'LAYERS': objBlock["LayerURLName"],
                'TRANSPARENT': 'true'
            },
        })
    });

    var layerLabelBlock = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            url: objLabelBlock["LayerURL"],
            params: {
                'LAYERS': objLabelBlock["LayerURLName"],
                'TRANSPARENT': 'true'
            },
        })
    });

    var markerMeasure = new ol.source.Vector();

    var geom_point = new ol.geom.Point([objlatlon['longitude'], objlatlon['latitude']]);
    var point_feature = new ol.Feature(geom_point);
    point_feature.setId("Station");
    markerMeasure.addFeature(point_feature);

    var geom_point2 = new ol.geom.Point([objlatlon['lonMeasure'], objlatlon['latMeasure']]);
    var point_feature2 = new ol.Feature(geom_point2);
    point_feature2.setId("Measurement");
    markerMeasure.addFeature(point_feature2);

    var layerPoint = new ol.layer.Vector({
        source: markerMeasure,
        style: function (feature) {
            return styleFeatureMeasure(feature)
        }
    });

    var layers = [layerFotoUdara, layerBlock, layerLabelBlock, layerPoint];

    map = new ol.Map({
        target: 'map',
        controls: ol.control.defaults({
            attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                collapsible: false
            })
        }).extend([
            scaleLineControl, mousePositionControl, zoomSliderControl
        ]),
        layers: layers,
        view: viewMap
    });

    setTimeout(updateSize, 1000);
}

function updateSize() {
    map.updateSize()
}

function getLocationMeasure(obj) {
    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/GetLocationMeasure',
        data: '{measureId: "' + obj["measurementId"] + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (JSON.parse(response.d).length > 0) {
                getFotoUdaraByEstate(obj, JSON.parse(response.d))
            } else {
                alert("Pengukuran Ini Tidak Diketahui lokasinya ");
                $(".loading").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getLocationMeasure : " + xhr.statusText);
            $(".loading").hide();
        }
    });
}

function getFotoUdaraByEstate(obj, datameasure) {
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/getFotoUdaraByEstate',
        data: '{estCode: "' + obj["estCode"] + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            dialogMap(datameasure, JSON.parse(response.d));
            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getFotoUdaraByEstate : " + xhr.statusText);
        }
    });
}

function styleFeatureMeasure(feature) {
    var color = "#FE0101";

    var styleFeatures = [];
    var styleTextPoin = new ol.style.Style({
        image: new ol.style.Circle({
            fill: new ol.style.Fill({
                color: '#ffffff'
            }),
            stroke: new ol.style.Stroke({
                color: color,
                width: 4
            }),
            radius: 4
        }),
        text: new ol.style.Text({
            textAlign: 'center',
            textBaseline: 'middle',
            font: '11px Verdana',
            offsetX: 0,
            offsetY: -15,
            text: feature.getId(),
            fill: new ol.style.Fill({
                color: '#000000',
            }),
            stroke: new ol.style.Stroke({
                color: '#ffffff',
                width: 3
            })
        })
    });
    styleFeatures.push(styleTextPoin);
    return styleFeatures;
}

function showFormNote(data, rowIdx) {
    var content = '<div class="form-horizontal">';
    content += "<div class='col-md-12' align='center'>";

    content += "<div class='row'>";
    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Tanggal</label>'
    content += '<div class="col-sm-9">';
    content += '<input type="text" class="form-control" id="tglNote">';
    content += '</div>';
    content += '</div>';

    content += "<div class='row'>";
    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Note</label>'
    content += '<div class="col-sm-9">';
    content += '<textarea rows="4" style="width:95%" id="note"></textarea>';
    content += '</div>';
    content += '</div>';

    content += "<div class='row'>";
    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Aktif</label>'
    content += '<div class="col-sm-9">';
    content += '<label class="pull-left" style="margin-left:15px"><input type="radio" class="form-control flat-blue" name="noteActive" value="1" checked> Aktif </label>';
    content += '<label class="pull-left"><input type="radio" class="form-control flat-blue" name="noteActive"  value="2"> Tidak Aktif </label>';
    content += '</div>';
    content += '</div>';

    content += '<div class="box-footer">';
    content += '<button type="submit" class="btn btn-primary pull-right" id="btnNote" style="margin-right:15px">Save</button>';
    content += '</div></div>';

    $("#formNote").html(content);

    var Header = "Add Note";
    if (data != undefined) {
        $("#tglNote").val(data["tanggal"]);
        $("#note").val(data["note"]);
        $('input:radio[name=noteActive]').filter('[value=' + data["statusId"] + ']').attr('checked', true);
        Header = "Edit Note " + data["tanggal"];
    }

    $('#tglNote').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        alwaysShowCalendars: false,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });

    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    })


    $("#btnNote").click(function () {
        //var r = confirm("Apakah Anda Yakin Untuk Menambahkan Note");
        //if (r == true) {
        $("#formNote").dialog("close");
        var stat = "Aktif"
        if ($("input:radio[name=noteActive]:checked").val() == '2') {
            stat = "Tidak Aktif";
        }
        if (data == undefined) {
            arrayNote.push();

            //tableNote.row.add($('<tr><td>' + $("#tglNote").val() + '</td><td>' + $("#note").val() + '</td><td>' + stat + '</td></tr>')[0]).draw(false);
            var obj = {
                noteId: new Date().valueOf(),
                tanggal: $("#tglNote").val(),
                note: $("#note").val(),
                status: stat,
                statusId: $("input:radio[name=noteActive]:checked").val()
            };
            tableNote.row.add(obj).draw(false);
            setupChart();
        } else {
            var obj = {
                noteId: data["noteId"],
                tanggal: $("#tglNote").val(),
                note: $("#note").val(),
                status: stat,
                statusId: $("input:radio[name=noteActive]:checked").val()
            };
            console.info("dataNote " + rowIdx, obj);
            tableNote.row(rowIdx).data(obj).draw();
            setupChart();
        }
        //}
    })

    $("#formNote")
        .dialog({
            "title": Header,
            "width": 400,
            "height": 330
        });


}
function showFormColorSetting(data, rowIdx) {
    var content = '<table border = 0 style ="width : 100%;">  ';
    var color = "";
    content += '<tr>';
    content += '<div class="form-group">';
    content += '<td>';
    content += '<label for="inputEmail3" class="col-sm-1 control-label">Nama Klasifikasi</label>'
    content += '</td>';
    content += '<td>';
    content += '<div class="col-sm-6">';
    content += '<input type="text" class="form-control" id="namaKlasifikasi" style = "width: 280px;">';
    content += '</td>';
    content += '</tr>';
    content += '</div>';

    content += '<tr>';
    content += '<div class="form-group">';
    content += '<td>';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Nilai Minimum</label>'
    content += '</td>';
    content += '<td>';
    content += '<div class="col-sm-6">';
    content += '<input type="text" class="form-control" id="nilaiMinimum" style = "width: 280px;">';
    content += '</td>';
    content += '</tr>';
    content += '</div>';

    content += '<tr>';
    content += '<div class="form-group">';
    content += '<td>';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Nilai Maksimum</label>'
    content += '</td>';
    content += '<td>';
    content += '<div class="col-sm-6">';
    content += '<input type="text" class="form-control" id="nilaiMaksimum" style = "width: 280px;">';
    content += '</td>';
    content += '</tr>';
    content += '</div>';

    content += '<tr>';
    content += '<div class="form-group">';
    content += '<td>';
    content += '<label class="col-sm-3 control-label">Select Color</label>';
    content += '</td>';
    content += '<td>';
    content += '<input type="color" class="form-control" id="codeWarna" style = "margin-left: 15px; width: 75%;">';
    content += '</td>';
    content += '</tr>';
    content += '</div>';


    content += '<tr>';
    content += '<div class="form-group">';
    content += '<td>';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Aktif</label>';
    content += '</td>';
    content += '<td>';
    content += '<label class="pull-left" style="margin-left:15px"><input type="radio" class="form-control flat-blue" name="colorActive" value="1" checked> Aktif </label>';
    content += '<label class="pull-left"  style="margin-left:15px"><input type="radio" class="form-control flat-blue" name="colorActive" value="0"> Tidak Aktif </label>';
    content += '</td>';
    content += '</tr>';
    content += '</div>';

    content += '<tr>';
    content += '<td>';
    content += '</td>';
    content += '<td>';
    content += '<button type="submit" class="btn btn-primary" id="btnColor" style = "margin-left : 60px;">Save</button>';
    content += '</td>';
    content += '</tr>';
    content += '</table>';
    //var content = '<div class="form-horizontal" style="resize: none">';
    //var color = "";
    //content += "<div class='col-md-12' align='center'>";
    //content += "<div class='row'>";
    //content += '<div class="form-group">';
    //content += '<label for="inputEmail3" class="col-sm-3 control-label">Nama Klasifikasi</label>'
    //content += '<div class="col-sm-6">';
    //content += '<input type="text" class="form-control" id="namaKlasifikasi" style = "width: 100%;">';
    //content += '</div>';
    //content += '</div>';

    //content += "<div class='row'>";
    //content += '<div class="form-group">';
    //content += '<label for="inputEmail3" class="col-sm-3 control-label">Nilai Minimum</label>'
    //content += '<div class="col-sm-6">';
    //content += '<input type="text" class="form-control" id="nilaiMinimum" style = "width: 100%;">';
    //content += '</div>';
    //content += '</div>';

    //content += "<div class='row'>";
    //content += '<div class="form-group">';
    //content += '<label for="inputEmail3" class="col-sm-3 control-label">Nilai Maksimum</label>'
    //content += '<div class="col-sm-6">';
    //content += '<input type="text" class="form-control" id="nilaiMaksimum" style = "width: 100%;">';
    //content += '</div>';
    //content += '</div>';

    //content += '<div class="row">';
    //content += '<div class="form-group">';
    //content += '<label class="col-sm-3 control-label">Select Color</label>';
    //content += '<input type="color" class="form-control" id="codeWarna" style = "width: 320px; margin-right: 100px;">';
    ////content += '<div id="colorSelected" style=width:50px;height:50px; margin-left: -1200px;"></div><br>';
    //content += '</div>';
    ////content += '<div class="row"><button type="submit" class="btn btn-primary" id="btnSaveColor">Save</button></div>';
    //content += '</div>';


    //content += '<div>';
    //content += '<button type="submit" class="btn btn-primary" id="btnColor">Save</button>';
    //content += '</div></div>';

    $("#graphColorSetting").html(content);

   
    //$('#colorSelected').css('background-color','#d7d800');
    //$('#colorSelected').colorpicker().on(
    //    'changeColor',
    //    function () {
    //        $('#colorSelected').css('background-color',
    //            $(this).colorpicker('getValue', color));
    //    });


    $('#btnColor').click(function () {
        $("#graphColorSetting").dialog("close");
        var x = $('#codeWarna').val();
        var stat = "Aktif"
        if ($("input:radio[name=colorActive]:checked").val() == '0') {
            stat = "Tidak Aktif";
        }
        //alert('kode warna = ' + x);
        if (data == undefined) {
            var obj = {
                namaKlasifikasi: $("#namaKlasifikasi").val(),
                nilaiMinimum: parseFloat($("#nilaiMinimum").val()),
                nilaiMaksimum: parseFloat($("#nilaiMaksimum").val()),
                codeWarna: $("#codeWarna").val(),
                status: stat,
                statusId: $("input:radio[name=colorActive]:checked").val(),
            };
            tableSetColor.row.add(obj).draw(false);
            setupChart();
        } else {
            var obj = {
                namaKlasifikasi: $("#namaKlasifikasi").val(),
                nilaiMinimum: parseFloat($("#nilaiMinimum").val()),
                nilaiMaksimum: parseFloat($("#nilaiMaksimum").val()),
                codeWarna: $("#codeWarna").val(),
                status: stat,
                statusId: $("input:radio[name=colorActive]:checked").val(),

            };
            tableSetColor.row(rowIdx).data(obj).draw();
            setupChart();
        }

    });

    var Header = "Color setting";

    if (data != undefined) {
        var status = data["statusId"];        
        $("#namaKlasifikasi").val(data["namaKlasifikasi"]);
        $("#nilaiMinimum").val(data["nilaiMinimum"]);
        $("#nilaiMaksimum").val(data["nilaiMaksimum"]);
        $("#codeWarna").val(data["codeWarna"]);
        $('input:radio[name=colorActive]').filter('[value=' + data["statusId"] + ']').attr('checked', true);
        Header = "Edit" + " " + data["namaKlasifikasi"];
    }
    $("#graphColorSetting")
        .dialog({
            "title": Header,
            "width": 590,
            "height": 330,
            resizeable: false
        });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

}

function showFormMeasureData() {


    var content = '<div class="form-horizontal">';

    content += "<div class='row'><div class='col-md-12' align='center'><div class='row'>";
    //var actionImg = [];
    //for (var i = 0; i < fotoMeasure.length; i++) {
    //    if (fotoMeasure.length == 3) {
    //        content += "<div class='col-md-4'>";
    //    } else if (fotoMeasure.length == 2) {
    //        content += "<div class='col-md-6'>";
    //    } else {
    //        content += "<div>";
    //    }
    //    var pathfoto = globalUrl + fotoMeasure[i]["pathImage"];
    //    if (fotoMeasure[i]["pathImage"] == null) {
    //        content += "<img src='../Image/Icon/no-photo.png' style='width:100px;heigth:100px;margin-left:10px'><br>";
    //    } else {
    //        var id = "zoom" + i;
    //        content += "<a class='fancyboxMeasure' href='" + pathfoto + "' rel='gallery' data-fancybox='images'><img id='" + id + "' src='" + pathfoto + "' style='width:100px;heigth:100px' ></a>";
    //        actionImg.push(id);
    //    }
    //    content += "</div>";
    //}
    content += "</div></div>";
    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Foto</label>';
    content += '<div class="col-sm-7">';
    content += '<a id="single_image" href="#preview">';
    //content += '<img  "preview" width="100" height="100">';
    content += "<img src='../Image/Icon/no-photo.png' id='preview' style='width:100px;heigth:100px;margin-left:10px'><br>";
    content += '</a>';
    content += '</div>';
    content += '</div>';

    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Foto</label>'
    content += '<div class="col-sm-7">';
    content += '<input type="file" accept="image/*" class="form-control" id="ImagesMeasureData">';
    content += '</div>';
    content += '</div>';

    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Tanggal</label>'
    content += '<div class="col-sm-7">';
    content += '<input type="text" class="form-control" id="tanggalInput">';
    content += '</div>';
    content += '</div>';


    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Nilai</label>'
    content += '<div class="col-sm-7">';
    content += '<input type="number" class="form-control" id="formNilai" style="">';
    content += '</div>';
    content += '</div>';


    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Kondisi</label>';
    content += '<div class="col-sm-7">';
    content += '<select id="formKondisi" class="form-control selectpicker" data-live-search="true" style="width:100%;">';
    for (var i = 0; i < masterkondisi.length; i++) {
        content += '<option value="' + masterkondisi[i]["valueId"] + '">' + masterkondisi[i]["valueName"] + '</option>';
    }
    content += '</select>';
    content += '</div>';
    content += '</div>';
    
    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Remarks</label>';
    content += '<div class="col-sm-7">';
    content += '<input type="text" class="form-control" id="formRemarks" >';
    //content += '<textarea id = "formRemarks" style="height: 70px; width: 209px;"></textarea>';
    content += '</div>';
    content += '</div>';
    
    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Petugas</label>'
    content += '<div class="col-sm-7">';
    content += '<select id="selectPetugas" class="form-control selectpicker" data-live-search="true" data-size="5" style="width:100%;">';
    for (var i = 0; i < listPetugas.length; i++) {
        content += '<option value="' + listPetugas[i]["idSurveyor"] + '">' + listPetugas[i]["namaSurveyor"] + '</option>';
    }
    content += '</select>';
    content += '</div>';
    content += '</div>';
    
    content += '<div class="form-group">';
    content += '<label for="inputEmail3" class="col-sm-3 control-label">Insentif</label>'
    content += '<div class="col-sm-7">';
    content += '<label><input type="radio" class="form-control flat-blue" name="berbayar" value="1"> Ya </label>';
    content += '<label style="margin-left:50px"><input type="radio" class="form-control flat-blue" name="berbayar"  value="0" > Tidak </label>';
    content += '</div>';
    content += '</div>';

    content += '</div>';
    content += '<div class="col-sm-1">';
    content += '</div>';
    content += '</div>';

    content += '</div>';

    content += '<div class="box-footer">';

    if (userAccess[0]["mdlAccCode"] == "WLR4" || userAccess[0]["mdlAccCode"] == "WLR6" ) {
        content += '<div class="col-sm-4" style="margin-left: 70px;margin-bottom: 10px;"><button type="submit" class="btn btn-primary pull-right" id="btnAddMeasurement">Save</button></div>';
    }
    content += '</div></div>';

    $("#formMeasure").html(content);


    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    })

    //$("#formJabatan").val(jabatanPetugas[0]["Jabatan"]);
    $('.selectpicker').selectpicker('refresh');

    $('#tanggalInput').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        alwaysShowCalendars: false,
        timePicker: false,
        timePicker24Hour: false,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });
    $("#formMeasure").dialog({
        "title": "Add Measure Data",
        "width": 550,
        "height": 650,
        "modal" : true
    });
   

    $("#btnAddMeasurement").click(function () {

        var tanggal = moment($('#tanggalInput').val(), 'DD/MM/YYYY').format('MM/DD/YYYY');
        var dynamicM = [];
        var arrayImage = [];
        var arrData = [];
        var jsonImage = {};
        var tmpImage = {};
        var jsonData = {};
        var JsonFileDetail = {};
        var dynamicMobj = {};

        var fileUpload = $("#ImagesMeasureData").get(0);
        var files = fileUpload.files;
        var file = document.getElementById('preview').src;
        var reader = new FileReader(file);
        var preview = document.querySelector('img');
        var data = new FormData();

        for (var i = 0; i < files.length; i++) {
            jsonImage["filename"] = Date.now() + ".jpeg";
            jsonImage["filesize"] = files[i].size, files[i];

            //console.log('filename = ' + files[i].name, files[i]);
            //console.log('filesize = ' + files[i].size, files[i]);

        }
        jsonData["Accuracy"] = 0;
        jsonData["DateInputMeasure"] = Date.now();
        jsonData["DateMeasure"] = tanggal;
        
        var lDiff = timeDifference(Date.now(),new Date(moment($('#tanggalInput').val(), 'DD/MM/YYYY').format('YYYY-MM-DD')).getTime());
        if (lDiff.day > 3) {
            alert("tanggal yang ada input sudah lewat dari 3 hari")
            return;
        }

        var a = document.getElementById("preview").src;
        var b = a.split(',');
        jsonImage["content"] = b[1];
        jsonData["FotoMeasure"] = [jsonImage];

        jsonData["Latitude"] = 0;
        jsonData["Longitude"] = 0;
        jsonData["RecordMeasureId"] = "1";
        jsonData["RecordStationId"] = $("#selectFilterRambu").val();


        dynamicMobj = {}
        dynamicMobj["questionId"] = 1;
        dynamicMobj["answer"] = $("#formNilai").val();
        dynamicM.push(dynamicMobj);

        dynamicMobj = {}
        dynamicMobj["questionId"] = 5;
        dynamicMobj["answer"] = $("#formRemarks").val();
        dynamicM.push(dynamicMobj);

        dynamicMobj = {}
        dynamicMobj["questionId"] = 6;
        dynamicMobj["answer"] = $("#formKondisi").val();
        dynamicM.push(dynamicMobj);

        jsonData["dynamic"] = JSON.stringify(dynamicM);

        jsonData["upload"] = false;
        jsonData["UserID"] = parseInt($("#uID").val());
        jsonData["petugas"] = $("#selectPetugas").val();
        jsonData["berbayar"] = $("input:radio[name=berbayar]:checked").val();


        //objSend["measurementId"] = "";
        //objSend["recordStationId"] = $("#selectFilterRambu").val();
        //objSend["inputBy"] = $('#uID').val();
        //objSend["status"] = 1;
        //objSend["tanggal"] = tanggal;        

        if ($("#formNilai").val() == "") {
            alert("Mohon Input Nilai")
            return;
        }

        if (!fileUpload.files[0]["type"].includes("image")) {
            alert("Mohon Upload Images")
            return;
        }

        if ($("input:radio[name=berbayar]:checked").val() == undefined) {
            alert("Mohon Tentukan Insentif")
            return;
        }

        SaveNewMeasure(jsonData);
        console.log("SaveNewMeasure",JSON.stringify(jsonData));


    });

    function SaveNewMeasure(objEdit) {
        $.ajax({
            type: 'POST',
            url: '../Service/WebService.asmx/SaveNewMeasure',
            data: JSON.stringify({ obj: JSON.stringify(objEdit) }),
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                alert('data pengukuran telah dimasukkan');
                $("#formMeasure").dialog("close");
                $(".loading").hide();
                var url = new URL(window.location.href);
                var param = url.searchParams.get("WLR");
                if (param == "WLR_DataRambuAir") {
                    getDataRambuAir();
                } else if (param == "WLR_DataZona") {
                    GetStationMeasureArray();
                } else if (param == "WLR_DataMingguan") {
                    setData(false);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("function SaveNewMeasure : " + xhr.statusText);
            }
        });
    }
    function previewFile() {
        console.log('preview file');
        var preview = document.querySelector('img');
        var file = document.getElementById('ImagesMeasureDate').files[0];
        console.log('file = ' + file);
        var reader = new FileReader();

        reader.onload = function (e) {
            //preview.src = reader.result;
            document.getElementById("preview").src = e.target.result;
        }

        if (file) {
            // alert('ada file');
            reader.readAsDataURL(file);

        } else {
            alert('tidak ada file');
            preview.src = "";
        }
    }
    $("#ImagesMeasureData").change(function () {
        console.log('preview file');
        var preview = document.querySelector('img');
        var file = document.getElementById('ImagesMeasureData').files[0];
        console.log('file = ' + file);
        var reader = new FileReader();

        reader.onload = function (e) {
            //preview.src = reader.result;
            document.getElementById("preview").src = e.target.result;
        }

        if (file) {
            // alert('ada file');
            reader.readAsDataURL(file);

        } else {
            alert('tidak ada file');
            preview.src = "";
        }
    });


    //function UploadFoto() {
    //    alert('upload foto');
    //    $(".loading").show();
    //    var fileUpload = $("#ImagesMeasureData").get(0);
    //    var files = fileUpload.files;
    //    //var x = document.getElementById("uploadExcel").value;
    //    console.log('fileUpload = ' + fileUpload);
    //    console.log('upload foto = ' + files);

    //    var data = new FormData();
    //    for (var i = 0; i < files.length; i++) {
    //        console.log('foto = '+files[i].name, files[i]);
    //        data.append(files[i].name, files[i]);
    //    }
    //    var dynamicMobj = {};
    //    dynamicMobj["questionId"] = 1;
    //    dynamicMobj["answer"] = $("#formNilai").val();
    //    dynamicM.push(dynamicMobj);

    //    dynamicMobj = {};
    //    dynamicMobj["questionId"] = 5;
    //    dynamicMobj["answer"] = $("#formRemarks").val();
    //    dynamicM.push(dynamicMobj);
    //    $.ajax({
    //        type: 'POST',  
    //        data: data,
    //        contentType: false,
    //        processData: false,
    //        xhr: function () {
    //            var myXhr = $.ajaxSettings.xhr();
    //            if (myXhr.upload) {
    //                myXhr.upload.addEventListener('sukses progress', function (e) {
    //                    if (e.lengthComputable) {
    //                        $('progress').attr({
    //                            value: e.loaded,
    //                            max: e.total,
    //                        });
    //                        console.info("sukses total upload", e.loaded);
    //                    }
    //                }, false);
    //            }
    //            return myXhr;
    //        },
    //        success: function (response) {
    //            console.log('response = ' + response)
    //            setupTableExcel(JSON.parse(response));
    //            $("#buttonModalTable").trigger('click');

    //        },
    //        error: function (xhr, ajaxOptions, thrownError) {
    //            alert("error function UploadFile : " + xhr.statusText);
    //            var x = document.getElementById("divStatusFailed");
    //            x.style.visibility = 'visible';
    //        },
    //        complete: function () {
    //            $("#idproggres").hide();
    //            $(".loading").hide();
    //        }
    //    });
    //}

}

function valueDefault(nilai, idnya) {
    if (idnya.id == "formKondisi" || idnya.id == "selectPetugas") {
        $("#" + idnya.id).val(nilai).change();
    } else {
        $("#" + idnya.id).val(nilai);
    }
}

function getListPetugasActive() {
    listPetugas = [];

    var idWmAreaF = $('#selectFilterCompany').val();
    if (idWmAreaF == undefined) {
        idWmAreaF = $('#selectFilterWMArea').val();
    }

    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/getListPetugasActive',
        data: '{idWmArea: "' + idWmAreaF + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            listPetugas = JSON.parse(response.d);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getListPetugas : " + xhr.statusText);
        }
    });
}

function parseCookies() {
    var cookies = {};
    if (document.cookie && document.cookie !== '') {
        var cookieArray = document.cookie.split(';');
        for (var i = 0; i < cookieArray.length; i++) {
            var cookie = cookieArray[i].trim();
            var cookieParts = cookie.split('=');
            var cookieName = decodeURIComponent(cookieParts[0]);
            var cookieValue = decodeURIComponent(cookieParts[1]);
            cookies[cookieName] = cookieValue;
        }
    }
    return cookies;
}

function drawTableModalModule(data) {
    var cookies = JSON.parse(JSON.stringify(parseCookies()));
    var moduleUserWlr = JSON.parse(cookies["moduleUserWlr"]);

    var content = '<tr>';
    if (data["mdlAccCode"] == moduleUserWlr[0]["mdlAccCode"]){
        content = '<tr style="background-color:yellow">';
    }

    content += '<td>' + data["mdlAccCode"] + '</td>';
    content += '<td>' + data["mdlAccDesc"] + '</td>';
    content += '<td>' + data["mdlAccInfo"] + '</td>';
    content += '</tr>';

    return content;
}


function showPICWLR() {
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/showPICWLRWMarea',
        data: '{idWmArea: "' + $("#selectFilterCompanyPIC").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            tblPicWlr.clear().draw();
            var data  = JSON.parse(response.d);
            for (var i = 0; i < data.length; i++) {
                tblPicWlr.row.add($(drawTablePICWLR(data[i]))[0]).draw();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getListPetugas : " + xhr.statusText);
        }
    });
}

function drawTablePICWLR(data) {
    var cookies = JSON.parse(JSON.stringify(parseCookies()));
    var moduleUserWlr = JSON.parse(cookies["moduleUserWlr"]);

    var content = '<tr>';
    if (data["UserID"] == moduleUserWlr[0]["UserID"]) {
        content = '<tr style="background-color:yellow">';
    }
    content += '<td>' + data["UserFullName"] + '</td>';
    content += '<td>' + data["Jabatan"] + '</td>';
    content += '<td>' + data["mdlAccDesc"] + '</td>';
    content += '</tr>';

    return content;
}

function filterListWM(listCompanyX) {
    var listCompany = [];
    cookies = JSON.parse(JSON.stringify(parseCookies()));
    cookiesEstateAccessWMAreaMapping = JSON.parse(cookies["estateAccessWMAreaMapping"]);
    
    for (var i = 0; i < listCompanyX.length; i++) {
        for (var j = 0; j < cookiesEstateAccessWMAreaMapping.length; j++) {
            if (listCompanyX[i]["idWMArea"] == cookiesEstateAccessWMAreaMapping[j]["idWMArea"]) {
                listCompany.push(listCompanyX[i]);
            }
        }
    }
    return listCompany;
}