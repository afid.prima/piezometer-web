﻿function setSelectedMenu(menu, submenu) {
    if (menu == "menuOnlinemap") {
        $("#navGear").show();
        $("#navOnlineMap").show();
        $("#toolOnlineMap").show();
        $('#toolOnlineMap').parent().addClass('active');
        $('#toolOverview').parent().addClass('active');
    } else {
        $("#navGear").hide();
        $("#navOnlineMap").hide();
        $("#toolOnlineMap").hide();
        $('#' + menu).parent().addClass('active');
        $('#' + submenu).parent().addClass('active');
    }
}

function dateFormat(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = dd + '/' + mm + '/' + yyyy;
}

function dateFormat2(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = yyyy + '' + mm + '' + dd;
}

function dateFormat3(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();
    var hh = date.getHours();
    var min = date.getMinutes();
    var ss = date.getSeconds();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = dd + '/' + mm + '/' + yyyy + '  -  ' + zeroPad(hh,2) + ':' + zeroPad(min,2) + ':' + zeroPad(ss,2);
}

function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
}

function getValueTinggiMukaAir(data) {
    var alldata = {};
    if (data == null) {
        alldata["nilai"] = "";
        alldata["kondisi"] = "";
        alldata["kondisiId"] = "";
        alldata["remarks"] = "";
    } else {
        for (var i = 0 ; i < data.length; i++) {
            if (data[i]["questionId"] == 1) {
                alldata["nilai"] = data[i]["answer"];
            } else if (data[i]["questionId"] == 5) {
                alldata["remarks"] = data[i]["answer"];
            } else if (data[i]["questionId"] == 6) {
                alldata["kondisiId"] = data[i]["answer"];
                if (data[i]["answer"] == "1") {
                    alldata["kondisi"] = "Baik";
                } else if (data[i]["answer"] == "2") {
                    alldata["kondisi"] = "Rusak/Hilang";
                } else if (data[i]["answer"] == "3") {
                    alldata["kondisi"] = "Perlu Diperbaiki";
                }
            }
        }
    }
    return alldata;
}

function rndColor() {    
    var hex = ['0', '1', '2', '3', '4', '5', '6', '7',
               '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'],
        color = '#', i;
    for (i = 0; i < 6 ; i++) {
        color = color + hex[Math.floor(Math.random() * 16)];
    }
    if (color == "#00FF00") {
        rndColor();
    } else {
        return color;
    }
}

function predicateBy(prop) {
    return function (a, b) {
        if (a[prop] > b[prop]) {
            return 1;
        } else if (a[prop] < b[prop]) {
            return -1;
        }
        return 0;
    }
}

function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function (m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function hexc(colorval) {
    var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    delete (parts[0]);
    for (var i = 1; i <= 3; ++i) {
        parts[i] = parseInt(parts[i]).toString(16);
        if (parts[i].length == 1) parts[i] = '0' + parts[i];
    }
    return '#' + parts.join('');
}

function timeDifference(date1, date2) {
    var difference = date1 - date2;

    var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
    difference -= daysDifference * 1000 * 60 * 60 * 24

    var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
    difference -= hoursDifference * 1000 * 60 * 60

    var minutesDifference = Math.floor(difference / 1000 / 60);
    difference -= minutesDifference * 1000 * 60

    var secondsDifference = Math.floor(difference / 1000);

    console.log('difference = ' +
        daysDifference + ' day/s ' +
        hoursDifference + ' hour/s ' +
        minutesDifference + ' minute/s ' +
        secondsDifference + ' second/s ');
    var obj = {
        day: daysDifference,
        hour: hoursDifference,
        minute: minutesDifference,
        second: secondsDifference
    }
    return obj;
}