﻿var listRambuAir, listCurahHujan, selectedRambu, selectedRambuMeasure;
var labelChart;
var chartData, chart;
var rata2CurahHujan, elvTanah;
var awal;
var minY, maxY;
var nilaiMaxCH, chartCH;
var arrayNote, chartNote;
var tableNote;
var tableSetColor;
var listPetugas;
//////////////////////////////////////////////////////////
///////////////// REGION ONLOAD ///////////////////
//////////////////////////////////////////////////////////
$(function () {
	selectFilterRambuAir
	//setSelectedMenu("menuData", "dataZona");
	$("#idproggres").hide();
	$("#btnHapusGraph").hide();
	masterZona = [];
	selectedRambu = [];
	selectedRambuMeasure = [];
	listRambuAir = [];
	listCurahHujan = [];
	labelChart = [];
	rata2CurahHujan = [];
	chartData = [];
	arrayNote = [];
	elvTanah = null;
	awal = true;
	nilaiMaxCH = 3;

	$('.selectpicker').selectpicker({});

	minY = null;
	maxY = null;

	listWMArea = filterListWM(JSON.parse($('#ListWMArea').val()));

	if ($('#uAcc').val() != "") {
		userAccess = JSON.parse($('#uAcc').val());
	} else {
		userAccess = [];
		userAccess.push({
			mdlAccCode: "Guest"
		});
	}

	$('#selectFilterWMArea').find('option').remove();
	for (var i = 0; i < listWMArea.length; i++) {
		$("#selectFilterWMArea").append($('<option>', {
			value: listWMArea[i]["idWMArea"],
			text: listWMArea[i]["wmAreaName"]
		}));
	}
	getZonebyWmAreaCode();
	getGraphByWMArea();
	GetPzoWeekTemp();
	if (userAccess[0]["mdlAccCode"] == "WLR4") {
		$("#divActionBtn").show();
	} else {
		$("#divActionBtn").hide();
	}

	//////////////////////////////////////////////////////////
	///////////////////// EVENT HANDLER //////////////////////
	//////////////////////////////////////////////////////////
	tableNote = $("#tableNote").DataTable({
		columns: [
			{ data: 'noteId' },
			{ data: 'tanggal', "width": "20%" },
			{ data: 'note', "width": "60%" },
			{ data: 'status', "width": "20%" },
			{ data: 'statusId' }
		],
		"columnDefs": [{ "visible": false, "targets": [0, 4] }],
	});

	$('#tableNote tbody').on('click', 'tr', function () {
		//alert('Row index: ' + tableNote.row(this).index());
		showFormNote(tableNote.row(this).data(), tableNote.row(this).index());
	});

	$('#selectFilterWMArea').change(function () {
		$("#btnHapusGraph").hide();
		getGraphByWMArea();
		getZonebyWmAreaCode();
		selectedRambu = [];
		chartData = [];
		chipsRambuRefrensi();

		getListPetugasActive();

		$('#namaGraph').val("");
		$("#btnUploadImg").val("");
		$("#divDownload").html("");
		setupChart();
		getGraphSelectedForReport();
	});

	$('#selectFilterZona').change(function () {
		console.log('dropdown graph di click');
		selectedRambu = [];
		var rambuSelected = [];

		setupRambuAir();
		setupRambuCurhaHujan();
		chartCH = {};
		tableNote.clear().draw();
		tableSetColor.clear().draw();
		console.log('master zona = ' + JSON.stringify(masterZona));
		//ambil rambu curah hujan
		for (var i = 0; i < masterZona.length; i++) {
			if (masterZona[i]["idGraph"] == $('#selectFilterZona').val()) {
				var chSelect = [];
				$("#keteranganGrafik").val(masterZona[i]["deskripsi"]);
				if (masterZona[i]["deskripsi"] != null) {
					var contentKeterangan = "";
					contentKeterangan += "Note : ";
					var text = masterZona[i]["deskripsi"];
					contentKeterangan += text;
					$('#divDeskripsiGrafik').html(contentKeterangan);
				}
				if (masterZona[i]["rambuCurahHujan"] != null) {
					var spl = masterZona[i]["rambuCurahHujan"].split(',');
					for (var s = 0; s < spl.length; s++) {
						chSelect.push(spl[s].trim());
						//console.log('rambu curah hujan = ' + chSelect);
					}
					$("#selectFilterCurahHujan").select2().val(chSelect).trigger("change");
				} else {
					$("#selectFilterCurahHujan").select2().val(chSelect).trigger("change");
				}
				break;
			}
		}

		for (var i = 0; i < listRambuAir.length; i++) {
			if (listRambuAir[i]["idGraph"] == $('#selectFilterZona').val()) {
				var rambu = listRambuAir[i];
				if (listRambuAir[i]["colorField"] == undefined || listRambuAir[i]["colorField"] == null || listRambuAir[i]["colorField"] == "") {
					rambu["color"] = rndColor();
				} else {
					rambu["color"] = listRambuAir[i]["colorField"];
				}

				rambuSelected.push(listRambuAir[i]["stationId"]);
				selectedRambu.push(rambu);
			}
		}
		$('#selectFilterRambuAir').selectpicker('val', rambuSelected);
		chipsRambuRefrensi();

		if ($('#selectFilterZona option:selected').text() == "Tidak Ada Graph") {
			$('#namaGraph').val("");
			$('#keteranganGrafik').val("");
		} else {
			$('#namaGraph').val($('#selectFilterZona option:selected').text());
		}
		$("#btnUploadImg").val("");
		var html = "";
		for (var i = 0; i < masterZona.length; i++) {
			if ((masterZona[i]["idGraph"] == $('#selectFilterZona').val()) && (masterZona[i]["fileGraph"] != null)) {
				html = '<a href="' + globalUrlWLR + masterZona[i]["fileGraph"] + '" target="_blank"><button type="button" class="btn btn-info btn-sm" id="downloadImages" style = "margin-top: 5px;"><i class="fa fa-info"></i></button>';
				document.getElementById("btnExportPdf").style.marginRight = "30px";
				break;
			} else {
				document.getElementById("btnExportPdf").style.marginRight = "0px";
			}
		}
		$("#divDownload").html(html);

		if ($('#selectFilterZona').val() == 0) {
			$("#btnHapusGraph").hide();
		} else {
			$("#btnHapusGraph").show();
		}
		GetTmasTarget($("#selectFilterZona").val());
		GetDetailNote();
		GetSettingKlasifikasi();
	});

	$('#selectFilterRambuAir').change(function () {
		var arrayRambu = [];
		$("#selectFilterRambuAir option:selected").each(function () {
			var $this = $(this);
			var ada = false;
			for (var j = 0; j < selectedRambu.length; j++) {
				if (selectedRambu[j]["stationId"] == $this.val()) {
					arrayRambu.push(selectedRambu[j]);
					ada = true;
					break;
				}
			}

			if (!ada) {
				var rambu = {};
				rambu["stationId"] = $this.val();
				rambu["stationName"] = $this.text();;
				rambu["sortField"] = selectedRambu.length;
				rambu["color"] = rndColor();
				arrayRambu.push(rambu)
			}
		});
		selectedRambu = arrayRambu;
		chipsRambuRefrensi();
	});

	$('#selectFilterCurahHujan').change(function (e) {
		if ($('#selectFilterCurahHujan').val() != undefined) {
			getValueCurahHujan();
		} else {

		}
	});
	//$('#SelectFilterWeek').change(function () {
	//	console.log('value dropdown week = ' + $('#SelectFilterWeek').val());
	//});

	$('#tglSelected').daterangepicker({
		ranges: {
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'Last 90 Days': [moment().subtract(89, 'days'), moment()],
			'Last 1 Year': [moment().subtract(365, 'days'), moment()]
		},
		startDate: moment().subtract(29, 'days'),
		endDate: moment(),
		locale: {
			format: 'DD/MM/YYYY'
		}
	});

	$('#tglSelected').change(function (e) {
		setupChart();
		GetStationMeasureArray();
	});

	$("#addNote").click(function () {
		showFormNote();
	})
	$("#addGraphColor").click(function () { //fariz
		showFormColorSetting();
	});

	$("#btnHapusGraph").click(function () {
		if ($("#selectFilterZona").val() == 0) {
			alert("Anda Belum Memilih Graph")
		}

		var r = confirm("Apakah Anda Yakin Untuk Hapus " + $("#namaGraph").val());
		if (r == true) {
			var body = {
				idWMArea: $("#selectFilterWMArea").val(),
				idGraph: $("#selectFilterZona").val()

			}
			deleteGraph(body);
		}
	});

	$("#btnSaveGraph").click(function () {
		var fileUpload = $("#btnUploadImg").get(0);
		var files = fileUpload.files;
		if (selectedRambu.length == 0) {
			alert("Anda Belum Memilih Rambu");
			return;
		}
		if ($("#namaGraph").val() == "") {
			alert("Anda Belum Memberi Nama Graph");
			return;
		}

		var detailGraph = [];
		for (var i = 0; i < selectedRambu.length; i++) {
			var objDetail = {};
			objDetail["stationId"] = selectedRambu[i]["stationId"];
			objDetail["sortField"] = selectedRambu[i]["sortField"];
			objDetail["colorField"] = selectedRambu[i]["color"];
			objDetail["TmasDefaultValue"] = $("#tmasTarget").val();
			objDetail["idZona"] = $("#DropdownFilterZona").val();
			detailGraph.push(objDetail);
		}

		var tblNoteOk = [];
		var dataNote = tableNote.data();

		for (var i = 0; i < dataNote.length; i++) {
			tblNoteOk.push(tableNote.row(i).data());
		}
		var tblKlasifikasi = [];
		var dataKlasifikasi = tableSetColor.data();
		//console.log('data klafisikasi= ' + JSON.stringify(dataKlasifikasi));
		for (var i = 0; i < dataKlasifikasi.length; i++) {
			tblKlasifikasi.push(tableSetColor.row(i).data())
		}
		var bodyData = {};
		bodyData["namaGraph"] = $("#namaGraph").val();
		bodyData["idWMArea"] = $("#selectFilterWMArea").val();
		bodyData["createBy"] = $('#uID').val();
		bodyData["detailGraph"] = detailGraph;
		bodyData["curahHujan"] = $('#selectFilterCurahHujan').val();
		bodyData["tableNote"] = tblNoteOk;
		bodyData["tempColor"] = tblKlasifikasi;
		bodyData["keteranganGrafik"] = $("#keteranganGrafik").val();
		//console.log("bodyData = " + JSON.stringify(bodyData))

		if ($("#selectFilterZona").val() == 0) {
			//console.info("insertGraph", JSON.stringify(bodyData));
			insertGraph(bodyData, files);
		} else {
			bodyData["idGraph"] = $("#selectFilterZona").val();
			//console.info("updateGraph", JSON.stringify(bodyData));
			updateGraph(bodyData, files)
		}
		$('#divDeskripsiGrafik').html("");
		$('#tmasTarget').val("");
	})

	tableSetColor = $("#tempColor").DataTable({
		columns: [
			{ data: 'namaKlasifikasi' },
			{ data: 'nilaiMinimum', "width": "20%" },
			{ data: 'nilaiMaksimum', "width": "20%" },
			{ data: 'codeWarna', "width": "60%" },
			{ data: 'status', "width": "20%" },
			{ data: 'statusId' },
			//{ data: 'warna', "width": "60%" }
		],
		"columnDefs": [{ "visible": false, "targets": [0, 5] }],

	});
	$('#tempColor tbody').on('click', 'tr', function () {
		//alert('Row index: ' + tableSetColor.row(this).index());
		showFormColorSetting(tableSetColor.row(this).data(), tableSetColor.row(this).index());
		//showFormNote(tableNote.row(this).data(), tableNote.row(this).index());

	});

	$('#btnExportPdf').click(function () {
		if ($('#selectFilterCurahHujan').val() != null) {
			if ($('#selectFilterCurahHujan').val().length > 0) {
				testJsPdfByResolusi(true);
			} else {
				testJsPdfByResolusi(false);
			}
		} else {
			testJsPdfByResolusi(false);
		}

	});
	$("#saveGraphParamater").click(function () {
		$.ajax({
			type: 'POST',
			url: '../Service/WebService.asmx/insertNewGraphParamater',
			data: '{idwmArea: "' + $('#selectFilterWMArea').val() + '",graphSelected:"' + $("#selectFilterGraph").val() + '"}',
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			success: function (response) {
				alert(response.d);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert("function getGraphByWMArea : " + xhr.statusText);
			}
		});
	});


	//SubmitUserAppUsage();

	getListPetugasActive();
});

//////////////////////////////////////////////////////////
///////////////// REGION CUSTOM METHOD ///////////////////
//////////////////////////////////////////////////////////

function setupChart() {

	getLabelChart();

	var dataChart = [];

	var dataChartNote = [];
	var dataNote = tableNote.data();
	var dataColorSetting = tableSetColor.data();
	// console.log('data color setting = ' + JSON.stringify(dataColorSetting));

	if (dataNote.length > 0) {
		for (var i = 0; i < labelChart.length; i++) {
			var nilai = null;
			var note = null;
			for (var j = 0; j < dataNote.length; j++) {
				var rowNote = tableNote.row(j).data();
				if ((labelChart[i] == rowNote["tanggal"]) && (rowNote["statusId"] == '1')) {
					//nilai = (nilaiMaxCH * 3);
					nilai = ((nilaiMaxCH / 8) * 2) + nilaiMaxCH;
					note = rowNote["note"];
					break;
				}
			}

			var sdate = labelChart[i].split('/');
			dataChartNote.push({
				x: new Date(sdate[2], parseInt(sdate[1]) - 1, sdate[0]),
				y: nilai,
				name: note
			});
		}
		console.info("chartNote", dataChartNote);
		chartNote = {};
		chartNote["type"] = "scatter";
		chartNote["legendText"] = "Note Graph";
		chartNote["color"] = "red";
		chartNote["showInLegend"] = "true";
		chartNote["toolTipContent"] = "<strong>{x}</strong><br/>{name}";
		chartNote["axisYType"] = "secondary";
		chartNote["dataPoints"] = dataChartNote;
		chartNote["labelMaxWidth"] = "50";

		dataChart.push(chartNote);
	}

	if (chartCH != undefined) {
		dataChart.push(chartCH);
	}

	for (var i = 0; i < chartData.length; i++) {
		dataChart.push(chartData[i]);
	}
	var titikY = {
		title: "Tinggi Muka Air Saluran",
		titleFontSize: 15,
		labelFontSize: 13
	}
	//alert('halo');

	var tmasTarget;
	if (elvTanah != null) {
		var elvTanah2 = elvTanah - 0.2;
		var elvTanah4 = elvTanah - 0.4;
		var elvTanah6 = elvTanah - 0.6;
		var stripLine = [{
			value: elvTanah,
			label: "0"
		}, {
			value: elvTanah2,
			label: "0.2"
		}, {
			value: elvTanah4,
			label: "0.4"
		}, {
			value: elvTanah6,
			label: "0.6"
		}];
		var tmasTarget = $("#tmasTarget").val();
		console.log('hasil penjumlahan = ' + (parseFloat(tmasTarget) + (parseFloat(0.05))).toFixed(2));
		if (tmasTarget != "") {
			let sValTMAST = parseFloat(tmasTarget - 0.05);
			let eValTMAST = parseFloat(tmasTarget + 0.05);

			if (tmasTarget.length == 1) {
				eValTMAST = parseFloat(tmasTarget + ".05");
			}

			stripLine.push({
				label: "TMAS Target",
				startValue: sValTMAST,
				endValue: eValTMAST,
				color: "#b3f2c2",
				labelAlign: "near"
			})
		}

		if (dataColorSetting.length > 0) {

			//console.log('data color setting kut kut kut = ' + JSON.stringify(dataColorSetting));            
			for (var i = 0; i < dataColorSetting.length; i++) {
				var rowColor = tableSetColor.row(i).data();
				if (dataColorSetting[i]["statusId"] == "1") {
					stripLine.push({
						label: rowColor["namaKlasifikasi"],
						startValue: rowColor["nilaiMinimum"],
						endValue: rowColor["nilaiMaksimum"],
						color: rowColor["codeWarna"],
						labelAlign: "near"
					});
				}
			}

		}
		console.log('object fix stripline = ' + JSON.stringify(stripLine));
		titikY["stripLines"] = stripLine;
	}

	if (tmasTarget != undefined) {
		if (tmasTarget != "") {
			if (tmasTarget < minY) {
				titikY["minimum"] = parseFloat(tmasTarget) - 0.1;
			} else if (tmasTarget > maxY) {
				titikY["maximum"] = parseFloat(tmasTarget) + 0.1;
			}
		}
	}

	//titikY["maximum"] = maxY + 0.1;
	//titikY["minimum"] = minY - 0.1;
	titikY["includeZero"] = false;
	console.info("titikY", JSON.stringify(titikY));

	var textTitle = "";

	if ($('#selectFilterZona').val() != 0) {
		textTitle = $('#selectFilterZona option:selected').text();
	}
	chart = new CanvasJS.Chart("divChart", {
		title: {
			text: textTitle,
			fontWeight: "bolder",
			fontFamily: "Calibri",
			fontSize: 21,
			padding: 10,
			labelMaxWidth: 50
		},
		zoomEnabled: true,
		panEnabled: true,
		axisXType: "secondary",
		animationEnabled: true,
		labelMaxWidth: 50,
		axisY: titikY,
		axisY2: {
			maximum: ((nilaiMaxCH / 8) * 2) + nilaiMaxCH,
			title: "Curah Hujan",
			titleFontSize: 15,
			labelFontSize: 13,
			labelMaxWidth: 50
		},
		axisX: {
			valueFormatString: "DD-MMM",
			title: "Tanggal",
			titleFontSize: 15,
			labelFontSize: 13,
			labelMaxWidth: 50
		},
		legend: {
			fontSize: 11,
			verticalAlign: "bottom",
			horizontalAlign: "center",
			labelMaxWidth: 50
		},
		data: dataChart
	});

	chart.render();
	var contentCH = "";
	var textOptionCH = [];
	if ($("#selectFilterCurahHujan").val() != null) {
		var b = $("#selectFilterCurahHujan").val();
		var a = $("#selectFilterCurahHujan option:selected").text();
		var textMulti = $.map($("#selectFilterCurahHujan option:selected"), function (el, i) {
			return $(el).text();
		});
		var a = textMulti.join(", ");
		var newCH = a.split(',')
		//console.log('panjang text baru = ' + newCH.length);
		contentCH += "Rata rata curah hujan diambil dari " + newCH.length + " Ombrometer yaitu <br/> ";
		for (var i = 0; i < $("#selectFilterCurahHujan").val().length; i++) {
			contentCH += '<div style =" margin-right: 20px;width: 100px; display: inline">' + '\u25cf' + newCH[i] + '</div>';
		}
		//for (var i = 0; i < $("#selectFilterCurahHujan").val().length; i++) {
		//    contentCH += '<div style =" margin-right: 20px;width: 100px; display: inline">' + '\u25cf' + $("#selectFilterCurahHujan").val()[i] + '</div>';
		//}
	}
	contentCH += "<br>Keterangan Jenis Rambu<br>"
	contentCH += "<i class='fa fa-circle'></i> Daily"
	contentCH += "<i class='fa fa-square' style = margin-left:40px;></i> Weekly"
	contentCH += "<i class='fa fa-caret-up'  style = margin-left:40px;></i> Random"
	$("#divKetChart").html(contentCH)
}

function addDataSet(idx) {
	var charRambu = [];
	var rambuM = selectedRambuMeasure[idx];

	if (idx == 0) {
		elvTanah = parseFloat(rambuM["elevasiTanah"]);
	}
	for (var i = 0; i < labelChart.length; i++) {
		var nilai = null;
		var measure = JSON.parse(rambuM["measure"]);
		for (var j = 0; j < measure.length; j++) {
			if (labelChart[i] == dateFormat(new Date(measure[j]["measurementDate"]))) {
				nilai = parseFloat(measure[j]["Nilai"]);
				break;
			}
		}


		if (nilai != null) {
			if (nilai > maxY) {
				maxY = nilai;
			}

			if (minY == null) {
				minY = nilai;
			} else if (nilai < minY) {
				minY = nilai;
			}
		}
		var sdate = labelChart[i].split('/');
		charRambu.push({
			x: new Date(sdate[2], parseInt(sdate[1]) - 1, sdate[0]),
			y: nilai
		});
	}
	//console.info(' ini chart'+ charRambu);
	var markerSymbol = "circle";
	if (rambuM["frequency"] == 2) {
		markerSymbol = "square";
	} else if (rambuM["frequency"] == 3) {
		markerSymbol = "triangle";
	}

	var newSeries = {
		name: rambuM["stationId"],
		legendText: rambuM["stationName"],
		color: rambuM["color"],
		markerColor: rambuM["color"],
		markerType: markerSymbol,
		connectNullData: true,
		nullDataLineDashType: "solid",
		type: "line",
		click: function (e) {
			var measurementId = 0;
			for (var i = 0; i < selectedRambuMeasure.length; i++) {
				if (selectedRambuMeasure[i]["stationId"] == e.dataSeries.name) {
					var measure = JSON.parse(selectedRambuMeasure[i]["measure"]);

					for (var j = 0; j < measure.length; j++) {
						if (dateFormat(new Date(measure[j]["measurementDate"])) == dateFormat(e.dataPoint.x)) {
							measurementId = measure[j]["measurementId"];
							break;
						}
					}
				}
			}
			editMeasure(measurementId);
			//alert(e.dataSeries.name + ", "+measurementId);
			//alert(e.dataSeries.name + ", dataPoint { x:" + e.dataPoint.label + ", y: " + e.dataPoint.y + " }");
		},
		markerSize: 8,
		showInLegend: "true",
		toolTipContent: "{legendText}<br/>{x}, <strong>{y}</strong> m",
		dataPoints: charRambu
	};


	//console.info("charRambu", newSeries);

	chartData.push(newSeries);

	idx++;
	if (idx < selectedRambuMeasure.length) {
		addDataSet(idx)
	} else {
		if ($('#selectFilterCurahHujan').val() != null) {
			getValueCurahHujan();
		} else {
			setupChart();
		}
	}
}

function addDataCurahHujan(rata2CurahHujan) {
	var charRataCH = [];
	nilaiMaxCH = 3;
	for (var i = 0; i < labelChart.length; i++) {
		var nilai = null;
		for (var j = 0; j < rata2CurahHujan.length; j++) {
			if (labelChart[i] == dateFormat(new Date(parseInt(rata2CurahHujan[j]["date"].replace('/Date(', '').replace(')/', ''))))) {
				nilai = Math.round(rata2CurahHujan[j]["sumRainInches"]);
				break;
			}
		}

		if (nilai != null) {
			if (nilaiMaxCH < nilai) {
				nilaiMaxCH = nilai;
			}
		}
		var sdate = labelChart[i].split('/');
		charRataCH.push({
			x: new Date(sdate[2], parseInt(sdate[1]) - 1, sdate[0]),
			y: nilai
		});
	}
	console.info("charRataCH", charRataCH);
	chartCH = {};
	chartCH["name"] = "Rata Rata Curah Hujan";
	chartCH["type"] = "column";
	chartCH["color"] = "orange";
	chartCH["showInLegend"] = "true";
	chartCH["toolTipContent"] = "{name}<br/>{x}, <strong>{y}</strong> mm";
	chartCH["axisYType"] = "secondary";
	chartCH["dataPoints"] = charRataCH;
	chartCH["fillOpacity"] = .6;
	chartCH["labelMaxWidth"] = 50;
	setupChart();
}

function getLabelChart() {
	var split = $('#tglSelected').val().split('-');

	var sd = split[0].trim().split("/");
	var ed = split[1].trim().split("/");

	var startDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
	var endtDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

	labelChart = [];
	for (var now = startDate; now <= endtDate; now.setDate(now.getDate() + 1)) {
		labelChart.push(dateFormat(now));
	}
}

function chipsRambuRefrensi() {
	selectedRambu = _.uniq(selectedRambu, function (x) {
		return x.stationId;
	});

	if (selectedRambu.length > 0) {
		selectedRambu.sort(predicateBy("sortField"));
		//console.info("rambu selected ", JSON.stringify(selectedRambu));
		var content = '<ul id="tgldragdropRambu" class="connectedSortable" style="margin-left:-45px">';
		for (var i = 0; i < selectedRambu.length; i++) {
			content += '<li class="ui-state-default chips" id="' + selectedRambu[i]["stationId"] + '" style="background:' + selectedRambu[i]["color"] + '"><font color="white">' + selectedRambu[i]["stationName"] + '</font></li>';
		}
		content += '</ul>';
		$("#divRambuRefrensi").html(content);

		$('#tgldragdropRambu').sortable({
			connectWith: ".connectedSortable",
			update: function () {
				var order1 = $('#tgldragdropRambu').sortable('toArray')
				for (var i = 0; i < order1.length; i++) {
					for (var j = 0; j < selectedRambu.length; j++) {
						if (selectedRambu[j]["stationId"] == order1[i]) {
							selectedRambu[j]["sortField"] = i;
							break;
						}
					}
				}
				selectedRambu.sort(predicateBy("sortField"));
				console.info("rambu selected change ", JSON.stringify(selectedRambu));
				GetStationMeasureArray();
			}
		}).disableSelection();

		$("#tgldragdropRambu li").click(function () {
			var namaRambu = $(this).html().replace('<font color="white">', "").replace('</font>', '');
			var paramRambu = { stationId: $(this).context.id, stationName: namaRambu };
			changeColor(paramRambu);
		});
		GetStationMeasureArray();
	} else {
		$("#divRambuRefrensi").html("");
	}
}

function setupMasterZona() {
	$('#selectFilterZona').find('option').remove();
	$("#selectFilterZona").append($('<option>', {
		value: "0",
		text: "Tidak Ada Graph"
	}));
	$('#selectFilterGraph').find('option').remove();
	$("#selectFilterGraph").append($('<option>', {
		value: "0",
		text: "Tidak Ada Graph"
	}));
	getGraphSelectedForReport();
	for (var i = 0; i < masterZona.length; i++) {
		$("#selectFilterZona").append($('<option>', {
			value: masterZona[i]["idGraph"],
			text: masterZona[i]["namaGraph"]
		}));
	}
	for (var i = 0; i < masterZona.length; i++) {
		$("#selectFilterGraph").append($('<option>', {
			value: masterZona[i]["idGraph"],
			text: masterZona[i]["namaGraph"]
		}));
	}
	$('.selectpicker').selectpicker('refresh');
}

function setupRambuAir() {
	$('#selectFilterRambuAir').find('option').remove();
	var statX = "";
	for (var i = 0; i < listRambuAir.length; i++) {
		if (statX != listRambuAir[i]["stationId"]) {
			$("#selectFilterRambuAir").append($('<option>', {
				value: listRambuAir[i]["stationId"],
				text: listRambuAir[i]["stationName"]
			}));
		}
		statX = listRambuAir[i]["stationId"];
	}
	$('.selectpicker').selectpicker('refresh');
}

function setupRambuCurhaHujan() {
	$('#selectFilterCurahHujan').find('option').remove();
	for (var i = 0; i < listCurahHujan.length; i++) {
		$("#selectFilterCurahHujan").append($('<option>', {
			//value: listCurahHujan[i]["estCode"] + ":" + listCurahHujan[i]["division"],
			//text: listCurahHujan[i]["estCode"] + " : " + listCurahHujan[i]["division"]

			value: listCurahHujan[i]["estCode"] + ":" + listCurahHujan[i]["FCCODE"],
			text: listCurahHujan[i]["FCBA"] + " : " + listCurahHujan[i]["FCCODE"]
		}));
	}
	$('.select2').select2();
}

function updateGraphSelected(body) {
	$('#selectFilterWMArea').val(body["idwmarea"]).change();
	chartCH = {};
	tableNote.clear().draw();
	tableSetColor.clear().draw();
	$('#keteranganGrafik').val("");
}

function changeColor(paramRambu) {
	var color = "";
	for (var i = 0; i < selectedRambu.length; i++) {
		if (selectedRambu[i]["stationId"] == paramRambu["stationId"]) {
			color = selectedRambu[i]["color"];
			break;
		}
	}

	var content = '<div class="row" align="center">';
	content += '<div class="row" align="center"><label>Select Color</label></div>';
	content += '<div class="row" id="colorSelected" style="background-color:' + color + ';width:50px;height:50px;"></div><br>';
	content += '<div class="row" align="center"><button type="submit" class="btn btn-primary" id="btnSaveColor">Save</button></div>';
	content += '</div>';
	$("#formChangeColor").html(content);

	$("#formChangeColor")
		.dialog({
			"title": paramRambu["stationName"],
			"width": 180,
			"height": 200
		});
	$('#colorSelected').colorpicker();
	$('#colorSelected').colorpicker().on(
		'changeColor',
		function () {
			$('#colorSelected').css('background-color',
				$(this).colorpicker('getValue', color));
		});
	$('#btnSaveColor').click(function () {
		for (var i = 0; i < selectedRambu.length; i++) {
			if (selectedRambu[i]["stationId"] == paramRambu["stationId"]) {
				var x = $('#colorSelected').css('backgroundColor');
				selectedRambu[i]["color"] = hexc(x);
				break;
			}
		}
		chipsRambuRefrensi();
		GetStationMeasureArray();
		$("#formChangeColor").dialog("close");
	})
}

//////////////////////////////////////////////////////////
///////////////// REGION WEB SERVICES ////////////////////
//////////////////////////////////////////////////////////

function getGraphByWMArea() {
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetGraphByWMArea',
		data: '{wmArea: "' + $('#selectFilterWMArea').val() + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			masterZona = [];
			masterZona = JSON.parse(response.d);
			setupMasterZona();
			getRambuAirByWMArea();
			//$(".loading").hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function getGraphByWMArea : " + xhr.statusText);
		}
	});
}

function getRambuAirByWMArea() {
	//$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetRambuAirByWMArea',
		data: '{wmArea: "' + $('#selectFilterWMArea').val() + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			listRambuAir = [];
			listRambuAir = JSON.parse(response.d);
			setupRambuAir();
			AllStationCurahHujan();
			//GetRataRataCurahHujanByWMArea();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function getRambuAirByWMArea : " + xhr.statusText);
		}
	});
}

function AllStationCurahHujan() {
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/getAllRambuCurahHujanByWmArea',
		data: '{idWMArea: "' + $('#selectFilterWMArea').val() + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			listCurahHujan = [];
			listCurahHujan = JSON.parse(response.d);
			$(".loading").hide();
			setupRambuCurhaHujan();
			setupChart();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function AllStationCurahHujan : " + xhr.statusText);
		}
	});
}

// rata rata curah hujan sudah tidak di pakai karena 1 rambu tmas akan mengikat ke 1 rambu curah hujan 
//function GetRataRataCurahHujanByWMArea() {
//    //$(".loading").show();
//    $.ajax({
//        type: 'POST',
//        url: '../Service/WebService.asmx/GetRataRataCurahHujanByWMArea',
//        data: '{idWMArea: "' + $('#selectFilterWMArea').val() + '"}',
//        contentType: 'application/json; charset=utf-8',
//        dataType: 'json',
//        success: function (response) {
//            rata2CurahHujan = [];
//            rata2CurahHujan = JSON.parse(response.d);
//            //console.info("rata2CurahHujan", rata2CurahHujan)
//            $(".loading").hide();

//            setupChart();
//        },
//        error: function (xhr, ajaxOptions, thrownError) {
//            alert("function GetRataRataCurahHujanByWMArea : " + xhr.statusText);
//        }
//    });
//}

function GetDetailNote() {
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetDetailNote',
		data: '{idGrap: "' + $('#selectFilterZona').val() + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			arrayNote = JSON.parse(response.d);
			for (var i = 0; i < arrayNote.length; i++) {
				var stat = "Aktif";
				if (arrayNote[i]["statusId"] == '2') {
					stat = "Tidak Aktif";
				}

				var obj = {
					noteId: arrayNote[i]["idNoteGraph"],
					tanggal: arrayNote[i]["tanggal"],
					note: arrayNote[i]["note"],
					status: stat,
					statusId: arrayNote[i]["statusId"]
				};
				tableNote.row.add(obj).draw(false);
			}
			GetStationMeasureArray();
			$(".loading").hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function getDetailNote : " + xhr.statusText);
		}
	});
}
function GetSettingKlasifikasi() {
	$(".loading").show();
	var stats;
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetKlasifikasiSetting',
		data: '{idGrap: "' + $('#selectFilterZona').val() + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			arrayKlasifikasi = JSON.parse(response.d);
			//console.log('data klasifikasi GetSettingKlasifikasi = ' + JSON.stringify(arrayKlasifikasi));
			for (var i = 0; i < arrayKlasifikasi.length; i++) {
				if (arrayKlasifikasi[i]["statusId"] == "1") {
					stats = "Aktif";
				} else {
					stats = "Tidak Aktif";
				}
				//console.log('status = ' + stats);
				//if (arrayKlasifikasi[i]["statusId"] == "1") {

				//}
				var obj = {
					namaKlasifikasi: arrayKlasifikasi[i]["NamaKlasifikasi"],
					nilaiMinimum: arrayKlasifikasi[i]["NilaiMinimum"],
					nilaiMaksimum: arrayKlasifikasi[i]["NilaiMaksimum"],
					codeWarna: arrayKlasifikasi[i]["colorField"],
					status: stats,
					statusId: arrayKlasifikasi[i]["statusId"],
					//warna: arrayKlasifikasi[i]["colorField"]
				};
				tableSetColor.row.add(obj).draw(false);
			}
			//GetStationMeasureArray();
			$(".loading").hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function getDetailNote : " + xhr.statusText);
		}
	});
}

function GetStationMeasureArray() {
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetStationMeasureArray',
		data: JSON.stringify({ stationSelected: JSON.stringify(selectedRambu) }),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			selectedRambuMeasure = [];
			selectedRambuMeasure = JSON.parse(response.d);
			//console.info("object Measure", selectedRambuMeasure);
			selectedRambuMeasure.sort(predicateBy("sortField"));
			chartData = [];
			minY = null;
			maxY = null;
			if (selectedRambuMeasure.length > 0) {
				addDataSet(0);
			} else {
				if ($('#selectFilterCurahHujan').val() != undefined) {
					getValueCurahHujan();
				} else {
					setupChart();
				}
			}
			$(".loading").hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function getMasterZona : " + xhr.statusText);
		}
	});
}

function getValueCurahHujan() {
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/getValueCurahHujan',
		data: '{stationSelected: "' + $('#selectFilterCurahHujan').val() + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			console.log('data curah hujan=' + JSON.stringify(response.d))
			addDataCurahHujan(JSON.parse(response.d));
			$(".loading").hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$(".loading").hide();
			alert("function getValueCurahHujan : " + xhr.statusText);
		}
	});
}

function insertGraph(body, file) {
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/InsertGraph',
		data: JSON.stringify({ obj: JSON.stringify(body) }),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			if (file.length > 0) {
				UploadFile(file, response.d);
			} else {
				updateGraphSelected(JSON.parse(response.d));
				$(".loading").hide();
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function SaveEditMeasure : " + xhr.statusText);
			$(".loading").hide();
		}
	});
}

function updateGraph(body, file) {
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/UpdateGraph',
		data: JSON.stringify({ obj: JSON.stringify(body) }),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			if (file.length > 0) {
				UploadFile(file, response.d);
			} else {
				updateGraphSelected(JSON.parse(response.d));
				$(".loading").hide();
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function updateGraph : " + xhr.statusText);
			$(".loading").hide();
		}
	});
}

function deleteGraph(body) {
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/DeleteGraph',
		data: JSON.stringify({ obj: JSON.stringify(body) }),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			updateGraphSelected(JSON.parse(response.d));
			$(".loading").hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function updateGraph : " + xhr.statusText);
			$(".loading").hide();
		}
	});
}

function UploadFile(fileImg, body) {
	var data = new FormData();
	for (var i = 0; i < fileImg.length; i++) {
		data.append(fileImg[i].name, fileImg[i]);
	}
	var paramURL = JSON.parse(body);
	$("#idproggres").show();
	$.ajax({
		type: 'POST',
		url: '../Service/FileUpload.ashx?idWmarea=' + paramURL["idwmarea"] + '&idGraph=' + paramURL["idGraph"],
		data: data,
		contentType: false,
		processData: false,
		// Custom XMLHttpRequest
		xhr: function () {
			var myXhr = $.ajaxSettings.xhr();
			if (myXhr.upload) {
				// For handling the progress of the upload
				myXhr.upload.addEventListener('progress', function (e) {
					if (e.lengthComputable) {
						$('progress').attr({
							value: e.loaded,
							max: e.total,
						});
						console.info("total upload", e.loaded);
					}
				}, false);
			}
			return myXhr;
		},
		success: function (response) {
			updateGraphSelected(paramURL);
			console.info("responseUpload", response);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function UploadFile : " + xhr.statusText);
		},
		complete: function () {
			$("#idproggres").hide();
			$(".loading").hide();
		}
	});

}
function testJsPdf(adaCurahHujan) {
	var selectFilterWMArea = document.getElementById("selectFilterWMArea");
	var selectFilterWmAreaText = selectFilterWMArea.options[selectFilterWMArea.selectedIndex].text;
	var tglSelected = $("#tglSelected").val();
	var newTgl = tglSelected.split("/");
	var periode = +newTgl[0] + "" + newTgl[1] + "" + newTgl[2].substring(2, 9) + "" + newTgl[3] + "" + newTgl[4].substring(2, 4);
	var today = new Date();
	var date = today.getDate();
	var month = today.getMonth() + 1;
	var year = today.getFullYear();
	if (date < 10) {
		date = '0' + date;
	}
	if (month < 10) {
		month = '0' + month;
	}
	var canvas = $("#divChart .canvasjs-chart-canvas").get(0);
	var contentKeterangan = "";
	var dataURL = canvas.toDataURL();
	var pdf = new jsPDF("p", "mm", "a3");
	var text = $('#keteranganGrafik').val();
	//var keterangan = $('#divKetChart').get(0); 
	pdf.addImage(dataURL, 'JPEG', 20, 20);
	var imageCircle = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3VAAAN1QE91ljxAAAAB3RJTUUH4gwFAgwaNyVvAgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAA9SURBVBhXY0ACIkB8BIgnAjEzSAAGtgHxfzRsD8RgHegSMAw2CpsECIPtwCYBwmDLsUmAMBiALEeTYOABAJ55I/naiWqLAAAAAElFTkSuQmCC";
	var imageSquare = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3WAAAN1gGQb3mcAAAAB3RJTUUH4gwFAg0A01ynOQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAsSURBVBhXYwCCV0D8Hwv+DcRYJWAYqyAMYxWEYayCMIxVEIYZfqEJQDHDPwD/KDCxKLNSbAAAAABJRU5ErkJggg==";
	var imageCaretUp = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3WAAAN1gGQb3mcAAAAB3RJTUUH4gwFAg0Nre3bhAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAA7SURBVBhXYyAFcENpDCADxFeBeCmYhwSkgRgk8R+KFwAxGJgC8QMghknA8BIgBhMgXaeR8BkoxgUYGACjDhBX3kKhXQAAAABJRU5ErkJggg==";
	var imageCircleSmall = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3UAAAN1AHvkboVAAAAB3RJTUUH4gwFAgwaNyVvAgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAuSURBVBhXY4ACDiBeAMSlQMwIE3gKxP+heBcQg1XABGAYrAVDEGQGSAtUgGEFAOhIETK5eVDZAAAAAElFTkSuQmCC';
	if ($("#selectFilterCurahHujan").val() != null) {
		contentKeterangan += "Rata rata curah hujan diambil dari " + $("#selectFilterCurahHujan").val().length + " Ombrometer yaitu<br/> ";
		for (var i = 0; i < $("#selectFilterCurahHujan").val().length; i++) {
			contentKeterangan += '<li style ="display: inline;">' + '\u2022' + $("#selectFilterCurahHujan").val()[i] + '</li>';

		}
	}
	var x = 20;
	var xCircle = 5;
	var y = 135;
	var yImage = y;
	pdf.setFontSize(8);
	pdf.text('Note : ', 20, 130);
	pdf.text(text, 30, 130);
	if (adaCurahHujan) {
		pdf.setFontSize(8);
		pdf.text("Rata rata curah hujan diambil dari " + $("#selectFilterCurahHujan").val().length + " Ombro meter yaitu", 20, y);
		y = y + 5;
		for (var i = 0; i < $("#selectFilterCurahHujan").val().length; i++) {
			yImage = y - 2;
			xCircle = x + 3;
			pdf.setFontSize(8);
			pdf.addImage(imageCircleSmall, 'PNG', x, yImage, 0, 0)
			pdf.text($("#selectFilterCurahHujan").val()[i], xCircle, y);
			x = x + 50;
		}
		y = y + 5;
	}
	pdf.setFontSize(8);
	x = 20;
	pdf.text("keterangan jenis rambu", 20, y)
	y = y + 5;
	yImage = y - 2;
	pdf.addImage(imageCircle, 'PNG', x, yImage, 0, 0);
	pdf.text('Daily', x + 3, y);
	x = x + 50;
	yImage = y - 2;
	pdf.text('weekly', x + 3, y);
	pdf.addImage(imageSquare, 'PNG', x, yImage, 0, 0);
	x = x + 50;
	yImage = y - 2;
	pdf.text('random', x + 3, y);
	pdf.addImage(imageCaretUp, 'PNG', x, yImage, 0, 0);
	//pdf.output("datauri"); 
	var pdfName = year + "" + month + "" + date + "_" + "WLR001" + "_" + "LaporanPencatatanCurahHujan" + "_" + selectFilterWmAreaText + "_" + periode;
	pdf.save(pdfName);
}

function getDescription() {
	var contentKeterangan = "";
	contentKeterangan += "Keterangan : ";
	var text = $('#keteranganGrafik').val();
	contentKeterangan += text;
	$('#divDeskripsiGrafik').html(contentKeterangan);
}

function DownloadGroupReport() {
	alert('link group report clicked');
}

function testJsPdfByResolusi(adaCurahHujan) {
	console.log('testJsPdfByResolusi = ' + adaCurahHujan);
	var b = $("#selectFilterCurahHujan").val();
	var a = $("#selectFilterCurahHujan option:selected").text();
	var textMulti = $.map($("#selectFilterCurahHujan option:selected"), function (el, i) {
		return $(el).text();
	});
	var a = textMulti.join(", ");
	var newCH = a.split(',')
	var mydiv = document.getElementById('divChart');
	var width = mydiv.clientWidth;
	var height = mydiv.clientHeight;
	if (width == 1366 && height == 768) {
		var selectFilterWMArea = document.getElementById("selectFilterWMArea");
		var selectFilterWmAreaText = selectFilterWMArea.options[selectFilterWMArea.selectedIndex].text;
		var tglSelected = $("#tglSelected").val();
		var newTgl = tglSelected.split("/");
		var periode = +newTgl[0] + "" + newTgl[1] + "" + newTgl[2].substring(2, 9) + "" + newTgl[3] + "" + newTgl[4].substring(2, 4);
		var today = new Date();
		var date = today.getDate();
		var month = today.getMonth() + 1;
		var year = today.getFullYear();
		if (date < 10) {
			date = '0' + date;
		}
		if (month < 10) {
			month = '0' + month;
		}
		var canvas = $("#divChart .canvasjs-chart-canvas").get(0);
		var contentKeterangan = "";
		var dataURL = canvas.toDataURL();
		var pdf = new jsPDF("p", "mm", "a3");
		var text = $('#divDeskripsiGrafik').val();
		pdf.addImage(dataURL, 'JPEG', 20, 20);
		var imageCircle = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3VAAAN1QE91ljxAAAAB3RJTUUH4gwFAgwaNyVvAgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAA9SURBVBhXY0ACIkB8BIgnAjEzSAAGtgHxfzRsD8RgHegSMAw2CpsECIPtwCYBwmDLsUmAMBiALEeTYOABAJ55I/naiWqLAAAAAElFTkSuQmCC";
		var imageSquare = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3WAAAN1gGQb3mcAAAAB3RJTUUH4gwFAg0A01ynOQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAsSURBVBhXYwCCV0D8Hwv+DcRYJWAYqyAMYxWEYayCMIxVEIYZfqEJQDHDPwD/KDCxKLNSbAAAAABJRU5ErkJggg==";
		var imageCaretUp = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3WAAAN1gGQb3mcAAAAB3RJTUUH4gwFAg0Nre3bhAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAA7SURBVBhXYyAFcENpDCADxFeBeCmYhwSkgRgk8R+KFwAxGJgC8QMghknA8BIgBhMgXaeR8BkoxgUYGACjDhBX3kKhXQAAAABJRU5ErkJggg==";
		var imageCircleSmall = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3UAAAN1AHvkboVAAAAB3RJTUUH4gwFAgwaNyVvAgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAuSURBVBhXY4ACDiBeAMSlQMwIE3gKxP+heBcQg1XABGAYrAVDEGQGSAtUgGEFAOhIETK5eVDZAAAAAElFTkSuQmCC';
		if ($("#selectFilterCurahHujan").val() != null) {
			contentKeterangan += "Rata rata curah hujan diambil dari " + newCH.length + " Ombrometer yaitu<br/> ";
			for (var i = 0; i < newCH.length; i++) {
				contentKeterangan += '<li style ="display: inline;">' + '\u2022' + newCH[i] + '</li>';

			}
		}
		var x = 30;
		var xCircle = 5;
		var y = 135;
		var yImage = y;
		pdf.setFontSize(8);
		pdf.text('Note : ', 30, 130);
		pdf.text(text, 40, 130);
		if (adaCurahHujan) {
			pdf.setFontSize(8);
			pdf.text("Rata rata curah hujan diambil dari " + newCH.length + " Ombrometer yaitu", 30, y);
			y = y + 5;
			for (var i = 0; i < newCH.length; i++) {
				yImage = y - 2;
				xCircle = x + 3;
				pdf.setFontSize(8);
				pdf.addImage(imageCircleSmall, 'PNG', x, yImage, 0, 0)
				pdf.text(newCH[i], xCircle, y);
				x = x + 50;
			}
			y = y + 5;
		}
		pdf.setFontSize(8);
		x = 30;
		pdf.text("keterangan jenis rambu", 30, y)
		y = y + 5;
		yImage = y - 2;
		pdf.addImage(imageCircle, 'PNG', x, yImage, 0, 0);
		pdf.text('Daily', x + 3, y);
		x = x + 50;
		yImage = y - 2;
		pdf.text('weekly', x + 3, y);
		pdf.addImage(imageSquare, 'PNG', x, yImage, 0, 0);
		x = x + 50;
		yImage = y - 2;
		pdf.text('random', x + 3, y);
		pdf.addImage(imageCaretUp, 'PNG', x, yImage, 0, 0);
		//pdf.output("datauri"); 
		var pdfName = year + "" + month + "" + date + "_" + "WLR001" + "_" + "LaporanPencatatanCurahHujan" + "_" + selectFilterWmAreaText + "_" + periode;
	} else {
		//alert('kondisi 2');
		var selectFilterWMArea = document.getElementById("selectFilterWMArea");
		var selectFilterWmAreaText = selectFilterWMArea.options[selectFilterWMArea.selectedIndex].text;
		var tglSelected = $("#tglSelected").val();
		var newTgl = tglSelected.split("/");
		var newTglFix = newTgl[2].replace(/ /g, "");
		var periode = +newTgl[0] + "" + newTgl[1] + "" + newTglFix + "" + newTgl[3] + "" + newTgl[4].substring(2, 4);
		var today = new Date();
		var date = today.getDate();
		var month = today.getMonth() + 1;
		var year = today.getFullYear();
		if (date < 10) {
			date = '0' + date;
		}
		if (month < 10) {
			month = '0' + month;
		}
		var canvas = $("#divChart .canvasjs-chart-canvas").get(0);
		var contentKeterangan = "";
		var dataURL = canvas.toDataURL();
		var pdf = new jsPDF("p", "mm", "a3");
		var text = $('#keteranganGrafik').val();
		pdf.addImage(dataURL, 'JPEG', 20, 20, 250, 100);
		var imageCircle = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3VAAAN1QE91ljxAAAAB3RJTUUH4gwFAgwaNyVvAgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAA9SURBVBhXY0ACIkB8BIgnAjEzSAAGtgHxfzRsD8RgHegSMAw2CpsECIPtwCYBwmDLsUmAMBiALEeTYOABAJ55I/naiWqLAAAAAElFTkSuQmCC";
		var imageSquare = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3WAAAN1gGQb3mcAAAAB3RJTUUH4gwFAg0A01ynOQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAsSURBVBhXYwCCV0D8Hwv+DcRYJWAYqyAMYxWEYayCMIxVEIYZfqEJQDHDPwD/KDCxKLNSbAAAAABJRU5ErkJggg==";
		var imageCaretUp = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAHCAYAAADEUlfTAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3WAAAN1gGQb3mcAAAAB3RJTUUH4gwFAg0Nre3bhAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAA7SURBVBhXYyAFcENpDCADxFeBeCmYhwSkgRgk8R+KFwAxGJgC8QMghknA8BIgBhMgXaeR8BkoxgUYGACjDhBX3kKhXQAAAABJRU5ErkJggg==";
		var imageCircleSmall = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA3UAAAN1AHvkboVAAAAB3RJTUUH4gwFAgwaNyVvAgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAuSURBVBhXY4ACDiBeAMSlQMwIE3gKxP+heBcQg1XABGAYrAVDEGQGSAtUgGEFAOhIETK5eVDZAAAAAElFTkSuQmCC';
		if ($("#selectFilterCurahHujan").val() != null) {
			contentKeterangan += "Rata rata curah hujan diambil dari " + $("#selectFilterCurahHujan").val().length + " Ombrometer yaitu<br/> ";
			for (var i = 0; i < newCH.length; i++) {
				contentKeterangan += '<li style ="display: inline;">' + '\u2022' + newCH[i] + '</li>';

			}
		}
		var x = 30;
		var xCircle = 5;
		var y = 135;
		var yImage = y;
		pdf.setFontSize(8);
		if (text != "") {
			pdf.text('Note : ', 30, 130);
			pdf.text(text, 40, 130);
		}
		if (adaCurahHujan) {
			pdf.setFontSize(8);
			pdf.text("Rata rata curah hujan diambil dari " + newCH.length + " Ombrometer yaitu", 30, y);
			y = y + 5;
			for (var i = 0; i < $("#selectFilterCurahHujan").val().length; i++) {
				yImage = y - 2;
				xCircle = x + 3;
				pdf.setFontSize(8);
				pdf.addImage(imageCircleSmall, 'PNG', x, yImage, 0, 0)
				pdf.text(newCH[i], xCircle, y);
				x = x + 50;
			}
			y = y + 5;
		}
		pdf.setFontSize(8);
		x = 30;
		pdf.text("keterangan jenis rambu", 30, y)
		y = y + 5;
		yImage = y - 2;
		pdf.addImage(imageCircle, 'PNG', x, yImage, 0, 0);
		pdf.text('Daily', x + 3, y);
		x = x + 50;
		yImage = y - 2;
		pdf.text('weekly', x + 3, y);
		pdf.addImage(imageSquare, 'PNG', x, yImage, 0, 0);
		x = x + 50;
		yImage = y - 2;
		pdf.text('random', x + 3, y);
		pdf.addImage(imageCaretUp, 'PNG', x, yImage, 0, 0);
		//pdf.output("datauri"); 
		var pdfName = year + "" + month + "" + date + "_" + "WLR001" + "_" + "LaporanPencatatanCurahHujan" + "_" + selectFilterWmAreaText + "_" + periode;
	}
	var string = pdf.output('datauristring');
	pdf.save(pdfName);

}
function GetTmasTarget(idGraph) {
	var selectedZona = [];
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetTmasTargetByIdGraph',
		data: '{idGraph: "' + idGraph + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			console.log('response = ' + response.d);
			var json = JSON.parse(response.d)
			for (var i = 0; i < json.length; i++) {
				console.log('tmasTarget = ' + json[i]["TmasTarget"]);
				console.log('zona = ' + json[i]["idZona"]);
				if (json[i]["TmasTarget"] != 0) {
					$("#tmasTarget").val(json[i]["TmasTarget"]);
				} else {
					$("#tmasTarget").val("");
				}
				selectedZona.push(json[i]["idZona"])
				console.log('idZona = ' + json[i]["idZona"]);
				$('#DropdownFilterZona').selectpicker('val', selectedZona);

			}

		},
		error: function (xhr, ajaxOptions, thrownError) {

		}
	});
}
function getZonebyWmAreaCode() {
	console.log('id wm area getZonebyWmAreaCode = ' + $('#selectFilterWMArea').val());
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetZoneByWmArea',
		data: '{idWmArea: "' + $('#selectFilterWMArea').val() + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var jsonData = $.parseJSON(response.d)
			$('#DropdownFilterZona').find('option').remove();
			$("#DropdownFilterZona").append($('<option>', {
				value: "",
				text: "Tidak Ada Zona"
			}));
			for (var i = 0; i < jsonData.length; i++) {
				$('#DropdownFilterZona').append($('<option>', {
					value: jsonData[i]["WMA_code"],
					text: jsonData[i]["WMA_code"],
				}));
				console.log('wma code = ' + jsonData[i]["WMA_code"]);
			}
			$('.selectpicker').selectpicker('refresh');
		},

		error: function (xhr, ajaxOptions, thrownError) {
			alert("function getGraphByWMArea : " + xhr.statusText);
		}
	});
}
function GetPzoWeekTemp() {
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetPzoWeekTemp',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			console.log('GetPzoWeekTemp = ' + response.d);
			var jsonData = $.parseJSON(response.d)
			$('#SelectFilterWeek').find('option').remove();
			for (var i = 0; i < jsonData.length; i++) {
				var year = jsonData[i]["Year"];
				var monthName = jsonData[i]["MonthName"];
				var weekName = jsonData[i]["WeekName"].substring(0, 2);
				var textWeek = year + ' ' + monthName + ',' + ' ' + weekName;
				var valueWeek = year + '-' + jsonData[i]["Month"] + '-' + jsonData[i]["Week"];

				$('#SelectFilterWeek').append($('<option>', {
					value: valueWeek,
					text: textWeek,
				}));
				console.log('wma code = ' + jsonData[i]["WMA_code"]);
			}
			$('.selectpicker').selectpicker('refresh');
		},

		error: function (xhr, ajaxOptions, thrownError) {
			alert("function getGraphByWMArea : " + xhr.statusText);
		}
	});
}

function DownloadReportPemantauanTMAS() {
	newDownloadGroupReport($("#selectFilterWMArea").val(), $("#selectFilterGraph").val());
}
function newDownloadGroupReport(idWMarea, graphId) {

	console.log('newDownloadGroupReport = ' + idWMarea);
	window.location.href = '../service/WLR_LaporanPemantauanTMAS.ashx?wmarea=' + idWMarea + '&graphId=' + graphId;

}

function LaporanTMATByZona() {
	console.log('Laporan Mingguan Piezometer - Sebaran TMAT Class Zona A,B,C');
	window.location.href = '../service/LaporanTMATByZona.ashx';
}
function LaporanPergerakanTMATByClass() {
	console.log('Laporan Mingguan Piezometer - Sebaran TMAT Class D');
	window.location.href = '../service/LaporanPergerakanTMATByClass.ashx';
}
function LaporanPergerakanTMATByClassKLHK() {
	console.log('Laporan Pergerakan TMAT by Zona (Class A,B,C)  -  Zona XXXX');
	window.location.href = '../service/LaporanPergerakanTMATByClassKLHK.ashx';
}
function LaporanPergerakanTMATKLHK() {
	console.log('Laporan Pergerakan TMAT by Kebun (Class D) - Kebun XXXX');
	window.location.href = '../service/LaporanPergerakanTMATKLHK.ashx';
}
function DownloadReportAnalisaPemantauanTMAS() {
	var graphFix = [];
	if ($("#selectFilterGraph").val() != null) {
		console.log('graph yang dipilih = ' + $("#selectFilterGraph").val());
		var tmpGraph = $("#selectFilterGraph").val();
		for (var i = 0; i < tmpGraph.length; i++) {
			if (tmpGraph[i] != 0) {
				graphFix.push(tmpGraph[i]);
			}
		}
		if (graphFix.length > 0) {
			console.log('week value = ' + $("#SelectFilterWeek").val());
			var weekColl = $("#SelectFilterWeek").val();
			var year = weekColl.split('-')[0];
			var month = weekColl.split('-')[1];
			var week = weekColl.split('-')[2];
			var graph = $("#selectFilterGraph").val();
			var pulau = $("#selectFilterWMArea").val();
			var raCode = $("#selectFilterRambuAir").val();
			var request = new XMLHttpRequest();
			window.location.href = '../service/PZO_AnalisaPerbandinganTMAT.ashx?graph=' + graph + '&month=' + month + '&week=' + week + '&year=' + year;
		} else {
			alert('silahkan pilih graph terlebih dahulu');
		}
		//window.location.href = '../service/WLR_LaporanPemantauanTMAS.ashx?wmarea=' + idWMarea;
	} else {
		alert('silahkan pilih graph terlebih dahulu');
	}


}
function getGraphSelectedForReport() {
	var selectedGraph = [];
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetSelectedGraphForReport',
		data: '{idWmarea: "' + $('#selectFilterWMArea').val() + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			console.log('response = ' + response.d);
			var jsonData = JSON.parse(response.d);
			var graphCollection;

			for (var i = 0; i < jsonData.length; i++) {
				var rambuGraphCollection = jsonData[i]["Graph"].split(',');
				for (var j = 0; j < rambuGraphCollection.length; j++) {
					selectedGraph.push(rambuGraphCollection[j]);
				}
				$('.selectpicker').selectpicker('refresh');
				//$('#selectFilterGraph').selectpicker('val', selectedGraph);
			}



		},
		error: function (xhr, ajaxOptions, thrownError) {

		}
	});
}


