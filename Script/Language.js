﻿var global_language, global_isclick, global_lblCHHarian, global_datears, global_tmattitle, global_tmastitle;
var global_tergenang, global_agaktergenang, global_normal, global_agakkering, global_kering;
var global_adjustmentexplanation1, global_adjustmentexplanation2, global_hari, global_potensibuka;

function GetLanguage() {
	WebserviceGetLanguage();
	global_language = $.parseJSON(localStorage.getItem("lang"));
}

function SetLanguageMainMenu() {
	if (localStorage.getItem("tipelang") == "ENG") {
		$("#spanlanguage").text("English");
		$("#imglanguage").attr("src", "../Image/flag/en.png");
	}
	else {
		$("#spanlanguage").text("Indonesia");
		$("#imglanguage").attr("src", "../Image/flag/in.png");
	}

	$("#lblusername").text(FilterLanguage("IOT01001"));
	$("#lblpassword").text(FilterLanguage("IOT01002"));
	$("#btnLogin").val(FilterLanguage("IOT01003"));
	$("#btncek").text(FilterLanguage("IOT99999"));
	//$("#selectFilterComp").prop('title', FilterLanguage("IOT15001"));
	//$('#selectFilterComp').find('option').remove();

	$("#setLang").text(FilterLanguage("IOT99001"));
		
	//dashboard
	$('#selectFilterComp').attr('title', FilterLanguage("IOT15001"));
	//$('#selectFilterEst').find('option').remove();
	$('#selectFilterEst').attr('title', FilterLanguage("IOT15001"));
	$("#lblCmp").text(FilterLanguage("IOT03002"));
	$("#lblKbn").text(FilterLanguage("IOT03003"));
	$("#lblKetDevice").text(FilterLanguage("IOT15002"));
	$("#lblKet").text(FilterLanguage("IOT15003"));
	$("#k1").text(FilterLanguage("IOT15004"));
	$("#k2").text(FilterLanguage("IOT15005"));
	$("#k3").text(FilterLanguage("IOT15006"));
	$("#lblDailyRpt1").text(FilterLanguage("IOT15007"));
	$("#lblDailyRpt2").text(FilterLanguage("IOT15007"));
	$("#lblDailyRpt3").text(FilterLanguage("IOT15007"));
	$("#lblDailyRpt4").text(FilterLanguage("IOT15007"));
	$("#lblMonthlyRpt1").text(FilterLanguage("IOT15008"));
	$("#lblMonthlyRpt2").text(FilterLanguage("IOT15008"));
	$("#lblMonthlyRpt3").text(FilterLanguage("IOT15008"));
	$("#lblMonthlyRpt4").text(FilterLanguage("IOT15008"));
	
	//$('#selectFilterComp').selectpicker('destroy').selectpicker();
	//$("#selectFilterComp").load(window.location.href + " #selectFilterComp");

	//main menu
	$("#btnSignOut").val(FilterLanguage("IOT02001"));
	$("#lblHome").text(FilterLanguage("IOT02002"));
	$("#lblDashboard").text(FilterLanguage("IOT02003"));
	$("#lblManagementStation").text(FilterLanguage("IOT02004"));
	$("#lblSource").text(FilterLanguage("IOT02005"));
	$("#lblReport").text(FilterLanguage("IOT02006"));
	$("#lblSetting").text(FilterLanguage("IOT02007"));
	$("#lblUnregisteredMertani").text(FilterLanguage("IOT02008"));
	$("#lblUnregisteredMertani2").text(FilterLanguage("IOT02008"));
	$("#lblDataARSMertani").text(FilterLanguage("IOT02009"));
	$("#lblAkurasiMertani").text(FilterLanguage("IOT02010"));
	$("#lblDataTMATMertani").text(FilterLanguage("IOT02011"));
	$("#lblDataTMASMertani").text(FilterLanguage("IOT02012"));
	$("#lblDataTMATHobo").text(FilterLanguage("IOT02013"));
	$("#lblPemantauanWatergate").text(FilterLanguage("IOT02014"));
	$("#lblReportPerkebun").text(FilterLanguage("IOT02015"));

	//menu source ars
	$("#lblDeviceARS").text(FilterLanguage("IOT03001"));
	$("#lblPT").text(FilterLanguage("IOT03002"));
	$("#lblKebun").text(FilterLanguage("IOT03003"));
	$("#lblStationName").text(FilterLanguage("IOT03004"));
	$("#lblDeviceName").text(FilterLanguage("IOT03005"));
	$("#lblDeviceId").text(FilterLanguage("IOT03006"));
	$("#lblStartDate").text(FilterLanguage("IOT03007"));
	$("#lblLastUpdate").text(FilterLanguage("IOT03008"));
	$("#lblBattery").text(FilterLanguage("IOT03009"));
	$("#lblSignal").text(FilterLanguage("IOT03010"));
	$("#lblRemarks").text(FilterLanguage("IOT03011"));
	$("#lblRemarks2").text(FilterLanguage("IOT03011"));
	$("#lblListDeviceARS").text(FilterLanguage("IOT03012"));
	$("#lblStatusDevice").text(FilterLanguage("IOT03013"));
	$("#lblCancel").text(FilterLanguage("IOT03016"));
	$("#lblSave").text(FilterLanguage("IOT03017"));
	$("#lblClearRemarks").text(FilterLanguage("IOT03018"));

	//menu source awl
	$("#lblDeviceAWL").text(FilterLanguage("IOT04001"));
	$("#lblDeviceType").text(FilterLanguage("IOT04002"));
	$("#lblListDeviceAWL").text(FilterLanguage("IOT04003"));

	//menu unregisterd mertani
	$("#lblDeviceUnregistered").text(FilterLanguage("IOT05001"));
	$("#lblListDeviceUnregistered").text(FilterLanguage("IOT05002"));

	//menu report ars data
	$("#lblLaporanArsMertani").text(FilterLanguage("IOT06001"));
	$("#lblTipeData").text(FilterLanguage("IOT06002"));
	$("#lblTanggal").text(FilterLanguage("IOT06003"));
	//$("#pilPT").text(FilterLanguage("IOT06004"));
	//$('#selectFilterPT').find('option').remove();
	$('#selectFilterPT').attr('title', FilterLanguage("IOT06004"));
	$('#selectFilterPT').selectpicker('destroy').selectpicker();
	$('#selectFilterEstate').attr('title', FilterLanguage("IOT06005"));
	$('#selectFilterEstate').selectpicker('destroy').selectpicker();
	$('#selectFilterArsID').attr('title', FilterLanguage("IOT06006"));
	$('#selectFilterArsID').selectpicker('destroy').selectpicker();
	//$('.selectpicker option:selected').remove();


	$("#lblHarian").text(FilterLanguage("IOT06007"));
	$("#lblBulanan").text(FilterLanguage("IOT06008"));
	$("#btn_process").text(FilterLanguage("IOT06009"));
	
	$("#lblLaporanCH").text(FilterLanguage("IOT06010"));
	$("#lblCHHarian").text(FilterLanguage("IOT06011"));
	$("#lblPT2").text(FilterLanguage("IOT03002"));
	$("#lblEst2").text(FilterLanguage("IOT03003"));
	$("#lblStArs").text(FilterLanguage("IOT06015"));
	$("#lblTanggal2").text(FilterLanguage("IOT06003"));
	$("#dataRangkuman").text(FilterLanguage("IOT06012"));

	//Menu Akurasi
	$("#lblAkurasiARSMertani").text(FilterLanguage("IOT07001"));
	$("#btn_all").text(FilterLanguage("IOT07002"));
	$("#tglDt").text(FilterLanguage("IOT06003"));
	$("#startDt").text(FilterLanguage("IOT03007"));
	$("#endDt").text(FilterLanguage("IOT07003"));
	$("#btn_process_accuracy").text(FilterLanguage("IOT06009"));
	$("#lblPerbandingan").text(FilterLanguage("IOT07004"));
	$("#tglTblAcc").text(FilterLanguage("IOT06003"));
	$("#devnameAcc").text(FilterLanguage("IOT03005"));
	$("#lbldiff").text(FilterLanguage("IOT07005"));

	//Menu Report Monthly ARS
	$("#lblMonMonthARS").text(FilterLanguage("IOT08001"));
	$("#lblCoverage").text(FilterLanguage("IOT08002"));
	$("#lblBulan").text(FilterLanguage("IOT08003"));
	$('#selectFilterPlantation').attr('title', FilterLanguage("IOT08004"));
	$('#selectFilterPlantation').selectpicker('destroy').selectpicker();
	$('#selectFilterRegion').attr('title', FilterLanguage("IOT08005"));
	$('#selectFilterRegion').selectpicker('destroy').selectpicker();
	$('#selectFilterGroup').attr('title', FilterLanguage("IOT08006"));
	$('#selectFilterGroup').selectpicker('destroy').selectpicker();
	$('#selectFilterPTAll').attr('title', FilterLanguage("IOT08007"));
	$('#selectFilterPTAll').selectpicker('destroy').selectpicker();
	$('#selectFilterEstateAll').attr('title', FilterLanguage("IOT07002"));
	$('#selectFilterEstateAll').selectpicker('destroy').selectpicker();
	$("#btn_report_monthARS").text(FilterLanguage("IOT08008"));
	$("#lblEstMonArs").text(FilterLanguage("IOT03003"));
	$("#lblDevMonArs").text(FilterLanguage("IOT03005"));
	$("#lblLastUpMonArs").text(FilterLanguage("IOT03008"));

	//Menu Report TMAT
	$("#lblReportTMAT").text(FilterLanguage("IOT09001"));
	$("#lprnTmat").text(FilterLanguage("IOT09001"));
	$("#lprTMAT").text(FilterLanguage("IOT09001"));
	$("#lblDataTypeTMAT").text(FilterLanguage("IOT06002"));
	$("#lblPTTmat").text(FilterLanguage("IOT03002"));
	$("#lblKebunTMat").text(FilterLanguage("IOT03003"));
	$("#lblTanggalTMAT").text(FilterLanguage("IOT06003"));
	
	$("#lblDateTMAT").text(FilterLanguage("IOT06003"));
	$("#lblHarianTMAT").text(FilterLanguage("IOT06007"));
	$("#lblPeriod").text(FilterLanguage("IOT09002"));
	$("#lblStasiunAWL").text(FilterLanguage("IOT09003"));
	$("#lblRenderChart").text(FilterLanguage("IOT09009"));
	$("#lblCustRange").text(FilterLanguage("IOT09010"));

	global_tmattitle = FilterLanguage("IOT09004");
	global_tergenang = FilterLanguage("IOT09005");
	global_agaktergenang = FilterLanguage("IOT09006");
	global_agakkering = FilterLanguage("IOT09007");
	global_kering = FilterLanguage("IOT09008");

	//Menu Monthly Report TMAT
	$("#lblmonitoringTMAT").text(FilterLanguage("IOT08001"));
	$("#lblWil").text(FilterLanguage("IOT08002"));
	$("#lblBulTmat").text(FilterLanguage("IOT08003"));
	$("#btn_process_tmat").text(FilterLanguage("IOT06009"));
	$("#btn_report_tmat").text(FilterLanguage("IOT08008"));
	//$("#lblmonitoringTMAT").text(FilterLanguage("IOT06003"));
	$("#lblEstTMAT").text(FilterLanguage("IOT03003"));
	$("#lblDevTMAT").text(FilterLanguage("IOT03005"));
	$("#lblLastUpTMAT").text(FilterLanguage("IOT03008"));

	//Menu Report TMAS
	$("#lblReportTMAS").text(FilterLanguage("IOT11001"));
	$("#lblLprnTMAS").text(FilterLanguage("IOT11001"));
	$("#lblLprTMAS").text(FilterLanguage("IOT11002"));
	global_tmastitle = FilterLanguage("IOT11003")
	//$("#lblReportTMAS").text(FilterLanguage("IOT11001"));

	//Menu Watergate
	$("#lblPemantauanWatergateMen").text(FilterLanguage("IOT13001"));
	$("#lblSavedGraph").text(FilterLanguage("IOT13002"));
	$("#lblDaterange").text(FilterLanguage("IOT13003"));
	$("#lblSetGraph").text(FilterLanguage("IOT13005"));
	$("#lblDalamPintu").text(FilterLanguage("IOT13006"));
	$("#lblCH").text(FilterLanguage("IOT13010"));
	$("#lblLuarPintu").text(FilterLanguage("IOT13007"));
	$("#lblPotensi").text(FilterLanguage("IOT13008"));
	$("#lblGraph").text(FilterLanguage("IOT13011"));
	$("#btn_save").text(FilterLanguage("IOT13012"));
	$("#btn_delete").text(FilterLanguage("IOT13013"));
	$("#lblDuration").text(FilterLanguage("IOT13014"));
	$("#lblPointData").text(FilterLanguage("IOT13016"));
	$("#closemdpoint").text(FilterLanguage("IOT13018"));
	$("#btnadjust").text(FilterLanguage("IOT13019"));

	$('#selectGraph').attr('title', FilterLanguage("IOT13004"));
	$('#selectGraph').selectpicker('destroy').selectpicker();
	$('#selectCH').attr('title', FilterLanguage("IOT13020"));
	$('#selectCH').selectpicker('destroy').selectpicker();
	$('#selectArsir').attr('title', FilterLanguage("IOT13009"));
	$('#selectArsir').selectpicker('destroy').selectpicker();
	//$("#lblLuarPintu").text(FilterLanguage("IOT13002"));
	global_adjustmentexplanation1 = FilterLanguage("IOT13017").split(',');
	global_hari = FilterLanguage("IOT13015");
	global_potensibuka = FilterLanguage("IOT13008");
	console.log(global_adjustmentexplanation1);

	//Report Kebun
	$("#lblRptKebun").text(FilterLanguage("IOT14001"));
}


function SetLanguageLogin() {
	//Set language
	//console.log($("#btnLogin"))
	//$("#btnLogin").text('ass')

	if (localStorage.getItem("tipelang") == "ENG") {
		$("#spanlanguage").text("English");
		$("#imglanguage").attr("src", "Image/flag/en.png");
	}
	else {
		$("#spanlanguage").text("Indonesia");
		$("#imglanguage").attr("src", "Image/flag/in.png");
	}
	
	$("#lblHeaderUsername").text(FilterLanguage("WLR01001"));
	$("#lblHeaderPassword").text(FilterLanguage("WLR01002"));
	$("#btnLogin").val(FilterLanguage("WLR01005"));
	$("#btncek").text(FilterLanguage("WLR99999"));
	
	$('#selectFilterComp').selectpicker('destroy').selectpicker();
	
}

function FilterLanguage(object) {
	let filter = global_language.filter(d => d.langCODE == localStorage.getItem("tipelang") && d.listCODE == object);
	//console.log(filter);
	if (filter.length > 0) {
		return filter[0]["listDESC"].toString();
	}
	else {
		return "";
	}
}

function LanguageClick(tipe) {
	if (global_language == null) {
		GetLanguage();
	}
	localStorage.setItem("tipelang", tipe);
	SetLanguageLogin();

	$(".dropdown-content").css("display", "none");
	$(".dropdown-content a").css("display", "block");	
}

function LanguageClickMenu(tipe) {
	if (global_language == null) {
		GetLanguage();
	}
	localStorage.setItem("tipelang", tipe);
	SetLanguageMainMenu();
	$('#selectFilterPTAll').selectpicker('refresh');
	//console.log($('#selectFilterPTAll').attr('title'))
	$(".dropdown-content").css("display", "none");
	$(".dropdown-content a").css("display", "block");
}

function ButtonLanguageClick() {
	global_isclick = true;
	if ($(".dropdown-content").css('display').toLowerCase() == 'block') {
		$(".dropdown-content").css("display", "none");
	}
	else {
		$(".dropdown-content").css("display", "block");
	}	
}

function CloseLanguage() {
	if (!global_isclick) {
		$(".dropdown-content").css("display", "none");
	}
	global_isclick = false;
}

//-----------------------------//
//-------- Web Service --------//
//----------------------------//

function WebserviceGetLanguage() {
	let hasil = "";
	$.ajax({
		type: 'POST',
		url: 'Service/MapService.asmx/GetLanguage',
		data: JSON.stringify({			
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		async: false,
		success: function (response) {
			if (response.d.includes("error")) {
				//alert("Terjadi kesalahan GetLanguagexxx");
			}
			else {
				localStorage.setItem("lang", response.d);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$("#loader").hide();
			alert("Maaf terjadi kesalahan GetLanguagexx.");
		}
	});

	return hasil;
}