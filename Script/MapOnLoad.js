﻿// Variable global
var globalprojectid, globalusergroup, globaluserrole, globaluserid, globalusername, globalutmprojection, globalrsid, globalgroupname, globalextent, globalnumzoom,globallegend, globaltableblock;
var globalisfullscreen;
var globallayerDisplayName, globallayerCategory, globallayerURL, globallayerURLName, globallayerDisplay, globallayerTypeName, globallayerfieldID, globaltreename, globaltreestatus, globallayercode, globallayermodule, globallayerstatus, globallayerid;
var map, globallayerfitur, globalinfo, globaljmlvector, globalidxinfo, globalidxblock, globaltreeindex, globaltreeawal;
var tampil, render, contenttable;
var txt, arryaTable, arrayField;
var pointLayer, lineLayer, polygonLayer, boxLayer, polygonQC, drawControls, measureControls;
var globalfirstinfo, globaljmllayer, globalindexlayer, globalcompany, globalestate, globalmappedestate, globaldate, globaltypemodul;
var globallayername, globalmodule, globalkeyid, globallayerid;
var globalresult, globalisfirstload;
var globallayerrotasi, globallayersatu, globallayerselainsatu, globallayerblock, globallayermaxrotasi;
var globalcurrentextent, globalimgurl, globalidweek, globalyear, globalmonth, globalmonthname, globalweek, globalmonthnamealias, globalestatezone;
var globalpzoblocklayer;
var globallistyear, globallistmonth, globallistweek;

$(window).load(function () {

    //Set Variable
    globalisfirstload = true;
    globalisfullscreen = false;
    globalprojectid = $('#projectid').val();
    globalcompany = $('#company').val();
    globalestate = $('#estate').val();
    globalmappedestate = $('#mapped_estate').val();
    globalestatezone = $('#estatezone').val();
    globaldate = $('#date').val();
    globalidweek = $('#idweek').val();
    globalyear = $('#year').val();
    globalmonth = $('#month').val();
    globalmonthname = $('#monthname').val();
    globalweek = $('#week').val();
    globalmonthnamealias = $('#monthnamealias').val();
    globalcurrentextent = $('#currentextent').val();
    globalutmprojection = $('#utmprojection').val();
    globalrsid = $('#rsid').val();
    globalgroupname = "Agro Mandiri Semesta Group";
    globalextent = $('#extent').val();
    globalnumzoom = 25;

    //Get List Year, Month and Week
    globallistyear = $('#listyear').val();
    globallistmonth = $('#listmonth').val();
    globallistweek = $('#listweek').val();

    //Set height map to max window
    $("#map").css({ 'height': (($(window).height()) - 40) + 'px' });
    //Get Today
    var today = new Date();
    var todayto = new Date();
    var dd = today.getDate();
    var ddto = today.getDate();
    var mm = (today.getMonth() + 1);
    var yyyy = today.getFullYear();
    if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = mm + '/' + dd + '/' + yyyy;
    if (ddto < 10) { ddto = '0' + ddto } todayto = mm + '/' + ddto + '/' + yyyy;

    $("#datefrom").val(today);
    $("#dateto").val(todayto);

    $("#inputFilterDate").val(globaldate);
    $("#inputFilterDate").datepicker({
        defaultDate: 0,
        changeMonth: true,
        numberOfMonths: 1
    });

    //Set Date
    $("#datefrom").datepicker({
        defaultDate: 0,
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function (selectedDate) {
            $("#dateto").datepicker("option", "minDate", selectedDate);
        }
    });
    $("#dateto").datepicker({
        defaultDate: 0,
        changeMonth: true,
        numberOfMonths: 1,
        onClose: function (selectedDate) {
            $("#datefrom").datepicker("option", "maxDate", selectedDate);
        }
    });

    //Set Dialog
    $('#dialoginfo').css("visibility", 'visible');
    $("#dialoginfo").dialog({ autoOpen: false, resizable: false, show: "blind", hide: "blind", width: "auto", maxHeight: 400 });
    $('#dialogoverview').css("visibility", 'visible');
    $("#dialogoverview").dialog({ autoOpen: false, resizable: false, show: "blind", hide: "blind", width: "auto" });
    $('#dialogdrawing').css("visibility", 'visible');
    $("#dialogdrawing").dialog({ autoOpen: false, resizable: false, show: "blind", hide: "blind", width: "210" });
    $('#dialogmeasurement').css("visibility", 'visible');
    $("#dialogmeasurement").dialog({ autoOpen: false, resizable: false, show: "blind", hide: "blind", width: "auto" });
    $('#dialoglegend').css("visibility", 'visible');
    $("#dialoglegend").dialog({ autoOpen: false, resizable: false, show: "blind", hide: "blind", maxHeight: 600, width: "250" });
    $('#dialogreport').css("visibility", 'visible');
    $("#dialogreport").dialog({ autoOpen: false, resizable: false, show: "blind", hide: "blind", width: "490", maxHeight: 580 });
    $('#dialogloginhistory').css("visibility", 'visible');
    $("#dialogloginhistory").dialog({ autoOpen: false, resizable: false, modal: true, width: "450", height: "420" });
    $('#dialogchangepassword').css("visibility", 'visible');
    $("#dialogchangepassword").dialog({ autoOpen: false, resizable: false, modal: true, width: "auto" });
    $('#dialogprojectdetail').css("visibility", 'visible');
    $("#dialogprojectdetail").dialog({ autoOpen: false, resizable: false, show: "blind", hide: "blind", width: "auto" });
    $('#dialogharvestdata').css("visibility", 'visible');
    $("#dialogharvestdata").dialog({ autoOpen: false, resizable: false, modal: true, width: "725", height: "auto" });
    $('#dialogharvestingblock').css("visibility", 'visible');
    $("#dialogharvestingblock").dialog({ autoOpen: false, resizable: false, modal: true, width: "150", height: "auto" });

    $('#dialoglaporankerja').css("visibility", 'visible');
    $("#dialoglaporankerja").dialog({ autoOpen: false, resizable: false, width: "auto", height: "480" }).dialogExtend({
        "minimizable": true
    });

    $('#dialogjobid').css("visibility", 'visible');
    $("#dialogjobid").dialog({ autoOpen: false, resizable: false, modal: true, width: "600", height: "480" });

    $('#dialogmapid').css("visibility", 'visible');
    $("#dialogmapid").dialog({ autoOpen: false, resizable: false, modal: true, width: "600", height: "480" });

    $('#dialogcompartment').css("visibility", 'visible');
    $("#dialogcompartment").dialog({ autoOpen: false, resizable: false, width: "850", height: "480" }).dialogExtend({
        "minimizable": true
    });

    $('#dialogwms').css("visibility", 'visible');
    $("#dialogwms").dialog({ autoOpen: false, resizable: false, width: "auto", height: "auto" });

    $('#layercontent').css("visibility", 'visible');
    $("#layercontent").dialog({ autoOpen: false, resizable: false, width: "300", maxHeight: 580, dialogClass: 'noTitleStuff', open: function (event, ui) {
        $(event.target).parent().css('position', 'absolute');
        $(event.target).parent().css('top', 40);
        $(event.target).parent().css('left', $(window).width() - 324);
        $(event.target).parent().css('background', 'rgba(255,255,255,0)');
        $(event.target).parent().css('border', '0 none');
    }
    });

    $('#filtercontent').css("visibility", 'visible');
    $("#filtercontent").dialog({ autoOpen: false, resizable: false, width: "360", maxHeight: 580, dialogClass: 'noTitleStuff', open: function (event, ui) {
        $(event.target).parent().css('position', 'absolute');
        $(event.target).parent().css('top', 40);
        $(event.target).parent().css('left', $(window).width() - 384);
        $(event.target).parent().css('background', 'rgba(255,255,255,0)');
        $(event.target).parent().css('border', '0 none');
    }
    });

    $('#dialogloading').css("visibility", 'visible');
    $("#dialogloading").dialog({ autoOpen: false, resizable: false, width: "166", height: "70", modal: true, dialogClass: 'noTitleStuff', open: function (event, ui) {
        $(event.target).parent().css('background', 'rgba(255,255,255,0)');
        $(event.target).parent().css('border', '0 none');
    }
    });

    // Piezometer
    var dialogExtendOptions = {
        "closable": true,
        "minimizable": true
    };
    $('#dialogsurveypiezometer').css("visibility", 'visible');
    $("#dialogsurveypiezometer").dialog({ autoOpen: false, resizable: false, width: "1100", maxHeight: 580, open: function (event, ui) {
        $(event.target).parent().css('background', 'rgba(255,255,255,0)');
        $(event.target).parent().css('border', '0 none');
    }
    }).dialogExtend(dialogExtendOptions);

    //legend
    $("#imgdialoglegend").attr("src", "../Images/legend/" + globallegend);

    ManipulationComponent();
    ListCompany();
    GetListYear();
    ListLayer(globalmappedestate);
    ListBlockByEstate(globalmappedestate);

    $('#selectFilterCompany').select2();
    $('#selectFilterEstate').select2();
    $('#selectFilterYear').select2();
    $('#selectFilterMonth').select2();
    $('#selectFilterWeek').select2();
});

function GetListYear() {
    var json = $.parseJSON(globallistyear);
    $('#selectFilterYear').find('option').remove();
    for (var i = 0; i < json.length; i++) {
        $("#selectFilterYear").append($('<option>', {
            value: json[i]["Year"],
            text: json[i]["Year"]
        }));
    }

    $("#selectFilterYear").val(globalyear).change();
}

function GetListMonth(year) {
    var json = $.parseJSON(globallistmonth);
    $('#selectFilterMonth').find('option').remove();
    for (var i = 0; i < json.length; i++) {
        if (json[i]["Year"] == year) {
            $("#selectFilterMonth").append($('<option>', {
                value: json[i]["Month"],
                text: json[i]["MonthName"]
            }));
        }
    }

    if ($("#selectFilterYear").val() == globalyear)
        $("#selectFilterMonth").val(globalmonth).change();
    else
        $("#selectFilterMonth").change();
}

function GetListWeek(year, month) {
    var json = $.parseJSON(globallistweek);
    $('#selectFilterWeek').find('option').remove();

    for (var i = 0; i < json.length; i++) {
        if (json[i]["Year"] == year && json[i]["Month"] == month) {
            $("#selectFilterWeek").append($('<option>', {
                //value: json[i]["ID"],
                value: json[i]["Week"],
                text: "Week " + json[i]["Week"]
            }));
        }
    }

    //if ($("#selectFilterWeek option[value='" + globalidweek + "']").length > 0)
    //    $("#selectFilterWeek").val(globalidweek);
    if ($("#selectFilterWeek option[value='" + globalweek + "']").length > 0)
        $("#selectFilterWeek").val(globalweek);
}
