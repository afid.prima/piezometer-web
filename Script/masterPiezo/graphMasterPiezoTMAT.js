﻿var SET_GRAPH_COLOR, allPoint;
var colorsIOTLine = ["blue", "aqua", "navy", "maroon", "fuchsia", "gray", "olive", "teal", "purple"];
function setupChart() {
    getLabelChart();
    setColorGraph();
    setPiezoGraph();
    setIOTGraph();

    max = parseFloat(Math.max(...allPoint) + 5).toFixed(2);
    min = parseFloat(Math.min(...allPoint) - 5).toFixed(2);

    var chart = new CanvasJS.Chart("mDivChart", {
        animationEnabled: true,
        theme: "light2",
        zoomEnabled: true,
        panEnabled: true,
        animationEnabled: true,
        axisX: {
            labelAngle: 45,
            //interval: _interval,
            //intervalType: _intervalType,
            labelFontSize: 12,
            labelFormatter: function (e) {
                return moment(e.value).format('D MMM, YYYY hh:mm:ss');
            }
        },
        axisY: {
            stripLines: SET_GRAPH_COLOR,
            maximum: max,
            minimum: min,
            reversed: true,
        },
        title: {
            //text: textTitle,
            fontWeight: "bolder",
            fontFamily: "Calibri",
            fontSize: 21,
            padding: 10,
            labelMaxWidth: 50
        },
        data: chartData
    });

    chart.render();
}

function setPiezoGraph() {
    chartData = [];
    chartPZO = [];
    allPoint = [];
    for (var i = 0; i < dataMeasurePiezo.length; i++) {
        var nilai = parseFloat(dataMeasurePiezo[i]["Ketinggian"]);
        if (nilai > 75) {
            nilai = 80;
        } else if (nilai < -10) {
            nilai = -10;
        }

        chartPZO.push({
            x: new Date(parseInt(dataMeasurePiezo[i]["tglInput"].replace('/Date(', '').replace(')/', ''))),
            y: nilai,
            label: "Week " + dataMeasurePiezo[i]["Week"] + ' ' + dataMeasurePiezo[i]["monthName"] + " " + dataMeasurePiezo[i]["Year"]
        });
        allPoint.push(nilai);
    }
    var newSeriesPZO = {
        name: dataMeasurePiezo[0]["PieRecordID"],
        legendText: dataMeasurePiezo[0]["PieRecordID"],
        markerType: "circle",
        connectNullData: false,
        //indexLabelLineThickness: 367,
        type: "line",
        color: "black",
        click: function (e) {
            getDetailCalibrate(e.dataPoint.x, e.dataSeries.legendText);
            //mdlCalibrationReference
            $('#mdlCalibrationReference').modal('show');
            //alert(e.dataSeries.name + ", "+measurementId);
            //alert(e.dataSeries.name + ", dataPoint { x:" + e.dataPoint.label + ", y: " + e.dataPoint.y + " }");
        },
        markerSize: 8,
        showInLegend: "true",
        xValueFormatString: "DD-MMM-YYYY hh:mm:ss",
        toolTipContent: "{legendText} , {label}<br/>{x} <strong>, {y} cm</strong>",
        // toolTipContent: _toolTipContent,
        dataPoints: chartPZO,
    };

    chartData.push(newSeriesPZO);
}
function setIOTGraph() {
    for (var x = 0; x < dataMeasureIOT.length; x++) {
        var dataIOT = dataMeasureIOT[x]["data"];
        chartIOT = [];
        for (var a = 0; a < labelChart.length; a++) {
            for (var i = 0; i < dataIOT.length; i++) {
                var dIOT = dateFormat(new Date(parseInt(dataIOT[i]["recordDate"].replace('/Date(', '').replace(')/', ''))))
                if (labelChart[a] == dIOT) {
                    var nilai = parseFloat(dataIOT[i]["valueFinal"]);
                    if (nilai > 75) {
                        nilai = 80;
                    } else if (nilai < -10) {
                        nilai = -10;
                    }

                    chartIOT.push({
                        x: new Date(parseInt(dataIOT[i]["recordDate"].replace('/Date(', '').replace(')/', ''))),
                        y: nilai,
                        label: dataIOT[i]["alias_name"]
                    });
                    allPoint.push(nilai);
                }
            }
        }
        let mil = x % colorsIOTLine.length;
        var newSeriesIOT = {
            name: "IOT " + dataMeasureIOT[x]["type"] + " " + dataIOT[0]["alias_name"],
            legendText: "IOT " + dataMeasureIOT[x]["type"] + " " + dataIOT[0]["alias_name"],
            markerType: "circle",
            connectNullData: false,
            //indexLabelLineThickness: 367,
            type: "line",
            color: colorsIOTLine[mil],
            click: function (e) {
                //getDetailCalibrate(e.dataPoint.x, e.dataSeries.legendText);
                ////mdlCalibrationReference
                //$('#mdlCalibrationReference').modal('show');
                //alert(e.dataSeries.name + ", " + e.dataSeries.);
                //alert(e.dataSeries.name + ", dataPoint { x:" + e.dataPoint.label + ", y: " + e.dataPoint.y + " }");
            },
            markerSize: 5,
            showInLegend: "true",
            xValueFormatString: "DD-MMM-YYYY hh:mm:ss",
            toolTipContent: "{legendText} ,<br/>{x} <strong>, {y} cm</strong>",
            // toolTipContent: _toolTipContent,
            dataPoints: chartIOT,
        };

        chartData.push(newSeriesIOT);
    }
}

function getDetailCalibrate(dateParam, namaPerangkat) {
    $.ajax({
        type: 'POST',
        url: '../Service/WebServiceMasterPiezo.asmx/getDetailCalibrate',
        data: '{piezoId: "' + $("#midPzo").val() + '",dateParam: "' + dateFormat3(dateParam) + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var jsonTable0 = $.parseJSON(response.d[0]);
            var jsonTable1 = $.parseJSON(response.d[1]);

            $("#midPzo1").text($("#midPzo").val());
            $("#midEst1").text(jsonTable1[0].estName);
            $("#midBlockPemantauan1").text($("#midBlockPemantauan").val());
            $("#midJenis1").text($("#midJenis").val());
            $("#midStatus1").text($("#midStatus").val());
            $("#midTgl1").text(jsonTable0[0].Column4);
            $("#midTmat1").text(jsonTable0[0].Column1);
            $("#midIdperangkat1").text(jsonTable0[0].Column5);

            var contentt = "";
            if (jsonTable0[0].urlImages == null) {
                contentt += '';
            } else {
                var sUrlImages = jsonTable0[0].urlImages.split(',');
                contentt += '';
                for (var j = 0; j < sUrlImages.length; j++) {
                    contentt += '<img src="' + sUrlImages[j] + '" alt="Description of Image" width="200" height="200">';
                }
            }

            $("#img").html(contentt);
            $("#midStation1").text(namaPerangkat);
            //$("#midSelisih1").text((parseInt(jsonTable0[0].Column1) - jsonTable1[0].valueFinal + parseInt(jsonTable1[0].calibrasi.toFixed(2))));
            $("#midCalibrate1").text($("#midPzo").val());



            var content = "";
            content += '<table id="tblKalibrasiIOTPZO" class="table table-striped table-bordered" style="width:100%">';
            content += '<thead><tr><th style="width:10px; text-align: center;">Tgl/Waktu</th><th style="text-align: center;">Rekod</th><th style="text-align: center;">Kalibrasi</th><th style="text-align: center;">TMAT,cm</th></tr></thead>';
            content += '<tbody>';
            for (var i = 0; i < jsonTable1.length; i++) {
                content += '<tr><td style="text-align: center;" id="tglIOT">' + jsonTable1[i].tanggalah + '</td>'
                content += '<td style = "text-align: center;" > ' + jsonTable1[i].value + '</td>';
                for (var s = 0; s < dataTblIOT.length; s++) {
                    let tanggalA = new Date(jsonTable1[i].tanggal2);
                    let tanggalB = new Date(dataTblIOT[s].dateParam);
                    let tanggalC = new Date(dataTblIOT[s].dateParam2);
                    if (tanggalA >= tanggalB && tanggalA <= tanggalC) {
                        content += '<td style="text-align: center;">' + parseFloat(dataTblIOT[s].calibrasi).toFixed(2) + '</td>';
                        content += '<td style="text-align: center;">' + parseInt(parseFloat(parseFloat(jsonTable1[i].value).toFixed(2)) - parseFloat(parseFloat(dataTblIOT[s].calibrasi).toFixed(2))) + '</td></tr>';
                        $("#midSelisih1").text(parseFloat(parseFloat(parseFloat(jsonTable1[i].value).toFixed(2)) - parseFloat(jsonTable0[0].Column1)).toFixed(2));
                    }
                }
                
            }
            content += '</tbody></table>';
            $("#tblCalibrate").html(content);

            $('#tblKalibrasiIOTPZO').DataTable({
                dom: 'frtip',
                "paging": false, // If you want to hide the pagination as well
                "info": false,   // Hides the "showing entries" info
                "searching": false, // Hides the search box
                "ordering": false, // Hides the search box
                columnDefs: [
                    {
                        target: 0,
                        visible: true,
                        searchable: false,
                        orderable: false,
                        width: '80%'
                    },
                    {
                        target: 1,
                        visible: true,
                        searchable: false,
                        orderable: false,
                    },
                    {
                        target: 2,
                        visible: true,
                        searchable: false,
                        orderable: false,
                    },
                    {
                        target: 3,
                        visible: true,
                        searchable: false,
                        orderable: false,
                    }
                ]
            });
            //setupChart();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function getPiezoMeasure : " + xhr.statusText);
        }
    });
}

function toDateFormat(epochTimestamp) {
    return `/Date(${epochTimestamp})/`;
}

function pad(number) {
    return (number < 10 ? '0' : '') + number;
}
function dateFormat99(tanggal) {

    // Pisahkan bagian tanggal dan waktu
    let [datePart, timePart] = tanggal.split(" ");

    // Pisahkan komponen tanggal
    let dateParam = datePart.replace(new RegExp(':', 'g'), '/');

    return dateParam
}

function setNewCalibrate() {
    var data = [];

    let now = new Date();
    let epochTimestamp = now.getTime();
    let formattedDate = toDateFormat(epochTimestamp);
    let dateString = "12/31/9999";
    let [day, month, year] = dateString.split("/");
    // Buat objek Date dari komponen-komponen tersebut
    let date = new Date(year, month - 1, day);

    // Pisahkan bagian tanggal dan waktu
    let [datePart, timePart] = $("#tglIOT").text().split(" ");

    // Pisahkan komponen tanggal
    let [days, months, years] = datePart.split(":");

    // Pisahkan komponen waktu (meskipun tidak diperlukan dalam format akhir)
    let [hours, minutes, seconds] = timePart.split(":");

    // Buat objek Date dari komponen-komponen tersebut
    let dateParam = new Date(years, months - 1, days, hours, minutes, seconds);

    // Ekstrak dan format tanggal dalam format DD/MM/YYYY
    let formattedDateParam = `${pad(dateParam.getMonth() + 1)}/${pad(dateParam.getDate())}/${dateParam.getFullYear()}`;


    // Pisahkan komponen tanggal
    let [monthYesterday, dayYesterday, yearYesterday] = formattedDateParam.split("/");

    // Buat objek Date dari komponen-komponen tersebut
    let yesterday = new Date(yearYesterday, monthYesterday - 1, dayYesterday)

    // Kurangi satu hari dari tanggal
    yesterday.setDate(yesterday.getDate() - 1);

    // Ekstrak dan format tanggal kemarin dalam format DD/MM/YYYY
    let yesterdays = `${pad(yesterday.getMonth() + 1)}/${pad(yesterday.getDate())}/${yesterday.getFullYear()}`;


    //// Buat objek Date (bulan dikurangi 1 karena bulan dalam JavaScript dimulai dari 0)
    //let date = new Date(year, month - 1, day);
    //let dateYesterday = new Date(yesterday.setDate(yesterday.getDate() - 1))


    //// Dapatkan komponen tanggal
    //let days = pad(dateYesterday.getDate());
    //let months = pad(dateYesterday.getMonth() + 1); // Bulan di JavaScript dimulai dari 0
    //let years = dateYesterday.getFullYear();

    //let formattedYesterday = `${days}/${months}/${years}`;

    $("#end" + (idxTableIOT - 1)).val(yesterdays);
    // Dapatkan epoch timestamp
    let epochTimestampEnd = date.getTime();

    let epochTimestampStart = dateParam.getTime();
    //data.push({
    //    PieRecordID: $("#midPzo1").text(),
    //    calibrasi: $("#midSelisih1").text(),
    //    createBy: $("#userid").val(),
    //    createDate: formattedDate,
    //    deviceId: $("#midIdperangkat1").text(),
    //    endDate: toDateFormat(epochTimestampEnd),
    //    estCode: $("#midEst1").text().split("/")[1],
    //    startDate: toDateFormat(epochTimestampStart),
    //    type: $("#jenisIOT" + (idxTableIOT-1)).val()
    //});
    // Mengurutkan secara ascending berdasarkan startDate
    //dataTblIOT.sort((a, b) => new Date(a.dateParam) - new Date(b.dateParam));
    //dataTblIOT.forEach(item => {
    //    if (item.dateParam2 === "12/31/9999") {
    //        item.dateParam2 = yesterdays; // Update age where id is 2
    //        item.endIOTDate = yesterdays; // Update age where id is 2
    //    }
    //});
    dataTblIOT.push({
        type: $("#jenisIOT" + (idxTableIOT - 1)).val(),
        deviceId: $("#midIdperangkat1").text(),
        piezoId: $("#midPzo").val(),
        dateParam: formattedDateParam,
        dateParam2: dateString,
        startDate: toDateFormat(epochTimestampStart),
        endDate: toDateFormat(epochTimestampEnd),
        startIOTDate: dateParam,
        endIOTDate: dateString,
        calibrasi: $("#midSelisih1").text()
    });
    dataTblIOT.sort((a, b) => new Date(a.dateParam) - new Date(b.dateParam));
    let newValues;
    for (var s = 0; s < dataTblIOT.length; s++) {
        //if (dataTblIOT[s].dateParam2 === "12/31/9999") {

        if ((s + 1) === dataTblIOT.length) {
            newValues = { startDate: toDateFormat(new Date(dataTblIOT[s].dateParam).getTime()), endDate: toDateFormat(new Date(dateString).getTime()), dateParam2: dateString, endIOTDate: new Date(dateString)};
        }
        else {
            newValues = { startDate: toDateFormat(new Date(dataTblIOT[s].dateParam).getTime()), endDate: (new Date(dataTblIOT[s].dateParam).getTime() === new Date(dataTblIOT[s + 1].dateParam).getTime() ? toDateFormat(new Date(dataTblIOT[s].dateParam).getTime()) : toDateFormat(new Date(getYesterday(dataTblIOT[s + 1].dateParam)).getTime())), dateParam2: (new Date(dataTblIOT[s].dateParam).getTime() === new Date(dataTblIOT[s + 1].dateParam).getTime() ? dataTblIOT[s + 1].dateParam : getYesterday(dataTblIOT[s + 1].dateParam)), endIOTDate: (new Date(dataTblIOT[s].dateParam).getTime() === new Date(dataTblIOT[s + 1].dateParam).getTime() ? new Date(dataTblIOT[s + 1].dateParam) : new Date(getYesterday(dataTblIOT[s + 1].dateParam)))};
        }
        for (let key in newValues) {
            dataTblIOT[s][key] = newValues[key];
        }
        //}
    }
    tableIOT.row.add($(addRowDataIOT())[0]).clear().draw(false);
    for (var s = 0; s < dataTblIOT.length; s++) {
        addTblRowDataIOT(dataTblIOT[s],2);
    }
    //alert(data);
    $('#mdlCalibrationReference').modal('hide');
}

function getYesterday(date) {

    // Pisahkan komponen tanggal
    let [monthYesterday, dayYesterday, yearYesterday] = date.split("/");

    // Buat objek Date dari komponen-komponen tersebut
    let yesterday = new Date(yearYesterday, monthYesterday - 1, dayYesterday)

    // Kurangi satu hari dari tanggal
    yesterday.setDate(yesterday.getDate() - 1);

    // Ekstrak dan format tanggal kemarin dalam format DD/MM/YYYY
    let yesterdays = `${pad(yesterday.getMonth() + 1)}/${pad(yesterday.getDate())}/${yesterday.getFullYear()}`;
    return yesterdays
}

function getYesterday2(date) {

    // Pisahkan komponen tanggal
    let [dayYesterday, monthYesterday, yearYesterday] = date.split("/");

    // Buat objek Date dari komponen-komponen tersebut
    let yesterday = new Date(yearYesterday, monthYesterday - 1, dayYesterday)

    // Kurangi satu hari dari tanggal
    yesterday.setDate(yesterday.getDate() - 1);

    // Ekstrak dan format tanggal kemarin dalam format DD/MM/YYYY
    let yesterdays = `${pad(yesterday.getMonth() + 1)}/${pad(yesterday.getDate())}/${yesterday.getFullYear()}`;
    return yesterdays
}

function setColorGraph() {
    SET_GRAPH_COLOR = [];
    if (mWaterDeepindicator.length > 0) {
        for (var s = 0; s < mWaterDeepindicator.length; s++) {
            if (mWaterDeepindicator[s].MaxValue == -1) {
                SET_GRAPH_COLOR.push({
                    startValue: mWaterDeepindicator[s].MinValue,
                    endValue: mWaterDeepindicator[s].MaxValue + 1,
                    color: mWaterDeepindicator[s].colorBack,
                    //opacity: .6,
                    //label: `${mWaterDeepindicator[s].IndicatorName} ${mWaterDeepindicator[s].IndicatorAlias}`,
                    labelFontColor: mWaterDeepindicator[s].colorFont
                });
            } else if (mWaterDeepindicator[s].MinValue == 41) {
                SET_GRAPH_COLOR.push({
                    startValue: mWaterDeepindicator[s].MinValue - 1,
                    endValue: mWaterDeepindicator[s].MaxValue,
                    color: mWaterDeepindicator[s].colorBack,
                    //opacity: .6,
                    //label: `${mWaterDeepindicator[s].IndicatorName} ${mWaterDeepindicator[s].IndicatorAlias}`,
                    labelFontColor: mWaterDeepindicator[s].colorFont
                });
            } else if (mWaterDeepindicator[s].MinValue == 46) {
                SET_GRAPH_COLOR.push({
                    startValue: mWaterDeepindicator[s].MinValue - 1,
                    endValue: mWaterDeepindicator[s].MaxValue,
                    color: mWaterDeepindicator[s].colorBack,
                    //opacity: .6,
                    //label: `${mWaterDeepindicator[s].IndicatorName} ${mWaterDeepindicator[s].IndicatorAlias}`,
                    labelFontColor: mWaterDeepindicator[s].colorFont
                });
            } else if (mWaterDeepindicator[s].MinValue == 61) {
                SET_GRAPH_COLOR.push({
                    startValue: mWaterDeepindicator[s].MinValue - 1,
                    endValue: mWaterDeepindicator[s].MaxValue,
                    color: mWaterDeepindicator[s].colorBack,
                    //opacity: .6,
                    //label: `${mWaterDeepindicator[s].IndicatorName} ${mWaterDeepindicator[s].IndicatorAlias}`,
                    labelFontColor: mWaterDeepindicator[s].colorFont
                });
            } else if (mWaterDeepindicator[s].MinValue == 66) {
                SET_GRAPH_COLOR.push({
                    startValue: mWaterDeepindicator[s].MinValue - 1,
                    endValue: mWaterDeepindicator[s].MaxValue,
                    color: mWaterDeepindicator[s].colorBack,
                    //opacity: .6,
                    //label: `${mWaterDeepindicator[s].IndicatorName} ${mWaterDeepindicator[s].IndicatorAlias}`,
                    labelFontColor: mWaterDeepindicator[s].colorFont
                });
            } else if (mWaterDeepindicator[s].MinValue == 999) {
                SET_GRAPH_COLOR.push({
                    startValue: mWaterDeepindicator[s].MinValue - 1,
                    endValue: mWaterDeepindicator[s].MaxValue,
                    color: mWaterDeepindicator[s].colorBack,
                    //opacity: .6,
                    //label: `${mWaterDeepindicator[s].IndicatorName} ${mWaterDeepindicator[s].IndicatorAlias}`,
                    labelFontColor: mWaterDeepindicator[s].colorFont
                });
            } else {
                SET_GRAPH_COLOR.push({
                    startValue: mWaterDeepindicator[s].MinValue,
                    endValue: mWaterDeepindicator[s].MaxValue,
                    color: mWaterDeepindicator[s].colorBack,
                    //opacity: .6,
                    //label: `${mWaterDeepindicator[s].IndicatorName} ${mWaterDeepindicator[s].IndicatorAlias}`,
                    labelFontColor: mWaterDeepindicator[s].colorFont
                });
            }


            //console.log('rambu curah hujan = ' + chSelect);
        }

    }
}

function getLabelChart() {
    var split = $('#mTglSelected').val().split('-');

    var sd = split[0].trim().split("/");
    var ed = split[1].trim().split("/");

    mStartDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
    mEndtDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

    var startDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
    var endtDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

    labelChart = [];
    for (var now = startDate; now <= endtDate; now.setDate(now.getDate() + 1)) {
        labelChart.push(dateFormat(now));
    }
}

function GetWaterDepthIndicator() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/getWaterDepthIndicator',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            mWaterDeepindicator = $.parseJSON(response.d);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function GetWaterDepthIndicator : " + xhr.statusText);
        }
    });
}

function getPiezoMeasure() {
    getLabelChart();
    $.ajax({
        type: 'POST',
        url: '../Service/WebServiceMasterPiezo.asmx/getPiezoMeasure',
        data: '{piezoId: "' + $("#midPzo").val() + '",dateParam: "' + dateFormat2(mStartDate) + '",dateParam2: "' + dateFormat2(mEndtDate) + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            dataMeasurePiezo = $.parseJSON(response.d);
            setupChart();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function getPiezoMeasure : " + xhr.statusText);
        }
    });
}

function getIOTPiezo(idx) {
    if (idx == 0) {
        $(".loading").show();
        dataMeasureIOT = [];
    }
    $.ajax({
        type: 'POST',
        url: '../Service/WebServiceMasterPiezo.asmx/getIOTPiezo',
        data: JSON.stringify(dataTblIOT[idx]),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var data = $.parseJSON(response.d);
            //dataMeasureIOT.push(...data);
            //dataMeasureIOT = _.uniq(dataMeasureIOT, function (item) {
            //    return item.recordDate;
            //});

            var objIOT = dataTblIOT[idx];
            objIOT["data"] = data;
            dataMeasureIOT.push(objIOT);
            idx++;

            if (idx < dataTblIOT.length) {
                getIOTPiezo(idx);
            } else {
                $(".loading").hide();
                setupChart();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function getIOTPiezo : " + xhr.statusText);
            $(".loading").hide();
        }
    });
}