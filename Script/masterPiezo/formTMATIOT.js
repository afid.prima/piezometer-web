﻿function addRowDataIOT() {
    var content = '<tr>';
    content += '<td style="width:2%;">' + idxTableIOT + '</td>';
    content += '<td style="width:2%;"><select id="jenisIOT' + idxTableIOT + '" class="selectpicker" data-live-search="true" >';
    content += '<option value="0">Pilih Type</option>';
    content += '<option value="mertani">Mertani</option>';
    content += '<option value="holykell">HolyKell</option>';
    content += '</select></td>';
    content += '<td style="width:10%;"><select id="deviceIOT' + idxTableIOT + '" class="form-control selectpicker" data-live-search="true" style="width:auto"></select></td>';
    content += '<td style="width:10%;"><input type="text" class="table-input form-control pull-right" id="start' + idxTableIOT + '"></td>';
    content += '<td style="width:10%;"><input type="text" class="form-control pull-right" id="end' + idxTableIOT + '"></td>';
    content += '<td style="width:10%;"><input type="number" class="form-control pull-right" id="kalibrasi' + idxTableIOT + '" value="0"></td>';
    content += '<td style="width:2%;">';
    content += '<a href="#" class="remove pull-left"><i class="fa fa-fw fa-trash"></i>Hapus</a>';
    content += '<a href="#" class="recalculate pull-right"><i class="fa fa-fw fa-rotate-left"></i>Recalculate</a>';
    content += '</td> ';
    content += '</tr>';
    return content;
}

function addTblRowDataIOT(data,flag) {
    tableIOT.row.add($(addRowDataIOT())[0]).draw(false);
    
    //for (var i = 0; i < masterTypeIOT; i++) {
    //    $("#jenisIOT" + idxTableIOT).append($('<option>', {
    //        value: masterTypeIOT[i]["value"],
    //        text: masterTypeIOT[i]["text"]
    //    }));
    //}


    $('#start' + idxTableIOT).datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    })


    $('#end' + idxTableIOT).datepicker({
        format: "dd/mm/yyyy",
        autoclose: true
    })

    $("#jenisIOT" + idxTableIOT).change(function () {
        var idJenis = this.id;
        var idDevice = this.id.replace("jenisIOT", "");

        $("#deviceIOT" + idDevice).find('option').remove();
        for (var x = 0; x < masterIOT.length; x++) {
            if (masterIOT[x]["type"] == $('#' + idJenis).val()) {
                $("#deviceIOT" + idDevice).append($('<option>', {
                    value: masterIOT[x]["device_id"],
                    text: masterIOT[x]["alias_name"]
                }));
            }
        }
        $('.selectpicker').selectpicker('refresh');
    });

    if (data.hasOwnProperty("type")) {
        var startDateX = new Date(parseInt(data["startDate"].replace('/Date(', '').replace(')/', '')));
        var endDateX = new Date(parseInt(data["endDate"].replace('/Date(', '').replace(')/', '')));

        $("#jenisIOT" + idxTableIOT).val(data["type"]);

        $("#deviceIOT" + idxTableIOT).find('option').remove();
        for (var x = 0; x < masterIOT.length; x++) {
            if (masterIOT[x]["type"] == data["type"]) {
                $("#deviceIOT" + idxTableIOT).append($('<option>', {
                    value: masterIOT[x]["device_id"],
                    text: masterIOT[x]["alias_name"]
                }));
            }
        }

        $("#deviceIOT" + idxTableIOT).val(data["deviceId"]);
        $("#start" + idxTableIOT).val(dateFormat(startDateX));
        $("#end" + idxTableIOT).val(dateFormat(endDateX)); 
        $("#kalibrasi" + idxTableIOT).val(data["calibrasi"]);

        var startIOT = $("#start" + idxTableIOT).val();
        var endIOT = $("#end" + idxTableIOT).val();

        var sd = startIOT.trim().split("/");
        var ed = endIOT.trim().split("/");

        var startIOTDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
        var endIOTDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

        if (startIOTDate > endIOTDate) {
            alert("Start date tidak bisa lebih besar dari end date");
            error = true;
            return;
        }


        var tanggal = new Date(convertToDate(startCallibrate));
        //if (startIOTDate < tanggal) {
        //    alert("Start date tidak bisa lebih kecil dari tanggal kalibrasi pertama (" + convertToDateReturn(startCallibrate) + ")");
        //    error = true;
        //    return;
        //}

        if (flag == 1) {
            dataTblIOT.push({
                type: $("#jenisIOT" + idxTableIOT).val(),
                deviceId: $("#deviceIOT" + idxTableIOT).val(),
                piezoId: $("#midPzo").val(),
                startDate: toDateFormat(startIOTDate.getTime()),
                endDate: toDateFormat(endIOTDate.getTime()),
                dateParam: dateFormat2(startIOTDate),
                dateParam2: dateFormat2(endIOTDate),
                startIOTDate: startIOTDate,
                endIOTDate: endIOTDate,
                calibrasi: $("#kalibrasi" + idxTableIOT).val()
            })
        }
        
    }

    $('.selectpicker').selectpicker('refresh');
    idxTableIOT++;

}

function convertToDateReturn(dateStr) {
    // Extract the timestamp from the date string
    let timestamp = parseInt(dateStr.replace(/\/Date\((\d+)\)\//, '$1'), 10);

    // Create a Date object from the timestamp
    let date = new Date(timestamp);

    // Format the date as "MM/DD/YYYY"
    let month = (date.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-indexed
    let day = date.getDate().toString().padStart(2, '0');
    let year = date.getFullYear();

    return `${month}/${day}/${year}`;
}

function convertToDate(dateStr) {
    // Extract the timestamp from the date string
    let timestamp = parseInt(dateStr.replace(/\/Date\((\d+)\)\//, '$1'), 10);

    // Create a Date object from the timestamp
    let date = new Date(timestamp);

    // Define arrays for days of the week and months
    const daysOfWeek = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    // Get components of the date
    let dayOfWeek = daysOfWeek[date.getDay()];
    let month = months[date.getMonth()];
    let day = date.getDate().toString().padStart(2, '0');
    let year = date.getFullYear();
    let hours = date.getHours().toString().padStart(2, '0');
    let minutes = date.getMinutes().toString().padStart(2, '0');
    let seconds = date.getSeconds().toString().padStart(2, '0');
    let timezoneOffset = -date.getTimezoneOffset();
    let timezoneSign = timezoneOffset >= 0 ? '+' : '-';
    let timezoneHours = Math.floor(Math.abs(timezoneOffset) / 60).toString().padStart(2, '0');
    let timezoneMinutes = (Math.abs(timezoneOffset) % 60).toString().padStart(2, '0');
    let timezone = `GMT${timezoneSign}${timezoneHours}${timezoneMinutes}`;

    // Get timezone name
    let timezoneName = date.toString().match(/\(([^)]+)\)$/)[1];

    // Format the date as "Thu Feb 02 2023 00:00:00 GMT+0700 (Western Indonesia Time)"
    return `${dayOfWeek} ${month} ${day} ${year} ${hours}:${minutes}:${seconds} ${timezone} (${timezoneName})`;
}

function recalculateIOT(isSave) {
    dataTblIOT = [];
    dataMeasureIOT = [];
    var error = false;
    tableIOT.rows().every(function () {
        var rowData = this.data();
        var idx = rowData[0];

        if ($("#jenisIOT" + idx).val() == "0") {
            alert("Mohon Pilih Jenis IOT");
            error = true;
            return;
        }

        if ($("#kalibrasi" + idx).val() == "") {
            alert("Mohon isi kalibrasi");
            error = true;
            return;
        }

        if ($("#start" + idx).val() == "") {
            alert("Mohon Tentukan Start Date");
            error = true;
            return;
        }

        if ($("#end" + idx).val() == "") {
            alert("Mohon Tentukan End Date");
            error = true;
            return;
        }

        var startIOT = $("#start" + idx).val();
        var endIOT = $("#end" + idx).val();

        var sd = startIOT.trim().split("/");
        var ed = endIOT.trim().split("/");

        var startIOTDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
        var endIOTDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

        if (startIOTDate > endIOTDate) {
            alert("Start date tidak bisa lebih besar dari end date");
            error = true;
            return;
        }

        //var tanggal = new Date(convertToDate(startCallibrate));
        //if (startIOTDate < tanggal) {
        //    alert("Start date tidak bisa lebih kecil dari tanggal kalibrasi pertama ( " + convertToDateReturn(startCallibrate) + " )");
        //    error = true;
        //    return;
        //}

        dataTblIOT.push({
            type: $("#jenisIOT" + idx).val(),
            deviceId: $("#deviceIOT" + idx).val(),
            piezoId: $("#midPzo").val(),
            dateParam: dateFormat2(startIOTDate),
            dateParam2: dateFormat2(endIOTDate),
            startIOTDate: startIOTDate,
            endIOTDate: endIOTDate,
            calibrasi: $("#kalibrasi" + idx).val()
        })
    });

    if (error) {
        return;
    }

    dataTblIOT.forEach(function (item) {
        item.startIOTDate = new Date(item.startIOTDate);
        item.endIOTDate = new Date(item.endIOTDate);
    });

    dataTblIOT = _.sortBy(dataTblIOT, 'startIOTDate');

    var overlappingRanges = [];
    if (isSave) {
        overlappingRanges = findOverlappingRanges(dataTblIOT);
    } 

    console.log(overlappingRanges);
    // Output the overlapping ranges in a more readable format
    overlappingRanges.forEach(function (range) {
        console.log(`Device ${range.deviceId1} (${range.start1} - ${range.end1}) overlaps with Device ${range.deviceId2} (${range.start2} - ${range.end2})`);
    });

    if (overlappingRanges.length == 0) {
        if (!isSave) {
            getIOTPiezo(0);
        } else {
            savePZOIOT();
        }
    } else {
        alert("Star dan End ada yang overlaping range");
    }
}

function findOverlappingRanges(data) {
    var overlappingRanges = [];

    for (var i = 0; i < data.length - 1; i++) {
        var current = data[i];
        var next = data[i + 1];

        if (current.dateParam2 >= next.dateParam) {
            overlappingRanges.push({
                deviceId1: current.deviceId,
                deviceId2: next.deviceId,
                start1: current.dateParam,
                end1: current.dateParam2,
                start2: next.dateParam,
                end2: next.dateParam2
            });
        }
    }

    return overlappingRanges;
}

function getPZOIOTStation() {
    $.ajax({
        type: 'POST',
        url: '../Service/WebServiceMasterPiezo.asmx/getPZOIOTStation',
        data: '{piezoId: "' + $("#midPzo").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var data = $.parseJSON(response.d);
            if (data.length > 0) {
                startCallibrate = data[0].CalibrationDate;
            }
            
            dataTblIOT = [];
            idxTableIOT = 0;
            tableIOT.clear().draw();
            for (var i = 0; i < data.length; i++) {
                addTblRowDataIOT(data[i],1);
            }

            if (dataTblIOT.length > 0) {
                getIOTPiezo(0);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function getPZOIOTStation : " + xhr.statusText);
        }
    });
}

function savePZOIOT() {
    if (dataTblIOT.length == 0) {
        alert("Tidak ada data IOT yang dipilih");
        return;
    }
    var dataSend = {
        userId: $("#userid").val(),
        estCode: $("#selectFilteringEstate").val(),
        data: dataTblIOT
    }
    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/WebServiceMasterPiezo.asmx/savePZOIOT',
        data: '{data: ' + JSON.stringify(JSON.stringify(dataSend)) + '}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            $(".loading").hide();
            console.log("savePZOIOT", $.parseJSON(response.d));
            var data = $.parseJSON(response.d);

            if (data.length > 0) {
                var a = "";
                for (var i = 0; i < data.length; i++) {
                    //if (a == "") {
                        a += "iot " + data[i]["alias_name"] + " sudah terkoneksi dengan piezoid " + data[i]["PieRecordID"] + " pada tanggal " + dateFormat(new Date(parseInt(data[i]["startDate"].replace('/Date(', '').replace(')/', '')))) + " sampai " + dateFormat(new Date(parseInt(data[i]["endDate"].replace('/Date(', '').replace(')/', '')))) + " \n"
                    //} else {
                    //    a += "\niot " + data[i]["alias_name"] + " sudah terkoneksi dengan piezoid " + data[i]["PieRecordID"] + " pada tanggal " + dateFormat(new Date(parseInt(data[i]["startDate"].replace('/Date(', '').replace(')/', '')))) + " sampai " + dateFormat(new Date(parseInt(data[i]["endDate"].replace('/Date(', '').replace(')/', ''))))
                    //}
                }
                alert(a);
            } else {
                $('#mdlCheckDataIOT').modal('hide');

                ListHistoryDAftarPizometer();
                ListHistorySummaryPizometer();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $(".loading").hide();
            console.log("function savePZOIOT : " + xhr.statusText);
        }
    });
}

function getAllIotTMATByEstate() {
    $.ajax({
        type: 'POST',
        url: '../Service/WebServiceMasterPiezo.asmx/getAllIotTMATByEstate',
        data: '{estCode: "' + $("#selectFilteringEstate").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            masterIOT = $.parseJSON(response.d);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function getAllIotTMATByEstate : " + xhr.statusText);
        }
    });
}