﻿//Map Basic Variable Declaration
var map, latLong, centerCoordinate, zoomLevel, mapProjection;

//Map Control Declaration
var scaleLineControl, mousePositionControl, overviewMapControl, zoomSliderControl;

//Map Layer Declaration
var layerBase, layerBlock, layerPiezometer, layerInfo, layerDrawing, sourceLayerDrawing, layerMeasure, sourceLayerMeasure, layerHighlight;

var listyear, listmonth, listweek, listallwmarea;
var year, month, monthname, week, monthnamealias, idweek;

//Variable for Event Handler
var isfullscreen;

//Variable for Toolbox
var draw;
var extentCoordinate;
var sketch, helpTooltipElement, helpTooltip, measureTooltipElement, measureTooltip, continuePolygonMsg = 'Click to continue drawing the polygon',
	continueLineMsg = 'Click to continue drawing the line', pointerMoveHandler, formatLength, formatArea, listener;
var isCheckZonaTrue;

//Variable For Piezometer
var mapped_estate, estate, globalimgurl;
var chkWmAreaChecked;
var chkZonaChecked;

$(function () {
	//Inisialisasi Checkbox dan RadioButton
	$('input').iCheck({
		checkboxClass: 'icheckbox_futurico',
		radioClass: 'iradio_square',
		increaseArea: '20%' // optional
	});
	$(".loading").hide();

	//For Event Handler
	isfullscreen = false;

	//Variable Declaration
	mapped_estate = $('#mapped_estate').val();
	estate = $('#estate').val();
	var utmprojection = $('#utmprojection').val();
	var rsid = $('#rsid').val();
	var coordinateExtent = $('#extent').val().split(',');
	var numzoom = $('#numzoom').val();
	var company = $('#company').val();
	var idwm = $('#idwm').val();
	var estatezone = $('#estatezone').val();
	var kondisi;

	listallwmarea = $.parseJSON($('#listAllWMArea').val());
	listyear = $('#listyear').val();
	//console.log(listyear);
	listmonth = $('#listmonth').val();
	listweek = $('#listweek').val();
	year = $('#year').val();
	month = $('#month').val();
	monthname = $('#monthname').val();
	week = $('#week').val();
	monthnamealias = $('#monthnamealias').val();
	idweek = $('#idweek').val();
	extentCoordinate = $("#extentCoordinate").val();
	kondisi = $('#isPieChart').val();
	sessionStorage.PreviousDate = null;
	var indexDiv = 0;
	var currentextent = $('#currentextent').val();

	if (sessionStorage.sessionWMCheck != null) {
		var isTrueSet = (sessionStorage.sessionWMCheck == 'true');
		if (isTrueSet) {
			for (var i = 0; i < listallwmarea.length; i++) {
				if (listallwmarea[i]["idWMArea"] == idwm) {
					coordinateExtent = [listallwmarea[i]["MinX"], listallwmarea[i]["MinY"], listallwmarea[i]["MaxX"], listallwmarea[i]["MaxY"]];
					zoomLevel = 12;
					break;
				}
			}
		}
	}

	latLong = [103.04289463774589, 0.0568859319754157, 103.11653040927285, 0.1393445512789726];
	centerCoordinate = [(latLong[0] + latLong[2]) / 2, (latLong[1] + latLong[3]) / 2];
	zoomLevel = 12;
	mapProjection = 'EPSG:4326';

	//MAIN FUNCTION
	$('#lbldate').html("Week " + week + ", " + monthnamealias + " " + year);
	//setUpDynamicCompanyFilter(false);
	$('#chkcompany').iCheck('check');
	$("#dynamicCompanyFilter").show();
	$("#dynamicWMAreaFilter").hide();
	$("#dynamicZonaFilter").hide();
	sessionStorage.sessionWMCheck = "false";
	sessionStorage.sessionZonaCheck = "false";

	//Control Init
	scaleLineControl = new ol.control.ScaleLine({ units: 'metric', className: 'ol-scale-line', target: document.getElementById('scale-line') });
	mousePositionControl = new ol.control.MousePosition({
		coordinateFormat: ol.coordinate.createStringXY(8),
		projection: mapProjection,
		target: document.getElementById('mouse-position'),
		undefinedHTML: '&nbsp;'
	});
	overviewMapControl = new ol.control.OverviewMap({
		className: 'ol-overviewmap ol-custom-overviewmap',
		collapseLabel: '\u00BB',
		label: '\u00AB',
		collapsed: true
	});
	zoomSliderControl = new ol.control.ZoomSlider();

	/*var isTrueCkWM = (sessionStorage.sessionWMCheck == 'true');
	if (isTrueCkWM)
	{
		layerBlock = new ol.layer.Image({
			source: new ol.source.ImageWMS({
				url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
				params: {
					'LAYERS': 'PZO:pzo_block_week_wmarea_unitpemantauan', //warnanya
					'viewparams': 'YEAR:' + year + ';WMAREA:' + idwm + ';MONTH:' + month + ';WEEK:' + week,
					'TRANSPARENT': 'true'
				},
			})
		});

		layerPiezometer = new ol.layer.Image({
			source: new ol.source.ImageWMS({
				url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
				params: {
					'LAYERS': 'PZO:	PZO_Point_Week_WM_Area_unitpemantauan',
					'viewparams': 'years:' + year + ';WMAREA:' + idwm + ';months:' + month + ';weeks:' + week,
					'TRANSPARENT': 'true'
				}
			})
		});
	}
	else
	{
		//console.log('else');
		//console.log('year = ' + year);
		//console.log('estate = ' + mapped_estate);
		//console.log('month = ' + month);
		//console.log('weeks = ' + week);
		layerBlock = new ol.layer.Image({
			source: new ol.source.ImageWMS({
				url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
				params: {
					'LAYERS': 'PZO:pzo_block_week_unitpemantauan', //warna pendek
					'viewparams': 'years:' + year + ';estate:' + 'MAG' + ';months:' + month + ';weeks:' + week,
					'TRANSPARENT': 'true'
				},
			})
		});

		layerPiezometer = new ol.layer.Image({
			source: new ol.source.ImageWMS({
				url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
				params: {
					'LAYERS': 'PZO:PZO_Point_Week_unitpemantauan',
					'viewparams': 'estate:' + 'MAG',
					'TRANSPARENT': 'true'
				}
			})
		});
	}*/

	/*layerInfo = new ol.layer.Tile({
		source: new ol.source.TileWMS({
			url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
			params: {
				'LAYERS': 'PZO:PZO_Info',
				'viewparams': 'PZOYEAR:' + year + ';ESTNR:' + mapped_estate + ';PZOMONTH:' + month + ';PZOWEEK:' + week,
				'TRANSPARENT': 'true'
			}
		})
	});*/

	sourceLayerDrawing = new ol.source.Vector({ wrapX: false });

	layerDrawing = new ol.layer.Vector({
		source: sourceLayerDrawing
	});

	sourceLayerMeasure = new ol.source.Vector();

	layerMeasure = new ol.layer.Vector({
		source: sourceLayerMeasure,
		style: new ol.style.Style({
			fill: new ol.style.Fill({
				color: 'rgba(255, 255, 255, 0.2)'
			}),
			stroke: new ol.style.Stroke({
				color: '#ffcc33',
				width: 2
			}),
			image: new ol.style.Circle({
				radius: 7,
				fill: new ol.style.Fill({
					color: '#ffcc33'
				})
			})
		})
	});

	/*if (sessionStorage.sessionZonaCheck == 'true') {
		$("#chkZona").prop("checked", isTrueSet);
		$("#dynamicZonaFilter").show();
	}*/
	//var isTrueCheckZona = sessionStorage.sessionZonaCheck == 'true'	

	map = new ol.Map({
		controls: ol.control.defaults({
			attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
				collapsible: false
			})
		}).extend([
			scaleLineControl, mousePositionControl, zoomSliderControl
		]),
		target: 'map',
		view: new ol.View({
			extent: latLong,
			projection: mapProjection,
			center: centerCoordinate,
			zoom: zoomLevel
		})
	});

	//map.addLayer(layerBlock);
	//map.addLayer(layerPiezometer);
	//layerBlock.setVisible(false);
	//layerPiezometer.setVisible(false);	

	//iCheck for checkbox and radio inputs
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_minimal-blue'
	})

	//Flat red color scheme for iCheck
	$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	})

	$('input').on('ifChanged', function (event) {
		if (this.id == "chkLayerBlock") {
			if (this.checked) {
				layerBlock.setVisible(true);
			}
			else {
				layerBlock.setVisible(false);
			}
		}
		else if (this.id == "chkLayerPiezometer") {
			if (this.checked) {
				layerPiezometer.setVisible(true);
			}
			else {
				layerPiezometer.setVisible(false);
			}
		}
		else if (this.id == "chkWmArea") {
			if (this.checked) {
				$("#dynamicCompanyFilter").hide();
				$("#dynamicWMAreaFilter").show();
				$("#dynamicZonaFilter").hide();
				sessionStorage.sessionWMCheck = true;
				isCheckZonaTrue = sessionStorage.sessionZonaCheck = false;
				chkZonaChecked = 0;
				chkWmAreaChecked = 1;
			}
		} else if (this.id == "chkZona") {
			if (this.checked) {
				$("#dynamicCompanyFilter").hide();
				$("#dynamicWMAreaFilter").show();
				$("#dynamicZonaFilter").show();
				isCheckZonaTrue = sessionStorage.sessionZonaCheck = true;
				sessionStorage.sessionWMCheck = false;
				chkZonaChecked = 1;
				chkWmAreaChecked = 0;
			}
		} else if (this.id == "chkcompany") {
			if (this.checked) {
				$("#dynamicCompanyFilter").show();
				$("#dynamicWMAreaFilter").hide();
				$("#dynamicZonaFilter").hide();
				sessionStorage.sessionWMCheck = false;
				isCheckZonaTrue = sessionStorage.sessionZonaCheck = false;
				chkZonaChecked = 0;
				chkWmAreaChecked = 0;
			}
		}
	});

	$("#toolsprevdate").click(function () {
		if (sessionStorage.PreviousDate != null && sessionStorage.PreviousDate != "null") {
			var time = sessionStorage.PreviousDate.split(',');
			week = time[0];
			month = time[1];
			year = time[2];
		}
		$.ajax({
			type: 'POST',
			url: '../Service/MapService.asmx/GetPreviousWeekFromParameter',
			data: '{Week: "' + week + '", Month: "' + month + '", Year: "' + year + '"}',
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			success: function (response) {
				var json = $.parseJSON(response.d);
				//console.log('respon balikan = ' + JSON.stringify(json));
				if (json.length > 0) {
					GetListWeek(json[0]["Year"], json[0]["Month"]);
					$("#selectFilterYear").val(json[0]["Year"]);
					$("#selectFilterMonth").val(json[0]["Month"]);
					$("#selectFilterWeek").val(json[0]["Week"]);
					$('.selectpicker').selectpicker('refresh');

					//var param = "W" + json[0]["Week"] + ("0" + json[0]["Month"]).slice(-2) + json[0]["Year"];
					sessionStorage.PreviousDate = json[0]["Week"] + "," + json[0]["Month"] + "," + json[0]["Year"];
					//getMapPreviousDate(json[0]["Week"], json[0]["Month"], json[0]["Year"]);                                        
					$('#lbldate').html("Week " + json[0]["Week"] + ", " + json[0]["MonthName"] + " " + json[0]["Year"]);
					triggerClick();
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.statusText);
			}
		});
	})

	$("#toolsnextdate").click(function () {
		if (sessionStorage.PreviousDate != null && sessionStorage.PreviousDate != "null") {
			var time = sessionStorage.PreviousDate.split(',');
			week = time[0];
			month = time[1];
			year = time[2];
		}
		$.ajax({
			type: 'POST',
			url: '../Service/MapService.asmx/GetNextWeekFromParameter',
			data: '{Week: "' + week + '", Month: "' + month + '", Year: "' + year + '"}',
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			success: function (response) {
				/*var json = $.parseJSON(response.d);
				if (json.length > 0) {
					var param = "W" + json[0]["Week"] + ("0" + json[0]["Month"]).slice(-2) + json[0]["Year"];					
					reLoad(param);
				}*/
				var json = $.parseJSON(response.d);
				//console.log('respon balikan = ' + JSON.stringify(json));
				if (json.length > 0) {
					GetListWeek(json[0]["Year"], json[0]["Month"]);
					$("#selectFilterYear").val(json[0]["Year"]);
					$("#selectFilterMonth").val(json[0]["Month"]);
					$("#selectFilterWeek").val(json[0]["Week"]);
					$('.selectpicker').selectpicker('refresh');

					//var param = "W" + json[0]["Week"] + ("0" + json[0]["Month"]).slice(-2) + json[0]["Year"];
					sessionStorage.PreviousDate = json[0]["Week"] + "," + json[0]["Month"] + "," + json[0]["Year"];
					//getMapPreviousDate(json[0]["Week"], json[0]["Month"], json[0]["Year"]);                                        
					$('#lbldate').html("Week " + json[0]["Week"] + ", " + json[0]["MonthName"] + " " + json[0]["Year"]);
					triggerClick();
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.statusText);
			}
		});
	})

	$("#btnGo").click(function () {
		triggerClick();
	})

	$("#selectFilterCompany").change(function () {
		ListEstate($("#selectFilterCompany").val());
	})

	$("#selectFilterYear").change(function () {
		GetListMonth($("#selectFilterYear").val());
	})

	$("#selectFilterMonth").change(function () {
		GetListWeek($("#selectFilterYear").val(), $("#selectFilterMonth").val());
	})
	$("#selectFilterWMArea").change(function () {
		ListZonaByWmArea($("#selectFilterWMArea").val());
	});
	$('.selectpicker').selectpicker({});
	ListCompany();
	ListWMArea();
	GetListYear();

	//Date picker
	$('#datepicker').datepicker({
		autoclose: true
	})

	//iCheck for checkbox and radio inputs
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_minimal-blue'
	})

	//Flat red color scheme for iCheck
	$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
		checkboxClass: 'icheckbox_flat-green',
		radioClass: 'iradio_flat-green'
	})

	MapEventHandler();

	triggerClick();

	map.on('singleclick', function (evt) {
		indexDiv = indexDiv + 1;
		/*var viewResolution = map.getView().getResolution();
		var url = layerPiezometer.getSource().getGetFeatureInfoUrl(evt.coordinate, viewResolution, 'EPSG:4326', { 'INFO_FORMAT': 'application/json' });
		//console.log('hello world click url = ' + url);
		reqwest({
			url: "../Handler/proxy.ashx?url=" + encodeURIComponent(url),
			type: 'json',
		}).then(function (data) {
			if (data.features.length > 0) {
				var feature = data.features[0];
				var props = feature.properties;
				ShowDetailV2(props.PieRecordID, indexDiv);
			}
		});

		var url_block = layerBlock.getSource().getGetFeatureInfoUrl(evt.coordinate, viewResolution, 'EPSG:4326', { 'INFO_FORMAT': 'application/json' });
		//console.log('hello world click url block = ' + url_block);
		reqwest({
			url: "../Handler/proxy.ashx?url=" + encodeURIComponent(url_block),
			type: 'json',
		}).then(function (data) {            
			if (data.features.length > 0) {
				var feature = data.features[0];
				var props = feature.properties;                   
				ShowDetailV2(props.PieRecordID, indexDiv);
			}
		});*/
		var viewResolution = map.getView().getResolution();
		var urlX = layerPiezometer.getSource().getGetFeatureInfoUrl(evt.coordinate, viewResolution, 'EPSG:4326', { 'INFO_FORMAT': 'application/json' });
		if (urlX != null) {
			urlX = layerBlock.getSource().getGetFeatureInfoUrl(evt.coordinate, viewResolution, 'EPSG:4326', { 'INFO_FORMAT': 'application/json' });
		}
		$(".loading").show()
		$.ajax({
			type: 'GET',
			url: urlX,
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			success: function (response) {
				$(".loading").hide();
				ShowDetailV2(response.features[0].properties.PieRecordID, indexDiv);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				$(".loading").hide();
				alert("function " + urlX + " : " + xhr.statusText);
			}
		});

	});

	//Go To Map
	//

})

function ListWMArea() {
	$.ajax({
		type: 'POST',
		url: '../Service/MapServiceTambahan.asmx/ListWMArea',
		data: '{}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		async: false,
		success: function (response) {
			var json = $.parseJSON(response.d);
			$('#selectFilterWMArea').find('option').remove();
			for (var i = 0; i < json.length; i++) {
				$("#selectFilterWMArea").append($('<option>', {
					value: json[i]["idWMArea"],
					text: json[i]["wmAreaName"]
				}));
			}
			$("#selectFilterWMArea").val($('#idwm').val()).change();
			var isTrue = (sessionStorage.sessionWMCheck == 'true');
			if (isTrue) {
				$('#titleEstate').html($('#selectFilterWMArea option:selected').text());
			}
			ListZonaByWmArea($("#selectFilterWMArea").val());
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function ListWMArea : " + xhr.statusText);
		}
	});
}

function ListCompany() {
	$.ajax({
		type: 'POST',
		url: '../Service/MapService.asmx/ListCompany',
		data: '{}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		async: false,
		success: function (response) {
			var dataValue = response.d[0].split(";");
			var dataText = response.d[1].split(";");
			var dataSubText = response.d[2].split(";");
			var dataLength = response.d[0].split(";").length;

			$('#selectFilterCompany').find('option').remove();
			$("#selectFilterCompany").append($('<option>', {
				value: "",
				text: "All Company"
			}));

			for (var i = 0; i < dataLength; i++) {
				$("#selectFilterCompany").append($('<option>', {
					value: dataValue[i],
					text: dataText[i]
				}));
			}

			$("#selectFilterCompany").val($('#company').val()).change();

		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function ListCompany : " + xhr.statusText);
		}
	});
}

function ListEstate(companycode) {
	//console.log(companycode);
	$.ajax({
		type: 'POST',
		url: '../Service/MapService.asmx/ListEstate',
		data: '{CompanyCode: "' + companycode + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		async: false,
		success: function (response) {
			var dataValue = response.d[0].split(";");
			var dataText = response.d[1].split(";");
			var dataLength = response.d[0].split(";").length;

			$('#selectFilterEstate').find('option').remove();
			for (var i = 0; i < dataLength; i++) {
				$("#selectFilterEstate").append($('<option>', {
					value: dataValue[i],
					text: dataValue[i] + " - " + dataText[i]
				}));
			}

			$("#selectFilterEstate").val($('#estate').val());
			var isTrue = (sessionStorage.sessionWMCheck == 'true');
			if (!isTrue) {
				$('#titleEstate').html($('#selectFilterEstate option:selected').text().split(' - ')[2]);
			}
			$('.selectpicker').selectpicker('refresh');
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function ListEstate : " + xhr.statusText);
		}
	});
}

function GetListYear() {
	var json = $.parseJSON(listyear);
	$('#selectFilterYear').find('option').remove();
	for (var i = 0; i < json.length; i++) {
		$("#selectFilterYear").append($('<option>', {
			value: json[i]["Year"],
			text: json[i]["Year"]
		}));
	}
	$("#selectFilterYear").val(year).change();
}

function GetListMonth(year) {
	var json = $.parseJSON(listmonth);
	$('#selectFilterMonth').find('option').remove();
	for (var i = 0; i < json.length; i++) {
		if (json[i]["Year"] == year) {
			$("#selectFilterMonth").append($('<option>', {
				value: json[i]["Month"],
				text: json[i]["MonthName"]
			}));
		}
	}

	if ($("#selectFilterYear").val() == year)
		$("#selectFilterMonth").val(month).change();
	else
		$("#selectFilterMonth").change();
}

function GetListWeek(year, month) {
	var json = $.parseJSON(listweek);
	$('#selectFilterWeek').find('option').remove();

	for (var i = 0; i < json.length; i++) {
		if (json[i]["Year"] == year && json[i]["Month"] == month) {
			//alert(json[i]["Week"]);
			$("#selectFilterWeek").append($('<option>', {
				value: json[i]["Week"],
				text: "Week " + json[i]["Week"]
			}));
		}
	}

	if ($("#selectFilterWeek option[value='" + week + "']").length > 0)
		$("#selectFilterWeek").val(week);

	$('.selectpicker').selectpicker('refresh');
}

function setUpDynamicCompanyFilter(show) {
	if (show) {
		//show wm area
		$("#dynamicCompanyFilter").hide();
		$("#dynamicWMAreaFilter").show();
		sessionStorage.sessionWMCheck = true;
	} else {
		//hide wm area
		$("#dynamicCompanyFilter").show();
		$("#dynamicWMAreaFilter").hide();
		sessionStorage.sessionWMCheck = false
	}
}

function showZonaDropdown(show) {
	var zona = document.getElementById("dynamicZonaFilter");
	if (show) {
		$("#dynamicZonaFilter").show();
		$("#dynamicWMAreaFilter").show();
		$("#dynamicCompanyFilter").hide();
		isCheckZonaTrue = sessionStorage.sessionZonaCheck = true;
	} else {
		$("#dynamicZonaFilter").hide();
		isCheckZonaTrue = sessionStorage.sessionZonaCheck = false;
	}
}

function ListZonaByWmArea(idWmArea) {
	var json = $.parseJSON(extentCoordinate);

	//console.log('json response ListZonaByWmArea =  ' + JSON.stringify(json));
	$('#selectFilterZona').find('option').remove();
	for (var i = 0; i < json.length; i++) {
		if (json[i]["idWMArea"] == idWmArea)
			$("#selectFilterZona").append($('<option>', {
				value: json[i]["WMA_code"],
				text: json[i]["WMA_code"]
			}));
		$('.selectpicker').selectpicker('refresh');
	}

}

function reLoad(idweek) {
	var isTrue = (sessionStorage.sessionWMCheck == 'true');
	if (isTrue) {
		window.location = "PZO_Map.aspx?typeModule=OLM&idwm=" + $("#selectFilterWMArea").val() + "&IDWEEK=" + idweek;
	} else {
		window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + $("#selectFilterCompany").val() + "&Estate=" + $("#selectFilterEstate").val() + "&IDWEEK=" + idweek + "&ZONA=" + $("#selectFilterZona").val();
	}
}

function getMapPreviousDate(week, month, year) {
	//console.log('week = ' + week);
	//console.log('month = ' + month);
	//console.log('year = ' + year);
	var extendCoordinate = "";
	var param = "W" + $("#selectFilterWeek").val() + ("0" + $("#selectFilterMonth").val()).slice(-2) + $("#selectFilterYear").val();
	if (sessionStorage.sessionWMCheck != "true" && sessionStorage.sessionZonaCheck != "true") {
		//alert('CHECK LIST WM DAN ZONA UNCHECKED');

		map.removeLayer(layerHighlight);
		$.ajax({
			type: 'POST',
			url: '../Service/MapServiceTambahan.asmx/GetEstateDetail',
			data: '{estCode: "' + $("#selectFilterEstate").val() + '"}',
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			success: function (response) {
				var json = $.parseJSON(response.d);
				extendCoordinate = json[0]['CoordinateExtent'];
				var extendCoordinateSplit = json[0]['CoordinateExtent'].split(',');
				latLong = [parseFloat(extendCoordinateSplit[0]), parseFloat(extendCoordinateSplit[1]), parseFloat(extendCoordinateSplit[2]), parseFloat(extendCoordinateSplit[3])];
				var hasilPenjumlahan1 = (parseFloat(extendCoordinateSplit[0]) + parseFloat(extendCoordinateSplit[2])) / 2;
				var hasilPenjumlahan2 = (parseFloat(extendCoordinateSplit[1]) + parseFloat(extendCoordinateSplit[3])) / 2;
				//var centerCoordinate0 = (parseFloat(extendCoordinate[0]) + parseFloat(extendCoordinate[2])) / 2
				//var centerCoordinate1 = (extendCoordinate[1] + extendCoordinate[3]) / 2
				centerCoordinate = [(extendCoordinateSplit[0] + extendCoordinateSplit[2]) / 2, (extendCoordinateSplit[1] + extendCoordinateSplit[3]) / 2];


				//console.log('coordinate extend = ' + latLong);

				//console.log('center coordinate 0 = ' + hasilPenjumlahan2);

				layerBlock.getSource().updateParams({
					'LAYERS': 'PZO:pzo_block_week_unitpemantauan', //warna pendek
					'viewparams': 'years:' + year + ';estate:' + $("#selectFilterEstate").val() + ';months:' + month + ';weeks:' + week,
					'TRANSPARENT': 'true',
					'TILED': true
				});

				layerPiezometer.getSource().updateParams({
					'LAYERS': 'PZO:PZO_Point_Week_unitpemantauan',
					'viewparams': 'estate:' + $("#selectFilterEstate").val(),
					'TRANSPARENT': 'true',
					'TILED': true
				});

				//console.log('map projection = ' + mapProjection);
				map.setView(new ol.View({
					extent: latLong,
					projection: mapProjection,
					center: [hasilPenjumlahan1, hasilPenjumlahan2],
					zoom: 13
				}));

				map.addLayer(layerPiezometer);
				//layerBlock.setVisible(true);
				//layerPiezometer.setVisible(true);

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.statusText);
			}
		});



	}
	else if (sessionStorage.sessionWMCheck == "true") {

		if (sessionStorage.sessionZonaCheck == "true") {
			var coordinateExtentPulau;
			//alert('check zona checked');
			for (var i = 0; i < listallwmarea.length; i++) {
				if (listallwmarea[i]["idWMArea"] == $("#selectFilterWMArea").val()) {
					coordinateExtentPulau = [listallwmarea[i]["MinX"], listallwmarea[i]["MinY"], listallwmarea[i]["MaxX"], listallwmarea[i]["MaxY"]];
					break;
				}
			}
			layerBlock.getSource().updateParams({
				'LAYERS': 'PZO:pzo_block_week_wmarea_unitpemantauan',
				'viewparams': 'YEAR:' + year + ';WMAREA:' + $("#selectFilterWMArea").val() + ';MONTH:' + month + ';WEEK:' + week,
				'TRANSPARENT': 'true',
				'TILED': true
			});
			map.removeLayer(layerPiezometer);
			map.removeLayer(layerHighlight);
			var style = "";
			latLong = "";
			centerCoordinate = "";
			var json = JSON.parse(extentCoordinate);
			for (var j = 0; j < json.length; j++) {
				if (json[j]["WMA_code"] == $("#selectFilterZona").val()) {
					//console.log(json[j]["WMA_code"] + '&' + $("#selectFilterZona").val() + ' sama ')
					coordinateExtent = [parseFloat(json[j]["MinX"]), parseFloat(json[j]["MinY"]), parseFloat(json[j]["MaxX"]), parseFloat(json[j]["MaxY"])];
					centerCoordinate = [(coordinateExtent[0] + coordinateExtent[2]) / 2, (coordinateExtent[1] + coordinateExtent[3]) / 2];
				}
			}
			style = new ol.style.Style({
				stroke: new ol.style.Stroke({
					color: '#9b59b6',
					width: 5
				}),
				fill: new ol.style.Fill({
					color: 'rgba(255, 255, 255, 0.3)'
				}),
			});

			var UrlCQL = "https://map.gis-div.com:8443/geoserver/pub_pzo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=PZO:PZO_WMZone&CQL_FILTER=WMA_code=%27" + $("#selectFilterZona").val() + "%27&maxFeatures=50&outputFormat=application/json";
			layerHighlight = new ol.layer.Vector({
				source: new ol.source.Vector({
					url: UrlCQL,
					format: new ol.format.GeoJSON()
				}),
				style: function (feature) {
					//style.getText().setText(feature.get('style'));
					return style;
				}
			});
			layerPiezometer = new ol.layer.Image({
				source: new ol.source.ImageWMS({
					url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
					params: {
						'LAYERS': 'PZO:	PZO_Point_Week_WM_Area_unitpemantauan',
						'viewparams': 'years:' + $("#selectFilterYear").val() + ';WMAREA:' + $("#selectFilterWMArea").val() + ';months:' + $("#selectFilterMonth").val() + ';weeks:' + $("#selectFilterWeek").val(),
						'TRANSPARENT': 'true'
					}
				})
			});

			zoomLevel = 14;
			//console.log('latLong = ' + coordinateExtentPulau);
			//console.log('center koordinate = ' + centerCoordinate);
			map.addLayer(layerHighlight);
			//alert('ini ke load nggak?');
			map.setView(new ol.View({
				extent: coordinateExtentPulau,
				projection: mapProjection,
				center: centerCoordinate,
				zoom: 15
			}));
		} else {
			//alert('only check wm area');
			map.removeLayer(layerBlock);
			map.removeLayer(layerPiezometer);
			map.removeLayer(layerHighlight);
			for (var i = 0; i < listallwmarea.length; i++) {
				if (listallwmarea[i]["idWMArea"] == $("#selectFilterWMArea").val()) {
					coordinateExtent = [parseFloat(listallwmarea[i]["MinX"]), parseFloat(listallwmarea[i]["MinY"]), parseFloat(listallwmarea[i]["MaxX"]), parseFloat(listallwmarea[i]["MaxY"])];
					centerCoordinate = [(coordinateExtent[0] + coordinateExtent[2]) / 2, (coordinateExtent[1] + coordinateExtent[3]) / 2];
					zoomLevel = 12;
					break;
				}
			}
			//console.log('coordinate extent = ' + coordinateExtent);
			layerBlock.getSource().updateParams({
				'LAYERS': 'PZO:pzo_block_week_wmarea_unitpemantauan',
				'viewparams': 'YEAR:' + year + ';WMAREA:' + $("#selectFilterWMArea").val() + ';MONTH:' + month + ';WEEK:' + week,
				'TRANSPARENT': 'true',
				'TILED': true
			});
			map.setView(new ol.View({
				extent: coordinateExtent,
				projection: mapProjection,
				center: centerCoordinate,
				zoom: 12
			}));
			map.addLayer(layerBlock);

			//map.getView().fit(coordinateExtent, { duration: 0 })

		}

	}
	else if (sessionStorage.sessionZonaCheck == "true") {
		var coordinateExtentPulau;
		map.removeLayer(layerPiezometer)
		map.removeLayer(layerHighlight);
		for (var i = 0; i < listallwmarea.length; i++) {
			if (listallwmarea[i]["idWMArea"] == $("#selectFilterWMArea").val()) {
				coordinateExtentPulau = [listallwmarea[i]["MinX"], listallwmarea[i]["MinY"], listallwmarea[i]["MaxX"], listallwmarea[i]["MaxY"]];
				break;
			}
		}
		//console.log('week sebelum update layer block = ' + week);
		//console.log('month sebelum update layer block = ' + month);
		//console.log('year sebelum update layer block = ' + year);
		layerBlock.getSource().updateParams({
			'LAYERS': 'PZO:pzo_block_week_wmarea_unitpemantauan',
			'viewparams': 'YEAR:' + year + ';WMAREA:' + $("#selectFilterWMArea").val() + ';MONTH:' + month + ';WEEK:' + week,
			'TRANSPARENT': 'true',
			'TILED': true
		});
		var style = "";
		latLong = "";
		centerCoordinate = "";
		var json = JSON.parse(extentCoordinate);
		for (var j = 0; j < json.length; j++) {
			if (json[j]["WMA_code"] == $("#selectFilterZona").val()) {
				//console.log(json[j]["WMA_code"] + '&' + $("#selectFilterZona").val() + ' sama ')
				coordinateExtent = [parseFloat(json[j]["MinX"]), parseFloat(json[j]["MinY"]), parseFloat(json[j]["MaxX"]), parseFloat(json[j]["MaxY"])];
				centerCoordinate = [(coordinateExtent[0] + coordinateExtent[2]) / 2, (coordinateExtent[1] + coordinateExtent[3]) / 2];
			}
		}
		style = new ol.style.Style({
			stroke: new ol.style.Stroke({
				color: '#9b59b6',
				width: 5
			}),
			fill: new ol.style.Fill({
				color: 'rgba(255, 255, 255, 0.3)'
			}),
		});

		var UrlCQL = "https://map.gis-div.com:8443/geoserver/pub_pzo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=PZO:PZO_WMZone&CQL_FILTER=WMA_code=%27" + $("#selectFilterZona").val() + "%27&maxFeatures=50&outputFormat=application/json";
		layerHighlight = new ol.layer.Vector({
			source: new ol.source.Vector({
				url: UrlCQL,
				format: new ol.format.GeoJSON()
			}),
			style: function (feature) {
				return style;
			}
		});

		zoomLevel = 16;
		//console.log('latLong = ' + coordinateExtentPulau);
		//console.log('center koordinate = ' + centerCoordinate);
		map.addLayer(layerHighlight);
		map.setView(new ol.View({
			extent: coordinateExtentPulau,
			projection: mapProjection,
			center: centerCoordinate,
			zoom: 14
		}));
		map.addLayer(layerBlock);
		map.addLayer(layerHighlight);
	}
}

function triggerClick() { //untuk load map dari pie chart (not efficient)
	//alert('triggerClicked')
	//var extendCoordinate = "";

	map.removeLayer(layerHighlight);
	map.removeLayer(layerBlock);
	map.removeLayer(layerPiezometer);

	if (sessionStorage.sessionWMCheck != "true" && sessionStorage.sessionZonaCheck != "true") {
		//Company
		$("#titleEstate").html($("#selectFilterEstate option:selected").text().split('-')[1]);
		$.ajax({
			type: 'POST',
			url: '../Service/MapServiceTambahan.asmx/GetEstateDetail',
			data: '{estCode: "' + $("#selectFilterEstate").val() + '"}',
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			success: function (response) {
				console.log(response.d)

				var json = $.parseJSON(response.d);
				extendCoordinate = json[0]['CoordinateExtent'];
				var extendCoordinateSplit = json[0]['CoordinateExtent'].split(',');
				latLong = [parseFloat(extendCoordinateSplit[0]), parseFloat(extendCoordinateSplit[1]), parseFloat(extendCoordinateSplit[2]), parseFloat(extendCoordinateSplit[3])];
				var hasilPenjumlahan1 = (parseFloat(extendCoordinateSplit[0]) + parseFloat(extendCoordinateSplit[2])) / 2;
				var hasilPenjumlahan2 = (parseFloat(extendCoordinateSplit[1]) + parseFloat(extendCoordinateSplit[3])) / 2;
				centerCoordinate = [(extendCoordinateSplit[0] + extendCoordinateSplit[2]) / 2, (extendCoordinateSplit[1] + extendCoordinateSplit[3]) / 2];

				/*layerBlock.getSource().updateParams({
					'LAYERS': 'PZO:pzo_block_week_unitpemantauan', //warna pendek
					'viewparams': 'years:' + $("#selectFilterYear").val() + ';estate:' + $("#selectFilterEstate").val() + ';months:' + $("#selectFilterMonth").val() + ';weeks:' + $("#selectFilterWeek").val(),
					'TRANSPARENT': 'true',
					'TILED': true
				});

				layerPiezometer.getSource().updateParams({
					'LAYERS': 'PZO:PZO_Point_Week_unitpemantauan',
					'viewparams': 'estate:' + $("#selectFilterEstate").val(),
					'TRANSPARENT': 'true',
					'TILED': true
				});*/

				layerBlock = new ol.layer.Image({
					source: new ol.source.ImageWMS({
						url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
						params: {
							'LAYERS': 'PZO:pzo_block_week_unitpemantauan', //warna pendek
							'viewparams': 'years:' + $("#selectFilterYear").val() + ';estate:' + $("#selectFilterEstate").val() + ';months:' + $("#selectFilterMonth").val() + ';weeks:' + $("#selectFilterWeek").val(),
							'TRANSPARENT': 'true'
						},
					})
				});

				layerPiezometer = new ol.layer.Image({
					source: new ol.source.ImageWMS({
						url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
						params: {
							'LAYERS': 'PZO:PZO_Point_Week_unitpemantauan',
							'viewparams': 'estate:' + $("#selectFilterEstate").val(),
							'TRANSPARENT': 'true'
						}
					})
				});

				map.setView(new ol.View({
					extent: latLong,
					projection: mapProjection,
					center: [hasilPenjumlahan1, hasilPenjumlahan2],
					zoom: 13
				}));
				map.addLayer(layerBlock);
				map.addLayer(layerPiezometer);
				layerBlock.setVisible(true);
				layerPiezometer.setVisible(true);

			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.statusText + ' gg');
			}
		});
	}
	else if (sessionStorage.sessionWMCheck == "true") {
		//Filter WMN
		$("#titleEstate").html($("#selectFilterWMArea option:selected").text());
		/*if (sessionStorage.sessionZonaCheck == "true")
		{
			map.removeLayer(layerHighlight);
			map.removeLayer(layerPiezometer);
			var coordinateExtentPulau;          
			for (var i = 0; i < listallwmarea.length; i++) {
				if (listallwmarea[i]["idWMArea"] == $("#selectFilterWMArea").val()) {
					coordinateExtentPulau = [listallwmarea[i]["MinX"], listallwmarea[i]["MinY"], listallwmarea[i]["MaxX"], listallwmarea[i]["MaxY"]];
					break;
				}
			}
			layerBlock.getSource().updateParams({
				'LAYERS': 'PZO:pzo_block_week_wmarea_unitpemantauan',
				'viewparams': 'YEAR:' + $("#selectFilterYear").val() + ';WMAREA:' + $("#selectFilterWMArea").val() + ';MONTH:' + $("#selectFilterMonth").val() + ';WEEK:' + $("#selectFilterWeek").val(),
				'TRANSPARENT': 'true',
				'TILED': true
			});
			var style = "";
			latLong = "";
			centerCoordinate = "";
			var json = JSON.parse(extentCoordinate);
			for (var j = 0; j < json.length; j++) {
				if (json[j]["WMA_code"] == $("#selectFilterZona").val()) {
					console.log(json[j]["WMA_code"] + '&' + $("#selectFilterZona").val() + ' sama ')
					coordinateExtent = [parseFloat(json[j]["MinX"]), parseFloat(json[j]["MinY"]), parseFloat(json[j]["MaxX"]), parseFloat(json[j]["MaxY"])];
					centerCoordinate = [(coordinateExtent[0] + coordinateExtent[2]) / 2, (coordinateExtent[1] + coordinateExtent[3]) / 2];
				}
			}
			style = new ol.style.Style({
				stroke: new ol.style.Stroke({
					color: '#9b59b6',
					width: 5
				}),
				fill: new ol.style.Fill({
					color: 'rgba(255, 255, 255, 0.3)'
				}),
			});

			var UrlCQL = "https://map.gis-div.com:8443/geoserver/pub_pzo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=PZO:PZO_WMZone&CQL_FILTER=WMA_code=%27" + $("#selectFilterZona").val() + "%27&maxFeatures=50&outputFormat=application/json";
			layerHighlight = new ol.layer.Vector({
				source: new ol.source.Vector({
					url: UrlCQL,
					format: new ol.format.GeoJSON()
				}),
				style: function (feature) {               
					return style;
				}
			});

			layerPiezometer = new ol.layer.Image({
				source: new ol.source.ImageWMS({
					url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
					params: {
						'LAYERS': 'PZO:	PZO_Point_Week_WM_Area_unitpemantauan',
						'viewparams': 'years:' + $("#selectFilterYear").val() + ';WMAREA:' + $("#selectFilterWMArea").val() + ';months:' + $("#selectFilterMonth").val() + ';weeks:' + $("#selectFilterWeek").val(),
						'TRANSPARENT': 'true'
					}
				})
			});

			zoomLevel = 13;
			//console.log('latLong = ' + coordinateExtentPulau);
			//console.log('center koordinate = ' + centerCoordinate);

 
			map.setView(new ol.View({
				extent: coordinateExtentPulau,
				projection: mapProjection,
				center: centerCoordinate,
				zoom: 13
			}));
			map.addLayer(layerHighlight)
		}
		else
		{     */
		for (var i = 0; i < listallwmarea.length; i++) {
			if (listallwmarea[i]["idWMArea"] == $("#selectFilterWMArea").val()) {
				coordinateExtent = [parseFloat(listallwmarea[i]["MinX"]), parseFloat(listallwmarea[i]["MinY"]), parseFloat(listallwmarea[i]["MaxX"]), parseFloat(listallwmarea[i]["MaxY"])];
				centerCoordinate = [(coordinateExtent[0] + coordinateExtent[2]) / 2, (coordinateExtent[1] + coordinateExtent[3]) / 2];
				zoomLevel = 12;
				break;
			}
		}
		//console.log('coordinate extent = ' + coordinateExtent);
		/*layerBlock.getSource().updateParams({
			'LAYERS': 'PZO:pzo_block_week_wmarea_unitpemantauan',
			'viewparams': 'YEAR:' + $("#selectFilterYear").val() + ';WMAREA:' + $("#selectFilterWMArea").val() + ';MONTH:' + $("#selectFilterMonth").val() + ';WEEK:' + $("#selectFilterWeek").val(),
			'TRANSPARENT': 'true',
			'TILED': true
		});*/

		layerBlock = new ol.layer.Image({
			source: new ol.source.ImageWMS({
				url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
				params: {
					'LAYERS': 'PZO:pzo_block_week_wmarea_unitpemantauan', //warnanya
					'viewparams': 'YEAR:' + $("#selectFilterYear").val() + ';WMAREA:' + $("#selectFilterWMArea").val() + ';MONTH:' + $("#selectFilterMonth").val() + ';WEEK:' + $("#selectFilterWeek").val(),
					'TRANSPARENT': 'true'
				},
			})
		});

		layerPiezometer = new ol.layer.Image({
			source: new ol.source.ImageWMS({
				url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
				params: {
					'LAYERS': 'PZO:	PZO_Point_Week_WM_Area_unitpemantauan',
					'viewparams': 'years:' + $("#selectFilterYear").val() + ';WMAREA:' + $("#selectFilterWMArea").val() + ';months:' + $("#selectFilterMonth").val() + ';weeks:' + $("#selectFilterWeek").val(),
					'TRANSPARENT': 'true'
				}
			})
		});

		map.setView(new ol.View({
			extent: coordinateExtent,
			projection: mapProjection,
			center: centerCoordinate,
			zoom: 12
		}));

		map.addLayer(layerBlock);
		map.addLayer(layerPiezometer);
		layerBlock.setVisible(true);
		layerPiezometer.setVisible(true);

		//}
	}
	else if (sessionStorage.sessionZonaCheck == "true") {
		//Filter Zona
		$("#titleEstate").html($("#selectFilterWMArea option:selected").text() + " - " + $("#selectFilterZona option:selected").text());
		var coordinateExtentPulau;
		for (var i = 0; i < listallwmarea.length; i++) {
			if (listallwmarea[i]["idWMArea"] == $("#selectFilterWMArea").val()) {
				coordinateExtentPulau = [listallwmarea[i]["MinX"], listallwmarea[i]["MinY"], listallwmarea[i]["MaxX"], listallwmarea[i]["MaxY"]];
				break;
			}
		}

		var style = "";
		latLong = "";
		centerCoordinate = "";
		var json = JSON.parse(extentCoordinate);

		for (var j = 0; j < json.length; j++) {
			if (json[j]["WMA_code"] == $("#selectFilterZona").val()) {
				//console.log(json[j]["WMA_code"] + '&' + $("#selectFilterZona").val() + ' sama ')
				coordinateExtent = [parseFloat(json[j]["MinX"]), parseFloat(json[j]["MinY"]), parseFloat(json[j]["MaxX"]), parseFloat(json[j]["MaxY"])];
				centerCoordinate = [(coordinateExtent[0] + coordinateExtent[2]) / 2, (coordinateExtent[1] + coordinateExtent[3]) / 2];
			}
		}
		style = new ol.style.Style({
			stroke: new ol.style.Stroke({
				color: '#9b59b6',
				width: 5
			}),
			fill: new ol.style.Fill({
				color: 'rgba(255, 255, 255, 0.3)'
			}),
		});

		var UrlCQL = "https://map.gis-div.com:8443/geoserver/pub_pzo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=PZO:PZO_WMZone&CQL_FILTER=WMA_code=%27" + $("#selectFilterZona").val() + "%27&maxFeatures=50&outputFormat=application/json";
		layerHighlight = new ol.layer.Vector({
			source: new ol.source.Vector({
				url: UrlCQL,
				format: new ol.format.GeoJSON()
			}),
			style: function (feature) {

				return style;
			}
		});

		layerBlock = new ol.layer.Image({
			source: new ol.source.ImageWMS({
				url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
				params: {
					'LAYERS': 'PZO:pzo_block_week_wmarea_unitpemantauan', //warnanya
					'viewparams': 'YEAR:' + $("#selectFilterYear").val() + ';WMAREA:' + $("#selectFilterWMArea").val() + ';MONTH:' + $("#selectFilterMonth").val() + ';WEEK:' + $("#selectFilterWeek").val(),
					'TRANSPARENT': 'true'
				},
			})
		});

		layerPiezometer = new ol.layer.Image({
			source: new ol.source.ImageWMS({
				url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
				params: {
					'LAYERS': 'PZO:	PZO_Point_Week_WM_Area_unitpemantauan',
					'viewparams': 'years:' + $("#selectFilterYear").val() + ';WMAREA:' + $("#selectFilterWMArea").val() + ';months:' + $("#selectFilterMonth").val() + ';weeks:' + $("#selectFilterWeek").val(),
					'TRANSPARENT': 'true'
				}
			})
		});

		zoomLevel = 12;
		//console.log('latLong = ' + coordinateExtentPulau);
		//console.log('center koordinate = ' + centerCoordinate);


		map.setView(new ol.View({
			extent: coordinateExtentPulau,
			projection: mapProjection,
			center: centerCoordinate,
			zoom: 12
		}));

		map.addLayer(layerBlock);
		map.addLayer(layerPiezometer);
		map.addLayer(layerHighlight);
		layerBlock.setVisible(true);
		layerPiezometer.setVisible(true);
		layerHighlight.setVisible(true);
	}

	$('#chkLayerBlock').iCheck('check');
	$('#chkLayerPiezometer').iCheck('check');
	//layerBlock.setVisible(true);
	//layerPiezometer.setVisible(true);
}
