﻿function MapEventHandler() {
    $('#btnCloseMyToolbox').click(function () {
        map.removeInteraction(draw);
        layerDrawing.getSource().clear();

        map.getOverlays().getArray().slice(0).forEach(function (overlay) {
            map.removeOverlay(overlay);
        });
        layerMeasure.getSource().clear();

        $('#btnStartDrawing').show();
        $('#btnStopDrawing').hide();
        $('#btnStartMeasure').show();
        $('#btnStopMeasure').hide();

        HideToolbox();
    })

    // Tool - Fullscreen
    $("#toolFullscreen").click(function () {
        if (isfullscreen) exitfullscreen(); else {
            fullScreen();
        }
    })

    // Tool - Overview
    $("#toolOverview").click(function () {
    })

    // Tool - Drawing
    $("#toolDrawing").click(function () {
        $('#myToolboxTitle').html('Drawing Toolbox');
        $('#myDrawingToolbox').show();
        $('#myMeasureToolbox').hide();

        ShowToolbox();
    })

    $('#btnStartDrawing').click(function () {
        $('#btnStartDrawing').hide();
        $('#btnStopDrawing').show();

        var value = $('#selectDrawingMode').val();
        draw = new ol.interaction.Draw({
            source: sourceLayerDrawing,
            type: value
        });
        map.addInteraction(draw);
    })

    $('#btnStopDrawing').click(function () {
        $('#btnStartDrawing').show();
        $('#btnStopDrawing').hide();

        map.removeInteraction(draw);
    })

    $('#btnClearDrawing').click(function () {
        layerDrawing.getSource().clear();
    })


    // Tool - Measure
    $("#toolMeasure").click(function () {
        $('#myToolboxTitle').html('Measure Toolbox');
        $('#myDrawingToolbox').hide();
        $('#myMeasureToolbox').show();

        ShowToolbox();
    })

    $('#btnStartMeasure').click(function () {
        $('#btnStartMeasure').hide();
        $('#btnStopMeasure').show();

        Measure();
    })

    $('#btnStopMeasure').click(function () {
        $('#btnStartMeasure').show();
        $('#btnStopMeasure').hide();

        map.removeInteraction(draw);
    })

    $('#btnClearMeasure').click(function () {

        map.getOverlays().getArray().slice(0).forEach(function (overlay) {
            map.removeOverlay(overlay);
        });
        createMeasureTooltip();
        createHelpTooltip();
        layerMeasure.getSource().clear();
    })

    // Tool - Legend
    $("#toolLegend").click(function () {
        ShowMapLegend();
    })

    // Tool - Full Extent
    $("#toolFullExtent").click(function () {
        map.getView().setCenter(centerCoordinate);
        map.getView().setZoom(zoomLevel);
    })
}

function fullScreen() {
    isfullscreen = true;

    var docElm = document.documentElement;
    if (docElm.requestFullscreen) {
        docElm.requestFullscreen();
    }
    else if (docElm.mozRequestFullScreen) {
        docElm.mozRequestFullScreen();
    }
    else if (docElm.webkitRequestFullScreen) {
        docElm.webkitRequestFullScreen();
    }
}

function exitfullscreen() {
    isfullscreen = false;

    if (document.exitFullscreen) {
        document.exitFullscreen();
    }
    else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    }
    else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
    }
}


function ShowToolbox() {
    // reset modal if it isn't visible
    if (!($('.modal.in').length)) {
        $('.modal-dialog').css({
            top: 0,
            left: 0
        });
    }
    $('#myToolbox').modal({
        backdrop: false,
        show: true
    });

    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });
}

function HideToolbox() {
    $('#myToolbox').modal('hide');
}

function ShowMapLegend() {
    // reset modal if it isn't visible
    if (!($('.modal.in').length)) {
        $('.modal-dialog').css({
            top: 0,
            left: 0
        });
    }
    $('#myMapLegend').modal({
        backdrop: false,
        show: true
    });

    $('.modal-dialog').draggable({
        handle: ".modal-header"
    });
}

function ShowDetailV2(pierecordid) {
    $('#divloadingdetail').show();
    $('#divcontentdetail').hide();
    if (!($('.modal.in').length)) {
        $('.modal-dialogdetail').css({
            top: 0,
            left: 0
        });
    }
    $('#myDetail').modal({
        backdrop: false,
        show: true
    });
    $('.modal-dialogdetail').draggable({
        handle: ".modal-header"
    });

    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetPiezoRecordDetailByPieRecordID',
        data: "{PieRecordID: '" + pierecordid + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            if (json == "") {
                var content = "<div class='box-body text-center'><h3 class='box-title'>Tidak Ada Piezo Di Blok Ini</h3></div>"
                $('#divmasterdetail').html(content);
                $('#divtablehistory').html("");
                $('#divloadingdetail').hide();
                $('#divcontentdetail').show();
            }else {
                var imagesrc = "", content = "";
                if (json[0]["ImageLocation"] == "") {
                    imagesrc = "<img style='cursor:pointer;' width='75px' height='75px' src='../Image/Icon/noimage.png' onclick='PreviewImage()'/>";
                }
                else {
                    imagesrc = "<img style='cursor:pointer;' width='75px' height='75px' src='http://172.30.1.122/PZOService/uploads/" + json[0]["estCode"] + "/" + json[0]["ImageLocation"] + "' onclick='PreviewImage()'/>";
                }

                globalimgurl = json[0]["estCode"] + "/" + json[0]["ImageLocation"];
                console.log(json);

                if (json.length > 0) {
                    $('#lblpiezorecordid').text("PiezoRecordID : " + json[0]["PieRecordID"]);

                    content = '<div class="col-md-7"><div class="row">';
                    content += '<div class="col-md-4">PiezoRecordID</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["PieRecordID"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Longitude</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["Longitude"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Latitude</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["Latitude"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Accuracy</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["Accuracy"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Is Active</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["IsActive"] + '</div></div></div>';
                    content += '<div class="col-md-5" style="text-align: center">' + imagesrc + '</div>';
                    $('#divmasterdetail').html(content);

                    content = '<table id="tablehistory" class="table table-striped table-bordered" style="width: 100%">';
                    content += '<thead><tr>';
                    content += '<th>Week</th><th>Tanggal</th><th>Ketinggian</th><th>Kondisi</th><th>Klasifikasi</th>';
                    content += '</tr></thead><tbody>';



                    for (var i = 0; i < json.length; i++) {
                        content += '<tr>';
                        content += "<td align='center'>" + json[i]["Week"] + "</td><td align='center'>" + json[i]["DateUpload2"] + "</td><td align='center'>" + json[i]["Ketinggian"] + "</td><td align='center'>" + json[i]["Kondisi"] + "</td><td align='center'>" + json[i]["Klasifikasi"] + "</td></tr>";
                    }

                    content += '</tbody></table>';
                }

                $('#divtablehistory').html(content);
                $('#tablehistory').DataTable({
                    dom: 'Bfrtip',
                    pageLength: 5,
                    buttons: [
                        'copy', 'csv', 'excel'
                    ],
                    "order": [[1, "desc"]],
                    "pagingType": "full_numbers"
                });
                setUpChart(json,0);
                $("#allData").click(function () {
                    setUpChart(json, 0);
                });
                $("#thn").click(function () {
                    setUpChart(json, 12);
                });
                $("#6bln").click(function () {
                    setUpChart(json, 6);
                });
                $("#3bln").click(function () {
                    setUpChart(json, 3);
                });
                $('#divloadingdetail').hide();
                $('#divcontentdetail').show();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetPiezoRecordDetailByPieRecordID : " + xhr.statusText);
        }
    });
}

function ShowDetail(pierecordid) {
    $("#divinfo").html("");
    $('#divloadingdetail').show();
    // reset modal if it isn't visible
    if (!($('.modal.in').length)) {
        $('.modal-dialogdetail').css({
            top: 0,
            left: 0
        });
    }
    $('#myDetail').modal({
        backdrop: false,
        show: true
    });

    $('.modal-dialogdetail').draggable({
        handle: ".modal-header"
    });
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetPiezoRecordDetailByPieRecordID',
        data: "{PieRecordID: '" + pierecordid + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            var imagesrc = "";
            if (json[0]["ImageLocation"] == "") {
                imagesrc = "<img style='cursor:pointer;' width='75px' height='75px' src='../Image/Icon/noimage.png' onclick='PreviewImage()'/>";
            }
            else {
                imagesrc = "<img style='cursor:pointer;' width='75px' height='75px' src='http://172.30.1.122/PZOService/uploads/" + mapped_estate + "/" + json[0]["ImageLocation"] + "' onclick='PreviewImage()'/>";
            }

            globalimgurl = json[0]["ImageLocation"];
            var contentinfo = "<br/><div style='position:absolute; top:60px;right:50px;width:70px;height:80px;'>" + imagesrc + "</div>"
                                    + "<div style='width:400px;font-family:Calibri; font-size:13px; background-color:#42A867'>" + json[0]["PieRecordID"] + "</div>"
                                    + "<table style='width:400px;font-family:Calibri; font-size:13px;'>"
                                    + "<tr><td style='width:75px;'>Longitude</td><td> : " + json[0]["Longitude"] + "</td></tr>"
                                    + "<tr><td>Latitude</td><td> : " + json[0]["Latitude"] + "</td></tr>"
                                    + "<tr><td>Accuracy</td><td> : " + json[0]["Accuracy"] + "</td></tr>"
                                    + "<tr><td>IsActive</td><td> : " + json[0]["IsActive"] + "</td></tr>"
                                    + "</table><br/>"
                                    + "<div style='width:400px;font-family:Calibri; font-size:13px; background-color:#42A867'>Data History</div>"
                                    + "<br/><div style='max-height:300px; overflow:auto'><table width='100%' cellpadding='3' cellspacing='0' border='1' style='font-family:Calibri; font-size:13px;' >"
                                    + "<tr bgcolor='#42A867'><th>Week</th><th>Tanggal</th><th>Ketinggian</th><th>Kondisi</th><th>Klasifikasi</th></tr>";

            for (var i = 0; i < json.length; i++) {
                if (i % 2 == 0)
                    contentinfo += "<tr bgcolor='#D1FFD9'>";
                else
                    contentinfo += "<tr bgcolor='#ABF0A1'>";

                contentinfo += "<td align='center'>" + json[i]["Week"] + "</td><td align='center'>" + json[i]["DateUpload"] + "</td><td align='center'>" + json[i]["Ketinggian"] + "</td><td align='center'>" + json[i]["Kondisi"] + "</td><td align='center'>" + json[i]["Klasifikasi"] + "</td></tr>";
            }

            contentinfo += "</table></div><br/>";
            $("#divinfo").html(contentinfo);
            $('#divloadingdetail').hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetPiezoRecordDetailByPieRecordID : " + xhr.statusText);
        }
    });


    
}

function Measure() {
    pointerMoveHandler = function (evt) {
        if (evt.dragging) {
            return;
        }
        /** @type {string} */
        var helpMsg = 'Click to start drawing';

        if (sketch) {
            var geom = (sketch.getGeometry());
            if (geom instanceof ol.geom.Polygon) {
                helpMsg = continuePolygonMsg;
            } else if (geom instanceof ol.geom.LineString) {
                helpMsg = continueLineMsg;
            }
        }

        helpTooltipElement.innerHTML = helpMsg;
        helpTooltip.setPosition(evt.coordinate);

        helpTooltipElement.classList.remove('hidden');
    };

    map.on('pointermove', pointerMoveHandler);

    map.getViewport().addEventListener('mouseout', function () {
        helpTooltipElement.classList.add('hidden');
    });

    formatLength = function (line) {
        var wgs84Sphere = new ol.Sphere(6378137);
        var length;
        var coordinates = line.getCoordinates();
        length = 0;
        var sourceProj = map.getView().getProjection();
        for (var i = 0, ii = coordinates.length - 1; i < ii; ++i) {
            var c1 = ol.proj.transform(coordinates[i], sourceProj, 'EPSG:4326');
            var c2 = ol.proj.transform(coordinates[i + 1], sourceProj, 'EPSG:4326');
            length += wgs84Sphere.haversineDistance(c1, c2);
        }
        var output;
        output = length;
        if (length > 1000) {
            output = (Math.round(length / 1000 * 100) / 100) +
                ' ' + 'km';
        } else {
            output = (Math.round(length * 100) / 100) +
                ' ' + 'm';
        }
        return output;


        //var length = ol.Sphere.getLength(line);
        //var output;
        ////if (length > 100) {
        ////    output = (Math.round(length / 1000 * 100) / 100) +
        ////        ' ' + 'km';
        ////} else {
        ////    output = (Math.round(length * 100) / 100) +
        ////        ' ' + 'm';
        ////}
        //output = length * 100000 + ' ' + 'm';
        //return output;
    };

    formatArea = function (polygon) {
        var wgs84Sphere = new ol.Sphere(6378137);
        var area;
        var sourceProj = map.getView().getProjection();
        var geom = /** @type {ol.geom.Polygon} */(polygon.clone().transform(
            sourceProj, 'EPSG:4326'));
        var coordinates = geom.getLinearRing(0).getCoordinates();
        area = Math.abs(wgs84Sphere.geodesicArea(coordinates));

        var output;
        if (area > 10000) {
            output = (Math.round(area / 10000 * 100) / 100) +
                ' ' + 'ha';
        } else {
            output = (Math.round(area * 100) / 100) +
                ' ' + 'm<sup>2</sup>';
        }
        return output;
        //var area = ol.Sphere.getArea(polygon);
        //var output;
        ////if (area > 10000) {
        ////    output = (Math.round(area / 1000000 * 100) / 100) +
        ////        ' ' + 'km<sup>2</sup>';
        ////} else {
        ////    output = (Math.round(area * 100) / 100) +
        ////        ' ' + 'm<sup>2</sup>';
        ////}
        //output = area * 1000000 * 1.3 + ' ' + 'ha';
        //return output;
    };

    addMeasureInteraction();
}

function addMeasureInteraction() {
    var type = $('#selectMeasureMode').val();
    draw = new ol.interaction.Draw({
        source: sourceLayerMeasure,
        type: type,
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.2)'
            }),
            stroke: new ol.style.Stroke({
                color: 'rgba(0, 0, 0, 0.5)',
                lineDash: [10, 10],
                width: 2
            }),
            image: new ol.style.Circle({
                radius: 5,
                stroke: new ol.style.Stroke({
                    color: 'rgba(0, 0, 0, 0.7)'
                }),
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                })
            })
        })
    });
    map.addInteraction(draw);

    createMeasureTooltip();
    createHelpTooltip();

    var listener;
    draw.on('drawstart',
        function (evt) {
            // set sketch
            sketch = evt.feature;

            /** @type {ol.Coordinate|undefined} */
            var tooltipCoord = evt.coordinate;

            listener = sketch.getGeometry().on('change', function (evt) {
                var geom = evt.target;
                var output;
                if (geom instanceof ol.geom.Polygon) {
                    output = formatArea(geom);
                    tooltipCoord = geom.getInteriorPoint().getCoordinates();
                } else if (geom instanceof ol.geom.LineString) {
                    output = formatLength(geom);
                    tooltipCoord = geom.getLastCoordinate();
                }
                measureTooltipElement.innerHTML = output;
                measureTooltip.setPosition(tooltipCoord);
            });
        }, this);

    draw.on('drawend',
        function () {
            measureTooltipElement.className = 'tooltip tooltip-static';
            measureTooltip.setOffset([0, -7]);
            // unset sketch
            sketch = null;
            // unset tooltip so that a new one can be created
            measureTooltipElement = null;
            createMeasureTooltip();
            ol.Observable.unByKey(listener);
        }, this);
}

function createHelpTooltip() {
    if (helpTooltipElement) {
        helpTooltipElement.parentNode.removeChild(helpTooltipElement);
    }
    helpTooltipElement = document.createElement('div');
    helpTooltipElement.className = 'tooltip hidden';
    helpTooltip = new ol.Overlay({
        element: helpTooltipElement,
        offset: [15, 0],
        positioning: 'center-left'
    });
    map.addOverlay(helpTooltip);
}


/**
 * Creates a new measure tooltip
 */
function createMeasureTooltip() {
    if (measureTooltipElement) {
        measureTooltipElement.parentNode.removeChild(measureTooltipElement);
    }
    measureTooltipElement = document.createElement('div');
    measureTooltipElement.className = 'tooltip tooltip-measure';
    measureTooltip = new ol.Overlay({
        element: measureTooltipElement,
        offset: [0, -15],
        positioning: 'bottom-center'
    });
    map.addOverlay(measureTooltip);
}

//image preview
function PreviewImage() {
    var img = new Image();
    var bcgDiv = document.getElementById("divBackground");
    var imgDiv = document.getElementById("divImage");
    var imgFull = document.getElementById("imgFull");
    var imgLoader = document.getElementById("imgLoader");
    imgLoader.style.display = "block";
    img.onload = function () {
        imgFull.src = img.src;
        imgFull.style.display = "block";
        imgLoader.style.display = "none";
    };

    if (globalimgurl == "") {
        img.src = "../Image/Icon/noimage.png";
    }
    else {
        img.src = "http://172.30.1.122/PZOService/uploads/"+ globalimgurl;
    }

    var width = document.body.clientWidth;
    if (document.body.clientHeight > document.body.scrollHeight) {
        bcgDiv.style.height = document.body.clientHeight + "px";
    }
    else {
        bcgDiv.style.height = document.body.scrollHeight + "px";
    }
    imgDiv.style.left = (width - 650) / 2 + "px";
    imgDiv.style.top = "20px";
    bcgDiv.style.width = "100%";

    bcgDiv.style.display = "block";
    imgDiv.style.display = "block";
    return false;
}
function HidePreview() {
    var bcgDiv = document.getElementById("divBackground");
    var imgDiv = document.getElementById("divImage");
    var imgFull = document.getElementById("imgFull");
    if (bcgDiv != null) {
        bcgDiv.style.display = "none";
        imgDiv.style.display = "none";
        imgFull.style.display = "none";
    }
}



