﻿var startDate, endDate;
var currDay, currMonth, currYear;
var arrSelectedWeek;
var arrWeek = [];
var actionTableUpload = [];
var actionTableButton = [];

$(function () {
    $('.selectpicker').selectpicker({});
    $("#periode").datepicker();
    $("#periodeExcel").datepicker();
    ListCompany();
    ListWeek('MER');

    var d = new Date();
    currDay = d.getDate();
    currMonth = d.getMonth() + 1;
    currYear = d.getFullYear();

    //Date range picker with time picker


    $("#selectFilterCompany").change(function () {
        ListEstate($("#selectFilterCompany").val());
    })

    $("#selectFilterCompanyExcel").change(function () {
        ListEstate($("#selectFilterCompanyExcel").val());
    })

    $("#btnsaveFileUpload").click(function () {
        $(".loading").show();
        fasterPreview(this);
        var _idPict = this.files && this.files.length ? this.files[0].name.split('.')[0] : '';
        var _ext = $('#fileUpload')[0].value.split('.')[1];
        var FileName = _idPict + "." + _ext
        //validateFileUpload(_idPict);
    });

    $("#btnsaveFileUploadExcel").click(function () {
        $(".loading").show();
        fasterPreview(this);
        var _idPict = this.files && this.files.length ? this.files[0].name.split('.')[0] : '';
        var _ext = $('#fileUploadExcel')[0].value.split('.')[1];
        var FileName = _idPict + "." + _ext
        validateFileUploadExcel(_idPict);
    });
    $("#btnSearchHOBODoc").click(function () {
        SearchHOBODoc($("#selectFilterCompany option:selected").val(), $("#selectFilterPeriodeHOBO option:selected").text(), $("#selectFilterYearHOBO option:selected").val());
    });
    $("#btnSearchExcelDoc").click(function () {
        SearchExcelDoc($("#selectFilterCompanyExcel option:selected").val(), $("#selectFilterPeriodeExcel option:selected").text(), $("#selectFilterYearExcel option:selected").val());
    });
    $("#btnDownloadReport").click(function () {
        if ($("#selectFilterCompany option:selected").val() != "")
        {
            $(".loading").show();
            TestDulu($("#selectFilterCompany option:selected").val(), $("#selectFilterPeriodeReport option:selected").val(), $("#selectFilterYearHOBO option:selected").val());
        }
        else
        {
            $.alert({
                title: 'Alert!',
                content: 'Mohon pilih company',
            });
        }

    });

});


function ListCompany() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListCompanyKLHK',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataSubText = response.d[2].split(";");
            var dataLength = response.d[0].split(";").length;

            $('#selectFilterCompany').find('option').remove();
            $('#selectFilterCompanyExcel').find('option').remove();
            $("#selectFilterCompany").append($('<option>', {
                value: "",
                text: "Choose Company"
            }));
            $("#selectFilterCompanyExcel").append($('<option>', {
                value: "",
                text: "Choose Company"
            }));
            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterCompany").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
                $("#selectFilterCompanyExcel").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterCompany").val("").change();
            $("#selectFilterCompanyExcel").val("").change();
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListCompany : " + xhr.statusText);
        }
    });
}

function ListEstate(companycode) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListLogger',
        data: '{CompanyCode: "' + companycode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            $('#selectFilterBlock').find('option').remove();
            $("#selectFilterBlock").append($('<option>', {
                value: "",
                text: "Choose Logger"
            }));

            json.forEach(function (obj) {
                $("#selectFilterBlock").append($('<option>', {
                    value: obj.Block,
                    text: obj.Block
                }));
            })
            $("#selectFilterBlock").val("");

            $('#selectFilterBlockExcel').find('option').remove();
            $("#selectFilterBlockExcel").append($('<option>', {
                value: "",
                text: "Choose Logger"
            }));

            json.forEach(function (obj) {
                $("#selectFilterBlockExcel").append($('<option>', {
                    value: obj.Block,
                    text: obj.Block
                }));
            })
            $("#selectFilterBlockExcel").val("");
            //GenerateTableData();


            $('.selectpicker').selectpicker('refresh');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListEstate : " + xhr.statusText);
        }
    });
}
function ListWeek(estcode) {

    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListMonth',
        data: '{Estcode: "' + estcode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            $('#selectFilterPeriodeHOBO').find('option').remove();
            $("#selectFilterPeriodeHOBO").append($('<option>', {
                value: "",
                text: "Select Month"
            }));
            json.forEach(function (obj) {
                $("#selectFilterPeriodeHOBO").append($('<option>', {
                    value: obj.value,
                    text: obj.Text
                }));
            })
            $("#selectFilterPeriodeHOBO").val("");

            $('#selectFilterPeriodeExcel').find('option').remove();
            $("#selectFilterPeriodeExcel").append($('<option>', {
                value: "",
                text: "Select Month"
            }));
            json.forEach(function (obj) {
                $("#selectFilterPeriodeExcel").append($('<option>', {
                    value: obj.value,
                    text: obj.Text
                }));
            })
            $("#selectFilterPeriodeExcel").val("");
            $('.selectpicker').selectpicker('refresh');

        }
    });
}

function fasterPreview(uploader) {
    if (uploader.files && uploader.files[0]) {
        $profileImage.attr('src',
            window.URL.createObjectURL(uploader.files[0]));
    }
}

function validateFileUpload(_idPict, logger) {
    var fuData = document.getElementById(_idPict);
    var filename = fuData.files[0].name;
    var FileUploadPath = fuData.value;

    if (FileUploadPath == '') {
        $.alert({
            title: 'Alert!',
            content: 'Please upload an file',
        });

    } else {
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();



        if (Extension == "hobo") {


            if (fuData.files && fuData.files[0]) {

                var size = fuData.files[0].size;

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                //saveDataUpload(filename, Extension, logger, _idPict);
                //}
            }

        }


        else if (Extension == "xlsx") {

            if (fuData.files && fuData.files[0]) {

                var size = fuData.files[0].size;

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);
                $.ajax({
                    type: 'POST',
                    url: '../Service/MapService.asmx/GetUserID',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        userID = response.d;
                        saveDataUploadExcel(filename, Extension, userID, logger, _idPict);
                    }
                });
                //}
            }
        }
    }
}

function validateFileUploadExcel(_idPict) {
    var fuData = document.getElementById('fileUploadExcel');
    var FileUploadPath = fuData.value;

    if (FileUploadPath == '') {
        $.alert({
            title: 'Alert!',
            content: 'Please upload an file',
        });
    } else {
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();



        if (Extension == "xlsx") {


            if (fuData.files && fuData.files[0]) {

                var size = fuData.files[0].size;

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);

                $.ajax({
                    type: 'POST',
                    url: '../Service/MapService.asmx/GetUserID',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        userID = response.d;
                        saveDataUploadExcel(_idPict, Extension, userID);
                    }
                });
                //}
            }

        }


        else {
            $.alert({
                title: 'Alert!',
                content: 'File only allows file types of Excel.',
            });
        }
    }
}

function saveDataUpload(filenames, Extension, logger, _idPict) {
    var FileName = filenames;
    //var filename = _idPict[0];
    var filename = $('#'+ _idPict)[0];
    var periode = $("#selectFilterPeriodeHOBO option:selected").val();
    //const timestampSeconds = Math.round((new Date()).getTime() / 1000);
    //console.log(timestampSeconds);
    var _msg = $("#selectFilterCompany option:selected").val() + "_" + logger + "_" + $("#selectFilterPeriodeHOBO option:selected").text() + "_" + $("#selectFilterYearHOBO option:selected").val();
    var files = filename.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(FileName, files[i]);

    }
    $.ajax({
        type: 'POST',
        url: '../Service/ImportHandler.ashx?filename=' + _msg + '&periode=' + periode + '&year=' + $("#selectFilterYearHOBO option:selected").val(),
        data: data,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response == 'Success') {

                $.ajax({
                    type: 'POST',
                    url: '../Service/MapService.asmx/SaveUploadPath',
                    data: "{'FileName' :'" + _msg + '.' + Extension + "','Path' :'" + _msg + '.' + Extension + "', 'companyCode' : '" + $("#selectFilterCompany option:selected").val() + "', 'logger' : '" + logger + "', 'periode' : '" + $("#selectFilterPeriodeHOBO option:selected").text() + "', 'year' : '" + $("#selectFilterYearHOBO option:selected").val() + "'}",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        //$saveButton.html("Save");
                        if (response.d == 'success') {
                            //history.go(0); //forward
                            $(".loading").hide();
                            $.alert({
                                title: 'Alert!',
                                content: 'Upload file HOBO success',
                            });
                            SearchHOBODoc($("#selectFilterCompany option:selected").val(), $("#selectFilterPeriodeHOBO option:selected").text(), $("#selectFilterYearHOBO option:selected").val());
                            
                        } else {
                            //_setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            } else {
                //_setCustomFunctions.showPopup('Info', 'Failed to save');
            }
        }

    });
}

function saveDataUploadExcel(filenames, Extension, userID, logger, _idPict) {
    var FileName = filenames
    var filename = $('#' + _idPict)[0];
    //const timestampSeconds = Math.round((new Date()).getTime() / 1000);
    //console.log(timestampSeconds);
    var _msg = $("#selectFilterCompany option:selected").val() + "_" + logger + "_" + $("#selectFilterPeriodeHOBO option:selected").text() + "_" + $("#selectFilterYearHOBO option:selected").val();
    var files = filename.files;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(FileName, files[i]);

    }
    $.ajax({
        type: 'POST',
        url: '../Service/ImportHandlerExcel.ashx?filename=' + _msg + '&userID=' + userID + '&logger=' + logger,
        data: data,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response == 'Success') {

                $.ajax({
                    type: 'POST',
                    url: '../Service/MapService.asmx/SaveUploadPathExcel',
                    data: "{'FileName' :'" + _msg + '.' + Extension + "','Path' :'" + _msg + '.' + Extension + "', 'companyCode' : '" + $("#selectFilterCompany option:selected").val() + "', 'logger' : '" + logger + "', 'periode' : '" + $("#selectFilterPeriodeHOBO option:selected").text() + "', 'year' : '" + $("#selectFilterYearHOBO option:selected").val() + "'}",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        //$saveButton.html("Save");
                        if (response.d == 'success') {
                            //history.go(0); //forward
                            $(".loading").hide();
                            $.alert({
                                title: 'Alert!',
                                content: 'Upload file Excel success',
                            });
                            SearchHOBODoc($("#selectFilterCompany option:selected").val(), $("#selectFilterPeriodeHOBO option:selected").text(), $("#selectFilterYearHOBO option:selected").val())
                        } else {
                            //_setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            } else {
                alert(response);
                history.go(0);
            }
        }

    });
}

function SearchHOBODoc(companycode, periode, year) {
    $(".loading").show();
    companycode = companycode.split('.')[1];
    var currData = [];
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDoc',
        data: '{CompanyCode: "' + companycode + '", Periode: "' + periode + '", Year: "' + year + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            console.log(json.length);
            dataJsonMI = json;
            var _status = [];
            var _date;
            var _year;
            var _month;
            var _logger;
            var content = '<table id="tableHOBODoc" class="table table-striped table-bordered table-text-center" style="width:100%">';
            content += '<thead><tr>';
            content += '<th>Logger</th><th>Status</th><th style="width:190px !important">Upload</th><th>Download HOBO</th><th>Download Excel</th><th>Detail Excel</th></tr></thead><tbody>';
            actionTableUpload = [];
            for (var i = 0; i < json.length; i++) {
                var idY = "fileUpload_" + i;
                var idZ = "btnsaveFileUpload_" + i;
                _logger = json[i]["Block"].toString();

                content += '<tr id="' + json[i]["CompanyCode"] + '">';
                content += '<td class="logger">' + json[i]["Block"] + '</td>';
                //content += '<td id="PieRecordID">' + json[i]["CompanyCode"] + '</td>';
                content += '<td> <div class="col-md-12"  style="padding-left: 0px;"><div class="col-md-8">HOBO : </div><div class="col-md-4">' + (json[i]["StatusHOBO"] == 'true' ? '<span class="bg-green">&#10004;</span>' : '<span class="bg-red">&#10006;</span>') + '</div></div>  <div class="col-md-12"  style="padding-left: 0px;"><div class="col-md-8">Excel : </div><div class="col-md-4">' + (json[i]["StatusExcel"] == 'true' ? '<span class="bg-green">&#10004;</span>' : '<span class="bg-red">&#10006;</span>') + '</div></div> ';
                content += '<td style="width:190px"><div class="col-md-12"><div class="col-md-6" style="margin-top: 5px;"><input id="fileUpload_' + i + '" type="file" name="upload_file_' + i + '" required="" accept=".xlsx,.hobo"></div><div class="col-md-6"><a type="button" id=' + idZ + ' class="btn btn-success pull-right">Save</a></div></div></td>';
                content += '<td><a href="../../' + json[i]["PhysicalPathHOBO"] + '"><img src="../Image/toolbar/Download.png" style=height:20px;width:20px></a></td>';
                content += '<td><a href="../../' + json[i]["PhysicalPathExcel"] + '"><img src="../Image/toolbar/Download.png" style=height:20px;width:20px></a></td>';
                content += '<td><img src="../Image/toolbar/Fullscreen-32.png" style=height:20px;width:20px id="test' + _logger + '" onclick="detailData(' + '\'' + _logger + '\',' + '\'' + year + '\',' + '\'' + periode + '\');" value=' + _logger + '></td>';
                //content += '<td><i class="fa fa-pencil-square" aria-hidden="true"></i></td>';
                content += '</tr>';

                actionTableUpload.push({ idUpload: idY, idButton: idZ, logger: _logger });
            }

            content += '</tbody></table>';

            $("#divTableHOBODoc").html(content);
            var length = actionTableUpload.length;
            for (var i = 0 ; i < length; i++) {
                $("#" + actionTableUpload[i].idButton).click(function () {
                    $(".loading").show();
                    
                    var id = $(this)[0].id;
                    for (var ml = 0 ; ml < length; ml++) {
                        if(id == actionTableUpload[ml].idButton)
                        {
                            var logger = actionTableUpload[ml].logger;
                            var idUpload = actionTableUpload[ml].idUpload;
                            //alert(logger)
                            var _idPict = this.files && this.files.length ? this.files[0].name.split('.')[0] : '';
                            validateFileUpload(idUpload, logger);
                        }
                    }
                    //arrayKebun = arrayKebun.filter(function (elem, index, self) {
                    //    return index === self.indexOf(elem);

                    //});
                    //arrayApplikasi = arrayApplikasi.filter(function (elem, index, self) {
                    //    return index === self.indexOf(elem);

                    //});
                    //_setCustomFunctions.showConfirmPopup(2, datas, 'Info', 'Are You Sure To Unregistered ?');
                });
            }
            $('#tableHOBODoc').DataTable({
                dom: 'lfrtip',
                lengthMenu: [[12, 15, 24, 48, -1], [12, 15, 24, 48, "All"]],
                pageLength: 12,
                destroy: true,
                stateSave: true,
                "columnDefs": [{
                    "searchable": false,
                    "class": "left",
                    "targets": 4,
                    width: 150
                },
                {
                    "searchable": false,
                    "class": "left",
                    "targets": 5,
                    width: 100
                }]
            });

            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetDataKLHK : " + xhr.statusText);
        }
    });
}

function SearchExcelDoc(companycode, periode, year) {
    $(".loading").show();
    var currData = [];
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetExcelDoc',
        data: '{CompanyCode: "' + companycode + '", Periode: "' + periode + '", Year: "' + year + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            console.log(json.length);
            dataJsonMI = json;
            var _status = [];
            var _date;
            var _year;
            var _month;
            var _logger;
            var content = '<table id="TableExcelDoc" class="table table-striped table-bordered table-text-center" style="width:100%">';
            content += '<thead><tr>';
            content += '<th>ID Perusahaan</th><th>Logger</th><th>Periode</th><th>Tahun</th><th>Detail Data</th><th>Action</th></tr></thead><tbody>';

            for (var i = 0; i < json.length; i++) {
                _year = json[i]["Year"].toString();
                _month = json[i]["Periode"].toString();
                _logger = json[i]["Logger"].toString();

                content += '<tr>';
                content += '<td id="' + json[i]["CompanyCode"] + '">' + json[i]["CompanyCode"] + '</td>';
                content += '<td>' + json[i]["Logger"] + '</td>';
                //content += '<td id="PieRecordID">' + json[i]["CompanyCode"] + '</td>';
                content += '<td>' + json[i]["Periode"] + '</td>';
                content += '<td>' + json[i]["Year"] + '</td>';
                content += '<td><img src="../Image/toolbar/Fullscreen-32.png" style=height:20px;width:20px id="test' + _logger + '" onclick="detailData(' + '\'' + _logger + '\',' + '\'' + _year + '\',' + '\'' + _month + '\');" value=' + _logger + '></td>';
                content += '<td><a href="..' + json[i]["PhysicalPath"] + '"><img src="../Image/toolbar/Download.png" style=height:20px;width:20px></a></td>';
                //content += '<td><i class="fa fa-pencil-square" aria-hidden="true"></i></td>';
                content += '</tr>';
            }

            content += '</tbody></table>';

            $("#divTableExcelDoc").html(content);
            $('#TableExcelDoc').DataTable({
                dom: 'lfrtip',
                lengthMenu: [[12, 15, 24, 48, -1], [12, 15, 24, 48, "All"]],
                pageLength: 12,
                destroy: true,
                stateSave: true,
            });

            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetDataKLHK : " + xhr.statusText);
        }
    });
}


function detailData(value, year, month) {
    $('#mdlImportData').modal('toggle');
    SearchDetailExcel(value, year, month)
}



function SearchDetailExcel(value, year, month) {
    $(".loading").show();
    var currData = [];
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetExcelDetail',
        data: '{logger: "' + value + '", Periode: "' + month + '", Year: "' + year + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            console.log(json.length);
            dataJsonMI = json;
            var _status = [];
            var _date;
            var _year;
            var _month;
            var _logger;
            var content = '<table id="TableExcelDetailDoc" class="table table-striped table-bordered table-text-center" style="width:100%">';
            content += '<thead><tr>';
            content += '<th>Logger</th><th>Tanggal</th><th>TMAT</th><th>Created By</th><th>Created Date</th></tr></thead><tbody>';

            for (var i = 0; i < json.length; i++) {
                _year = json[i]["Year"];
                _month = json[i]["Periode"];
                _logger = json[i]["Logger"];

                content += '<tr id="' + json[i]["CompanyCode"] + '">';
                content += '<td>' + json[i]["Logger"] + '</td>';
                //content += '<td id="PieRecordID">' + json[i]["CompanyCode"] + '</td>';
                content += '<td>' + json[i]["Date"] + '</td>';
                content += '<td>' + json[i]["TMAT"] + '</td>';
                content += '<td>' + json[i]["username"] + '</td>';
                content += '<td>' + json[i]["CreatedDate"] + '</td>';
                content += '</tr>';
            }

            content += '</tbody></table>';

            $("#divTableDetailExcelDoc").html(content);
            $('#TableExcelDetailDoc').DataTable({
                dom: 'lfrtip',
                lengthMenu: [[12, 15, 24, 48, -1], [12, 15, 24, 48, "All"]],
                pageLength: 12,
                destroy: true,
                stateSave: true,
            });

            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetDataKLHK : " + xhr.statusText);
        }
    });

}

function TestDulu(companycode, periode, year) {

    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/DownloadReportHOBO',
        data: '{CompanyCode: "' + companycode + '",Periode: "' + periode + '", Year: "' + year + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response.d == 'success') {
                window.location.href = '../../Service/DownloadReportHOBO.ashx?CompanyCode=' + companycode + '&Periode=' + periode + '&Year=' + year
                $(".loading").hide();
            }
            else
            {
                $.alert({
                    title: 'Alert!',
                    content: response.d,
                });
                $(".loading").hide();
            }
        }
    });
}