﻿
var actionTableMasterEdit = [];
var actionTableMasterDelete = [];
$(function () {

    $("#btnAddHobo").click(function () {
        document.getElementById('btnUpdate').style.display = 'none';
        document.getElementById('btnSave').style.display = 'inline';
        $('#mdlAddData').modal('toggle');
    });
    ListEstate();
    $("#selectFilterEstate").change(function () {
        ListAfdeling($("#selectFilterEstate").val());
    });
    $("#selectFilteringEstate").change(function () {
        ListAfdeling($("#selectFilteringEstate").val());
    });
    $("#selectFilteringAfdeling").change(function () {
        ListBlock($("#selectFilteringAfdeling").val());
    });
    $("#selectFilterAfdeling").change(function () {
        ListBlock($("#selectFilterAfdeling").val());
    });
    $("#selectFilteringBlock").change(function () {
        ListTMAT($("#selectFilteringBlock").val());
    });
    $("#selectFilterBlock").change(function () {
        ListTMAT($("#selectFilterBlock").val());
    });

    $("#btnSave").click(function () {
        SaveDataHOBO(1)
    });
    $("#btnUpdate").click(function () {
        SaveDataHOBO(2)
    });
    $("#btnSearch").click(function () {
        var estcode = $('#selectFilteringEstate').val();
        var afd = $('#selectFilteringAfdeling').val();
        var block = $('#selectFilteringBlock').val();
        ListMasterHOBO(estcode, afd, block);
    });
    ListMasterHOBO($('#selectFilteringEstate').val(), $('#selectFilteringAfdeling').val(), $('#selectFilteringBlock').val());
});

function ListEstate() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListAllEstate',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            $('#selectFilterEstate').find('option').remove();
            $('#selectFilteringEstate').find('option').remove();
            $("#selectFilterEstate").append($('<option>', {
                value: "",
                text: "Choose Estate"
            }));
            $("#selectFilteringEstate").append($('<option>', {
                value: "",
                text: "All Estate"
            }));
            json.forEach(function (obj) {
                $("#selectFilterEstate").append($('<option>', {
                    value: obj.EstCode,
                    text: obj.NewEstName
                }));

                $("#selectFilteringEstate").append($('<option>', {
                    value: obj.EstCode,
                    text: obj.NewEstName
                }));
            })

            $("#selectFilteringEstate").val("").change();
            $("#selectFilterEstate").val("").change();
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListEstate : " + xhr.statusText);
        }
    });
}
function ListAfdeling(estcode) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListAfdeling',
        data: '{EstCode: "' + estcode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            $('#selectFilteringAfdeling').find('option').remove();
            $('#selectFilterAfdeling').find('option').remove();
            $("#selectFilteringAfdeling").append($('<option>', {
                value: "",
                text: "All Afdeling"
            }));
            $("#selectFilterAfdeling").append($('<option>', {
                value: "",
                text: "Choose Afdeling"
            }));
            json.forEach(function (obj) {
                $("#selectFilteringAfdeling").append($('<option>', {
                    value: obj.value,
                    text: obj.Name
                }));

                $("#selectFilterAfdeling").append($('<option>', {
                    value: obj.value,
                    text: obj.Name
                }));
            })

            

                $("#selectFilteringAfdeling").val("").change();
            $("#selectFilterAfdeling").val("").change();
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListAfdeling : " + xhr.statusText);
        
        }
    });
}

function ListBlock(afd) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListBlockTMAT',
        data: '{afdeling: "' + afd + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            $('#selectFilteringBlock').find('option').remove();
            $('#selectFilterBlock').find('option').remove();
            $("#selectFilteringBlock").append($('<option>', {
                value: "",
                text: "All Block"
            }));            
            $("#selectFilterBlock").append($('<option>', {
                value: "",
                text: "Choose Block"
            }));
            json.forEach(function (obj) {
                $("#selectFilteringBlock").append($('<option>', {
                    value: obj.value,
                    text: obj.Name
                }));

                $("#selectFilterBlock").append($('<option>', {
                    value: obj.value,
                    text: obj.Name
                }));
            })

            $("#selectFilteringBlock").val("").change();
            $("#selectFilterBlock").val("").change();
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListBlock : " + xhr.statusText);
        }
    });
}

function ListTMAT(block) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListTMAT',
        data: '{block: "' + block + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            $('#selectFilterTMAT').find('option').remove();
            $("#selectFilterTMAT").append($('<option>', {
                value: "",
                text: "Choose TMAT"
            }));
            json.forEach(function (obj) {
                $("#selectFilterTMAT").append($('<option>', {
                    value: obj.value,
                    text: obj.Name
                }));
            })

            $("#selectFilterTMAT").val("").change();
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListTMAT : " + xhr.statusText);
        }
    });
}

function SaveDataHOBO(type) {
    if (type == 1) {
        var data = {
            EstCode: $('#selectFilterEstate').val(), Afdeling: $('#selectFilterAfdeling').val(),
            Block: $('#selectFilterBlock').val(), KodeTMAT: $('#selectFilterTMAT').val(),
            Merk: $('input[name=merk]').val(), Type: $('input[name=type]').val(),
            SerialNumber: $('input[name=serialnumber]').val(), Kondisi: $('input[name=kondisi]').val(),
            Baterai: $('input[name=baterai]').val(), lastCheck: $('input[name=lastcheck]').val(),
            Remark: $('input[name=remark]').val()

        };
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/SaveHOBO',
            data: JSON.stringify({ dataobject: JSON.stringify(data) }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                if (response.d == 'success') {
                    //$table.DataTable().ajax.reload();
                    $('#mdlAddData').modal('hide');
                    $.alert({
                        title: 'Alert!',
                        content: 'Master HOBO has been save succesfully',
                    });
                    $('input[name=codeHidden]').val('');
                    $("#selectFilterAfdeling").val('');
                    $("#selectFilterBlock").val('');
                    $('#selectFilterTMAT').val('');
                    $('input[name=merk]').val('');
                    $('input[name=type]').val('');
                    $('input[name=serialnumber]').val('');
                    $('input[name=kondisi]').val('');
                    $('input[name=baterai]').val('');
                    $('input[name=remark]').val('');
                    $("#selectFilterEstate").val('');
                    $('#lastcheck').val('');


                    $('.selectpicker').selectpicker('refresh');
                    ListMasterHOBO($('#selectFilteringEstate').val(), $('#selectFilteringAfdeling').val(), $('#selectFilteringBlock').val());
                } else {
                    $.alert({
                        title: 'Alert!',
                        content: 'Failed to save',
                    });

                    $('input[name=codeHidden]').val('');
                    $("#selectFilterAfdeling").val('');
                    $("#selectFilterBlock").val('');
                    $("#selectFilterTMAT").val('');
                    $('input[name=merk]').val('');
                    $('input[name=type]').val('');
                    $('input[name=serialnumber]').val('');
                    $('input[name=kondisi]').val('');
                    $('input[name=baterai]').val('');
                    $('input[name=remark]').val('');
                    $("#selectFilterEstate").val('');
                    $('#lastcheck').val('');

                    $('.selectpicker').selectpicker('refresh');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });
    }
    else
    {
        var data = {
            ID: $('input[name=codeHidden]').val(), EstCode: $('#selectFilterEstate').val(), Afdeling: $('#selectFilterAfdeling').val(),
            Block: $('#selectFilterBlock').val(), KodeTMAT: $('#selectFilterTMAT').val(),
            Merk: $('input[name=merk]').val(), Type: $('input[name=type]').val(),
            SerialNumber: $('input[name=serialnumber]').val(), Kondisi: $('input[name=kondisi]').val(),
            Baterai: $('input[name=baterai]').val(), lastCheck: $('input[name=lastcheck]').val(),
            Remark: $('input[name=remark]').val()

        };
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/EditHOBO',
            data: JSON.stringify({ dataobject: JSON.stringify(data) }),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                if (response.d == 'success') {
                    //$table.DataTable().ajax.reload();
                    $('#mdlAddData').modal('hide');
                    $.alert({
                        title: 'Alert!',
                        content: 'Master HOBO has been update succesfully',
                    });
                    ListMasterHOBO($('#selectFilteringEstate').val(), $('#selectFilteringAfdeling').val(), $('#selectFilteringBlock').val());
                } else {
                    $.alert({
                        title: 'Alert!',
                        content: 'Failed to update',
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
            }
        });

    }
}
function ListMasterHOBO(estcode, afd, block) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetMasterHOBO',
        data: '{EstCode: "' + estcode + '", Afd: "' + afd + '", Block: "' + block + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            console.log(json.length);
            dataJsonMI = json;
            var _status = [];
            var content = '<table id="tableMasterHOBO" class="table table-striped table-bordered table-text-center" style="width:100%">';
            content += '<thead><tr>';
            content += '<th>EstCode</th><th>Afdeling</th><th>Block</th><th>Kode TMAT</th><th>Merk</th><th>Type</th><th>Serial Number</th><th>Kondisi</th><th>Baterai</th><th>Last check</th><th>Remarks</th><th>Edit</th><th>Delete</th></tr></thead><tbody>';
            actionTableMasterEdit = [];
            actionTableMasterDelete = [];
            for (var i = 0; i < json.length; i++) {
                var idY = "btnEdit_" + i;
                var idZ = "btnDelete_" + i;
                var id = json[i]["IDMaster"];
                content += '<tr id="' + json[i]["IDMaster"] + '">';
                //content += '<td id="PieRecordID">' + json[i]["CompanyCode"] + '</td>';
                content += '<td style="display:none;">' + json[i]["ID"] + '</td>';
                content += '<td>' + json[i]["EstCode"] + '</td>';
                content += '<td>' + json[i]["Afdeling"] + '</td>';
                content += '<td>' + json[i]["Block"] + '</td>';
                content += '<td>' + json[i]["KodeTMAT"] + '</td>';
                content += '<td>' + json[i]["Merk"] + '</td>';
                content += '<td>' + json[i]["Type"] + '</td>';
                content += '<td>' + json[i]["SerialNumber"] + '</td>';
                content += '<td>' + json[i]["Kondisi"] + '</td>';
                content += '<td>' + json[i]["Baterai"] + '</td>';
                content += '<td>' + json[i]["Lastcheck"] + '</td>';
                content += '<td>' + json[i]["Remarks"] + '</td>';
                content += '<td><a type="button" id=' + idY + ' class="btn btn-success pull-right">Edit</a></td>';
                content += '<td><a type="button" id=' + idZ + ' class="btn btn-danger pull-right">Delete</a></td>';
                content += '</tr>';
                actionTableMasterEdit.push({ idMaster: id, idButtonEdit: idY });
                actionTableMasterDelete.push({ idMaster: id, idButtonDelete: idZ });
            }
            actionTableMasterDelete = actionTableMasterDelete.filter(function (elem, index, self) {
                return index === self.indexOf(elem);
            });
            content += '</tbody></table>';

            $("#divtblmasterhobo").html(content);


            var length = actionTableMasterEdit.length;
            for (var i = 0 ; i < actionTableMasterEdit.length; i++) {
                $("#" + actionTableMasterEdit[i].idButtonEdit).click(function () {
                    //$(".loading").show();

                    var ids = $(this)[0].id;
                    for (var ml = 0 ; ml < length; ml++) {
                        if (ids == actionTableMasterEdit[ml].idButtonEdit) {
                            //alert(actionTableMasterEdit[ml].idMaster);
                            getDataMaster(actionTableMasterEdit[ml].idMaster);
                        }
                    }
                });
            }
            var length1 = actionTableMasterDelete.length;
            for (var j = 0 ; j < actionTableMasterDelete.length; j++) {
                $("#" + actionTableMasterDelete[j].idButtonDelete).click(function () {
                    //$(".loading").show();

                    var id = $(this)[0].id;
                    for (var mn = 0 ; mn < actionTableMasterDelete.length; mn++) {
                        if (id == actionTableMasterDelete[mn].idButtonDelete) {
                            deleteMaster(actionTableMasterDelete[mn].idMaster);
                        }
                    }
                });
            }
            $('#tableMasterHOBO').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[12, 15, 24, 48, -1], [12, 15, 24, 48, "All"]],
                pageLength: 12,
                destroy: true,
                stateSave: true,
                "aoColumnDefs": [
                { "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13] }, 
                { "bSearchable": false, "aTargets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13] }
                ],
                "columnDefs": [{
                    "class": "center",
                    "targets": 0,
                    width: 50
                }, {
                    "class": "left",
                    "targets": 1,
                    width: 50
                }, {
                    "class": "center",
                    "targets": 2,
                    width: 50
                }, {
                    "class": "center",
                    "targets": 3,
                    width: 50
                }, {
                    "class": "center",
                    "targets": 4,
                    width: 50
                }, {
                    "class": "center",
                    "targets": 5,
                    width: 50
                }, {
                    "class": "center",
                    "targets": 6,
                    width: 50
                }, {
                    "class": "center",
                    "targets": 7,
                    width: 50
                }, {
                    "class": "center",
                    "targets": 8,
                    width: 50
                }, {
                    "class": "center",
                    "targets": 9,
                    width: 50
                }, {
                    "class": "center",
                    "targets": 10,
                    width: 50
                }],
                buttons: [
                    'copy',
                    {
                        extend: 'csvHtml5',
                        title: '',
                        fieldBoundary: '',
                        header: false,
                        footer: false
                    },
                    {
                        extend: 'excelHtml5',
                        title: '',
                        exportOptions: {
                            columns: [0, 3, 4, 5, 14, 8, 9, 12]
                        }
                    }, {
                        extend: 'pdfHtml5',
                        title: '',
                        exportOptions: {
                            columns: [0, 3, 4, 5, 14, 8, 9, 12]
                        }
                    },
                    'print'
                ],
                fnPreDrawCallback: function (oSettings) {
                    /* reset currData before each draw*/
                    //currData = [];
                },
                fnDrawCallback: function () {
                    //console.log(currData)
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    var info = $(this).DataTable().page.info();
                    var page = info.page;
                    var length = info.length;
                    var index = (page * length + (iDisplayIndex + 1));
                    //$('td:eq(0)', nRow).html(index);
                    ////currData.push(aData);
                }
            });

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetDataKLHK : " + xhr.statusText);
        }

    });
}
function getDataMaster(id)
{
    document.getElementById('btnUpdate').style.display = 'inline';
    document.getElementById('btnSave').style.display = 'none';
    $.ajax({
        url: '../Service/MapService.asmx/GetMasterHoboData',
        type: "POST",
        data: "{'ID' :'" + id + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var json = $.parseJSON(data.d);
            json.forEach(function (obj) {
                $('input[name=codeHidden]').val(obj.IDMaster);
                $("#selectFilterAfdeling").val(obj.Afdeling);
                $("#selectFilterBlock").val(obj.Block);
                $("#selectFilterTMAT").val(obj.KodeTMAT);
                $('input[name=merk]').val(obj.Merk);
                $('input[name=type]').val(obj.Type);
                $('input[name=serialnumber]').val(obj.SerialNumber);
                $('input[name=kondisi]').val(obj.Kondisi);
                $('input[name=baterai]').val(obj.Baterai);
                $('input[name=remark]').val(obj.Remarks);
                $("#selectFilterEstate").val(obj.EstCode);
                //$("#lastcheck").val(obj.Lastcheck);
                //$('input[name=lastcheck]').val(obj.Lastcheck);
                $('#lastcheck').val(obj.Lastcheck);


                $('.selectpicker').selectpicker('refresh');

                $('#mdlAddData').modal('toggle');
            })
        },
        failure: function (msg) {
            alert("Something go wrong! ");
        }
    });
}

function deleteMaster(id) {
    var data = { ID: id };
    $.confirm({
        title: 'Confirm!',
        content: 'Apakah anda yakin untuk hapus data',
        buttons: {
            confirm: function () {
                $.ajax({
                    type: 'POST',
                    url: '../Service/MapService.asmx/DeleteHOBO',
                    data: JSON.stringify({ dataobject: JSON.stringify(data) }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        if (response.d == 'success') {
                            $.alert({
                                title: 'Alert!',
                                content: 'Master HOBO has been delete succesfully',
                            });
                            ListMasterHOBO($('#selectFilteringEstate').val(), $('#selectFilteringAfdeling').val(), $('#selectFilteringBlock').val());
                        } else {
                            $.alert({
                                title: 'Alert!',
                                content: 'Failed to Delete',
                            });
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            cancel: function () {
                $(".loading").hide();
            }
        }
    });
}
