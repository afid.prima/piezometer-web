﻿//Map Basic Variable Declaration
var map, latLong, centerCoordinate, zoomLevel, mapProjection;

//Map Control Declaration
var scaleLineControl, mousePositionControl, overviewMapControl, zoomSliderControl;

//Map Layer Declaration
var layerBase, layerBlock, layerPiezometer, layerInfo, layerDrawing, sourceLayerDrawing, layerMeasure, sourceLayerMeasure, layerHighlight;

var listyear, listmonth, listweek, listallwmarea;
var year, month, monthname, week, monthnamealias, idweek;

//Variable for Event Handler
var isfullscreen;

//Variable for Toolbox
var draw;
var extentCoordinate;
var sketch, helpTooltipElement, helpTooltip, measureTooltipElement, measureTooltip, continuePolygonMsg = 'Click to continue drawing the polygon',
    continueLineMsg = 'Click to continue drawing the line', pointerMoveHandler, formatLength, formatArea, listener;
var isCheckZonaTrue;

//Variable For Piezometer
var listWMArea, area, userid, global_selectblock;

var global_jsoncompanyestate;
$(function () {
    //Inisialisasi Checkbox dan RadioButton
    //$('input').iCheck({
    //    checkboxClass: 'icheckbox_futurico',
    //    radioClass: 'iradio_square',
    //    increaseArea: '20%' // optional
    //});
    $(".loading").hide();

    //For Event Handler
    isfullscreen = false;

    //Variable Declaration
    area = "";
    userid = $('#userid').val();
    listyear = $('#listyear').val();
    listmonth = $('#listmonth').val();
    listweek = JSON.parse($('#listweek').val());
    year = $('#year').val();
    month = $('#month').val();
    monthname = $('#monthname').val();
    week = $('#week').val();
    monthnamealias = $('#monthnamealias').val();
    idweek = $('#idweek').val();
    extentCoordinate = $("#extentCoordinate").val();


    $("#toolsprevdate").click(function () {

        idweek--;
        for (var i = 0; i < listweek.length; i++) {
            if (idweek == listweek[i]['ID']) {
                year = listweek[i]["Year"];
                month = listweek[i]["Month"];
                week = listweek[i]["Week"];
                monthnamealias = listweek[i]["MonthName"];
            }
        }


        $('#lbldate').html("Week " + week + ", " + monthnamealias + " " + year);
        GetKetinggianPiezometerByZona();
    })

    $("#toolsnextdate").click(function () {
        idweek++;
        for (var i = 0; i < listweek.length; i++) {
            if (idweek == listweek[i]['ID']) {
                year = listweek[i]["Year"];
                month = listweek[i]["Month"];
                week = listweek[i]["Week"];
                monthnamealias = listweek[i]["MonthName"];
            }
        }

        $('#lbldate').html("Week " + week + ", " + monthnamealias + " " + year);
        GetKetinggianPiezometerByZona();
    })

    $("#btnGo").click(function () {
        setLocalStorage();
        window.location.href = "PZO_MapAnalysis.aspx";

        $("#loader").show();

    });

    //$("#selectFilterYear").change(function () {
    //    GetListMonth($("#selectFilterYear").val());
    //})

    //$("#selectFilterMonth").change(function () {
    //    GetListWeek($("#selectFilterYear").val(), $("#selectFilterMonth").val());
    //})
    
    //MAIN FUNCTION
    $('#lbldate').html("Week " + week + ", " + monthnamealias + " " + year);
    if (localStorage.getItem("pzoolwmvalue") == null || localStorage.getItem("pzoolwmvalue") == undefined) {
        setLocalStorage();
    }
    getListWMArea();
    WebserviceGetCompanyEstate();
    $('#titleEstate').html(localStorage.getItem("pzoolwmvalue").split("*")[1]);

    //GetListYear();
    //GetListMonth($("#selectFilterYear").val());
    //GetListWeek($("#selectFilterYear").val(), $("#selectFilterMonth").val());


    WebserviceGetParit(); // Get Parit
    //WebserviceGetMasterAsset(); //Get Layer Asset
    WebserviceGetParitBy(); // Ambil Data Info Table
    SetMap();
    SetLayerBase();
    SetLayer();

    //Zoom To Full Extent
    //Extent Kebun / Company
    let coor1 = [parseFloat(localStorage.getItem("pzoolwmvalue").split("*")[3]), parseFloat(localStorage.getItem("pzoolwmvalue").split("*")[4])];
    let coor2 = [parseFloat(localStorage.getItem("pzoolwmvalue").split("*")[5]), parseFloat(localStorage.getItem("pzoolwmvalue").split("*")[6])];
    let coorxm1 = ol.proj.fromLonLat(coor1);
    let coorxm2 = ol.proj.fromLonLat(coor2);

    global_extentindonesia = [coorxm1[0], coorxm1[1], coorxm2[0], coorxm2[1]];
    let extcoor = ol.extent.boundingExtent([[global_extentindonesia[0], global_extentindonesia[1]], [global_extentindonesia[2], global_extentindonesia[3]]]);
    map.getView().fit(extcoor, map.getSize());
    global_zoomextent = map.getView().getZoom();
    global_centerextent = [parseFloat(map.getView().getCenter()[0]), parseFloat(map.getView().getCenter()[1])];

    HandlerMap();
    HandlerPiezo();
    setupPiezoAnalysis();
})

function ListZonaByWmArea(idWmArea) {
    var json = $.parseJSON(extentCoordinate);

    //console.log('json response ListZonaByWmArea =  ' + JSON.stringify(json));
    $('#selectFilterZona').find('option').remove();
    for (var i = 0; i < json.length; i++) {
        if (json[i]["idWMArea"] == idWmArea)
            $("#selectFilterZona").append($('<option>', {
                value: json[i]["WMA_code"],
                text: json[i]["WMA_code"]
            }));
        $('.selectpicker').selectpicker('refresh');
    }

}

function SetMap() {
    let scalelinecontrol = new ol.control.ScaleLine({ units: 'metric', target: document.getElementById('scale-line') });
    let mousepositioncontrol = new ol.control.MousePosition({
        coordinateFormat: ol.coordinate.createStringXY(5),
        projection: 'EPSG:4326',
        target: document.getElementById('mouse-position'),
        undefinedHTML: '&nbsp;'
    });

    let overlay = new ol.Overlay({
        element: document.getElementById('popup'),
        autoPan: true,
        autoPanAnimation: {
            duration: 250
        }
    });

    let coordinateExtent = global_extentindonesia;
    viewmap = new ol.View({
        projection: 'EPSG:3857',
        extent: coordinateExtent
    });

    map = new ol.Map({
        controls: ol.control.defaults({
            attributionOptions: ({
                collapsible: false
            })
        }).extend([
            scalelinecontrol, mousepositioncontrol, new ol.control.ZoomSlider()
        ]),
        logo: false,
        target: 'map',
        layers: [],
        overlays: [overlay],
        view: viewmap
    });

    //map.on('pointermove', pointerMoveHandler);
    //map.getViewport().addEventListener('mouseout', function () {
    //    if (!$("#panel-measure").is(":hidden")) {
    //        helpTooltipElement.classList.add('hidden');
    //        //alert();
    //    }
    //});

    //Click Untuk Info
    //map.on('click', MapClick);
}

function MapClick(event) {

}


function MapUpdateSize() {
    $("#map").css({ 'height': $(window).height() - 57 + 'px' });
    map.updateSize();
}

function setLocalStorage() {
    area = "PT.THIP";
    if (localStorage.getItem("pzoolwmvalue") != null) {
        area = $("#selectfiltercompany").val()
    }
	
	if(global_jsoncompanyestate == undefined){
		localStorage.setItem("pzoolwmvalue", "all*PT.THIP*TH3A - THP1*102.88*0.056*103.298*0.345");
		return;
	}
	
    let filter = global_jsoncompanyestate.filter(d => d.companyCode == area);
    let MinXPT = 0;
    let MinYPT = 0;
    let MaxXPT = 0;
    let MaxYPT = 0;
    var ListCompanyEstate = filter[0]["ListCompanyEstate"];
    for (var i = 0; i < ListCompanyEstate.length; i++) {
        if (i == 0) {
            MinXPT = ListCompanyEstate[i]["MinXPT"];
            MinYPT = ListCompanyEstate[i]["MinYPT"];
            MaxXPT = ListCompanyEstate[i]["MaxXPT"];
            MaxYPT = ListCompanyEstate[i]["MaxYPT"];
        } else {
            if (MinXPT > ListCompanyEstate[i]["MinXPT"]) {
                MinXPT = ListCompanyEstate[i]["MinXPT"];
            }
            if (MinYPT > ListCompanyEstate[i]["MinYPT"]) {
                MinYPT = ListCompanyEstate[i]["MinYPT"];
            }
            if (MaxXPT < ListCompanyEstate[i]["MaxXPT"]) {
                MaxXPT = ListCompanyEstate[i]["MaxXPT"];
            }

            if (MaxYPT < ListCompanyEstate[i]["MaxYPT"]) {
                MaxYPT = ListCompanyEstate[i]["MaxYPT"];
            }
        }
    }

    let value = "all";
    value += "*" + filter[0]["companyCode"];
    value += "*" + filter[0]["estate"];
    value += "*" + MinXPT;
    value += "*" + MinYPT;
    value += "*" + MaxXPT;
    value += "*" + MaxYPT;
	localStorage.setItem("pzoolwmvalue", value);
}

function WebserviceGetCompanyEstate() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceAnalysis.asmx/GetCompanyEstate',
        data: JSON.stringify({
            userid: userid
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (response) {
            if (response.d.includes("Error : ")) {
                alert(response.d);
            } else {
                var data1 = JSON.parse(response.d);
                var data = JSON.parse(response.d);
                data = _.uniq(data, x => x.companyCode);
                $("#selectfiltercompany").find('option').remove();
                for (var i = 0; i < data.length; i++) {
                    var lanjut = false;
                    for (var ii = 0; ii < listWMArea.length; ii++) {
                        if (data[i]["companyCode"] == listWMArea[ii]["CompanyCode"]) {
                            lanjut = true;
                            break;
                        }
                    }

                    if (lanjut) {
                        var arr = [];
                        for (var j = 0; j < data1.length; j++) {
                            if (data[i]["companyCode"] == data1[j]["companyCode"]) {
                                arr.push(data1[j]);
                            }
                        }
                        data[i]["ListCompanyEstate"] = arr;

                        if (arr.length > 0) {
                            $('#selectfiltercompany').append($('<option>', {
                                value: data[i]["companyCode"],
                                text: data[i]["companyCode"] + " - " + data[i]["CompanyName"]
                            }));
                        }
                    }
                }

                global_jsoncompanyestate = data;
                if (localStorage.getItem("pzoolwmvalue") != null) {
                    $('#selectfiltercompany').val(localStorage.getItem("pzoolwmvalue").split("*")[1]);
                }
                $('.selectpicker').selectpicker('refresh');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Maaf terjadi kesalahan GetListCompanyEstate.");
        }
    });
}

function getListWMArea() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/ListWMArea',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (response) {
            var json = $.parseJSON(response.d);
            listWMArea = $.parseJSON(response.d);
            $('#selectFilterWMArea').find('option').remove();
            for (var i = 0; i < json.length; i++) {
                if (json[i]["CompanyCode"] == localStorage.getItem("pzoolwmvalue").split("*")[1]) {
                    $("#selectFilterWMArea").append($('<option>', {
                        value: json[i]["idWMArea"],
                        text: json[i]["wmAreaName"]
                    }));
                }
            }
            $("#selectFilterWMArea").val($('#idwm').val()).change();
            $('.selectpicker').selectpicker('refresh');
            ListZonaByWmArea($("#selectFilterWMArea").val());
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListWMArea : " + xhr.statusText);
        }
    });
}
