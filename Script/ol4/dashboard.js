﻿//////////////////////////////////////////////////////////
///////////////// REGION ONLOAD ///////////////////
//////////////////////////////////////////////////////////
var selectedCompany = "", selectedEstate = "", selectedCompanyForSubChart = "", selectedCompanyShortname = "", labelUnit = "";
var canvas, ctx;
var myBar, barChartConfig, barChartData, myOtherBar, otherBarChartConfig, otherBarChartData;
var myChartDataPengukuran;
var flagValue = 0;
var today = new Date();;
var uid = $("#userid").val();
$(function () {

	Init();
	//$("#btnCloseSubChart").hide();
	//Variable 
	//document.getElementById("liheader").innerText = $();
	$('#divChartPiezometer').html('<canvas id="chartPiezometer" style="height: 180px;"></canvas>')

	canvas = document.getElementById('chartPiezometer');
	ctx = canvas.getContext('2d');

	$('.selectpicker').selectpicker({});
	//Flat green color scheme for iCheck
	$('input[type="radio"].flat-green').iCheck({
		radioClass: 'iradio_flat-green'
	})

    $("#btnGenerateData").click(function () {
        $(".loading").show();
        generateDataReport(flagValue);
    });

	ListCompany();
	
	var date = today.getDate();
	if (date < 10) {
		date = '0' + date;
	}
	var month = today.getMonth() + 1;
	if (month < 10) {
		month = '0' + month;
	}
	var minutes = today.getMinutes();
	if (minutes < 10) {
		minutes = '0' + minutes;
	}
	var hours = today.getHours();
	if (hours < 10) {
		hours = '0' + hours;
	}
	var time = date + "/" + month + "/" + today.getFullYear() + "/" + hours + ":" + minutes /*":" + today.getSeconds()*/;
	$('#lblTime').text(time);
	$("#btnCloseSubChart").click(function () {
		labelUnit = "All Estate";
		ShowChartCustomProgress("", "", flagValue);
		ShowChartDataPengukuran_Custom("", "", flagValue, "");

		$("#dialog").dialog("close");

		$('#mainchart').show();
		$('#subchart').hide();
		//$("#btnCloseSubChart").hide();
	})
})

//////////////////////////////////////////////////////////
///////////////// REGION CUSTOM METHOD ///////////////////
//////////////////////////////////////////////////////////

function Init() {
	//POPUP DIALOG
	$("#dialog").dialog();
	$("#dialog").dialog("close");
	if (uid != null || uid != "") {
		if (uid == "Guest") {
			document.getElementById("liDownloadData").style.display = "none";
		}
	} else {
		alert('false');
		document.getElementById("liDownloadData").style.display = "none";
	}
}

function ShowChartDefaultProgress(companycode, estcode) {
	//alert('masuk sini gak methodnya?');
	$(".loading").show();
	$.ajax({
		type: 'POST',
		//url: '../Service/MapService.asmx/GetProgressForDashboard',
		//data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '"}',
		url: '../Service/MapService.asmx/GetDataProgressForDashboard',
		data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", AdjustmentValue: "' + flagValue + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var json = $.parseJSON(response.d);

			if (json.length > 0) {
				//Show Chart Data Pengukuran
				ShowChartDataPengukuran("", "", flagValue, "")

				$('#lblWeekName').text(json[0].WeekName);
				var tmpWeekName = json[0].MonthName.split(",");
				var only_week = tmpWeekName[1].split("");
				var fixedWeek = only_week[2];
				$('#lblDateRange').text("Week" + " " + +fixedWeek + " : " + json[0].DateRange);

				var labelname = [];
				var data_recorded = [], data_unrecorded = [], data_percbanjir = [], data_perctergenang = [], data_percagaktergenang = [],
					data_percnormal = [], data_percagakkering = [], data_perckering = [], data_percrusakhilang = [], data_percrecorded = [], data_percunrecorded = [];
				var dataset = [];

				var goalcompletion_data = "";
				var arr_percentage = [];
				var goalcompletion_percentage;
				for (var i = 0; i < json.length; i++) {
					labelname.push(json[i].LabelName);
					data_recorded.push(json[i].Recorded);
					data_unrecorded.push(json[i].NoData);
					data_percbanjir.push(json[i].PercBanjir);
					data_perctergenang.push(json[i].PercA);
					data_percagaktergenang.push(json[i].PercB);
					data_percnormal.push(json[i].PercC);
					data_percagakkering.push(json[i].PercD);
					data_perckering.push(json[i].PercE);
					data_percrusakhilang.push(json[i].PercRusakHilang);
					data_percrecorded.push(json[i].PercRecorded);
					data_percunrecorded.push(json[i].PercNoData);

					//Goal Completion
					goalcompletion_percentage = parseFloat(json[i].Recorded) / parseFloat(json[i].TotalData) * 100;
					color_string = "";
					if (Math.floor(goalcompletion_percentage) > 90) {
						color_string = "#005ce6";
					} else if (Math.floor(goalcompletion_percentage) < 70) {
						color_string = "#e6e600";
					} else if (Math.floor(goalcompletion_percentage) >= 70 && Math.floor(goalcompletion_percentage) <= 90) {
						color_string = "#ff751a";
					}
					arr_percentage.push({
						LabelName: json[i].LabelName,
						data_recorded: json[i].Recorded,
						data_unrecorded: json[i].NoData,
						data_percbanjir: json[i].PercBanjir,
						data_perctergenang: json[i].PercA,
						data_percagaktergenang: json[i].PercB,
						data_percnormal: json[i].PercC,
						data_percagakkering: json[i].PercD,
						data_perckering: json[i].PercE,
						data_percrusakhilang: json[i].PercRusakHilang,
						data_percrecorded: json[i].PercRecorded,
						data_percunrecorded: json[i].PercNoData,
						goalcompletion_percentage: goalcompletion_percentage,
						color_string: color_string,
						TotalData: json[i].TotalData,
					});
				}
				arr_percentage.sort(function (a, b) {
					return customSort(a.goalcompletion_percentage, b.goalcompletion_percentage) || customSort(a.LabelName, b.LabelName)
				});
				for (var i = 0; i < arr_percentage.length; i++) {
					goalcompletion_data += '<div class="progress-group">';
					goalcompletion_data += '<span class="progress-text">' + arr_percentage[i].LabelName + '</span>';
					//goalcompletion_data += '<span class="progress-number">' + ($('#rbByPiezoMaster').is(":checked") ? json[i].Recorded : json[i].PercRecorded) + '/<b>' + ($('#rbByPiezoMaster').is(":checked") ? arr_percentage[i].TotalData : 100) + '</b></span>';
					goalcompletion_data += '<span class="progress-number">' + ($('#rbByPiezoMaster').is(":checked") ? arr_percentage[i].data_recorded : arr_percentage[i].data_percrecorded) + '/<b>' + ($('#rbByPiezoMaster').is(":checked") ? arr_percentage[i].TotalData : 100) + '</b></span>';
					goalcompletion_data += '<div class="progress sm">';
					goalcompletion_data += '<div class="progress-bar progress-bar-aqua" style="width: ' + arr_percentage[i].goalcompletion_percentage + '%; background-color: ' + arr_percentage[i].color_string + '"></div>';
					goalcompletion_data += '</div></div>';
				}


				$('#goalcompletion').html(goalcompletion_data);

				if ($('#rbSummaryKondisi').is(":checked")) {
					dataset.push({
						label: 'Banjir',
						backgroundColor: "#000000", data: data_percbanjir
					})

					dataset.push({
						label: 'Tergenang',
						backgroundColor: "#2f74b7", data: data_perctergenang
					})

					dataset.push({
						label: 'Agak Tergenang',
						backgroundColor: "#01b0f3", data: data_percagaktergenang
					})

					dataset.push({
						label: 'Normal',
						backgroundColor: "#008000", data: data_percnormal
					})

					dataset.push({
						label: 'Agak Kering',
						backgroundColor: "#FFFF00", data: data_percagakkering
					})

					dataset.push({
						label: 'Kering',
						backgroundColor: "#FF0000", data: data_perckering
					})

					dataset.push({
						label: 'Rusak/Hilang',
						backgroundColor: "#808080", data: data_percrusakhilang
					})

					dataset.push({
						label: 'No Data',
						backgroundColor: "#c0c0c0", data: data_percunrecorded
					})

				}
				else {
					dataset.push({
						label: 'Recorded',
						backgroundColor: "#00ff00", data: $('#rbByPiezoMaster').is(":checked") ? data_recorded : data_percrecorded
					})

					dataset.push({
						label: 'No Data',
						backgroundColor: "#ff0000", data: $('#rbByPiezoMaster').is(":checked") ? data_unrecorded : data_percunrecorded
					})
				}

				barChartData = {
					labels: labelname,
					datasets: dataset
				};

				barChartConfig = {
					type: 'bar',
					data: barChartData,
					options: {
						tooltips: {
							mode: 'label',
							callbacks: {
								label: function (tooltipItem, data) {
									return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
								}
							}
						},
						responsive: true,
						scales: {
							xAxes: [{
								stacked: true,
								barThickness: 20
							}],
							yAxes: [{
								stacked: true
							}]
						}
					}
				}

				myBar = new Chart(ctx, barChartConfig);

				document.getElementById("chartPiezometer").onclick = function (evt) {
					var activePoints = myBar.getElementsAtEvent(evt);
					var firstPoint = activePoints[0];
					var secondPoint = activePoints[1];
					var label = myBar.data.labels[firstPoint._index];
					var value = myBar.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
					var value_2 = myBar.data.datasets[secondPoint._datasetIndex].data[secondPoint._index];
					//alert(label + ": " + value + " - " + value_2);

					var company = (label.substring(0, 4) === "THIP" ? "PT.THIP" : ("PT." + label.replace("-", "")));
					var keterangan = (label.substring(0, 4) === "THIP" ? label : "");
					labelUnit = label;
					selectedCompanyForSubChart = (label.substring(0, 4) === "THIP" ? "PT.THIP" : ("PT." + label.replace("-", "")));
					selectedCompanyShortname = (label.substring(0, 4) === "THIP" ? label : "");
					ShowSubChartCustomProgress(company, "", keterangan);
					ShowChartDataPengukuran_Custom(company, "", flagValue, selectedCompanyShortname);

					$("#btnCloseSubChart").show();
					$('#mainchart').hide();
					ShowChartSumberData();
					$('#subchart').show();
				};

			}
			else {
				alert("No Data");
			}
			$(".loading").hide();

		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.statusText);
		}
	});
}

function ShowChartCustomProgress(companycode, estcode, adjust) {
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/MapService.asmx/GetDataProgressForDashboard',
		data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", AdjustmentValue: "' + adjust + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var json = $.parseJSON(response.d);

			if (json.length > 0) {
				$('#divChartPiezometer').html('');
				$('#divChartPiezometer').html('<canvas id="chartPiezometer" style="height: 180px;"></canvas>')
				canvas = document.getElementById('chartPiezometer');
				ctx = canvas.getContext('2d');

				$('#lblWeekName').text(json[0].WeekName);
				var tmpWeekName = json[0].MonthName.split(",");
				var only_week = tmpWeekName[1].split("");
				var fixedWeek = only_week[2];
				//$('#lblDateRange').text("Range: " + json[0].DateRange);
				$('#lblDateRange').text("Week" + " " + fixedWeek + " : " + json[0].DateRange);

				var labelname = [];
				var data_recorded = [], data_unrecorded = [], data_percbanjir = [], data_perctergenang = [], data_percagaktergenang = [],
					data_percnormal = [], data_percagakkering = [], data_perckering = [], data_percrusakhilang = [], data_percrecorded = [], data_percunrecorded = [];
				var dataset = [];
				var arr_percentage = [];
				var goalcompletion_data = "";
				var goalcompletion_percentage;
				var color_string;
				for (var i = 0; i < json.length; i++) {
					labelname.push(json[i].LabelName);
					data_recorded.push(json[i].Recorded);
					data_unrecorded.push(json[i].NoData);
					data_percbanjir.push(json[i].PercBanjir);
					data_perctergenang.push(json[i].PercA);
					data_percagaktergenang.push(json[i].PercB);
					data_percnormal.push(json[i].PercC);
					data_percagakkering.push(json[i].PercD);
					data_perckering.push(json[i].PercE);
					data_percrusakhilang.push(json[i].PercRusakHilang);
					data_percrecorded.push(json[i].PercRecorded);
					data_percunrecorded.push(json[i].PercNoData);

					//Goal Completion
					goalcompletion_percentage = parseFloat(json[i].Recorded) / parseFloat(json[i].TotalData) * 100;
					if (Math.floor(goalcompletion_percentage) > 90) {
						color_string = "#005ce6";
					} else if (Math.floor(goalcompletion_percentage) < 70) {
						color_string = "#e6e600";
					} else if (Math.floor(goalcompletion_percentage) >= 70 && Math.floor(goalcompletion_percentage) <= 90) {
						color_string = "#ff751a";
					}
					arr_percentage.push({
						LabelName: json[i].LabelName,
						data_recorded: json[i].Recorded,
						data_unrecorded: json[i].NoData,
						data_percbanjir: json[i].PercBanjir,
						data_perctergenang: json[i].PercA,
						data_percagaktergenang: json[i].PercB,
						data_percnormal: json[i].PercC,
						data_percagakkering: json[i].PercD,
						data_perckering: json[i].PercE,
						data_percrusakhilang: json[i].PercRusakHilang,
						data_percrecorded: json[i].PercRecorded,
						data_percunrecorded: json[i].PercNoData,
						goalcompletion_percentage: goalcompletion_percentage,
						color_string: color_string,
						TotalData: json[i].TotalData,
					});
				}
				//arr_percentage.sort(function (a, b) {
				//    return a.goalcompletion_percentage - b.goalcompletion_percentage;
				//});
				arr_percentage.sort(function (a, b) {
					return customSort(a.goalcompletion_percentage, b.goalcompletion_percentage) || customSort(a.LabelName, b.LabelName)
				})
				//if (goalcompletion_percentage < 100) {
				//    arr_percentage.sort(function (a, b) {
				//        return a.goalcompletion_percentage - b.goalcompletion_percentage;
				//    }); 
				//} else {
				//    arr_percentage.sort();
				//}                         
				for (var i = 0; i < arr_percentage.length; i++) {
					goalcompletion_data += '<div class="progress-group">';
					goalcompletion_data += '<span class="progress-text">' + arr_percentage[i].LabelName + '</span>';
					//goalcompletion_data += '<span class="progress-number">' + ($('#rbByPiezoMaster').is(":checked") ? arr_percentage[i].data_recorded : arr_percentage[i].data_percrecorded) + '/<b>' + ($('#rbByPiezoMaster').is(":checked") ? json[i].TotalData : 100) + '</b></span>';
					goalcompletion_data += '<span class="progress-number">' + ($('#rbByPiezoMaster').is(":checked") ? arr_percentage[i].data_recorded : parseInt(arr_percentage[i].goalcompletion_percentage)) + '/<b>' + ($('#rbByPiezoMaster').is(":checked") ? arr_percentage[i].TotalData : 100) + '</b></span>';
					goalcompletion_data += '<div class="progress sm">';
					goalcompletion_data += '<div class="progress-bar progress-bar-aqua" style="width: ' + arr_percentage[i].goalcompletion_percentage + '%; background-color: ' + arr_percentage[i].color_string + '"></div>';
					goalcompletion_data += '</div></div>';
				}
				goalcompletion_data += '<label> Date';
				goalcompletion_data += '</label>';

				$('#goalcompletion').html(goalcompletion_data);

				if ($('#rbSummaryKondisi').is(":checked")) {
					dataset.push({
						label: 'Banjir',
						backgroundColor: "#000000", data: data_percbanjir
					})

					dataset.push({
						label: 'Tergenang',
						backgroundColor: "#2f74b7", data: data_perctergenang
					})

					dataset.push({
						label: 'Agak Tergenang',
						backgroundColor: "#01b0f3", data: data_percagaktergenang
					})

					dataset.push({
						label: 'Normal',
						backgroundColor: "#008000", data: data_percnormal
					})

					dataset.push({
						label: 'Agak Kering',
						backgroundColor: "#FFFF00", data: data_percagakkering
					})

					dataset.push({
						label: 'Kering',
						backgroundColor: "#FF0000", data: data_perckering
					})

					dataset.push({
						label: 'Rusak/Hilang',
						backgroundColor: "#808080", data: data_percrusakhilang
					})

					dataset.push({
						label: 'No Data',
						backgroundColor: "#c0c0c0", data: data_percunrecorded
					})

				}
				else {
					dataset.push({
						label: 'Recorded',
						backgroundColor: "#00ff00", data: $('#rbByPiezoMaster').is(":checked") ? data_recorded : data_percrecorded
					})

					dataset.push({
						label: 'No Data',
						backgroundColor: "#ff0000", data: $('#rbByPiezoMaster').is(":checked") ? data_unrecorded : data_percunrecorded
					})
				}

				barChartData = {
					labels: labelname,
					datasets: dataset
				};

				barChartConfig = {
					type: 'bar',
					data: barChartData,
					options: {
						tooltips: {
							mode: 'label',
							callbacks: {
								label: function (tooltipItem, data) {
									return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
								}
							}
						},
						responsive: true,
						scales: {
							xAxes: [{
								stacked: true,
								barThickness: 20
							}],
							yAxes: [{
								stacked: true
							}]
						}
					}
				}

				myBar = new Chart(ctx, barChartConfig);

				document.getElementById("chartPiezometer").onclick = function (evt) {
					var activePoints = myBar.getElementsAtEvent(evt);
					var firstPoint = activePoints[0];
					var secondPoint = activePoints[1];
					var label = myBar.data.labels[firstPoint._index];
					var value = myBar.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
					var value_2 = myBar.data.datasets[secondPoint._datasetIndex].data[secondPoint._index];
					//alert(label + ": " + value + " - " + value_2);

					var company = (label.substring(0, 4) === "THIP" ? "PT.THIP" : ("PT." + label.replace("-", "")));
					var keterangan = (label.substring(0, 4) === "THIP" ? label : "");
					labelUnit = label;
					selectedCompanyForSubChart = (label.substring(0, 4) === "THIP" ? "PT.THIP" : ("PT." + label.replace("-", "")));
					selectedCompanyShortname = (label.substring(0, 4) === "THIP" ? label : "");
					ShowSubChartCustomProgress(company, "", keterangan);
					ShowChartDataPengukuran_Custom(company, "", flagValue, selectedCompanyShortname);

					$("#btnCloseSubChart").show();
					$('#mainchart').hide();
					$('#subchart').show();
				};

			}
			else {
				alert("No Data For Progress");
			}

			$(".loading").hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.statusText);
		}
	});
}

function ShowSubChartCustomProgress(companycode, estcode, keterangan) {
	
	$('#lblCompanyCode').text(companycode);
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/MapService.asmx/GetDataProgressForDashboard',
		data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", AdjustmentValue: "' + flagValue + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var json = $.parseJSON(response.d);

			if (json.length > 0) {
				$('#divSubChartPiezometer').html('');
				$('#divSubChartPiezometer').html('<canvas id="subChartPiezometer" style="height: 180px;"></canvas>')
				canvas = document.getElementById('subChartPiezometer');
				ctx = canvas.getContext('2d');

				$('#lblWeekName').text(json[0].WeekName);
				$('#lblDateRange').text("Range: " + json[0].DateRange);

				var labelname = [];
				var data_recorded = [], data_unrecorded = [], data_percbanjir = [], data_perctergenang = [], data_percagaktergenang = [],
					data_percnormal = [], data_percagakkering = [], data_perckering = [], data_percrusakhilang = [], data_percrecorded = [], data_percunrecorded = [];
				var dataset = [];

				var goalcompletion_data = "";
				var arr_percentage = [];
				var goalcompletion_percentage;
				for (var i = 0; i < json.length; i++) {
					if ((companycode == "PT.THIP" && json[i].Flag == keterangan) || keterangan == "") {
						labelname.push(json[i].LabelName);
						data_recorded.push(json[i].Recorded);
						data_unrecorded.push(json[i].NoData);
						data_percbanjir.push(json[i].PercBanjir);
						data_perctergenang.push(json[i].PercA);
						data_percagaktergenang.push(json[i].PercB);
						data_percnormal.push(json[i].PercC);
						data_percagakkering.push(json[i].PercD);
						data_perckering.push(json[i].PercE);
						data_percrusakhilang.push(json[i].PercRusakHilang);
						data_percrecorded.push(json[i].PercRecorded);
						data_percunrecorded.push(json[i].PercNoData);

						//Goal Completion
						goalcompletion_percentage = parseFloat(json[i].Recorded) / parseFloat(json[i].TotalData) * 100;

						if (Math.floor(goalcompletion_percentage) > 90) {
							color_string = "#005ce6";
						} else if (Math.floor(goalcompletion_percentage) < 70) {
							color_string = "#e6e600";
						} else if (Math.floor(goalcompletion_percentage) >= 70 && Math.floor(goalcompletion_percentage) <= 90) {
							color_string = "#ff751a";
						}

						arr_percentage.push({
							LabelName: json[i].LabelName,
							data_recorded: json[i].Recorded,
							data_unrecorded: json[i].NoData,
							data_percbanjir: json[i].PercBanjir,
							data_perctergenang: json[i].PercA,
							data_percagaktergenang: json[i].PercB,
							data_percnormal: json[i].PercC,
							data_percagakkering: json[i].PercD,
							data_perckering: json[i].PercE,
							data_percrusakhilang: json[i].PercRusakHilang,
							data_percrecorded: json[i].PercRecorded,
							data_percunrecorded: json[i].PercNoData,
							goalcompletion_percentage: goalcompletion_percentage,
							color_string: color_string,
							totalData: json[i].TotalData,
						});
					}
				}
				arr_percentage.sort(function (a, b) {
					return customSort(a.goalcompletion_percentage, b.goalcompletion_percentage) || customSort(a.LabelName, b.LabelName)
				})
				for (var i = 0; i < arr_percentage.length; i++) {
					goalcompletion_data += '<div class="progress-group">';
					goalcompletion_data += '<span class="progress-text">' + arr_percentage[i].LabelName + '</span>';
					goalcompletion_data += '<span class="progress-number">' + ($('#rbByPiezoMaster').is(":checked") ? arr_percentage[i].data_recorded : arr_percentage[i].data_percrecorded) + '/<b>' + ($('#rbByPiezoMaster').is(":checked") ? arr_percentage[i].totalData : 100) + '</b></span>';
					goalcompletion_data += '<div class="progress sm">';
					goalcompletion_data += '<div class="progress-bar progress-bar-aqua" style="width: ' + goalcompletion_percentage + '%; background-color: ' + arr_percentage[i].color_string + '"></div>';
					goalcompletion_data += '</div></div>';
				}
				$('#goalcompletion').html(goalcompletion_data);

				if ($('#rbSummaryKondisi').is(":checked")) {
					dataset.push({
						label: 'Banjir',
						backgroundColor: "#000000", data: data_percbanjir
					})

					dataset.push({
						label: 'Tergenang',
						backgroundColor: "#2f74b7", data: data_perctergenang
					})

					dataset.push({
						label: 'Agak Tergenang',
						backgroundColor: "#01b0f3", data: data_percagaktergenang
					})

					dataset.push({
						label: 'Normal',
						backgroundColor: "#008000", data: data_percnormal
					})

					dataset.push({
						label: 'Agak Kering',
						backgroundColor: "#FFFF00", data: data_percagakkering
					})

					dataset.push({
						label: 'Kering',
						backgroundColor: "#FF0000", data: data_perckering
					})

					dataset.push({
						label: 'Rusak/Hilang',
						backgroundColor: "#808080", data: data_percrusakhilang
					})

					dataset.push({
						label: 'No Data',
						backgroundColor: "#c0c0c0", data: data_percunrecorded
					})

				}
				else {
					dataset.push({
						label: 'Recorded',
						backgroundColor: "#00ff00", data: $('#rbByPiezoMaster').is(":checked") ? data_recorded : data_percrecorded
					})

					dataset.push({
						label: 'No Data',
						backgroundColor: "#ff0000", data: $('#rbByPiezoMaster').is(":checked") ? data_unrecorded : data_percunrecorded
					})
				}

				otherBarChartData = {
					labels: labelname,
					datasets: dataset
				};

				otherBarChartConfig = {
					type: 'bar',
					data: otherBarChartData,
					options: {
						tooltips: {
							mode: 'label',
							callbacks: {
								label: function (tooltipItem, data) {
									return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
								}
							}
						},
						responsive: true,
						scales: {
							xAxes: [{
								stacked: true,
								barThickness: 20
							}],
							yAxes: [{
								stacked: true
							}]
						}
					}
				}

				myOtherBar = new Chart(ctx, otherBarChartConfig);


				$("#subChartPiezometer").click(function (evt) {
					var activePoints = myOtherBar.getElementsAtEvent(evt);
					var firstPoint = activePoints[0];
					var secondPoint = activePoints[1];

					var label = myOtherBar.data.labels[firstPoint._index];
					var value = myOtherBar.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
					var value_2 = myOtherBar.data.datasets[secondPoint._datasetIndex].data[secondPoint._index];
					ShowPopupChart("", label.split(' - ')[0], flagValue, selectedCompanyShortname);
				})
				//document.getElementById("subChartPiezometer").onclick = function (evt) {
				//    var activePoints = myOtherBar.getElementsAtEvent(evt);
				//    var firstPoint = activePoints[0];
				//    var secondPoint = activePoints[1];
				//    var label = myOtherBar.data.labels[firstPoint._index];
				//    var value = myOtherBar.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
				//    var value_2 = myOtherBar.data.datasets[secondPoint._datasetIndex].data[secondPoint._index];

				//    $('#popupchart_content').html('tessssssssssssss');
				//    // reset modal if it isn't visible
				//    if (!($('.modal.in').length)) {
				//        $('.modal-dialog').css({
				//            top: 0,
				//            left: 100
				//        });
				//    }
				//    $('#popupchart').modal({
				//        backdrop: false,
				//        show: true
				//    });


				//    //$('#content-popover').html('<a id="test" tabindex="0" role="button" data-html="true" data-trigger="focus" title="<b>Chart</b>" data-content="<div>' + label + '</div>"></a>')

				//    //$('#test').popover('show');
				//};
			}
			else {
				alert("No Data For Progress");
			}

			$(".loading").hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.statusText);
		}
	});
}

function ShowChartDataPengukuran(companycode, estcode, adjust, keterangan) {
	$('#divChartDataPengukuran').html('');
	$('#divChartDataPengukuran').html('<canvas id="chartDataPengukuran"></canvas>');

	$.ajax({
		type: 'POST',
		url: '../Service/MapService.asmx/GetDataPengukuranForDashboard',
		data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", AdjustmentValue: "' + adjust + '", Keterangan : "' + keterangan + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var json = $.parseJSON(response.d);
			console.log(json);

			var chartTitle = "";
			if (companycode == "" && estcode == "") {
				chartTitle = "All Estate";
			}
			else if (companycode != "" && estcode == "") {
				chartTitle = $("#selectFilterCompany option:selected").text();
			}
			else if (estcode != "") {
				chartTitle = $("#selectFilterEstate option:selected").text();
			}

			$('#lblTitleWeek').html(json[0].WeekName);
			var chartDatactxDataPengukuran_Data = {
				labels: ['Banjir', 'Tergenang', 'Agak Tergenang', 'Normal', 'Agak Kering', 'Kering', 'Rusak/Hilang', 'No Data'],
				datasets: [{
					data: [json[0]["Banjir"], json[0]["A"], json[0]["B"], json[0]["C"], json[0]["D"], json[0]["E"], json[0]["Rusak / Hilang"], json[0]["NoData"]],
					backgroundColor: [
						'#000000',
						'#2f74b7',
						'#01b0f3',
						'#008000',
						'#FFFF00',
						'#FF0000',
						'#808080',
						'#C0C0C0'
					]
				}]
			};

			var ctxDataPengukuran = document.getElementById('chartDataPengukuran').getContext('2d');
			myChartDataPengukuran = new Chart(ctxDataPengukuran, {
				type: 'doughnut',
				data: chartDatactxDataPengukuran_Data,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: "Pengukuran Piezometer " + chartTitle
					},
					animation: {
						animateScale: true,
						animateRotate: true
					}
				}
			});
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.statusText);
		}
	});
}

function ShowChartDataPengukuran_Custom(companycode, estcode, adjust, keterangan) {
	//alert('di click')
	$.ajax({
		type: 'POST',
		url: '../Service/MapService.asmx/GetDataPengukuranForDashboard',
		data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", AdjustmentValue: "' + adjust + '", Keterangan: "' + keterangan + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var json = $.parseJSON(response.d);

			if (json.length > 0) {
				var chartTitle = "";
				//if (companycode == "" && estcode == "") {
				//    chartTitle = "All Estate";
				//}
				//else if (companycode != "" && estcode == "") {
				//    chartTitle = $("#selectFilterCompany option:selected").text();
				//}
				//else if (estcode != "") {
				//    chartTitle = $("#selectFilterEstate option:selected").text();
				//}
				chartTitle = labelUnit;
				$('#lblTitleWeek').html(json[0].WeekName);
				myChartDataPengukuran.data.datasets[0].data = [json[0]["Banjir"], json[0]["A"], json[0]["B"], json[0]["C"], json[0]["D"], json[0]["E"], json[0]["Rusak / Hilang"], json[0]["NoData"]];
				myChartDataPengukuran.update();
				myChartDataPengukuran.options.title.text = "Pengukuran Piezometer " + chartTitle;
				myChartDataPengukuran.update();
			}
			else {
				if (adjust < 0) flagValue = flagValue + 1;
				else flagValue = flagValue - 1;
				alert("No Data For Data Pengukuran");
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.statusText);
		}
	});
}

function ShowChartSumberData() {
	$.ajax({
		type: 'POST',
		url: '../Service/MapService.asmx/GetDataSumberData',
		data: '{}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var json = $.parseJSON(response.d);

			$('#divChartSumberData').html('');
			$('#divChartSumberData').html('<canvas id="chartSumberData"></canvas>');

			var chartSumberData_Data = {
				labels: [json[0].DataSource, json[1].DataSource],
				datasets: [{
					data: [json[0].Jumlah, json[1].Jumlah],
					backgroundColor: [
						'green',
						'blue'
					]
				}]
			};

			var ctxSumberData = document.getElementById('chartSumberData').getContext('2d');
			var myChartSumberData = new Chart(ctxSumberData, {
				type: 'doughnut',
				data: chartSumberData_Data,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: "Sumber Data Piezometer"
					},
					animation: {
						animateScale: true,
						animateRotate: true
					}
				}
			});
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.statusText);
		}
	})
}

function ListCompany() {
	$.ajax({
		type: 'POST',
		url: '../Service/MapService.asmx/ListCompany',
		data: '{}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var dataValue = response.d[0].split(";");
			var dataText = response.d[1].split(";");
			var dataSubText = response.d[2].split(";");
			var dataLength = response.d[0].split(";").length;


			$('#selectFilterCompany').find('option').remove();
			$("#selectFilterCompany").append($('<option>', {
				value: "",
				text: "All Company"
			}));

			for (var i = 0; i < dataLength; i++) {
				$("#selectFilterCompany").append($('<option>', {
					value: dataValue[i],
					text: dataSubText[i] + " - " + dataText[i]
				}));
			}

			$("#selectFilterCompany").val("").change();
			ShowChartDefaultProgress("", "");			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function ListCompany : " + xhr.statusText);
		}
	});
}

function ListEstate(companycode) {
	$.ajax({
		type: 'POST',
		url: '../Service/MapService.asmx/ListEstate',
		data: '{CompanyCode: "' + companycode + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var dataValue = response.d[0].split(";");
			var dataText = response.d[1].split(";");
			var dataLength = response.d[0].split(";").length;

			$('#selectFilterEstate').find('option').remove();
			$("#selectFilterEstate").append($('<option>', {
				value: "",
				text: "All Estate"
			}));
			for (var i = 0; i < dataLength; i++) {
				$("#selectFilterEstate").append($('<option>', {
					value: dataValue[i],
					text: dataValue[i] + " - " + dataText[i]
				}));
			}

			$("#selectFilterEstate").val("");
			$('.selectpicker').selectpicker('refresh');
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert("function ListEstate : " + xhr.statusText);
		}
	});
}

function ShowPopupChart(company, estate, adjust, keterangan) {
	$.ajax({
		type: 'POST',
		url: '../Service/MapService.asmx/GetDataPengukuranForDashboard',
		data: '{CompanyCode: "' + company + '", EstCode: "' + estate + '", AdjustmentValue: "' + adjust + '", Keterangan : "' + keterangan + '"}',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var json = $.parseJSON(response.d);

			$("#dialog").dialog({
				open: function (event, ui) {
					$(".ui-widget-overlay").bind("click", function (event, ui) {
						$("#dialog").dialog("close");
					});
				},
				closeOnEscape: true,
				draggable: true,
				resizable: false,
				title: "Data Pengukuran " + estate,
				width: 700,
				modal: false,
				show: 500
			});
			$(".ui-widget-overlay").css({ "background-color": "#111111" });

			//var chartDatactxDataPengukuran_Data = {
			//    labels: ['Banjir', 'Tergenang', 'Agak Tergenang', 'Normal', 'Kering', 'Rusak/Hilang', 'No Data'],
			//    datasets: [{
			//        data: [json[0]["Banjir"], json[0]["A"], json[0]["B"], json[0]["C"], json[0]["D"], json[0]["Rusak / Hilang"], json[0]["NoData"]],
			//        backgroundColor: [
			//            '#00BFFF',
			//            '#FF8000',
			//            '#FFFF00',
			//            '#008000',
			//            '#FF0000',
			//            '#808080',
			//            '#C0C0C0'
			//        ]
			//    }]
			//};

			//var ctxDataPengukuran = document.getElementById('chartDataPengukuran_Popup').getContext('2d');
			//myChartDataPengukuran = new Chart(ctxDataPengukuran, {
			//    type: 'doughnut',
			//    data: chartDatactxDataPengukuran_Data,
			//    options: {
			//        responsive: true,
			//        legend: {
			//            position: 'top',
			//        },
			//        title: {
			//            display: true,
			//            text: "Pengukuran Piezometer " + ""
			//        },
			//        animation: {
			//            animateScale: true,
			//            animateRotate: true
			//        }
			//    }
			//});

			var options = {
				animationEnabled: true,
				data: [{
					type: "doughnut",
					startAngle: 60,
					indexLabelFontSize: 17,
					indexLabel: "{label} - {y} (#percent%)",
					toolTipContent: "<b>{label}:</b> {y} (#percent%)",
					dataPoints: [
						{ y: json[0]["Banjir"], label: "Banjir", color: '#000000' },
						{ y: json[0]["A"], label: "Tergenang", color: '#2f74b7' },
						{ y: json[0]["B"], label: "Agak Tergenang", color: '#01b0f3' },
						{ y: json[0]["C"], label: "Normal", color: '#008000' },
						{ y: json[0]["D"], label: "Agak Kering", color: '#FFFF00' },
						{ y: json[0]["E"], label: "Kering", color: '#FF0000' },
						{ y: json[0]["Rusak / Hilang"], label: "Rusak/Hilang", color: '#808080' },
						{ y: json[0]["NoData"], label: "No Data", color: '#C0C0C0' }
					]
				}]
			};
			$("#chartDataPengukuran_Popup").CanvasJSChart(options);

			$("#dialog").dialog("open");

			$('#btnGoToMap').unbind('click');
			$("#btnGoToMap").click(function (evt) {
				window.open('PZO_Map.aspx?typeModule=OLM&Company=' + $('#lblCompanyCode').text() + '&Estate=' + estate + '&IDWEEK=' + $('#lblWeekName').text() + '&chart=true', '_blank');
			})

			$('#btnWeeklyReport').unbind('click');
			$("#btnWeeklyReport").click(function (evt) {
				var $this = $(this);
				$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

				$.ajax({
					type: 'POST',
					url: '../Service/MapService.asmx/GenerateWeeklyReport',
					data: '{EstCode: "' + estate + '", WeekName: "' + $('#lblWeekName').text() + '"}',
					contentType: 'application/json; charset=utf-8',
					dataType: 'json',
					success: function (response) {
						$this.html("Weekly Report");

						var content = response.d;
						var request = new XMLHttpRequest();
						request.open('POST', '../Handler/GetReportHandler.ashx?reporttype=weekly&filename=' + response.d, true);
						request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
						request.responseType = 'blob';

						request.onload = function () {
							// Only handle status code 200
							if (request.status === 200) {
								// Try to find out the filename from the content disposition `filename` value
								var disposition = request.getResponseHeader('content-disposition');
								var matches = /"([^"]*)"/.exec(disposition);
								var filename = (matches != null && matches[1] ? matches[1] : response.d);

								// The actual download
								var blob = new Blob([request.response], { type: 'application/pdf' });
								var link = document.createElement('a');
								link.href = window.URL.createObjectURL(blob);
								link.download = filename;

								document.body.appendChild(link);

								link.click();

								document.body.removeChild(link);
							}
							else {
								alert('File not found');
							}
						};

						request.send('content=' + content);
					},
					error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr.statusText);
					}
				})
			})

			$('#btnWeeklyReportV2').unbind('click');
			$("#btnWeeklyReportV2").click(function (evt) {
				var $this = $(this);
				$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");
				var week
				var mdlCode = 'WLR';
				var reportId = 'RPT1a';
				var EstNewCode = estate;
				var CompanyCode = $('#lblCompanyCode').text();
				var dateParam = '';
				var weekName = $('#lblWeekName').text();
				var userid = uid;
				var fileRepX = 'PZO_LaporanPencatatanPiezometerByZona';
				//var win = window.open('../Service/DownloadPDFHandler.ashx?bln=' + $("#selectFilterMonth").val() + '&thn=' + $("#selectFilterYear").val() + '&estCode=' + $("#selectFilterKebun").val() + '&afdeling=' + $("#selectFilterAfdeling").val() + '&gang=' + $("#selectFilterGang").val() + '&status=' + $("#selectFilterStatus").val() + '&fileRepX=MCS_LaporanMonitoringAbsensi&dateParam=' + dateFormat6(selectedDate) + '&tanggalLaporan=' + dateFormat10(selectedDate) + /*+ '&InspectorBy=' + $('#selectFilterInspector').val() + '&InspectorName=' + $("#selectFilterInspector option:selected").text() +*/ '&namaFile=' + nmpdf, '_blank');
				var win = window.open('https://app.gis-div.com/pcsweb/Service/DownloadPDFHandler2.ashx?shortCutWeb=PZO' + '&mdlCode=' + mdlCode + '&reportCode=' + reportId + '&fileRepX=' + fileRepX + '&EstNewCode=' + EstNewCode + '&weekName=' + weekName + '&userIds=' + userid + '&reportCategory=Weekly&pageId=android.ashx&remarks=&TypeDownload=WEB&namaFile=&ext=pdf', '_blank');

				$this.html("RPT1a - Weekly Report V2");

			})
			//$("#btnWeeklyReportV2").click(function (evt) {
			//	var $this = $(this);
			//	$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

			//	$.ajax({
			//		type: 'POST',
			//		url: '../Service/MapService.asmx/GenerateWeeklyReportV2',
			//		data: '{EstCode: "' + estate + '", WeekName: "' + $('#lblWeekName').text() + '"}',
			//		contentType: 'application/json; charset=utf-8',
			//		dataType: 'json',
			//		success: function (response) {
			//			$this.html("Weekly Report V2");
			//			console.log
			//			var content = response.d;
			//			var request = new XMLHttpRequest();
			//			request.open('POST', '../Handler/GetReportHandler.ashx?reporttype=weekly&filename=' + response.d, true);
			//			request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');						
			//			request.responseType = 'blob';

			//			request.onload = function () {
			//				// Only handle status code 200
			//				if (request.status === 200) {
			//					// Try to find out the filename from the content disposition `filename` value
			//					var disposition = request.getResponseHeader('content-disposition');
			//					var matches = /"([^"]*)"/.exec(disposition);
			//					var filename = (matches != null && matches[1] ? matches[1] : response.d);

			//					// The actual download
			//					var blob = new Blob([request.response], { type: 'application/pdf' });
			//					var link = document.createElement('a');
			//					link.href = window.URL.createObjectURL(blob);
			//					link.download = filename;

			//					document.body.appendChild(link);

			//					link.click();

			//					document.body.removeChild(link);
			//				}
			//				else {
			//					alert('File not found');
			//				}
			//			};

			//			request.send('content=' + content);
			//		},
			//		error: function (xhr, ajaxOptions, thrownError) {
			//			console.log(xhr.statusText);
			//		}
			//	})
			//})

			
			$('#btnRODPZORPT2').unbind('click');
			$("#btnRODPZORPT2").click(function (evt) {
				var $this = $(this);
				$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

				var week
				var mdlCode = 'WLR';
				var reportId = 'RPT2';
				var CompanyCode = $('#lblCompanyCode').text();
				var dateParam = '';
				var weekName = $('#lblWeekName').text();
				var userid = uid;
				var fileRepX = 'PZO_LaporanSummaryInspeksiPiezometer';

				//alert('CompanyCode : ' + CompanyCode);
				//alert('weekName : ' + weekName);
				//alert('userid : ' + userid);

				//alert('selectFilterCompany : ' + $("#selectFilterCompany").val());
				//alert('selectedCompany : ' + selectedCompany)


				//alert('lblCompanyCode : ' + $('#lblCompanyCode').text() );
				//alert('estate : ' + estate)

				//selectedCompany = $("#selectFilterCompany").val();
				//var mdlCode = context.Request.Params["mdlCode"];
				//var reportId = context.Request.Params["reportCode"];

				//estcode = context.Request.Params["estate"] != null ? context.Request.Params["estate"].ToString() : estcode;
				//estcode = context.Request.Params["estcode"] != null ? context.Request.Params["estcode"].ToString() : estcode;

				//CompanyCode = context.Request.Params["CompanyCode"] != null ? context.Request.Params["CompanyCode"].ToString() : CompanyCode;


				//dateParam = context.Request.Params["dateParam"] != null ? context.Request.Params["dateParam"].ToString() : "";

				//alert('click button pdf')
				//var bln = selectedDate.getMonth() + 1;
				//var thn = selectedDate.getFullYear();
				//////var estcode = $("#selectFilterKebun option:selected").html().split('-');
				//var dt = new Date();
				//let shortMonth = dt.toLocaleString('en-us', { month: 'short' }); /$ Jun $/
				//var dates = `${dt.getDate().toString().padStart(2, '0')} ${shortMonth} ${dt.getFullYear().toString().padStart(4, '0')} ${dt.getHours().toString().padStart(2, '0')}:${dt.getMinutes().toString().padStart(2, '0')}:${dt.getSeconds().toString().padStart(2, '0')}`

				//const javaScriptRelease = Date.parse(dates);
				//var nmpdf = $("#selectFilterYear option:selected").val().toString().substring(2, 4) + ($("#selectFilterMonth option:selected").val().toString().length == 1 ? "0" + $("#selectFilterMonth option:selected").val().toString() : $("#selectFilterMonth option:selected").val().toString()) + "_" + '_Q6_' + 'Laporan Monitoring Absensi ' + "_" + javaScriptRelease;
				//////var namaFile = $("#selectFilterYear option:selected").val().toString().substring(2, 4) + ($("#selectFilterMonth option:selected").val().toString().length == 1 ? "0" + $("#selectFilterMonth option:selected").val().toString() : $("#selectFilterMonth option:selected").val().toString()) + "_" + estcode[0] + '_Mth3' + '_' + 'Laporan SDM Panen' + "_" + javaScriptRelease;
				//console.info("pdf", nmpdf);
				//http://localhost:6999/Service/DownloadPDFHandler2.ashx?dateParam=2023-02-22&startDate=22/02/2023&startDateParam=22/02/23&thn=2023&bln=2&dateParam99=02/22/2023&dateParamPeta=20230222&dateParamStr=2023-02-22&CompanyCode=PT.THIP&tanggalLaporan=2023-02-22&module=WLR&mdlCode=WLR&reportCategory=Weekly&pageId=android.ashx&reportCode=RPT2&remarks=%27%27&TypeDownload=Android&fileRepX=PZO_LaporanSummaryInspeksiPiezometer&%20&userIds=2540&namaFile=230222_RPT2__Laporan%20Summary%20Inspeksi%20Piezometer_1677098586000.pdf&ext=pdf

				//var win = window.open('../Service/DownloadPDFHandler.ashx?bln=' + $("#selectFilterMonth").val() + '&thn=' + $("#selectFilterYear").val() + '&estCode=' + $("#selectFilterKebun").val() + '&afdeling=' + $("#selectFilterAfdeling").val() + '&gang=' + $("#selectFilterGang").val() + '&status=' + $("#selectFilterStatus").val() + '&fileRepX=MCS_LaporanMonitoringAbsensi&dateParam=' + dateFormat6(selectedDate) + '&tanggalLaporan=' + dateFormat10(selectedDate) + /*+ '&InspectorBy=' + $('#selectFilterInspector').val() + '&InspectorName=' + $("#selectFilterInspector option:selected").text() +*/ '&namaFile=' + nmpdf, '_blank');
				var win = window.open('https://app.gis-div.com/pcsweb/Service/DownloadPDFHandler2.ashx?shortCutWeb=PZO' + '&mdlCode=' + mdlCode + '&reportCode=' + reportId + '&fileRepX=' + fileRepX + '&CompanyCode=' + CompanyCode + '&weekName=' + weekName + '&userIds=' + userid + '&reportCategory=Weekly&pageId=android.ashx&remarks=&TypeDownload=WEB&namaFile=&ext=pdf', '_blank');

				//http://localhost:6999/Service/DownloadPDFHandler2.ashx?dateParam=2023-02-22&startDate=22/02/2023&startDateParam=22/02/23&thn=2023&bln=2&dateParam99=02/22/2023
				//& dateParamPeta=20230222 & dateParamStr=2023 - 02 - 22
					//&CompanyCode=PT.THIP
					//mdlCode=WLR&reportCode=RPT2&reportCategory=Weekly&pageId=android.ashx&remarks=&TypeDownload=Android
					//&fileRepX=PZO_LaporanSummaryInspeksiPiezometer&userIds=2540&namaFile=&ext=pdf


				$this.html("RPT2 - Summary Inspeksi");

			})


			$('#btnRODPZORPT2a').unbind('click');
			$("#btnRODPZORPT2a").click(function (evt) {
				var $this = $(this);
				$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

				var week
				var mdlCode = 'WLR';
				var reportId = 'RPT2a';
				var CompanyCode = $('#lblCompanyCode').text();
				var dateParam = '';
				var weekName = $('#lblWeekName').text();
				var userid = uid;
				var fileRepX = 'PZO_LaporanSummaryHasilInspeksiPiezometer';
				//var win = window.open('../Service/DownloadPDFHandler.ashx?bln=' + $("#selectFilterMonth").val() + '&thn=' + $("#selectFilterYear").val() + '&estCode=' + $("#selectFilterKebun").val() + '&afdeling=' + $("#selectFilterAfdeling").val() + '&gang=' + $("#selectFilterGang").val() + '&status=' + $("#selectFilterStatus").val() + '&fileRepX=MCS_LaporanMonitoringAbsensi&dateParam=' + dateFormat6(selectedDate) + '&tanggalLaporan=' + dateFormat10(selectedDate) + /*+ '&InspectorBy=' + $('#selectFilterInspector').val() + '&InspectorName=' + $("#selectFilterInspector option:selected").text() +*/ '&namaFile=' + nmpdf, '_blank');
				var win = window.open('https://app.gis-div.com/pcsweb/Service/DownloadPDFHandler2.ashx?shortCutWeb=PZO' + '&mdlCode=' + mdlCode + '&reportCode=' + reportId + '&fileRepX=' + fileRepX + '&CompanyCode=' + CompanyCode + '&weekName=' + weekName + '&userIds=' + userid + '&reportCategory=Weekly&pageId=android.ashx&remarks=&TypeDownload=WEB&namaFile=&ext=pdf', '_blank');

				$this.html("RPT2a - Summary Hasil Inspeksi");

			})



			$('#btnRODPZORPT3').unbind('click');
			$("#btnRODPZORPT3").click(function (evt) {
				var $this = $(this);
				$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

				var week
				var mdlCode = 'WLR';
				var reportId = 'RPT3';
				var CompanyCode = $('#lblCompanyCode').text();
				var dateParam = '';
				var weekName = $('#lblWeekName').text();
				var userid = uid;
				var fileRepX = 'PZO_LaporanListUserDownload';
				//var win = window.open('../Service/DownloadPDFHandler.ashx?bln=' + $("#selectFilterMonth").val() + '&thn=' + $("#selectFilterYear").val() + '&estCode=' + $("#selectFilterKebun").val() + '&afdeling=' + $("#selectFilterAfdeling").val() + '&gang=' + $("#selectFilterGang").val() + '&status=' + $("#selectFilterStatus").val() + '&fileRepX=MCS_LaporanMonitoringAbsensi&dateParam=' + dateFormat6(selectedDate) + '&tanggalLaporan=' + dateFormat10(selectedDate) + /*+ '&InspectorBy=' + $('#selectFilterInspector').val() + '&InspectorName=' + $("#selectFilterInspector option:selected").text() +*/ '&namaFile=' + nmpdf, '_blank');
				var win = window.open('https://app.gis-div.com/pcsweb/Service/DownloadPDFHandler2.ashx?shortCutWeb=PZO' + '&mdlCode=' + mdlCode + '&reportCode=' + reportId + '&fileRepX=' + fileRepX + '&CompanyCode=' + CompanyCode + '&weekName=' + weekName + '&userIds=' + userid + '&reportCategory=Weekly&pageId=android.ashx&remarks=&TypeDownload=WEB&namaFile=&ext=pdf', '_blank');

				$this.html("RPT3 - Laporan List User Download");

			})



			$('#btnRODPZORPT4').unbind('click');
			$("#btnRODPZORPT4").click(function (evt) {
				var $this = $(this);
				$this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

				var week
				var mdlCode = 'WLR';
				var reportId = 'RPT4';
				var EstNewCode = estate;
				var CompanyCode = $('#lblCompanyCode').text();
				var dateParam = '';
				var weekName = $('#lblWeekName').text();
				var userid = uid;
				var fileRepX = 'PZO_LaporanPiezometerTahunan';
				var fileExt = 'excel';
				//var win = window.open('../Service/DownloadPDFHandler.ashx?bln=' + $("#selectFilterMonth").val() + '&thn=' + $("#selectFilterYear").val() + '&estCode=' + $("#selectFilterKebun").val() + '&afdeling=' + $("#selectFilterAfdeling").val() + '&gang=' + $("#selectFilterGang").val() + '&status=' + $("#selectFilterStatus").val() + '&fileRepX=MCS_LaporanMonitoringAbsensi&dateParam=' + dateFormat6(selectedDate) + '&tanggalLaporan=' + dateFormat10(selectedDate) + /*+ '&InspectorBy=' + $('#selectFilterInspector').val() + '&InspectorName=' + $("#selectFilterInspector option:selected").text() +*/ '&namaFile=' + nmpdf, '_blank');
				var win = window.open('https://app.gis-div.com/pcsweb/Service/DownloadPDFHandler2.ashx?shortCutWeb=PZO' + '&mdlCode=' + mdlCode + '&reportCode=' + reportId + '&fileRepX=' + fileRepX + '&EstNewCode=' + EstNewCode + '&weekName=' + weekName + '&userIds=' + userid + '&reportCategory=Weekly&pageId=android.ashx&remarks=&TypeDownload=WEB&namaFile=&ext=' + fileExt, '_blank');


				$this.html("RPT4 - Laporan Piezometer Tahunan");

			})


		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.statusText);
		}
	});


	////SHOW DIALOG
	//$("#dialog").dialog({
	//    autoOpen: false
	//}).position({
	//    my: 'left',
	//    at: 'right',
	//    of: $(this)
	//});

	//$("#dialog").dialog("open");
}


//////////////////////////////////////////////////////////
///////////////////// EVENT HANDLER //////////////////////
//////////////////////////////////////////////////////////

$("#selectFilterCompany").change(function () {
	ListEstate($("#selectFilterCompany").val());
})

$("#btnSubmitFilter").click(function () {
	selectedCompany = $("#selectFilterCompany").val();
	selectedEstate = $("#selectFilterEstate").val();
	ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
	ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, "");
})

$("#btnPrevWeek").click(function () {
	flagValue = flagValue - 1;
	if ($('#mainchart').is(":visible")) {
		ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
		ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname);
	} else if ($('#subchart').is(":visible")) {
		ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
		ShowChartDataPengukuran_Custom(selectedCompanyForSubChart, selectedEstate, flagValue, selectedCompanyShortname);
	}

	//ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname);
})

$("#btnNextWeek").click(function () {
	flagValue = flagValue + 1;
	if ($('#mainchart').is(":visible")) {
		ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
		ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname);
	} else if ($('#subchart').is(":visible")) {
		ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
		ShowChartDataPengukuran_Custom(selectedCompanyForSubChart, selectedEstate, flagValue, selectedCompanyShortname);
	}

	//ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
	//ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname);
	//ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
})

$("#btnThisWeek").click(function () {
	flagValue = 0;
	if ($('#mainchart').is(":visible")) {
		ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
		ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname)
	} else if ($('#subchart').is(":visible")) {
		ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
		ShowChartDataPengukuran_Custom(selectedCompanyForSubChart, selectedEstate, flagValue, selectedCompanyShortname)
	}

	//ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
	//ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname)
	//ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
})

$('input').on('ifChecked', function (event) {
	if ($('#mainchart').is(":visible")) {
		ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
		ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname);
	} else if ($('#subchart').is(":visible")) {
		ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
		ShowChartDataPengukuran_Custom(selectedCompanyForSubChart, selectedEstate, flagValue, selectedCompanyShortname);
	}
});
function customSort(x, y) {
	return x > y ? 1 : (x < y ? -1 : 0);
}

function generateDataReport(flagValue) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GenerateDataReport',
        data: '{AdjustmentValue: "' + flagValue + '", userID: "' + uid + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            if (json != null) {
                $("#lblLastData").html("Last Generate : " + dateFormat12(new Date(parseInt(json[0]["lastUpdate"].replace('/Date(', '').replace(')/', '')))));

                console.log("success generate");
                $(".loading").hide();

            }
            else {
                console.log("failed generate");
                $(".loading").hide();
            }
        }
    })
}
function dateFormat12(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();
    var hh = date.getHours();
    var min = date.getMinutes();
    var ss = date.getSeconds();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = dd + '-' + mm + '-' + yyyy + ' ' + zeroPad(hh, 2) + ':' + zeroPad(min, 2);
}
function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
}
