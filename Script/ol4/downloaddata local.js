﻿var startDate, endDate;
var currDay, currMonth, currYear;
var arrSelectedWeek;
var arrWeek = [];
var dataJsonMI;
var dataJsonML;
var _dataTableExisting = [];
var _pierecordid;
var _week;
var _ketinggian;
var _ketinggianFinal;
var _generate;
var _estate;
var arrEstate = [];
var BlockRehab = [];
var thisWeek;
var thisWeekId;
var flagSelectAllEstate = 0;
var flagSelectedAllZona = 0;
var arrEstateDropdown = [];
var arrZonaDropdown = [];
var ArrPulauDropdown = [];
var dataChartTMAT, blackListPiezo, masterWeekChart, jArrayBlackList;
var listGisUser, uId;
$(function () {
    listGisUser = $.parseJSON($('#listGisUser').val());
    uId = $.parseJSON($('#userid').val());

    setAccessModule();
    $('.selectpicker').selectpicker({});
    $('.select2').select2({
        placeholder: "No data",
    });

    $('#tglSelectedGraph').daterangepicker({
        ranges: {
            /*'Last 30 Days': [moment().subtract(29, 'days'), moment()],*/
            'Last 90 Days': [moment().subtract(89, 'days'), moment()],
            'Last 1 Year': [moment().subtract(365, 'days'), moment()]
        },
        startDate: moment().subtract(89, 'days'),
        endDate: moment(),
        locale: {
            format: 'DD/MM/YYYY'
        }
    });

    $('input[type="radio"].flat-green').iCheck({
        radioClass: 'iradio_flat-green'
    })

    var d = new Date();
    currDay = d.getDate();
    currMonth = d.getMonth() + 1;
    currYear = d.getFullYear();

    startDate = currYear + "-" + currMonth + "-" + "01"; //new Date(currYear, currMonth, 1);
    endDate = currYear + "-" + currMonth + "-" + currDay; //new Date(currYear, currMonth, currDay);
    console.log("daynow : " + d + " " + "start date :" + startDate + " " + "end date : " + endDate);

    //Date range picker with time picker
    $('#periode').daterangepicker({
        startDate: new Date(currYear, currMonth - 1, 1), timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY'
    },
        function (start, end, label) {
            startDate = start.format('YYYY-MM-DD');
            endDate = end.format('YYYY-MM-DD');
            console.log("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        })

    $('#periodeForKLHK').daterangepicker({
        startDate: new Date(currYear, currMonth - 1, 1), timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY'
    },
        function (start, end, label) {
            startDate = start.format('YYYY-MM-DD');
            endDate = end.format('YYYY-MM-DD');
            console.log("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        })
    $('#periodeForGenerateKLHK').daterangepicker({
        startDate: new Date(currYear, currMonth - 1, 1), timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY'
    },
        function (start, end, label) {
            startDate = start.format('YYYY-MM-DD');
            endDate = end.format('YYYY-MM-DD');
            console.log("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        })

    $('#periodeforpencatatanPerubahan').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 2000,
        maxYear: parseInt(moment().format('YYYY'), 10)
    },
        function (start, end, label) {
            startDate = start.format('YYYY-MM-DD');
            endDate = end.format('YYYY-MM-DD');
            console.log("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        })

    $("#selectFilterCompany").change(function () {
        ListEstate($("#selectFilterCompany").val(), "data");
    })

    $("#selectFilterCompanyForKLHK").change(function () {
        ListEstate($("#selectFilterCompanyForKLHK").val(), "klhk");
    })

    $("#selectFilterCompanyForGenerateKLHK").change(function () {
        ListEstate($("#selectFilterCompanyForGenerateKLHK").val(), "klhk");
    })

    $("#selectFilterCompanyForPerbandingan").change(function () {
        ListEstate($("#selectFilterCompanyForPerbandingan").val(), "data");
    })

    $("#selectFilterAddCompanyBlockRehab").change(function () {
        ListEstate($("#selectFilterAddCompanyBlockRehab").val(), "klhk");
    })

    $("#selectFilterAddEstateBlockRehab").change(function () {
        ListBlock($("#selectFilterAddEstateBlockRehab").val());
    })
    $("#selectFilterEstatePencatatanPerubahanTMAT").change(function () {
        $('.loading').show()

        getZonaByEstate();

        $('#selectFilterEstatePencatatanPerubahanTMAT').selectpicker('refresh');
    });
    $("#selectFilterEstateGrapTmasTmat").change(function () {
        $('.loading').show()

        getZonaByEstateGraph();

        $('#selectFilterEstateGrapTmasTmat').selectpicker('refresh');
    });




    $("#btnPrevWeekPencatatanPerubahan").click(function () {
        getPrevNextWeekName(0)
    });
    $("#btnNextWeekPencatatanPerubahan").click(function () {
        getPrevNextWeekName(1)
    });

    $("#btnPrevWeekPetaPerbandingan").click(function () {
        getPrevNextWeekPerbandingan(0)
    });
    $("#btnNextWeekPetaPerbandingan").click(function () {
        getPrevNextWeekPerbandingan(1)
    });

    $("#btnPrevWeekPetaPerbandinganxx").click(function () {
        getPrevNextWeekPerbandinganxx(0)
    });
    $("#btnNextWeekPetaPerbandinganxx").click(function () {
        getPrevNextWeekPerbandinganxx(1)
    });


    $("#btnPrevWeekGrapTmasTmat").click(function () {
        getPrevNextWeekNameGraph(0)
    });
    $("#btnNextWeekGrapTmasTmat").click(function () {
        getPrevNextWeekNameGraph(1)
    });
    
    $("#btnPrevWeekPetaKondisiPiezo").click(function () {
        getPrevNextWeekPerbandinganKondisiPiezo(0)
    });
    $("#btnNextWeekPetaKondisiPiezo").click(function () {
        getPrevNextWeekPerbandinganKondisiPiezo(1)
    });

    //$("#selectFilterEstateForGenerateKLHK").change(function () {
    //    ListWeek($("#selectFilterEstateForGenerateKLHK").val());
    //})

    $('#selectFilterEstateForGenerateKLHK').on('change', function () {
        var opunit = '';
        if ($('#selectFilterEstateForGenerateKLHK').val() != null) {
            $('#selectFilterEstateForGenerateKLHK').val().forEach(function (opt) {
                if (opunit === '')
                    opunit += opt;
                else
                    opunit += ',' + opt;
            })
        }
    });
    $('#selectFilterEstatePulauPerubahanTMAT').change(function () {
        $('.loading').show()
        populateEstateByPulau(this.id)
    });


    $('#selectFilterAddEstateBlockRehab').on('change', function () {
        var opunit = '';
        if ($('#selectFilterAddEstateBlockRehab').val() != null) {
            $('#selectFilterAddEstateBlockRehab').val().forEach(function (opt) {
                if (opunit === '')
                    opunit += opt;
                else
                    opunit += ',' + opt;
            })
        }
    });

    $('#selectFilterAddBlockRehab').on('change', function () {
        var opunit = '';
        if ($('#selectFilterAddBlockRehab').val() != null) {
            $('#selectFilterAddBlockRehab').val().forEach(function (opt) {
                //if (opunit === '')
                //    opunit += opt;
                //else
                //    opunit += ',' + opt;

                BlockRehab.push({ Block: opt })

            })
        }

    });

    //$("#rbFilterByDate").click(function () {
    //    alert('filter by date aktif');
    //    var divDateFilter = document.getElementById("divPeriode");
    //    var divWeekFilter = document.getElementById("divWeekDropDown");
    //    var divWeekFilter2 = document.getElementById("divWeekDropDown2");
    //    divWeekFilter.style.display = "none";
    //    divWeekFilter2.style.display = "none";
    //    divDateFilter.style.display = "block";
    //});
    //$("#rbFilterByDate").click(function () {      
    //});
    $('#rbFilterByDate').on('ifChanged', function () {
        var divDateFilter = document.getElementById("divPeriode");
        var divWeekFilter = document.getElementById("divWeekDropDown");
        var divWeekFilter2 = document.getElementById("divWeekDropDown2");
        divWeekFilter.style.display = "none";
        divWeekFilter2.style.display = "none";
        divDateFilter.style.display = "block";
    });
    $('#rbFilterByWeek').on('ifChanged', function () {
        var divDateFilter = document.getElementById("divPeriode");
        var divWeekFilter = document.getElementById("divWeekDropDown");
        var divWeekFilter2 = document.getElementById("divWeekDropDown2");
        divDateFilter.style.display = "none";
        divWeekFilter.style.display = "block";
        divWeekFilter2.style.display = "block";
    });
    //$("#rbFilterByWeek").click(function () {

    //});
    $("#btnSubmitFilter").click(function () {
        $(".loading").show();
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/GetDataPiezometer',
            data: '{CompanyCode: "' + $('#selectFilterCompany').val() + '", EstCode: "' + $('#selectFilterEstate').val() + '", StartDate: "' + startDate + '", EndDate: "' + endDate + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var json = $.parseJSON(response.d);
                //console.log('json = ' + JSON.stringify(json));
                console.log(json.length);

                var content = '<table id="tableData" class="table table-striped table-bordered" style="width:100%">';
                content += '<thead><tr>';
                content += '<th>Data Taken</th><th>Estate</th><th>Block</th><th>PiezoRecordID</th><th>Ketinggian</th><th>Indicator Name</th><th>Indicator Alias</th><th>Week Name</th><th>Date</th><th>Kali Ambil</th><th>Foto Record</th></tr></thead><tbody>';

                for (var i = 0; i < json.length; i++) {
                    content += '<tr>';
                    content += '<td>' + json[i]["DataTaken"] + '</td>';
                    content += '<td>' + json[i]["EstCode"] + '</td>';
                    content += '<td>' + json[i]["Block"] + '</td>';
                    content += '<td>' + json[i]["PieRecordID"] + '</td>';
                    content += '<td>' + json[i]["Ketinggian"] + '</td>';
                    content += '<td>' + json[i]["IndicatorName"] + '</td>';
                    content += '<td>' + json[i]["IndicatorAlias"] + '</td>';
                    content += '<td>' + json[i]["MonthName"] + '</td>';
                    content += '<td>' + dateFormat5(new Date(parseInt(json[i]["Date"]))) + '</td>';
                    content += '<td>' + json[i]["banyak"] + '</td>';
					if(json[i]["urlImages"] == null){
						content += '<td></td>';
					}else{
						var sUrlImages = json[i]["urlImages"].split(',');
						content += '<td>';
						for(var j = 0 ; j < sUrlImages.length; j++){
							content += '<a href="'+sUrlImages[j]+'" target="_blank"><i class="fa fa-picture-o"></i></a>  ';
						}
						content += '</td>';
					}
                    content += '</tr>';
                    //var humanReadableDate = new Date(parseInt(json[i]["Date"]*1000));
                    //console.log('new format = ' + dateFormat5(humanReadableDate));
                }

                content += '</tbody></table>';
                $("#divTableData").html(content);
                $('#tableData').DataTable({
                    dom: 'Blfrtip',
                    lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                    pageLength: 10,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });

                $(".loading").hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("function GetDataPiezometer : " + xhr.statusText);
            }
        });
    })
    $("#btnSubmitFilterForKLHK").click(function () {
        GenerateTableKLHK($("#selectFilterCompanyForKLHK").val(), $("#selectFilterEstateForKLHK").val(), startDate, endDate);
    });

    $("#btnSubmitFilterForGenerateKLHK").click(function () {
        if ($("#selectFilterCompanyForGenerateKLHK option:selected").val() != "") {
            if ($("#selectFilterPeriodeForGenerateKLHK").val() != "") {
                $(".loading").show();
                GenerateTableKLHKForGenerate($("#selectFilterCompanyForGenerateKLHK  option:selected").val(), $('#selectFilterEstateForGenerateKLHK').val(), $("#selectFilterPeriodeForGenerateKLHK  option:selected").val(), $("#selectFilterYearForGenerateKLHK  option:selected").val());
            }
            else {
                $.alert({
                    title: 'Alert!',
                    content: 'Mohon Pilih Periode',
                });
                $(".loading").hide();
            }
        }
        else {
            $.alert({
                title: 'Alert!',
                content: 'Mohon Pilih Company',
            });
            $(".loading").hide();
        }

    });

    $("#btnSubmitFilterForBlockRehab").click(function () {
        if ($("#selectFilterCompanyFinalKLHK option:selected").val() != "") {
            $(".loading").show();
            GenerateTableBlockRehab($("#selectFilterCompanyBlockRehab  option:selected").val());
        }
        else {
            $.alert({
                title: 'Alert!',
                content: 'Mohon Pilih Company',
            });
            $(".loading").hide();
        }

    });

    //$("#btnDownloadKLHK").click(function () {
    //    DownloadTableKLHK($("#selectFilterCompanyForKLHK option:selected").val(), $("#selectFilterEstateForKLHK option:selected").val(), startDate, endDate);
    //});

    $("#btnUploadKLHK").click(function () {
        $('#mdlImportData').modal('toggle');
    });

    $("#btnAddBlockRehab").click(function () {
        $('#mdlblockRehab').modal('toggle');
    });

    $("#btnUpdateKLHK").click(function () {
        if ($("#selectFilterCompanyForGenerateKLHK option:selected").val() != "") {
            $(".loading").show();
            //var r = confirm("Apakah anda yakin untuk update data");
            $.confirm({
                title: 'Confirm!',
                content: 'Apakah anda yakin untuk update data',
                buttons: {
                    confirm: function () {
                        UpdateDataKLHK();
                    },
                    cancel: function () {
                        $(".loading").hide();
                    }
                }
            });
        }
        else {
            $.alert({
                title: 'Alert!',
                content: 'Mohon Pilih Company',
            });
            $(".loading").hide();
        }

    });

    $("#btnGenerateKLHK").click(function () {
        if ($("#selectFilterCompanyForGenerateKLHK option:selected").val() != "") {
            $(".loading").show();
            $.ajax({
                type: 'POST',
                url: '../Service/MapService.asmx/CheckDataHasApprovedAll',
                data: '{CompanyCode: "' + $("#selectFilterCompanyForGenerateKLHK option:selected").val() + '", EstCode: "' + $("#selectFilterEstateForGenerateKLHK").val() + '", Periode: "' + $("#selectFilterPeriodeForGenerateKLHK option:selected").val() + '", Year: "' + $("#selectFilterYearForGenerateKLHK option:selected").val() + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var json = $.parseJSON(response.d);
                    if (json.length != 0) {
                        $.alert({
                            title: 'Alert!',
                            content: 'Data bulan lalu belum di approve!',
                        });
                        $(".loading").hide();
                    }
                    else {
                        $.ajax({
                            type: 'POST',
                            url: '../Service/MapService.asmx/CheckDataThisMonth',
                            data: '{CompanyCode: "' + $("#selectFilterCompanyForGenerateKLHK option:selected").val() + '", EstCode: "' + $("#selectFilterEstateForGenerateKLHK").val() + '", Periode: "' + $("#selectFilterPeriodeForGenerateKLHK option:selected").val() + '", Year: "' + $("#selectFilterYearForGenerateKLHK option:selected").val() + '"}',
                            contentType: 'application/json; charset=utf-8',
                            dataType: 'json',
                            success: function (response) {
                                var json = $.parseJSON(response.d);
                                if (json.length != 0) {
                                    $.alert({
                                        title: 'Alert!',
                                        content: 'Data bulan ini belum lengkap!',
                                    });
                                    $(".loading").hide();
                                }
                                else {
                                    GenerateDataKLHK($("#selectFilterCompanyForGenerateKLHK option:selected").val(), $('#selectFilterEstateForGenerateKLHK').val(), $("#selectFilterPeriodeForGenerateKLHK option:selected").val());
                                }
                            }
                        });
                    }
                }
            });

        }

        else {
            $.alert({
                title: 'Alert!',
                content: 'Mohon Pilih Company',
            });
            $(".loading").hide();
        }

    });

    $("#btnSaveGenerateManualKLHK").click(function () {
        if ($("#selectFilterCompanyForGenerateKLHK option:selected").val() != "") {
            //if ($("#selectFilterEstateForGenerateKLHK").val() != "") {
            $(".loading").show();
            $.confirm({
                title: 'Confirm!',
                content: 'Apakah anda yakin untuk simpan data',
                buttons: {
                    confirm: function () {
                        SubmitFinalKLHK($("#selectFilterCompanyForGenerateKLHK").val(), $("#selectFilterEstateForGenerateKLHK").val(), $("#selectFilterPeriodeForGenerateKLHK").val());
                    },
                    cancel: function () {
                        $(".loading").hide();
                    }
                }
            });
            //var r = confirm("Apakah anda yakin untuk approved data");
            //if (r == true) {
            //    //txt = "You pressed OK!";
            //    //alert(txt);
            //    SubmitFinalKLHK($("#selectFilterCompanyForGenerateKLHK").val(), $("#selectFilterEstateForGenerateKLHK").val(), $("#selectFilterPeriodeForGenerateKLHK").val());
            //} else {
            //    //txt = "You pressed Cancel!";
            //    //alert(txt);
            //    $(".loading").hide();
            //}

            //}
            //else {
            //    alert("Mohon Pilih Estate");
            //    $(".loading").hide();
            //}
        }
        else {
            $.alert({
                title: 'Alert!',
                content: 'Mohon Pilih Company',
            });
            $(".loading").hide();
        }
    });

    $("#btnDownloadTemplateKLHK").click(function () {
        $(".loading").show();
        DownloadTemplateKLHK();
    });

    $("#btnUpload").click(function () {
        fasterPreview(this);
        $(".loading").show();
        var _idPict = this.files && this.files.length ? this.files[0].name.split('.')[0] : '';
        var _ext = $('#fileUpload')[0].value.split('.')[0];
        var filename = $('#fileUpload')[0].value.split('.')[1];
        var filename2 = $('#fileUpload').val();
        var filename = $('#fileUpload').get(0);
        var FileName = _idPict + "." + _ext;
        var r = confirm("Apakah anda yakin untuk upload file");
        $.confirm({
            title: 'Confirm!',
            content: 'Apakah anda yakin untuk upload file',
            buttons: {
                confirm: function () {
                    validateFileUpload(filename, startDate, endDate);
                },
                cancel: function () {
                    $(".loading").hide();
                }
            }
        });
        //if (r == true) {
        //    validateFileUpload(filename, startDate, endDate);
        //}
        //else {
        //    $(".loading").hide()
        //}

    });

    $("#btnCekNullDataKLHK").click(function () {
        $('#mdlCheckData').modal('toggle');
        CheckNullData($("#selectFilterCompanyForGenerateKLHK").val(), $("#selectFilterEstateForGenerateKLHK").val(), $("#selectFilterPeriodeForGenerateKLHK").val(), $("#selectFilterYearForGenerateKLHK  option:selected").val());
    });

    $("#btnSaveBlockRehab").click(function () {

        if ($("#selectFilterAddCompanyBlockRehab").val() != "") {
            if ($("#selectFilterAddEstateBlockRehab").val() != "") {
                if ($("#selectFilterAddBlockRehab").val() != "") {
                    $(".loading").show();
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Apakah anda yakin untuk simpan data',
                        buttons: {
                            confirm: function () {
                                SaveBlockRehab();
                            },
                            cancel: function () {
                                $(".loading").hide();
                            }
                        }
                    });
                }
                else {
                    $.alert({
                        title: 'Alert!',
                        content: 'Mohon Pilih Block',
                    });
                    $(".loading").hide();
                }

            }
            else {
                $.alert({
                    title: 'Alert!',
                    content: 'Mohon Pilih Estate',
                });
                $(".loading").hide();
            }

        }
        else {

            $.alert({
                title: 'Alert!',
                content: 'Mohon Pilih Company',
            });
            $(".loading").hide();
        }
    });

    $("#btnClearBlockRehab").click(function () {
        $.confirm({
            title: 'Confirm!',
            content: 'Apakah anda yakin untuk hapus semua data',
            buttons: {
                confirm: function () {
                    ClearBlockRehab();
                },
                cancel: function () {
                    $(".loading").hide();
                }
            }
        });
    });

    $('#btnSubmitFilterForPencatatanPerubahanTMATPDF').click(function () {
        $('.loading').show();
        var klhk = 0;
        var checkBox = document.getElementById("checkklhk");
        if (checkBox.checked == true) {
            klhk = 1;
        } else {
            klhk = 0
        }
        var ele = document.getElementsByName('check');

        for (i = 0; i < ele.length; i++) {
            if (ele[i].checked)
                klhk = ele[i].value;
        }
        var flagAllEstate = 0;
        var flagAllZona = 0;
        var flagAllPulau = 0;
        var estateYangDipilih = [];
        var zonaYangDipilih = [];
        var pulauYangDipilih = [];
        estateYangDipilih = $("#selectFilterEstatePencatatanPerubahanTMAT").val();
        zonaYangDipilih = $("#selectFilterZonaPencatatanPerubahanTMAT").val();
        pulauYangDipilih = $("#selectFilterEstatePulauPerubahanTMAT").val();

        console.log('total estate master=' + arrEstateDropdown.length);
        console.log('total zona master=' + arrZonaDropdown.length);
        console.log('total pulau master=' + ArrPulauDropdown.length);

        console.log('total estate yang dipilih=' + estateYangDipilih.length);
        console.log('total zona yang dipilih=' + zonaYangDipilih.length);
        console.log('total pulau yang dipilih=' + pulauYangDipilih.length);

        if (arrEstateDropdown.length == estateYangDipilih.length) {
            flagAllEstate = 1;
        }
        if (arrZonaDropdown.length == zonaYangDipilih.length) {
            flagAllZona = 1;
        }

        if (ArrPulauDropdown.length == pulauYangDipilih.length) {
            flagAllPulau = 1;
        }


        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/GenerateReportPerubahanPencatatanTMAT',
            data: '{wmArea: "' + $("#selectFilterZonaPencatatanPerubahanTMAT").val() + '", WeekName: "' + $('#txtWeekId').val() + '",weekLabel: "' + document.getElementById('lblTitleWeek').textContent + '",klhk: "' + klhk + '",estCode:"' + $("#selectFilterEstatePencatatanPerubahanTMAT").val() + '",flagAllEstate:"' + flagAllEstate + '",flagAllZona:"' + flagAllZona + '",pulau:"' + $("#selectFilterEstatePulauPerubahanTMAT").val() + '",flagAllPulau:"' + flagAllPulau + '",txtParamPulau:"' + $("#selectFilterEstatePulauPerubahanTMAT option:selected").text() + '",txtParamKebun:"' + $("#selectFilterEstatePencatatanPerubahanTMAT").val() + '",txtParamZona:"' + $("#selectFilterZonaPencatatanPerubahanTMAT").val() + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                /*  $this.html("Weekly Report");*/

                var content = response.d;
                var request = new XMLHttpRequest();
                request.open('POST', '../Handler/GetReportHandler.ashx?reporttype=weekly&filename=' + response.d, true);
                request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                request.responseType = 'blob';

                request.onload = function () {
                    // Only handle status code 200
                    if (request.status === 200) {
                        // Try to find out the filename from the content disposition `filename` value
                        var disposition = request.getResponseHeader('content-disposition');
                        var matches = /"([^"]*)"/.exec(disposition);
                        var filename = (matches != null && matches[1] ? matches[1] : response.d);

                        // The actual download
                        var blob = new Blob([request.response], { type: 'application/pdf' });
                        var link = document.createElement('a');
                        link.href = window.URL.createObjectURL(blob);
                        link.download = filename;

                        document.body.appendChild(link);

                        link.click();

                        document.body.removeChild(link);
                    }
                    else {
                        alert('File not found');
                    }
                };

                request.send('content=' + content);
                $('.loading').hide()
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.statusText);
                $('.loading').show()
            }
        })
    })
    
    $('#btnGeneratePetaPerbandingan').click(function () {
        $('.loading').show();

        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/generatePetaPerbandingan',
            data: '{weekId: "' + $("#txtWeekIdPetaPerbandingan").val() + '", companyCode: "' + $("#selectFilterCompanyForPerbandingan").val() + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                $('.loading').hide()
                alert("Berhasil Generate");
                $("#btnSubmitPerbedaanPetaGo").click();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.statusText);
                $('.loading').hide()
            }
        })
    });

    $('#btnSubmitPerbedaanPetaGo').click(function () {

        if ($("#selectFilterCompanyForPerbandingan").val() == "") {
            alert("Mohon Pilih Company");
            return;
        }

        var bisa  = false;

        var ptReady = ["PT.THIP", "PT.JJP"];
        for (var i = 0; i < ptReady.length; i++) {
            if (ptReady[i] == $("#selectFilterCompanyForPerbandingan").val()) {
                bisa = true;
            }
        }

        //if (!bisa) {
        //    alert("pt belum tersedia");
        //    return;
        //}

        $('.loading').show()
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/getPetaPerbandinganPetaGo',
            data: '{pt: "' + $("#selectFilterCompanyForPerbandingan").val() + '", weekId: "' + $("#txtWeekIdPetaPerbandingan").val() + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var tblLastWeek = JSON.parse(response.d[0])
                var summaryLastWeek = JSON.parse(response.d[1])
                var ketLastWeek = JSON.parse(response.d[2])
                var wmsLastWeek = JSON.parse(response.d[3])
                var statisticLastWeek = JSON.parse(response.d[8])

                var tblThisWeek = JSON.parse(response.d[4])
                var summaryThisWeek = JSON.parse(response.d[5])
                var ketThisWeek = JSON.parse(response.d[6])
                var wmsThisWeek = JSON.parse(response.d[7])
                var statisticThisWeek = JSON.parse(response.d[9])

                //tblPetaLastWeek.clear().draw();
                //tblPetaThisWeek.clear().draw();
                //for (var i = 0; i < tblLastWeek.length; i++) {
                //    tblPetaLastWeek.row.add($(addRowPetaPerbandingan(tblLastWeek[i]))[0]).draw();
                //}

                //for (var i = 0; i < tblThisWeek.length; i++) {
                //    tblPetaThisWeek.row.add($(addRowPetaPerbandingan(tblThisWeek[i]))[0]).draw();
                //}

                drawTablePetaSummary(tblLastWeek, "tblPetaLastWeek");
                drawTablePetaSummary(tblThisWeek, "tblPetaThisWeek");
                
                drawTableSummarySummary(statisticLastWeek, "tblSummaryLastWeek"); 
                drawTableSummarySummary(statisticThisWeek, "tblSummaryThisWeek"); 

                $("#lblLastWeek").html("Week " + ketLastWeek[0]["Week"] + " " + ketLastWeek[0]["nameOfMonth"] + " " + ketLastWeek[0]["Year"] + " (" + ketLastWeek[0]["StartDate"] + " - " + ketLastWeek[0]["EndDate"] + ")");
                //$("#lblKetLastWeek").html("Green Block : " + summaryLastWeek[0]["totalGreenBlock"].toLocaleString('id')   + " (" + summaryLastWeek[0]["persenGreen"] + "%)" );

                //$("#lblUrlLastWeek").html("<a href='" + wmsLastWeek[0]["url"] + "' style='color:white;' target='_blank'> Goto Map </a>");
                //$("#lblUrlLastWeek").hide();


                $("#lblThisWeek").html("Week " + ketThisWeek[0]["Week"] + " " + ketThisWeek[0]["nameOfMonth"] + " " + ketThisWeek[0]["Year"] + " (" + ketThisWeek[0]["StartDate"] + " - " + ketThisWeek[0]["EndDate"] + ")");
                //$("#lblKetThisWeek").html("Green Block : " + summaryThisWeek[0]["totalGreenBlock"].toLocaleString('id') + " (" + summaryThisWeek[0]["persenGreen"] + "%)");

                //$("#lblUrlThisWeek").html("<a href='" + wmsThisWeek[0]["url"] + "' style='color:white;' target='_blank'> Goto Map </a>");
                //$("#lblUrlThisWeek").hide();

                if (bisa) {
                    $("#imgLastWeek").html("<img id='imgLastWeekimg' src='" + wmsLastWeek[0]["urlImg"] + "' alt='Image to Crop' crossorigin='anonymous'/>");
                    $("#imgThisWeek").html("<img id='imgThisWeekimg' src='" + wmsThisWeek[0]["urlImg"] + "' alt='Image to Crop' crossorigin='anonymous'/>");
                } else {
                    $("#imgLastWeek").html("<img id='imgLastWeekimg' src='" + wmsLastWeek[0]["urlGeoAndika"] + "' alt='Image to Crop' crossorigin='anonymous'/>");
                    $("#imgThisWeek").html("<img id='imgThisWeekimg' src='" + wmsThisWeek[0]["urlGeoAndika"] + "' alt='Image to Crop' crossorigin='anonymous'/>");
                }
                imgCrop = true;
                $('.loading').hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.statusText);
                $('.loading').hide()
            }
        })
    })

    $('#btnGoKondisiPiezo').click(function () {
        if ($("#selectFilterCompanyForKondisiPiezo").val() == "") {
            alert("Mohon Pilih Company");
            return;
        }

        $('.loading').show()
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/getPetaKondisiPiezoGo',
            data: '{pt: "' + $("#selectFilterCompanyForKondisiPiezo").val() + '", weekId: "' + $("#txtWeekIdPetaKondisiPiezo").val() + '", range: "' + $("#selectFilterBerturutTurutForKondisiPiezo").val() + '", indicatorID: "' + $("#selectFilterKondisiForKondisiPiezo").val() + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var header = JSON.parse(response.d[0])
                var data = JSON.parse(response.d[1]);
                var wmsThisWeek = JSON.parse(response.d[2])

                $("#lblThisWeekKondisiPiezo").html("Week " + header[0]["weekMin"] + " " + header[0]["monthMin"] + " " + header[0]["yearMin"] + " Sampai Dengan Week " + header[0]["weekMax"] + " " + header[0]["monthMax"] + " " + header[0]["yearMax"] + " (" + header[0]["StartDateMin"] + " - " + header[0]["EndDateMax"] + ")");

                drawTableSummaryKondisiPiezo(data);

                $("#imgThisWeekKondisiPiezo").html("<img id='imgThisWeekimg' src='" + wmsThisWeek[0]["url"] + "' alt='Image to Crop' crossorigin='anonymous'/>");

                $('.loading').hide()
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.statusText);
                $('.loading').hide()
            }
        })
    })

    //$('#btnSubmitFilterForPencatatanPerubahanTMATPDF').click(function () {
    //    //alert('text label=' + document.getElementById('lblTitleWeek').textContent)
    //    var klhk = 0;
    //    var checkBox = document.getElementById("checkklhk");
    //    if (checkBox.checked == true) {
    //        klhk = 1;
    //    } else {
    //        klhk = 0
    //    }
    //    /*        getDetailPencatatanPerubahan();*/
    //    $.ajax({
    //        type: 'POST',
    //        url: '../Service/MapService.asmx/GenerateReportPerubahanPencatatanTMAT',
    //        data: '{wmArea: "' + $("#selectFilterZonaPencatatanPerubahanTMAT").val() + '", WeekName: "' + $('#txtWeekId').val() + '",weekLabel: "' + document.getElementById('lblTitleWeek').textContent + '",klhk: "' + klhk + '"}',
    //        contentType: 'application/json; charset=utf-8',
    //        dataType: 'json',
    //        success: function (response) {
    //            $this.html("Weekly Report");

    //            var content = response.d;
    //            var request = new XMLHttpRequest();
    //            request.open('POST', '../Handler/GetReportHandler.ashx?reporttype=weekly&filename=' + response.d, true);
    //            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    //            request.responseType = 'blob';

    //            request.onload = function () {
    //                // Only handle status code 200
    //                if (request.status === 200) {
    //                    // Try to find out the filename from the content disposition `filename` value
    //                    var disposition = request.getResponseHeader('content-disposition');
    //                    var matches = /"([^"]*)"/.exec(disposition);
    //                    var filename = (matches != null && matches[1] ? matches[1] : response.d);

    //                    // The actual download
    //                    var blob = new Blob([request.response], { type: 'application/pdf' });
    //                    var link = document.createElement('a');
    //                    link.href = window.URL.createObjectURL(blob);
    //                    link.download = filename;

    //                    document.body.appendChild(link);

    //                    link.click();

    //                    document.body.removeChild(link);
    //                }
    //                else {
    //                    alert('File not found');
    //                }
    //            };

    //            request.send('content=' + content);
    //        },
    //        error: function (xhr, ajaxOptions, thrownError) {
    //            console.log(xhr.statusText);
    //        }
    //    })
    //});

    $("#btnSubmitFilterForPencatatanPerubahanTMAT").click(function () {
        $('.loading').show()
        var klhk = 0;
        var checkBox = document.getElementById("checkklhk");
        if (checkBox.checked == true) {
            klhk = 1;
        } else {
            klhk = 0
        }

        generateWebViewTMATPerubahan(klhk);
    });

    $("#btnGoGrapTmasTmat").click(function () {
        if ($("#selectFilterEstatePulauGrapTmasTmat").val().length == 0) {
            alert("Mohon Pilih WM Area");
            return;
        }
        if ($("#selectFilterEstateGrapTmasTmat").val().length == 0) {
            alert("Mohon Pilih Estate");
            return;
        }
        getChartTMAT();
    });

    $("#btnSaveChartTMAT").click(function () {
        saveChartTMAT();
    });

    $("#btnSaveChartTMATBlackList").click(function () {
        saveChartTMATBlackList();
    });

    $("#chkAllPiezo").click(function () {
        if (dataChartTMAT != undefined) {
            setUpChartTMAT(dataChartTMAT);
        }
    });
    $("#chkBlack").click(function () {
        if (dataChartTMAT != undefined) {
            setUpChartTMAT(dataChartTMAT);
        }
    });
    $("#chkBlackOnly").click(function () {
        if (dataChartTMAT != undefined) {
            setUpChartTMAT(dataChartTMAT);
        }
    });

    //$("#lblBlackList").click(function () {
    //    showBlackListForm();
    //});

    $("#lblGraphTmasTmatAllPzo").click(function () {
        showAlListForm();
    });
    $("#lblGraphTmasTmatSelected").click(function () {
        showBlackListForm();
    });
    $("#lblGraphTmasTmatUnSelected").click(function () {
        showWhiteListForm();
    });

    $("#chkChartTMATAll").click(function () {
        checkedTMATAll(this.checked);
    });

    $("#btnDownloadPeta").click(function () {

        /*if ($("#selectFilterCompanyForPerbandingan").val() == "PT.GAN") {
            if (imgCrop) {
                cropImageMap("imgLastWeekimg", 250)
                cropImageMap("imgThisWeekimg", 250)
            }

            imgCrop = false;

            setTimeout(function () { console.log("Executed after 2 second"); }, 2000);
        }*/

        var targetDiv = document.getElementById("divMap");
        if ($("#lblLastWeek").html() == "") {
            alert("Mohon klik Go dahulu")
            return;
        }

        $('.loading').show();
        // Menggunakan dom-to-image untuk mengubah elemen menjadi gambar
        domtoimage.toPng(targetDiv, { quality: 1 }).then(function (dataUrl) {
            // Membuat elemen <a> untuk mengunduh gambar
            var downloadLink = document.createElement("a");
            downloadLink.href = dataUrl;
            downloadLink.download = $("#selectFilterCompanyForPerbandingan").val() + " " + $("#lblLastWeek").html() + " - " + $("#lblThisWeek").html() + " Map.jpeg"; // Nama file yang diunduh

            // Menambahkan elemen <a> ke dalam dokumen
            document.body.appendChild(downloadLink);

            // Mengklik elemen <a> secara otomatis untuk memulai proses pengunduhan
            downloadLink.click();

            // Menghapus elemen <a> setelah selesai mengunduh
            document.body.removeChild(downloadLink);
            $('.loading').hide();
        })
            .catch(function (error) {
                console.error("Error:", error);
                $('.loading').hide();
            });
    })

    $("#btnDownloadPetaKondisiPiezo").click(function () {

        var targetDiv = document.getElementById("divMapKondisiPiezo");
        if ($("#lblThisWeekKondisiPiezo").html() == "") {
            alert("Mohon klik Go dahulu")
            return;
        }

        $('.loading').show();
        // Menggunakan dom-to-image untuk mengubah elemen menjadi gambar
        domtoimage.toPng(targetDiv, { quality: 1 }).then(function (dataUrl) {
            // Membuat elemen <a> untuk mengunduh gambar
            var downloadLink = document.createElement("a");
            downloadLink.href = dataUrl;
            downloadLink.download = $("#selectFilterCompanyForKondisiPiezo").val() + " " + $("#lblThisWeekKondisiPiezo").html() + " Map.jpeg"; // Nama file yang diunduh

            // Menambahkan elemen <a> ke dalam dokumen
            document.body.appendChild(downloadLink);

            // Mengklik elemen <a> secara otomatis untuk memulai proses pengunduhan
            downloadLink.click();

            // Menghapus elemen <a> setelah selesai mengunduh
            document.body.removeChild(downloadLink);
            $('.loading').hide();
        })
            .catch(function (error) {
                console.error("Error:", error);
                $('.loading').hide();
            });
    })

    $("#btnDownloadTable").click(function () {
        var targetDiv = document.getElementById("divSummary");
        if ($("#lblLastWeek").html() == "") {
            alert("Mohon klik Go dahulu")
            return;
        }

        $('.loading').show();
        // Menggunakan dom-to-image untuk mengubah elemen menjadi gambar
        domtoimage.toPng(targetDiv, { quality: 1 }).then(function (dataUrl) {
            // Membuat elemen <a> untuk mengunduh gambar
            var downloadLink = document.createElement("a");
            downloadLink.href = dataUrl;
            downloadLink.download = $("#selectFilterCompanyForPerbandingan").val() + " " + $("#lblLastWeek").html() + " - " + $("#lblThisWeek").html() + " Table.jpeg"; // Nama file yang diunduh

            // Menambahkan elemen <a> ke dalam dokumen
            document.body.appendChild(downloadLink);

            // Mengklik elemen <a> secara otomatis untuk memulai proses pengunduhan
            downloadLink.click();

            // Menghapus elemen <a> setelah selesai mengunduh
            document.body.removeChild(downloadLink);
            $('.loading').hide();
        })
            .catch(function (error) {
                console.error("Error:", error);
                $('.loading').hide();
            });
    })

    $("#btnDownloadTablKondisiPiezoe").click(function () {
        var targetDiv = document.getElementById("divSummaryKondisiPiezo");
        if ($("#lblThisWeekKondisiPiezo").html() == "") {
            alert("Mohon klik Go dahulu")
            return;
        }

        $('.loading').show();
        // Menggunakan dom-to-image untuk mengubah elemen menjadi gambar
        domtoimage.toPng(targetDiv, { quality: 1 }).then(function (dataUrl) {
            // Membuat elemen <a> untuk mengunduh gambar
            var downloadLink = document.createElement("a");
            downloadLink.href = dataUrl;
            downloadLink.download = $("#selectFilterCompanyForKondisiPiezo").val() + " " + $("#lblThisWeekKondisiPiezo").html() + " Table.jpeg"; // Nama file yang diunduh

            // Menambahkan elemen <a> ke dalam dokumen
            document.body.appendChild(downloadLink);

            // Mengklik elemen <a> secara otomatis untuk memulai proses pengunduhan
            downloadLink.click();

            // Menghapus elemen <a> setelah selesai mengunduh
            document.body.removeChild(downloadLink);
            $('.loading').hide();
        })
            .catch(function (error) {
                console.error("Error:", error);
                $('.loading').hide();
            });
    })

    $("#btnDownloadPetaLastWeek").click(function () {

        /*if ($("#selectFilterCompanyForPerbandingan").val() == "PT.GAN") {
            if (imgCrop) {
                cropImageMap("imgLastWeekimg", 250)
                cropImageMap("imgThisWeekimg", 250)
            }

            imgCrop = false;

            setTimeout(function () { console.log("Executed after 2 second"); }, 2000);
        }*/

        var targetDiv = document.getElementById("imgLastWeek");
        if ($("#lblLastWeek").html() == "") {
            alert("Mohon klik Go dahulu")
            return;
        }

        $('.loading').show();
        // Menggunakan dom-to-image untuk mengubah elemen menjadi gambar
        domtoimage.toPng(targetDiv, { quality: 1 }).then(function (dataUrl) {
            // Membuat elemen <a> untuk mengunduh gambar
            var downloadLink = document.createElement("a");
            downloadLink.href = dataUrl;
            downloadLink.download = $("#selectFilterCompanyForPerbandingan").val() + " " + $("#lblLastWeek").html() + " Map.jpeg"; // Nama file yang diunduh

            // Menambahkan elemen <a> ke dalam dokumen
            document.body.appendChild(downloadLink);

            // Mengklik elemen <a> secara otomatis untuk memulai proses pengunduhan
            downloadLink.click();

            // Menghapus elemen <a> setelah selesai mengunduh
            document.body.removeChild(downloadLink);
            $('.loading').hide();
        })
            .catch(function (error) {
                console.error("Error:", error);
                $('.loading').hide();
            });
    })

    $("#btnDownloadTableLastWeek").click(function () {
        var targetDiv = document.getElementById("summaryLastWeek");
        if ($("#lblLastWeek").html() == "") {
            alert("Mohon klik Go dahulu")
            return;
        }

        $('.loading').show();
        // Menggunakan dom-to-image untuk mengubah elemen menjadi gambar
        domtoimage.toPng(targetDiv, { quality: 1 }).then(function (dataUrl) {
            // Membuat elemen <a> untuk mengunduh gambar
            var downloadLink = document.createElement("a");
            downloadLink.href = dataUrl;
            downloadLink.download = $("#selectFilterCompanyForPerbandingan").val() + " " + $("#lblLastWeek").html() + " Table.jpeg"; // Nama file yang diunduh

            // Menambahkan elemen <a> ke dalam dokumen
            document.body.appendChild(downloadLink);

            // Mengklik elemen <a> secara otomatis untuk memulai proses pengunduhan
            downloadLink.click();

            // Menghapus elemen <a> setelah selesai mengunduh
            document.body.removeChild(downloadLink);
            $('.loading').hide();
        })
            .catch(function (error) {
                console.error("Error:", error);
                $('.loading').hide();
            });
    })

    $("#btnDownloadPetaThisWeek").click(function () {

        /*if ($("#selectFilterCompanyForPerbandingan").val() == "PT.GAN") {
            if (imgCrop) {
                cropImageMap("imgLastWeekimg", 250)
                cropImageMap("imgThisWeekimg", 250)
            }

            imgCrop = false;

            setTimeout(function () { console.log("Executed after 2 second"); }, 2000);
        }*/

        var targetDiv = document.getElementById("imgThisWeek");
        if ($("#lblLastWeek").html() == "") {
            alert("Mohon klik Go dahulu")
            return;
        }

        $('.loading').show();
        // Menggunakan dom-to-image untuk mengubah elemen menjadi gambar
        domtoimage.toPng(targetDiv, { quality: 1 }).then(function (dataUrl) {
            // Membuat elemen <a> untuk mengunduh gambar
            var downloadLink = document.createElement("a");
            downloadLink.href = dataUrl;
            downloadLink.download = $("#selectFilterCompanyForPerbandingan").val() + " " + $("#lblThisWeek").html() + " Map.jpeg"; // Nama file yang diunduh

            // Menambahkan elemen <a> ke dalam dokumen
            document.body.appendChild(downloadLink);

            // Mengklik elemen <a> secara otomatis untuk memulai proses pengunduhan
            downloadLink.click();

            // Menghapus elemen <a> setelah selesai mengunduh
            document.body.removeChild(downloadLink);
            $('.loading').hide();
        })
            .catch(function (error) {
                console.error("Error:", error);
                $('.loading').hide();
            });
    })

    $("#btnDownloadTableThisWeek").click(function () {
        var targetDiv = document.getElementById("summaryThisWeek");
        if ($("#lblLastWeek").html() == "") {
            alert("Mohon klik Go dahulu")
            return;
        }

        $('.loading').show();
        // Menggunakan dom-to-image untuk mengubah elemen menjadi gambar
        domtoimage.toPng(targetDiv, { quality: 1 }).then(function (dataUrl) {
            // Membuat elemen <a> untuk mengunduh gambar
            var downloadLink = document.createElement("a");
            downloadLink.href = dataUrl;
            downloadLink.download = $("#selectFilterCompanyForPerbandingan").val() + " " + $("#lblThisWeek").html() + " Table.jpeg"; // Nama file yang diunduh

            // Menambahkan elemen <a> ke dalam dokumen
            document.body.appendChild(downloadLink);

            // Mengklik elemen <a> secara otomatis untuk memulai proses pengunduhan
            downloadLink.click();

            // Menghapus elemen <a> setelah selesai mengunduh
            document.body.removeChild(downloadLink);
            $('.loading').hide();
        })
            .catch(function (error) {
                console.error("Error:", error);
                $('.loading').hide();
            });
    })

    $("#btnCaptureReportMap").click(function () {
        
        /*if ($("#selectFilterCompanyForPerbandingan").val() == "PT.GAN") {
            if (imgCrop) {
                cropImageMap("imgLastWeekimg", 250)
                cropImageMap("imgThisWeekimg", 250)
            }

            imgCrop = false;

            setTimeout(function () {console.log("Executed after 2 second");}, 2000);
        }*/

        var targetDiv = document.getElementById("divReportPetaPerbadingan");
        if ($("#lblLastWeek").html() == "") {
            alert("Mohon klik Go dahulu")
            return;
        }

        $('.loading').show();
        // Menggunakan dom-to-image untuk mengubah elemen menjadi gambar
        domtoimage.toPng(targetDiv, { quality: 1 }).then(function (dataUrl) {
                // Membuat elemen <a> untuk mengunduh gambar
                var downloadLink = document.createElement("a");
                downloadLink.href = dataUrl;
                downloadLink.download = $("#selectFilterCompanyForPerbandingan").val() + " " + $("#lblLastWeek").html() + " - " + $("#lblThisWeek").html()+ ".jpeg"; // Nama file yang diunduh

                // Menambahkan elemen <a> ke dalam dokumen
                document.body.appendChild(downloadLink);

                // Mengklik elemen <a> secara otomatis untuk memulai proses pengunduhan
                downloadLink.click();

                // Menghapus elemen <a> setelah selesai mengunduh
                document.body.removeChild(downloadLink);
                $('.loading').hide();
            })
            .catch(function (error) {
                console.error("Error:", error);
                $('.loading').hide();
            });
    });



    $("#btnCaptureReportMapKondisiPiezo").click(function () {

        /*if ($("#selectFilterCompanyForPerbandingan").val() == "PT.GAN") {
            if (imgCrop) {
                cropImageMap("imgLastWeekimg", 250)
                cropImageMap("imgThisWeekimg", 250)
            }

            imgCrop = false;

            setTimeout(function () {console.log("Executed after 2 second");}, 2000);
        }*/

        var targetDiv = document.getElementById("divReportKondisiPiezo");
        if ($("#lblThisWeekKondisiPiezo").html() == "") {
            alert("Mohon klik Go dahulu")
            return;
        }

        $('.loading').show();
        // Menggunakan dom-to-image untuk mengubah elemen menjadi gambar
        domtoimage.toPng(targetDiv, { quality: 1 }).then(function (dataUrl) {
            // Membuat elemen <a> untuk mengunduh gambar
            var downloadLink = document.createElement("a");
            downloadLink.href = dataUrl;
            downloadLink.download = $("#selectFilterCompanyForKondisiPiezo").val() + " " + $("#lblThisWeekKondisiPiezo").html() + ".jpeg"; // Nama file yang diunduh

            // Menambahkan elemen <a> ke dalam dokumen
            document.body.appendChild(downloadLink);

            // Mengklik elemen <a> secara otomatis untuk memulai proses pengunduhan
            downloadLink.click();

            // Menghapus elemen <a> setelah selesai mengunduh
            document.body.removeChild(downloadLink);
            $('.loading').hide();
        })
            .catch(function (error) {
                console.error("Error:", error);
                $('.loading').hide();
            });
    });

    tblListPzoChart = $("#tblListPzoChart").DataTable({
        "scrollX": true,
        dom: 'Blfrtip',
        "deferRender": true,
        "paging": false,
        buttons: []
    });

    tblListPzoChartBlackList = $("#tblListPzoChartBlackList").DataTable({
        "scrollX": true,
        dom: 'Blfrtip',
        "deferRender": true,
        "paging": false,
        buttons: [
            'excel'
        ]
    });

    getWaterDepthIndicator();
    ListCompany();
    ListCompanyPeta();
    ListCompanyKLHK();
    GenerateTableData();
    FirstGenerateTableKLHK();
    FirstGenerateTableKLHKForGenerate();
    FirstGenerateTableBlockRehab();
    ListWeekX();
    ListWeek($("#selectFilterEstateForGenerateKLHK").val());
    getCurrentWeekName();
    populateDropDownPulau();


    $("#btnGeneratePetaPerbandingan").hide();
    for (var i = 0; i < listGisUser.length; i++) {
        if (listGisUser[i]["UserID"] == uId) {
            $("#btnGeneratePetaPerbandingan").show();
            break;
        }
    }

})


function setAccessModule() {

    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetModuleAccess',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            mdlAccCode = response.d;
            if (mdlAccCode == 'PZO6' || mdlAccCode == 'PZO5') {
                document.getElementById('b').style.display = 'flex';
                document.getElementById('c').style.display = 'flex';
                document.getElementById('a').style.display = 'flex';
                document.getElementById('e').style.display = 'flex';
                document.getElementById('d').style.display = 'none';

            }
            else if (mdlAccCode == 'PZO9') {
                document.getElementById('d').style.display = 'flex';
                document.getElementById('b').style.display = 'none';
                document.getElementById('c').style.display = 'none';
                document.getElementById('a').style.display = 'none';
                document.getElementById('e').style.display = 'none';
            }
        }
    });
}

$("#btnSubmitFilterWeek").click(function () {
    var startWeekDate = $("#weekDropdown").val();
    var endWeekDate = $("#weekDropdown2").val();
    if (isNullOrWhitespace(startWeekDate) || startWeekDate == "") {
        alert('Start week tidak boleh kosong');
    } else {
        alert('kondisi terisi')
        GetDateByWeek(startWeekDate, endWeekDate);
    }
    //GetDateByWeek(startWeekDate, endWeekDate);
});
$('#btnThisWeek').click(function () {
    $('.loading').show();
    $("#lblTitleWeek").html(thisWeek)
    $("#txtWeekId").val(thisWeekId)
    $('.loading').hide();
});
$('#btnThisWeekGrapTmasTmat').click(function () {
    $('.loading').show();
    $("#lblTitleWeekGrapTmasTmat").html(thisWeek)
    $("#txtWeekIdGrapTmasTmat").val(thisWeekId)
    $('.loading').hide();
});
//perbandingan peta
$('#btnThisWeekPetaPerbandingan').click(function () {
    $('.loading').show();
    $("#lblTitleWeekPetaPerbandingan").html(thisWeek)
    $("#txtWeekIdPetaPerbandingan").val(thisWeekId)
    $('.loading').hide();
});

$('#btnThisWeekPetaPerbandinganxx').click(function () {
    $('.loading').show();
    $("#lblTitleWeekPetaPerbandinganxx").html(thisWeek)
    $("#txtWeekIdPetaPerbandinganxx").val(thisWeekId)
    $('.loading').hide();
});


$('#btnThisWeekPetaKondisiPiezo').click(function () {
    $('.loading').show();
    $("#lblTitleWeekPetaKondisiPiezo").html(thisWeek)
    $("#txtWeekIdPetaKondisiPiezo").val(thisWeekId)
    $('.loading').hide();
});

function getWaterDepthIndicator(param) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/getWaterDepthIndicator',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var data = JSON.parse(response.d);
            
            $('#selectFilterKondisiForKondisiPiezo').find('option').remove();
            for (var i = 0; i < data.length; i++) {
                if (data[i]["MinValue"] != "999") {
                    $("#selectFilterKondisiForKondisiPiezo").append($('<option>', {
                        value: data[i]["IndicatorID"],
                        text: data[i]["IndicatorName"] +" - "+ data[i]["IndicatorAliasReport"],
                        style: 'background: ' + data[i]["colorBack"] + '; color: ' + data[i]["colorFont"] + ';'
                    }));
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}

function ListCompany() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListCompany',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataSubText = response.d[2].split(";");
            var dataLength = response.d[0].split(";").length;

            $('#selectFilterCompany').find('option').remove();
            $("#selectFilterCompany").append($('<option>', {
                value: "",
                text: "All Company"
            }));
            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterCompany").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterCompany").val("").change();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListCompany : " + xhr.statusText);
        }
    });
}

function ListCompanyPeta() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListCompany',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataSubText = response.d[2].split(";");
            var dataLength = response.d[0].split(";").length;

            $('#selectFilterCompanyForPerbandingan').find('option').remove(); 
            $('#selectFilterCompanyForKondisiPiezo').find('option').remove(); 

            $("#selectFilterCompanyForPerbandingan").append($('<option>', {
                value: "",
                text: "Choose Company"
            }));
            $("#selectFilterCompanyForKondisiPiezo").append($('<option>', {
                value: "",
                text: "Choose Company"
            }));
            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterCompanyForPerbandingan").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
                $("#selectFilterCompanyForKondisiPiezo").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterCompanyForPerbandingan").val("").change();
            $("#selectFilterCompanyForKondisiPiezo").val("").change();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListCompany : " + xhr.statusText);
        }
    });
}


function ListCompanyKLHK() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListCompanyKLHK',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataSubText = response.d[2].split(";");
            var dataLength = response.d[0].split(";").length;

            $('#selectFilterCompanyForKLHK').find('option').remove();
            $("#selectFilterCompanyForKLHK").append($('<option>', {
                value: "",
                text: "All Company"
            }));
            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterCompanyForKLHK").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterCompanyForKLHK").val("").change();
            //

            $('#selectFilterCompanyForGenerateKLHK').find('option').remove();
            $("#selectFilterCompanyForGenerateKLHK").append($('<option>', {
                value: "",
                text: "Choose Company"
            }));

            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterCompanyForGenerateKLHK").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterCompanyForGenerateKLHK").val("").change();

            //
            $('#selectFilterCompanyBlockRehab').find('option').remove();
            $("#selectFilterCompanyBlockRehab").append($('<option>', {
                value: "",
                text: "Choose Company"
            }));

            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterCompanyBlockRehab").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterCompanyBlockRehab").val("").change();

            //
            $('#selectFilterAddCompanyBlockRehab').find('option').remove();
            $("#selectFilterAddCompanyBlockRehab").append($('<option>', {
                value: "",
                text: "Choose Company"
            }));

            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterAddCompanyBlockRehab").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterAddCompanyBlockRehab").val("").change();

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListCompanyKLHK : " + xhr.statusText);
        }


    });


}

function ListEstate(companycode, source) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListEstate',
        data: '{CompanyCode: "' + companycode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataLength = response.d[0].split(";").length;

            if (source == "data") {
                $('#selectFilterEstate').find('option').remove();
                $("#selectFilterEstate").append($('<option>', {
                    value: "",
                    text: "All Estate"
                }));
                $('#selectFilterEstatePencatatanPerubahanTMAT').find('option').remove();
                for (var i = 0; i < dataLength; i++) {
                    $("#selectFilterEstate").append($('<option>', {
                        value: dataValue[i],
                        text: dataValue[i] + " - " + dataText[i]
                    }));

                }

                $("#selectFilterEstate").val("");
                //$("#selectFilterEstatePencatatanPerubahanTMAT").val("");
                //GenerateTableData();
            }
            else if (source == "klhk") {
                $('#selectFilterEstateForKLHK').find('option').remove();
                $("#selectFilterEstateForKLHK").append($('<option>', {
                    value: "",
                    text: "All Estate"
                }));
                for (var i = 0; i < dataLength; i++) {
                    $("#selectFilterEstateForKLHK").append($('<option>', {
                        value: dataValue[i],
                        text: dataValue[i] + " - " + dataText[i]
                    }));
                }

                $("#selectFilterEstateForKLHK").val("");

                //
                $('#selectFilterEstateForGenerateKLHK').find('option').remove();

                for (var i = 0; i < dataLength; i++) {
                    $("#selectFilterEstateForGenerateKLHK").append($('<option>', {
                        value: dataValue[i],
                        text: dataValue[i] + " - " + dataText[i]
                    }));
                }

                $("#selectFilterEstateForGenerateKLHK").val("");


                //
                $('#selectFilterAddEstateBlockRehab').find('option').remove();

                for (var i = 0; i < dataLength; i++) {
                    $("#selectFilterAddEstateBlockRehab").append($('<option>', {
                        value: dataValue[i],
                        text: dataValue[i] + " - " + dataText[i]
                    }));
                }

                $("#selectFilterAddEstateBlockRehab").val("");

                //
                $('#selectFilterAddEstateBlockRehab').find('option').remove();

                for (var i = 0; i < dataLength; i++) {
                    $("#selectFilterAddEstateBlockRehab").append($('<option>', {
                        value: dataValue[i],
                        text: dataValue[i] + " - " + dataText[i]
                    }));
                }

                $("#selectFilterAddEstateBlockRehab").val("");
            }

            $('.selectpicker').selectpicker('refresh');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListEstate : " + xhr.statusText);
        }
    });
}

function ListBlock(estCode) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListAllBlock',
        data: '{EstCode: "' + estCode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            json.forEach(function (obj) {
                $("#selectFilterAddBlockRehab").append($('<option>', {
                    value: obj.Block,
                    text: obj.Block
                }));
            })
            $("#selectFilterAddBlockRehab").val("");
            $('.selectpicker').selectpicker('refresh');

            $("#selectFilterAddBlockRehab").val("");
        }
    });
}



function ListWeek(estcode) {

    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListMonth',
        data: '{Estcode: "' + estcode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            $('#selectFilterPeriodeForGenerateKLHK').find('option').remove();
            $("#selectFilterPeriodeForGenerateKLHK").append($('<option>', {
                value: "",
                text: "Select Month"
            }));
            json.forEach(function (obj) {
                $("#selectFilterPeriodeForGenerateKLHK").append($('<option>', {
                    value: obj.value,
                    text: obj.Text
                }));
            })
            $("#selectFilterPeriodeForGenerateKLHK").val("");
            $('.selectpicker').selectpicker('refresh');

        }
    });
}

function GenerateTableData() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = yyyy + '-' + mm + '-' + dd;
    var firstday = yyyy + '-' + mm + '-' + '01';

    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDataPiezometer',
        data: '{CompanyCode: "' + $('#selectFilterCompany').val() + '", EstCode: "' + $('#selectFilterEstate').val() + '", StartDate: "' + firstday + '", EndDate: "' + today + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            //console.log(json.length);

            var content = '<table id="tableData" class="table table-striped table-bordered" style="width:100%">';
            content += '<thead><tr>';
            content += '<th>Data Taken</th><th>Estate</th><th>Block</th><th>PiezoRecordID</th><th>Ketinggian</th><th>Indicator Name</th><th>Indicator Alias</th><th>Week Name</th></tr></thead><tbody>';

            for (var i = 0; i < json.length; i++) {
                content += '<tr>';
                content += '<td>' + json[i]["DataTaken"] + '</td>';
                content += '<td>' + json[i]["EstCode"] + '</td>';
                content += '<td>' + json[i]["Block"] + '</td>';
                content += '<td>' + json[i]["PieRecordID"] + '</td>';
                content += '<td>' + json[i]["Ketinggian"] + '</td>';
                content += '<td>' + json[i]["IndicatorName"] + '</td>';
                content += '<td>' + json[i]["IndicatorAlias"] + '</td>';
                content += '<td>' + json[i]["MonthName"] + '</td>';
                content += '</tr>';
            }

            content += '</tbody></table>';
            $("#divTableData").html(content);
            $('#tableData').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 10,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetDataPiezometer : " + xhr.statusText);
        }
    });
}

function GenerateTableKLHK(companycode, estcode, startdate, enddate) {
    //console.log(estcode + ' - ' + startdate + ' - ' + enddate);
    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDataKLHK',
        data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", StartDate: "' + startdate + '", EndDate: "' + enddate + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            console.log(json.length);

            var content = '<table id="tableKLHK" class="table table-striped table-bordered table-text-center" style="width:100%">';
            content += '<thead><tr>';
            content += '<th>ID Perusahaan</th><th style="display: none;"></th><th>Kode TPMAT</th><th>Nama  Petugas Survei</th><th>Tanggal Survei</th><th>Tinggi Muka Air Tanah</th><th>Kode Stasiun Curah Hujan</th><th>Curah Hujan</th><th style="display: none;"></th><th style="display: none;"></th><th>Ambang Baku Kerusakan Tinggi MAT</th></tr></thead><tbody>';

            for (var i = 0; i < json.length; i++) {
                content += '<tr>';
                content += '<td>' + json[i]["CompanyCode"] + '</td>';
                content += '<td style="display: none;"></td>';
                content += '<td>' + json[i]["BlockTMA"] + '</td>';
                content += '<td>' + ((json[i]["Nama Surveyor"] == "-" || json[i]["Nama Surveyor"] == null) ? "" : json[i]["Nama Surveyor"]) + '</td>';
                content += '<td>' + (json[i]["Tanggal Survey"] == null ? "" : json[i]["Tanggal Survey"]) + '</td>';
                content += '<td>' + (json[i]["Ketinggian"] == null ? "" : json[i]["Ketinggian"]) + '</td>';
                content += '<td>' + json[i]["KodeSTA"] + '</td>';
                content += '<td>' + (json[i]["CurahHujan"] == null ? "" : json[i]["CurahHujan"]) + '</td>';
                content += '<td style="display: none;"></td>';
                content += '<td style="display: none;"></td>';
                content += '<td>' + json[i]["ParameterSistem"] + '</td>';
                content += '</tr>';
            }

            content += '</tbody></table>';
            $("#divTableKLHK").html(content);
            $('#tableKLHK').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 10,
                buttons: [
                    'copy',
                    {
                        extend: 'csvHtml5',
                        title: 'RPZO002_DataKLHK',
                        fieldBoundary: '',
                        header: false,
                        footer: false
                    },
                    {
                        extend: 'excelHtml5',
                        title: 'RPZO002_DataKLHK'
                    }, {
                        extend: 'pdfHtml5',
                        title: 'RPZO002_DataKLHK'
                    }, 'print'
                ]
            });
            //$('#tableKLHK').DataTable({
            //    dom: 'Bfrtip',
            //    buttons: [
            //        'copy', 'csv', 'excel', 'pdf', 'print'
            //    ]
            //});
            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetDataKLHK : " + xhr.statusText);
        }
    });
}

function GenerateTableKLHKForGenerate(companycode, estcode, periode, year) {
    $(".loading").show();
    var currData = [];

    var da = document.getElementById("d");
    var b = document.getElementById("b");
    var h = document.getElementById("h");

    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetModuleAccess',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            mdlAccCode = response.d;
            $.ajax({
                type: 'POST',
                url: '../Service/MapService.asmx/GetDataGenerateKLHK',
                data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", Periode: "' + periode + '", Year: "' + year + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var json = $.parseJSON(response.d);
                    console.log(json.length);
                    dataJsonMI = json;
                    var _status = [];
                    var _date;
                    var content = '<table id="tableGenerateKLHK" class="table table-striped table-bordered table-text-center" style="width:100%">';
                    content += '<thead><tr>';
                    content += '<th>ID Perusahaan</th><th>EstCode</th><th style="display:none"></th><th>Kode TPMAT</th><th>Nama  Petugas Survei</th><th>Tanggal Survei</th><th>Week</th><th>Tinggi Muka Air Tanah</th><th>Kode Stasiun Curah Hujan</th><th>Curah Hujan</th><th style="display: none;"></th><th style="display: none;"></th><th>Ambang Baku Kerusakan Tinggi MAT</th><th>Hasil Generate</th><th style="display: none;">Tinggi Muka Air Tanah</th></tr></thead><tbody>';

                    if (json.length == 0) {
                        da.style.display = "none";
                        b.style.display = "none";
                    }
                    for (var i = 0; i < json.length; i++) {


                        if (json[i]["PieRecordID"] != null && json[i]["status"] == 1 && json[i]["sttBlock"] == 0 && json[i]["StatusDesc"] == "APPROVED") {
                            _status.push("2");
                            _date = json[i]["DateAction"];
                        }
                        else if (json[i]["PieRecordID"] != null && json[i]["status"] == null && json[i]["sttBlock"] == 1 && json[i]["StatusDesc"] == "APPROVED") {
                            _status.push("2");
                            _date = json[i]["DateAction"];
                        }
                        else if (json[i]["PieRecordID"] != null && json[i]["status"] == null && json[i]["sttBlock"] == 0 && json[i]["HasilGenerate"] == null) {
                            _status.push("3");
                        }
                        else if (json[i]["PieRecordID"] != null && json[i]["status"] == 0 && json[i]["sttBlock"] == 0 && json[i]["Ketinggian"] != null && json[i]["HasilGenerate"] != null) {
                            _status.push("1");
                        }
                        //if (json[i]["PieRecordID"] != null && json[i]["status"] == 1 && json[i]["sttBlock"] == 0) {
                        //    _status.push("2");
                        //    _date = json[i]["DateAction"];
                        //}
                        //else if (json[i]["PieRecordID"] != null && json[i]["status"] == 0 && json[i]["sttBlock"] == 0) {
                        //    _status.push("1");
                        //    _date = json[i]["DateAction"];
                        //}
                        //else if (json[i]["PieRecordID"] != null && json[i]["status"] == 0 && json[i]["sttBlock"] == 2) {
                        //    _status.push("1");
                        //}
                        //else if (json[i]["PieRecordID"] != null && json[i]["status"] == null && json[i]["sttBlock"] == 0 && json[i]["HasilGenerate"] == 0) {
                        //    _status.push("3");
                        //}
                        //else if (json[i]["PieRecordID"] != null && json[i]["status"] == null && json[i]["sttBlock"] == 0 && json[i]["HasilGenerate"] == null) {
                        //    _status.push("3");
                        //}
                        //else if (json[i]["PieRecordID"] != null && json[i]["status"] == 0 && json[i]["sttBlock"] == 0 && json[i]["HasilGenerate"] == 0) {
                        //    _status.push("2");
                        //}
                        //else if (json[i]["PieRecordID"] != null && json[i]["status"] == 0 && json[i]["sttBlock"] == 2 && json[i]["HasilGenerate"] == 0) {
                        //    _status.push("3");
                        //}
                        //else if (json[i]["PieRecordID"] == null && json[i]["sttBlock"] == 2) {
                        //    _status.push("2");
                        //}

                        _pierecordid = json[i]["PieRecordID"];
                        _week = json[i]["WeekName"];
                        _ketinggian = json[i]["Ketinggian"] == null ? "" : json[i]["Ketinggian"] == 0 ? 0 : json[i]["Ketinggian"] == -999 ? -999 : "-" + json[i]["Ketinggian"].toString();
                        _generate = json[i]["HasilGenerate"] == null && json[i]["PieRecordID"] != null ? 0 : (json[i]["HasilGenerate"] == null && json[i]["PieRecordID"] == null) || json[i]["sttBlock"] == 2 ? null : json[i]["HasilGenerate"] == 0 ? 0 : json[i]["HasilGenerate"] == -999 ? -999 : "-" + json[i]["HasilGenerate"].toString();
                        content += '<tr id="' + json[i]["PieRecordID"] + '">';
                        //content += '<td id="PieRecordID">' + json[i]["CompanyCode"] + '</td>';
                        content += '<td>' + json[i]["CompanyCode"] + '</td>';
                        content += '<td>' + json[i]["EstCodeTMA"] + '</td>';
                        content += '<td id="PieRecordID" name="PieRecordID" class="PieRecordID" style="display: none;">' + json[i]["PieRecordID"] + '</td>';
                        content += '<td>' + json[i]["BlockTMA"] + '</td>';
                        content += '<td>' + ((json[i]["Nama Surveyor"] == "-" || json[i]["Nama Surveyor"] == null) ? "" : json[i]["Nama Surveyor"]) + '</td>';
                        content += '<td>' + (json[i]["Tanggal Survey"] == null ? "" : json[i]["Tanggal Survey"]) + '</td>';
                        content += '<td>' + (json[i]["WeekName"] == null ? "" : json[i]["WeekName"]) + '</td>';
                        content += '<td>' + (_ketinggian) + '</td>';
                        content += '<td>' + json[i]["KodeSTA"] + '</td>';
                        content += '<td>' + (json[i]["CurahHujan"] == null ? "" : json[i]["CurahHujan"]) + '</td>';
                        content += '<td id="WeekName" name="WeekName" class="WeekName" style="display: none;">' + json[i]["WeekName"] + '</td>';
                        content += '<td style="display: none;" id="HasilUpdate">' + i + '</td>';
                        content += '<td>' + json[i]["ParameterSistem"] + '</td>';
                        if (json[i]["status"] == 1 && (json[i]["sttBlock"] == 1 || json[i]["sttBlock"] == 0)) {
                            content += '<td><input type="text" onchange="textbox(this.value,' + '\'' + _pierecordid + '\',' + '\'' + _week + '\');" value=' + _generate + ' style="width: 30px;" id="test' + i + '" name="test" class="test" disabled="disabled"></></td>';
                        }
                        else {
                            content += '<td><input type="text" onchange="textbox(this.value,' + '\'' + _pierecordid + '\',' + '\'' + _week + '\');" value=' + _generate + ' style="width: 30px;" id="test' + i + '" name="test" class="test"></></td>';
                        }

                        content += '<td style="display: none;">' + _generate + '</td>';
                        //content += '<td><i class="fa fa-pencil-square" aria-hidden="true"></i></td>';
                        content += '</tr>';
                    }

                    content += '</tbody></table>';

                    da.style.display = "none";
                    b.style.display = "none";
                    var arraycontainsturtles = (_status.indexOf("1") > -1);
                    if (arraycontainsturtles == true) {
                        document.getElementById('statusGenerate').innerHTML = "GENERATED";

                        if (mdlAccCode == "PZO6") {
                            b.style.display = "flex";
                            da.style.display = "none";
                        }
                        else {
                            da.style.display = "flex";
                            b.style.display = "none";
                        }
                    }
                    else {
                        var arraycontainsturtles = (_status.indexOf("0") > -1);
                        if (arraycontainsturtles == true) {
                            document.getElementById('statusGenerate').innerHTML = "GENERATED";
                            da.style.display = "flex";
                            b.style.display = "none";
                            e.style.display = "none";
                        }
                        else {
                            var arraycontainsturtles = (_status.indexOf("3") > -1);
                            if (arraycontainsturtles == true) {
                                document.getElementById('statusGenerate').innerHTML = "UNGENERATED";

                                if (mdlAccCode == "PZO6") {
                                    b.style.display = "flex";
                                    da.style.display = "none";
                                }
                                else {
                                    da.style.display = "none";
                                    b.style.display = "none";
                                }

                            }
                            else {
                                var arraycontainsturtles = (_status.indexOf("2") > -1);
                                document.getElementById('statusGenerate').innerHTML = "APPROVED";
                                document.getElementById('statusDate').innerHTML = _date;
                                b.style.display = "none";
                                e.style.display = "none";
                                da.style.display = "none";
                            }
                        }
                    }

                    $("#divTableGenerateKLHK").html(content);
                    $('#tableGenerateKLHK').DataTable({
                        dom: 'Blfrtip',
                        lengthMenu: [[12, 15, 24, 48, -1], [12, 15, 24, 48, "All"]],
                        pageLength: 12,
                        destroy: true,
                        stateSave: true,
                        "columnDefs": [{
                            "searchable": false,
                            "class": "center",
                            "targets": 0,
                            width: 50
                        }, {
                            "searchable": false,
                            "class": "left",
                            "targets": 1,
                            width: 50
                        }, {
                            "searchable": false,
                            "class": "center",
                            "targets": 2,
                            width: 50
                        }],

                        buttons: [
                            'copy',
                            {
                                extend: 'csvHtml5',
                                title: '',
                                fieldBoundary: '',
                                header: false,
                                footer: false
                            },
                            {
                                extend: 'excelHtml5',
                                title: '',
                                exportOptions: {
                                    columns: [0, 3, 4, 5, 14, 8, 9, 12]
                                }
                            }, {
                                extend: 'pdfHtml5',
                                title: '',
                                exportOptions: {
                                    columns: [0, 3, 4, 5, 14, 8, 9, 12]
                                }
                            },
                            'print'
                        ],
                        fnPreDrawCallback: function (oSettings) {
                            /* reset currData before each draw*/
                            currData = [];
                        },
                        fnDrawCallback: function () {
                            console.log(currData)
                        },
                        fnRowCallback: function (nRow, aData, iDisplayIndex) {
                            var info = $(this).DataTable().page.info();
                            var page = info.page;
                            var length = info.length;
                            var index = (page * length + (iDisplayIndex + 1));
                            //$('td:eq(0)', nRow).html(index);
                            currData.push(aData);
                        }
                    });

                    //$('#tableKLHK').DataTable({
                    //    dom: 'Bfrtip',
                    //    buttons: [
                    //        'copy', 'csv', 'excel', 'pdf', 'print'
                    //    ]
                    //});

                    //$("#tableGenerateKLHK").on('mousedown.edit', "i.fa.fa-pencil-square", function (e) {

                    //    $(this).removeClass().addClass("fa fa-envelope-o");
                    //    var $row = $(this).closest("tr").off("mousedown");
                    //    //var $row = $(this).closest('tr');
                    //    //var UserID = $row.find('.UserID').html();

                    //    //$tds[0].disabled = "false";
                    //    //$("#minute").removeAttr("disabled");
                    //    //$.each($tds, function (i, el) {
                    //    //    var txt = $(this).text();
                    //    //    $(this).html("").append("<input type='text' value=\"" + txt + "\">");
                    //    //});

                    //});

                    $("#tableGenerateKLHK").on('mousedown.save', "i.fa.fa-pencil-square", function (e) {

                        var $row = $(this).closest("tr");
                        var PieRecordID = $row.find("td").not(':first').not(':last').find('#PieRecordID').prevObject[0].innerText;
                        var WeekName = $row.find("td").not(':first').not(':last').find('#WeekName').prevObject[7].innerText;
                        var HasilUpdate = $row.find("td").not(':first').not(':last').find('#HasilUpdate').prevObject[8].innerText;
                        var Value = $row.find('.test').val();
                        //var UpdateTD = $("#HasilGenerate").parent('td');
                        $("#tableGenerateKLHK").DataTable().cell({ row: HasilUpdate, column: 12 }).data(Value).draw();
                        //alert(PieRecordID + '-' + WeekName);
                        //$(".loading").show();
                        //UpdateGenerateKLHK(PieRecordID, WeekName, Value);
                    });

                    $(".loading").hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("function GetDataKLHK : " + xhr.statusText);
                }
            });
        }
    });

}

function GenerateTableBlockRehab(companycode) {
    $(".loading").show();
    var currData = [];
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDataBlockRehab',
        data: '{CompanyCode: "' + companycode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            console.log(json.length);
            dataJsonMI = json;
            var content = '<table id="tableBlockRehab" class="table table-striped table-bordered table-text-center" style="width:100%">';
            content += '<thead><tr>';
            content += '<th>ID Perusahaan</th><th>EstCode</th><th>Kode TPMAT</th>><th>Action</th></tr></thead><tbody>';
            var _estcode;
            var _block;
            for (var i = 0; i < json.length; i++) {
                _estcode = json[i]["EstCodeTMA"];
                _block = json[i]["BlockTMA"];
                content += '<tr>';
                content += '<td>' + json[i]["CompanyCode"] + '</td>';
                content += '<td>' + json[i]["EstCodeTMA"] + '</td>';
                content += '<td>' + json[i]["BlockTMA"] + '</td>';
                content += '<td><button type="button" id="btnRemoveBlock" onclick = "removeblock(' + '\'' + _estcode + '\',' + '\'' + _block + '\');" class="btn btn-success pull-right">Remove</button></td>';
                content += '</tr>';
            }

            content += '</tbody></table>';
            $("#divTableBlockRehab").html(content);
            $('#tableBlockRehab').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[12, 15, 24, 48, -1], [12, 15, 24, 48, "All"]],
                pageLength: 12,
                destroy: true,
                stateSave: true,
                "columnDefs": [{
                    "searchable": false,
                    "class": "center",
                    "targets": 0,
                    width: 100
                }, {
                    "searchable": false,
                    "class": "left",
                    "targets": 1,
                    width: 150
                }, {
                    "searchable": false,
                    "class": "center",
                    "targets": 3,
                    width: 50
                }],

                buttons: [
                    'copy',
                    {
                        extend: 'csvHtml5',
                        title: '',
                        fieldBoundary: '',
                        header: false,
                        footer: false
                    },
                    {
                        extend: 'excelHtml5',
                        title: '',
                        exportOptions: {
                            columns: [0, 1, 3, 4, 5, 7, 8, 9, 12, 14]
                        }
                    }, {
                        extend: 'pdfHtml5',
                        title: '',
                        exportOptions: {
                            columns: [0, 1, 3, 4, 5, 7, 8, 9, 12, 14]
                        }
                    }, 'print'
                ]
            });

            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetDataKLHK : " + xhr.statusText);
        }
    });
}

function textbox(value, pierecordid, weekname) {
    if (_dataTableExisting.length > 1) {
        for (var i = 0; i < _dataTableExisting.length; i++) {
            if (_dataTableExisting[i].PieRecordID === pierecordid && _dataTableExisting[i].Date === weekname) {
                _dataTableExisting[i].HasilGenerate = value;
            }
            else {
                _dataTableExisting.push({ PieRecordID: pierecordid, Date: weekname, HasilGenerate: value });
            }
        }
    }
    else {
        _dataTableExisting.push({ PieRecordID: pierecordid, Date: weekname, HasilGenerate: value });
    }


}

function removeblock(estcode, block) {
    $.confirm({
        title: 'Confirm!',
        content: 'Apakah anda yakin untuk hapus data',
        buttons: {
            confirm: function () {
                $.ajax({
                    type: 'POST',
                    url: '../Service/MapService.asmx/RemoveBlock',
                    data: "{'EstCode' :'" + estcode + "','Block' :'" + block + "'}",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        //$saveButton.html("Save");
                        if (response.d == 'success') {
                            //history.go(0); //forward
                            $(".loading").hide();
                            $.alert({
                                title: 'Alert!',
                                content: 'Hapus data sukses',
                            });
                            GenerateTableBlockRehab($("#selectFilterAddCompanyBlockRehab  option:selected").val());
                        } else {
                            //_setCustomFunctions.showPopup('Info', 'Failed to save');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                    }
                });
            },
            cancel: function () {
                $(".loading").hide();
            }
        }
    });

}


function DownloadTableKLHK(companycode, estcode, startdate, enddate) {
    console.log(estcode + ' - ' + startdate + ' - ' + enddate);
    window.location.href = '../../Service/Report.ashx?estCode=' + estcode + '&startdate=' + startdate + '&enddate=' + enddate;
}

function FirstGenerateTableKLHK() {
    var content = '<table id="tableKLHK" class="table table-striped table-bordered table-text-center" style="width:100%">';
    content += '<thead><tr>';
    content += '<th>ID Perusahaan</th><th style="display: none;"></th><th>Kode TPMAT</th><th>Nama  Petugas Survei</th><th>Tanggal Survei</th><th>Tinggi Muka Air Tanah</th><th>Kode Stasiun Curah Hujan</th><th>Curah Hujan</th><th style="display: none;"></th><th style="display: none;"></th><th>Ambang Baku Kerusakan Tinggi MAT</th></tr></thead><tbody></tbody></table>';

    $("#divTableKLHK").html(content);
    $('#tableKLHK').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [[10, 15, 25, -1], [10, 15, 25, "All"]],
        pageLength: 10,
        buttons: [
            'copy',
            {
                extend: 'csvHtml5',
                title: 'RPZO002_DataKLHK',
                fieldBoundary: '',
                header: false,
                footer: false
            },
            {
                extend: 'excelHtml5',
                title: 'RPZO002_DataKLHK'
            }, {
                extend: 'pdfHtml5',
                title: 'RPZO002_DataKLHK'
            }, 'print'
        ]
    });
}

function FirstGenerateTableKLHKForGenerate() {
    var currData = [];
    var content = '<table id="tableGenerateKLHK" class="table table-striped table-bordered table-text-center" style="width:100%">';
    content += '<thead><tr>';
    content += '<th>ID Perusahaan</th><th>EstCode</th><th style="display:none"></th><th>Kode TPMAT</th><th>Nama  Petugas Survei</th><th>WeekName</th><th>Tanggal Survei</th><th>Tinggi Muka Air Tanah</th><th>Kode Stasiun Curah Hujan</th><th>Curah Hujan</th><th style="display: none;"></th><th style="display: none;"></th><th>Ambang Baku Kerusakan Tinggi MAT</th><th>Hasil Generate</th><th style="display: none;">Hasil Generate</th></tr></thead><tbody></tbody></table>';

    $("#divTableGenerateKLHK").html(content);
    $('#tableGenerateKLHK').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [[12, 15, 24, -1], [12, 15, 24, "All"]],
        pageLength: 12,
        stateSave: true,
        "columnDefs": [{
            "searchable": false,
            "class": "center",
            "targets": 0,
            width: 50
        }, {
            "searchable": false,
            "class": "left",
            "targets": 1,
            width: 50
        }, {
            "searchable": false,
            "class": "center",
            "targets": 2,
            width: 50
        }],
        buttons: [
            'copy',
            {
                extend: 'csvHtml5',
                title: '',
                fieldBoundary: '',
                header: false,
                footer: false
            },
            {
                extend: 'excelHtml5',
                title: '',
                exportOptions: {
                    columns: [0, 3, 4, 5, 14, 8, 9, 12]
                }
            }, {
                extend: 'pdfHtml5',
                title: '',
                exportOptions: {
                    columns: [0, 3, 4, 5, 14, 8, 9, 12]
                }
            }, 'print'
        ],
        fnPreDrawCallback: function (oSettings) {
            /* reset currData before each draw*/
            currData = [];
        },
        fnDrawCallback: function () {
            console.log(currData)
        },
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            var page = info.page;
            var length = info.length;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
            currData.push(aData);

            var $row = $(this).closest('tr');
            //val = td.childNodes[0].value;
            alert('aaaa');
            //_dataTableExisting.push($(aData[3]).next().html());
        }
    });
}

function FirstGenerateTableBlockRehab() {
    var currData = [];
    var content = '<table id="tableBlockRehab" class="table table-striped table-bordered table-text-center" style="width:100%">';
    content += '<thead><tr>';
    content += '<th>ID Perusahaan</th><th>EstCode</th><th>Kode TPMAT</th><th>Action</th></tr></thead><tbody>';

    $("#divTableBlockRehab").html(content);
    $('#tableBlockRehab').DataTable({
        dom: 'Blfrtip',
        lengthMenu: [[12, 15, 24, -1], [12, 15, 24, "All"]],
        pageLength: 12,
        stateSave: true,
        "columnDefs": [{
            "searchable": false,
            "class": "center",
            "targets": 0,
            width: 100
        }, {
            "searchable": false,
            "class": "left",
            "targets": 1,
            width: 150
        }, {
            "searchable": false,
            "class": "center",
            "targets": 3,
            width: 50
        }],
        fnPreDrawCallback: function (oSettings) {
            /* reset currData before each draw*/
            currData = [];
        },
        fnDrawCallback: function () {
            console.log(currData)
        },
        fnRowCallback: function (nRow, aData, iDisplayIndex) {
            var info = $(this).DataTable().page.info();
            var page = info.page;
            var length = info.length;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', nRow).html(index);
            currData.push(aData);

            var $row = $(this).closest('tr');
            //val = td.childNodes[0].value;
            //alert('aaaa');
            //_dataTableExisting.push($(aData[3]).next().html());
        }
    });
}
function ListWeekX() {
    arrSelectedWeek = [];
    //alert('ListWeekX')
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetListWeek',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            $('#weekDropdown').find('option').remove();
            $("#weekDropdown").append($('<option>', {
                value: "",
                text: "Select Week Periode"
            }));
            $('#weekDropdown2').find('option').remove();
            $("#weekDropdown2").append($('<option>', {
                value: "",
                text: "Select Week Periode"
            }));
            for (var i = 0; i < json.length; i++) {
                var newDateStart = dateFormat(new Date(parseInt(json[i]["StartDate"].replace('/Date(', '').replace(')/', ''))));
                var newDateEnd = dateFormat(new Date(parseInt(json[i]["EndDate"].replace('/Date(', '').replace(')/', ''))));
                var newDate = newDateStart + ' - ' + newDateEnd;
                arrSelectedWeek.push(newDate);
                //console.log('arr selected week = ' + arrSelectedWeek[i]);
                $("#weekDropdown").append($('<option>', {
                    value: newDate,
                    text: json[i]["WeekName"]
                }));
                $("#weekDropdown2").append($('<option>', {
                    value: newDate,
                    text: json[i]["WeekName"]
                }));
            }



            $("#weekDropdown").val("").change();

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('error');
            console.log("function ListWeekX : " + xhr.statusText);
        }
    });
}

function GetDateByWeek(startWeek, endWeek) {
    $(".loading").show();
    console.log('method get date by week');
    console.log('start week = ' + startWeek);
    console.log('end week = ' + endWeek);
    var arrCombineDate = [];
    var firstDateWeek = startWeek.split('-');
    var endDateWeek = endWeek.split('-');
    var first;
    var end;
    for (var i = 0; i < firstDateWeek.length; i++) {
        arrCombineDate.push(firstDateWeek[i]);
    }
    if (!isNullOrWhitespace(endWeek)) {
        for (var i = 0; i < endDateWeek.length; i++) {
            arrCombineDate.push(endDateWeek[i]);
            first = arrCombineDate[0];
            end = arrCombineDate[3];
            console.log('panjang end date = ' + endDateWeek.length);
            console.log('first date end date terisi = ' + first);
            console.log('end date end date terisi = ' + end);
        }
    } else {

        console.log('array kombinasi tanggal = ' + arrCombineDate);
        for (var i = 0; i < arrCombineDate.length; i++) {
            var first = arrCombineDate[0];
            var end = arrCombineDate[1];
        }
    }
    console.log('first = ' + first);
    console.log('end = ' + end);
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDataPiezometer',
        data: '{EstCode: "' + $('#selectFilterEstate').val() + '", StartDate: "' + first + '", EndDate: "' + end + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            console.log(json.length);

            var content = '<table id="tableData" class="table table-striped table-bordered" style="width:100%">';
            content += '<thead><tr>';
            content += '<th>Data Taken</th><th>Estate</th><th>Block</th><th>PiezoRecordID</th><th>Ketinggian</th><th>Indicator Name</th><th>Indicator Alias</th><th>Week Name</th></tr></thead><tbody>';

            for (var i = 0; i < json.length; i++) {
                content += '<tr>';
                content += '<td>' + json[i]["DataTaken"] + '</td>';
                content += '<td>' + json[i]["EstCode"] + '</td>';
                content += '<td>' + json[i]["Block"] + '</td>';
                content += '<td>' + json[i]["PieRecordID"] + '</td>';
                content += '<td>' + json[i]["Ketinggian"] + '</td>';
                content += '<td>' + json[i]["IndicatorName"] + '</td>';
                content += '<td>' + json[i]["IndicatorAlias"] + '</td>';
                content += '<td>' + json[i]["MonthName"] + '</td>';
                content += '</tr>';
            }

            content += '</tbody></table>';
            $("#divTableData").html(content);
            $('#tableData').DataTable({
                dom: 'Blfrtip',
                lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                pageLength: 10,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('error');
            console.log("function ListWeekX : " + xhr.statusText);
        }
    });
}
//function setDate(DateValue) {
//    console.log('method SetDate DateValue = ' + DateValue);
//}
function dateFormat(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = dd + '/' + mm + '/' + yyyy;
}

function dateFormat2(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = yyyy + '' + mm + '' + dd;
}

function dateFormat3(date) {
    const MN = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    console.log('dateformat 3 = ' + date);
    var monthNames = MN[date.getMonth() + 1];
    var dd = date.getDate();
    console.log('date = ' + dd);
    var mm = date.getMonth() + 1; //January is 0!
    console.log('month = ' + mm)
    var yyyy = date.getFullYear();
    var hh = date.getHours();
    var min = date.getMinutes();
    var ss = date.getSeconds();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = dd + ' -  ' + monthNames + ' - ' + yyyy + ' : ' + zeroPad(hh, 2) + ':' + zeroPad(min, 2) + ':' + zeroPad(ss, 2);
}

function isNullOrWhitespace(input) {

    if (typeof input === 'undefined' || input == null) return true;

    return input.replace(/\s/g, '').length < 1;
}

function dateFormat5(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();
    var hh = date.getHours();
    var min = date.getMinutes();
    var ss = date.getSeconds();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = mm + '/' + dd + '/' + yyyy + ' ' + zeroPad(hh, 2) + ':' + zeroPad(min, 2) + ':' + zeroPad(ss, 2);;
}

function dateFormat6(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = mm + '/' + dd + '/' + yyyy;
}

function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
}


//download Template file KLHK

function DownloadTemplateKLHK(uploader) {
    window.location.href = '../../Service/Download.ashx'
    $(".loading").hide();
}

//Upload File KLHK

function fasterPreview(uploader) {
    if (uploader.files && uploader.files[0]) {
        $profileImage.attr('src',
            window.URL.createObjectURL(uploader.files[0]));
    }
}

function validateFileUpload(_idPict) {
    var fuData = document.getElementById('fileUpload');
    var FileUploadPath = fuData.value;

    if (FileUploadPath == '') {
        $.alert({
            title: 'Alert!',
            content: 'Please upload an file',
        });
        $(".loading").hide();

    } else {
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();



        if (Extension == "xlsx") {


            if (fuData.files && fuData.files[0]) {

                var size = fuData.files[0].size;

                var reader = new FileReader();
                var userID;

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(fuData.files[0]);

                $.ajax({
                    type: 'POST',
                    url: '../Service/MapService.asmx/GetUserID',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        userID = response.d;
                        saveDataUpload(_idPict, Extension, userID);
                    }
                });
                //}
            }

        }


        else {
            $.alert({
                title: 'Alert!',
                content: 'File only allows file types of excel.',
            });
        }
    }
}

function saveDataUpload(_idPict, Extension, userID) {
    var _ext = $('#fileUpload')[0].value.split('.')[1];
    var FileName = _idPict + "." + _ext
    var filename = $('#fileUpload')[0];
    var _ext = $('#fileUpload')[0].value.split('.')[1];
    //const timestampSeconds = Math.round((new Date()).getTime() / 1000);
    //console.log(timestampSeconds);
    //var _msg = $("#selectFilterEstate option:selected").val() + "_" + periode + "_" + timestampSeconds;
    var files = filename.files;
    var userID;
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
        data.append(FileName, files[i]);

    }


    $.ajax({
        type: 'POST',
        url: '../Service/ImportKLHKHandler.ashx?userID=' + userID,
        data: data,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response == 'berhasil') {
                $(".loading").hide();
                $.alert({
                    title: 'Alert!',
                    content: 'import data excel sukses',
                });
                $('#mdlImportData').modal('hide');
                history.go(0);
                //GenerateTableKLHKForGenerate($("#selectFilterCompanyForGenerateKLHK option:selected").val(), $("#selectFilterEstateForGenerateKLHK").val(), $("#selectFilterPeriodeForGenerateKLHK option:selected").val());
            } else {
                //_setCustomFunctions.showPopup('Info', 'Failed to save');
                $(".loading").hide();
                $.alert({
                    title: 'Alert!',
                    content: 'import data excel gagal \n' + response,
                });
                history.go(0); //forward
            }
        }

    });
}

function UpdateGenerateKLHK(PieRecordID, WeekName, Value) {

    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/UpdateGenerateKLHK',
        data: "{'PieRecordID' :'" + PieRecordID + "','WeekName' :'" + WeekName + "','HasilGenerate' :'" + Value + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            //$saveButton.html("Save");
            if (response.d == 'success') {
                //history.go(0); //forward
                $(".loading").hide();
                GenerateTableKLHKForGenerate($("#selectFilterCompanyForGenerateKLHK  option:selected").val(), $("#selectFilterEstateForGenerateKLHK  option:selected").val(), $("#selectFilterPeriodeForGenerateKLHK  option:selected").val(), $("#selectFilterYearForGenerateKLHK  option:selected").val());
            } else {
                //_setCustomFunctions.showPopup('Info', 'Failed to save');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function UpdateDataKLHK() {
    var table = $('#tableGenerateKLHK').DataTable();

    var datatable = [];
    var datatableWeek = [];
    var headers = [];

    //$('#tableGenerateKLHK th').each(function (index, item) {
    //    headers[index] = $(item).html();
    //});
    //$('#tableGenerateKLHK tr').has('td').each(function () {
    //    var arrayItem = {};
    //    $('td', $(this)).each(function (index, item) {
    //        arrayItem[headers[index]] = $(item).html();
    //    });
    //    datatable.push(arrayItem);
    //});
    var td = "";
    var val = "";
    var data = table
        .rows()
        .data();
    var dataGenerate = "";
    for (var i = 0; i < data.length; i++) {
        td = table.cell(i, 12).node();
        val = td.childNodes[0].value;
        datatable.push({ PieRecordID: data[i][1], Date: data[i][9], HasilGenerate: val });
        datatableWeek.push({ Date: data[i][9], });
    }
    console.log(datatable);
    //    for (var i = 0 ; i < data.length; i++) {
    //        datatable.push({
    //            PieRecordID: data[i][1], Date: data[i][8], HasilGenerate: data[i][12]
    //        })
    //        datatableWeek.push({
    //            Date: data[i][8]
    //        })
    //    }
    var resulWeek = datatableWeek.reduce((unique, o) => {
        if (!unique.some(obj => obj.Date === o.Date)) {
            unique.push(o);
        }
        return unique;
    }, []);
    var datatable = _dataTableExisting.reduce((unique, o) => {
        if (!unique.some(obj => obj.Date === o.Date && obj.PieRecordID === o.PieRecordID)) {
            unique.push(o);
        }
        return unique;
    }, []);
    var _datatable = { ListPiezo: datatable, ListWeek: resulWeek, Month: $("#selectFilterPeriodeForGenerateKLHK").val(), Year: $("#selectFilterYearForGenerateKLHK").val() };
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/UpdateGenerateKLHK',
        data: JSON.stringify({ dataobject: JSON.stringify(_datatable) }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response.d == 'success') {
                $.alert({
                    title: 'Alert!',
                    content: 'Update sukses',
                });
                $(".loading").hide();
                GenerateTableKLHKForGenerate($("#selectFilterCompanyForGenerateKLHK option:selected").val(), $("#selectFilterEstateForGenerateKLHK").val(), $("#selectFilterPeriodeForGenerateKLHK option:selected").val(), $("#selectFilterYearForGenerateKLHK  option:selected").val());
            }
        }
    });

}

function GenerateDataKLHK(companycode, estcode, month) {
    //console.log(dataJson);

    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDataLastWeek',
        data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", Month: "' + month + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            //console.log(json.length);
            dataJsonML = json;
            dataJsonMI;
            //console.log(dataJsonML);
            //console.log(dataJsonMI);
            var datas = [];
            var datas2 = [];
            var dataFinal = [];
            var dateWeek = [];
            var HasilGenerate;
            for (var i = 0; i < dataJsonMI.length; i++) {
                dataJsonMI[i].PieRecordID;
                if (dataJsonMI[i].sttBlock != 2) {
                    for (var j = 0; j < dataJsonML.length; j++) {
                        if (dataJsonMI[i].EstCodeTMA == dataJsonML[j].EstCodeTMA && dataJsonMI[i].PieRecordID == dataJsonML[j].PieRecordID && dataJsonMI[i].Counting == dataJsonML[j].Counting && dataJsonMI[i].Counting == 1) {
                            dataJsonML[j].PieRecordID;
                            if (dataJsonMI[i].status == 0 || dataJsonMI[i].status == null) {
                                if (dataJsonML[j].HasilGenerateML == 0) {
                                    HasilGenerate = dataJsonMI[i].Ketinggian;
                                }
                                else {

                                    if (dataJsonMI[i].Ketinggian == -999 || dataJsonMI[i].Ketinggian == 999) {
                                        HasilGenerate = -999;
                                    }
                                    else if (dataJsonMI[i].Ketinggian > dataJsonML[j].HasilGenerateML && dataJsonMI[i].CurahHujan > dataJsonML[j].CurahHujanML) {
                                        HasilGenerate = dataJsonMI[i].Ketinggian;
                                    }
                                    else if (dataJsonMI[i].Ketinggian > dataJsonML[j].HasilGenerateML && dataJsonMI[i].CurahHujan < dataJsonML[j].CurahHujanML) {
                                        HasilGenerate = dataJsonMI[i].Ketinggian;
                                    }
                                    else if (dataJsonMI[i].Ketinggian == null) {
                                        HasilGenerate = null;
                                    }
                                    else if ((dataJsonMI[i].Ketinggian - dataJsonML[j].HasilGenerateML) < 5) {
                                        HasilGenerate = dataJsonMI[i].Ketinggian;
                                    }
                                    else {
                                        HasilGenerate = dataJsonML[j].HasilGenerateML - (Math.floor(Math.random() * 5) + 1);
                                    }
                                }
                                datas.push({
                                    EstCode: dataJsonMI[i].EstCodeTMA, PieRecordID: dataJsonMI[i].PieRecordID, CurahHujan: dataJsonMI[i].CurahHujan,
                                    CurahHujanML: dataJsonML[j].CurahHujanML, Ketinggian: dataJsonMI[i].Ketinggian,
                                    KetinggianML: dataJsonML[j].KetinggianML, HasilGenerateML: dataJsonML[j].HasilGenerateML,
                                    HasilGenerate: HasilGenerate, Date: dataJsonMI[i].WeekName

                                });

                                dateWeek.push({
                                    Date: dataJsonMI[i].WeekName
                                })
                            }

                        }
                        else if (dataJsonMI[i].Counting == 2) {

                            datas2.push({
                                EstCode: dataJsonMI[i].EstCodeTMA, PieRecordID: dataJsonMI[i].PieRecordID,
                                CurahHujan: dataJsonMI[i].CurahHujan, Ketinggian: dataJsonMI[i].Ketinggian, Date: dataJsonMI[i].WeekName

                            });
                        }
                    }
                }
                else if (dataJsonMI[i].PieRecordID != null && dataJsonMI[i].Ketinggian == null) {
                    $(".loading").hide();
                    $.alert({
                        title: 'Alert!',
                        content: 'Mohon periksa data ketinggian Piezorecord : ' + dataJsonMI[i].PieRecordID + ' pada bulan : ' + month,
                    });
                    return false;
                }

            }
            var result = datas.reduce((unique, o) => {
                if (!unique.some(obj => obj.EstCode === o.EstCode && obj.PieRecordID === o.PieRecordID && obj.Date === o.Date)) {
                    unique.push(o);
                }
                return unique;
            }, []);

            var resulWeek = dateWeek.reduce((unique, o) => {
                if (!unique.some(obj => obj.EstCode === o.EstCode && obj.PieRecordID === o.PieRecordID && obj.Date === o.Date)) {
                    unique.push(o);
                }
                return unique;
            }, []);
            var resulData = datas2.reduce((unique, o) => {
                if (!unique.some(obj => obj.EstCode === o.EstCode && obj.PieRecordID === o.PieRecordID && obj.Date === o.Date)) {
                    unique.push(o);
                }
                return unique;
            }, []);

            //console.log(datas);
            console.log(result);
            console.log(resulWeek);
            //console.log(resulData);


            for (var i = 0; i < resulData.length; i++) {
                for (var j = 0; j < result.length; j++) {
                    if (resulData[i].EstCode == result[j].EstCode && resulData[i].PieRecordID == result[j].PieRecordID) {
                        dataJsonML[j].PieRecordID;
                        if (result[j].HasilGenerate == 0) {
                            HasilGenerate = resulData[i].Ketinggian;
                        }
                        else {

                            if (resulData[i].Ketinggian == -999 || resulData[i].Ketinggian == 999) {
                                HasilGenerate = -999;
                            }
                            else if (resulData[i].Ketinggian > result[j].HasilGenerate && resulData[i].CurahHujan > result[j].CurahHujan) {
                                HasilGenerate = resulData[i].Ketinggian;
                            }
                            else if (resulData[i].Ketinggian > result[j].HasilGenerate && resulData[i].CurahHujan < result[j].CurahHujan) {
                                HasilGenerate = resulData[i].Ketinggian;
                            }
                            else if (resulData[i].Ketinggian == null) {
                                HasilGenerate = resulData[i].Ketinggian;
                            }
                            else if ((resulData[i].Ketinggian - result[j].HasilGenerate) < 5) {
                                HasilGenerate = resulData[i].Ketinggian;
                            }
                            else {
                                HasilGenerate = (result[j].HasilGenerate) - (Math.floor(Math.random() * 5) + 1);
                            }
                        }
                        dataFinal.push({
                            PieRecordID: resulData[i].PieRecordID, CurahHujan: resulData[i].CurahHujan,
                            CurahHujanML: result[j].CurahHujan, Ketinggian: resulData[i].Ketinggian,
                            KetinggianML: result[j].Ketinggian, HasilGenerateML: result[j].HasilGenerate,
                            HasilGenerate: HasilGenerate, Date: resulData[i].Date

                        });

                        resulWeek.push({
                            Date: resulData[i].Date
                        })
                    }
                }

            }
            var resulWeek = resulWeek.reduce((unique, o) => {
                if (!unique.some(obj => obj.Date === o.Date)) {
                    unique.push(o);
                }
                return unique;
            }, []);
            var finaldata = dataFinal.concat(result);
            console.log(finaldata);

            var _datatable = { ListPiezo: finaldata, ListWeek: resulWeek, CompanyCode: companycode, EstCode: estcode.length == 0 ? "" : estcode.toString(), Month: $("#selectFilterPeriodeForGenerateKLHK").val(), Year: 2020 };
            $.ajax({
                type: 'POST',
                url: '../Service/MapService.asmx/GenerateKLHK',
                data: JSON.stringify({ dataobject: JSON.stringify(_datatable) }),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    if (response.d == 'success') {
                        $.alert({
                            title: 'Alert!',
                            content: 'Generate sukses',
                        });
                        $(".loading").hide();
                        GenerateTableKLHKForGenerate($("#selectFilterCompanyForGenerateKLHK option:selected").val(), $("#selectFilterEstateForGenerateKLHK").val(), $("#selectFilterPeriodeForGenerateKLHK option:selected").val(), $("#selectFilterYearForGenerateKLHK  option:selected").val());
                    }
                }
            });
        }
    });
}

function SubmitFinalKLHK(companycode, estcode, month) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/SubmitFinalDataKLHK',
        data: "{'CompanyCode' :'" + companycode + "','EstCode' :'" + estcode + "','Periode' :'" + month + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response.d == 'success') {
                $.alert({
                    title: 'Alert!',
                    content: 'Submit sukses',
                });
                $(".loading").hide();
                //history.go(0); //forward
                GenerateTableKLHKForGenerate($("#selectFilterCompanyForGenerateKLHK option:selected").val(), $("#selectFilterEstateForGenerateKLHK").val(), $("#selectFilterPeriodeForGenerateKLHK option:selected").val(), $("#selectFilterYearForGenerateKLHK  option:selected").val());
            } else {
                $.alert({
                    title: 'Alert!',
                    content: 'Submit gagal',
                });
                //_setCustomFunctions.showPopup('Info', 'Failed to save');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function CheckNullData(companycode, estcode, month, year) {
    $(".loading").show();
    var currData = [];
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/CheckNullData',
        data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", Periode: "' + month + '", Year: "' + year + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            console.log(json.length);
            dataJsonMI = json;
            var _status = [];
            var _date;
            var _year;
            var _month;
            var _logger;
            var content = '<table id="TableDetailNullData" class="table table-striped table-bordered table-text-center" style="width:100%">';
            content += '<thead><tr>';
            content += '<th>Company Code</th><th>EstCode</th><th>PieRecordID</th><th>Block</th><th>WeekName</th></tr></thead><tbody>';

            for (var i = 0; i < json.length; i++) {

                content += '<tr id="' + json[i]["PieRecordID"] + '">';
                content += '<td>' + json[i]["CompanyCode"] + '</td>';
                content += '<td>' + json[i]["EstCode"] + '</td>';
                //content += '<td id="PieRecordID">' + json[i]["CompanyCode"] + '</td>';
                content += '<td>' + json[i]["PieRecordID"] + '</td>';
                content += '<td>' + json[i]["BlockTMA"] + '</td>';
                content += '<td>' + json[i]["WeekName"] + '</td>';
                content += '</tr>';
            }

            content += '</tbody></table>';

            $("#divTableDetailNullData").html(content);
            $('#TableDetailNullData').DataTable({
                dom: 'lfrtip',
                lengthMenu: [[12, 15, 24, 48, -1], [12, 15, 24, 48, "All"]],
                pageLength: 12,
                destroy: true,
                stateSave: true,
            });

            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetDataKLHK : " + xhr.statusText);
        }
    });

}

function SaveBlockRehab() {

    var FinalBlock = BlockRehab.reduce((unique, o) => {
        if (!unique.some(obj => obj.Block === o.Block)) {
            unique.push(o);
        }
        return unique;
    }, []);

    var _datatable = { ListBlock: FinalBlock };
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/SaveBlockRehab',
        data: JSON.stringify({ dataobject: JSON.stringify(_datatable) }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response.d == 'success') {
                $.alert({
                    title: 'Alert!',
                    content: 'Submit sukses',
                });
                $(".loading").hide();
                $('#mdlblockRehab').modal('toggle');
                //history.go(0); //forward
                GenerateTableBlockRehab($("#selectFilterAddCompanyBlockRehab  option:selected").val());
            } else {
                //_setCustomFunctions.showPopup('Info', 'Failed to save');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function ClearBlockRehab() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ClearBlockRehab',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response.d == 'success') {
                $.alert({
                    title: 'Alert!',
                    content: 'Clear data sukses',
                });
                $(".loading").hide();
                //history.go(0); //forward
                //GenerateTableBlockRehab($("#selectFilterAddCompanyBlockRehab  option:selected").val());
            } else {
                //_setCustomFunctions.showPopup('Info', 'Failed to save');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}
function getZonaByEstate() {
    console.log('get zona by estate');
    $('#selectFilterZonaPencatatanPerubahanTMAT').find('option').remove();
    arrZonaDropdown = [];
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/getZonaByEstate',
        data: '{estCode: "' + $("#selectFilterEstatePencatatanPerubahanTMAT").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            //console.log('data zonanya=' + JSON.stringify(response))
            var json = JSON.parse(response.d);
            if (json.length > 0) {
                for (var i = 0; i < json.length; i++) {
                    $("#selectFilterZonaPencatatanPerubahanTMAT").append($('<option>', {
                        value: json[i]["WMA_CODE"],
                        text: json[i]["NamaZona"]
                    }));
                    arrZonaDropdown.push(json[i]["WMA_CODE"]);

                }
                $("#selectFilterZonaPencatatanPerubahanTMAT").val(arrZonaDropdown)
                $('.selectpicker').selectpicker('refresh');


            } else {
                $("#selectFilterZonaPencatatanPerubahanTMAT").find('option').remove();
                $('.selectpicker').selectpicker('refresh');
            }
            $('.loading').hide()
            //history.go(0); //forward
            //GenerateTableBlockRehab($("#selectFilterAddCompanyBlockRehab  option:selected").val());
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });

}

function getCurrentWeekName() {
    var today = new Date();
    var day = today.getDate();
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/getWeeknameforPencatatanPerubahan',
        data: '{dayparam: "' + day + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            console.log('data waktunya=' + JSON.stringify(response))
            var json = JSON.parse(response.d);
            if (json.length > 0) {
                $("#lblTitleWeek").html(json[0]["MonthName"])
                $("#txtWeekId").val(json[0]["ID"])
                $("#lblTitleWeekGrapTmasTmat").html(json[0]["MonthName"])
                $("#txtWeekIdGrapTmasTmat").val(json[0]["ID"])
                $("#lblTitleWeekPetaPerbandingan").html(json[0]["MonthName"])
                $("#txtWeekIdPetaPerbandingan").val(json[0]["ID"])
                $("#lblTitleWeekPetaKondisiPiezo").html(json[0]["MonthName"])
                $("#txtWeekIdPetaKondisiPiezo").val(json[0]["ID"])
                thisWeek = json[0]["MonthName"];
                thisWeekId = json[0]["ID"];

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });

}
function getPrevNextWeekName(param) {
    var value = $("#txtWeekId").val()
    if (param == 1) {
        value = parseInt(value) + 1
    } else if (param == 0) {
        if (value - 1 != 0) {
            value = value - 1
        }
    }
    $("#txtWeekId").val(value);
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/getNextPrevWeekNameForPencatatanPerubahan',
        data: '{weekId: "' + $("#txtWeekId").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
           // alert (response.d)
            var json = JSON.parse(response.d);
            if (json.length > 0) {
                $("#lblTitleWeek").html(json[0]["MonthName"])
                $("#txtWeekId").val(json[0]["ID"])
            }
            //history.go(0); //forward
            //GenerateTableBlockRehab($("#selectFilterAddCompanyBlockRehab  option:selected").val());
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });

}

function getPrevNextWeekPerbandingan(param) {
    var value = $("#txtWeekIdPetaPerbandingan").val()
    if (param == 1) {
        value = parseInt(value) + 1
    } else if (param == 0) {
        if (value - 1 != 0) {
            value = value - 1
        }
    }
    $("#txtWeekIdPetaPerbandingan").val(value);
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/getNextPrevWeekNamePerbandinganPeta',
        data: '{weekId: "' + $("#txtWeekIdPetaPerbandingan").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            //alert(response.d);
            var json = JSON.parse(response.d);
            if (json.length > 0) {
                $("#lblTitleWeekPetaPerbandingan").html(json[0]["MonthName"])
                $("#txtWeekIdPetaPerbandingan").val(json[0]["ID"])
            }
            //history.go(0); //forward
            //GenerateTableBlockRehab($("#selectFilterAddCompanyBlockRehab  option:selected").val());
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });

}

function getPrevNextWeekPerbandinganxx(param) {
    var value = $("#txtWeekIdPetaPerbandinganxx").val()
    if (param == 1) {
        value = parseInt(value) + 1
    } else if (param == 0) {
        if (value - 1 != 0) {
            value = value - 1
        }
    }
    $("#txtWeekIdPetaPerbandinganxx").val(value);
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/getNextPrevWeekNamePerbandinganPetaxx',
        data: '{weekId: "' + $("#txtWeekIdPetaPerbandinganxx").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            //alert(response.d);
            var json = JSON.parse(response.d);
            if (json.length > 0) {
                $("#lblTitleWeekPetaPerbandinganxx").html(json[0]["MonthName"])
                $("#txtWeekIdPetaPerbandinganxx").val(json[0]["ID"])
            }
            //history.go(0); //forward
            //GenerateTableBlockRehab($("#selectFilterAddCompanyBlockRehab  option:selected").val());
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });

}

function getPrevNextWeekPerbandinganKondisiPiezo(param) {
    var value = $("#txtWeekIdPetaKondisiPiezo").val()
    if (param == 1) {
        value = parseInt(value) + 1
    } else if (param == 0) {
        if (value - 1 != 0) {
            value = value - 1
        }
    }
    $("#txtWeekIdPetaKondisiPiezo").val(value);
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/getNextPrevWeekNamePerbandinganPeta',
        data: '{weekId: "' + $("#txtWeekIdPetaKondisiPiezo").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            //alert(response.d);
            var json = JSON.parse(response.d);
            if (json.length > 0) {
                $("#lblTitleWeekPetaKondisiPiezo").html(json[0]["MonthName"])
                $("#txtWeekIdPetaKondisiPiezo").val(json[0]["ID"])
            }
            //history.go(0); //forward
            //GenerateTableBlockRehab($("#selectFilterAddCompanyBlockRehab  option:selected").val());
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function getDetailPencatatanPerubahan() {

    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/getPencatatanPerubahanPiezometer',
        data: '{weekId: "' + $("#txtWeekId").val() + '", wmarea: "' + $("#selectFilterZonaPencatatanPerubahanTMAT").val() + '", estCode:"' + $("#selectFilterEstatePencatatanPerubahanTMAT").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            console.log('data zonanya=' + JSON.stringify(response))
            var json = JSON.parse(response.d);
            //if (json.length > 0) {
            //    $("#lblTitleWeek").html(json[0]["WeekName"])
            //    $("#txtWeekId").val(json[0]["ID"])
            //}
            //history.go(0); //forward
            //GenerateTableBlockRehab($("#selectFilterAddCompanyBlockRehab  option:selected").val());
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });

}
function populateDropDownPulau() {
    $('.loading').show()
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/ListWMArea',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            //console.log('data zonanya=' + JSON.stringify(response))
            var json = JSON.parse(response.d);
            if (json.length > 0) {
                $("#selectFilterEstatePulauPerubahanTMAT").find('option').remove();
                $("#selectFilterEstatePulauGrapTmasTmat").find('option').remove();
                $("#selectFilterWmArea_j").find('option').remove();

                $("#selectFilterWmArea_j").append($('<option>', {
                    value: "",
                    text: "Nothing selected",
                    disabled:true
                }));

                for (var i = 0; i < json.length; i++) {
                    $("#selectFilterEstatePulauPerubahanTMAT").append($('<option>', {
                        value: json[i]["idWMArea"],
                        text: json[i]["wmAreaName"]
                    }));

                    $("#selectFilterEstatePulauGrapTmasTmat").append($('<option>', {
                        value: json[i]["idWMArea"],
                        text: json[i]["wmAreaName"]
                    }));

                    $("#selectFilterWmArea_j").append($('<option>', {
                        value: json[i]["idWMArea"],
                        text: json[i]["wmAreaName"]
                    }));



                    ArrPulauDropdown.push(json[i]["idWMArea"]);
                }
            
            }
            
            $('#selectFilterWmArea_j').selectpicker('val', '');
            $('#selectFilterWmArea_j').selectpicker('refresh');

            
            $('.loading').hide()

        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('.loading').hide()
        }
    });

}
function populateEstateByPulau() {
    arrEstateDropdown = [];
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/getEstateByWMArea',
        data: '{wmArea: "' + $("#selectFilterEstatePulauPerubahanTMAT").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            console.log('data zonanya=' + JSON.stringify(response))
            var json = JSON.parse(response.d);
            if (json.length > 0) {
                $("#selectFilterEstatePencatatanPerubahanTMAT").find('option').remove();
                for (var i = 0; i < json.length; i++) {
                    $("#selectFilterEstatePencatatanPerubahanTMAT").append($('<option>', {
                        value: json[i]["estCode"],
                        text: json[i]["EstName"]
                    }));
                    arrEstateDropdown.push(json[i]["estCode"]);
                }

            }
            $("#selectFilterEstatePencatatanPerubahanTMAT").val(arrEstateDropdown)
            $('.selectpicker').selectpicker('refresh');
            getZonaByEstate();
            //document.getElementsByClassName("actions-btn bs-select-all btn btn-default").click();
            $('.loading').hide()
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });


}
function generateWebViewTMATPerubahan(param) {
    var ele = document.getElementsByName('check');
    for (i = 0; i < ele.length; i++) {
        if (ele[i].checked)
            param = ele[i].value;
    }
    var total2 = 0;

    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/getDataPerubahanTMAT',
        data: '{WMACode: "' + $("#selectFilterZonaPencatatanPerubahanTMAT").val() + '", weekName:"' + $('#txtWeekId').val() + '",flagKlhk:"' + param + '", estCode:"' + $("#selectFilterEstatePencatatanPerubahanTMAT").val() + '",pulau:"' + $("#selectFilterEstatePulauPerubahanTMAT").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            //console.log('data zonanya=' + JSON.stringify(response))
            var json = JSON.parse(response.d[0]);
            //console.log('array json =' + JSON.stringify(response));
            if (json.length > 0) {
                var content = '<table id="tableData2" class="table table-striped table-bordered tableData2" style="width:100%">';
                content += '<thead><tr>';
                content += '<th rowspan = "2">Status perubahan</th><th rowspan = "2">Perubahan TMAT</th><th colspan = "6"><center>TMAT Minggu ini </center></th> <th rowspan = "2" style="text-align:right"> total </th> <th  rowspan = "2" style="text-align:right"> persen </th> </tr>';
                content += '<tr>';
                content += '<th style="text-align:right"> <0Cm </th> <th style="text-align:right">1-34cm</th><th style="text-align:right">35-40cm</th> <th style="text-align:right">41-60cm</th> <th style="text-align:right">61-65cm</th> <th style="text-align:right"> >65cm </th> </tr>';
                content += '</thead>';

                for (var i = 0; i < json.length; i++) {

                    if (i == 0) {
                        content += '<tr>';
                        content += '<td>' + 'S' + (parseInt(i) + parseInt(1)) + '</td>';
                        content += '<td>' + json[i]["Ket"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F">' + json[i]["<0cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i]["1-34cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#e93224 ">' + json[i]["35-40cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#e93224  ">' + json[i]["41-60cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#e93224  ">' + json[i]["61-65cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#e93224  ">' + json[i][">65cm"] + '</td>';
                        content += '<td style="text-align:right;">' + json[i]["Total"] + '</td>';
                        content += '<td style="text-align:right; ">' + json[i]["persen"] + '</td>';
                        content += '</tr>';
                    } else if (i == 1) {
                        content += '<tr>';
                        content += '<td>' + 'S' + (parseInt(i) + parseInt(1)) + '</td>';
                        content += '<td>' + json[i]["Ket"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc">' + json[i]["<0cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc ">' + json[i]["1-34cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#a0ce62 ">' + json[i]["35-40cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#e93224  ">' + json[i]["41-60cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#e93224  ">' + json[i]["61-65cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#e93224  ">' + json[i][">65cm"] + '</td>';
                        content += '<td style="text-align:right;">' + json[i]["Total"] + '</td>';
                        content += '<td style="text-align:right; ">' + json[i]["persen"] + '</td>';
                        content += '</tr>';
                    } else if (i == 2) {
                        content += '<tr>';
                        content += '<td>' + 'S' + (parseInt(i) + parseInt(1)) + '</td>';
                        content += '<td>' + json[i]["Ket"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc">' + json[i]["<0cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc ">' + json[i]["1-34cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc ">' + json[i]["35-40cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i]["41-60cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i]["61-65cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i][">65cm"] + '</td>';
                        content += '<td style="text-align:right;">' + json[i]["Total"] + '</td>';
                        content += '<td style="text-align:right; ">' + json[i]["persen"] + '</td>';
                        content += '</tr>';
                    } else if (i == 3) {
                        content += '<tr>';
                        content += '<td>' + 'S' + (parseInt(i) + parseInt(1)) + '</td>';
                        content += '<td>' + json[i]["Ket"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc">' + json[i]["<0cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc ">' + json[i]["1-34cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc ">' + json[i]["35-40cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i]["41-60cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i]["61-65cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i][">65cm"] + '</td>';
                        content += '<td style="text-align:right;">' + json[i]["Total"] + '</td>';
                        content += '<td style="text-align:right; ">' + json[i]["persen"] + '</td>';
                        content += '</tr>';
                    } else if (i == 4) {
                        content += '<tr>';
                        content += '<td>' + 'S' + (parseInt(i) + parseInt(1)) + '</td>';
                        content += '<td>' + json[i]["Ket"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc">' + json[i]["<0cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc ">' + json[i]["1-34cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc ">' + json[i]["35-40cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i]["41-60cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i]["61-65cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i][">65cm"] + '</td>';
                        content += '<td style="text-align:right;">' + json[i]["Total"] + '</td>';
                        content += '<td style="text-align:right; ">' + json[i]["persen"] + '</td>';
                        content += '</tr>';
                    } else if (i == 5) {
                        content += '<tr>';
                        content += '<td>' + 'S' + (parseInt(i) + parseInt(1)) + '</td>';
                        content += '<td>' + json[i]["Ket"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc">' + json[i]["<0cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc ">' + json[i]["1-34cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#2c71bc ">' + json[i]["35-40cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i]["41-60cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i]["61-65cm"] + '</td>';
                        content += '<td style="text-align:right; background-color:#92D14F ">' + json[i][">65cm"] + '</td>';
                        content += '<td style="text-align:right;">' + json[i]["Total"] + '</td>';
                        content += '<td style="text-align:right; ">' + json[i]["persen"] + '</td>';
                        content += '</tr>';
                    }
                    total2 = parseInt(total2) + parseInt(json[i]["Total"]);


                    content += '</tr>';

                }
                content += '<tfoot style="font-size: 12px">';
                content += '<tr>';
                content += ' <th colspan="2" style="text-align:right">Total:</th> <th style="text-align:right">Total 1:</th><th style="text-align:right">Total 2:</th><th style="text-align:right">Total 3:</th><th style="text-align:right">Total 4:</th><th style="text-align:right">Total 5:</th><th style="text-align:right">Total 6:</th><th style="text-align:right">Total 7:</th>';
                content += '</tr>';
                content += '<tr>';
                content += ' <th colspan="2" style="text-align:right">%:</th> <th style="text-align:right">% 1:</th><th style="text-align:right">% 2:</th><th style="text-align:right">% 3:</th><th style="text-align:right">% 4:</th><th style="text-align:right">% 5:</th><th style="text-align:right"></th><th style="text-align:right"></th>';
                content += '</tr>';
                content += '</tfoot>';
                var json2 = JSON.parse(response.d[1]);
                if (json2.length > 0) {
                    document.getElementById('lblDataTidakLengkap').innerHTML = 'Data tidak lengkap : ' + json2[0]["totalNoData"];
                    document.getElementById('lblTotalData').innerHTML = 'Total Data : ' + json2[0]["totalRecord"];
                }
                console.log('total=' + total2);
                $("#tempatTable").html(content);
                var table = $('#tableData2').DataTable({
                    lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                    pageLength: 10,
                    "columnDefs": [
                        { "visible": false, "targets": 0 }
                    ],
                    footerCallback: function (row, data, start, end, display) {
                        var api = this.api(), data;

                        //Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        //Total over all pages
                        total = api
                            .column(2)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        var completedGrandTotal = api
                            .column(2, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        //Total over this page
                        pageTotal = api
                            .column(2, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        //Update footer
                        $(api.column(2).footer(0)).html(
                            pageTotal.toLocaleString('en-EN')
                        );
                        $(api.column(3).footer(1)).html(
                            pageTotal.toLocaleString('en-EN')
                        );

                        //Total over all pages
                        total = api
                            .column(3)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        var completedGrandTotal2 = api
                            .column(3, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        //Total over this page

                        pageTotal = api
                            .column(3, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        //Update footer
                        $(api.column(3).footer()).html(
                            pageTotal.toLocaleString('en-EN')
                        );


                        //Total over all pages
                        total = api
                            .column(4)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        var completedGrandTotal3 = api
                            .column(4, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        //Total over this page
                        pageTotal = api
                            .column(4, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        //Update footer
                        $(api.column(4).footer()).html(
                            pageTotal.toLocaleString('en-EN')


                        );
                        total = api
                            .column(5)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        var completedGrandTotal4 = api
                            .column(5, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        //Total over this page
                        pageTotal = api
                            .column(5, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        // Update footer
                        $(api.column(5).footer()).html(
                            pageTotal.toLocaleString('en-EN')
                        );

                        total = api
                            .column(6)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        var completedGrandTotal5 = api
                            .column(6, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        //Total over this page
                        pageTotal = api
                            .column(6, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        // Update footer
                        $(api.column(6).footer()).html(
                            pageTotal.toLocaleString('en-EN')
                        );
                        total = api
                            .column(7)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        var completedGrandTotal6 = api
                            .column(7, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        // Total over this page
                        pageTotal = api
                            .column(7, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        // Update footer
                        $(api.column(7).footer()).html(
                            pageTotal.toLocaleString('en-EN')
                        );

                        total = api
                            .column(8)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        var completedGrandTotal7 = api
                            .column(8, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        // Total over this page
                        pageTotal = api
                            .column(8, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);
                        // Update footer
                        $(api.column(8).footer()).html(
                            pageTotal.toLocaleString('en-EN')
                        );
                        //total = api
                        //    .column(9)
                        //    .data()
                        //    .reduce(function (a, b) {
                        //        return intVal(a) + intVal(b);
                        //    }, 0);
                        //// Total over this page
                        //pageTotal = api
                        //    .column(9, { page: 'current' })
                        //    .data()
                        //    .reduce(function (a, b) {
                        //        return intVal(a) + intVal(b);
                        //    }, 0);
                        //// Update footer
                        //$(api.column(9).footer()).html(
                        //    pageTotal.toLocaleString('en-EN')
                        //);

                        var persen = completedGrandTotal / total2 * 100;
                        var persen2 = completedGrandTotal2 / total2 * 100;
                        var persen3 = completedGrandTotal3 / total2 * 100;
                        var persen4 = completedGrandTotal4 / total2 * 100;
                        var persen5 = completedGrandTotal5 / total2 * 100;
                        var persen6 = completedGrandTotal6 / total2 * 100;

                        $('tr:eq(1) th:eq(1)', api.table().footer()).html(persen.toFixed(2));
                        $('tr:eq(1) th:eq(2)', api.table().footer()).html(persen2.toFixed(2));
                        $('tr:eq(1) th:eq(3)', api.table().footer()).html(persen3.toFixed(2));
                        $('tr:eq(1) th:eq(4)', api.table().footer()).html(persen4.toFixed(2));
                        $('tr:eq(1) th:eq(5)', api.table().footer()).html(persen5.toFixed(2));
                        $('tr:eq(1) th:eq(6)', api.table().footer()).html(persen6.toFixed(2));

                    }


                });




                $('#tableData2 tbody').on('click', 'td', function () {
                    $('.loading').show();
                    var clicked_td = $(this);
                    var td_index = clicked_td.index();
                    var tr_index = clicked_td.parent().index();
                    var columns = table.settings().init().columns;
                    console.log(td_index);
                    console.log(tr_index);
                    var statusKetinggian = table.column(td_index + parseInt(1)).title();
                    var tableData = table.data().toArray();

                    //var col = table.cell(this).index().column;
                    //console.log('nama colom=' + table.column(col).title()); 
                    //var colText = $(".tableData2 thead th:nth-child(" + col + ")").text();
                    console.log('status ketinggian is=' + statusKetinggian)
                    console.log('status perubahan is=' + tableData[tr_index][0]);
                    if (statusKetinggian == "Perubahan TMAT" || statusKetinggian == "total" || statusKetinggian == "persen") {
                        alert('area click tidak valid, hanya bisa klik pada status ketinggian');
                        $('.loading').hide()
                    } else {
                        GetPenjabaranBlock($('#txtWeekId').val(), $("#selectFilterZonaPencatatanPerubahanTMAT").val(), statusKetinggian, tableData[tr_index][0], $("#selectFilterEstatePulauPerubahanTMAT").val());
                    }



                });

            }
            //$('.selectpicker').selectpicker('refresh');
            $('.loading').hide()
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });




    //var humanReadableDate = new Date(parseInt(json[i]["Date"]*1000));
    //console.log('new format = ' + dateFormat5(humanReadableDate));
}

function GetPenjabaranBlock(idWeek, wmArea, statusKetinggian, statusPerubahan, pulau) {
    document.getElementById('lblketKetinggian').innerHTML = 'Ketinggian ' + statusKetinggian + ' Kenaikan 0-5cm';
    var ele = document.getElementsByName('check');
    for (i = 0; i < ele.length; i++) {
        if (ele[i].checked)
            klhk = ele[i].value;
    }

    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/getBlockPerubahan',
        data: '{IdWeek: "' + idWeek + '",wmarea: "' + wmArea + '", statusKetinggian:"' + statusKetinggian + '",statusperubahan:"' + statusPerubahan + '",flagKlhk:"' + klhk + '",estCode:"' + $("#selectFilterEstatePencatatanPerubahanTMAT").val() + '",pulau:"' + pulau + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {

            var json = JSON.parse(response.d);
            if (json.length > 0) {
                var content = '<table id="tableDataPenjabaran" class="table table-striped table-bordered tableDataBlockPenjabaran" style="width:100%">';
                content += '<thead><tr>';
                content += '<th>Estate Code</th><th>Piezo Id</th> <th>Block</th> <th>Ketinggian</th> <th>Ketinggian W-1</th> <th>Selisih Ketinggian</th> </thead></tr>';
                for (var i = 0; i < json.length; i++) {
                    content += '<tr>';
                    content += '<td>' + json[i]["EstCode"] + '</td>';
                    content += '<td>' + json[i]["PieRecordID"] + '</td>';
                    content += '<td>' + json[i]["block"] + '</td>';
                    content += '<td>' + json[i]["Ketinggian W"] + '</td>';
                    content += '<td>' + json[i]["Ketinggian W-1"] + '</td>';
                    content += '<td>' + json[i]["selisihKetinggian"] + '</td>';

                    content += '</tr>';

                }
                $('#tempatTablePenjabaran').html(content);
                $('#tableDataPenjabaran').DataTable({
                    dom: 'Blfrtip',
                    lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "All"]],
                    pageLength: 10,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });

                $("#mdlJabarBlock").modal('toggle');
            }
            $('.loading').hide()
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

function drawTablePetaSummary(data,id) {
    var content = '<table class="table table-striped table-bordered" style="font-size: 14px;font-family:Arial Narrow;">';
    content += '<thead><tr>';
    content += '<th style="width: 10%; text-align: center;">Warna</th>';
    content += '<th style="width: 70%; text-align: center;">Keterangan</th>';
    content += '<th style="width: 10%; text-align: center;">Total<br>Block</th>';
    content += '<th style="width: 10%; text-align: center;">%</th>';
    content += '</tr></thead>';
    content += '<tbody style="text-align: left;font-weight:bold;">';
    for (var i = 0; i < data.length; i++){
        var json = data[i];
        content += '<tr>';
        content += '<td bgColor="' + json["colorBack"] + '" style="color:' + json["colorFont"] + ';"></td>';
        content += '<td>' + json["IndicatorAliasReport"] + " ( " + json["IndicatorName"] + " ) </td>";
        if (json["totalPiezo"] == null) {
            content += '<td style="text-align:right"> - </td>';
        } else {
            content += '<td style="text-align:right" >' + json["totalPiezo"].toLocaleString('id') + '</td>';
        }

        if (json["persenPiezo"] == null) {
            content += '<td style="text-align:right"> - </td>';
        } else {
            content += '<td style="text-align:right">' + json["persenPiezo"] + '</td>';
        }
        content += '</tr>';
    }
    content += '</tbody>';
    content += '</table>';
    $("#" + id).html(content);
}

function drawTableSummarySummary(data, id) {
    var content = '<table class="table table-striped table-bordered" style="font-size: 15px;font-family:Arial Narrow;">';
    content += '<thead><tr>';
    content += '<th style="width: 70%; text-align: center;">Statistik</th>';
    content += '<th style="width: 10%; text-align: center;">Minggu Lalu</th>';
    content += '<th style="width: 10%; text-align: center;">Minggu Ini</th>';
    content += '<th style="width: 10%; text-align: center;">Selisih</th>';
    content += '</tr></thead>';
    content += '<tbody style="text-align: left;font-weight:bold;">';
    for (var i = 0; i < data.length; i++) {
        var json = data[i];
        content += '<tr>';
        content += '<td>' + json["Statistic"] + ' </td>';
		if(json["mingguLalu"] != null){
			content += '<td style="text-align:right">' + json["mingguLalu"].toLocaleString('id') + ' </td>';
		}else{
			content += '<td style="text-align:right"> </td>';
		}
        
		if(json["mingguIni"] != null){
			content += '<td style="text-align:right">' + json["mingguIni"].toLocaleString('id') + ' </td>';
		}else{
			content += '<td style="text-align:right"> </td>';
		}
        if (json["id"] == "2" && json["selisih"] != null) {
            content += '<td style="text-align:right">' + json["selisih"].toLocaleString('id') + ' </td>';
        } else {
            content += '<td style="text-align:right"></td>';
        }
        content += '</tr>';
    }
    content += '</tbody>';
    content += '</table>';
    $("#" + id).html(content);
}

function drawTableSummaryKondisiPiezo(data) {
    var estCode = _.uniq(data, true /* array already sorted */, function (item) {
        return item.NewEstName;
    });
    estCode = _.sortBy(estCode, function (o) { return o.id; })

    var content = "";
    for (var i = 0; i < estCode.length; i++) {

        var block = "";
        let b = 0; 
        for (var j = 0; j < data.length; j++) {
            if (data[j]["NewEstName"] == estCode[i]["NewEstName"]) {
                if (block == "") {
                    block = data[j]["Block"];
                } else {
                    block += ", " + data[j]["Block"];
                }
                b++;
            }
        }

        content += '<label>' + estCode[i]["NewEstName"] + ' : ('+ b +')</label>';
        content += '<p>'+block + '</p>';
    }
    $("#summaryKondisiPiezoThisWeek").html(content);
}

function cropImageMap(idImg,minPx) {
    var image = document.getElementById(idImg);
    var canvas = document.createElement("canvas");

    // Mengatur ukuran canvas sesuai dengan gambar
    canvas.width = image.width;
    canvas.height = image.height - minPx; // Mengurangi 20 piksel dari tinggi gambar

    // Gambar gambar pada canvas dengan melakukan pemotongan
    var ctx = canvas.getContext("2d");
    ctx.drawImage(image, 0, minPx, image.width, image.height - minPx, 0, 0, image.width, image.height - minPx);

    // Ganti sumber gambar dengan gambar yang sudah di-crop
    var newImage = new Image();
    newImage.onload = function () {
        image.src = newImage.src;
    };
    newImage.src = canvas.toDataURL();
}