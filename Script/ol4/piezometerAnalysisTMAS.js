﻿//javascript setupchart pertama
//Map Basic Variable Declaration
var map, latLong, centerCoordinate, zoomLevel, mapProjection;

//Map Control Declaration
var scaleLineControl, mousePositionControl, overviewMapControl, zoomSliderControl;

//Map Layer Declaration
var layerBase, layerBlock, layerPiezometer, layerInfo, layerDrawing, sourceLayerDrawing, layerMeasure, sourceLayerMeasure, layerHighlight, layerPulau;

var listyear, listmonth, listweek, listallwmarea;
var year, month, monthname, week, monthnamealias, idweek;

//Variable for Event Handler
var isfullscreen;

//Variable for Toolbox
var draw;
var extentCoordinate;
var sketch, helpTooltipElement, helpTooltip, measureTooltipElement, measureTooltip, continuePolygonMsg = 'Click to continue drawing the polygon',
    continueLineMsg = 'Click to continue drawing the line', pointerMoveHandler, formatLength, formatArea, listener;
var isCheckZonaTrue;
var viewMap;
var extendCoordinatePulau;
var dataBlock;
var dataBlockParameter;
var indexDiv = 0;
var flagBlockClick = 0;
var flagButtonGoClick = 0;
//Variable For Piezometer
var mapped_estate, estate, globalimgurl;
var arrDiv = [];
var arrBlock = [];
var arrBlockParam = [];
var arrBlockName = [];
var indexAkhir = 0;

$(function () {
    $(".loading").hide();
    //For Event Handler	
    isfullscreen = false;
    //Variable Declaration
    mapped_estate = $('#mapped_estate').val();
    estate = $('#estate').val();
    var utmprojection = $('#utmprojection').val();
    var rsid = $('#rsid').val();
    var coordinateExtent = $('#extent').val().split(',');;
    var numzoom = $('#numzoom').val();
    var company = $('#company').val();
    var idwm = $('#idwm').val();
    var estatezone = $('#estatezone').val();
    //var isCheckZonaTrue;
    listallwmarea = $.parseJSON($('#listAllWMArea').val());
    listyear = $('#listyear').val();
    console.log(listyear);
    listmonth = $('#listmonth').val();
    listweek = $('#listweek').val();
    year = $('#year').val();
    month = $('#month').val();
    monthname = $('#monthname').val();
    week = $('#week').val();
    monthnamealias = $('#monthnamealias').val();
    idweek = $('#idweek').val();
    extentCoordinate = $("#extentCoordinate").val();
    dataBlockParameter = $("#ListBlockByWMACode").val();
    var zonaValueInitial = 1;
    extendCoordinatePulau = $("#extentCoordinatePulau").val();
    sessionStorage.PreviousDate = null;
    sessionStorage.nextDate = null;
    sessionStorage.currentDate = null;
    sessionStorage.buttonGoClicked = null;
    dataBlock = $("#listBlock").val();
    var currentextent = $('#currentextent').val();
    $("#dynamicWMAreaFilter").show();
    $('#lbldate').html("Week " + week + ", " + monthnamealias + " " + year);
    var dataMasterGraph = $("#ListMasterGraph").val();

    $("#divChartTMASLoading").hide();
    $("#divChartCollectionLoading").hide();

    //if (sessionStorage.sessionWMCheck != null) {
    //	var isTrueSet = (sessionStorage.sessionWMCheck == 'true');
    //	if (isTrueSet) {
    //		for (var i = 0; i < listallwmarea.length; i++) {
    //			if (listallwmarea[i]["idWMArea"] == idwm) {
    //				coordinateExtent = [listallwmarea[i]["MinX"], listallwmarea[i]["MinY"], listallwmarea[i]["MaxX"], listallwmarea[i]["MaxY"]];
    //				zoomLevel = 12;
    //				break;
    //			}
    //		}
    //	}
    //}

    //console.log('wma area = ' + idwm);
    //console.log('mapped estate = ' + mapped_estate);
    coordinateExtent = [102.88001291500001, 0.1109373030001848, 103.29773179800009, 0.3451648029999888];

    latLong = [parseFloat(coordinateExtent[0]), parseFloat(coordinateExtent[1]), parseFloat(coordinateExtent[2]), parseFloat(coordinateExtent[3])];
    centerCoordinate = [(latLong[0] + latLong[2]) / 2, (latLong[1] + latLong[3]) / 2];
    zoomLevel = 12;
    mapProjection = 'EPSG:4326';

    viewMap = new ol.View({
        extent: latLong,
        projection: mapProjection,
        center: centerCoordinate,
        zoom: zoomLevel
    })
    map = new ol.Map({
        interactions: ol.interaction.defaults({
            //doubleClickZoom: false,
            dragAndDrop: false,
            dragPan: true,
            keyboardPan: false,
            keyboardZoom: false,
           // mouseWheelZoom: false,
            pointer: true,
            select: false
        }),
        controls: null,
        target: 'map',
        view: viewMap
    });
    layerPulau = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
            params: {
                'LAYERS': 'pub_pzo:pzo_block_week_wmarea_unitpemantauan',
                'viewparams': 'YEAR:' + '2019' + ';WMAREA:1;MONTH:' + '4' + ';WEEK:' + '1',
                'TRANSPARENT': 'true',
            },
        })
    });

    layerBlock = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
            params: {
                'LAYERS': 'pub_pzo:pzo_block_week_wmarea_unitpemantauan', //warnanya
                'viewparams': 'YEAR:' + year + ';WMAREA:' + idwm + ';MONTH:' + month + ';WEEK:' + week,
                'TRANSPARENT': 'true'
            },
        })
    });

    layerPiezometer = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
            params: {
                'LAYERS': 'pub_pzo:PZO_Point_Week_WM_Area_unitpemantauan',
                'viewparams': 'years:' + year + ';WMAREA:' + 1 + ';months:' + 4 + ';weeks:' + 4,
                'TRANSPARENT': 'true'
            }
        })
    });
    layerInfo = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
            params: {
                'LAYERS': 'pub_pzo:PZO_Info',
                'viewparams': 'PZOYEAR:' + year + ';ESTNR:' + mapped_estate + ';PZOMONTH:' + month + ';PZOWEEK:' + week,
                'TRANSPARENT': 'true'
            }
        })
    });


    sourceLayerDrawing = new ol.source.Vector({ wrapX: false });

    layerDrawing = new ol.layer.Vector({
        source: sourceLayerDrawing
    });


    sourceLayerMeasure = new ol.source.Vector();

    layerMeasure = new ol.layer.Vector({
        source: sourceLayerMeasure,
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.2)'
            }),
            stroke: new ol.style.Stroke({
                color: '#ffcc33',
                width: 2
            }),
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({
                    color: '#ffcc33'
                })
            })
        })
    });
    //map.addLayer(layerPulau);
    map.addLayer(layerBlock);
    map.addLayer(layerPiezometer);
    map.addLayer(layerInfo);

    //layerPulau.setVisible(true);
    //layerBlock.setVisible(true);

    map.on('singleclick', function (evt) {
        flagBlockClick = 1;
        if (flagButtonGoClick == 1) {
            indexDiv = indexAkhir
        }
        if (sessionStorage.PreviousDate != null && sessionStorage.PreviousDate != "null") {
            var time = sessionStorage.PreviousDate.split(',');
            week = time[0];
            month = time[1];
            year = time[2];
        }
        //$(".loading").show();

        var viewResolution = /** @type {number} */(map.getView().getResolution());
        //var url = layerPiezometer.getSource().getGetFeatureInfoUrl(evt.coordinate, viewResolution, 'EPSG:4326', { 'INFO_FORMAT': 'application/json' });
        ////console.log('hello world click url = ' + url);
        //reqwest({
        //    url: "../Handler/proxy.ashx?url=" + encodeURIComponent(url),
        //    type: 'json',
        //}).then(function (data) {
        //    if (data.features.length > 0) {
        //        var feature = data.features[0];
        //        var props = feature.properties;

        //        //ShowDetailV3(props.PieRecordID, indexDiv);
        //        ShowDetailV3($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year, props.PieRecordID, indexDiv)
        //    }
        //});

        var url_block = layerBlock.getSource().getGetFeatureInfoUrl(evt.coordinate, viewResolution, 'EPSG:4326', { 'INFO_FORMAT': 'application/json' });
        //console.log('hello world click url block = ' + url_block);
        //reqwest({
        //    url: "../Handler/proxy.ashx?url=" + encodeURIComponent(url_block),
        //    type: 'json',
        //}).then(function (data) {
        //    if (data.features.length > 0) {
        //        var feature = data.features[0];
        //        var props = feature.properties;
        //        var content = "";
        //        if (arrBlock.length == 0) {
        //            indexDiv = 0;
        //            arrBlock.push(props.PieRecordID)
        //        } else {
        //            indexDiv = arrBlock.length;
        //            arrBlock.push(props.PieRecordID)
        //        }
        //        var selectBlock = document.getElementById("selectFilterBlock");

        //        //$(".loading").hide();
        //        console.log('index div single click == ' + indexDiv);
        //        var content = "";
        //        content += "<div class='col-lg-6' style='height: 420px;'>";
        //        content += "<div id='tempGraphDiv" + (indexDiv % 4) + "'>";
        //        content += "</div>";
        //        content += "</div>";
        //        arrBlockName.push(props.Block)
        //        arrDiv.push(content);
        //        //alert('flag block click = ' + flagBlockClick);
        //        console.log(arrBlock);
        //        console.log(indexDiv);
        //        console.log(arrDiv);
        //        console.log(flagBlockClick);
        //        console.log(arrBlockName);
        //        ShowDetailV3($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year, arrBlock, indexDiv, arrDiv, flagBlockClick, arrBlockName);
        //        indexDiv = indexDiv + 1;
        //        indexAkhir = indexAkhir + 1

        //        $('.modal-dialogdetail').css({
        //            top: 0,
        //            left: 0
        //        });
        //        $('#myDetail2').modal({
        //            backdrop: false,
        //            show: true
        //        });
        //        $('.modal-dialogdetail').draggable({
        //            handle: ".modal-header"
        //        });
        //    }
        //});

        $(".loading").show()
        $.ajax({
            type: 'GET',
            url: url_block,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                $(".loading").hide();
                if (response.features.length > 0) {
                    var feature = response.features[0];
                    var props = feature.properties;
                    var content = "";
                    if (arrBlock.length == 0) {
                        indexDiv = 0;
                        arrBlock.push(props.PieRecordID)
                    } else {
                        indexDiv = arrBlock.length;
                        arrBlock.push(props.PieRecordID)
                    }
                    var selectBlock = document.getElementById("selectFilterBlock");

                    //$(".loading").hide();
                    console.log('index div single click == ' + indexDiv);
                    var content = "";
                    content += "<div class='col-lg-6' style='height: 420px;'>";
                    content += "<div id='tempGraphDiv" + (indexDiv % 4) + "'>";
                    content += "</div>";
                    content += "</div>";
                    arrBlockName.push(props.Block)
                    arrDiv.push(content);
                    //alert('flag block click = ' + flagBlockClick);
                    console.log(arrBlock);
                    console.log(indexDiv);
                    console.log(arrDiv);
                    console.log(flagBlockClick);
                    console.log(arrBlockName);
                    GetMasterRambuAir($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year);
                    ShowDetailV3($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year, arrBlock, indexDiv, arrDiv, flagBlockClick, arrBlockName);
                    indexDiv = indexDiv + 1;
                    indexAkhir = indexAkhir + 1

                    $('.modal-dialogdetail').css({
                        top: 0,
                        left: 0
                    });
                    $('#myDetail2').modal({
                        backdrop: false,
                        show: true
                    });
                    $('.modal-dialogdetail').draggable({
                        handle: ".modal-header"
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $(".loading").hide();
                alert("function " + urlX + " : " + xhr.statusText);
            }
        });
    });

    $("#toolsprevdate").click(function () {
        //alert('sessionStorage.buttonGoClicked = ' + sessionStorage.buttonGoClicked);
        if (sessionStorage.currentDate != null && sessionStorage.currentDate != "null") {
            var time = sessionStorage.currentDate.split(',');
            week = time[0];
            month = time[1];
            year = time[2];
        }
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/GetPreviousWeekFromParameter',
            data: '{Week: "' + week + '", Month: "' + month + '", Year: "' + year + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                console.log('response = ' + JSON.stringify(response));
                var json = $.parseJSON(response.d);
                if (json.length > 0) {
                    $('#lbldate').html("Week " + json[0]["Week"] + ", " + json[0]["MonthName"] + " " + json[0]["Year"]);
                    sessionStorage.currentDate = json[0]["Week"] + "," + json[0]["Month"] + "," + json[0]["Year"];
                    //GetMasterRambuAir($("#selectFilterZona").val(), json[0]["Week"], json[0]["Month"], $("#selectFilterWMArea").val(), json[0]["Year"]);
                    if (sessionStorage.buttonGoClicked == 'true') {
                        GetMasterRambuAir($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year)
                        document.getElementById('divChartCollection').innerHTML = "";
                        var block = $("#selectFilterBlock").val();
                        console.log('block dari toolsprevdate = ' + block);
                        var blockCollection = block.toString().split(',');
                        for (var a = 0; a < blockCollection.length; a++) {
                            indexDiv = indexDiv + 1;
                            var pieRecordId = blockCollection[a];
                            console.log('update block = ' + indexDiv);
                            //GetChartTMAS(
                            ShowDetailV3($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year, pieRecordId, indexDiv)
                        }
                    }
                    getPreviousDateMap(json[0]["Week"], json[0]["Month"], json[0]["Year"])

                    $(".loading").hide();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.statusText);
            }
        });
    })



    $("#toolsnextdate").click(function () {
        if (sessionStorage.currentDate != null && sessionStorage.currentDate != "null") {
            var time = sessionStorage.currentDate.split(',');
            week = time[0];
            month = time[1];
            year = time[2];
        }
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/GetNextWeekFromParameter',
            data: '{Week: "' + week + '", Month: "' + month + '", Year: "' + year + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var json = $.parseJSON(response.d);
                if (json.length > 0) {
                    var json = $.parseJSON(response.d);
                    $('#lbldate').html("Week " + json[0]["Week"] + ", " + json[0]["MonthName"] + " " + json[0]["Year"]);
                    sessionStorage.currentDate = json[0]["Week"] + "," + json[0]["Month"] + "," + json[0]["Year"];
                    //GetMasterRambuAir($("#selectFilterZona").val(), json[0]["Week"], json[0]["Month"], $("#selectFilterWMArea").val(), json[0]["Year"]);
                    getNextDateMap(json[0]["Week"], json[0]["Month"], json[0]["Year"])
                    if (sessionStorage.buttonGoClicked == 'true') {
                        //alert('session storage buttonGo clikced');
                        document.getElementById('divChartCollection').innerHTML = "";
                        var block = $("#selectFilterBlock").val();
                        var blockCollection = block.toString().split(',');
                        for (var i = 0; i < blockCollection.length; i++) {
                            indexDiv = indexDiv + 1;
                            console.log('update block = ' + indexDiv);
                            ShowDetailV3($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year, blockCollection[i], indexDiv)
                        }
                    }
                    $(".loading").hide();

                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.statusText);
            }
        });
    })

    $("#btnGo").click(function () {
        //sessionStorage.buttonGoClicked = true;
        //if (sessionStorage.currentDate != null && sessionStorage.currentDate != "null") {
        //    var time = sessionStorage.currentDate.split(',');
        //    week = time[0];
        //    month = time[1];
        //    year = time[2];
        //}
        ////console.log('tanggal = ' + JSON.stringify(tanggal));
        ////console.log('btn go clicked')
        ////console.log('year = ' + year);
        ////console.log('bulan = ' + month)
        ////console.log('week = ' + week)
        //$(".loading").show();
        //var coordinateExtentPulau;

        //for (var i = 0; i < listallwmarea.length; i++) {
        //    if (listallwmarea[i]["idWMArea"] == $("#selectFilterWMArea").val()) {
        //        coordinateExtentPulau = [listallwmarea[i]["MinX"], listallwmarea[i]["MinY"], listallwmarea[i]["MaxX"], listallwmarea[i]["MaxY"]];
        //        break;
        //    }
        //}
        //layerBlock.getSource().updateParams({
        //    'LAYERS': 'pub_pzo:pzo_block_week_wmarea_unitpemantauan',
        //    'viewparams': 'YEAR:' + year + ';WMAREA:' + $("#selectFilterWMArea").val() + ';MONTH:' + month + ';WEEK:' + week,
        //    'TRANSPARENT': 'true',
        //    'TILED': true
        //});
        ////map.removeLayer(layerPiezometer);
        //map.removeLayer(layerHighlight);
        //var style = "";
        //latLong = "";
        //centerCoordinate = "";
        //var json = JSON.parse(extentCoordinate);
        //for (var j = 0; j < json.length; j++) {
        //    if (json[j]["WMA_code"] == $("#selectFilterZona").val()) {
        //        console.log(json[j]["WMA_code"] + '&' + $("#selectFilterZona").val() + ' sama ')
        //        coordinateExtent = [parseFloat(json[j]["MinX"]), parseFloat(json[j]["MinY"]), parseFloat(json[j]["MaxX"]), parseFloat(json[j]["MaxY"])];
        //        centerCoordinate = [(coordinateExtent[0] + coordinateExtent[2]) / 2, (coordinateExtent[1] + coordinateExtent[3]) / 2];
        //    }
        //}
        //style = new ol.style.Style({
        //    stroke: new ol.style.Stroke({
        //        color: '#9b59b6',
        //        width: 5
        //    }),
        //    fill: new ol.style.Fill({
        //        color: 'rgba(255, 255, 255, 0.3)'
        //    }),
        //});

        //var UrlCQL = "http://map.gis-div.com:8443/geoserver/pub_pzo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=pub_pzo:PZO_WMZone&CQL_FILTER=WMA_code=%27" + $("#selectFilterZona").val() + "%27&maxFeatures=50&outputFormat=application/json";
        //layerHighlight = new ol.layer.Vector({
        //    source: new ol.source.Vector({
        //        url: UrlCQL,
        //        format: new ol.format.GeoJSON()
        //    }),
        //    style: function (feature) {
        //        //style.getText().setText(feature.get('style'));
        //        return style;
        //    }
        //});
        //layerPiezometer = new ol.layer.Image({
        //    source: new ol.source.ImageWMS({
        //        url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
        //        params: {
        //            'LAYERS': 'pub_pzo:PZO_Point_Week_WM_Area_unitpemantauan',
        //            'viewparams': 'years:' + $("#selectFilterYear").val() + ';WMAREA:' + $("#selectFilterWMArea").val() + ';months:' + '4' + ';weeks:' + '3',
        //            'TRANSPARENT': 'true'
        //        }
        //    })
        //});

        //zoomLevel = 12;

        ////var Navigation = new OpenLayers.Control.Navigation({
        ////	'zoomWheelEnabled': false,
        ////	'defaultDblClick': function (event) {
        ////		return;
        ////	}
        ////});

        //map.addLayer(layerHighlight);
        //map.setView(new ol.View({
        //    extent: coordinateExtentPulau,
        //    projection: mapProjection,
        //    center: centerCoordinate,
        //    zoom: 13
        //}));

        //GetMasterRambuAir($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year);
        $(".loading").show();
        //$("#myDetail2 .close").click();
        updateBlock($("#selectFilterZona").val(), $("#selectFilterBlock").val());
        $("#btnGoOnly").trigger("click");        
    })
    $("#btnGoOnly").click(function () {
        //$("#myDetail2 .close").click();
        //$('#myDetail2').modal('hide');
        flagButtonGoClick = 1;
        sessionStorage.buttonGoClicked = true;
        $(".loading").show();
        document.getElementById('divChartCollection').innerHTML = "";
        if (sessionStorage.currentDate != null && sessionStorage.currentDate != "null") {
            var time = sessionStorage.currentDate.split(',');
            week = time[0];
            month = time[1];
            year = time[2];
        }

        var coordinateExtentPulau;

        for (var i = 0; i < listallwmarea.length; i++) {
            if (listallwmarea[i]["idWMArea"] == $("#selectFilterWMArea").val()) {
                coordinateExtentPulau = [listallwmarea[i]["MinX"], listallwmarea[i]["MinY"], listallwmarea[i]["MaxX"], listallwmarea[i]["MaxY"]];
                break;
            }
        }
        layerBlock.getSource().updateParams({
            'LAYERS': 'pub_pzo:pzo_block_week_wmarea_unitpemantauan',
            'viewparams': 'YEAR:' + year + ';WMAREA:' + $("#selectFilterWMArea").val() + ';MONTH:' + month + ';WEEK:' + week,
            'TRANSPARENT': 'true',
            'TILED': true
        });

        map.removeLayer(layerHighlight);
        var style = "";
        latLong = "";
        centerCoordinate = "";
        var json = JSON.parse(extentCoordinate);
        for (var j = 0; j < json.length; j++) {
            if (json[j]["WMA_code"] == $("#selectFilterZona").val()) {
                console.log(json[j]["WMA_code"] + '&' + $("#selectFilterZona").val() + ' sama ')
                coordinateExtent = [parseFloat(json[j]["MinX"]), parseFloat(json[j]["MinY"]), parseFloat(json[j]["MaxX"]), parseFloat(json[j]["MaxY"])];
                centerCoordinate = [(coordinateExtent[0] + coordinateExtent[2]) / 2, (coordinateExtent[1] + coordinateExtent[3]) / 2];
            }
        }
        style = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: '#9b59b6',
                width: 5
            }),
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.3)'
            }),
        });

        var UrlCQL = "http://map.gis-div.com:8443/geoserver/pub_pzo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=pub_pzo:PZO_WMZone&CQL_FILTER=WMA_code=%27" + $("#selectFilterZona").val() + "%27&maxFeatures=50&outputFormat=application/json";
        layerHighlight = new ol.layer.Vector({
            source: new ol.source.Vector({
                url: UrlCQL,
                format: new ol.format.GeoJSON()
            }),
            style: function (feature) {

                return style;
            }
        });
        layerPiezometer = new ol.layer.Image({
            source: new ol.source.ImageWMS({
                url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
                params: {
                    'LAYERS': 'pub_pzo:PZO_Point_Week_WM_Area_unitpemantauan',
                    'viewparams': 'years:' + $("#selectFilterYear").val() + ';WMAREA:' + $("#selectFilterWMArea").val() + ';months:' + '4' + ';weeks:' + '3',
                    'TRANSPARENT': 'true'
                }
            })
        });

        zoomLevel = 14;
        console.log('latLong = ' + coordinateExtentPulau);
        console.log('center koordinate = ' + centerCoordinate);

        map.addLayer(layerHighlight);
        map.setView(new ol.View({
            extent: coordinateExtentPulau,
            projection: mapProjection,
            center: centerCoordinate,
            zoom: 14
        }));

        var block = $("#selectFilterBlock").val();
        var blockCollection = block.toString().split(',');
        arrBlock.length = 0;
        arrBlockName.length = 0;
        indexAkhir = indexDiv + blockCollection.length;

        for (var c = 0; c < blockCollection.length; c++) {

            var val = blockCollection[c];
            var txt = $("#selectFilterBlock option[value='" + val + "']").text();
            arrBlockName.push(txt);
        }

        //alert("blockcollection = "+blockCollection.length);
        for (var a = 0; a < blockCollection.length; a++) {
            arrBlock.push(blockCollection[a]);
        }

        //alert("indexdiv = " + indexDiv + " , arrblock = " + arrBlock.length);
        for (var b = indexDiv; b < arrBlock.length; b++) {
            var content = "";
            content += "<div class='col-lg-6' style='height: 420px;'>";
            content += "<div id='tempGraphDiv" + (b % 4) + "'>";
            content += "</div>";
            content += "</div>";
            arrDiv.push(content);

        }
        indexDiv = a;
        //console.log('arrBlockName = ' + arrBlockName);
        ShowDetailV3($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year, arrBlock, b, arrDiv, 0, arrBlockName)
        GetMasterRambuAir($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year);
        //}
        $('.modal-dialogdetail').css({
            top: 0,
            left: 0
        });
        $('#myDetail2').modal({
            backdrop: false,
            show: true
        });
        $('.modal-dialogdetail').draggable({
            handle: ".modal-header"
        });        

        //$(".loading").hide();
    });


    $("#generateChartCarousel").click(function () {
        //alert('generate chart carousel clicked');
        var block = $("#selectFilterBlock").val();
        var blockCollection = block.toString().split(',');
        for (var i = 0; i < blockCollection.length; i++) {
            indexDiv = indexDiv + 1
            ShowDetailV4($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year, blockCollection[i], indexDiv)
        }
    });

    $("#selectFilterCompany").change(function () {
        ListEstate($("#selectFilterCompany").val());
    })

    $("#selectFilterYear").change(function () {
        GetListMonth($("#selectFilterYear").val());
    })

    $("#selectFilterMonth").change(function () {
        GetListWeek($("#selectFilterYear").val(), $("#selectFilterMonth").val());
    })
    $("#selectFilterWMArea").change(function () {
        ListZonaByWmArea($("#selectFilterWMArea").val());
        ListPieRecordID($("#selectFilterZona").val());
    });
    $("#selectFilterZona").change(function () {
        //$(".loading").show();
        ListPieRecordID($("#selectFilterZona").val());
        //getBlockParameter($("#selectFilterZona").val());
    });
    $('.selectpicker').selectpicker({});
    ListCompany();
    ListWMArea();
    GetListYear();

    ////Initialize Select2 Elements
    //$('.select2').select2();

    //Date picker
    $('#datepicker').datepicker({
        autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    })

    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    })
    $("#btnPrintDownload").click(function () {
        DownloadLaporanPDF();
        //DownloadLaporanAllBlock();
    });
    $("#btnPrintDownloadAllBlock").click(function () {
        //DownloadLaporanPDF();
        DownloadLaporanAllBlock();
    });
    MapEventHandler();

    $('input').iCheck({
        checkboxClass: 'icheckbox_futurico',
        radioClass: 'iradio_square',
        increaseArea: '20%' // optional
    });
    $('input').on('ifChanged', function (event) {
        if (this.id == "chkLayerBlock") {
            if (this.checked) {
                layerBlock.setVisible(true);
            }
            else {
                layerBlock.setVisible(false);
            }
        }
        else if (this.id == "chkLayerPiezometer") {
            if (this.checked) {
                layerPiezometer.setVisible(true);
            }
            else {
                layerPiezometer.setVisible(false);
            }
        }      
    });	
})

function ListWMArea() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/ListWMArea',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            $('#selectFilterWMArea').find('option').remove();
            for (var i = 0; i < json.length; i++) {
                $("#selectFilterWMArea").append($('<option>', {
                    value: json[i]["idWMArea"],
                    text: json[i]["wmAreaName"]
                }));
            }
            $("#selectFilterWMArea").val($('#idwm').val()).change();
            var isTrue = (sessionStorage.sessionWMCheck == 'true');
            if (isTrue) {
                $('#titleEstate').html($('#selectFilterWMArea option:selected').text());
            }
            ListZonaByWmArea($("#selectFilterWMArea").val());
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListWMArea : " + xhr.statusText);
        }
    });
}

function ListCompany() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListCompany',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataSubText = response.d[2].split(";");
            var dataLength = response.d[0].split(";").length;

            $('#selectFilterCompany').find('option').remove();
            $("#selectFilterCompany").append($('<option>', {
                value: "",
                text: "All Company"
            }));
            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterCompany").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterCompany").val($('#company').val()).change();

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListCompany : " + xhr.statusText);
        }
    });
}

function ListEstate(companycode) {
    console.log(companycode);
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListEstate',
        data: '{CompanyCode: "' + companycode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataLength = response.d[0].split(";").length;

            $('#selectFilterEstate').find('option').remove();
            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterEstate").append($('<option>', {
                    value: dataValue[i],
                    text: dataValue[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterEstate").val($('#estate').val());
            var isTrue = (sessionStorage.sessionWMCheck == 'true');
            if (!isTrue) {
                $('#titleEstate').html($('#selectFilterEstate option:selected').text().split(' - ')[1]);
            }
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListEstate : " + xhr.statusText);
        }
    });
}

function GetListYear() {
    var json = $.parseJSON(listyear);
    $('#selectFilterYear').find('option').remove();
    for (var i = 0; i < json.length; i++) {
        $("#selectFilterYear").append($('<option>', {
            value: json[i]["Year"],
            text: json[i]["Year"]
        }));
    }

    $("#selectFilterYear").val(year).change();
}

function GetListMonth(year) {
    var json = $.parseJSON(listmonth);
    $('#selectFilterMonth').find('option').remove();
    for (var i = 0; i < json.length; i++) {
        if (json[i]["Year"] == year) {
            $("#selectFilterMonth").append($('<option>', {
                value: json[i]["Month"],
                text: json[i]["MonthName"]
            }));
        }
    }

    if ($("#selectFilterYear").val() == year)
        $("#selectFilterMonth").val(month).change();
    else
        $("#selectFilterMonth").change();
}

function GetListWeek(year, month) {
    var json = $.parseJSON(listweek);
    $('#selectFilterWeek').find('option').remove();

    for (var i = 0; i < json.length; i++) {
        if (json[i]["Year"] == year && json[i]["Month"] == month) {
            $("#selectFilterWeek").append($('<option>', {
                //value: json[i]["ID"],
                value: json[i]["Week"],
                text: "Week " + json[i]["Week"]
            }));
        }
    }

    if ($("#selectFilterWeek option[value='" + week + "']").length > 0)
        $("#selectFilterWeek").val(week);

    $('.selectpicker').selectpicker('refresh');
}

function setUpDynamicCompanyFilter(show) {

    var company = document.getElementById("dynamicCompanyFilter");
    var wmarea = document.getElementById("dynamicWMAreaFilter");

    if (show) {
        //show wm area
        $("#dynamicCompanyFilter").hide();
        $("#dynamicWMAreaFilter").show();
        sessionStorage.sessionWMCheck = true;
    } else {
        //hide wm area
        $("#dynamicCompanyFilter").show();
        $("#dynamicWMAreaFilter").hide();
        sessionStorage.sessionWMCheck = false
    }
}
function showZonaDropdown(show) {
    var zona = document.getElementById("dynamicZonaFilter");
    if (show) {
        $("#dynamicZonaFilter").show();
        $("#dynamicWMAreaFilter").show();
        $("#dynamicCompanyFilter").hide();
        isCheckZonaTrue = sessionStorage.sessionZonaCheck = true;
    } else {
        $("#dynamicZonaFilter").hide();
        isCheckZonaTrue = sessionStorage.sessionZonaCheck = false;
        //$("#dynamicCompanyFilter").show();
        //$("#dynamicWMAreaFilter").hide();
    }
}
function ListZonaByWmArea(idWmArea) {
    var json = $.parseJSON(extentCoordinate);
    //console.log('json response ListZonaByWmArea =  ' + JSON.stringify(json));
    $('#selectFilterZona').find('option').remove();
    for (var i = 0; i < json.length; i++) {
        if (json[i]["idWMArea"] == idWmArea)
            $("#selectFilterZona").append($('<option>', {
                value: json[i]["WMA_code"],
                text: json[i]["WMA_code"]
            }));
    }
    $('.selectpicker').selectpicker('refresh');

}
function ListPieRecordID(wmaCode) {

    var json = $.parseJSON(dataBlock);
    $("#selectFilterBlock").find('option').remove();
    for (var i = 0; i < json.length; i++) {
        if (json[i]["WMA_code"] === wmaCode) {
            $("#selectFilterBlock").append($('<option>', {
                value: json[i]["PieRecordID"],
                text: json[i]["Block"]
            }));
        }
    }
    $('.selectpicker').selectpicker('refresh');
    getBlockParameter(wmaCode);
    $(".loading").hide();
}


function reLoad(idweek) {
    var isTrue = (sessionStorage.sessionWMCheck == 'true');
    if (isTrue) {
        window.location = "PZO_NewAnalysisTMAS.aspx?typeModule=OLM&WMACODE=" + $("#selectFilterZona").val() + "&year=" + year + "&month=" + month + "&week=" + week + "&Company=" + $("#selectFilterCompany").val();
    } else {
        window.location = "PZO_NewAnalysisTMAS.aspx?typeModule=OLM&WMACODE=" + $("#selectFilterZona").val() + "&year=" + year + "&month=" + month + "&week=" + week;
    }
    //'viewparams': 'WMACODE:' + 'A05A' + ';year:' + year + ';month:' + 4 + ';week:' + 4,
}

function zoomToZona() {
    //console.log('extent Coordinate = ' + JSON.stringify(extentCoordinate));
    //var json = JSON.parse(extentCoordinate);
    //mapped_estate = $('#mapped_estate').val();
    //estate = $('#estate').val();
    //var utmprojection = $('#utmprojection').val();
    //var rsid = $('#rsid').val();
    //var coordinateExtent = $('#extent').val().split(',');;
    //var numzoom = $('#numzoom').val();
    //var company = $('#company').val();
    //var idwm = $('#idwm').val();
    //var estatezone = $('#estatezone').val();
    //for (var i = 0; i < json.length; i++) {
    //	if (json[i]["WMA_code"] == $("#selectFilterZona").val()) {
    //		latLong = [parseFloat(json[i]["MinX"]), parseFloat(json[i]["MinY"]), parseFloat(json[i]["MaxX"]), parseFloat(json[i]["MaxY"])];
    //		centerCoordinate = [(latLong[0] + latLong[2]) / 2, (latLong[1] + latLong[3]) / 2];
    //	}
    //}

    //console.log('latLong = ' + latLong);
    //console.log('center koordinate = ' + centerCoordinate);
    //console.log('year = ' + year);
    //console.log('wmArea = ' + idwm);
    //console.log('month = ' + month);
    //console.log('week = ' + week);
    //zoomLevel = 15;
    //mapProjection = 'EPSG:4326';
    //map.updateParams
    //map.getView().fit(latLong, { duration: 0 })



    //if (isTrueCkWM) {
    //	layerBlock = new ol.layer.Image({
    //		source: new ol.source.ImageWMS({
    //			url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
    //			params: {
    //				'LAYERS': 'pub_pzo:pzo_block_week_wmarea_unitpemantauan', //warnanya
    //				'viewparams': 'YEAR:' + year + ';WMAREA:' + idwm + ';MONTH:' + month + ';WEEK:' + week,
    //				'TRANSPARENT': 'true'
    //			},
    //		})
    //	});

    //	layerPiezometer = new ol.layer.Image({
    //		source: new ol.source.ImageWMS({
    //			url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
    //			params: {
    //				'LAYERS': 'pub_pzo:PZO_Point_Week_WM_Area_unitpemantauan',
    //				'viewparams': 'years:' + year + ';WMAREA:' + idwm + ';months:' + month + ';weeks:' + week,
    //				'TRANSPARENT': 'true'
    //			}
    //		})
    //	});
    //} else {
    //	layerBlock = new ol.layer.Image({
    //		source: new ol.source.ImageWMS({
    //			url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
    //			params: {
    //				'LAYERS': 'PZO:pzo_block_week_unitpemantauan', //warna pendek
    //				'viewparams': 'years:' + year + ';estate:' + mapped_estate + ';months:' + month + ';weeks:' + week,
    //				'TRANSPARENT': 'true'

    //			},
    //		})
    //	});


    //	layerPiezometer = new ol.layer.Image({
    //		source: new ol.source.ImageWMS({
    //			url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
    //			params: {
    //				'LAYERS': 'PZO:PZO_Point_Week_unitpemantauan',
    //				'viewparams': 'years:' + year + ';estate:' + mapped_estate + ';months:' + month + ';weeks:' + week,
    //				'TRANSPARENT': 'true'
    //			}
    //		})
    //	});
    //}


    //layerInfo = new ol.layer.Tile({
    //	source: new ol.source.TileWMS({
    //		url: 'https://map.gis-div.com:8443/geoserver/pub_pzo/wms',
    //		params: {
    //			'LAYERS': 'pub_pzo:PZO_Info',
    //			'viewparams': 'PZOYEAR:' + year + ';ESTNR:' + mapped_estate + ';PZOMONTH:' + month + ';PZOWEEK:' + week,
    //			'TRANSPARENT': 'true'
    //		}
    //	})
    //});

    //sourceLayerDrawing = new ol.source.Vector({ wrapX: false });

    //layerDrawing = new ol.layer.Vector({
    //	source: sourceLayerDrawing
    //});

    //sourceLayerMeasure = new ol.source.Vector();

    //layerMeasure = new ol.layer.Vector({
    //	source: sourceLayerMeasure,
    //	style: new ol.style.Style({
    //		fill: new ol.style.Fill({
    //			color: 'rgba(255, 255, 255, 0.2)'
    //		}),
    //		stroke: new ol.style.Stroke({
    //			color: '#ffcc33',
    //			width: 2
    //		}),
    //		image: new ol.style.Circle({
    //			radius: 7,
    //			fill: new ol.style.Fill({
    //				color: '#ffcc33'
    //			})
    //		})
    //	})
    //});

    //map = new ol.Map({
    //	controls: ol.control.defaults({
    //		attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
    //			collapsible: false
    //		})
    //	}).extend([
    //		scaleLineControl, mousePositionControl, zoomSliderControl
    //	]),
    //	target: 'map',
    //	view: new ol.View({
    //		extent: latLong,
    //		projection: mapProjection,
    //		center: centerCoordinate,
    //		zoom: zoomLevel
    //	})
    //});


    //map.addLayer(layerBlock);
    //map.addLayer(layerPiezometer);
    //map.addLayer(layerInfo);
    //map.addLayer(layerDrawing);
    //map.addLayer(layerMeasure);

    //layerBlock.setVisible(true);
    //layerPiezometer.setVisible(true);
    //layerInfo.setVisible(true);
}
function GetMasterRambuAir(wmaCode, week, month, idWm, year) {

    if ($("#divChartTMASLoading") != undefined) {
        $("#divChartTMASLoading").show();
    }
    //console.log('get master rambu air = ' + wmaCode);
    //console.log('get master rambu air = ' + week);
    //console.log('get master rambu air = ' + month);
    //console.log('get master rambu air = ' + idWm);
    //console.log('get master rambu air = ' + year);
    var arrRambuAir = [];
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/GetMasterRambuAir',
        data: '{wmaCode: "' + wmaCode + '",week:"' + week + '",month:"' + month + '",idWmArea:"' + idWm + '",year:"' + year + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        //async: false,
        success: function (response) {
            //console.log('response = ' + JSON.stringify(response));
            var json = $.parseJSON(response.d);
            if (json.length > 0) {
                for (var i = 0; i < json.length; i++) {
                    arrRambuAir.push(json[i]["stationName"]);
                }
                //console.log('array rambu air master = ' + arrRambuAir);
                GetChartTMAS(wmaCode, week, month, idWm, year, arrRambuAir);
            }
            if ($("#divChartTMASLoading") != undefined) {
                $("#divChartTMASLoading").hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.statusText);
            if ($("#divChartTMASLoading") != undefined) {
                $("#divChartTMASLoading").hide();
            }
        }
    });
}
function GetChartTMAS(wmaCode, week, month, idWm, year, arrRambuAir) {

    var arrColor = ["#FF5733", "#740D0D", "#DA7211", "#8C6C00", "#8C6C00", "#049E56", "#09DC99", "#008783", "#3A69BD", "#4C3ABD", "#4C1D02"]
    var indexArrRambuAir = 0;
    var arrTMAS = [];
    var dataNilai = [];
    var dataBar = [];
    var configDataBar = [];
    var grafik = [];
    var MinValueStripLines;
    var MaxValueStripLines;
    var initialRambu = arrRambuAir[indexArrRambuAir];
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/GetChartTMAS',
        data: '{wmaCode: "' + wmaCode + '",week:"' + week + '",month:"' + month + '",idWmArea:"' + idWm + '",year:"' + year + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        //async:false,
        success: function (response) {
            var json = $.parseJSON(response.d);
            console.log('balikan get chart tmas = ' + JSON.stringify(json));
            for (var i = 0; i < arrRambuAir.length; i++) {
                var arrData = [];
                for (var j = 0; j < json.length; j++) {
                    if (json[j]["rainInches"] != 0) {
                        dataBar.push({
                            x: new Date(parseInt(json[j]["tanggal"].replace('/Date(', '').replace(')/', ''))),
                            y: json[j]["rainInches"]
                        })
                    }
                    //console.log(arrRambuAir[i] + '=' + initialRambu);
                    if (arrRambuAir[i] == initialRambu) {
                        if (json[j][arrRambuAir[i]] != 0) {

                            arrData.push({
                                x: new Date(parseInt(json[j]["tanggal"].replace('/Date(', '').replace(')/', ''))),
                                //name: arrRambuAir[i],
                                y: json[j][arrRambuAir[i]]
                            });
                        }

                        if (json[j][arrRambuAir[i]] != 0) {
                            arrTMAS.push(json[j][arrRambuAir[i]]);
                        }

                    }
                    MinValueStripLines = json[j]["minTarget"];
                    MaxValueStripLines = json[j]["maxTarget"];
                    var NamaGraph = json[j]["namaGraph"];
                }

                dataNilai.push({
                    type: "line",
                    markerSize: 8,
                    markerType: "circle",
                    dataPoints: arrData,
                    lineColor: arrColor[i]
                })
                //console.log('data bar = ' + JSON.stringify(dataBar));
                indexArrRambuAir++;
                //console.log('data bar = ' + JSON.stringify(dataBar));
                initialRambu = arrRambuAir[indexArrRambuAir];


            }

            grafik.push({
                type: "column",
                axisYType: "secondary",
                name: "Curah Hujan",
                dataPoints: dataBar
                //lineColor: arrColor[0]
            })

            //console.log('data nilai =' + JSON.stringify(dataNilai));
            for (var i = 0; i < dataNilai.length; i++) {
                grafik.push(dataNilai[i]);
            }
            setupChartTMAS(arrData, i, arrTMAS, dataNilai, configDataBar, grafik, MinValueStripLines, MaxValueStripLines, NamaGraph);
            //updateBlock($("#selectFilterZona").val(), $("#selectFilterBlock").val());

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.statusText);
        }
    });
}
function setupChartTMAS(data, index, arrNilaiTMAS, dataNilai, dataBar, grafik, MinValueStripLines, MaxValueStripLines, NamaGraph) {


    var dataPoints = [];

    var objAxisX = {
        reversed: false,
        title: "Minggu",
        labelFontSize: 10,
        titleFontSize: 15
    };


    var chart = new CanvasJS.Chart("divChartTMAS", {
        title: {
            text: NamaGraph,
            fontWeight: "bolder",
            fontFamily: "Calibri",
            fontSize: 15,
            padding: 1
        },
        zoomEnabled: true,
        axisXType: "secondary",
        animationEnabled: true,
        axisY: {
            stripLines: [
                {
                    startValue: parseFloat(MinValueStripLines),
                    endValue: parseFloat(MaxValueStripLines),
                    color: "#42f480"
                }
            ],
            reversed: false,
            title: "Kedalaman Air",
            minimum: Math.min.apply(null, arrNilaiTMAS) - 0.05,
            maximum: Math.max.apply(null, arrNilaiTMAS) + 0.05,
            labelFontSize: 10,
            titleFontSize: 15
        },
        axisY2: {
            title: "Curah Hujan (mm)",
            labelFontSize: 10,
            titleFontSize: 15
        },
        axisX: objAxisX,
        data: grafik      
    });

    chart.render();
    //$(".loading").hide();

}
function getPreviousDateMap(week, month, year) {

    var coordinateExtentPulau;
    var centerCoordinatePulau;
    map.removeLayer(layerPiezometer)
    map.removeLayer(layerHighlight);
    //alert('check zona checked');
    for (var i = 0; i < listallwmarea.length; i++) {
        if (listallwmarea[i]["idWMArea"] == $("#selectFilterWMArea").val()) {
            coordinateExtentPulau = [parseFloat(listallwmarea[i]["MinX"]), parseFloat(listallwmarea[i]["MinY"]), parseFloat(listallwmarea[i]["MaxX"]), parseFloat(listallwmarea[i]["MaxY"])];
            centerCoordinatePulau = [(coordinateExtentPulau[0] + coordinateExtentPulau[2]) / 2, (coordinateExtentPulau[1] + coordinateExtentPulau[3]) / 2];
            break;
        }
    }

    layerBlock.getSource().updateParams({
        'LAYERS': 'pub_pzo:pzo_block_week_wmarea_unitpemantauan',
        'viewparams': 'YEAR:' + year + ';WMAREA:' + $("#selectFilterWMArea").val() + ';MONTH:' + month + ';WEEK:' + week,
        'TRANSPARENT': 'true',
        'TILED': true
    });

    var style = "";
    latLong = "";
    centerCoordinate = "";
    var json = JSON.parse(extentCoordinate);
    for (var j = 0; j < json.length; j++) {
        if (json[j]["WMA_code"] == $("#selectFilterZona").val()) {
            //console.log(json[j]["WMA_code"] + '&' + $("#selectFilterZona").val() + ' sama ')
            coordinateExtent = [parseFloat(json[j]["MinX"]), parseFloat(json[j]["MinY"]), parseFloat(json[j]["MaxX"]), parseFloat(json[j]["MaxY"])];
            centerCoordinate = [(coordinateExtent[0] + coordinateExtent[2]) / 2, (coordinateExtent[1] + coordinateExtent[3]) / 2];
        }
    }
    style = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: '#9b59b6',
            width: 5
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.3)'
        }),
    });

    var UrlCQL = "http://map.gis-div.com:8443/geoserver/pub_pzo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=pub_pzo:PZO_WMZone&CQL_FILTER=WMA_code=%27" + $("#selectFilterZona").val() + "%27&maxFeatures=50&outputFormat=application/json";
    layerHighlight = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: UrlCQL,
            format: new ol.format.GeoJSON()
        }),
        style: function (feature) {

            return style;
        }
    });


    zoomLevel = 16;

    map.setView(new ol.View({
        extent: coordinateExtentPulau,
        projection: mapProjection,
        center: centerCoordinatePulau,
        zoom: 12
    }));
    map.addLayer(layerBlock);
    map.addLayer(layerHighlight);

    //map.getView().fit(coordinateExtentPulau, { duration: 0 })
}
function updateBlock(wmaCode, block) {
    flagButtonGoClick = 1;
    console.log('update block = ' + block);
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/UpdateBlock',
        data: '{wmaCode: "' + wmaCode + '", block: "' + block + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        //async:false,
        success: function (response) {
            //alert('update parameter success');
            //var block = $("#selectFilterBlock").val();
            //var blockCollection = block.toString().split(',');
            //indexAkhir = indexDiv + blockCollection.length;
            //for (var c = 0; c < blockCollection.length; c++) {

            //    var val = blockCollection[c];
            //    var txt = $("#selectFilterBlock option[value='" + val + "']").text();
            //    arrBlockName.push(txt);
            //}

            //for (var a = 0; a < blockCollection.length; a++) {
            //    arrBlock.push(blockCollection[a]);
            //}
            //for (var b = indexDiv; b < arrBlock.length; b++) {
            //    var content = "";
            //    content += "<div class='col-lg-6' style='height: 420px;'>";
            //    content += "<div id='tempGraphDiv" + (b % 4) + "'>";
            //    content += "</div>";
            //    content += "</div>";
            //    arrDiv.push(content);

            //}
            //indexDiv = a;
            //console.log('arrBlock = ' + arrBlock);
            //ShowDetailV3($("#selectFilterZona").val(), week, month, $("#selectFilterWMArea").val(), year, arrBlock, b, arrDiv, 0, arrBlockName)


        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.statusText);
        }
    });
}
function getBlockParameter(wmaCode) {
    var json = JSON.parse(dataBlockParameter);

    for (var i = 0; i < json.length; i++) {
        if (json[i]["WMA_code"] == wmaCode) {
            if (json[i]["Block"] != null || json[i]["Block"] != 'null') {
                var blockCollection = json[i]["Block"].split(',');
                for (var x = 0; x < blockCollection.length; x++) {
                    arrBlockParam.push(blockCollection[x]);
                }
            }


        }
    }
    $('#selectFilterBlock').selectpicker('val', arrBlockParam);
    $('.selectpicker').selectpicker('refresh');
}
function DownloadLaporanPDF() {
    if (sessionStorage.PreviousDate != null && sessionStorage.PreviousDate != "null") {
        var time = sessionStorage.PreviousDate.split(',');
        week = time[0];
        month = time[1];
        year = time[2];
    }
    window.location.href = "../Handler/WLR_AnalisaPerbandinganTMATdanTMASbyZona.ashx?zona=" + $("#selectFilterZona").val() + "&week=" + week + "&month=" + month + "&year=" + year + "&pzoid=" + $("#selectFilterBlock").val();

    //var link = "~/content/RPT/ReportHandler/WLR_AnalisaPerbandinganTMATdanTMASbyZona.ashx?zona=" + zona + "&week=" + week + "&month=" + month + "&year=" + year + "&pzoid=" + pzoid;
}
function getNextDateMap(week, month, year) {
    var coordinateExtentPulau;
    var centerCoordinatePulau;
    map.removeLayer(layerPiezometer)
    map.removeLayer(layerHighlight);
    //alert('check zona checked');
    for (var i = 0; i < listallwmarea.length; i++) {
        if (listallwmarea[i]["idWMArea"] == $("#selectFilterWMArea").val()) {
            coordinateExtentPulau = [parseFloat(listallwmarea[i]["MinX"]), parseFloat(listallwmarea[i]["MinY"]), parseFloat(listallwmarea[i]["MaxX"]), parseFloat(listallwmarea[i]["MaxY"])];
            centerCoordinatePulau = [(coordinateExtentPulau[0] + coordinateExtentPulau[2]) / 2, (coordinateExtentPulau[1] + coordinateExtentPulau[3]) / 2];
            break;
        }
    }

    layerBlock.getSource().updateParams({
        'LAYERS': 'pub_pzo:pzo_block_week_wmarea_unitpemantauan',
        'viewparams': 'YEAR:' + year + ';WMAREA:' + $("#selectFilterWMArea").val() + ';MONTH:' + month + ';WEEK:' + week,
        'TRANSPARENT': 'true',
        'TILED': true
    });

    var style = "";
    latLong = "";
    centerCoordinate = "";
    var json = JSON.parse(extentCoordinate);
    for (var j = 0; j < json.length; j++) {
        if (json[j]["WMA_code"] == $("#selectFilterZona").val()) {
            //console.log(json[j]["WMA_code"] + '&' + $("#selectFilterZona").val() + ' sama ')
            coordinateExtent = [parseFloat(json[j]["MinX"]), parseFloat(json[j]["MinY"]), parseFloat(json[j]["MaxX"]), parseFloat(json[j]["MaxY"])];
            centerCoordinate = [(coordinateExtent[0] + coordinateExtent[2]) / 2, (coordinateExtent[1] + coordinateExtent[3]) / 2];
        }
    }
    style = new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: '#9b59b6',
            width: 5
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.3)'
        }),
    });

    var UrlCQL = "http://map.gis-div.com:8443/geoserver/pub_pzo/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=pub_pzo:PZO_WMZone&CQL_FILTER=WMA_code=%27" + $("#selectFilterZona").val() + "%27&maxFeatures=50&outputFormat=application/json";
    layerHighlight = new ol.layer.Vector({
        source: new ol.source.Vector({
            url: UrlCQL,
            format: new ol.format.GeoJSON()
        }),
        style: function (feature) {

            return style;
        }
    });


    zoomLevel = 16;

    map.setView(new ol.View({
        extent: coordinateExtentPulau,
        projection: mapProjection,
        center: centerCoordinatePulau,
        zoom: 12
    }));
    map.addLayer(layerBlock);
    map.addLayer(layerHighlight);

    //map.getView().fit(coordinateExtentPulau, { duration: 0 })
}
function DownloadLaporanAllBlock() {
    if (sessionStorage.PreviousDate != null && sessionStorage.PreviousDate != "null") {
        var time = sessionStorage.PreviousDate.split(',');
        week = time[0];
        month = time[1];
        year = time[2];
    }
    window.location.href = "../Handler/WLR_AnalisaPerbandinganTMATdanTMASbyZonaAllBlock.ashx?zona=" + $("#selectFilterZona").val() + "&week=" + week + "&month=" + month + "&year=" + year + "&pzoid=" + $("#selectFilterBlock").val();

    //var link = "~/content/RPT/ReportHandler/WLR_AnalisaPerbandinganTMATdanTMASbyZona.ashx?zona=" + zona + "&week=" + week + "&month=" + month + "&year=" + year + "&pzoid=" + pzoid;
}






