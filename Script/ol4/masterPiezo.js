﻿var tableMasterPiezo, tableMdlBlock, tableIOT;
var dataPiezo, idxTableIOT, masterIOT, userid;
var labelChart, mStartDate, mEndtDate, dataMeasurePiezo, mWaterDeepindicator, dataMeasureIOT, dataTblIOT, startCallibrate;
let dateNow = new Date();
let arr = [];
$(function () {
    dataTblIOT = [];
    dataPiezo = [];
    idxTableIOT = 0;
    masterIOT = [];
    tableMasterPiezo = $('#tableMasterPiezo').DataTable({
        "scrollX": true,
        dom: 'Blfrtip',
        "deferRender": true,
        "paging": false,
        buttons: ['excel']
    });

    tableMdlBlock = $('#tableMdlBlock').DataTable({
        "scrollX": true,
        dom: 'lfrti',
        searching: false,
        "deferRender": true,
        "paging": false,
        buttons: [],
    });


    tableIOT = $('#tableIOT').DataTable({
        "sScrollY": ($(window).height() - 400),
        "scrollX": true,
        dom: 'Blfrtip',
        searching: false,
        "deferRender": true,
        "paging": false,
        "order": [[3, 'asc']]
    });

    tableIOT.column(0).visible(false);

    $("#tableIOT").on('click', '.remove', function () {
        var table = $('#tableIOT').DataTable();
        var row = $(this).parents('tr');
        var row2 = $(this).closest('tr');

        // Get the DataTable row index
        var rowIndex = table.row(row2).index();
        if ($(row).hasClass('child')) {
            table.row($(row).prev('tr')).remove().draw();
        }
        else {
            table.row($(this).parents('tr')).remove().draw();
            dataTblIOT.splice(rowIndex, 1);
            let dateString = "12/31/9999";

            dataTblIOT.sort((a, b) => new Date(a.dateParam) - new Date(b.dateParam));
            let newValues;
            for (var s = 0; s < dataTblIOT.length; s++) {
                //if (dataTblIOT[s].dateParam2 === "12/31/9999") {

                if ((s + 1) === dataTblIOT.length) {
                    newValues = { startDate: toDateFormat(new Date(dataTblIOT[s].dateParam).getTime()), endDate: toDateFormat(new Date(dateString).getTime()), dateParam2: dateString, endIOTDate: new Date(dateString) };
                }
                else {
                    newValues = { startDate: toDateFormat(new Date(dataTblIOT[s].dateParam).getTime()), endDate: (new Date(dataTblIOT[s].dateParam).getTime() === new Date(dataTblIOT[s + 1].dateParam).getTime() ? toDateFormat(new Date(dataTblIOT[s].dateParam).getTime()) : toDateFormat(new Date(getYesterday(dataTblIOT[s + 1].dateParam)).getTime())), dateParam2: (new Date(dataTblIOT[s].dateParam).getTime() === new Date(dataTblIOT[s + 1].dateParam).getTime() ? dataTblIOT[s + 1].dateParam : getYesterday(dataTblIOT[s + 1].dateParam)), endIOTDate: (new Date(dataTblIOT[s].dateParam).getTime() === new Date(dataTblIOT[s + 1].dateParam).getTime() ? new Date(dataTblIOT[s + 1].dateParam) : new Date(getYesterday(dataTblIOT[s + 1].dateParam))) };
                }
                for (let key in newValues) {
                    dataTblIOT[s][key] = newValues[key];
                }
                //}
            }
            tableIOT.row.add($(addRowDataIOT())[0]).clear().draw(false);
            for (var s = 0; s < dataTblIOT.length; s++) {
                addTblRowDataIOT(dataTblIOT[s], 2);
            }

        }

    });

    $("#tableIOT").on('click', '.recalculate', function () {
        //var tr = $(this).closest('tr'); // Get the closest row element
        //var rowIndex = tableIOT.row(tr).index(); // Get the row index
        //var rowData = tableIOT.row(tr).data(); // Get the row data
        //var idx = rowData[0];

        recalculateIOT(false);
    });

    $("#selectFilteringEstate").change(function () {
        ListHistoryDAftarPizometer();
        ListHistorySummaryPizometer();
        getAllIotTMATByEstate();
    });

    $("#selectFiltering").change(function () {
        ListHistoryDAftarPizometer();
        ListHistorySummaryPizometer();
    });

    $('#mTglSelected').daterangepicker({
        ranges: {
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Quarter': [getQuartalStartEnd(moment().month(),1), getQuartalStartEnd(moment().month(),2)],
            'Last 1 Year': [moment().subtract(365, 'days'), moment()],
            'This Year': [moment().startOf('year'), moment().endOf('year')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        locale: {
            format: 'DD/MM/YYYY'
        }
    });
    $("#btnPrevWeek").click(function () {
        if ($("#mTglSelected").data('daterangepicker').chosenLabel == "This Month") {
            var split = $('#mTglSelected').val().split('-');

            var sd = split[0].trim().split("/");
            var ed = split[1].trim().split("/");

            var startDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

            var momentDate = moment(startDate);
            var momentDate = momentDate.add(- 1, 'months');

            $("#mTglSelected").data('daterangepicker').setStartDate(momentDate.startOf('month'));
            $("#mTglSelected").data('daterangepicker').setEndDate(momentDate.endOf('month'));
        } else if ($("#mTglSelected").data('daterangepicker').chosenLabel == "Quarter") {
            var ArrQuart = [{ Q: '01,02,03' }, { Q: '04,05,06' }, { Q: '07,08,09' }, { Q: '10,11,12' }];

            var split = $('#mTglSelected').val().split('-');

            var sd = split[0].trim().split("/");
            var ed = split[1].trim().split("/");
            //if (arr.length>0) {
                for (var i = 0; i < ArrQuart.length; i++) {
                    const index = ArrQuart[i]['Q'].indexOf(ed[1]);
                    if (index !== -1) {
                        if (i > 0) {
                            var startDate = new Date(parseInt(ed[2]), (parseInt(ArrQuart[(i - 1)]['Q'].trim().split(",")[0]) - 1), parseInt(getYesterday2(split[0])));
                            var endDate = new Date(parseInt(ed[2]), (parseInt(ArrQuart[(i - 1)]['Q'].trim().split(",")[2]) - 1), parseInt(getYesterday2(split[0])));
                        }
                        else {
                            var startDate = new Date(parseInt((ed[2] - 1)), (parseInt(ArrQuart[(i + 3)]['Q'].trim().split(",")[0]) - 1), parseInt(ed[0]));
                            var endDate = new Date(parseInt((ed[2] - 1)), (parseInt(ArrQuart[(i + 3)]['Q'].trim().split(",")[2]) - 1), parseInt(ed[0]));
                        }

                        arr.push(i);
                    } else {
                        console.log(`tidak ditemukan`);
                    }
                }
            //}
 



            var momentDate = moment(startDate);
            var momentDate = momentDate.add(0, 'months');


            var momentEndDate = moment(endDate);
            var momentEndDate = momentEndDate.add(0, 'months');

            $("#mTglSelected").data('daterangepicker').setStartDate(momentDate.startOf('month'));
            $("#mTglSelected").data('daterangepicker').setEndDate(momentEndDate.endOf('month'));
            
        }
        else if ($("#mTglSelected").data('daterangepicker').chosenLabel == "This Year") {
            var split = $('#mTglSelected').val().split('-');

            var sd = split[0].trim().split("/");
            var ed = split[1].trim().split("/");

            var startDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

            var momentDate = moment(startDate);
            var momentDate = momentDate.add(- 1, 'year');

            $("#mTglSelected").data('daterangepicker').setStartDate(momentDate.startOf('year'));
            $("#mTglSelected").data('daterangepicker').setEndDate(momentDate.endOf('year'));
        } else {
            var split = $('#mTglSelected').val().split('-');

            var sd = split[0].trim().split("/");
            var ed = split[1].trim().split("/");

            var startDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
            var endtDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

            var difference = endtDate - startDate;

            let diff = Math.floor(difference / (24 * 3600 * 1000));

            var endtDate2 = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
            var startDate2 = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));

            endtDate2.setDate(endtDate2.getDate() - 1);
            startDate2.setDate(startDate2.getDate() - 1);
            startDate2.setDate(startDate2.getDate() - diff);

            $("#mTglSelected").data('daterangepicker').setStartDate(startDate2);
            $("#mTglSelected").data('daterangepicker').setEndDate(endtDate2);
            console.info("difference", diff);
        }
    });

    $("#btnNextWeek").click(function () {
        if ($("#mTglSelected").data('daterangepicker').chosenLabel == "This Month") {
            var split = $('#mTglSelected').val().split('-');

            var sd = split[0].trim().split("/");
            var ed = split[1].trim().split("/");

            var startDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

            var momentDate = moment(startDate);
            var momentDate = momentDate.add(1, 'months');

            $("#mTglSelected").data('daterangepicker').setStartDate(momentDate.startOf('month'));
            $("#mTglSelected").data('daterangepicker').setEndDate(momentDate.endOf('month'));
        } else if ($("#mTglSelected").data('daterangepicker').chosenLabel == "Quarter") {
            var ArrQuart = [{ Q: '01,02,03' }, { Q: '04,05,06' }, { Q: '07,08,09' }, { Q: '10,11,12' }];

            var split = $('#mTglSelected').val().split('-');

            var sd = split[0].trim().split("/");
            var ed = split[1].trim().split("/");
            //if (arr.length>0) {
            for (var i = 0; i < ArrQuart.length; i++) {
                const index = ArrQuart[i]['Q'].indexOf(getYesterday2(split[1]).split("/")[0]);
                if (index !== -1) {
                    if (i < 3) {
                    var startDate = new Date(parseInt(ed[2]), (parseInt(ArrQuart[(i + 1)]['Q'].trim().split(",")[0]) - 1), parseInt(getYesterday2(split[1])));
                        var endDate = new Date(parseInt(ed[2]), (parseInt(ArrQuart[(i + 1)]['Q'].trim().split(",")[2]) - 1), parseInt(getYesterday2(split[1])));
                    }
                    else {
                        var startDate = new Date(parseInt(ed[2]) + 1, (parseInt(ArrQuart[(i - 3)]['Q'].trim().split(",")[0]) - 1), parseInt(ed[0]));
                        var endDate = new Date(parseInt(ed[2]) + 1, (parseInt(ArrQuart[(i - 3)]['Q'].trim().split(",")[2]) - 1), parseInt(ed[0]));
                    }

                    arr.push(i);
                } else {
                    console.log(`tidak ditemukan`);
                }
            }
            //}




            var momentDate = moment(startDate);
            var momentDate = momentDate.add(0, 'months');


            var momentEndDate = moment(endDate);
            var momentEndDate = momentEndDate.add(0, 'months');

            $("#mTglSelected").data('daterangepicker').setStartDate(momentDate.startOf('month'));
            $("#mTglSelected").data('daterangepicker').setEndDate(momentEndDate.endOf('month'));

        } else if ($("#mTglSelected").data('daterangepicker').chosenLabel == "This Year") {
            var split = $('#mTglSelected').val().split('-');

            var sd = split[0].trim().split("/");
            var ed = split[1].trim().split("/");

            var startDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

            var momentDate = moment(startDate);
            var momentDate = momentDate.add(1, 'year');

            $("#mTglSelected").data('daterangepicker').setStartDate(momentDate.startOf('year'));
            $("#mTglSelected").data('daterangepicker').setEndDate(momentDate.endOf('year'));
        } else {
            var split = $('#mTglSelected').val().split('-');

            var sd = split[0].trim().split("/");
            var ed = split[1].trim().split("/");

            var startDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
            var endtDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

            var difference = endtDate - startDate;

            let diff = Math.floor(difference / (24 * 3600 * 1000));

            var endtDate2 = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));
            var startDate2 = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

            endtDate2.setDate(endtDate2.getDate() + 1);
            startDate2.setDate(startDate2.getDate() + 1);
            endtDate2.setDate(endtDate2.getDate() + diff);

            $("#mTglSelected").data('daterangepicker').setStartDate(startDate2);
            $("#mTglSelected").data('daterangepicker').setEndDate(endtDate2);
            console.info("difference", diff);
        }
    });

    $("#btnThis").click(function () {
        if ($("#mTglSelected").data('daterangepicker').chosenLabel == "This Month") {
            var momentDate = moment(new Date());

            $("#mTglSelected").data('daterangepicker').setStartDate(momentDate.startOf('month'));
            $("#mTglSelected").data('daterangepicker').setEndDate(momentDate.endOf('month'));

        } else if ($("#mTglSelected").data('daterangepicker').chosenLabel == "This Year") {
            var momentDate = moment(new Date());

            $("#mTglSelected").data('daterangepicker').setStartDate(momentDate.startOf('year'));
            $("#mTglSelected").data('daterangepicker').setEndDate(momentDate.endOf('year'));

        } else {
            var split = $('#mTglSelected').val().split('-');

            var sd = split[0].trim().split("/");
            var ed = split[1].trim().split("/");

            var startDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
            var endtDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

            var difference = endtDate - startDate;

            let diff = Math.floor(difference / (24 * 3600 * 1000));

            var endtDate2 = new Date();
            var startDate2 = new Date();

            startDate2.setDate(startDate2.getDate() - diff);

            $("#mTglSelected").data('daterangepicker').setStartDate(startDate2);
            $("#mTglSelected").data('daterangepicker').setEndDate(endtDate2);
            console.info("difference", diff);
        }
    });

    $('#mTglSelected').change(function () {
        getPiezoMeasure();
    });

    $("#btnTambah").click(function () {
        var data = {};
        addTblRowDataIOT(data, 1);
    });

    $("#btnMdlSaveIOT").click(function () {
        recalculateIOT(true);
    })

    $("#btnMdlSave").click(function () {
        preSaveDataUnitPemantauaan();
    })

    ListEstate();
    GetWaterDepthIndicator();
});

function getQuartalStartEnd(month,flag) {
    let start, end;
    if (month <= 2) { // Kuartal 1: Jan - Mar
        start = moment().startOf('year');
        end = moment(start).endOf('quarter');
    } else if (month <= 5) { // Kuartal 2: Apr - Jun
        start = moment().startOf('year').add(3, 'months');
        end = moment(start).endOf('quarter');
    } else if (month <= 8) { // Kuartal 3: Jul - Sep
        start = moment().startOf('year').add(6, 'months');
        end = moment(start).endOf('quarter');
    } else { // Kuartal 4: Okt - Des
        start = moment().startOf('year').add(9, 'months');
        end = moment(start).endOf('quarter');
    }
    if (flag == 1) {
        return start;
    }
    else {
        return end;
    }
}


function getTomorrowDate(dateStr) {
    // Parse the date string
    let parts = dateStr.split('/');
    let month = parseInt(parts[0], 10) - 1; // Months are zero-indexed in JavaScript
    let day = parseInt(parts[1], 10);
    let year = parseInt(parts[2], 10);

    // Create a Date object
    let date = new Date(year, month, day);

    // Add one day
    date.setDate(date.getDate() + 1);

    // Format the new date back to "MM/DD/YYYY"
    let tomorrowMonth = (date.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-indexed
    let tomorrowDay = date.getDate().toString().padStart(2, '0');
    let tomorrowYear = date.getFullYear();

    return `${tomorrowMonth}/${tomorrowDay}/${tomorrowYear}`;
}

function dateFormat(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = dd + '/' + mm + '/' + yyyy;
}

function dateFormat2(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = mm + '/' + dd + '/' + yyyy;
}
function dateFormat3(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();

    var hh = date.getHours();
    var mn = date.getMinutes(); //January is 0!
    var ss = date.getSeconds();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = mm + '/' + dd + '/' + yyyy + ' ' + hh + ':' + mn + ':' + ss;
}

function addRowDataMasterPiezo(json) {
    var content = '<tr>';
    content += '<td style="width:2%;">' + json["Number"] + '</td>';
    content += '<td style="width:10%;">' + json["UnitPemantauan"] + '</td>';
    content += '<td style="width:5%;">' + json["EstCode"] + '</td>';
    content += '<td style="width:5%;">' + json["Division"] + '</td>';
    content += '<td style="width:5%;">' + json["Mapping"] + '</td>';
    content += '<td style="width:10%;">' + json["Jenis"] + '</td>';
    content += '<td style="width:10%;">' + json["idUnitPemantauan"] + '</td>';
    content += '<td style="width:10%;">' + json["History"] + '</td>';
    content += '<td style="width:10%;">' + json["History"] + '</td>';
    content += '<td style="width:10%;">' + json["Distance"] + '</td>';
    content += '<td style="width:10%;">' + json["deviceNameIOT"] + '</td>';
    //content += '<td style="width:10%;">' + json["StatusUP"] + '</td>';
    content += '<td style="width:30%;">' + json["StatusUP"] + '<br>' + json["LastUpdate"] + '</td>';
    content += '<td style="width:10%;"><a href="#" id="pop' + json["idUnitPemantauan"] + '"><i class="fa fa-fw fa-edit"></i>Unit Pemantauan</a></td >';
    content += '<td style="width:10%;"><a href= "#" id= "popIOT' + json["idUnitPemantauan"] + '"><i class="fa fa-fw fa-random"></i>TMAT IOT</a ></td > ';
    content += '</tr>';
    return content;
}

function addRowDataDetailBlock(idx, json) {
    var content = '<tr>';
    content += '<td style="width:2%;">' + idx + '</td>';
    content += '<td style="width:10%;">' + json["Block"] + '</td>';
    content += '<td style="width:5%;"><input type="radio" name="isDefaultDetail" class="minimal" id="isDefaultDetail' + json["Block"] + '"></td>';
    content += '<td style="width:5%;"><input type="checkbox" class="minimal" id="isActiveDetail' + json["Block"] + '"></td>';
    content += '<td style="width:10%;">' + json["IsActive2"] + '</td>';
    content += '<td style="width:5%;"><input type="checkbox" class="minimal" id="lepasDetail' + json["Block"] + '"></td>';
    content += '<td style="width:2%;">' + json["idMappingBlockUnitPemantauan"] + '</td>';
    content += '<td style="width:2%;">' + json["IsActive2"] + '</td>';
    content += '</tr>';
    return content;
}

function preSaveDataUnitPemantauaan() {
    var data = [];
    tableMdlBlock.rows().every(function () {
        var rowData = this.data();
        var block = rowData[1];

        data.push({
            block: block,
            isDefault: $("#isDefaultDetail" + block).is(":checked"),
            isActive: $("#isActiveDetail" + block).is(":checked"),
            isActive2: rowData[7],
            status: rowData[4],
            lepas: $("#lepasDetail" + block).is(":checked"),
            idMappingBlockUnitPemantauan: rowData[6]
        })
    });

    var objSend = {
        userid: $("#userid").val(),
        estCode: $("#selectFilteringEstate").val(),
        idPemantauaan: $("#idPemantauaan").val(),
        block: $("#block").val(),
        alias: $("#alias").val(),
        jenisPiezo: $("#jenisPiezo").val(),
        jenisPiezoPrev: $("#jenisPiezoPrev").val(),
        isActive: $("#isActive").is(":checked"),
        isActivePrev: $("#isActivePrev").val(),
        keterangn: $("#keterangn").val(),
        detailBlockPemantauan: data
    };

    console.info("saveDataUnitPemantauaan", JSON.stringify(objSend));
    saveDataUnitPemantauaan(objSend);

}

function ListEstate() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListAllEstate',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            $('#selectFilteringEstate').find('option').remove();
            json.forEach(function (obj) {
                $("#selectFilteringEstate").append($('<option>', {
                    value: obj.EstCode,
                    text: obj.EstCode + " - " + obj.NewEstName
                }));
            })

            $("#selectFilteringEstate").val("").change();
            $('.selectpicker').selectpicker('refresh');
            ListHistoryDAftarPizometer();
            ListHistorySummaryPizometer();
            getAllIotTMATByEstate();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListEstate : " + xhr.statusText);
        }
    });
}

function ListHistoryDAftarPizometer() {
    $(".loading").show();
    tableMasterPiezo.clear().draw();
    dataPiezo = [];
    $.ajax({
        type: 'POST',
        url: '../Service/WebServiceMasterPiezo.asmx/ListHistoryDAftarPizometer',
        data: '{estCode: "' + $("#selectFilteringEstate").val() + '",type: "' + $("#selectFiltering").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            dataPiezo = $.parseJSON(response.d);
            //console.info("ListHistoryDAftarPizometer", JSON.stringify(json));
            for (var i = 0; i < json.length; i++) {
                tableMasterPiezo.row.add($(addRowDataMasterPiezo(json[i]))[0]).draw();

                $("#pop" + json[i]["idUnitPemantauan"]).click(function () {
                    var obj = {};

                    for (var a = 0; a < dataPiezo.length; a++) {
                        if (dataPiezo[a]["idUnitPemantauan"] == this.id.replace("pop", "")) {
                            obj = dataPiezo[a];
                        }
                    }
                    ListPizometerEditHistory(obj);
                });

                $("#popIOT" + json[i]["idUnitPemantauan"]).click(function () {

                    var id = this.id.replace("popIOT", '');
                    var dataS = dataPiezo.find(item => item["idUnitPemantauan"] === id) || {};
                    $('#mdlCheckDataIOT').modal('show');
                    $('#midPzo').val(dataS["idUnitPemantauan"]);
                    $('#midBlockPemantauan').val(dataS["Mapping"]);
                    $('#midJenis').val(dataS["Jenis"]);
                    $('#midStatus').val(dataS["StatusUP"]);
                    dataMeasureIOT = [];
                    tableIOT.clear().draw();
                    getPiezoMeasure();
                    getPZOIOTStation();
                });
            }
            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListHistoryDAftarPizometer : " + xhr.statusText);
            $(".loading").hide();
        }
    });
}

function ListHistorySummaryPizometer() {
    $("#lblUnitpemantauanKLHLoger").html("0");
    $("#lblKLHlogerUnregistrasi").html("0");
    $("#lblKLHlogerNonAktif").html("0");
    $("#lblKLHlogerAktif").html("0");

    $("#lblKlhManualUnitPemantauan").html("0");
    $("#lblKlhManualUnreg").html("0");
    $("#lblKlhManualNonAktif").html("0");
    $("#lblKlhManualAktif").html("0");

    $("#lblKebunUnitPemantauan").html("0");
    $("#lblKebunUnreg").html("0");
    $("#lblKebunNonAktif").html("0");
    $("#lblKebunAktif").html("0");

    $("#lbltotalUnitpemantauan").html("0");
    $("#lbltotalUnreg").html("0");
    $("#lbltotalNonAktif").html("0");
    $("#lbltotalAktif").html("0");

    $.ajax({
        type: 'POST',
        url: '../Service/WebServiceMasterPiezo.asmx/ListHistorySummaryPizometer',
        data: '{estCode: "' + $("#selectFilteringEstate").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var data = $.parseJSON(response.d);

            let TotalnilaiUnitPemantauan = 0;
            let TotalnilaiAktif = 0;
            let TotalnilaiNonaktif = 0;
            let Totalnilaiunreg = 0;

            for (var i = 0; i < data.length; i++) {
                var Status = data[i]["Jenis"];
                if ("Kebun - Manual" == Status) {
                    $("#lblKebunUnitPemantauan").html(data[i]["ZigmaUP"]);
                    TotalnilaiUnitPemantauan += parseInt(data[i]["ZigmaUP"]);

                    $("#lblKebunAktif").html(data[i]["ZigmaAktif"]);
                    TotalnilaiAktif += parseInt(data[i]["ZigmaAktif"]);

                    $("#lblKebunNonAktif").html(data[i]["ZigmaNonAktif"]);
                    TotalnilaiNonaktif += parseInt(data[i]["ZigmaNonAktif"]);


                    $("#lblKebunUnreg").html(data[i]["ZigmaUnreg"]);
                    Totalnilaiunreg += parseInt(data[i]["ZigmaUnreg"]);
                } else if ("KLH - Logger" == Status) {
                    $("#lblUnitpemantauanKLHLoger").html(data[i]["ZigmaUP"]);
                    TotalnilaiUnitPemantauan += parseInt(data[i]["ZigmaUP"]);

                    $("#lblKLHlogerAktif").html(data[i]["ZigmaAktif"]);
                    TotalnilaiAktif += parseInt(data[i]["ZigmaAktif"]);

                    $("#lblKLHlogerNonAktif").html(data[i]["ZigmaNonAktif"]);
                    TotalnilaiNonaktif += parseInt(data[i]["ZigmaNonAktif"]);

                    $("#lblKLHlogerUnregistrasi").html(data[i]["ZigmaUnreg"]);
                    Totalnilaiunreg += parseInt(data[i]["ZigmaUnreg"]);
                } else if ("KLH - Manual" == Status) {
                    $("#lblKlhManualUnitPemantauan").html(data[i]["ZigmaUP"]);
                    TotalnilaiUnitPemantauan += parseInt(data[i]["ZigmaUP"]);

                    $("#lblKlhManualAktif").html(data[i]["ZigmaAktif"]);
                    TotalnilaiAktif += parseInt(data[i]["ZigmaAktif"]);

                    $("#lblKlhManualNonAktif").html(data[i]["ZigmaNonAktif"]);
                    TotalnilaiNonaktif += parseInt(data[i]["ZigmaNonAktif"]);

                    $("#lblKlhManualUnreg").html(data[i]["ZigmaUnreg"]);
                    Totalnilaiunreg += parseInt(data[i]["ZigmaUnreg"]);
                }
            }

            $("#lbltotalUnitpemantauan").html(TotalnilaiUnitPemantauan);
            $("#lbltotalAktif").html(TotalnilaiAktif);
            $("#lbltotalNonAktif").html(TotalnilaiNonaktif);
            $("#lbltotalUnreg").html(Totalnilaiunreg);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListHistorySummaryPizometer : " + xhr.statusText);
            $(".loading").hide();
        }
    });
}

function ListPizometerEditHistory(obj) {
    $('#mdlCheckData').modal('show');
    $.ajax({
        type: 'POST',
        url: '../Service/WebServiceMasterPiezo.asmx/ListPizometerEditHistory',
        data: '{estCode: "' + $("#selectFilteringEstate").val() + '",idUnitPemantauan: "' + obj["idUnitPemantauan"] + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var data = $.parseJSON(response.d);
            tableMdlBlock.clear().draw();
            tableMdlBlock.column(6).visible(false);
            tableMdlBlock.column(7).visible(false);

            let no = 1;
            var oBlock = {};
            for (var i = 0; i < data.length; i++) {
                var isDefault = false;
                if (data[i]["Block"] == obj["UnitPemantauan"]) {
                    oBlock = data[i];
                    isDefault = true;
                }
                tableMdlBlock.row.add($(addRowDataDetailBlock(no, data[i]))[0]).draw();
                no++;

                $("#isDefaultDetail" + data[i]["Block"] + ",#isActiveDetail" + data[i]["Block"] + ",#lepasDetail" + data[i]["Block"]).iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });

                if (isDefault) {
                    $("#isDefaultDetail" + data[i]["Block"]).iCheck('check');
                }

                if (data[i]["IsActive2"] == "FALSE") {
                    $("#isActiveDetail" + data[i]["Block"]).iCheck('uncheck');
                } else {
                    $("#isActiveDetail" + data[i]["Block"]).iCheck('check');
                }

                $("#lepasDetail" + data[i]["Block"]).iCheck('check');

                if (data[i]["Block"] == obj["UnitPemantauan"]) {
                    $("#isActiveDetail" + data[i]["Block"]).iCheck('disable');
                }
            }

            $("#idPemantauaan").val(obj["idUnitPemantauan"]);
            $("#block").val(obj["UnitPemantauan"]);
            $("#alias").val(oBlock["Alias"]);
            $("#jenisPiezo").val(oBlock["KodeJenisUP"]);
            $("#jenisPiezoPrev").val(oBlock["KodeJenisUP"]);
            $("#isActivePrev").val(oBlock["IsActive"]);
            $("#keterangn").val("");
            if (oBlock["IsActive"] == "FALSE") {
                $("#isActive").iCheck('uncheck');
            } else {
                $("#isActive").iCheck('check')
            }

            $("#jenisPiezoPrev").hide();
            $("#isActivePrev").hide();

            $('.selectpicker').selectpicker('refresh');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function ListHistorySummaryPizometer : " + xhr.statusText);
        }
    });
}

function saveDataUnitPemantauaan(data) {
    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/WebServiceMasterPiezo.asmx/saveDataUnitPemantauaan',
        data: '{data: ' + JSON.stringify(JSON.stringify(data)) + '}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            $(".loading").hide();
            $('#mdlCheckData').modal('hide');
            ListHistoryDAftarPizometer();
            ListHistorySummaryPizometer();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $(".loading").hide();
            console.log("function saveDataUnitPemantauaan : " + xhr.statusText);
        }
    });
}