﻿//Map Basic Variable Declaration
var map, latLong, centerCoordinate, zoomLevel, mapProjection;

//Map Control Declaration
var scaleLineControl, mousePositionControl, overviewMapControl, zoomSliderControl;

//Map Layer Declaration
var layerBase, layerBlock, layerPiezometer, layerInfo, layerDrawing, sourceLayerDrawing, layerMeasure, sourceLayerMeasure;

var listyear, listmonth, listweek;
var year, month, monthname, week, monthnamealias, idweek;

//Variable for Event Handler
var isfullscreen;

//Variable for Toolbox
var draw;
var sketch, helpTooltipElement, helpTooltip, measureTooltipElement, measureTooltip, continuePolygonMsg = 'Click to continue drawing the polygon',
        continueLineMsg = 'Click to continue drawing the line', pointerMoveHandler, formatLength, formatArea, listener;

//Variable For Piezometer
var mapped_estate, estate, globalimgurl;

$(function () {
    //For Event Handler
    isfullscreen = false;

    //Variable Declaration
    mapped_estate = $('#mapped_estate').val();
    estate = $('#estate').val();
    var utmprojection = $('#utmprojection').val();
    var rsid = $('#rsid').val();
    var coordinateExtent = $('#extent').val().split(',');;
    var numzoom = $('#numzoom').val();
    var company = $('#company').val();
    var estatezone = $('#estatezone').val();
    listyear = $('#listyear').val();
    console.log(listyear);
    listmonth = $('#listmonth').val();
    listweek = $('#listweek').val();
    year = $('#year').val();
    month = $('#month').val();
    monthname = $('#monthname').val();
    week = $('#week').val();
    monthnamealias = $('#monthnamealias').val();
    idweek = $('#idweek').val();
    //var listyear = $('#listyear').val();
    //console.log(listyear);
    //var listmonth = $('#listmonth').val();
    //var listweek = $('#listweek').val();
    //var year = $('#year').val();
    //var month = $('#month').val();
    //var monthname = $('#monthname').val();
    //var week = $('#week').val();
    //var monthnamealias = $('#monthnamealias').val();
    //var idweek = $('#idweek').val();
    var currentextent = $('#currentextent').val();

    latLong = [parseFloat(coordinateExtent[0]), parseFloat(coordinateExtent[1]), parseFloat(coordinateExtent[2]), parseFloat(coordinateExtent[3])];
    centerCoordinate = [(latLong[0] + latLong[2]) / 2, (latLong[1] + latLong[3]) / 2];
    zoomLevel = 13;
    mapProjection = 'EPSG:4326';

    console.log(coordinateExtent);
    console.log(latLong);
    console.log(centerCoordinate);
    console.log(zoomLevel);
    console.log(mapProjection);

    ////Map Control Declaration
    //var scaleLineControl, mousePositionControl, overviewMapControl, zoomSliderControl;

    ////Map Layer Declaration
    //var layerBase, layerBlock, layerPiezometer, layerInfo;

    //MAIN FUNCTION
    $('#lbldate').html("Week " + week + ", " + monthnamealias + " " + year);

    //Control Init
    scaleLineControl = new ol.control.ScaleLine({ units: 'metric', className: 'ol-scale-line', target: document.getElementById('scale-line') });
    mousePositionControl = new ol.control.MousePosition({
        coordinateFormat: ol.coordinate.createStringXY(8),
        projection: mapProjection,
        target: document.getElementById('mouse-position'),
        undefinedHTML: '&nbsp;'
    });
    overviewMapControl = new ol.control.OverviewMap({
        className: 'ol-overviewmap ol-custom-overviewmap',
        collapseLabel: '\u00BB',
        label: '\u00AB',
        collapsed: true
    });
    zoomSliderControl = new ol.control.ZoomSlider();

    ////Layer Init
    //layerBase = new ol.layer.Tile({
    //    source: new ol.source.TileWMS({ })
    //});

    layerBlock = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            url: 'http://dev.gis-div.com:8080/geoserver/PZO/wms',
            params: {
                'LAYERS': 'PZO:pzo_block_week',
                'viewparams': 'years:' + year + ';estate:' + mapped_estate + ';months:' + month + ';weeks:' + week,
                'TRANSPARENT': 'true'
            },
        })
    });

    layerPiezometer = new ol.layer.Image({
        source: new ol.source.ImageWMS({
            url: 'http://dev.gis-div.com:8080/geoserver/PZO/wms',
            params: {
                'LAYERS': 'PZO:PZO_Point_Week',
                'viewparams': 'years:' + year + ';estate:' + mapped_estate + ';months:' + month + ';weeks:' + week,
                'TRANSPARENT': 'true'
            }
        })
    });

    layerInfo = new ol.layer.Tile({
        source: new ol.source.TileWMS({
            url: 'http://dev.gis-div.com:8080/geoserver/PZO/wms',
            params: {
                'LAYERS': 'PZO:PZO_Info',
                'viewparams': 'PZOYEAR:' + year + ';ESTNR:' + mapped_estate + ';PZOMONTH:' + month + ';PZOWEEK:' + week,
                'TRANSPARENT': 'true'
            }
        })
    });

    sourceLayerDrawing = new ol.source.Vector({ wrapX: false });

    layerDrawing = new ol.layer.Vector({
        source: sourceLayerDrawing
    });

    sourceLayerMeasure = new ol.source.Vector();

    layerMeasure = new ol.layer.Vector({
        source: sourceLayerMeasure,
        style: new ol.style.Style({
            fill: new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.2)'
            }),
            stroke: new ol.style.Stroke({
                color: '#ffcc33',
                width: 2
            }),
            image: new ol.style.Circle({
                radius: 7,
                fill: new ol.style.Fill({
                    color: '#ffcc33'
                })
            })
        })
    });

    map = new ol.Map({
        controls: ol.control.defaults({
            attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                collapsible: false
            })
        }).extend([
          scaleLineControl, mousePositionControl, zoomSliderControl
        ]),
        target: 'map',
        view: new ol.View({
            extent: latLong,
            projection: mapProjection,
            center: centerCoordinate,
            zoom: zoomLevel
        })
    });

    //map.addLayer(layerBase);
    map.addLayer(layerBlock);
    map.addLayer(layerPiezometer);
    map.addLayer(layerInfo);
    map.addLayer(layerDrawing);
    map.addLayer(layerMeasure);
    //layerBase.setVisible(true);
    layerBlock.setVisible(true);
    layerPiezometer.setVisible(true);
    layerInfo.setVisible(true);

    $('input').on('ifChanged', function (event) {
        if (this.id == "chkLayerBlock") {
            if (this.checked) {
                layerBlock.setVisible(true);
                layerInfo.setVisible(true);
            }
            else {
                layerBlock.setVisible(false);
                layerInfo.setVisible(false);
            }
        }
        else if (this.id == "chkLayerPiezometer") {
            if (this.checked) {
                layerPiezometer.setVisible(true);
            }
            else {
                layerPiezometer.setVisible(false);
            }
        }
    });


    map.on('singleclick', function (evt) {
        var viewResolution = /** @type {number} */(map.getView().getResolution());
        var url = layerPiezometer.getSource().getGetFeatureInfoUrl(evt.coordinate, viewResolution, 'EPSG:4326', { 'INFO_FORMAT': 'application/json' });

        reqwest({
            url: "../Handler/proxy.ashx?url=" + encodeURIComponent(url),
            type: 'json',
        }).then(function (data) {
            if (data.features.length > 0) {
                var feature = data.features[0];
                var props = feature.properties;

                ShowDetailV2(props.PieRecordID);
            }
        });

        var url_block = layerBlock.getSource().getGetFeatureInfoUrl(evt.coordinate, viewResolution, 'EPSG:4326', { 'INFO_FORMAT': 'application/json' });
        reqwest({
            url: "../Handler/proxy.ashx?url=" + encodeURIComponent(url_block),
            type: 'json',
        }).then(function (data) {
            if (data.features.length > 0) {
                var feature = data.features[0];
                var props = feature.properties;

                ShowDetailV2(props.PieRecordID);
            }
        });
    });

    $("#toolsprevdate").click(function () {
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/GetPreviousWeekFromParameter',
            data: '{Week: "' + week + '", Month: "' + month + '", Year: "' + year + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var json = $.parseJSON(response.d);
                if (json.length > 0) {
                    var param = "W" + json[0]["Week"] + ("0" + json[0]["Month"]).slice(-2) + json[0]["Year"];
                    
                    //console.log('years:' + json[0]["Year"] + ';estate:' + mapped_estate + ';months:' + json[0]["Month"] + ';weeks:' + json[0]["Week"]);
                    //layerBlock.getSource().updateParams({
                    //    'LAYERS': 'PZO:pzo_block_week',
                    //    'viewparams': 'years:' + json[0]["Year"] + ';estate:' + mapped_estate + ';months:' + json[0]["Month"] + ';weeks:' + json[0]["Week"],
                    //    'TRANSPARENT': 'true',
                    //    'TILED': true
                    //});
                    window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + company + "&Estate=" + estate + "&IDWEEK=" + param;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.statusText);
            }
        });
    })

    $("#toolsnextdate").click(function () {
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/GetNextWeekFromParameter',
            data: '{Week: "' + week + '", Month: "' + month + '", Year: "' + year + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var json = $.parseJSON(response.d);
                if (json.length > 0) {
                    var param = "W" + json[0]["Week"] + ("0" + json[0]["Month"]).slice(-2) + json[0]["Year"];
                    //layerBlock.getSource().updateParams({
                    //    'LAYERS': 'PZO:pzo_block_week',
                    //    'viewparams': 'years:' + json[0]["Year"] + ';estate:' + mapped_estate + ';months:' + json[0]["Month"] + ';weeks:' + json[0]["Week"],
                    //    'TRANSPARENT': 'true',
                    //    'TILED': true
                    //});

                    window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + company + "&Estate=" + estate + "&IDWEEK=" + param;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.statusText);
            }
        });
    })

    $("#btnGo").click(function () {
        var param = "W" + $("#selectFilterWeek").val() + ("0" + $("#selectFilterMonth").val()).slice(-2) + $("#selectFilterYear").val();
        window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + $("#selectFilterCompany").val() + "&Estate=" + $("#selectFilterEstate").val() + "&IDWEEK=" + param;
    })

    $("#selectFilterCompany").change(function () {
        ListEstate($("#selectFilterCompany").val());
    })

    $("#selectFilterYear").change(function () {
        GetListMonth($("#selectFilterYear").val());
    })

    $("#selectFilterMonth").change(function () {
        GetListWeek($("#selectFilterYear").val(), $("#selectFilterMonth").val());
    })

    $('.selectpicker').selectpicker({});
    ListCompany();
    GetListYear();

    ////Initialize Select2 Elements
    //$('.select2').select2();

    //Date picker
    $('#datepicker').datepicker({
        autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    })

    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
    })

    MapEventHandler();
})

function ListCompany() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListCompany',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataSubText = response.d[2].split(";");
            var dataLength = response.d[0].split(";").length;

            $('#selectFilterCompany').find('option').remove();
            $("#selectFilterCompany").append($('<option>', {
                value: "",
                text: "All Company"
            }));
            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterCompany").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterCompany").val($('#company').val()).change();

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListCompany : " + xhr.statusText);
        }
    });
}

function ListEstate(companycode) {
    console.log(companycode);
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListEstate',
        data: '{CompanyCode: "' + companycode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataLength = response.d[0].split(";").length;

            $('#selectFilterEstate').find('option').remove();
            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterEstate").append($('<option>', {
                    value: dataValue[i],
                    text: dataValue[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterEstate").val($('#estate').val());
            $('#titleEstate').html($('#selectFilterEstate option:selected').text().split(' - ')[1]);
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListEstate : " + xhr.statusText);
        }
    });
}

function GetListYear() {
    var json = $.parseJSON(listyear);
    $('#selectFilterYear').find('option').remove();
    for (var i = 0; i < json.length; i++) {
        $("#selectFilterYear").append($('<option>', {
            value: json[i]["Year"],
            text: json[i]["Year"]
        }));
    }

    $("#selectFilterYear").val(year).change();
}

function GetListMonth(year) {
    var json = $.parseJSON(listmonth);
    $('#selectFilterMonth').find('option').remove();
    for (var i = 0; i < json.length; i++) {
        if (json[i]["Year"] == year) {
            $("#selectFilterMonth").append($('<option>', {
                value: json[i]["Month"],
                text: json[i]["MonthName"]
            }));
        }
    }

    if ($("#selectFilterYear").val() == year)
        $("#selectFilterMonth").val(month).change();
    else
        $("#selectFilterMonth").change();
}

function GetListWeek(year, month) {
    var json = $.parseJSON(listweek);
    $('#selectFilterWeek').find('option').remove();

    for (var i = 0; i < json.length; i++) {
        if (json[i]["Year"] == year && json[i]["Month"] == month) {
            $("#selectFilterWeek").append($('<option>', {
                //value: json[i]["ID"],
                value: json[i]["Week"],
                text: "Week " + json[i]["Week"]
            }));
        }
    }

    if ($("#selectFilterWeek option[value='" + week + "']").length > 0)
        $("#selectFilterWeek").val(week);

    $('.selectpicker').selectpicker('refresh');
}