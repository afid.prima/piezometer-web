﻿function setUpChartTMAT(dataChart) {

    dataPoint = [];
    blackListPiezo = dataChart["piezoBlackList"];
    masterWeekChart = dataChart["mWeek"];
    for (var i = 0; i < 5; i++) {
        setDataPointChart(dataChart["data" + i]);
    }

    listWaterDeep = JSON.parse($("#listWaterDeep").val());
    stripLines = [];
    for (var i = 0; i < listWaterDeep.length; i++) {
        //if (listWaterDeep[i]["MaxValue"] == 998) {
        //    stripLines.push({
        //        value: listWaterDeep[i]["MinValue"],
        //        label: listWaterDeep[i]["IndicatorAliasReport"]
        //    })
        //} else {
            stripLines.push({
                value: listWaterDeep[i]["MinValue"],
                label: listWaterDeep[i]["IndicatorAliasReport"]
            })
        //}
    }


    $("#lblBlackList").html("<u>"+ blackListPiezo.length + " Piezometer Black List </u>");
    console.info("dataPoint", dataPoint);
    var chart = new CanvasJS.Chart("divGraph", {
        animationEnabled: true,
        zoomEnabled: true,
        theme: "light2",
        title: {
            text: $('#selectFilterZonaGrapTmasTmat').val(),
            fontWeight: "bolder",
            fontFamily: "Calibri",
            fontSize: 21,
            padding: 10
        },
        axisX: {
            title: "Week",
            interval: 1,
            labelFormatter: function (e) {
                var weekSHow = "";
                for (var i = 0; i < masterWeekChart.length; i++) {
                    if (masterWeekChart[i]["ID"] == e.value) {
                        weekSHow = masterWeekChart[i]["weekNameX"];
                        break;
                    }
                }
                return weekSHow;
            }
        },
        axisY: {
            title: "Tinggi Muka Air Tanah",
            stripLines: stripLines,
            reversed: true
        },
        data: [{
            color: "Green",
            click: clickItemChartTMAT,
            type: "bubble",
            indexLabel: $("#chkBlack").is(":checked") || $("#chkBlackOnly").is(":checked") ? "{z}" : "{label}/{z}",
            indexLabelFontColor: "Black",
            toolTipContent: $("#chkBlack").is(":checked") || $("#chkBlackOnly").is(":checked") ? "<b>{name}</b><br/>Jumlah Piezometer: {z}" : "<b>{name}</b><br/>Jumlah Piezometer: {z}<br/>Jumlah Piezometer Black List: {label}",
            dataPoints: dataPoint
            //[
            //    { x: 39.6, y: 5.225, z: 1347, name: "China" },
            //    { x: 3.3, y: 4.17, z: 21.49, name: "Australia" },
            //    { x: 1.5, y: 4.043, z: 304.09, name: "US" },
            //    { x: 17.4, y: 2.647, z: 2.64, name: "Brazil" },
            //    { x: 8.6, y: 2.154, z: 141.95, name: "Russia" },
            //    { x: 52.98, y: 1.797, z: 1190.86, name: "India" },
            //    { x: 4.3, y: 1.735, z: 26.16, name: "Saudi Arabia" },
            //    { x: 1.21, y: 1.41, z: 39.71, name: "Argentina" },
            //    { x: 5.7, y: .993, z: 48.79, name: "SA" },
            //    { x: 13.1, y: 1.02, z: 110.42, name: "Mexico" },
            //    { x: 2.4, y: .676, z: 33.31, name: "Canada" },
            //    { x: 2.8, y: .293, z: 64.37, name: "France" },
            //    { x: 3.8, y: .46, z: 127.70, name: "Japan" },
            //    { x: 40.3, y: .52, z: 234.95, name: "Indonesia" },
            //    { x: 42.3, y: .197, z: 68.26, name: "Thailand" },
            //    { x: 31.6, y: .35, z: 78.12, name: "Egypt" },
            //    { x: 1.1, y: .176, z: 61.39, name: "U.K" },
            //    { x: 3.7, y: .144, z: 59.83, name: "Italy" },
            //    { x: 1.8, y: .169, z: 82.11, name: "Germany" }
            //]
        }]
    });
    chart.render();
}

function setDataPointChart(data) {
    data1s10 = [];
    data11s20 = [];
    data21s30 = [];
    data31s40 = [];
    data41s50 = [];
    data51s60 = [];
    data61s70 = [];
    data71 = [];

    blackList1 = [];
    blackList11 = [];
    blackList21 = [];
    blackList31 = [];
    blackList41 = [];
    blackList51 = [];
    blackList61 = [];
    blackList71 = [];
    
    for (var i = 0; i < data.length; i++) {
        var insert = true;
        var blackList = null;
        if ($("#chkBlack").is(":checked")) {
            for (var j = 0; j < blackListPiezo.length; j++) {
                if (data[i]["PieRecordID"] == blackListPiezo[j]["PieRecordID"]) {
                    insert = false;
                    break;
                }
            }
        }
        if ($("#chkBlackOnly").is(":checked")) {
            insert = false;
            for (var j = 0; j < blackListPiezo.length; j++) {
                if (data[i]["PieRecordID"] == blackListPiezo[j]["PieRecordID"]) {
                    insert = true;
                    break;
                }
            }
        }

        for (var j = 0; j < blackListPiezo.length; j++) {
            if (data[i]["PieRecordID"] == blackListPiezo[j]["PieRecordID"]) {
                blackList = data[i];
                break;
            }
        }

        if (insert) {
            if (data[i]["Ketinggian"] >= 1 && data[i]["Ketinggian"] <= 10) {
                if (blackList != null) {
                    blackList1.push(blackList);
                }

                data1s10.push(data[i]);
            } else if (data[i]["Ketinggian"] >= 11 && data[i]["Ketinggian"] <= 20) {
                if (blackList != null) {
                    blackList11.push(blackList);
                }

                data11s20.push(data[i]);
            } else if (data[i]["Ketinggian"] >= 21 && data[i]["Ketinggian"] <= 30) {
                if (blackList != null) {
                    blackList21.push(blackList);
                }

                data21s30.push(data[i]);
            } else if (data[i]["Ketinggian"] >= 31 && data[i]["Ketinggian"] <= 40) {
                if (blackList != null) {
                    blackList31.push(blackList);
                }

                data31s40.push(data[i]);
            } else if (data[i]["Ketinggian"] >= 41 && data[i]["Ketinggian"] <= 50) {
                if (blackList != null) {
                    blackList41.push(blackList);
                }

                data41s50.push(data[i]);
            } else if (data[i]["Ketinggian"] >= 51 && data[i]["Ketinggian"] <= 60) {
                if (blackList != null) {
                    blackList51.push(blackList);
                }

                data51s60.push(data[i]);
            } else if (data[i]["Ketinggian"] >= 61 && data[i]["Ketinggian"] <= 70) {
                if (blackList != null) {
                    blackList61.push(blackList);
                }
                
                data61s70.push(data[i]);
            } else if (data[i]["Ketinggian"] >= 71 && data[i]["Ketinggian"] != 999) {
                if (blackList != null) {
                    blackList71.push(blackList);
                }
                
                data71.push(data[i]);
            }
        }
    }

    if (data1s10.length > 0) {
        dataPoint.push({
            x: data1s10[0]["id"],
            y: 5,
            z: data1s10.length,
            name: "Piezo dengan range 1 - 10",
            obj: data1s10,
            label: blackList1.length
        })
    }
    if (data11s20.length > 0) {
        dataPoint.push({
            x: data11s20[0]["id"],
            y: 15,
            z: data11s20.length,
            name: "Piezo dengan range 11 - 20",
            obj: data11s20,
            label: blackList11.length
        })
    }
    if (data21s30.length > 0) {
        dataPoint.push({
            x: data21s30[0]["id"],
            y: 25,
            z: data21s30.length,
            name: "Piezo dengan range 21 - 30",
            obj: data21s30,
            label: blackList21.length
        })
    }
    if (data31s40.length > 0) {
        dataPoint.push({
            x: data31s40[0]["id"],
            y: 35,
            z: data31s40.length,
            name: "Piezo dengan range 31 - 40",
            obj: data31s40,
            label: blackList31.length
        })
    }
    if (data41s50.length > 0) {
        dataPoint.push({
            x: data41s50[0]["id"],
            y: 45,
            z: data41s50.length,
            name: "Piezo dengan range 41 - 50",
            obj: data41s50,
            label: blackList41.length
        })
    }
    if (data51s60.length > 0) {
        dataPoint.push({
            x: data51s60[0]["id"],
            y: 55,
            z: data51s60.length,
            name: "Piezo dengan range 51 - 60",
            obj: data51s60,
            label: blackList51.length
        })
    }
    if (data61s70.length > 0) {
        dataPoint.push({
            x: data61s70[0]["id"],
            y: 65,
            z: data61s70.length,
            name: "Piezo dengan range 61 - 70",
            obj: data61s70,
            label: blackList61.length
        })
    }
    if (data71.length > 0) {
        dataPoint.push({
            x: data71[0]["id"],
            y: 75,
            z: data71.length,
            name: "Piezo dengan range > 71",
            obj: data71,
            label: blackList71.length
        })
    }
}

function getAllPiezoGraphTmasTmat() {
    var allPiezo = [];

    for (var i = 0; i < dataChartTMAT.data0.length; i++) {
        if (dataChartTMAT.data0[i]["Ketinggian"] != 999) {
            allPiezo.push(dataChartTMAT.data0[i]);
        }
    }
    for (var i = 0; i < dataChartTMAT.data1.length; i++) {
        if (dataChartTMAT.data1[i]["Ketinggian"] != 999) {
            allPiezo.push(dataChartTMAT.data1[i]);
        }
    }
    for (var i = 0; i < dataChartTMAT.data2.length; i++) {
        if (dataChartTMAT.data2[i]["Ketinggian"] != 999) {
            allPiezo.push(dataChartTMAT.data2[i]);
        }
    }
    for (var i = 0; i < dataChartTMAT.data3.length; i++) {
        if (dataChartTMAT.data3[i]["Ketinggian"] != 999) {
            allPiezo.push(dataChartTMAT.data3[i]);
        }
    }
    for (var i = 0; i < dataChartTMAT.data4.length; i++) {
        if (dataChartTMAT.data4[i]["Ketinggian"] != 999) {
            allPiezo.push(dataChartTMAT.data4[i]);
        }
    }

    allPiezo = _.uniq(allPiezo, function (x) {
        return x.PieRecordID;
    });

    return allPiezo;
}

function setupTableSummary(dataChart) {
    var allPiezo = getAllPiezoGraphTmasTmat();
    

    let jmlPzoAll = allPiezo.length;
    let jmlPzoBlack = dataChart.piezoBlackList.length;
    let jmlPzoWhite = jmlPzoAll - jmlPzoBlack;

    let pPzoBlack = jmlPzoBlack / jmlPzoAll * 100;
    let pPzoWhite = jmlPzoWhite / jmlPzoAll * 100;

    $("#lblGraphTmasTmatAllPzo").html("<u>" + jmlPzoAll + "</u> <i class='fa fa-search'></i>");
    $("#lblGraphTmasTmatSelected").html("<u>" + jmlPzoBlack + " (" + Math.ceil(pPzoBlack) +"%)"+ "</u> <i class='fa fa-search'></i>");
    $("#lblGraphTmasTmatUnSelected").html("<u>" + jmlPzoWhite + " (" + Math.ceil(pPzoWhite) + "%)" + "</u> <i class='fa fa-search'></i>");
}

function checkedTMATAll(checkAll) {
    for (var i = 0; i < objDataPointChart.length; i++) {
        if (checkAll) {
            if (objDataPointChart[i]["tipe"].includes("-KLHK")) {
                $("#chkChartTMAT" + objDataPointChart[i]["PieRecordID"]).prop('checked', true);
            } else {
                $("#chkChartTMAT" + objDataPointChart[i]["PieRecordID"]).prop('checked', false);
            }
        } else {
            $("#chkChartTMAT" + objDataPointChart[i]["PieRecordID"]).prop('checked', false);
        }
    }
}

function clickItemChartTMAT(e) {
    var weekShow = "";
    for (var i = 0; i < masterWeekChart.length; i++) {
        if (masterWeekChart[i]["ID"] == e.dataPoint.x) {
            weekShow = masterWeekChart[i]["weekNameX"];
            break;
        }
    }

    objDataPointChart = e.dataPoint.obj;

    console.info("clickItemChartTMAT", e);
    $('#mdlChartTMAT').modal('show');
    $('#idHeadChartTMAT').html(e.dataPoint.name + " " + weekShow)
    tblListPzoChart.clear().draw();
    for (var i = 0; i < e.dataPoint.obj.length; i++) {
        tblListPzoChart.row.add($(addRowDataChartTmat(e.dataPoint.obj[i]))[0]).draw();
        if (e.dataPoint.obj[i]["tipe"].includes("KLHK-")) {
            $("#chkChartTMAT" + e.dataPoint.obj[i]["PieRecordID"]).attr("disabled", true);
        }
    }
}

function addRowDataChartTmat(json) {
    var chk = false;
    for (var j = 0; j < blackListPiezo.length; j++) {
        if (json["PieRecordID"] == blackListPiezo[j]["PieRecordID"]){
            json["UserFullName"] = blackListPiezo[j]["UserFullName"]
            chk = true;
            break;
        }
    }

    var content = '<tr>';
    content += '<td style="width:20%;">' + json["EstNewCode"] + '</td>';
    content += '<td style="width:20%;">' + json["Block"] + '</td>';
    content += '<td style="width:20%;">' + json["PieRecordID"] + '</td>';
    content += '<td style="width:20%;">' + json["tipe"] + '</td>';
    content += '<td style="width:10%; text-align:right;">' + json["elevasiTanah"] + '</td>';
    content += '<td style="width:10%; text-align:right;">' + json["Ketinggian"] + '</td>';
    if (chk) {
        content += '<td style="width:30%; text-align:center;"><input type="checkbox" id="chkChartTMAT' + json["PieRecordID"] + '" checked></td>';
        content += '<td style="width:20%;">' + json["UserFullName"] + '</td>';
    } else {
        content += '<td style="width:30%; text-align:center;"><input type="checkbox" id="chkChartTMAT' + json["PieRecordID"] + '" ></td>';
        content += '<td style="width:20%;"></td>';
    }
    content += '</tr>';
    return content;
}

function saveChartTMAT() {
    if (objDataPointChart.length == 0) {    
        alert("tidak ada muncul");
        return;
    }

    var uncheckPiezo = [];
    var valid = true;
    for (var i = 0; i < objDataPointChart.length; i++) {
        if ($("#chkChartTMAT" + objDataPointChart[i]["PieRecordID"]).is(":checked") && objDataPointChart[i]["tipe"].includes("KLHK-")) {
            valid = false;
        } else if ($("#chkChartTMAT" + objDataPointChart[i]["PieRecordID"]).is(":checked")) {
            uncheckPiezo.push({
                PieRecordID : objDataPointChart[i]["PieRecordID"],
                estCode : objDataPointChart[i]["PieRecordID"].split('-')[0],
                block: objDataPointChart[i]["Block"],
                check : "true"
            });
        } else {
            uncheckPiezo.push({
                PieRecordID: objDataPointChart[i]["PieRecordID"],
                estCode: objDataPointChart[i]["PieRecordID"].split('-')[0],
                block: objDataPointChart[i]["Block"],
                check: "false"
            });
        }
    }

    if (!valid) {
        alert("Untuk KLHK tidak bisa di pilih sebagai black list");
        return;
    }

    if (uncheckPiezo.length == 0) {
        alert("tidak ada piezo yang di pilih");
        return;
    }

    var sendData = {
        zona: objDataPointChart[0]["WMA_code"],
        userId: $("#userid").val(),
        listPiezo: uncheckPiezo
    }

    console.info("sendData", sendData);
    insertChartTMATPiezo({
        data: JSON.stringify(sendData)
    });
}

function showBlackListForm() {
    if (blackListPiezo == undefined) {
        return;
    }

    if (masterWeekChart == undefined) {
        return;
    }

    $("#mdlChartTMATBlackList").modal('show');
    $("#idHeadChartTMATBlackList").html("List Piezometer Selected " + blackListPiezo[0]["WMA_code"]);

    for (var i = 0; i < masterWeekChart.length; i++) {
        if (masterWeekChart[i]["weekNo"] == 0) {
            $("#week1").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 1) {
            $("#week2").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 2) {
            $("#week3").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 3) {
            $("#week4").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 4) {
            $("#week5").html(masterWeekChart[i]["weekNameX"])
        }
    }

    jArrayBlackList = [];
    for (var i = 0; i < blackListPiezo.length; i++) {
        let w1 = getKetinggianBlackList(blackListPiezo[i]["PieRecordID"], dataChartTMAT["data0"]);
        let w2 = getKetinggianBlackList(blackListPiezo[i]["PieRecordID"], dataChartTMAT["data1"]);
        let w3 = getKetinggianBlackList(blackListPiezo[i]["PieRecordID"], dataChartTMAT["data2"]);
        let w4 = getKetinggianBlackList(blackListPiezo[i]["PieRecordID"], dataChartTMAT["data3"]);
        let w5 = getKetinggianBlackList(blackListPiezo[i]["PieRecordID"], dataChartTMAT["data4"]);

        jArrayBlackList.push({
            WMA_code: blackListPiezo[0]["WMA_code"],
            EstNewCode: blackListPiezo[i]["EstNewCode"],
            block: blackListPiezo[i]["block"],
            PieRecordID: blackListPiezo[i]["PieRecordID"],
            elevasiTanah: blackListPiezo[i]["elevasiTanah"],
            tipe: blackListPiezo[i]["tipe"],
            longitude: blackListPiezo[i]["Longitude"],
            latitude: blackListPiezo[i]["Latitude"],
            UserFullName: blackListPiezo[i]["UserFullName"] != null ? blackListPiezo[i]["UserFullName"] : "",
            check: true,
            w1: w1,
            w2: w2,
            w3: w3,
            w4: w4,
            w5: w5
        })
    }

    tblListPzoChartBlackList.clear().draw();
    for (var i = 0; i < jArrayBlackList.length; i++) {
        tblListPzoChartBlackList.row.add($(addRowDataChartBlockList(jArrayBlackList[i]))[0]).draw();
        if (jArrayBlackList[i]["tipe"].includes("KLHK-")) {
            $("#chkChartTMATBlack" + jArrayBlackList[i]["PieRecordID"]).attr("disabled", true);
        }
    }
}

function showAlListForm() {
    if (blackListPiezo == undefined) {
        return;
    }

    if (masterWeekChart == undefined) {
        return;
    }

    $("#mdlChartTMATBlackList").modal('show');
	if(blackListPiezo.length > 0 ){
		$("#idHeadChartTMATBlackList").html("List Piezometer " + blackListPiezo[0]["WMA_code"]);
	}else{
		$("#idHeadChartTMATBlackList").html("List Piezometer ");
	}

    for (var i = 0; i < masterWeekChart.length; i++) {
        if (masterWeekChart[i]["weekNo"] == 0) {
            $("#week1").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 1) {
            $("#week2").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 2) {
            $("#week3").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 3) {
            $("#week4").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 4) {
            $("#week5").html(masterWeekChart[i]["weekNameX"])
        }
    }
    var allPiezo = getAllPiezoGraphTmasTmat();
    
    jArrayBlackList = [];
    for (var i = 0; i < allPiezo.length; i++) {
        let w1 = getKetinggianBlackList(allPiezo[i]["PieRecordID"], dataChartTMAT["data0"]);
        let w2 = getKetinggianBlackList(allPiezo[i]["PieRecordID"], dataChartTMAT["data1"]);
        let w3 = getKetinggianBlackList(allPiezo[i]["PieRecordID"], dataChartTMAT["data2"]);
        let w4 = getKetinggianBlackList(allPiezo[i]["PieRecordID"], dataChartTMAT["data3"]);
        let w5 = getKetinggianBlackList(allPiezo[i]["PieRecordID"], dataChartTMAT["data4"]);

        var check = false;
        for (var j = 0; j < blackListPiezo.length; j++) {
            if (allPiezo[i]["PieRecordID"] == blackListPiezo[j]["PieRecordID"]) {
                allPiezo[i]["UserFullName"] = blackListPiezo[j]["UserFullName"];
                check = true;
                break;
            }
        }

        jArrayBlackList.push({
            WMA_code: allPiezo[0]["WMA_code"],
            EstNewCode: allPiezo[i]["EstNewCode"],
            block: allPiezo[i]["Block"],
            PieRecordID: allPiezo[i]["PieRecordID"],
            elevasiTanah: allPiezo[i]["elevasiTanah"],
            tipe: allPiezo[i]["tipe"],
            longitude: allPiezo[i]["Longitude"],
            latitude: allPiezo[i]["Latitude"],
            UserFullName: allPiezo[i]["UserFullName"] != undefined ? allPiezo[i]["UserFullName"] : "",
            check: check,
            w1: w1,
            w2: w2,
            w3: w3,
            w4: w4,
            w5: w5
        })
    }

    tblListPzoChartBlackList.clear().draw();
    for (var i = 0; i < jArrayBlackList.length; i++) {
        tblListPzoChartBlackList.row.add($(addRowDataChartBlockList(jArrayBlackList[i]))[0]).draw();
        if (jArrayBlackList[i]["tipe"].includes("KLHK-")) {
            $("#chkChartTMATBlack" + jArrayBlackList[i]["PieRecordID"]).attr("disabled", true);
        }
    }
}

function showWhiteListForm() {
    if (blackListPiezo == undefined) {
        return;
    }

    if (masterWeekChart == undefined) {
        return;
    }

    $("#mdlChartTMATBlackList").modal('show');
    $("#idHeadChartTMATBlackList").html("List Piezometer Un Selected " + blackListPiezo[0]["WMA_code"]);

    for (var i = 0; i < masterWeekChart.length; i++) {
        if (masterWeekChart[i]["weekNo"] == 0) {
            $("#week1").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 1) {
            $("#week2").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 2) {
            $("#week3").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 3) {
            $("#week4").html(masterWeekChart[i]["weekNameX"])
        } else if (masterWeekChart[i]["weekNo"] == 4) {
            $("#week5").html(masterWeekChart[i]["weekNameX"])
        }
    }
    var allPiezo = getAllPiezoGraphTmasTmat();
    
    jArrayBlackList = [];
    for (var i = 0; i < allPiezo.length; i++) {
        let w1 = getKetinggianBlackList(allPiezo[i]["PieRecordID"], dataChartTMAT["data0"]);
        let w2 = getKetinggianBlackList(allPiezo[i]["PieRecordID"], dataChartTMAT["data1"]);
        let w3 = getKetinggianBlackList(allPiezo[i]["PieRecordID"], dataChartTMAT["data2"]);
        let w4 = getKetinggianBlackList(allPiezo[i]["PieRecordID"], dataChartTMAT["data3"]);
        let w5 = getKetinggianBlackList(allPiezo[i]["PieRecordID"], dataChartTMAT["data4"]);

        var check = false;
        for (var j = 0; j < blackListPiezo.length; j++) {
            if (allPiezo[i]["PieRecordID"] == blackListPiezo[j]["PieRecordID"]) {
                check = true;
                break;
            }
        }

        if (!check) {
            jArrayBlackList.push({
                WMA_code: allPiezo[0]["WMA_code"],
                EstNewCode: allPiezo[i]["EstNewCode"],
                block: allPiezo[i]["Block"],
                PieRecordID: allPiezo[i]["PieRecordID"],
                elevasiTanah: allPiezo[i]["elevasiTanah"],
                tipe: allPiezo[i]["tipe"],
                longitude: allPiezo[i]["Longitude"],
                latitude: allPiezo[i]["Latitude"],
                UserFullName: "",
                check: check,
                w1: w1,
                w2: w2,
                w3: w3,
                w4: w4,
                w5: w5
            })
        }
    }

    tblListPzoChartBlackList.clear().draw();
    for (var i = 0; i < jArrayBlackList.length; i++) {
        tblListPzoChartBlackList.row.add($(addRowDataChartBlockList(jArrayBlackList[i]))[0]).draw();
        if (jArrayBlackList[i]["tipe"].includes("KLHK-")) {
            $("#chkChartTMATBlack" + jArrayBlackList[i]["PieRecordID"]).attr("disabled", true);
        }
    }
}

function getKetinggianBlackList(PieRecordID,data) {
    let value = 0;
    for (var j = 0; j < data.length; j++) {
        var d0 = data[j];
        if (PieRecordID == d0["PieRecordID"]) {
            value = d0["Ketinggian"];
            break;
        }
    }
    return value;
}

function addRowDataChartBlockList(json) {

    var content = '<tr>';
    content += '<td style="width:20%;">' + json["EstNewCode"] + '</td>';
    content += '<td style="width:20%;">' + json["block"] + '</td>';
    content += '<td style="width:20%;">' + json["PieRecordID"] + '</td>';
    content += '<td style="width:20%;">' + json["tipe"] + '</td>';
    content += '<td style="width:20%; text-align:right;">' + json["longitude"] + '</td>';
    content += '<td style="width:20%; text-align:right;">' + json["latitude"] + '</td>';
    content += '<td style="width:10%; text-align:right;">' + json["elevasiTanah"] + '</td>';
    content += '<td style="width:10%; text-align:right;">' + json["w1"] + '</td>';
    content += '<td style="width:10%; text-align:right;">' + json["w2"] + '</td>';
    content += '<td style="width:10%; text-align:right;">' + json["w3"] + '</td>';
    content += '<td style="width:10%; text-align:right;">' + json["w4"] + '</td>';
    content += '<td style="width:10%; text-align:right;">' + json["w5"] + '</td>';
    if (json["check"] == true) {
        content += '<td style="width:30%; text-align:center;"><input type="checkbox" id="chkChartTMATBlack' + json["PieRecordID"] + '" checked></td>';
    } else {
        content += '<td style="width:30%; text-align:center;"><input type="checkbox" id="chkChartTMATBlack' + json["PieRecordID"] + '"></td>';
    }
    content += '<td style="width:10%;">' + json["UserFullName"] + '</td>';
    content += '</tr>';
    return content;
}

function saveChartTMATBlackList() {
    if (jArrayBlackList.length == 0) {
        alert("tidak ada muncul");
        return;
    }

    var uncheckPiezo = [];
    var valid = true;
    for (var i = 0; i < jArrayBlackList.length; i++) {
        if ($("#chkChartTMATBlack" + jArrayBlackList[i]["PieRecordID"]).is(":checked") && jArrayBlackList[i]["tipe"].includes("KLHK-")) {
            valid = false;
        } else if ($("#chkChartTMATBlack" + jArrayBlackList[i]["PieRecordID"]).is(":checked")) {
            uncheckPiezo.push({
                PieRecordID: jArrayBlackList[i]["PieRecordID"],
                estCode: jArrayBlackList[i]["PieRecordID"].split('-')[0],
                block: jArrayBlackList[i]["block"],
                check: "true"
            });
        } else {
            uncheckPiezo.push({
                PieRecordID: jArrayBlackList[i]["PieRecordID"],
                estCode: jArrayBlackList[i]["PieRecordID"].split('-')[0],
                block: jArrayBlackList[i]["block"],
                check: "false"
            });
        }
    }

    if (!valid) {
        alert("Untuk KLHK tidak bisa di pilih sebagai black list");
        return;
    }

    if (uncheckPiezo.length == 0) {
        alert("tidak ada piezo yang di pilih");
        return;
    }

    var sendData = {
        zona: jArrayBlackList[0]["WMA_code"],
        userId: $("#userid").val(),
        listPiezo: uncheckPiezo
    }

    console.info("sendData", sendData);
    insertChartTMATPiezo({
        data: JSON.stringify(sendData)
    });
}

function dateFormat(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    var yyyy = date.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }
    return date = dd + '/' + mm + '/' + yyyy;
}

function getLabelChartTMAS() {
    let startDate = 0;
    let endDate = 0;

    for (var i = 0; i < masterWeekChart.length; i++) {
        let start = parseInt(masterWeekChart[i]["StartDate"].replace('/Date(', '').replace(')/', ''));
        let end = parseInt(masterWeekChart[i]["EndDate"].replace('/Date(', '').replace(')/', ''));
        if (startDate == 0) {
            startDate = start;
        } else if (startDate > start) {
            startDate = start;
        }

        if (endDate == 0) {
            endDate = end;
        } else if (endDate < end) {
            endDate = end;
        }
    }

    var difference = Math.floor((endDate - startDate) / (1000 * 60 * 60 * 24));
    labelChartTMAS = [];
    for (var i = difference; i >= 0; i--) {
        var d = new Date(endDate);
        d.setDate(d.getDate() - i);
        labelChartTMAS.push(dateFormat(d));
    }

    console.info("labelChartTMAS", labelChartTMAS)
}

function addDataSetTMAS(idx) {
    var charRambu = [];
    var rambuM = selectedRambuMeasure[idx];

    if (idx == 0) {
        elvTanah = parseFloat(rambuM["elevasiTanah"]);
    }
    for (var i = 0; i < labelChartTMAS.length; i++) {
        var nilai = null;
        var measure = JSON.parse(rambuM["measure"]);
        for (var j = 0; j < measure.length; j++) {
            if (labelChartTMAS[i] == dateFormat(new Date(measure[j]["measurementDate"]))) {
                nilai = parseFloat(measure[j]["Nilai"]);
                break;
            }
        }


        if (nilai != null) {
            if (nilai > maxY) {
                maxY = nilai;
            }

            if (minY == null) {
                minY = nilai;
            } else if (nilai < minY) {
                minY = nilai;
            }
        }
        var sdate = labelChartTMAS[i].split('/');
        charRambu.push({
            x: new Date(sdate[2], parseInt(sdate[1]) - 1, sdate[0]),
            y: nilai
        });
    }
    //console.info(' ini chart'+ charRambu);
    var markerSymbol = "circle";
    if (rambuM["frequency"] == 2) {
        markerSymbol = "square";
    } else if (rambuM["frequency"] == 3) {
        markerSymbol = "triangle";
    }

    var newSeries = {
        name: rambuM["stationId"],
        legendText: rambuM["stationName"],
        color: rambuM["color"],
        markerColor: rambuM["color"],
        markerType: markerSymbol,
        connectNullData: true,
        nullDataLineDashType: "solid",
        type: "line",
        click: function (e) {
            var measurementId = 0;
            for (var i = 0; i < selectedRambuMeasure.length; i++) {
                if (selectedRambuMeasure[i]["stationId"] == e.dataSeries.name) {
                    var measure = JSON.parse(selectedRambuMeasure[i]["measure"]);

                    for (var j = 0; j < measure.length; j++) {
                        if (dateFormat(new Date(measure[j]["measurementDate"])) == dateFormat(e.dataPoint.x)) {
                            measurementId = measure[j]["measurementId"];
                            break;
                        }
                    }
                }
            }
            editMeasure(measurementId);
            //alert(e.dataSeries.name + ", "+measurementId);
            //alert(e.dataSeries.name + ", dataPoint { x:" + e.dataPoint.label + ", y: " + e.dataPoint.y + " }");
        },
        markerSize: 8,
        showInLegend: "true",
        toolTipContent: "{legendText}<br/>{x}, <strong>{y}</strong> m",
        dataPoints: charRambu
    };


    //console.info("charRambu", newSeries);

    chartData.push(newSeries);

    idx++;
    if (idx < selectedRambuMeasure.length) {
        addDataSetTMAS(idx)
    } else {
        if (selectedCurahHujan != "") {
            getValueCurahHujan();
        } else {
            setupChartTMAS();
        }
    }
}

function addDataCurahHujanTMAS(rata2CurahHujan) {
    var charRataCH = [];
    nilaiMaxCH = 3;
    for (var i = 0; i < labelChartTMAS.length; i++) {
        var nilai = null;
        for (var j = 0; j < rata2CurahHujan.length; j++) {
            if (labelChartTMAS[i] == dateFormat(new Date(parseInt(rata2CurahHujan[j]["date"].replace('/Date(', '').replace(')/', ''))))) {
                nilai = Math.round(rata2CurahHujan[j]["sumRainInches"]);
                break;
            }
        }

        if (nilai != null) {
            if (nilaiMaxCH < nilai) {
                nilaiMaxCH = nilai;
            }
        }
        var sdate = labelChartTMAS[i].split('/');
        charRataCH.push({
            x: new Date(sdate[2], parseInt(sdate[1]) - 1, sdate[0]),
            y: nilai
        });
    }
    console.info("charRataCH", charRataCH);
    chartCH = {};
    chartCH["name"] = "Rata Rata Curah Hujan";
    chartCH["type"] = "column";
    chartCH["color"] = "orange";
    chartCH["showInLegend"] = "true";
    chartCH["toolTipContent"] = "{name}<br/>{x}, <strong>{y}</strong> mm";
    chartCH["axisYType"] = "secondary";
    chartCH["dataPoints"] = charRataCH;
    chartCH["fillOpacity"] = .6;
    setupChartTMAS();
}

function setupChartTMAS() {
    console.info("setupChartTMAS");
    getLabelChartTMAS();

    var dataChart = [];

    var dataChartNote = [];

    if (chartCH != undefined) {
        dataChart.push(chartCH);
    }

    for (var i = 0; i < chartData.length; i++) {
        dataChart.push(chartData[i]);
    }
    var titikY = {
        title: "Tinggi Muka Air Saluran",
        titleFontSize: 15,
        labelFontSize: 13
    }


    if (elvTanah != null) {
        var elvTanah2 = elvTanah - 0.2;
        var elvTanah4 = elvTanah - 0.4;
        var elvTanah6 = elvTanah - 0.6;
        var stripLine = [{
            value: elvTanah,
            label: "0"
        }, {
            value: elvTanah2,
            label: "0.2"
        }, {
            value: elvTanah4,
            label: "0.4"
        }, {
            value: elvTanah6,
            label: "0.6"
        }];

        console.log('hasil penjumlahan = ' + (parseFloat(tmasTarget) + (parseFloat(0.05))).toFixed(2));
        if (tmasTarget != "") {
            var objDefault = {
                label: "TMAS Target",
                startValue: parseFloat(tmasTarget - 0.05),
                endValue: parseFloat(tmasTarget + 0.05),
                color: "#b3f2c2",
                labelAlign: "near"
            }
        }

        stripLine.push(objDefault)
        console.log('object fix stripline = ' + JSON.stringify(stripLine));
        titikY["stripLines"] = stripLine;
    }
    titikY["includeZero"] = false;
    console.info("titikY", JSON.stringify(titikY));

    chart = new CanvasJS.Chart("divGraph2", {
        title: {
            text: textTitle,
            fontWeight: "bolder",
            fontFamily: "Calibri",
            fontSize: 21,
            padding: 10
        },
        zoomEnabled: true,
        panEnabled: true,
        axisXType: "secondary",
        animationEnabled: true,
        axisY: titikY,
        axisY2: {
            maximum: ((nilaiMaxCH / 8) * 2) + nilaiMaxCH,
            title: "Curah Hujan",
            titleFontSize: 15,
            labelFontSize: 13,
        },
        axisX: {
            valueFormatString: "DD-MMM",
            title: "Tanggal",
            titleFontSize: 15,
            labelFontSize: 13
        },
        legend: {
            fontSize: 11,
            verticalAlign: "bottom",
            horizontalAlign: "center"
        },
        data: dataChart
    });

    chart.render();

}

function populateEstateByPulauGraph() {
    arrEstateDropdownGraph = [];
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/getEstateByWMArea',
        data: '{wmArea: "' + $("#selectFilterEstatePulauGrapTmasTmat").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = JSON.parse(response.d);
            $("#selectFilterEstateGrapTmasTmat").find('option').remove();

            if (json.length > 0) {
                for (var i = 0; i < json.length; i++) {
                    $("#selectFilterEstateGrapTmasTmat").append($('<option>', {
                        value: json[i]["estCode"],
                        text: json[i]["EstName"]
                    }));

                    arrEstateDropdownGraph.push(json[i]["estCode"]);
                }
            }


            $("#selectFilterEstateGrapTmasTmat").val(arrEstateDropdownGraph);

            $('.selectpicker').selectpicker('refresh');
            getZonaByEstateGraph();
            $('.loading').hide()
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });
}

//dicki function 


function getZonaByEstateGraph() {
    console.log('get zona by estate');
    $('#selectFilterZonaGrapTmasTmat').find('option').remove();
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/getZonaByEstate',
        data: '{estCode: "' + $("#selectFilterEstateGrapTmasTmat").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            //console.log('data zonanya=' + JSON.stringify(response))
            var json = JSON.parse(response.d);
            if (json.length > 0) {
                for (var i = 0; i < json.length; i++) {
                    $("#selectFilterZonaGrapTmasTmat").append($('<option>', {
                        value: json[i]["WMA_CODE"],
                        text: json[i]["NamaZona"]
                    }));

                }
                $('.selectpicker').selectpicker('refresh');


            } else {
                $("#selectFilterZonaGrapTmasTmat").find('option').remove();
                $('.selectpicker').selectpicker('refresh');
            }
            $('.loading').hide()
        },
        error: function (xhr, ajaxOptions, thrownError) {
        }
    });

}



function getPrevNextWeekNameGraph(param) {
    $('.loading').show()
    var value = $("#txtWeekIdGrapTmasTmat").val()
    if (param == 1) {
        value = parseInt(value) + 1
    } else if (param == 0) {
        if (value - 1 != 0) {
            value = value - 1
        }
    }
    $("#txtWeekIdGrapTmasTmat").val(value);
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/getNextPrevWeekNameForPencatatanPerubahan',
        data: '{weekId: "' + $("#txtWeekIdGrapTmasTmat").val() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = JSON.parse(response.d);
            if (json.length > 0) {
                $("#lblTitleWeekGrapTmasTmat").html(json[0]["MonthName"])
                $("#txtWeekIdGrapTmasTmat").val(json[0]["ID"])
            }
            $('.loading').hide()
        },
        error: function (xhr, ajaxOptions, thrownError) {

            $('.loading').hide()
        }
    });

}

function getChartTMAT() {
    $('.loading').show();
    let idWeekBefor = $("#txtWeekIdGrapTmasTmat").val() - 4;
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/getChartTMAT',
        data: '{idWeek: "' + $("#txtWeekIdGrapTmasTmat").val() + '",idWeekBefor: "' + idWeekBefor + '",wmcode:"' + $('#selectFilterZonaGrapTmasTmat').val()+'"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var jsonPiezoBlackList = JSON.parse(response.d[0]);
            var jsonWeek = JSON.parse(response.d[1]);
            var jsonData0 = JSON.parse(response.d[2]);
            var jsonData1 = JSON.parse(response.d[3]);
            var jsonData2 = JSON.parse(response.d[4]);
            var jsonData3 = JSON.parse(response.d[5]);
            var jsonData4 = JSON.parse(response.d[6]);

            dataChartTMAT = {
                piezoBlackList: jsonPiezoBlackList,
                mWeek: jsonWeek,
                data0: jsonData0,
                data1: jsonData1,
                data2: jsonData2,
                data3: jsonData3,
                data4: jsonData4
            }
            setUpChartTMAT(dataChartTMAT);
            setupTableSummary(dataChartTMAT);

            getLabelChartTMAS();
            getChartFromZonaTMAS();

            $('.loading').hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('.loading').hide();
        }
    });

}

function getChartFromZonaTMAS() {
    $.ajax({
        type: 'POST',
        url: window.location.origin +'/wlr/Service/WebService.asmx/getChartFromZona',
        data: '{zonaId: "' + $('#selectFilterZonaGrapTmasTmat').val().toUpperCase() + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            dataChartZona = $.parseJSON(response.d);
            selectedRambu = [];
            var allCH = [];
            for (var i = 0; i < dataChartZona.length; i++) {
                if (dataChartZona[i]["stationId"] != "") {
                    selectedRambu.push({
                        stationId: dataChartZona[i]["stationId"],
                        stationName: dataChartZona[i]["stationName"],
                        sortField: dataChartZona[i]["sortField"],
                        color: dataChartZona[i]["colorField"]
                    })
                }

                if (dataChartZona[i]["rambuCurahHujan"].trim().split(",").length > 0) {
                    var rsplitCH = dataChartZona[i]["rambuCurahHujan"].trim().split(",");
                    for (var j = 0; j < rsplitCH.length; j++) {
                        allCH.push(rsplitCH[j].trim())
                    }
                }

                tmasTarget = dataChartZona[i]["TmasTarget"]
                textTitle = dataChartZona[i]["namaGraph"]
            }

            allCH = _.uniq(allCH, false);
            selectedCurahHujan = "";
            for (var i = 0; i < allCH.length; i++) {
                if (selectedCurahHujan == "") {
                    selectedCurahHujan = allCH[i];
                } else {
                    selectedCurahHujan += "," + allCH[i];
                }

            }

            if (selectedRambu.length > 0) {
                GetStationMeasureArray(selectedRambu);
            } else if (selectedCurahHujan != "") {
                getValueCurahHujan();
            } else {
                setupChartTMAS();
            }
        },

        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getChartFromZona : " + xhr.statusText);
        }
    });
}
function GetStationMeasureArray() {
    $.ajax({
        type: 'POST',
        url: window.location.origin +'/wlr/Service/WebService.asmx/GetStationMeasureArray',
        data: JSON.stringify({ stationSelected: JSON.stringify(selectedRambu) }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            selectedRambuMeasure = [];
            selectedRambuMeasure = JSON.parse(response.d);
            console.info("object Measure", selectedRambuMeasure);
            //selectedRambuMeasure.sort(predicateBy("sortField"));
            chartData = [];
            minY = null;
            maxY = null;
            if (selectedRambuMeasure.length > 0) {
                addDataSetTMAS(0);
            } else {
                if (selectedCurahHujan != "") {
                    getValueCurahHujan();
                } else {
                    setupChartTMAS();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getMasterZona : " + xhr.statusText);
        }
    });
}

function getValueCurahHujan() {
    $.ajax({
        type: 'POST',
        url: window.location.origin +'/wlr/Service/WebService.asmx/getValueCurahHujan',
        data: '{stationSelected: "' + selectedCurahHujan + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            addDataCurahHujanTMAS(JSON.parse(response.d));
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getValueCurahHujan : " + xhr.statusText);
        }
    });
}

function insertChartTMATPiezo(sendData) {
    $('.loading').show();
    $.ajax({
        type: 'POST',
        url: '../Service/MapServiceTambahan.asmx/insertChartTMATPiezo',
        data: JSON.stringify(sendData),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            $('#mdlChartTMAT').modal('hide');
            $("#mdlChartTMATBlackList").modal('hide');
            $('.loading').hide();
            setTimeout(alert("Berhasil Simpan Back List Piezometer"), 2000)

            getChartTMAT();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function insertChartTMATPiezo : " + xhr.statusText);
            $('.loading').hide();
        }
    });
}

//update dicki 
