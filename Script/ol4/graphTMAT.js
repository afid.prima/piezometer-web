﻿var labelChart;
var DATA_PIZOMETER_VALUE;
var minY, maxY, reChart, chart, chartData = [], chartCH = [];
var LIST_DETAIL_PIZOMETER, LIST_DETAIL_CURAH_HUJAN = [];
var DATA_SELECTED_VALUE_PIZOMETER = [];
var DATA_SELECTED_CURAH_HUJAN = [];
var DATA_CURAH_HUJAN_VALUE = [];
var INDICATOR_COLOR = [];
var SET_GRAPH_COLOR = [];
var LIST_PIZOMETER_STATIC_COLOR = [];
var currentPzoLength, currentCurahHujanLength = 0;
var filterDataPer = '1';
var selected_piezometer_estate = [];


$(function () {
	$('#selectFilterPer').selectpicker('val', '1');


	$('#selectAllFilterPizometer_J').on('change', function () {
	
		if ($(this).is(':checked')) {
			$(this).attr('value', 'true');
			selectAll();
			if ($('#selectFilterPizometer_J').val().length > 15) {
				$("#alertSelectFilterPizometer_J").text("Proses menyimpan data memakan waktu beberapa menit");
			} else {
				$("#alertSelectFilterPizometer_J").text("");
            }
			
			showMessage('success', 'Select All Piezometer');
		} else {
			$(this).attr('value', 'false');
			DATA_SELECTED_VALUE_PIZOMETER = [];
			LIST_PIZOMETER_STATIC_COLOR = [];
			$("#selectFilterPizometer_J").select2().val("").trigger("change");
			setupChart();
			resetChart();
			$("#alertSelectFilterPizometer_J").text("");
			showMessage('success', 'Deselect All Piezometer');
		}

    })

	$("#selectFilterPer").on('change', function () {
		filterDataPer = $("#selectFilterPer").val();

		console.log({ filterDataPer})

		getValuePizometerFilter();


		if ($('#selectFilterCurahHujan_j').val().length > 0 && $('#selectFilterPizometer_J').val().length > 0) {
			getValueCurahHujanFilter();
		}


		showMessage('success', 'Preview graph changes');
	});
	
	$("#tglSelectedGraph").on("change", function () {


		getValuePizometerFilter();


		if ($('#selectFilterCurahHujan_j').val().length > 0 && $('#selectFilterPizometer_J').val().length > 0) {
			getValueCurahHujanFilter();
		}

		showMessage('success', 'Preview graph changes by date');
		
	});
	
	$('#selectFilterCurahHujan_j').change(function (e) {
	
		if ($('#selectFilterPizometer_J').val().length < 1) {
			$('#selectFilterCurahHujan_j').val("");

			$("#selectGraphDetailIotError").text("Pizometer wajib di isi");
			return;
		} else {
			$("#selectGraphDetailIotError").text("");
		}


		if ($('#selectFilterCurahHujan_j').val().length > 0) {
			getValueCurahHujanFilter();
			if (currentCurahHujanLength > 0) {
				if (currentCurahHujanLength < $('#selectFilterCurahHujan_j').val().length) {
					showMessage('success', 'added graph rainfall line successfully');

				} else {
					showMessage('success', 'removed graph rainfall line successfully');

				}
			}

			//console.log({ currentCurahHujanLength })

			currentCurahHujanLength = $('#selectFilterCurahHujan_j').val().length;
		} else {
			currentCurahHujanLength = 0
		}
	});

	$('#selectFilterWmArea_j').change(function () {
		$('.loading').show();

		getZona();
		getCurahHujanGraph();
		showGraphTmat();
		resetFormGraphTmat("1");
		resetChart();
		
	
	});
	
	$("#selectFilterEstate_j").change(function () {
		$('.loading').show();

		$('.selectpicker').selectpicker('refresh');


		$("#selectFilterPizometer_J").find('option').remove();
		$('#selectFilterPizometer_J').selectpicker("refresh");

		$("#selectFilterCurahHujan_j").select2().val("").trigger("change");

		DATA_SELECTED_CURAH_HUJAN = [];
		DATA_SELECTED_VALUE_PIZOMETER = [];
		LIST_PIZOMETER_STATIC_COLOR = [];

		if ($("#selectFilterPizometer_J").val() == "" && $("#selectFilterPizometer_J").val() == "") {

			resetChart();
			setupChart();
		}

		selected_piezometer_estate = [];

		getPiezometerGraph();

	});

	
	$("#selectFilterPizometer_J").change(function () {

		if ($("#selectFilterZona_j").val() == "") {
			$("#selectFilterPizometer_J").val("");
			showMessage("danger","Please select zona first")
			return;
        }

		//console.log({ LIST_PIZOMETER_STATIC_COLOR, cek: $("#selectFilterPizometer_J").val() });
		var arrayIot = [];
		$("#selectFilterPizometer_J option:selected").each(function () {
			var $this = $(this);
			var ada = false;
			for (var j = 0; j < LIST_PIZOMETER_STATIC_COLOR.length; j++) {
				if (LIST_PIZOMETER_STATIC_COLOR[j]["PieRecordID"] == $this.val()) {
					arrayIot.push(LIST_PIZOMETER_STATIC_COLOR[j]);
					ada = true;
					break;
				}
			}

			if (!ada) {
				var rambu = {};
				let estCode = $this.attr('data-estcode');
				let division = $this.attr('data-division');
				let block = $this.attr('data-block');
				rambu["PieRecordID"] = $this.val();
				rambu["sortField"] = LIST_PIZOMETER_STATIC_COLOR.length;
				rambu["colorField"] = getRandomColor();
				rambu["lineThickness"] = 3;
				rambu["estCode"] = estCode;
				rambu["Division"] = division;
				rambu["Block"] = block;
				arrayIot.push(rambu);


			}
		});
		LIST_PIZOMETER_STATIC_COLOR = arrayIot;
		//console.log({ LIST_PIZOMETER_STATIC_COLOR, DATA_SELECTED_VALUE_PIZOMETER, arrayIot });
		console.log({ selectPizo: LIST_PIZOMETER_STATIC_COLOR})
		getValuePizometerFilter();

		if ($('#selectFilterPizometer_J').val().length > 15) {
			$("#alertSelectFilterPizometer_J").text("Proses menyimpan data memakan waktu beberapa menit");
		} else {
			$("#alertSelectFilterPizometer_J").text("");
        }
		
		
	});


	$("#selectFilterZona_j").change(function () {
		$('.loading').show();

		$("#selectFilterPizometer_J").val("");
		$('#selectFilterPizometer_J').selectpicker("refresh");

		$("#selectFilterCurahHujan_j").select2().val("").trigger("change");

		DATA_SELECTED_CURAH_HUJAN = [];



		if ($("#selectFilterPizometer_J").val() == "" && $("#selectFilterPizometer_J").val() == "") {
			resetChart();
			setupChart();
		}

		getEstCode();
		
	});


	$('#selectFilterGraph_j').on('change', function () {
		if ($("#selectFilterWmArea_j").val() == "") {
			showMessage("warning", 'Please select wm area');
			$("#selectFilterGraph_j").val("");
			return;
        }

		if ($('#selectFilterGraph_j').val() != '') {
			showGraphTmatById();
		
			$("#btnCreateGraphTmat").text('Save Changes');
		} else {
			resetFormGraphTmat();
			resetChart();
			$("#btnCreateGraphTmat").text('Save');
        }
    })

	$('#btnDeleteGraphTmat').on('click', function () {
		const idGraph = $('#selectFilterGraph_j').val();
		
		$('#btnDeleteGraphTmat').prop('disabled', true);
		if (idGraph == "") {
			showMessage("warning", 'Please selected graph before delete.');
		} else {

			if ($('#btnDeleteGraphTmat').text() == "Delete") {
			
				$('#btnDeleteGraphTmat').html("Confirm delete");
				showMessage("warning", 'Please confirm delete');
			} else {
				
				deleteGraphTmat(idGraph);
				$('#btnDeleteGraphTmat').text("Delete");
            }
			$('#btnDeleteGraphTmat').prop('disabled', false);
		}
	});

	$("#btnCreateGraphTmat").on('click', function () {
		const idGraph = $('#selectFilterGraph_j').val();
		const graphName = $('#namaGraph_j').val();
		const wmArea = $('#selectFilterWmArea_j').val();
		const zonaId = $('#selectFilterZona_j').val() ? $('#selectFilterZona_j').val().toString() : "";
		const curahHujan = $("#selectFilterCurahHujan_j").val();
		const pieRecordID = LIST_PIZOMETER_STATIC_COLOR;
		const createBy = $('#userid').val();
		const updateBy = $('#userid').val();

		if (!wmArea) {
			alert("Wm Area required");
			return;
		}

		if (!graphName) {
			alert("Graph Name required");
			return;
		}

		if (!zonaId) {
			alert("Id Zona required");
			return;
		}
		$('#btnCreateGraphTmat').prop('disabled', true);
		if (idGraph == "") {
			createGraphTmat(graphName, wmArea, zonaId, pieRecordID, curahHujan, createBy, updateBy);
			$('#btnCreateGraphTmat').prop('disabled', false);
		} else {
			updateGraphTmat(idGraph, graphName, zonaId, pieRecordID, curahHujan, updateBy);
			$('#btnCreateGraphTmat').prop('disabled', false);
        }
		
	})
});

function handleTebalLine(value) {
	$("#showUkuranTebalLine").text(value);
}



function getDeepIndicatorColor() {
	$('.loading').show();

	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/getDeepIndicatorColor',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {

			INDICATOR_COLOR = JSON.parse(response.d);
			setColorGraph();
			$('.loading').hide();
			//GetRataRataCurahHujanByWMArea();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$('.loading').hide();
			showMessage('danger', 'Something wrong, please try again or contact developer');
			console.log(thrownError)
		}
	});
}



function showGraphTmatById() {
	$('.loading').show();
	arrDropdownGraph = [];

	const idGraph = $("#selectFilterGraph_j").val();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetGraphTMATById',
		data: JSON.stringify({
			idGraph
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var json = JSON.parse(response.d);
			const { graphName } = json[0];
			
			//remove selected pizometer and curah hujan
			

			$('#namaGraph_j').val(graphName);
			getDetailPizometer();

			

			/*console.log({ json});*/
			showMessage('success', 'Preview graph changes');
			$('.loading').hide();
			//GetRataRataCurahHujanByWMArea();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			console.log(thrownError)
			$('.loading').hide();
		}
	});
}

function getDetailPizometer() {
	$('.loading').show();
	arrDropdownGraph = [];

	const idGraph = $("#selectFilterGraph_j").val();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/getDetailPizometer',
		data: JSON.stringify({
			idGraph
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {

		

			DATA_SELECTED_VALUE_PIZOMETER = JSON.parse(response.d);
			LIST_PIZOMETER_STATIC_COLOR = JSON.parse(response.d);

			const multipleZona = DATA_SELECTED_VALUE_PIZOMETER.map((row) => row.WMA_CODE);
			const multipleEstate = DATA_SELECTED_VALUE_PIZOMETER.map((row) => row.estCode);
			selected_piezometer_estate = multipleEstate;

			$('#selectFilterZona_j').val(multipleZona);
			$('#selectFilterZona_j').selectpicker("refresh");
			$('#selectFilterZona_j').change();

			console.log({ DATA_SELECTED_VALUE_PIZOMETER})
			$('.loading').hide();
			//GetRataRataCurahHujanByWMArea();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			console.log(thrownError)
			$('.loading').hide();
		}
	});
}

function getDetailCurahHujan() {
	$('.loading').show();
	arrDropdownGraph = [];

	const idGraph = $("#selectFilterGraph_j").val();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/getDetailCurahHujan',
		data: JSON.stringify({
			idGraph
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			DATA_SELECTED_CURAH_HUJAN = [];
			DATA_SELECTED_CURAH_HUJAN = JSON.parse(response.d);
			setupGraphDetailCurahHujan();


			$('.loading').hide();
			//GetRataRataCurahHujanByWMArea();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			console.log(thrownError)
			$('.loading').hide();
		}
	});
}

function createGraphTmat(graphName = "", wmArea = 0, zonaId = "", pieRecordID = [], curahHujan = [], createBy = 0, updateBy = 0) {
	$('.loading').show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/CreateGraphTmat',
		data: JSON.stringify({
			graphName,
			wmArea,
			zonaId,
			pieRecordID: JSON.stringify(pieRecordID),
			curahHujan: JSON.stringify(curahHujan),
			createBy,
			updateBy
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {

			const res = JSON.parse(response.d);
			//console.log(res);
			resetFormGraphTmat();
			resetChart();
			showMessage('success', 'Create successfully');
			$('.loading').hide();
			//GetRataRataCurahHujanByWMArea();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			console.log(thrownError)
			$('.loading').hide();
		}
	});
}

function updateGraphTmat(idGraph = "", graphName = "",  zonaId = "", pieRecordID = [], curahHujan = [], updateBy = 0) {
	$('.loading').show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/UpdateGraphTmat',
		data: JSON.stringify({
			idGraph,
			graphName,
			zonaId,
			pieRecordID: JSON.stringify(pieRecordID),
			curahHujan: JSON.stringify(curahHujan),
			updateBy
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {

			//console.log(res);
			resetFormGraphTmat()
			resetChart();
			showMessage('success', 'Changes successfully');
			$('.loading').hide();
			//GetRataRataCurahHujanByWMArea();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(thrownError);
			showMessage('danger', 'Something wrong, please try again or contact developer');
			$('.loading').hide();
		}
	});
}

function deleteGraphTmat(idGraph = "") {
	$('.loading').show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/DeleteGraphTmat',
		data: JSON.stringify({
			idGraph
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {

			const res = JSON.parse(response.d);
			//console.log(res);
			resetFormGraphTmat();
			showGraphTmat();
			resetChart();
			showMessage('success', 'Changes successfully');
			$('.loading').hide();
			//GetRataRataCurahHujanByWMArea();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(thrownError);
			showMessage('danger', 'Something wrong, please try again or contact developer');
			$('.loading').hide();
		}
	});
}

function getZona() {
	//console.log('get zona by estate');
	const idWmArea = $('#selectFilterWmArea_j').val();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetZona',
		data: JSON.stringify({
			idWmArea
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			console.log('data zonanya=' + JSON.stringify(response))
			var json = JSON.parse(response.d);
			$('#selectFilterZona_j').find('option').remove();


			if (json.length > 0) {
				for (var i = 0; i < json.length; i++) {
					$("#selectFilterZona_j").append($('<option>', {
						value: json[i]["WMA_CODE"],
						text: json[i]["NamaZona"]
					}));

				}
				$('#selectFilterZona_j').selectpicker('val', '');
				$('#selectFilterZona_j').selectpicker("refresh");



			} else {
				$('#selectFilterZona_j').selectpicker("refresh");

			}
			$('.loading').hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			$('.loading').hide();
		}
	});

}

function getEstCode() {
	arrEstateDropdownGraph = [];

	const zona = $("#selectFilterZona_j").val() ? $("#selectFilterZona_j").val().toString() : "";
	const idWmArea = $('#selectFilterWmArea_j').val();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetEstcode',
		data: JSON.stringify({
			zona,
			idWmArea
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var json = JSON.parse(response.d);
			$("#selectFilterEstate_j").find('option').remove();

			if (json.length > 0) {
				for (var i = 0; i < json.length; i++) {
					$("#selectFilterEstate_j").append($('<option>', {
						value: json[i]["EstCode"],
						text: json[i]["NewEstName"]
					}));
					arrEstateDropdownGraph.push(json[i]["EstCode"]);
				}
			}
			$("#selectFilterEstate_j").val(selected_piezometer_estate);
			$("#selectFilterEstate_j").selectpicker("refresh");

			if (zona) {
				getPiezometerGraph();
            }
			
			//getZonaByEstateGraph();
			$('.loading').hide()
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			$('.loading').hide();
		}
	});
}

function showGraphTmat() {
	$('.loading').show();
	arrDropdownGraph = [];

	const idWMArea = $("#selectFilterWmArea_j").val();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetAllGraphTMAT',
		data: JSON.stringify({
			idWMArea
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			var json = JSON.parse(response.d);
			$("#selectFilterGraph_j").find('option').remove();
			$("#selectFilterGraph_j").append($('<option>', {
				value: "",
				text: "Nothing selected",
			}));

			if (json.length > 0) {
				for (var i = 0; i < json.length; i++) {
					$("#selectFilterGraph_j").append($('<option>', {
						value: json[i]["idGraph"],
						text: json[i]["graphName"]
					}));
					arrDropdownGraph.push(json[i]["estCode"]);
				}
			}

			//$("#selectFilterEstate_j").val(arrEstateDropdownGraph);
			$('#selectFilterGraph_j').selectpicker('val', '')
			$('#selectFilterGraph_j').selectpicker('refresh');

			$('.loading').hide();
			//GetRataRataCurahHujanByWMArea();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			console.log(thrownError)
			$('.loading').hide();
		}
	});
}

function getValuePizometerFilter() {
	
	if (filterDataPer == '1') {
		
		GetValueGraphTMATPerDay();
	} else {
		GetValueGraphTMAT();
	}


}

function GetValueGraphTMAT() {

	const multiplepieRecordID = $("#selectFilterPizometer_J").val();

	if (multiplepieRecordID.length < 1) {
		$("#selectFilterCurahHujan_j").val("");
		$('#selectFilterCurahHujan_j').select2();
		currentPzoLength = 0;
	}

	var split = $('#tglSelectedGraph').val().split('-');

	var sd = split[0].trim().split("/");
	var ed = split[1].trim().split("/");

	var startDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
	var endDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

	$('.loading').show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetValueGraphTMAT',
		data: JSON.stringify({
			multiplepieRecordID: JSON.stringify(multiplepieRecordID),
			startDate,
			endDate,
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			DATA_PIZOMETER_VALUE = [];
			DATA_PIZOMETER_VALUE = JSON.parse(response.d);
			console.log({ DATA_PIZOMETER_VALUE });
			
			if (DATA_PIZOMETER_VALUE.length > 0) {
				addSetPizometer();
				setupChart();
			}


			if ($('#selectFilterPizometer_J').val() != "" ) {
				if (currentPzoLength > 0) {
					if (currentPzoLength < LIST_PIZOMETER_STATIC_COLOR.length) {
						showMessage('success', 'added graph pizmoter line successfully');

					} else {
						showMessage('success', 'removed graph pizmoter line successfully');

					}
				}
				
				currentPzoLength = LIST_PIZOMETER_STATIC_COLOR.length
            }
			$('.loading').hide();
			//GetRataRataCurahHujanByWMArea();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			console.log(thrownError)
			$('.loading').hide();
		}
	});
}


function GetValueGraphTMATPerDay() {

	const multiplepieRecordID = $("#selectFilterPizometer_J").val();
	console.log({ multiplepieRecordID });

	if (multiplepieRecordID.length < 1) {
		$("#selectFilterCurahHujan_j").val("");
		$('#selectFilterCurahHujan_j').select2();
		currentPzoLength = 0;
	}

	var split = $('#tglSelectedGraph').val().split('-');

	var sd = split[0].trim().split("/");
	var ed = split[1].trim().split("/");

	var startDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
	var endDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

	$('.loading').show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetValueGraphTMATPerDay',
		data: JSON.stringify({
			multiplepieRecordID: JSON.stringify(multiplepieRecordID),
			startDate,
			endDate,
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			DATA_PIZOMETER_VALUE = JSON.parse(response.d);

			if (DATA_PIZOMETER_VALUE.length > 0) {
				addSetPizometer();
				setupChart();
            }



			if ($('#selectFilterPizometer_J').val() != "") {
				if (currentPzoLength > 0) {
					if (currentPzoLength < LIST_PIZOMETER_STATIC_COLOR.length) {
						showMessage('success', 'added graph pizmoter line successfully');

					} else {
						showMessage('success', 'removed graph pizmoter line successfully');

					}
				}

				currentPzoLength = LIST_PIZOMETER_STATIC_COLOR.length
			}
			$('.loading').hide();
			//GetRataRataCurahHujanByWMArea();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			console.log(thrownError)
			$('.loading').hide();
		}
	});
}

function getValueCurahHujanFilter() {
	if (filterDataPer == '1') {
		getValueCurahHujanPerDay();
	} else {
		getValueCurahHujan();
    }
}

function getValueCurahHujan() {
	const multiplepieRecordID = $("#selectFilterPizometer_J").val();
	let pieRecordId = "";
	//console.log({ multiplepieRecordID })
	if (multiplepieRecordID.length > 0) {
		pieRecordId = multiplepieRecordID[0];
    }

	var split = $('#tglSelectedGraph').val().split('-');

	var sd = split[0].trim().split("/");
	var ed = split[1].trim().split("/");

	var startDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
	var endDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/getValueCurahHujanGraphTmat',
		data: JSON.stringify({
			pieRecordId,
			startDate,
			endDate,
			stationSelected: $('#selectFilterCurahHujan_j').val().toString()
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {

			//console.log('data curah hujan='+JSON.stringify(response.d))
			DATA_CURAH_HUJAN_VALUE = JSON.parse(response.d);
			console.log({ DATA_CURAH_HUJAN_VALUE });
			if (DATA_CURAH_HUJAN_VALUE.length > 0) {
				addsetcurahhujan();
				setupChart();
            }
		

		


			$(".loading").hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			$(".loading").hide();
			
		}
	});
}

function getValueCurahHujanPerDay() {
	var split = $('#tglSelectedGraph').val().split('-');

	var sd = split[0].trim().split("/");
	var ed = split[1].trim().split("/");

	var startDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
	var endDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));
	$(".loading").show();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/getValueCurahHujanGraphTmatPerDay',
		data: JSON.stringify({
			startDate,
			endDate,
			stationSelected: $('#selectFilterCurahHujan_j').val().toString()
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			DATA_CURAH_HUJAN_VALUE = [];
			//console.log('data curah hujan='+JSON.stringify(response.d))
			DATA_CURAH_HUJAN_VALUE = JSON.parse(response.d);
			console.log({ DATA_CURAH_HUJAN_VALUE })
			if (DATA_CURAH_HUJAN_VALUE.length > 0) {
				addsetcurahhujan();
				setupChart();
			}




			$(".loading").hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			$(".loading").hide();

		}
	});
}

function getPiezometerGraph() {
	//console.log('getPiezometerGraph');

	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetPiezomenterGraph',
		data: JSON.stringify({
			estCode: $("#selectFilterEstate_j").val() ? $("#selectFilterEstate_j").val().toString() : "",
			zona: $("#selectFilterZona_j").val() ? $("#selectFilterZona_j").val().toString() : ""
		}),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			LIST_DETAIL_PIZOMETER = []

			LIST_DETAIL_PIZOMETER = JSON.parse(response.d);

			
			setupGraphDetailPizometer();
			getDetailCurahHujan();
			//if ($("#selectFilterGraph_j").val() != "" && $("#selectFilterZona_j").val() != "") {
			//	getDetailPizometer();
   //         }
			$('.loading').hide();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			$(".loading").hide();
		}
	});

}

function getCurahHujanGraph() {
	//console.log('get curah hujan');
	const idWMArea = $("#selectFilterWmArea_j").val();
	$.ajax({
		type: 'POST',
		url: '../Service/WebService.asmx/GetCurahHujanGraph',
		data: JSON.stringify({ idWMArea }),
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (response) {
			LIST_DETAIL_CURAH_HUJAN = [];
			LIST_DETAIL_CURAH_HUJAN = JSON.parse(response.d);
			setupGraphDetailCurahHujan();

			$('.loading').hide()
		},
		error: function (xhr, ajaxOptions, thrownError) {
			showMessage('danger', 'Something wrong, please try again or contact developer');
			$(".loading").hide();
		}
	});

}

function setupChart() {
	getLabelChart();
	getDeepIndicatorColor();
	indicatorLabelColor();
	piezometerReferensi();

	let dataChart = [];

	if (chartCH.length > 0) {
		dataChart.push(chartCH[0]);
	}

	if (chartData.length > 0) {
		for (var i = 0; i < chartData.length; i++) {
			dataChart.push(chartData[i]);
		}
    }

	var textTitle = "";

	if ($('#selectFilterZona').val() != 0) {
		if ($('#selectFilterGraph_j option:selected').val() != "") {
			textTitle = $('#selectFilterGraph_j option:selected').text();
        }	
	}

	if (textTitle == "") {
		if ($('#namaGraph_j').val() == "") {
			textTitle = "";
		} else {

			textTitle = "Create new graph " + $('#namaGraph_j').val();
        }
		
	}


	var dataPointAll = []
	let max = 0;
	let min = 0;
	if (dataChart.length > 0) {
		for (var p = 0; p < dataChart.length; p++) {

			if (dataChart[p]["dataPoints"] != undefined && dataChart[p]["type"] == "line") {

				for (var k = 0; k < dataChart[p]["dataPoints"].length; k++) {
					dataPointAll.push(dataChart[p]["dataPoints"][k]["y"]);
				}
			}

		}

		max = parseFloat(Math.max(...dataPointAll) + 5).toFixed(2);
		min = parseFloat(Math.min(...dataPointAll) - 5).toFixed(2);
	}



	const _interval = filterDataPer == "1" ? 0 : 1;
	const _intervalType = filterDataPer == "1" ? "day" : "";
	const _labelAngle = filterDataPer == "1" ? 0 : 0;

	var chart = new CanvasJS.Chart("divChart", {
		animationEnabled: true,
		zoomEnabled: true,
		theme: "light2",
		axisX: {
			labelAngle: _labelAngle,
			interval: _interval,
			intervalType: _intervalType,
			labelFontSize: 12,
			labelFormatter: function (e) {
				
				let result;
				if (filterDataPer == "1") {
					result = moment(e.value).format('DD/MM/YY');
				} else {
					let _label = "No data";
					if (e.label != null) {
						_label = e.label
					}
				
					result = _label;
                }
				
				return result;
			}
		},
		axisY: {
			stripLines: SET_GRAPH_COLOR,
			maximum: max,
			minimum: min,
			reversed: true,
			interval: 10,
			title: "Piezometer",
			titleFontSize: 15,
			labelFontSize: 13,
			labelMaxWidth: 50
		},
		axisY2: [
			//{
			//	title: "TMAT s",
			//	titleFontSize: 15,
			//	labelFontSize: 13,
			//	labelMaxWidth: 50
			//},
			{
				title: "Curah Hujan",
				titleFontSize: 15,
				labelFontSize: 13,
				labelMaxWidth: 50,
				titleFontColor: "#75aafa",
				lineColor: "#75aafa",
				tickColor: "#75aafa",
				labelFontColor: "#75aafa"
			}
		],
		title: {
			text: textTitle,
			fontWeight: "bolder",
			fontFamily: "Calibri",
			fontSize: 21,
			padding: 10,
			labelMaxWidth: 50
		},
		data: dataChart
	});
	
	chart.render();
}

// Fungsi untuk mendapatkan minggu ke berapa dalam sebuah objek Date
function getWeek(value) {
	var date = value.getDate();
	var day = value.getDay();
	var result = Math.ceil((date - 1 - day) / 7);
	return result == 0 ? 1 : result;
}

function addSetPizometer() {
	chartData = [];
	if (DATA_PIZOMETER_VALUE != undefined && DATA_PIZOMETER_VALUE.length > 0) {

		for (var j = 0; j < DATA_PIZOMETER_VALUE.length; j++) {
			var chartIOT = [];
			var nilai = null;
			var _toolTipContent = "";
			for (var m = 0; m < DATA_PIZOMETER_VALUE[j].length; m++) {
				nilai = parseFloat(DATA_PIZOMETER_VALUE[j][m]["Ketinggian"]);


				if (nilai != null) {
					if (nilai > 100) {
						nilai = 100;
					}

					if (nilai < -100) {
						nilai = -100;
					}
				}

				_toolTipContent = filterDataPer == '1' ? `{legendText}<br/>{x}, <strong>{xValue}</strong> cm` : `{legendText}<br/>{label}, <strong>{xValue}</strong> cm`;
				var months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
				//var sdate = dateFormat(new Date(DATA_IOT[j][m]["RecordTime"])).split('/');

				if (filterDataPer == "1") {
					chartIOT.push({
						x: new Date(DATA_PIZOMETER_VALUE[j][m]["tglPengukuran"]),
						y: nilai,
						xValue: parseFloat(DATA_PIZOMETER_VALUE[j][m]["Ketinggian"])
					});

				} else {
					chartIOT.push({
						x: parseFloat(DATA_PIZOMETER_VALUE[j][m]["ID"]),
						y: nilai,
						label: "Week " + DATA_PIZOMETER_VALUE[j][m]["Week"] + ' ' + months[DATA_PIZOMETER_VALUE[j][m]["Month"] - 1] + " " + DATA_PIZOMETER_VALUE[j][m]["Year"],
						xValue: parseFloat(DATA_PIZOMETER_VALUE[j][m]["Ketinggian"])
					});
				}
			}
			


			if (DATA_PIZOMETER_VALUE[j].length > 0) {

	;
				//console.log({ addset: LIST_PIZOMETER_STATIC_COLOR, DATA_PIZOMETER_VALUE });
				if (DATA_PIZOMETER_VALUE[j][0]["colorField"] == "" || DATA_PIZOMETER_VALUE[j][0]["colorField"] == null) {

					if (LIST_PIZOMETER_STATIC_COLOR.length > 0) {
						DATA_PIZOMETER_VALUE[j][0]["colorField"] = LIST_PIZOMETER_STATIC_COLOR[j]["colorField"];
					}

				}

				if (DATA_PIZOMETER_VALUE[j][0]["estCode"] == "" || DATA_PIZOMETER_VALUE[j][0]["estCode"] == null) {

					if (LIST_PIZOMETER_STATIC_COLOR.length > 0) {
						DATA_PIZOMETER_VALUE[j][0]["estCode"] = LIST_PIZOMETER_STATIC_COLOR[j]["estCode"];
					}

				}

				if (DATA_PIZOMETER_VALUE[j][0]["Division"] == "" || DATA_PIZOMETER_VALUE[j][0]["Division"] == null) {

					if (LIST_PIZOMETER_STATIC_COLOR.length > 0) {
						DATA_PIZOMETER_VALUE[j][0]["Division"] = LIST_PIZOMETER_STATIC_COLOR[j]["Division"];
					}

				}

				if (DATA_PIZOMETER_VALUE[j][0]["lineThickness"] == "" || DATA_PIZOMETER_VALUE[j][0]["lineThickness"] == null) {

					if (LIST_PIZOMETER_STATIC_COLOR.length > 0) {
						DATA_PIZOMETER_VALUE[j][0]["lineThickness"] = LIST_PIZOMETER_STATIC_COLOR[j]["lineThickness"];
					}

				}
				//var colorIot = selectedRambuMeasure.filter((row) => row.stationId == DATA_IOT_DETAIL[j][0]["recordStationId"]);
			
				var newSeriesIot = {
					name: DATA_PIZOMETER_VALUE[j][0]["PieRecordID"] + ` [ ${DATA_PIZOMETER_VALUE[j][0]["estCode"]} | ${DATA_PIZOMETER_VALUE[j][0]["Division"]} | ${DATA_PIZOMETER_VALUE[j][0]["Block"]} ]`,
					legendText: DATA_PIZOMETER_VALUE[j][0]["PieRecordID"] + ` [ ${DATA_PIZOMETER_VALUE[j][0]["Block"]} ]`,
					color: DATA_PIZOMETER_VALUE[j][0]["colorField"],
					markerColor: DATA_PIZOMETER_VALUE[j][0]["colorField"],
					markerType: "circle",
					connectNullData: false,
					indexLabelLineThickness: 367,
					lineThickness: DATA_PIZOMETER_VALUE[j][0]["lineThickness"],
					markerSize: DATA_PIZOMETER_VALUE[j][0]["lineThickness"] + 5,
					axisYIndex: 0,
					axisYType: "primary",
					type: "line",
					click: function (e) {

						//alert(e.dataSeries.name + ", "+measurementId);
						//alert(e.dataSeries.name + ", dataPoint { x:" + e.dataPoint.label + ", y: " + e.dataPoint.y + " }");
					},
					showInLegend: "true",
					xValueFormatString: "DD-MMM-YYYY",
					//toolTipContent: "{legendText}<br/>{label}, <strong>{y}</strong> m",
					toolTipContent: _toolTipContent,
					dataPoints: chartIOT,
				};



				chartData.push(newSeriesIot);
			}
		

		}


		console.log({ check:chartData})
	}

}


function addsetcurahhujan() {
	
	if (DATA_CURAH_HUJAN_VALUE != undefined && DATA_CURAH_HUJAN_VALUE.length > 0) {
		var chartIOT = [];
		var nilai = null;
		chartCH = [];
		for (var j = 0; j < DATA_CURAH_HUJAN_VALUE.length; j++) {
		
			nilai = DATA_CURAH_HUJAN_VALUE[j]["sumRainInches"];


			if (nilai != null) {
				if (nilai > maxY) {
					maxY = nilai;
				}

				if (minY == null) {
					minY = nilai;
				} else if (nilai < minY) {
					minY = nilai;
				}
			}
			//var sdate = dateformat(new date(data_iot[j][m]["recordtime"])).split('/');
			var months = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];

			if (filterDataPer == '1') {
				chartIOT.push({
					x: new Date(DATA_CURAH_HUJAN_VALUE[j]["date"]),
					y: nilai,
				});
			} else {
				chartIOT.push({
					x: parseFloat(DATA_CURAH_HUJAN_VALUE[j]["ID"]),
					y: nilai,
					label: "Week " + DATA_CURAH_HUJAN_VALUE[j]["Week"] + ' ' + months[DATA_CURAH_HUJAN_VALUE[j]["Month"] - 1] + " " + DATA_CURAH_HUJAN_VALUE[j]["Year"]
				});
            }


		} 
		var _toolTipContent = filterDataPer == '1' ? "{legendText}<br/>{x}, <strong>{y}</strong> mm" : "{legendText}<br/>{label}, <strong>{y}</strong> mm"
		var newSeriesIot = {
			name: "Rata Rata Curah Hujan",
			legendText: "Rata Rata Curah Hujan",
			color: "#75aafa",
			markerColor: "#75aafa",
			markerType: "circle",
			axisYType: 'secondary',
			axisYIndex:1,
			fillOpacity: .6,
			labelMaxWidth: 50,
			type: "column",
			connectNullData: false,
			indexLabelLineThickness:367,
			click: function (e) {

				//alert(e.dataSeries.name + ", "+measurementId);
				//alert(e.dataSeries.name + ", dataPoint { x:" + e.dataPoint.label + ", y: " + e.dataPoint.y + " }");
			},
			markerSize: 8,
			showInLegend: "true",
			xValueFormatString: "DD-MMM-YYYY",
			toolTipContent: _toolTipContent,
			dataPoints: chartIOT
		};
		chartCH.push(newSeriesIot);
	}
	//console.log({ chartCH });
}


function getLabelChart() {
	var split = $('#tglSelectedGraph').val().split('-');

	var sd = split[0].trim().split("/");
	var ed = split[1].trim().split("/");

	var startDate = new Date(parseInt(sd[2]), parseInt(sd[1]) - 1, parseInt(sd[0]));
	var endtDate = new Date(parseInt(ed[2]), parseInt(ed[1]) - 1, parseInt(ed[0]));

	labelChart = [];
	for (var now = startDate; now <= endtDate; now.setDate(now.getDate() + 7)) {
		labelChart.push(dateFormat(now));
	}

	//console.log({ labelChart})
}


function resetFormGraphTmat(type = "") {
	if (type != "") {
		LIST_PIZOMETER_STATIC_COLOR = [];
		if ($('#selectFilterGraph_j').val() != '') {
			$('#namaGraph_j').val("");

			$("#selectFilterEstate_j").find('option').remove();
			$("#selectFilterZona_j").find('option').remove();
			$('#selectFilterGraph_j').find('option').remove();

			$('.selectpicker').selectpicker('refresh');

			$("#selectFilterCurahHujan_j").find('option').remove();
			$('#selectFilterCurahHujan_j').select2();

			$("#selectFilterPizometer_J").find('option').remove();
			$('#selectFilterPizometer_J').selectpicker("refresh");
        }

	} else {
		LIST_PIZOMETER_STATIC_COLOR = [];
		$('#namaGraph_j').val("");
		$("#selectFilterWmArea_j").selectpicker('val', "");

		$("#selectFilterEstate_j").find('option').remove();

		$("#selectFilterZona_j").find('option').remove();

		$('#selectFilterGraph_j').find('option').remove();

		$('.selectpicker').selectpicker('refresh');

		$("#selectFilterCurahHujan_j").find('option').remove();
		$('#selectFilterCurahHujan_j').select2();

		$("#selectFilterPizometer_J").find('option').remove();
		$('#selectFilterPizometer_J').selectpicker("refresh");

		
    }
	
}

function resetChart() {
	chartData = [];
	chartCH = [];
	setupChart();
}

function setupGraphDetailPizometer() {
	$('#selectFilterPizometer_J').find('option').remove();

	if (LIST_DETAIL_PIZOMETER.length > 0) {

		for (var i = 0; i < LIST_DETAIL_PIZOMETER.length; i++) {
			$("#selectFilterPizometer_J").append($('<option>', {
				'data-estCode': LIST_DETAIL_PIZOMETER[i]["estCode"],
				'data-division': LIST_DETAIL_PIZOMETER[i]["Division"],
				'data-block': LIST_DETAIL_PIZOMETER[i]["Block"],
				value: LIST_DETAIL_PIZOMETER[i]["PieRecordID"],
				text: LIST_DETAIL_PIZOMETER[i]["PieRecordID"] + ` [ ${LIST_DETAIL_PIZOMETER[i]["estCode"]} | ${LIST_DETAIL_PIZOMETER[i]["Division"]} | ${LIST_DETAIL_PIZOMETER[i]["Block"]} ]`
			}));

		}
    }


	let chIotSelect = [];

	for (var s = 0; s < DATA_SELECTED_VALUE_PIZOMETER.length; s++) {
		chIotSelect.push(DATA_SELECTED_VALUE_PIZOMETER[s]["PieRecordID"]);
		//console.log('rambu curah hujan = ' + chSelect);
	}

	//console.log({ chIotSelect})
	$('#selectFilterPizometer_J').selectpicker("refresh");
	$('#selectFilterPizometer_J').val(chIotSelect);
	$('#selectFilterPizometer_J').selectpicker("refresh");
	$('#selectFilterPizometer_J').change();



}

function setupGraphDetailCurahHujan() {
	$('#selectFilterCurahHujan_j').find('option').remove();

	if (LIST_DETAIL_CURAH_HUJAN.length > 0) {
		for (var i = 0; i < LIST_DETAIL_CURAH_HUJAN.length; i++) {
			$("#selectFilterCurahHujan_j").append($('<option>', {
				value: LIST_DETAIL_CURAH_HUJAN[i]["estCode"] + ":" + LIST_DETAIL_CURAH_HUJAN[i]["FCCODE"],
				text: LIST_DETAIL_CURAH_HUJAN[i]["FCBA"] + " : " + LIST_DETAIL_CURAH_HUJAN[i]["FCCODE"]
			}));

		}
    }

	let chIotSelect = [];

	for (var s = 0; s < DATA_SELECTED_CURAH_HUJAN.length; s++) {
		chIotSelect.push(DATA_SELECTED_CURAH_HUJAN[s]["curahHujan"]);
		//console.log('rambu curah hujan = ' + chSelect);
	}


	//CONSOLE.L
	console.log({ curahHUjan: chIotSelect });
	$("#selectFilterCurahHujan_j").select2().val(chIotSelect).trigger("change");
}

function getRandomColor() {
	const letters = '0123456789ABCDEF';
	let color = '#';
	for (let i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}

function piezometerReferensi() {
	LIST_PIZOMETER_STATIC_COLOR = sortById(LIST_PIZOMETER_STATIC_COLOR);

	if (LIST_PIZOMETER_STATIC_COLOR.length > 0) {
		//LIST_PIZOMETER_STATIC_COLOR.sort(predicateBy("sortField"));
		//console.info("rambu selected ", JSON.stringify(selectedRambu));
		var content = '<ul id="tgldragdropPiezometer" class="connectedSortable" style="margin-left:-45px">';
		for (var i = 0; i < LIST_PIZOMETER_STATIC_COLOR.length; i++) {
			content += '<li class="ui-state-default chips" id="' + LIST_PIZOMETER_STATIC_COLOR[i]["PieRecordID"] + '" style="background:' + LIST_PIZOMETER_STATIC_COLOR[i]["colorField"] + '"><font color="white">' + LIST_PIZOMETER_STATIC_COLOR[i]["PieRecordID"] + ` [ ${LIST_PIZOMETER_STATIC_COLOR[i]["estCode"]} | ${LIST_PIZOMETER_STATIC_COLOR[i]["Division"]} | ${LIST_PIZOMETER_STATIC_COLOR[i]["Block"]} ]` + '</font></li>';
		}
		content += '</ul>';
		$("#divPiezometerRefrensi").html(content);

		//console.log($("#tgldragdropPiezometer"));
		$('#tgldragdropPiezometer').sortable({
			connectWith: ".connectedSortable",
			update: function () {
				var order1 = $('#tgldragdropPiezometer').sortable('toArray')
				for (var i = 0; i < order1.length; i++) {
					for (var j = 0; j < LIST_PIZOMETER_STATIC_COLOR.length; j++) {
						if (LIST_PIZOMETER_STATIC_COLOR[j]["PieRecordID"] == order1[i]) {
							LIST_PIZOMETER_STATIC_COLOR[j]["sortField"] = i;
							break;
						}
					}
				}
				//LIST_PIZOMETER_STATIC_COLOR.sort(predicateBy("sortField"));
				LIST_PIZOMETER_STATIC_COLOR = sortById(LIST_PIZOMETER_STATIC_COLOR);
				//console.info({ LIST_PIZOMETER_STATIC_COLOR });
				getValuePizometerFilter();
				//getStationIotRelation();
			}
		}).disableSelection();

		$("#tgldragdropPiezometer li").click(function () {
			var name = $(this).html().replace('<font color="white">', "").replace('</font>', '');
			var params = { id: $(this).attr("id"), name: name };
			//console.log({ params });
			changeColor(params);
		});

	} else {
		$("#divPiezometerRefrensi").html("");
	}



}

function changeColor(params) {
	var colorVar = "";
	let lineThickness = 3;
	for (var i = 0; i < LIST_PIZOMETER_STATIC_COLOR.length; i++) {
		if (LIST_PIZOMETER_STATIC_COLOR[i]["PieRecordID"] == params["id"]) {
			colorVar = LIST_PIZOMETER_STATIC_COLOR[i]["colorField"];
			lineThickness = LIST_PIZOMETER_STATIC_COLOR[i]["lineThickness"];
			break;
		}
	}

	var content = '<div class="row" align="center">';
	content += '<div class="row" align="center"><label>Select Color</label></div>';
	content += '<div class="row picker" id="colorSelected" style="width:100px;height:100px;"></div><br>';
	content += '<div class="row" align="center"><label>Line Thickness</label></div>';
	content += '<div class="slidecontainer" ><input id="tebalLine" class="slider_l" oninput="handleTebalLine(this.value)" onchange="handleTebalLine(this.value)" style=""  type="range" min="1" max="15" value="' + lineThickness+'"/></div>';
	content += '<div class="row" style="margin-bottom: 20px;" align="center">ukuran: <div id="showUkuranTebalLine">' + lineThickness +'</div></div>';
	content += '<div class="row" align="center"><button type="submit" class="btn btn-primary" id="btnSaveColor">Save</button></div>';
	content += '</div>';
	$("#formChangeColor").html(content);

	// Create a <style> element
	var styleElement = document.createElement('style');

	// Define the CSS rules
	var cssRules = `
    /* Thumb: webkit */
    .slider_l::-webkit-slider-thumb {
        border: 2px solid ${colorVar};
        box-shadow: -407px 0 0 400px ${colorVar};
    }

    /* Thumb: Firefox */
    .slider_l::-moz-range-thumb {
        border: 1px solid ${colorVar};
        box-shadow: -407px 0 0 400px ${colorVar};
    }
`;

	// Set the CSS rules to the <style> element
	styleElement.innerHTML = cssRules;

	// Append the <style> element to the document head
	document.head.appendChild(styleElement);


	Alwan.defaults.swatches = ['Black', 'Yellow', 'Red', 'Green', 'Blue', 'Brown', 'Grey'];
	const alwan = {
		bg: new Alwan('#colorSelected', {
			color: colorVar,
			popover: false,
			toggle: false,
			opacity: false
		})
	}

	alwan.bg.on('color', (color) => {
		colorVar = color.hex;
	});



	$("#formChangeColor").dialog({
		"title": params["name"],
		"width": 280,
		"height": 480
	});
	//$('#colorSelected').colorpicker();
	//$('#colorSelected').colorpicker().on(
	//	'changeColor',
	//	function () {
	//		$('#colorSelected').css('background-color',
	//			$(this).colorpicker('getValue', color));
	//    }
	//);

	$('#btnSaveColor').click(function () {

		if ($("#tebalLine").val() != "") {
			console.log($("#tebalLine").val());
			lineThickness = parseFloat($("#tebalLine").val());
		}



		for (var i = 0; i < LIST_PIZOMETER_STATIC_COLOR.length; i++) {
			if (LIST_PIZOMETER_STATIC_COLOR[i]["PieRecordID"] == params["id"]) {
				var x = $('#colorSelected').css('backgroundColor');
				LIST_PIZOMETER_STATIC_COLOR[i]["colorField"] = colorVar;
				LIST_PIZOMETER_STATIC_COLOR[i]["lineThickness"] = lineThickness;
				break;
			}
		}
		
		for (var i = 0; i < DATA_PIZOMETER_VALUE.length; i++) {
			if (DATA_PIZOMETER_VALUE[i].length > 0) {
				if (DATA_PIZOMETER_VALUE[i][0]["PieRecordID"] == params["id"]) {
					var x = $('#colorSelected').css('backgroundColor');
					DATA_PIZOMETER_VALUE[i][0]["colorField"] = colorVar;
					DATA_PIZOMETER_VALUE[i][0]["lineThickness"] = lineThickness;
					//console.log({ selectedColor: LIST_PIZOMETER_STATIC_COLOR[i]["colorField"], serverColor: DATA_PIZOMETER_VALUE })
					break;
				}
				
			}

		}

		

		addSetPizometer();
		setupChart();
		
		//chipsRambuRefrensi();
		//GetStationMeasureArray();
		//getStationIotRelation();
		$("#formChangeColor").dialog("close");
	})
}

function sortById(data) {
	return data.sort(function (a, b) {
		return (a.sortField - b.sortField);
	});
}

function setColorGraph() {
	SET_GRAPH_COLOR = [];
	if (INDICATOR_COLOR.length > 0) {
		for (var s = 0; s < INDICATOR_COLOR.length; s++) {
			if (INDICATOR_COLOR[s].MaxValue == -1) {
				SET_GRAPH_COLOR.push({
					startValue: INDICATOR_COLOR[s].MinValue ,
					endValue: INDICATOR_COLOR[s].MaxValue + 1,
					color: INDICATOR_COLOR[s].colorBack,
					//opacity: .6,
					//label: `${INDICATOR_COLOR[s].IndicatorName} ${INDICATOR_COLOR[s].IndicatorAlias}`,
					labelFontColor: INDICATOR_COLOR[s].colorFont
				});
			} else if (INDICATOR_COLOR[s].MinValue == 41) {
				SET_GRAPH_COLOR.push({
					startValue: INDICATOR_COLOR[s].MinValue - 1,
					endValue: INDICATOR_COLOR[s].MaxValue,
					color: INDICATOR_COLOR[s].colorBack,
					//opacity: .6,
					//label: `${INDICATOR_COLOR[s].IndicatorName} ${INDICATOR_COLOR[s].IndicatorAlias}`,
					labelFontColor: INDICATOR_COLOR[s].colorFont
				});
			} else if (INDICATOR_COLOR[s].MinValue == 46) {
				SET_GRAPH_COLOR.push({
					startValue: INDICATOR_COLOR[s].MinValue - 1,
					endValue: INDICATOR_COLOR[s].MaxValue,
					color: INDICATOR_COLOR[s].colorBack,
					//opacity: .6,
					//label: `${INDICATOR_COLOR[s].IndicatorName} ${INDICATOR_COLOR[s].IndicatorAlias}`,
					labelFontColor: INDICATOR_COLOR[s].colorFont
				});
			} else if (INDICATOR_COLOR[s].MinValue == 61) {
				SET_GRAPH_COLOR.push({
					startValue: INDICATOR_COLOR[s].MinValue - 1,
					endValue: INDICATOR_COLOR[s].MaxValue,
					color: INDICATOR_COLOR[s].colorBack,
					//opacity: .6,
					//label: `${INDICATOR_COLOR[s].IndicatorName} ${INDICATOR_COLOR[s].IndicatorAlias}`,
					labelFontColor: INDICATOR_COLOR[s].colorFont
				});
			} else if (INDICATOR_COLOR[s].MinValue == 66) {
				SET_GRAPH_COLOR.push({
					startValue: INDICATOR_COLOR[s].MinValue - 1,
					endValue: INDICATOR_COLOR[s].MaxValue,
					color: INDICATOR_COLOR[s].colorBack,
					//opacity: .6,
					//label: `${INDICATOR_COLOR[s].IndicatorName} ${INDICATOR_COLOR[s].IndicatorAlias}`,
					labelFontColor: INDICATOR_COLOR[s].colorFont
				});
			}else if (INDICATOR_COLOR[s].MinValue == 999) {
				SET_GRAPH_COLOR.push({
					startValue: INDICATOR_COLOR[s].MinValue - 1,
					endValue: INDICATOR_COLOR[s].MaxValue,
					color: INDICATOR_COLOR[s].colorBack,
					//opacity: .6,
					//label: `${INDICATOR_COLOR[s].IndicatorName} ${INDICATOR_COLOR[s].IndicatorAlias}`,
					labelFontColor: INDICATOR_COLOR[s].colorFont
				});
			}else {
				SET_GRAPH_COLOR.push({
					startValue: INDICATOR_COLOR[s].MinValue,
					endValue: INDICATOR_COLOR[s].MaxValue,
					color: INDICATOR_COLOR[s].colorBack,
					//opacity: .6,
					//label: `${INDICATOR_COLOR[s].IndicatorName} ${INDICATOR_COLOR[s].IndicatorAlias}`,
					labelFontColor: INDICATOR_COLOR[s].colorFont
				});
            }


			//console.log('rambu curah hujan = ' + chSelect);
		}
		
    }
}

function indicatorLabelColor() {
	var content = '';
	if (INDICATOR_COLOR.length > 0) {
		for (var s = 0; s < INDICATOR_COLOR.length; s++) {
			content += `<div class="square-j" style="background: ${INDICATOR_COLOR[s].colorBack}; width: 12px; height: 12px; margin-right: 5px;position:relative;"> </div>`;
			content += `<div class="title-note-j" style="font-size: 14px; font-weight: bold;margin-right: 10px;">${INDICATOR_COLOR[s].IndicatorName} ${INDICATOR_COLOR[s].IndicatorAliasReport}</div>`;
		}

		$('#indicatorLabelColor').html(content);
	}

}



function showMessage(type = "success", message) {
	if (type == "info") {
		Toastify({
			position: "right",
			gravity: "bottom",
			text: message,
			duration: 3000,
			style: {
				background: "#eff6ff",
				color: "#1d4ed8"
			}
		}).showToast();
	} else if (type == "warning") {
		Toastify({
			position: "right",
			gravity: "bottom",
			text: message,
			duration: 3000,
			style: {
				background: "#fefce8",
				color: "#a16207"
			}
		}).showToast();
    }else if (type == "success") {
		Toastify({
			position: "right",
			gravity: "bottom",
			text: message,
			duration: 3000,
			style: {
				background: "#f0fdf4",
				color: "#15803d"
			}
		}).showToast();
	} else {
		Toastify({
			position: "right",
			gravity: "bottom",
			text: message,
			duration: 3000,
			style: {
				background: "#fef2f2",
				color: "#c61c1c"
			}
		}).showToast();
    }
}

function selectAll() {
	$("#selectFilterPizometer_J > option").prop("selected", true);
	$("#selectFilterPizometer_J").trigger("change");
}

function deselectAll() {
	$("selectFilterPizometer_J > option").prop("selected", false);
	$("#selectFilterPizometer_J").trigger("change");
}