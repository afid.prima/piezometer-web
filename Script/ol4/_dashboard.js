﻿//////////////////////////////////////////////////////////
///////////////// REGION ONLOAD ///////////////////
//////////////////////////////////////////////////////////
var selectedCompany = "", selectedEstate = "", selectedCompanyForSubChart = "", selectedCompanyShortname = "", labelUnit = "";
var canvas, ctx;
var myBar, barChartConfig, barChartData, myOtherBar, otherBarChartConfig, otherBarChartData;
var myChartDataPengukuran;
var flagValue = 0;
$(function () {
    Init();
    $("#btnCloseSubChart").hide();
    //Variable 
    $('#divChartPiezometer').html('<canvas id="chartPiezometer" style="height: 180px;"></canvas>')

    canvas = document.getElementById('chartPiezometer');
    ctx = canvas.getContext('2d');

    $('.selectpicker').selectpicker({});
	//Flat green color scheme for iCheck
    $('input[type="radio"].flat-green').iCheck({
        radioClass: 'iradio_flat-green'
    })
    ListCompany();
    ShowChartDefaultProgress("", "");
    ShowChartSumberData();

    $("#btnCloseSubChart").click(function () {
        labelUnit = "All Estate";
        ShowChartCustomProgress("", "", flagValue);
        ShowChartDataPengukuran_Custom("", "", flagValue, "");

        $("#dialog").dialog("close");

        $('#mainchart').show();
        $('#subchart').hide();
        $("#btnCloseSubChart").hide();
    })
})

//////////////////////////////////////////////////////////
///////////////// REGION CUSTOM METHOD ///////////////////
//////////////////////////////////////////////////////////

function Init() {
    //POPUP DIALOG
    $("#dialog").dialog();
    $("#dialog").dialog("close");
}

function ShowChartDefaultProgress(companycode, estcode) {
    $(".loading").show();
    $.ajax({
        type: 'POST',
        //url: '../Service/MapService.asmx/GetProgressForDashboard',
        //data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '"}',
        url: '../Service/MapService.asmx/GetDataProgressForDashboard',
        data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", AdjustmentValue: "' + flagValue + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            if (json.length > 0) {
                //Show Chart Data Pengukuran
                ShowChartDataPengukuran("", "", flagValue, "")

                $('#lblWeekName').text(json[0].WeekName);
                $('#lblDateRange').text("Range: " + json[0].DateRange);

                var labelname = [];
                var data_recorded = [], data_unrecorded = [], data_percbanjir = [], data_perctergenang = [], data_percagaktergenang = [],
                    data_percnormal = [], data_perckering = [], data_percrusakhilang = [], data_percrecorded = [], data_percunrecorded = [];
                var dataset = [];

                var goalcompletion_data = "";
                var goalcompletion_percentage;
                for (var i = 0; i < json.length; i++) {
                    labelname.push(json[i].LabelName);
                    data_recorded.push(json[i].Recorded);
                    data_unrecorded.push(json[i].NoData);
                    data_percbanjir.push(json[i].PercBanjir);
                    data_perctergenang.push(json[i].PercA);
                    data_percagaktergenang.push(json[i].PercB);
                    data_percnormal.push(json[i].PercC);
                    data_perckering.push(json[i].PercD);
                    data_percrusakhilang.push(json[i].PercRusakHilang);
                    data_percrecorded.push(json[i].PercRecorded);
                    data_percunrecorded.push(json[i].PercNoData);

                    //Goal Completion
                    goalcompletion_percentage = parseFloat(json[i].Recorded) / parseFloat(json[i].TotalData) * 100;

                    goalcompletion_data += '<div class="progress-group">';
                    goalcompletion_data += '<span class="progress-text">' + json[i].LabelName + '</span>';
                    goalcompletion_data += '<span class="progress-number">' + ($('#rbByPiezoMaster').is(":checked") ? json[i].Recorded : json[i].PercRecorded) + '/<b>' + ($('#rbByPiezoMaster').is(":checked") ? json[i].TotalData : 100) + '</b></span>';
                    goalcompletion_data += '<div class="progress sm">';
                    goalcompletion_data += '<div class="progress-bar progress-bar-aqua" style="width: ' + goalcompletion_percentage + '%"></div>';
                    goalcompletion_data += '</div></div>';
                }

                $('#goalcompletion').html(goalcompletion_data);

                if ($('#rbSummaryKondisi').is(":checked")) {
                    dataset.push({
                        label: 'Banjir',
                        backgroundColor: "#00BFFF", data: data_percbanjir
                    })

                    dataset.push({
                        label: 'Tergenang',
                        backgroundColor: "#FF8000", data: data_perctergenang
                    })

                    dataset.push({
                        label: 'Agak Tergenang',
                        backgroundColor: "#FFFF00", data: data_percagaktergenang
                    })

                    dataset.push({
                        label: 'Normal',
                        backgroundColor: "#008000", data: data_percnormal
                    })

                    dataset.push({
                        label: 'Kering',
                        backgroundColor: "#FF0000", data: data_perckering
                    })

                    dataset.push({
                        label: 'Rusak/Hilang',
                        backgroundColor: "#808080", data: data_percrusakhilang
                    })

                    dataset.push({
                        label: 'No Data',
                        backgroundColor: "#C0C0C0", data: data_percunrecorded
                    })

                }
                else {
                    dataset.push({
                        label: 'Recorded',
                        backgroundColor: "#00ff00", data: $('#rbByPiezoMaster').is(":checked") ? data_recorded : data_percrecorded
                    })

                    dataset.push({
                        label: 'No Data',
                        backgroundColor: "#ff0000", data: $('#rbByPiezoMaster').is(":checked") ? data_unrecorded : data_percunrecorded
                    })
                }

                barChartData = {
                    labels: labelname,
                    datasets: dataset
                };

                barChartConfig = {
                    type: 'bar',
                    data: barChartData,
                    options: {
                        tooltips: {
                            mode: 'label',
                            callbacks: {
                                label: function (tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
                                }
                            }
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                stacked: true,
                                barThickness: 20
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        }
                    }
                }

                myBar = new Chart(ctx, barChartConfig);

                document.getElementById("chartPiezometer").onclick = function (evt) {
                    var activePoints = myBar.getElementsAtEvent(evt);
                    var firstPoint = activePoints[0];
                    var secondPoint = activePoints[1];
                    var label = myBar.data.labels[firstPoint._index];
                    var value = myBar.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
                    var value_2 = myBar.data.datasets[secondPoint._datasetIndex].data[secondPoint._index];
                    //alert(label + ": " + value + " - " + value_2);

                    var company = (label.substring(0, 4) === "THIP" ? "PT.THIP" : ("PT." + label.replace("-", "")));
                    var keterangan = (label.substring(0, 4) === "THIP" ? label : "");
                    labelUnit = label;
                    selectedCompanyForSubChart = (label.substring(0, 4) === "THIP" ? "PT.THIP" : ("PT." + label.replace("-", "")));
                    selectedCompanyShortname = (label.substring(0, 4) === "THIP" ? label : "");
                    ShowSubChartCustomProgress(company, "", keterangan);
                    ShowChartDataPengukuran_Custom(company, "", flagValue, selectedCompanyShortname);

                    $("#btnCloseSubChart").show();
                    $('#mainchart').hide();
                    $('#subchart').show();
                };

            }
            else {
                alert("No Data");
            }
            $(".loading").hide();
            
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.statusText);
        }
    });
}

function ShowChartCustomProgress(companycode, estcode, adjust) {
    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDataProgressForDashboard',
        data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", AdjustmentValue: "' + adjust + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            if (json.length > 0) {
                $('#divChartPiezometer').html('');
                $('#divChartPiezometer').html('<canvas id="chartPiezometer" style="height: 180px;"></canvas>')
                canvas = document.getElementById('chartPiezometer');
                ctx = canvas.getContext('2d');
                
                $('#lblWeekName').text(json[0].WeekName);
                $('#lblDateRange').text("Range: " + json[0].DateRange);

                var labelname = [];
                var data_recorded = [], data_unrecorded = [], data_percbanjir = [], data_perctergenang = [], data_percagaktergenang = [],
                    data_percnormal = [], data_perckering = [], data_percrusakhilang = [], data_percrecorded = [], data_percunrecorded = [];
                var dataset = [];

                var goalcompletion_data = "";
                var goalcompletion_percentage;
                for (var i = 0; i < json.length; i++) {
                    labelname.push(json[i].LabelName);
                    data_recorded.push(json[i].Recorded);
                    data_unrecorded.push(json[i].NoData);
                    data_percbanjir.push(json[i].PercBanjir);
                    data_perctergenang.push(json[i].PercA);
                    data_percagaktergenang.push(json[i].PercB);
                    data_percnormal.push(json[i].PercC);
                    data_perckering.push(json[i].PercD);
                    data_percrusakhilang.push(json[i].PercRusakHilang);
                    data_percrecorded.push(json[i].PercRecorded);
                    data_percunrecorded.push(json[i].PercNoData);

                    //Goal Completion
                    goalcompletion_percentage = parseFloat(json[i].Recorded) / parseFloat(json[i].TotalData) * 100;

                    goalcompletion_data += '<div class="progress-group">';
                    goalcompletion_data += '<span class="progress-text">' + json[i].LabelName + '</span>';
                    goalcompletion_data += '<span class="progress-number">' + ($('#rbByPiezoMaster').is(":checked") ? json[i].Recorded : json[i].PercRecorded) + '/<b>' + ($('#rbByPiezoMaster').is(":checked") ? json[i].TotalData : 100) + '</b></span>';
                    goalcompletion_data += '<div class="progress sm">';
                    goalcompletion_data += '<div class="progress-bar progress-bar-aqua" style="width: ' + goalcompletion_percentage + '%"></div>';
                    goalcompletion_data += '</div></div>';
                }

                $('#goalcompletion').html(goalcompletion_data);

                if ($('#rbSummaryKondisi').is(":checked")) {
                    dataset.push({
                        label: 'Banjir',
                        backgroundColor: "#00BFFF", data: data_percbanjir
                    })

                    dataset.push({
                        label: 'Tergenang',
                        backgroundColor: "#FF8000", data: data_perctergenang
                    })

                    dataset.push({
                        label: 'Agak Tergenang',
                        backgroundColor: "#FFFF00", data: data_percagaktergenang
                    })

                    dataset.push({
                        label: 'Normal',
                        backgroundColor: "#008000", data: data_percnormal
                    })

                    dataset.push({
                        label: 'Kering',
                        backgroundColor: "#FF0000", data: data_perckering
                    })

                    dataset.push({
                        label: 'Rusak/Hilang',
                        backgroundColor: "#808080", data: data_percrusakhilang
                    })

                    dataset.push({
                        label: 'No Data',
                        backgroundColor: "#C0C0C0", data: data_percunrecorded
                    })

                }
                else {
                    dataset.push({
                        label: 'Recorded',
                        backgroundColor: "#00ff00", data: $('#rbByPiezoMaster').is(":checked") ? data_recorded : data_percrecorded
                    })

                    dataset.push({
                        label: 'No Data',
                        backgroundColor: "#ff0000", data: $('#rbByPiezoMaster').is(":checked") ? data_unrecorded : data_percunrecorded
                    })
                }

                barChartData = {
                    labels: labelname,
                    datasets: dataset
                };

                barChartConfig = {
                    type: 'bar',
                    data: barChartData,
                    options: {
                        tooltips: {
                            mode: 'label',
                            callbacks: {
                                label: function (tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
                                }
                            }
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                stacked: true,
                                barThickness: 20
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        }
                    }
                }

                myBar = new Chart(ctx, barChartConfig);

                document.getElementById("chartPiezometer").onclick = function (evt) {
                    var activePoints = myBar.getElementsAtEvent(evt);
                    var firstPoint = activePoints[0];
                    var secondPoint = activePoints[1];
                    var label = myBar.data.labels[firstPoint._index];
                    var value = myBar.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
                    var value_2 = myBar.data.datasets[secondPoint._datasetIndex].data[secondPoint._index];
                    //alert(label + ": " + value + " - " + value_2);

                    var company = (label.substring(0, 4) === "THIP" ? "PT.THIP" : ("PT." + label.replace("-", "")));
                    var keterangan = (label.substring(0, 4) === "THIP" ? label : "");
                    labelUnit = label;
                    selectedCompanyForSubChart = (label.substring(0, 4) === "THIP" ? "PT.THIP" : ("PT." + label.replace("-", "")));
                    selectedCompanyShortname = (label.substring(0, 4) === "THIP" ? label : "");
                    ShowSubChartCustomProgress(company, "", keterangan);
                    ShowChartDataPengukuran_Custom(company, "", flagValue, selectedCompanyShortname);

                    $("#btnCloseSubChart").show();
                    $('#mainchart').hide();
                    $('#subchart').show();
                };

            }
            else {
                alert("No Data For Progress");
            }

            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.statusText);
        }
    });
}

function ShowSubChartCustomProgress(companycode, estcode, keterangan) {
    $('#lblCompanyCode').text(companycode);

    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDataProgressForDashboard',
        data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", AdjustmentValue: "' + flagValue + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            if (json.length > 0) {
                $('#divSubChartPiezometer').html('');
                $('#divSubChartPiezometer').html('<canvas id="subChartPiezometer" style="height: 180px;"></canvas>')
                canvas = document.getElementById('subChartPiezometer');
                ctx = canvas.getContext('2d');

                $('#lblWeekName').text(json[0].WeekName);
                $('#lblDateRange').text("Range: " + json[0].DateRange);

                var labelname = [];
                var data_recorded = [], data_unrecorded = [], data_percbanjir = [], data_perctergenang = [], data_percagaktergenang = [],
                    data_percnormal = [], data_perckering = [], data_percrusakhilang = [], data_percrecorded = [], data_percunrecorded = [];
                var dataset = [];

                var goalcompletion_data = "";
                var goalcompletion_percentage;
                for (var i = 0; i < json.length; i++) {
                    if ((companycode == "PT.THIP" && json[i].Flag == keterangan) || keterangan == "") {
                        labelname.push(json[i].LabelName);
                        data_recorded.push(json[i].Recorded);
                        data_unrecorded.push(json[i].NoData);
                        data_percbanjir.push(json[i].PercBanjir);
                        data_perctergenang.push(json[i].PercA);
                        data_percagaktergenang.push(json[i].PercB);
                        data_percnormal.push(json[i].PercC);
                        data_perckering.push(json[i].PercD);
                        data_percrusakhilang.push(json[i].PercRusakHilang);
                        data_percrecorded.push(json[i].PercRecorded);
                        data_percunrecorded.push(json[i].PercNoData);

                        //Goal Completion
                        goalcompletion_percentage = parseFloat(json[i].Recorded) / parseFloat(json[i].TotalData) * 100;

                        goalcompletion_data += '<div class="progress-group">';
                        goalcompletion_data += '<span class="progress-text">' + json[i].LabelName + '</span>';
                        goalcompletion_data += '<span class="progress-number">' + ($('#rbByPiezoMaster').is(":checked") ? json[i].Recorded : json[i].PercRecorded) + '/<b>' + ($('#rbByPiezoMaster').is(":checked") ? json[i].TotalData : 100) + '</b></span>';
                        goalcompletion_data += '<div class="progress sm">';
                        goalcompletion_data += '<div class="progress-bar progress-bar-aqua" style="width: ' + goalcompletion_percentage + '%"></div>';
                        goalcompletion_data += '</div></div>';
                    }
                }

                $('#goalcompletion').html(goalcompletion_data);

                if ($('#rbSummaryKondisi').is(":checked")) {
                    dataset.push({
                        label: 'Banjir',
                        backgroundColor: "#00BFFF", data: data_percbanjir
                    })

                    dataset.push({
                        label: 'Tergenang',
                        backgroundColor: "#FF8000", data: data_perctergenang
                    })

                    dataset.push({
                        label: 'Agak Tergenang',
                        backgroundColor: "#FFFF00", data: data_percagaktergenang
                    })

                    dataset.push({
                        label: 'Normal',
                        backgroundColor: "#008000", data: data_percnormal
                    })

                    dataset.push({
                        label: 'Kering',
                        backgroundColor: "#FF0000", data: data_perckering
                    })

                    dataset.push({
                        label: 'Rusak/Hilang',
                        backgroundColor: "#808080", data: data_percrusakhilang
                    })

                    dataset.push({
                        label: 'No Data',
                        backgroundColor: "#C0C0C0", data: data_percunrecorded
                    })

                }
                else {
                    dataset.push({
                        label: 'Recorded',
                        backgroundColor: "#00ff00", data: $('#rbByPiezoMaster').is(":checked") ? data_recorded : data_percrecorded
                    })

                    dataset.push({
                        label: 'No Data',
                        backgroundColor: "#ff0000", data: $('#rbByPiezoMaster').is(":checked") ? data_unrecorded : data_percunrecorded
                    })
                }

                otherBarChartData = {
                    labels: labelname,
                    datasets: dataset
                };

                otherBarChartConfig = {
                    type: 'bar',
                    data: otherBarChartData,
                    options: {
                        tooltips: {
                            mode: 'label',
                            callbacks: {
                                label: function (tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
                                }
                            }
                        },
                        responsive: true,
                        scales: {
                            xAxes: [{
                                stacked: true,
                                barThickness: 20
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        }
                    }
                }

                myOtherBar = new Chart(ctx, otherBarChartConfig);


                $("#subChartPiezometer").click(function (evt) {
                    var activePoints = myOtherBar.getElementsAtEvent(evt);
                    var firstPoint = activePoints[0];
                    var secondPoint = activePoints[1];

                    var label = myOtherBar.data.labels[firstPoint._index];
                    var value = myOtherBar.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
                    var value_2 = myOtherBar.data.datasets[secondPoint._datasetIndex].data[secondPoint._index];
                    ShowPopupChart("", label.split(' - ')[0], flagValue, selectedCompanyShortname);
                })
                //document.getElementById("subChartPiezometer").onclick = function (evt) {
                //    var activePoints = myOtherBar.getElementsAtEvent(evt);
                //    var firstPoint = activePoints[0];
                //    var secondPoint = activePoints[1];
                //    var label = myOtherBar.data.labels[firstPoint._index];
                //    var value = myOtherBar.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
                //    var value_2 = myOtherBar.data.datasets[secondPoint._datasetIndex].data[secondPoint._index];

                //    $('#popupchart_content').html('tessssssssssssss');
                //    // reset modal if it isn't visible
                //    if (!($('.modal.in').length)) {
                //        $('.modal-dialog').css({
                //            top: 0,
                //            left: 100
                //        });
                //    }
                //    $('#popupchart').modal({
                //        backdrop: false,
                //        show: true
                //    });


                //    //$('#content-popover').html('<a id="test" tabindex="0" role="button" data-html="true" data-trigger="focus" title="<b>Chart</b>" data-content="<div>' + label + '</div>"></a>')

                //    //$('#test').popover('show');
                //};
            }
            else {
                alert("No Data For Progress");
            }

            $(".loading").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.statusText);
        }
    });
}

function ShowChartDataPengukuran(companycode, estcode, adjust, keterangan) {
    $('#divChartDataPengukuran').html('');
    $('#divChartDataPengukuran').html('<canvas id="chartDataPengukuran"></canvas>');

    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDataPengukuranForDashboard',
        data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", AdjustmentValue: "' + adjust + '", Keterangan : "' + keterangan + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            console.log(json);

            var chartTitle = "";
            if (companycode == "" && estcode == "") {
                chartTitle = "All Estate";
            }
            else if (companycode != "" && estcode == "") {
                chartTitle = $("#selectFilterCompany option:selected").text();
            }
            else if (estcode != "") {
                chartTitle = $("#selectFilterEstate option:selected").text();
            }

            $('#lblTitleWeek').html(json[0].WeekName);
            var chartDatactxDataPengukuran_Data = {
                labels: ['Banjir', 'Tergenang', 'Agak Tergenang', 'Normal', 'Kering', 'Rusak/Hilang', 'No Data'],
                datasets: [{
                    data: [json[0]["Banjir"], json[0]["A"], json[0]["B"], json[0]["C"], json[0]["D"], json[0]["Rusak / Hilang"], json[0]["NoData"]],
                    backgroundColor: [
                        '#00BFFF',
                        '#FF8000',
                        '#FFFF00',
                        '#008000',
                        '#FF0000',
                        '#808080',
                        '#C0C0C0'
                    ]
                }]
            };

            var ctxDataPengukuran = document.getElementById('chartDataPengukuran').getContext('2d');
            myChartDataPengukuran = new Chart(ctxDataPengukuran, {
                type: 'doughnut',
                data: chartDatactxDataPengukuran_Data,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: "Pengukuran Piezometer " + chartTitle
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.statusText);
        }
    });
}

function ShowChartDataPengukuran_Custom(companycode, estcode, adjust, keterangan) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDataPengukuranForDashboard',
        data: '{CompanyCode: "' + companycode + '", EstCode: "' + estcode + '", AdjustmentValue: "' + adjust + '", Keterangan: "' + keterangan + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            if (json.length > 0) {
                var chartTitle = "";
                //if (companycode == "" && estcode == "") {
                //    chartTitle = "All Estate";
                //}
                //else if (companycode != "" && estcode == "") {
                //    chartTitle = $("#selectFilterCompany option:selected").text();
                //}
                //else if (estcode != "") {
                //    chartTitle = $("#selectFilterEstate option:selected").text();
                //}
                chartTitle = labelUnit;
                $('#lblTitleWeek').html(json[0].WeekName);
                myChartDataPengukuran.data.datasets[0].data = [json[0]["Banjir"], json[0]["A"], json[0]["B"], json[0]["C"], json[0]["D"], json[0]["Rusak / Hilang"], json[0]["NoData"]];
                myChartDataPengukuran.update();
                myChartDataPengukuran.options.title.text = "Pengukuran Piezometer " + chartTitle;
                myChartDataPengukuran.update();
            }
            else {
                if (adjust < 0) flagValue = flagValue + 1;
                else flagValue = flagValue - 1;
                alert("No Data For Data Pengukuran");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.statusText);
        }
    });
}

function ShowChartSumberData() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDataSumberData',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            $('#divChartSumberData').html('');
            $('#divChartSumberData').html('<canvas id="chartSumberData"></canvas>');

            var chartSumberData_Data = {
                labels: [json[0].DataSource, json[1].DataSource],
                datasets: [{
                    data: [json[0].Jumlah, json[1].Jumlah],
                    backgroundColor: [
                        'green',
                        'blue'
                    ]
                }]
            };

            var ctxSumberData = document.getElementById('chartSumberData').getContext('2d');
            var myChartSumberData = new Chart(ctxSumberData, {
                type: 'doughnut',
                data: chartSumberData_Data,
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: "Sumber Data Piezometer"
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.statusText);
        }
    })
}

function ListCompany() {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListCompany',
        data: '{}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataSubText = response.d[2].split(";");
            var dataLength = response.d[0].split(";").length;


            $('#selectFilterCompany').find('option').remove();
            $("#selectFilterCompany").append($('<option>', {
                value: "",
                text: "All Company"
            }));

            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterCompany").append($('<option>', {
                    value: dataValue[i],
                    text: dataSubText[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterCompany").val("").change();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListCompany : " + xhr.statusText);
        }
    });
}

function ListEstate(companycode) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/ListEstate',
        data: '{CompanyCode: "' + companycode + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var dataValue = response.d[0].split(";");
            var dataText = response.d[1].split(";");
            var dataLength = response.d[0].split(";").length;

            $('#selectFilterEstate').find('option').remove();
            $("#selectFilterEstate").append($('<option>', {
                value: "",
                text: "All Estate"
            }));
            for (var i = 0; i < dataLength; i++) {
                $("#selectFilterEstate").append($('<option>', {
                    value: dataValue[i],
                    text: dataValue[i] + " - " + dataText[i]
                }));
            }

            $("#selectFilterEstate").val("");
            $('.selectpicker').selectpicker('refresh');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function ListEstate : " + xhr.statusText);
        }
    });
}

function ShowPopupChart(company, estate, adjust, keterangan) {
    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetDataPengukuranForDashboard',
        data: '{CompanyCode: "' + company + '", EstCode: "' + estate + '", AdjustmentValue: "' + adjust + '", Keterangan : "' + keterangan + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);

            $("#dialog").dialog({
                open: function (event, ui) {
                    $(".ui-widget-overlay").bind("click", function (event, ui) {
                        $("#dialog").dialog("close");
                    });
                },
                closeOnEscape: true,
                draggable: true,
                resizable: false,
                title: "Data Pengukuran " + estate,
                width: 700,
                modal: false,
                show: 500
            });
            $(".ui-widget-overlay").css({ "background-color": "#111111" });

            //var chartDatactxDataPengukuran_Data = {
            //    labels: ['Banjir', 'Tergenang', 'Agak Tergenang', 'Normal', 'Kering', 'Rusak/Hilang', 'No Data'],
            //    datasets: [{
            //        data: [json[0]["Banjir"], json[0]["A"], json[0]["B"], json[0]["C"], json[0]["D"], json[0]["Rusak / Hilang"], json[0]["NoData"]],
            //        backgroundColor: [
            //            '#00BFFF',
            //            '#FF8000',
            //            '#FFFF00',
            //            '#008000',
            //            '#FF0000',
            //            '#808080',
            //            '#C0C0C0'
            //        ]
            //    }]
            //};

            //var ctxDataPengukuran = document.getElementById('chartDataPengukuran_Popup').getContext('2d');
            //myChartDataPengukuran = new Chart(ctxDataPengukuran, {
            //    type: 'doughnut',
            //    data: chartDatactxDataPengukuran_Data,
            //    options: {
            //        responsive: true,
            //        legend: {
            //            position: 'top',
            //        },
            //        title: {
            //            display: true,
            //            text: "Pengukuran Piezometer " + ""
            //        },
            //        animation: {
            //            animateScale: true,
            //            animateRotate: true
            //        }
            //    }
            //});

            var options = {
                animationEnabled: true,
                data: [{
                    type: "doughnut",
                    startAngle: 60,
                    indexLabelFontSize: 17,
                    indexLabel: "{label} - {y} (#percent%)",
                    toolTipContent: "<b>{label}:</b> {y} (#percent%)",
                    dataPoints: [
                        { y: json[0]["Banjir"], label: "Banjir", color: '#00BFFF' },
                        { y: json[0]["A"], label: "Tergenang", color: '#FF8000' },
                        { y: json[0]["B"], label: "Agak Tergenang", color: '#FFFF00' },
                        { y: json[0]["C"], label: "Normal", color: '#008000' },
                        { y: json[0]["D"], label: "Kering", color: '#FF0000' },
                        { y: json[0]["Rusak / Hilang"], label: "Rusak/Hilang", color: '#808080' },
                        { y: json[0]["NoData"], label: "No Data", color: '#C0C0C0' }
                    ]
                }]
            };
            $("#chartDataPengukuran_Popup").CanvasJSChart(options);

            $("#dialog").dialog("open");

            $('#btnGoToMap').unbind('click');
            $("#btnGoToMap").click(function (evt) {
                window.open('PZO_Map.aspx?typeModule=OLM&Company=' + $('#lblCompanyCode').text() +'&Estate=' + estate + '&IDWEEK=' + $('#lblWeekName').text(), '_blank'); 
            })

            $('#btnWeeklyReport').unbind('click');
            $("#btnWeeklyReport").click(function (evt) {
                var $this = $(this);
                $this.html("<i class='fa fa-spinner fa-spin'></i> Processing");

                $.ajax({
                    type: 'POST',
                    url: '../Service/MapService.asmx/GenerateWeeklyReport',
                    data: '{EstCode: "' + estate + '", WeekName: "' + $('#lblWeekName').text() + '"}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        $this.html("Weekly Report");
                        
						var content = response.d;
                        var request = new XMLHttpRequest();
                        request.open('POST', '../Handler/GetReportHandler.ashx?reporttype=weekly&filename=' + response.d, true);
                        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                        request.responseType = 'blob';

                        request.onload = function () {
                            // Only handle status code 200
                            if (request.status === 200) {
                                // Try to find out the filename from the content disposition `filename` value
                                var disposition = request.getResponseHeader('content-disposition');
                                var matches = /"([^"]*)"/.exec(disposition);
                                var filename = (matches != null && matches[1] ? matches[1] : response.d);

                                // The actual download
                                var blob = new Blob([request.response], { type: 'application/pdf' });
                                var link = document.createElement('a');
                                link.href = window.URL.createObjectURL(blob);
                                link.download = filename;

                                document.body.appendChild(link);

                                link.click();

                                document.body.removeChild(link);
                            }
							else {
								alert('File not found');
							}

                            // some error handling should be done here...
                        };

                        request.send('content=' + content);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.statusText);
                    }
                })
            })
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.statusText);
        }
    });


    ////SHOW DIALOG
    //$("#dialog").dialog({
    //    autoOpen: false
    //}).position({
    //    my: 'left',
    //    at: 'right',
    //    of: $(this)
    //});

    //$("#dialog").dialog("open");
}

//////////////////////////////////////////////////////////
///////////////////// EVENT HANDLER //////////////////////
//////////////////////////////////////////////////////////

$("#selectFilterCompany").change(function () {
    ListEstate($("#selectFilterCompany").val());
})

$("#btnSubmitFilter").click(function () {
    selectedCompany = $("#selectFilterCompany").val();
    selectedEstate = $("#selectFilterEstate").val();
    ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
    ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, "");
})

$("#btnPrevWeek").click(function () {
    flagValue = flagValue - 1;
    if ($('#mainchart').is(":visible")) {
        ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
        ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname);
    } else if ($('#subchart').is(":visible")) {
        ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
        ShowChartDataPengukuran_Custom(selectedCompanyForSubChart, selectedEstate, flagValue, selectedCompanyShortname);
    }

    //ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname);
})

$("#btnNextWeek").click(function () {
    flagValue = flagValue + 1;
    if ($('#mainchart').is(":visible")) {
        ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
        ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname);
    } else if ($('#subchart').is(":visible")) {
        ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
        ShowChartDataPengukuran_Custom(selectedCompanyForSubChart, selectedEstate, flagValue, selectedCompanyShortname);
    }

    //ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
    //ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname);
    //ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
})

$("#btnThisWeek").click(function () {
    flagValue = 0;
    if ($('#mainchart').is(":visible")) {
        ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
        ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname)
    } else if ($('#subchart').is(":visible")) {
        ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
        ShowChartDataPengukuran_Custom(selectedCompanyForSubChart, selectedEstate, flagValue, selectedCompanyShortname)
    }

    //ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
    //ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname)
    //ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
})

$('input').on('ifChecked', function (event) {
    if ($('#mainchart').is(":visible")) {
        ShowChartCustomProgress(selectedCompany, selectedEstate, flagValue);
        ShowChartDataPengukuran_Custom(selectedCompany, selectedEstate, flagValue, selectedCompanyShortname);
    } else if ($('#subchart').is(":visible")) {
        ShowSubChartCustomProgress(selectedCompanyForSubChart, "", selectedCompanyShortname);
        ShowChartDataPengukuran_Custom(selectedCompanyForSubChart, selectedEstate, flagValue, selectedCompanyShortname);
    }
});