﻿var zona, dataSettingZona, dataChartZona, objParam, selectedRambu, selectedCurahHujan, selectedRambuMeasure, rambuSelected, chartData, tmasTarget;
var statusBisaDownload = 0;
var contentRender = "";
var global_datereport;

var IOT_companycode = "";
var IOT_estcode = "";
var IOT_deviceid = "";
var IOT_startingdate = "";
var IOT_endingdate = "";

var global_listdevice;
var global_dailyrainfall;
var global_summarydailyrainfall;
var global_monthlyrainfall;
var global_listbulan = [];
var global_listtahun;
var global_dailytmat;
var global_monthlytmat;
var global_bulanberjalan;
var global_holiday;
var global_listperiod;
var global_startdate;
var global_enddate;
var global_type;
var global_weektemp;
var global_currentdays;
var global_tmattitle;
var global_tergenang, global_agaktergenang, global_normal, global_agakkering, global_kering;
var global_waterindicator;


var local_companyCode, local_wmArea, local_idZona, local_pieRecordIDArr, local_awlDeviceId, local_WMArea2, local_namaGraph, local_CH, local_HH, local_colorField, local_weekName;

var local_CH1 = 0
var local_HH1 = 0

//////////////////////////////////////////////////////////
///////////////// REGION ONLOAD ///////////////////
//////////////////////////////////////////////////////////
$(function () {
    $(".loading").show();
    setTimeout(function () {
        handler();
        //$(".loading").hide();
    }, 1);



    statusBisaDownload = 0;
    selectedRambu = [];
    dataSettingZona = [];
    dataChartZona = [];
    rambuSelected = [];
    chartData = [];
    chartCH = {};
    selectedCurahHujan = "";
    objParam = getAllUrlParams(window.location.href);
    console.log(objParam);
    nilaiMaxCH = 3;
    elvTanah = null;

    //if (objParam.idzona != undefined && objParam.ts != undefined) {
    //    setupcekDownloadChartImageZona();
    //}

    //if (objParam.idzona != undefined && objParam.ts != undefined) {

    //    getLabelChart();
    //    getChartFromZona();
    //}
    if (objParam.weekid != undefined) {
        getWeekID();
    }
    else {
        $(".loading").hide();
    }

    
    //if (objParam.zonaanalysisid != undefined ) {
    //    getSettingZona();
    //    //if (local_idZona != undefined) {

    //    //    getLabelChart();
    //    //    getChartFromZona();
    //    //}
    //}
});

function handler() {

    $("#printcharting").click(function () {
        setTimeout(function () {
            //html2canvas(document.querySelector("#reportDetail1"), { scale: 1 }).then(canvas => {

            //    var dataURL = canvas.toDataURL();
            //    var width = canvas.width;
            //    var printWindow = window.open("");
            //    console.log(canvas);
            //    console.log(dataURL);
            //    console.log(printWindow);
            //    $(printWindow.document.body)
            //        .html("<img id='Image' src=" + dataURL + " style='" + width + "'></img>")
            //        .ready(function () {
            //            printWindow.focus();

            //        });
            //    setTimeout(function () {
            //        //printWindow.document.title = window.parent.document.title = global_datereport.slice(2, 4) + global_datereport.slice(5, 7) + global_datereport.slice(8, 10) + `_IoT1_Laporan ARS 7 Hari Terakhir_` + Math.round((new Date()).getTime() / 1000);
            //        printWindow.document.title = window.parent.document.title = `_IoT1_Laporan ARS 7 Hari Terakhir_` + Math.round((new Date()).getTime() / 1000);
            //        printWindow.print();
            //    }, 1000);
            //});

            var htmlSource = $('#reportDetail1')[0];
            html2canvas(htmlSource, { scrollX: -window.scrollX, scrollY: -window.scrollY })
                .then(function (canvas) {
                    //var img = canvas.toDataURL();
                    //window.open(img);
                    var dataURL = canvas.toDataURL();
                    var width = canvas.width;
                    var printWindow = window.open("");
                    //$(printWindow.document.body).innerHTML = "";
                    //$(printWindow.document).remove();
                    console.log(canvas);
                    console.log(dataURL);
                    console.log(printWindow);
                    $(printWindow.document.body)
                        .html("");

                    $(printWindow.document.body)
                        .html("<img id='Image' src=" + dataURL + " style='" + width + "'></img>")
                        .ready(function () {
                            printWindow.focus();
                            //printWindow.print();


                        });
                    setTimeout(function () {
                        //printWindow.document.title = window.parent.document.title = global_datereport.slice(2, 4) + global_datereport.slice(5, 7) + global_datereport.slice(8, 10) + `_IoT1_Laporan ARS 7 Hari Terakhir_` + Math.round((new Date()).getTime() / 1000);
                        printWindow.document.title = window.parent.document.title = `_IoT1_Laporan ARS 7 Hari Terakhir_` + Math.round((new Date()).getTime() / 1000);
                        printWindow.print();
                    }, 1000);


                });



        }, 1000);

    });



    $("#savepdf").click(function () {
        setTimeout(function () {
            html2canvas(document.querySelector("#reportDetail1")).then(canvas => {

                var dataURL = canvas.toDataURL();
                //var width = canvas.width;
                //var printWindow = window.open("");

                console.log(canvas);
                console.log(dataURL);
                //console.log(printWindow);
                //$(printWindow.document.body)
                //    .html("<img id='Image' src=" + dataURL + " style='" + width + "'></img>")
                //    .ready(function () {
                //        printWindow.focus();

                //    });
                setTimeout(function () {
                    var imgWidth = 195;
                    var pageHeight = 287;
                    var imgHeight = canvas.height * imgWidth / canvas.width;
                    var heightLeft = imgHeight;

                    console.log(imgHeight, heightLeft);

                    var doc = new jsPDF('p', 'mm', 'a4', true);
                    var position = 10;

                    margins = {
                        top: 6,
                        bottom: 6,
                        left: 6,
                        width: 5
                    };

                    doc.addImage(dataURL, 'PNG', margins.left, margins.top, imgWidth, imgHeight + 30, undefined, 'FAST');
                    heightLeft -= pageHeight;



                    console.log(heightLeft);
                    let i = 0
                    while (heightLeft >= 0) {
                        i++
                        console.log(i);
                        position = heightLeft - imgHeight - 3;
                        console.log(position);
                        console.log(heightLeft);
                        doc.addPage();
                        //doc.text("ANJENG BANGET", 5, position);
                        if (i == 1) {
                            doc.addImage(dataURL, 'PNG', margins.left, position + margins.top + i / 6, imgWidth, imgHeight + 20, undefined, 'FAST')

                        } else if (i == 2) {
                            doc.addImage(dataURL, 'PNG', margins.left, position + margins.top + i / 6, imgWidth, imgHeight + 20, undefined, 'FAST')
                        }

                        heightLeft -= pageHeight;
                    }

                    window.open(doc.output('bloburl'));

                    //var rawdata = doc.output();

                    //var len = rawdata.length,
                    //    ab = new ArrayBuffer(len),
                    //    u8 = new Uint8Array(ab);

                    //while (len--) u8[len] = rawdata.charCodeAt(len);

                    //var blob = new Blob([ab], { type: "application/pdf" });

                    //saveAs(blob, global_datereport.slice(2, 4) + global_datereport.slice(5, 7) + global_datereport.slice(8, 10) + `_Rpt1_Laporan ARS 7 Hari Terakhir_` + Math.round((new Date()).getTime() / 1000));

                }, 1000);
            });
        }, 1000);
    });
}

//////////////////////////////////////////////////////////
///////////////// REGION CUSTOM METHOD ///////////////////
//////////////////////////////////////////////////////////
function getAllUrlParams(url) {

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];

        // split our query string into its component parts
        var arr = queryString.split('&');

        for (var i = 0; i < arr.length; i++) {
            // separate the keys and the values
            var a = arr[i].split('=');

            // set parameter name and value (use 'true' if empty)
            var paramName = a[0];
            var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

            // (optional) keep case consistent
            paramName = paramName.toLowerCase();
            if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

            // if the paramName ends with square brackets, e.g. colors[] or colors[2]
            if (paramName.match(/\[(\d+)?\]$/)) {

                // create key if it doesn't exist
                var key = paramName.replace(/\[(\d+)?\]/, '');
                if (!obj[key]) obj[key] = [];

                // if it's an indexed array e.g. colors[2]
                if (paramName.match(/\[\d+\]$/)) {
                    // get the index value and add the entry at the appropriate position
                    var index = /\[(\d+)\]/.exec(paramName)[1];
                    obj[key][index] = paramValue;
                } else {
                    // otherwise add the value to the end of the array
                    obj[key].push(paramValue);
                }
            } else {
                // we're dealing with a string
                if (!obj[paramName]) {
                    // if it doesn't exist, create property
                    obj[paramName] = paramValue;
                } else if (obj[paramName] && typeof obj[paramName] === 'string') {
                    // if property does exist and it's a string, convert it to an array
                    obj[paramName] = [obj[paramName]];
                    obj[paramName].push(paramValue);
                } else {
                    // otherwise add the property
                    obj[paramName].push(paramValue);
                }
            }
        }
    }

    return obj;
}



function getLabelChart() {
    labelChart = [];
    for (var i = 30; i >= 0; i--) {
        var d = new Date();
        d.setDate(d.getDate() - i);
        labelChart.push(dateFormat(d));
    }
}

function addDataSet(idx) {
    var charRambu = [];
    var rambuM = selectedRambuMeasure[idx];

    if (idx == 0) {
        elvTanah = parseFloat(rambuM["elevasiTanah"]);
    }
    for (var i = 0; i < labelChart.length; i++) {
        var nilai = null;
        var measure = JSON.parse(rambuM["measure"]);
        for (var j = 0; j < measure.length; j++) {
            if (labelChart[i] == dateFormat(new Date(measure[j]["measurementDate"]))) {
                nilai = parseFloat(measure[j]["Nilai"]);
                break;
            }
        }


        if (nilai != null) {
            if (nilai > maxY) {
                maxY = nilai;
            }

            if (minY == null) {
                minY = nilai;
            } else if (nilai < minY) {
                minY = nilai;
            }
        }
        var sdate = labelChart[i].split('/');
        charRambu.push({
            x: new Date(sdate[2], parseInt(sdate[1]) - 1, sdate[0]),
            y: nilai
        });
    }
    //console.info(' ini chart'+ charRambu);
    var markerSymbol = "circle";
    if (rambuM["frequency"] == 2) {
        markerSymbol = "square";
    } else if (rambuM["frequency"] == 3) {
        markerSymbol = "triangle";
    }

    var newSeries = {
        name: rambuM["stationId"],
        legendText: rambuM["stationName"],
        color: rambuM["color"],
        markerColor: rambuM["color"],
        markerType: markerSymbol,
        connectNullData: true,
        nullDataLineDashType: "solid",
        type: "line",
        click: function (e) {
            var measurementId = 0;
            for (var i = 0; i < selectedRambuMeasure.length; i++) {
                if (selectedRambuMeasure[i]["stationId"] == e.dataSeries.name) {
                    var measure = JSON.parse(selectedRambuMeasure[i]["measure"]);

                    for (var j = 0; j < measure.length; j++) {
                        if (dateFormat(new Date(measure[j]["measurementDate"])) == dateFormat(e.dataPoint.x)) {
                            measurementId = measure[j]["measurementId"];
                            break;
                        }
                    }
                }
            }
            editMeasure(measurementId);
            //alert(e.dataSeries.name + ", "+measurementId);
            //alert(e.dataSeries.name + ", dataPoint { x:" + e.dataPoint.label + ", y: " + e.dataPoint.y + " }");
        },
        markerSize: 8,
        showInLegend: "true",
        toolTipContent: "{legendText}<br/>{x}, <strong>{y}</strong> m",
        dataPoints: charRambu
    };


    //console.info("charRambu", newSeries);

    chartData.push(newSeries);

    idx++;
    if (idx < selectedRambuMeasure.length) {
        addDataSet(idx)
    } else {
        if (selectedCurahHujan != "") {
            getValueCurahHujan();
        } else {
            setupChart();
        }
    }
}

function addDataCurahHujan(rata2CurahHujan) {
    var charRataCH = [];
    nilaiMaxCH = 3;
    for (var i = 0; i < labelChart.length; i++) {
        var nilai = null;
        for (var j = 0; j < rata2CurahHujan.length; j++) {
            if (labelChart[i] == dateFormat(new Date(parseInt(rata2CurahHujan[j]["date"].replace('/Date(', '').replace(')/', ''))))) {
                nilai = Math.round(rata2CurahHujan[j]["sumRainInches"]);
                break;
            }
        }
        var xHH = 0
        if (Math.round(rata2CurahHujan[j]["sumRainInches"]) > 0 ) {
            xHH = 1;
        }

        local_CH1 += Math.round(rata2CurahHujan[j]["sumRainInches"]);
        local_HH1 += xHH ;

        if (nilai != null) {
            if (nilaiMaxCH < nilai) {
                nilaiMaxCH = nilai;
            }
        }
        var sdate = labelChart[i].split('/');
        charRataCH.push({
            x: new Date(sdate[2], parseInt(sdate[1]) - 1, sdate[0]),
            y: nilai
        });
    }
    console.info("charRataCH", charRataCH);
    chartCH = {};
    chartCH["name"] = "Rata Rata Curah Hujan";
    chartCH["type"] = "column";
    chartCH["color"] = "orange";
    chartCH["showInLegend"] = "true";
    chartCH["toolTipContent"] = "{name}<br/>{x}, <strong>{y}</strong> mm";
    chartCH["axisYType"] = "secondary";
    chartCH["dataPoints"] = charRataCH;
    chartCH["fillOpacity"] = .6;
    setupChart();


    $("#kontenCH1").html(local_CH1);
    $("#kontenHH1").html(local_HH1);

}

function setupcekDownloadChartImageZona() {
    ////var generateWMCode = objParam.idzona.toUpperCase();
    //var generateWMCode = objParam.idzona.toUpperCase();
    //var generateFolder = 'DOWNLOAD';
    //var generateFileName = 'WLRCaptureZona_' + objParam.idzona.toUpperCase() + "_" + objParam.ts.toUpperCase();


    //cekDownloadChartImageZona(generateWMCode, generateFolder, generateFileName);

    //if (statusBisaDownload == "1") {
    //    document.getElementById("divCounting").innerHTML = "Please Wait 10 seconds to generate";
    //} else {
    //    //document.getElementById("divCounting").innerHTML = "Generate has been downloaded ";
    //}
    ////alert("function statusBisaDownload : " + statusBisaDownload);
}




function setupChart() {
    console.info("setupChart");
    getLabelChart();

    var dataChart = [];

    var dataChartNote = [];
    //var dataNote = tableNote.data();
    //var dataColorSetting = tableSetColor.data();
    // console.log('data color setting = ' + JSON.stringify(dataColorSetting));

    //if (dataNote.length > 0) {
    //    for (var i = 0; i < labelChart.length; i++) {
    //        var nilai = null;
    //        var note = null;
    //        for (var j = 0; j < dataNote.length; j++) {
    //            var rowNote = tableNote.row(j).data();
    //            if ((labelChart[i] == rowNote["tanggal"]) && (rowNote["statusId"] == '1')) {
    //                //nilai = (nilaiMaxCH * 3);
    //                nilai = ((nilaiMaxCH / 8) * 2) + nilaiMaxCH;
    //                note = rowNote["note"];
    //                break;
    //            }
    //        }

    //        var sdate = labelChart[i].split('/');
    //        dataChartNote.push({
    //            x: new Date(sdate[2], parseInt(sdate[1]) - 1, sdate[0]),
    //            y: nilai,
    //            name: note
    //        });
    //    }
    //    console.info("chartNote", dataChartNote);
    //    chartNote = {};
    //    chartNote["type"] = "scatter";
    //    chartNote["legendText"] = "Note Graph";
    //    chartNote["color"] = "red";
    //    chartNote["showInLegend"] = "true";
    //    chartNote["toolTipContent"] = "<strong>{x}</strong><br/>{name}";
    //    chartNote["axisYType"] = "secondary";
    //    chartNote["dataPoints"] = dataChartNote;

    //    dataChart.push(chartNote);
    //}

    if (chartCH != undefined) {
        dataChart.push(chartCH);
    }

    for (var i = 0; i < chartData.length; i++) {
        dataChart.push(chartData[i]);
    }
    var titikY = {
        title: "Tinggi Muka Air Saluran",
        titleFontSize: 10,
        labelFontSize: 10
    }


    if (elvTanah != null) {
        var elvTanah2 = elvTanah - 0.2;
        var elvTanah4 = elvTanah - 0.4;
        var elvTanah6 = elvTanah - 0.6;
        var stripLine = [{
            value: elvTanah,
            label: "0"
        }, {
            value: elvTanah2,
            label: "0.2"
        }, {
            value: elvTanah4,
            label: "0.4"
        }, {
            value: elvTanah6,
            label: "0.6"
        }];

        console.log('hasil penjumlahan = ' + (parseFloat(tmasTarget) + (parseFloat(0.05))).toFixed(2));
        if (tmasTarget != "") {
            var objDefault = {
                label: "TMAS Target",
                startValue: parseFloat(tmasTarget - 0.05),
                endValue: parseFloat(tmasTarget + 0.05),
                color: "#b3f2c2",
                labelAlign: "near"
            }
        }

        stripLine.push(objDefault)
        //if (dataColorSetting.length > 0) {

        //    //console.log('data color setting kut kut kut = ' + JSON.stringify(dataColorSetting));            
        //    for (var i = 0; i < dataColorSetting.length; i++) {
        //        var rowColor = tableSetColor.row(i).data();
        //        if (dataColorSetting[i]["statusId"] == "1") {
        //            var obj = {
        //                label: rowColor["namaKlasifikasi"],
        //                startValue: rowColor["nilaiMinimum"],
        //                endValue: rowColor["nilaiMaksimum"],
        //                color: rowColor["codeWarna"],
        //                labelAlign: "near"
        //            }

        //            stripLine.push(obj);
        //        }
        //    }

        //}
        console.log('object fix stripline = ' + JSON.stringify(stripLine));
        titikY["stripLines"] = stripLine;
    }

    //titikY["maximum"] = maxY + 0.1;
    //titikY["minimum"] = minY - 0.1;
    titikY["includeZero"] = false;
    console.info("titikY", JSON.stringify(titikY));


    //if ($('#selectFilterZona').val() != 0) {
    //    textTitle = $('#selectFilterZona option:selected').text();
    //}
    //alert("Folder : " + document.URL);
    //alert("FileName : " + objParam.idzona.toUpperCase() + "_" + objParam.ts.toUpperCase());

    //var generateWMCode = objParam.idzona.toUpperCase();
    //var generateFolder = 'DOWNLOAD';
    //var generateFileName = 'WLRCaptureZona_' + objParam.idzona.toUpperCase() + "_" + objParam.ts.toUpperCase();


    //cekDownloadChartImageZona(generateWMCode, generateFolder, generateFileName);

    //if (statusBisaDownload == "1") {
    //    document.getElementById("divCounting").innerHTML = "Please Wait 10 seconds to generate";
    //} else {
    //    document.getElementById("divCounting").innerHTML = "Generate has been downloaded ";
    //}
    //alert("function statusBisaDownload : " + statusBisaDownload);

    //insertLogData(generateWMCode, generateFolder, generateFileName);

    //alert  XtraReport.FromFile(@"" + context.Server.MapPath("..") + "\\RepX\\" + fileRepX + ".repx", true);


    //var container = document.getElementById("chart");
    //container.innerHTML += '<div id="divChart" style="width:800px; height:400px;"  ></div>'


    //container.innerHTML += '<div style="width: 45%">'
    //container.innerHTML += '    <div style="background-color: aqua; width: 100%; height: 400px">'
    //container.innerHTML += '        <div id="divChart" style="height: 100%; width: 100%"></div>'
    //container.innerHTML += '    </div>'
    //container.innerHTML += '</div>'

    //container.innerHTML += '<div id="divChart" ></div>'

    //console.log(j);
    //if ((j + 1) % 4 == 0) {
    //    container.innerHTML += '<div id="chart' + j + '" style="height: 61vh; width: 100%; margin-top:1.3% ; "></div>';
    //} else {
    //    container.innerHTML += '<div id="chart' + j + '" style="height: 61vh; width: 100%; margin-top:1.3% "></div>';
    //}

    var generateFileName = "";


    chart = new CanvasJS.Chart("divChart", {
        title: {
            text: textTitle,
            fontWeight: "bolder",
            fontFamily: "Calibri",
            fontSize: 15,
            padding: 2
        },
        //exportEnabled: true,
        exportFileName: generateFileName,
        zoomEnabled: true,
        panEnabled: true,
        axisXType: "secondary",
        animationEnabled: true,
        axisY: titikY,
        axisY2: {
            maximum: ((nilaiMaxCH / 8) * 2) + nilaiMaxCH,
            title: "Curah Hujan",
            titleFontSize: 10,
            labelFontSize: 10,
        },
        axisX: {
            valueFormatString: "DD-MMM",
            title: "Tanggal",
            titleFontSize: 10,
            labelFontSize: 10
        },
        legend: {
            fontSize: 10,
            verticalAlign: "bottom",
            horizontalAlign: "center"
        },
        data: dataChart
    });

    chart.render();


    //container.innerHTML += '<div style="width: 45%">'
    //container.innerHTML += '    <div style="background-color: aqua; width: 100%; height: 400px">'
    //container.innerHTML += '        <div id="divChart2a" style="height: 100%; width: 100%"></div>'
    //container.innerHTML += '    </div>'
    //container.innerHTML += '</div>'


    //container.innerHTML += '<div id="divChart2a" style="width:400px; height:400px;"  ></div>'


    


    //let element = document.getElementById("trChart2a");
    //let hidden = element.getAttribute("hidden");


    //element.removeAttribute("hidden");
    //element.setAttribute("hidden", "hidden");

    //document.getElementById("trChart2a").style.display = '';
    //document.getElementById("trChart2c").style.display = '';

    document.getElementById("trChart2").style.display = 'none';
    document.getElementById("trChart2a").style.display = 'none';
    document.getElementById("trChart2c").style.display = 'none';



    if (local_pieRecordIDArr.trim().split(",").length > 0) {
        var rslocal_pieRecordIDArr = local_pieRecordIDArr.trim().split(",");
        for (var j = 0; j < rslocal_pieRecordIDArr.length; j++) {
            document.getElementById("trChart2").style.display = '';
            //allCH.push(rsplitCH[j].trim())
            ShowDetailPzo(rslocal_pieRecordIDArr[j].trim(), j + 1);
            if (j + 1 == 1) {
                document.getElementById("trChart2a").style.display = '';
            }
            if (j + 1 == 3) {
                document.getElementById("trChart2c").style.display = '';
            }
        }
    }

    //ShowDetailPzo('MER-41', 1);
    //ShowDetailPzo('MER-42', 2);
    //ShowDetailPzo('MER-43', 3);
    //ShowDetailPzo('MER-44', 4);


    //IOT_companycode = "PT.THIP";
    //IOT_estcode = "MER";
    //IOT_deviceid = "MT03-GMPL011WL-GWB-10485760";
    //IOT_startingdate = "2023-08-06"; /Date(1694278800000)/
    //IOT_endingdate = "2023-09-10";


    IOT_companycode = local_companyCode;
    IOT_estcode = local_companyCode ;
    IOT_deviceid = local_awlDeviceId;
    //IOT_startingdate = "2023-08-06";
    //IOT_endingdate = "2023-09-10";


    
    document.getElementById("trChart3").style.display = 'none';
    document.getElementById("trChart3a").style.display = 'none';
    if (IOT_deviceid != undefined && IOT_deviceid.trim() != "") {
        document.getElementById("trChart3").style.display = '';
        document.getElementById("trChart3a").style.display = '';
        IOT_GetWaterDepthIndicator();
        IOT_GetMonthlyTMAT();
    }
    $(".loading").hide();
    //chart2a = new CanvasJS.Chart("divChart2a", {
    //    title: {
    //        text: textTitle,
    //        fontWeight: "bolder",
    //        fontFamily: "Calibri",
    //        fontSize: 21,
    //        padding: 10
    //    },
    //    exportEnabled: true,
    //    exportFileName: generateFileName,
    //    zoomEnabled: true,
    //    panEnabled: true,
    //    axisXType: "secondary",
    //    animationEnabled: true,
    //    axisY: titikY,
    //    axisY2: {
    //        maximum: ((nilaiMaxCH / 8) * 2) + nilaiMaxCH,
    //        title: "Curah Hujan",
    //        titleFontSize: 15,
    //        labelFontSize: 13,
    //    },
    //    axisX: {
    //        valueFormatString: "DD-MMM",
    //        title: "Tanggal",
    //        titleFontSize: 15,
    //        labelFontSize: 13
    //    },
    //    legend: {
    //        fontSize: 11,
    //        verticalAlign: "bottom",
    //        horizontalAlign: "center"
    //    },
    //    data: dataChart
    //});

    //chart2a.render();



    //chart2b = new CanvasJS.Chart("divChart2b", {
    //    title: {
    //        text: textTitle,
    //        fontWeight: "bolder",
    //        fontFamily: "Calibri",
    //        fontSize: 21,
    //        padding: 10
    //    },
    //    exportEnabled: true,
    //    exportFileName: generateFileName,
    //    zoomEnabled: true,
    //    panEnabled: true,
    //    axisXType: "secondary",
    //    animationEnabled: true,
    //    axisY: titikY,
    //    axisY2: {
    //        maximum: ((nilaiMaxCH / 8) * 2) + nilaiMaxCH,
    //        title: "Curah Hujan",
    //        titleFontSize: 15,
    //        labelFontSize: 13,
    //    },
    //    axisX: {
    //        valueFormatString: "DD-MMM",
    //        title: "Tanggal",
    //        titleFontSize: 15,
    //        labelFontSize: 13
    //    },
    //    legend: {
    //        fontSize: 11,
    //        verticalAlign: "bottom",
    //        horizontalAlign: "center"
    //    },
    //    data: dataChart
    //});

    //chart2b.render();


    //chart2c = new CanvasJS.Chart("divChart2c", {
    //    title: {
    //        text: textTitle,
    //        fontWeight: "bolder",
    //        fontFamily: "Calibri",
    //        fontSize: 21,
    //        padding: 10
    //    },
    //    exportEnabled: true,
    //    exportFileName: generateFileName,
    //    zoomEnabled: true,
    //    panEnabled: true,
    //    axisXType: "secondary",
    //    animationEnabled: true,
    //    axisY: titikY,
    //    axisY2: {
    //        maximum: ((nilaiMaxCH / 8) * 2) + nilaiMaxCH,
    //        title: "Curah Hujan",
    //        titleFontSize: 15,
    //        labelFontSize: 13,
    //    },
    //    axisX: {
    //        valueFormatString: "DD-MMM",
    //        title: "Tanggal",
    //        titleFontSize: 15,
    //        labelFontSize: 13
    //    },
    //    legend: {
    //        fontSize: 11,
    //        verticalAlign: "bottom",
    //        horizontalAlign: "center"
    //    },
    //    data: dataChart
    //});

    //chart2c.render();

    //chart2d = new CanvasJS.Chart("divChart2d", {
    //    title: {
    //        text: textTitle,
    //        fontWeight: "bolder",
    //        fontFamily: "Calibri",
    //        fontSize: 21,
    //        padding: 10
    //    },
    //    exportEnabled: true,
    //    exportFileName: generateFileName,
    //    zoomEnabled: true,
    //    panEnabled: true,
    //    axisXType: "secondary",
    //    animationEnabled: true,
    //    axisY: titikY,
    //    axisY2: {
    //        maximum: ((nilaiMaxCH / 8) * 2) + nilaiMaxCH,
    //        title: "Curah Hujan",
    //        titleFontSize: 15,
    //        labelFontSize: 13,
    //    },
    //    axisX: {
    //        valueFormatString: "DD-MMM",
    //        title: "Tanggal",
    //        titleFontSize: 15,
    //        labelFontSize: 13
    //    },
    //    legend: {
    //        fontSize: 11,
    //        verticalAlign: "bottom",
    //        horizontalAlign: "center"
    //    },
    //    data: dataChart
    //});

    //chart2d.render();


    //var contentCH = "";
    //var textOptionCH = [];
    //if ($("#selectFilterCurahHujan").val() != null) {
    //    var b = $("#selectFilterCurahHujan").val();
    //    var a = $("#selectFilterCurahHujan option:selected").text();
    //    var textMulti = $.map($("#selectFilterCurahHujan option:selected"), function (el, i) {
    //        return $(el).text();
    //    });
    //    var a = textMulti.join(", ");
    //    var newCH = a.split(',')
    //    //console.log('panjang text baru = ' + newCH.length);
    //    contentCH += "Rata rata curah hujan diambil dari " + newCH.length + " Ombrometer yaitu <br/> ";
    //    for (var i = 0; i < $("#selectFilterCurahHujan").val().length; i++) {
    //        contentCH += '<div style =" margin-right: 20px;width: 100px; display: inline">' + '\u25cf' + newCH[i] + '</div>';
    //    }
    //    //for (var i = 0; i < $("#selectFilterCurahHujan").val().length; i++) {
    //    //    contentCH += '<div style =" margin-right: 20px;width: 100px; display: inline">' + '\u25cf' + $("#selectFilterCurahHujan").val()[i] + '</div>';
    //    //}
    //}
    //contentCH += "<br>Keterangan Jenis Rambu<br>"
    //contentCH += "<i class='fa fa-circle'></i> Daily"
    //contentCH += "<i class='fa fa-square' style = margin-left:40px;></i> Weekly"
    //contentCH += "<i class='fa fa-caret-up'  style = margin-left:40px;></i> Random"
    //$("#divKetChart").html(contentCH)

    //for (let i = 0; i < 10; i++) {
    //    var delay = 10000;
    //    setTimeout(function () {
    //        console.count();
    //        document.getElementById("divCounting").innerHTML = i;
    //        refresh();
    //    }
    //        , delay);
    //}
    //document.getElementById("divCounting").innerHTML = "Generate has been downloaded ";
    //if (statusBisaDownload == 1) {
    //    //document.getElementById("divCounting").innerHTML = "Please Wait 10 seconds to generate";
    //    var delay = 20000;

    //    setTimeout(function () {

    //        ////fungsi untuk download jpg
    //        //$('.canvasjs-chart-toolbar div>:nth-child(2)').click();

    //        ////fungsi untuk insert log
    //        //insertLogData(generateWMCode, generateFolder, generateFileName);

    //        //////divChart
    //        //////chart.exportChart({ format: "jpg" });


    //        ////////////var win = window.open('', '_self', '');
    //        ////////////win.focus();
    //        ////////////win.close();

    //        ////////////var customWindow = window.open('', '_blank', '');
    //        ////////////customWindow.close();

    //    }
    //        , delay);


    //}
    //document.getElementById("divCounting").innerHTML = "Generate has been downloaded ";
    //window.close();

    //window.open('', '_self', ''); window.close();



    //setTimeout(function () {
    //    window.open('', '_self', ''); window.close();
    //    //System.Environment.Exit(0);
    //}
    //    , delay);

}






function ShowDetailPzo(pierecordid, nchart) {
    //$('#divloadingdetail').show();
    //$('#divcontentdetail').hide();
    //// if (!($('.modal.in').length)) {       
    //$('.modal-dialogdetail').css({
    //    top: 0,
    //    left: 0
    //});
    ////}
    //$('#myDetail').modal({
    //    backdrop: false,
    //    show: true
    //});
    //$('.modal-dialogdetail').draggable({
    //    handle: ".modal-header"
    //});

    $.ajax({
        type: 'POST',
        url: '../Service/MapService.asmx/GetPiezoRecordDetailByPieRecordID',
        data: "{PieRecordID: '" + pierecordid + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = $.parseJSON(response.d);
            if (json == "") {
                var content = "<div class='box-body text-center'><h3 class='box-title'>Tidak Ada Piezo Di Blok Ini</h3></div>"
                $('#divmasterdetail').html(content);
                $('#divtablehistory').html("");
                $('#divloadingdetail').hide();
                $('#divcontentdetail').show();
            } else {
                var imagesrc = "", content = "";
                if (json[0]["ImageLocation"] == "") {
                    imagesrc = "<img style='cursor:pointer;' width='75px' height='75px' src='../Image/Icon/noimage.png' onclick='PreviewImage()'/>";
                }
                else {
                    imagesrc = "<img style='cursor:pointer;' width='75px' height='75px' src='http://172.30.1.122/PZOService/uploads/" + json[0]["estCode"] + "/" + json[0]["ImageLocation"] + "' onclick='PreviewImage()'/>";
                }

                globalimgurl = json[0]["estCode"] + "/" + json[0]["ImageLocation"];
                console.log(json);

                if (json.length > 0) {
                    $('#lblpiezorecordid').text("PiezoRecordID : " + json[0]["PieRecordID"]);

                    content = '<div class="col-md-7"><div class="row">';
                    content += '<div class="col-md-4">PiezoRecordID</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["PieRecordID"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Block</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["Block"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Longitude</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["Longitude"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Latitude</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["Latitude"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Accuracy</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["Accuracy"] + '</div></div>';
                    content += '<div class="row">';
                    content += '<div class="col-md-4">Is Active</div>';
                    content += '<div class="col-md-8">' + ': ' + json[0]["IsActive"] + '</div></div></div>';
                    content += '<div class="col-md-5" style="text-align: center">' + imagesrc + '</div>';
                    $('#divmasterdetail').html(content);

                    content = '<table id="tablehistory" class="table table-striped table-bordered" style="width: 100%">';
                    content += '<thead><tr>';
                    content += '<th>Week</th><th>Tanggal</th><th>Ketinggian</th><th>Kondisi</th><th>Klasifikasi</th>';
                    content += '</tr></thead><tbody>';



                    for (var i = 0; i < json.length; i++) {
                        content += '<tr>';
                        content += "<td align='center'>" + json[i]["Week"] + "</td><td align='center'>" + json[i]["DateUpload2"] + "</td><td align='center'>" + json[i]["Ketinggian"] + "</td><td align='center'>" + json[i]["Kondisi"] + "</td><td align='center'>" + json[i]["Klasifikasi"] + "</td></tr>";
                    }

                    content += '</tbody></table>';
                }

                //$('#divtablehistory').html(content);
                //$('#tablehistory').DataTable({
                //    dom: 'Bfrtip',
                //    pageLength: 5,
                //    buttons: [
                //        'copy', 'csv', 'excel'
                //    ],
                //    "order": [[1, "desc"]],
                //    "pagingType": "full_numbers"
                //});

                //ShowDetailPzoChart(json, 0, nchart);
                //$("#allData").click(function () {
                //    ShowDetailPzoChart(json, 0, nchart);
                //});
                //$("#thn").click(function () {
                //    ShowDetailPzoChart(json, 12, nchart);
                //});
                //$("#6bln").click(function () {
                //    ShowDetailPzoChart(json, 6, nchart);
                //});
                //$("#3bln").click(function () {
                //    ShowDetailPzoChart(json, 3, nchart);
                //});

                //set 3bln
                ShowDetailPzoChart(json, 3, nchart);
                $('#divloadingdetail').hide();
                $('#divcontentdetail').show();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function GetPiezoRecordDetailByPieRecordID : " + xhr.statusText);
        }
    });
}



function ShowDetailPzoChart(data, lamaData, nchart) {
    console.info(data);
    var dataPoints = [];
    var dataPoints_CH = [];
    var count = 1;
    var objAxisX = {
        reversed: true,
        //labelAngle: -45,
        title: "Minggu"
    };
    var d = new Date(data[0]["Date"]);
    if (lamaData == 3) {
        d.setDate(d.getDate() - (7 * 5));
        objAxisX = {
            reversed: true,
            //interval: 1,
            //labelAngle: -45,
            title: "Minggu"
        };
    }

    //if (lamaData == 3) {
    //    d.setMonth(d.getMonth() - 3);
    //    objAxisX = {
    //        reversed: true,
    //        interval: 2,
    //        //labelAngle: -45,
    //        title: "Minggu"
    //    };
    //} else if (lamaData == 6) {
    //    d.setMonth(d.getMonth() - 6);
    //    objAxisX = {
    //        reversed: true,
    //        interval: 3,
    //        //labelAngle: -45,
    //        title: "Minggu"
    //    };
    //} else if (lamaData == 12) {
    //    d.setMonth(d.getMonth() - 12);
    //}
    console.info(d);

    for (var i = 0; i < data.length; i++) {
        if (lamaData == 0) {
            dataPoints.push({
                x: count,
                y: data[i]["Ketinggian"],
                label: data[i]["WeekFormat3"]
            });
            dataPoints_CH.push({
                x: count,
                y: data[i]["TotalCH"],
                label: data[i]["WeekFormat3"]
            });
            count++;
        } else {
            if (d.getTime() <= data[i]["Date"]) {
                dataPoints.push({
                    x: count,
                    y: data[i]["Ketinggian"],
                    label: data[i]["WeekFormat3"]
                });
                dataPoints_CH.push({
                    x: count,
                    y: data[i]["TotalCH"],
                    label: data[i]["WeekFormat3"]
                });
                count++;
            }
        }
    }
    console.info(dataPoints);
    if (nchart == 1) {
        var chart2a = new CanvasJS.Chart("divChart2a", {
            zoomEnabled: true,
            axisXType: "secondary",
            animationEnabled: true,
            axisY: {
                reversed: true,
                title: "Kedalaman Air",
                stripLines: [
                    {
                        opacity: .3,
                        color: "#00BFFF",
                        startValue: -10,
                        endValue: 1
                    },
                    {
                        opacity: .3,
                        color: "#2f74b7",
                        startValue: 1,
                        endValue: 30
                    },
                    {
                        opacity: .3,
                        color: "#01b0f3",
                        startValue: 30,
                        endValue: 40
                    },
                    {
                        opacity: .3,
                        color: "#00b04e",
                        startValue: 40,
                        endValue: 50
                    },
                    {
                        opacity: .3,
                        color: "#fcfe07",
                        startValue: 50,
                        endValue: 60
                    },
                    {
                        opacity: .3,
                        color: "#fe0000",
                        startValue: 60,
                        endValue: 150
                    },
                    {
                        opacity: .3,
                        color: "#888888",
                        startValue: 150,
                        endValue: 999
                    }
                ]
            },
            axisY2: {
                title: "Curah Hujan (mm)"
            },
            axisX: objAxisX,
            data: [
                {
                    type: "column",
                    axisYType: "secondary",
                    name: "Curah Hujan",
                    dataPoints: dataPoints_CH
                },
                {
                    yValueFormatString: "### cm",
                    type: "line",
                    markerSize: 8,
                    markerType: "circle",
                    dataPoints: dataPoints
                }]

        });


        chart2a.render();

    }

    if (nchart == 2) {
        var chart2b = new CanvasJS.Chart("divChart2b", {
            zoomEnabled: true,
            axisXType: "secondary",
            animationEnabled: true,
            axisY: {
                reversed: true,
                title: "Kedalaman Air",
                stripLines: [
                    {
                        opacity: .3,
                        color: "#00BFFF",
                        startValue: -10,
                        endValue: 1
                    },
                    {
                        opacity: .3,
                        color: "#2f74b7",
                        startValue: 1,
                        endValue: 30
                    },
                    {
                        opacity: .3,
                        color: "#01b0f3",
                        startValue: 30,
                        endValue: 40
                    },
                    {
                        opacity: .3,
                        color: "#00b04e",
                        startValue: 40,
                        endValue: 50
                    },
                    {
                        opacity: .3,
                        color: "#fcfe07",
                        startValue: 50,
                        endValue: 60
                    },
                    {
                        opacity: .3,
                        color: "#fe0000",
                        startValue: 60,
                        endValue: 150
                    },
                    {
                        opacity: .3,
                        color: "#888888",
                        startValue: 150,
                        endValue: 999
                    }
                ]
            },
            axisY2: {
                title: "Curah Hujan (mm)"
            },
            axisX: objAxisX,
            data: [
                {
                    type: "column",
                    axisYType: "secondary",
                    name: "Curah Hujan",
                    dataPoints: dataPoints_CH
                },
                {
                    yValueFormatString: "### cm",
                    type: "line",
                    markerSize: 8,
                    markerType: "circle",
                    dataPoints: dataPoints
                }]

        });


        chart2b.render();

    }


    if (nchart == 3) {
        var chart2c = new CanvasJS.Chart("divChart2c", {
            zoomEnabled: true,
            axisXType: "secondary",
            animationEnabled: true,
            axisY: {
                reversed: true,
                title: "Kedalaman Air",
                stripLines: [
                    {
                        opacity: .3,
                        color: "#00BFFF",
                        startValue: -10,
                        endValue: 1
                    },
                    {
                        opacity: .3,
                        color: "#2f74b7",
                        startValue: 1,
                        endValue: 30
                    },
                    {
                        opacity: .3,
                        color: "#01b0f3",
                        startValue: 30,
                        endValue: 40
                    },
                    {
                        opacity: .3,
                        color: "#00b04e",
                        startValue: 40,
                        endValue: 50
                    },
                    {
                        opacity: .3,
                        color: "#fcfe07",
                        startValue: 50,
                        endValue: 60
                    },
                    {
                        opacity: .3,
                        color: "#fe0000",
                        startValue: 60,
                        endValue: 150
                    },
                    {
                        opacity: .3,
                        color: "#888888",
                        startValue: 150,
                        endValue: 999
                    }
                ]
            },
            axisY2: {
                title: "Curah Hujan (mm)"
            },
            axisX: objAxisX,
            data: [
                {
                    type: "column",
                    axisYType: "secondary",
                    name: "Curah Hujan",
                    dataPoints: dataPoints_CH
                },
                {
                    yValueFormatString: "### cm",
                    type: "line",
                    markerSize: 8,
                    markerType: "circle",
                    dataPoints: dataPoints
                }]

        });


        chart2c.render();

    }


    if (nchart == 4) {
        var chart2d = new CanvasJS.Chart("divChart2d", {
            zoomEnabled: true,
            axisXType: "secondary",
            animationEnabled: true,
            axisY: {
                reversed: true,
                title: "Kedalaman Air",
                stripLines: [
                    {
                        opacity: .3,
                        color: "#00BFFF",
                        startValue: -10,
                        endValue: 1
                    },
                    {
                        opacity: .3,
                        color: "#2f74b7",
                        startValue: 1,
                        endValue: 30
                    },
                    {
                        opacity: .3,
                        color: "#01b0f3",
                        startValue: 30,
                        endValue: 40
                    },
                    {
                        opacity: .3,
                        color: "#00b04e",
                        startValue: 40,
                        endValue: 50
                    },
                    {
                        opacity: .3,
                        color: "#fcfe07",
                        startValue: 50,
                        endValue: 60
                    },
                    {
                        opacity: .3,
                        color: "#fe0000",
                        startValue: 60,
                        endValue: 150
                    },
                    {
                        opacity: .3,
                        color: "#888888",
                        startValue: 150,
                        endValue: 999
                    }
                ]
            },
            axisY2: {
                title: "Curah Hujan (mm)"
            },
            axisX: objAxisX,
            data: [
                {
                    type: "column",
                    axisYType: "secondary",
                    name: "Curah Hujan",
                    dataPoints: dataPoints_CH
                },
                {
                    yValueFormatString: "### cm",
                    type: "line",
                    markerSize: 8,
                    markerType: "circle",
                    dataPoints: dataPoints
                }]

        });


        chart2d.render();

    }



}


















//function closeWindow() {

//    //// Open the new window
//    //// with the URL replacing the
//    //// current page using the
//    //// _self value


//    let new_window =
//    	open(location, '_self');
//    // Close this window
//    new_window.close();


//    //var myWindow = window.open("", "_self");
//    //myWindow.document.write("<p>Generate Success !!</p>");
//    //////myWindow.focus();
//    //////myWindow.close();


//    return false;
//}
//////////////////////////////////////////////////////////
///////////////// REGION WEBSERVICE METHOD ///////////////////
//////////////////////////////////////////////////////////

function insertLogData(WMACode, DownloadFolder, DownloadFileName) {

    //var DownloadFolder = "";
    //var DownloadFileName = "";

    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/insertLogData',
        data: '{WMACode:"' + WMACode + '",DownloadFolder:"' + DownloadFolder + '",DownloadFileName:"' + DownloadFileName + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = JSON.parse(response.d);

            //$('#btnClose').click();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function insertLogData : " + xhr.statusText);
        }
    });
}


function cekDownloadChartImageZona(WMACode, DownloadFolder, DownloadFileName) {

    //var DownloadFolder = "";
    //var DownloadFileName = "";

    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/cekDownloadChartImageZona',
        data: '{WMACode:"' + WMACode + '",DownloadFolder:"' + DownloadFolder + '",DownloadFileName:"' + DownloadFileName + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var json = JSON.parse(response.d);
            //alert("function json : " + json);
            if (json == "1") {
                statusBisaDownload = 1;
                //alert("function cekDownloadChartImageZona status : " + statusSudahdiDownload);
                //document.getElementById("divCounting").innerHTML = "Please Wait 10 seconds to generate";

                //}
            } else {
                statusBisaDownload = 0;
                //document.getElementById("divCounting").innerHTML = "Generate has been downloaded ";
                //alert("function cekDownloadChartImageZona status : " + statusSudahdiDownload);
            }
            //alert("function statusSudahdiDownload : " + statusSudahdiDownload);

            //var dataPMKS = JSON.parse(json['statusSudahdiDownload']);
            //var dataProduksi = JSON.parse(json['produksi']);
            //var dataDetail = JSON.parse(json['details']);
            //console.info("dataDetail", dataDetail);

            //if (dataPMKS[0]["lastGetData"] == null) {
            //    $("#lastGetData").html("");
            //} else {
            //    $("#lastGetData").html(dateFormat3(new Date(parseInt(dataPMKS[0]["lastGetData"].replace('/Date(', '').replace(')/', '')))));
            //}


            //var json = JSON.parse(response.d);

            //$('#btnClose').click();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function cekDownloadChartImageZona : " + xhr.statusText);
        }
    });
}


function getWeekID() {

    $(".loading").show();
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/getWeekID',
        data: JSON.stringify({
            weekId: objParam.weekid,
            weekIdBefore: 5
        }),

        //data: '{weekId: "' + objParam.weekid + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        //async: true,
        success: function (response) {
            var strJson = [];
            strJson = JSON.parse(response.d);
            dataWeekID= $.parseJSON(response.d);
            for (var i = 0; i < dataWeekID.length; i++) {
                local_weekName = dataWeekID[i]["WeekName3"];
                //$("#kontenWeekName").html(dataWeekID[i]["WeekName3"]);
                //IOT_startingdate = dataWeekID[i]["StartDate6"];
                //IOT_endingdate = dataWeekID[i]["EndDate"];
                IOT_startingdate = dateFormat6(new Date(parseInt(dataWeekID[i]["StartDate6"].replace('/Date(', '').replace(')/', ''))));
                IOT_endingdate = dateFormat6(new Date(parseInt(dataWeekID[i]["EndDate"].replace('/Date(', '').replace(')/', ''))));
                //$(".loading").hide();

                if (objParam.zonaanalysisid != undefined) {
                    getSettingZona();
                    //if (local_idZona != undefined) {

                    //    getLabelChart();
                    //    getChartFromZona();
                    //}
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getWeekID : " + xhr.statusText);
        }
    });

}



function getSettingZona() {
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/getSettingZona',
        //data: JSON.stringify({ stationSelected: JSON.stringify(selectedRambu) }),
        data: '{zonaAnalysisId: "' + objParam.zonaanalysisid + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var strJson = [];
            strJson = JSON.parse(response.d);
            dataSettingZona = $.parseJSON(response.d);
            for (var i = 0; i < dataSettingZona.length; i++) {
                local_companyCode= dataSettingZona[i]["companyCode"];
                local_wmArea= dataSettingZona[i]["wmArea"];
                local_idZona= dataSettingZona[i]["idZona"];
                local_pieRecordIDArr= dataSettingZona[i]["pieRecordIDArr"];
                local_awlDeviceId= dataSettingZona[i]["awlDeviceId"];
                local_WMArea2 = dataSettingZona[i]["wmArea2"];
                local_namaGraph= dataSettingZona[i]["namaGraph"];
                local_CH= dataSettingZona[i]["CH"];
                local_HH= dataSettingZona[i]["HH"];
                local_colorField= dataSettingZona[i]["colorField"];
                if (local_idZona != undefined) {

                    getSettingHeader();
                    getLabelChart();
                    getChartFromZona();
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getSettingZona : " + xhr.statusText);
        }
    });
}


function getSettingHeader() {
    var content = '<font face="Arial Narrow" size="12">';
    content += '<table id="tbHeader" style="height: 54px; width: 100%; border-collapse: collapse;" border="0">';
    content += '<tbody>';
    content += '<tr style="height: 12px;">';
    content += '<td style="width: 14.0151%; height: 12px;">' + 'WM Area' + '</td>';
    content += '<td style="width: 2.22536%; height: 12px;">' + ':' + '</td>';
    content += '<td style="width: 61.3152%; height: 12px;">' + local_WMArea2 + '</td>';
    content += '<td style="width: 22.4442%; height: 12px;" colspan="3">' + 'Curah hujan :' + '</td>';
    content += '</tr>';
    content += '<tr style="height: 12px;">';
    content += '<td style="width: 14.0151%; height: 12px;">' + 'Zona' + '</td>';
    content += '<td style="width: 2.22536%; height: 12px;">' + ':' + '</td>';
    content += '<td style="width: 61.3152%; height: 12px;">' + local_namaGraph + '</td>';
    content += '<td style="width: 7.48201%; height: 12px;">' + '- HH' + '</td>';
    content += '<td style="width: 2.51535%; height: 12px;">' + ':' + '</td>';
    //content += '<td style="width: 12.4468%; height: 12px;">' + local_HH + '</td>';
    content += '<td style="width: 12.4468%; height: 12px;">' + '<div id="kontenHH1"></div>' + '</td>';
    content += '</tr>';
    content += '<tr style="height: 12px;">';
    content += '<td style="width: 14.0151%; height: 12px;">' + 'Periode' + '</td>';
    content += '<td style="width: 2.22536%; height: 12px;">' + ':' + '</td>';
    //content += '<td style="width: 61.3152%; height: 12px;">' + 'Aug-W3, 2023' + '</td>';
    //content += '<td style="width: 61.3152%; height: 12px;">' + '<div id="kontenWeekName"></div>' + '</td>';
    content += '<td style="width: 61.3152%; height: 12px;">' + local_weekName + '</td>';
    content += '<td style="width: 7.48201%; height: 12px;">' + '- CH' + '</td>';
    content += '<td style="width: 2.51535%; height: 12px;">' + ':' + '</td>';
    //content += '<td style="width: 12.4468%; height: 12px;">' + local_CH + '</td>';
    content += '<td style="width: 12.4468%; height: 12px;">' + '<div id="kontenCH1"></div>' + '</td>';
    content += '</tr>';
    content += '</tbody>';
    content += "</table></font>";

    //$("#kontenCH1").html(local_CH1);
    //$("#kontenHH").html(local_HH1);
    $("#kontenHead2").html(content);
}


function getChartFromZona() {
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/getChartFromZona',
        //data: '{zonaId: "' + objParam.idzona.toUpperCase() + '"}',
        data: '{zonaId: "' + local_idZona + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            dataChartZona = $.parseJSON(response.d);
            selectedRambu = [];
            var allCH = [];
            for (var i = 0; i < dataChartZona.length; i++) {
                if (dataChartZona[i]["stationId"] != "") {
                    selectedRambu.push({
                        stationId: dataChartZona[i]["stationId"],
                        stationName: dataChartZona[i]["stationName"],
                        sortField: dataChartZona[i]["sortField"],
                        color: dataChartZona[i]["colorField"]
                    })
                }

                if (dataChartZona[i]["rambuCurahHujan"].trim().split(",").length > 0) {
                    var rsplitCH = dataChartZona[i]["rambuCurahHujan"].trim().split(",");
                    for (var j = 0; j < rsplitCH.length; j++) {
                        allCH.push(rsplitCH[j].trim())
                    }
                }

                tmasTarget = dataChartZona[i]["TmasTarget"]
                textTitle = dataChartZona[i]["namaGraph"]
            }

            allCH = _.uniq(allCH, false);
            selectedCurahHujan = "";
            for (var i = 0; i < allCH.length; i++) {
                if (selectedCurahHujan == "") {
                    selectedCurahHujan = allCH[i];
                } else {
                    selectedCurahHujan += "," + allCH[i];
                }

            }

            if (selectedRambu.length > 0) {
                GetStationMeasureArray(selectedRambu);
            } else if (selectedCurahHujan != "") {
                getValueCurahHujan();
            } else {
                setupChart();
            }
        },

        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getChartFromZona : " + xhr.statusText);
        }
    });
}


function GetStationMeasureArray() {
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/GetStationMeasureArray',
        data: JSON.stringify({ stationSelected: JSON.stringify(selectedRambu) }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            selectedRambuMeasure = [];
            selectedRambuMeasure = JSON.parse(response.d);
            console.info("object Measure", selectedRambuMeasure);
            //selectedRambuMeasure.sort(predicateBy("sortField"));
            chartData = [];
            minY = null;
            maxY = null;
            if (selectedRambuMeasure.length > 0) {
                addDataSet(0);
            } else {
                if (selectedCurahHujan != "") {
                    getValueCurahHujan();
                } else {
                    setupChart();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getMasterZona : " + xhr.statusText);
        }
    });
}

function getValueCurahHujan() {
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/getValueCurahHujan',
        data: '{stationSelected: "' + selectedCurahHujan + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            addDataCurahHujan(JSON.parse(response.d));
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("function getValueCurahHujan : " + xhr.statusText);
        }
    });
}




//fungsi mengambil data TMAT harian
function IOT_GetHoliday() {
    $(".loader").show();
    console.log("GetHoliday");
    $.ajax({
        type: 'POST',
        url: '../Service/webservice.asmx/GetHoliday',
        data: JSON.stringify({

        }),

        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (response) {
            global_holiday = $.parseJSON(response.d);
            console.log(global_holiday);
            $(".loader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function webserviceGetDailyRainfall : " + xhr.statusText);
        }
    });

}



function IOT_GetMonthlyTMAT() {
    $(".loader").show();
    console.log("IOT_GetMonthlyTMAT" + "")
    $.ajax({
        type: 'POST',
        url: '../Service/webservice.asmx/GetMonthlyTMAT',
        data: JSON.stringify({
            //companycode: $("#selectFilterPTAWL").val(),
            //estcode: $('#selectFilterEstateAWL').val(),
            //deviceid: $('#selectFilterArsIDAWL').val(),
            //startingdate: global_startdate,
            //endingdate: global_enddate
            companycode: IOT_companycode,
            estcode: IOT_estcode,
            deviceid: IOT_deviceid,
            startingdate: IOT_startingdate,
            endingdate: IOT_endingdate

        }),

        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (response) {
            global_monthlytmat = $.parseJSON(response.d);
            //global_monthlytmat = _.uniq(global_monthlytmat, function (x) { return x.recordTime; });
            console.log(global_monthlytmat);
            IOT_GetHoliday();

            $("#keteranganstasiun").css("visibility", "visible");
            let selectedMonth = $("#selectMonth").find("option:selected").text();
            let daily = document.getElementById("datacurahhujan");
            //daily.innerHTML = '<b>Laporan TMAT<b>';
            ////let hours = global_dailyrainfall.map(a => a.recordTime);
            ////console.log(hours);
            //document.getElementById("namaPT").innerHTML = $("#selectFilterPTAWL").val();
            //document.getElementById("namaEstate").innerHTML = $("#selectFilterEstateAWL").val();
            //document.getElementById("stasiunARS").innerHTML = $("#selectFilterArsIDAWL option:selected").text();
            //document.getElementById("tanggalRec").innerHTML = global_startdate + ' - ' + global_enddate
            ////document.getElementById("tanggalRec").innerHTML = global_listbulan[$("#selectPeriod").val().slice(5, 7) - 1] + '&nbsp' + $("#selectPeriod").val().slice(0, 4)
            ////console.log(document.getElementById("tanggalRec").innerHTML);
            //document.getElementById("dateMode").innerHTML = 'Periode'
            $(".chartdiv").css("display", "block");
            IOT_initializeChartBulanan();

            $(".loader").hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("function webserviceGetDailyRainfall : " + xhr.statusText);
        }
    });

}


function IOT_initializeChartBulanan() {
    let datachart = [];
    let data = [];
    let data2 = [];
    let data3 = [];
    let data4 = [];
    let data5 = [];
    let d;
    let json = global_monthlytmat
    let filter = _.uniq(json, function (x) { return x.bulan; });
    let color = ['#3c87a4', '#7aa43c', '#a93d8b', '#cb961b', '#76bcbc', '#797a84', '#47e4d5', '#efcef7', '#d2f3d1', '#1e434f', '#ffb3ec', '#ffbf00'];
    let jam = json.map(a => a.jam);
    console.log(jam);
    //console.log(jam[0].substring(11, 19).replace(/\-/g, ':'))

    let day = new Date();
    day.setFullYear($("#selectYear").val(), $("#selectMonth").val(), 0);
    let lastday = day.getDate();

    const date1 = new Date(IOT_startingdate);
    const date2 = new Date(IOT_endingdate);
    const diffTime = Math.abs(date2 - date1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));


    for (let i = 0; i < filter.length; i++) {
        //Inisilaisasi label libur
        let labellibur = '', labellibur2 = '';


        let jsondata = json
        console.log(jsondata);
        datachart = [];
        datachart2 = [];
        datachart3 = [];
        datachart4 = [];
        datachart5 = [];
        console.log(global_startdate, global_enddate);
        //for (let x = 0; x < jsondata.length; x++) {
        //    console.log(new Date(new Date().setFullYear(parseInt(jsondata[x]["dateformatted"].slice(0, 4)), parseInt(jsondata[x]["dateformatted"].slice(5, 7)) - 1, jsondata[x]["hari"]))); 
        //};

        for (let j = 0; j < jsondata.length; j++) {

            d = new Date(new Date().setFullYear(parseInt(jsondata[j]["dateformatted"].slice(0, 4)), parseInt(jsondata[j]["dateformatted"].slice(5, 7)) - 1, jsondata[j]["hari"], jsondata[j]["jam"]))

            if (d.getDay() == 0) {
                //console.log(global_holiday.filter(d => d.Holiday_Date == i+1))
                labellibur = '('
                labellibur2 = ')'
            } else {
                labellibur = ''
                labellibur2 = ''
            }
            if (global_holiday.filter(d => d.Holiday_Date == jsondata[j]["dateformatted"]).length > 0) {
                //console.log(global_holiday.filter(d => d.Holiday_Date == i+1))
                labellibur = '['
                labellibur2 = ']'
            }
            //`${labellibur} ${jsondata[j]["hariLabel"]} ${labellibur2}    <${getMoonPhase(parseInt(jsondata[j]["dateformatted"].slice(0, 4)), parseInt(jsondata[j]["dateformatted"].slice(5, 7)), parseInt(jsondata[j]["dateformatted"].slice(8, 10)))} >`

            let nilaiy = jsondata[j]["data"];
            //if (parseFloat(nilaiy) > 4) nilaiy = 4;
            datachart.push({
                label: jsondata[j]["labeljam"],
                y: nilaiy,
                date: jsondata[j]["jam"],
                jam: jsondata[j]["labeljam"],
                clock: jsondata[j]["clock"],
                hari: jsondata[j]["hari"],
            });
            let nilaiy2 = jsondata[j]["battery"];
            //if (parseFloat(nilaiy) > 4) nilaiy = 4;
            datachart2.push({
                //label: `${labellibur} ${jsondata[j]["hariLabel"]} ${labellibur2}   <${moment().year(parseInt(jsondata[j]["dateformatted"].slice(0, 4))).month(parseInt(jsondata[j]["dateformatted"].slice(5, 7))).date(parseInt(jsondata[j]["dateformatted"].slice(8, 10))).solar().format('DD')} >`,
                y: nilaiy2,
                jam: jsondata[j]["labeljam"],
            });
            let nilaiy3 = jsondata[j]["sinyal"];
            //if (parseFloat(nilaiy) > 4) nilaiy = 4;
            datachart3.push({
                //label: `${labellibur} ${jsondata[j]["hariLabel"]} ${labellibur2}   <${moment().year(parseInt(jsondata[j]["dateformatted"].slice(0, 4))).month(parseInt(jsondata[j]["dateformatted"].slice(5, 7))).date(parseInt(jsondata[j]["dateformatted"].slice(8, 10))).solar().format('DD')} >`,
                y: nilaiy3,
                jam: jsondata[j]["labeljam"],
            });
            datachart4.push({
                //label: `${labellibur} ${jsondata[j]["hariLabel"]} ${labellibur2}   <${moment().year(parseInt(jsondata[j]["dateformatted"].slice(0, 4))).month(parseInt(jsondata[j]["dateformatted"].slice(5, 7))).date(parseInt(jsondata[j]["dateformatted"].slice(8, 10))).solar().format('DD')} >`,
                y: null,
                jam: jsondata[j]["labeljam"],
                clock: jsondata[j]["clock"],
                //label: `${labellibur} ${jsondata[j]["hariLabel"]} ${labellibur2}    <${getMoonPhase(parseInt(jsondata[j]["dateformatted"].slice(0, 4)), parseInt(jsondata[j]["dateformatted"].slice(5, 7)), parseInt(jsondata[j]["dateformatted"].slice(8, 10)))} >`,
                //label: `${labellibur} ${jsondata[j]["hariLabel"]} ${labellibur2}`,
                label: `${labellibur}${jsondata[j]["hariLabel"]}${labellibur2}`,
            });
            let nilaiy4 = jsondata[j]["CH"];
            datachart5.push({
                //label: `${labellibur} ${jsondata[j]["hariLabel"]} ${labellibur2}   <${moment().year(parseInt(jsondata[j]["dateformatted"].slice(0, 4))).month(parseInt(jsondata[j]["dateformatted"].slice(5, 7))).date(parseInt(jsondata[j]["dateformatted"].slice(8, 10))).solar().format('DD')} >`,
                y: nilaiy4,
                jam: jsondata[j]["labeljam"],
                clock: jsondata[j]["clock"],
                hari: jsondata[j]["hari"],
                //label: `${labellibur} ${jsondata[j]["hariLabel"]} ${labellibur2}    <${getMoonPhase(parseInt(jsondata[j]["dateformatted"].slice(0, 4)), parseInt(jsondata[j]["dateformatted"].slice(5, 7)), parseInt(jsondata[j]["dateformatted"].slice(8, 10)))} >`,
                //label: `${labellibur} ${jsondata[j]["hariLabel"]} ${labellibur2}`,
                label: `${labellibur}${jsondata[j]["hariLabel"]}${labellibur2}`,
            });

        }
        console.log(datachart)
        data.push({
            type: "line",
            name: "TMAT",
            showInLegend: true,
            axisYIndex: 0,
            dataPoints: datachart,
            BorderColor: "black",
            "color": "#920fba",
        });
        data2.push({
            type: "line",
            name: "Battery",
            showInLegend: true,
            axisYIndex: 0,
            dataPoints: datachart2,
            BorderColor: "black"
        })
        data3.push({
            type: "line",
            name: "Sinyal",
            showInLegend: true,
            axisYIndex: 0,
            dataPoints: datachart3,
            BorderColor: "black"
        })
        data4.push({
            type: "line",
            name: "Label Jam",
            showInLegend: true,
            axisYIndex: 0,
            dataPoints: datachart4,
            BorderColor: "black"
        })
        data5.push({
            type: "column",
            name: "Rainfall",
            showInLegend: true,
            axisYIndex: 0,
            dataPoints: datachart5,
            BorderColor: "black"
        })
    }
    data = data.concat(data2, data3, data4, data5);
    console.log(data)
    IOT_RenderChartBulanan(data, datachart, datachart2, datachart3, datachart4, datachart5, diffDays);
}

function IOT_RenderChartBulanan(data, datachart, datachart2, datachart3, datachart4, datachart5, diffDays) {
    //console.log(datachart, datachart1, datachart2)

    ////Buat custom legend
    let waterdepth = global_waterindicator

    global_tergenang = 'Tergenang';
    global_agaktergenang = 'Agak tergenang';
    global_normal = 'Normal';
    global_agakkering = 'Agak kering';
    global_kering = 'Kering';

    let banjir = {
        type: 'rangeArea',
        axisXType: "secondary",
        name: "Banjir (" + waterdepth[0]["IndicatorName"] + " cm)",
        //color: "black",
        toolTipContent: null,
        showInLegend: true,
        markerSize: 0,
        dataPoints: [{ x: 0, y: null }]
    }

    let tergenang = {
        type: 'rangeArea',
        axisXType: "secondary",
        name: global_tergenang + " (" + waterdepth[1]["IndicatorName"] + " cm)",
        color: "#1a8cff",
        toolTipContent: null,
        showInLegend: true,
        markerSize: 0,
        dataPoints: [{ x: 0, y: null }]
    }

    let agaktergenang = {
        type: 'rangeArea',
        axisXType: "secondary",
        name: global_agaktergenang + " (" + waterdepth[2]["IndicatorName"] + " cm)",
        color: "#80dfff",
        toolTipContent: null,
        showInLegend: true,
        markerSize: 0,
        dataPoints: [{ x: 0, y: null }]
    }

    let normal = {
        type: 'rangeArea',
        axisXType: "secondary",
        name: "Normal (" + waterdepth[3]["IndicatorName"] + " cm)",
        color: "#8cd9b3",
        toolTipContent: null,
        showInLegend: true,
        markerSize: 0,
        dataPoints: [{ x: 0, y: null }]
    }

    let agakkering = {
        type: 'rangeArea',
        axisXType: "secondary",
        name: global_agakkering + " (" + waterdepth[4]["IndicatorName"] + " cm)",
        color: "yellow",
        toolTipContent: null,
        showInLegend: true,
        markerSize: 0,
        dataPoints: [{ x: 0, y: null }]
    }

    let kering = {
        type: 'rangeArea',
        axisXType: "secondary",
        name: global_kering + " (" + waterdepth[5]["IndicatorName"] + " cm)",
        color: "red",
        toolTipContent: null,
        showInLegend: true,
        markerSize: 0,
        dataPoints: [{ x: 0, y: null }]
    }
    console.log(datachart, 'datachart')
    //datapoint yang dipakai
    let labeljam = datachart4.map(x => ({
        "y": x.y,
        "label": x.label,
        "click": onClick,
        "keterangan": 'Hari',
        "clock": x.clock

    }));
    let TMAT = datachart.map(x => ({
        "y": x.y,
        "label": x.label,
        "click": onClick,
        "keterangan": 'TMAT',
        "clock": x.clock,
        "hari": x.hari,
        "markerSize": 5,
        "color": "#920fba",
        "satuan": "cm"

    }));
    let Battery = datachart2.map(x => ({
        "y": x.y,
        "label": x.label,
        "click": onClick,
        "keterangan": 'Battery'
    }));
    let Signal = datachart3.map(x => ({
        "y": x.y,
        "label": x.label,
        "click": onClick,
        "keterangan": 'Signal'
    }));
    let CH = datachart5.map(x => ({
        "y": x.y,
        //"label": x.label,
        "click": onClick,
        "keterangan": 'CH',
        "hari": x.hari,
        "clock": x.clock,
    }));

    let dataViz = [];
    for (let i = 0; i < 1; i++) {
        dataViz.push({
            type: "line",
            name: 'TMAT',
            axisYIndex: 0,
            axisXindex: 0,
            showInLegend: true,
            dataPoints: TMAT,
            BorderColor: "black",
            color: "#920fba"
        })
    }

    let dataViz2 = [];
    for (let i = 0; i < 1; i++) {
        dataViz2.push({
            type: "line",
            name: 'Battery',
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: Battery,
            BorderColor: "black",
            visible: false,
        })
    }

    let dataViz3 = [];
    for (let i = 0; i < 1; i++) {
        dataViz3.push({
            type: "line",
            name: 'Signal',
            axisYIndex: 0,
            showInLegend: true,
            dataPoints: Signal,
            BorderColor: "black",
            visible: false,
        })
    }
    let dataViz4 = [];
    for (let i = 0; i < 1; i++) {
        dataViz4.push({
            type: "line",
            name: 'Hari',
            axisXIndex: 1,
            showInLegend: false,
            dataPoints: labeljam,
            BorderColor: "black",
            visible: true,
        })
    }
    let dataViz5 = [];
    for (let i = 0; i < 1; i++) {
        dataViz5.push({
            type: "stepArea",
            name: 'Rainfall',
            axisYIndex: 1,
            axisXindex: 1,
            showInLegend: true,
            dataPoints: CH,
            BorderColor: "black",
            Color: "red",
            fillOpacity: .5,
            visible: true,
            axisYType: "secondary",
            lineThickness: 0,
            color: '#38a9c2',
        })
    }
    console.log(dataViz2);


    //Dynamic label berdasarkan minimum && maximum value Y


    let yVal = []
    for (let i = 0; i < datachart.length; i++) {

        yVal.push(Object.values(dataViz[0].dataPoints[i])[0])

    }
    //console.log(Math.max(...yVal));
    //console.log(yVal);
    let maxY = Math.max(...yVal)
    let minY = Math.min(...yVal)

    console.log(minY, maxY);
    if (minY == 0 && maxY == 0) {
        //untuk data kosong
    } else {
        //untuk data yang ada
        if (maxY < waterdepth[0]["MaxValue"] || minY < waterdepth[0]["MaxValue"]) {
            dataViz.push(banjir)
            //dataViz[0]["color"] = 'white'
        }
        if (maxY >= waterdepth[1]["MinValue"] || maxY <= waterdepth[1]["MaxValue"]) {
            dataViz.push(tergenang)
            //dataViz[0]["color"] = 'white'
        }
        if (maxY >= waterdepth[2]["MinValue"] || maxY <= waterdepth[2]["MaxValue"]) {
            dataViz.push(agaktergenang)
            //dataViz[0]["color"] = 'white'
        }
        if (maxY >= waterdepth[3]["MinValue"] || maxY <= waterdepth[3]["MaxValue"]) {
            dataViz.push(normal)
            //dataViz[0]["color"] = 'white'
        }
        if (maxY >= waterdepth[4]["MinValue"] || maxY <= waterdepth[4]["MaxValue"]) {
            dataViz.push(agakkering)
            //dataViz[0]["color"] = 'white'
        }
        if (maxY > waterdepth[5]["MaxValue"]) {
            dataViz.push(kering)
            //dataViz[0]["color"] = 'white'
        }
    }

    //dataViz = dataViz4.concat(dataViz2, dataViz3, dataViz5, dataViz);
    dataViz = dataViz4.concat(dataViz5, dataViz);
    console.log(dataViz)

    var chart = new CanvasJS.Chart("divChartIOT", {
        animationEnabled: true,
        zoomEnabled: true,
        theme: "light2",
        title: {
            text: global_tmattitle,
            fontSize: 15,
        },
        data: dataViz,
        toolTip: {
            shared: false,
            borderThickness: 0,
            contentFormatter: function (e) {

                return "Hari " + e.entries[0].dataPoint.hari + '<br>' + "Waktu " + e.entries[0].dataPoint.clock + '<br>' + e.entries[0].dataPoint.keterangan + " " + parseFloat(e.entries[0].dataPoint.y).toFixed(1) + " cm";
            }

        },
        axisX: [{

            interval: 6,
            labelAngle: 0,
            gridDashType: "dot",
            gridThickness: 1,
            lineThickness: 1,
            lineColor: "black",
            //minimum: 0
            //labelMaxWidth: 45,
            //labelFontColor: "red",
            labelFormatter: function (e) {
                //console.log(e)
                //untuk setingan jam 14 hari ganti 1
                //if (diffDays > 14) {
                //    return " ";
                //} else {
                //    return + e.label;
                //}
                if (diffDays > 1) {
                    return " ";
                } else {
                    return + e.label;
                }
            }
            //minimum: 0
            //labelMaxWidth: 45,
            //labelFontColor: "red",


        },
        {
            title: "Hari",
            interval: 24,
            labelAngle: 0,
            gridDashType: "dot",
            gridThickness: 2,
            gridDashType: "solid",
            labelMaxWidth: 45,
            lineThickness: 1,
            lineColor: "black"
            //labelFontColor: "red",


        },
        {
            //title: "CH",
            interval: 1,
            labelAngle: 0,
            gridDashType: "dot",
            gridThickness: 0,
            labelMaxWidth: 45,
            //labelFontColor: "red",


        }
        ],
        //axis buat custom legend
        axisX2: {
            title: "",
            tickLength: 2,
            margin: 0,
            lineThickness: 0,
            minimum: 0,
            maximum: 0,
            valueFormatString: " ",
        },
        axisX3: {
            title: "",
            tickLength: 2,
            margin: 0,
            lineThickness: 0,
            minimum: 0,
            maximum: 0,
            valueFormatString: " ",
        },
        axisX4: {
            title: "",
            tickLength: 2,
            margin: 0,
            lineThickness: 0,
            minimum: 0,
            maximum: 0,
            valueFormatString: " ",
        },
        axisX5: {
            title: "",
            tickLength: 2,
            margin: 0,
            lineThickness: 0,
            minimum: 0,
            maximum: 0,
            valueFormatString: " ",
        },
        axisX6: {
            title: "",
            tickLength: 2,
            margin: 0,
            lineThickness: 0,
            minimum: 0,
            maximum: 0,
            valueFormatString: " ",
        },
        axisX7: {
            title: "",
            tickLength: 2,
            margin: 0,
            lineThickness: 0,
            minimum: 0,
            maximum: 0,
            valueFormatString: " ",
        },
        axisY: [{
            title: "TMAT (cm)",
            stripLines: [
                {
                    startValue: waterdepth[0]["MaxValue"],
                    endValue: waterdepth[0]["MinValue"],
                    color: "#000000",
                    opacity: 0.4
                },
                {
                    startValue: waterdepth[1]["MaxValue"],
                    endValue: waterdepth[1]["MinValue"],
                    color: "#1a8cff",
                    opacity: 0.4
                },
                {
                    startValue: waterdepth[2]["MaxValue"],
                    endValue: waterdepth[2]["MinValue"],
                    color: "#80dfff",
                    opacity: 0.4
                },
                {
                    startValue: waterdepth[3]["MaxValue"],
                    endValue: waterdepth[3]["MinValue"],
                    color: "#8cd9b3",
                    opacity: 0.4
                },
                {
                    startValue: waterdepth[4]["MaxValue"],
                    endValue: waterdepth[4]["MinValue"],
                    color: "#ffff00",
                    opacity: 0.4
                },
                {
                    startValue: waterdepth[5]["MaxValue"],
                    endValue: waterdepth[5]["MinValue"],
                    color: "#ff0000",
                    opacity: 0.4
                }
            ],
            reversed: true,
            lineThickness: 1,
            lineColor: "black"

        }],
        axisY2: [{
            title: "Rainfall (mm)",
            lineThickness: 1,
            lineColor: "black"
        }],
        legend: {
            cursor: "pointer",
            verticalAlign: "bottom",
            horizontalAlign: "canter",
            dockInsidePlotArea: false,
            itemclick: function (e) {
                if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                    e.dataSeries.visible = false;
                } else {
                    e.dataSeries.visible = true;
                }
                e.chart.render();
            }
        },
    });
    console.log(chart.data)
    chart.render();




    //addBorderToDataPoint(chart, 1);
    function onClick(e) {


    }

}

function IOT_GetWaterDepthIndicator() {
    let hasil = "";
    $.ajax({
        type: 'POST',
        url: '../Service/WebService.asmx/IOT_GetWaterDepthIndicator',
        data: JSON.stringify({
        }),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function (response) {
            global_waterindicator = $.parseJSON(response.d);
            console.log(global_waterindicator);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $("#loader").hide();
            alert("Maaf terjadi kesalahan GetLanguage.");
        }
    });

    return hasil;
}

function getMoonPhase(year, month, day) {
    var c = e = jd = b = 0;

    if (month < 3) {
        year--;
        month += 12;
    }

    ++month;

    c = 365.25 * year;

    e = 30.6 * month;

    jd = c + e + day - 694039.09; //jd is total days elapsed

    jd /= 29.5305882; //divide by the moon cycle

    b = parseInt(jd); //int(jd) -> b, take integer part of jd



    jd -= b; //subtract integer part to leave fractional part of original jd

    //b = Math.round(jd * 8); //scale fraction from 0-8 and round
    //console.log(jd*29.5);
    //console.log(b);
    //if (b >= 8) {
    //    b = 0; //0 and 8 are the same so turn 8 into 0
    //}

    b = Math.round(jd * 29.5);

    // 0 => New Moon
    // 1 => Waxing Crescent Moon
    // 2 => Quarter Moon
    // 3 => Waxing Gibbous Moon
    // 4 => Full Moon
    // 5 => Waning Gibbous Moon
    // 6 => Last Quarter Moon
    // 7 => Waning Crescent Moon

    return b;
}



//END