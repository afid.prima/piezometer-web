﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WLR_DataZonaChart.aspx.cs" Inherits="PZO_PiezometerOnlineMap.Content.WLR_DataZonaChart" %>

<style>
    /*
    @page {
      size: A4 portrait ;
    }*/
    /*    @page {
        size: 7in 9.25in;
        margin: 27mm 16mm 27mm 16mm;
    }*/
    /* margin auto, to centre page on screen*/
    body {
        background-color: #3cb12f;
        height: 842px;
        width: 725px;
        margin-left: auto;
        margin-right: auto;
    }

    /*    body {
        background-color: #3cb12f;
        height: 842px;
        width: 595px;
        margin-left: auto;
        margin-right: auto;
    }*/
    /*change the margins as you want them to be.*/
    /*    @media print {
        body{
            width: 21cm;
            height: 29.7cm;
            margin: 0mm 0mm 0mm 0mm;
    } */

    /*@page {
        size: 21cm 29.7cm;
        margin: 30mm 45mm 30mm 45mm;*/
    /* change the margins as you want them to be. */
    /*}*/

    /*@media print {
        body{
            width: 21cm;
            height: 29.7cm;
            margin: 30mm 45mm 30mm 45mm;*/
    /*change the margins as you want them to be.*/
    /*}*/
    /*}*/
    /*    div.chapter, div.appendix {
        page-break-after: always;
    }*/

    div.titlepage {
        page: blank;
    }
    /*@media print {*/
    /* All your print styles go here */
    /*#header, #footer, #nav { display: none !important; } 
    }*/
    #tgldragdropRambu {
        list-style-type: none;
        cursor: move;
    }

        #tgldragdropRambu li {
            display: inline-block;
            padding: 0 10px;
            font-size: 16px;
            line-height: 30px;
            border-radius: 15px;
            margin: 4px;
        }

    .modal {
        overflow: auto !important;
    }

    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }

    table.dataTable thead tr {
        background-color: #33ac5a;
    }

    .modal-content {
        /*top: 30vh;*/
    }

    .map {
        height: 400px;
        width: 100%;
    }

    .flex-container {
        display: flex;
    }

    .flex-child {
        flex: 1;
        border: 2px solid #e0e0eb;
    }

        .flex-child:first-child {
            margin-right: 20px;
        }

    .container-long {
        /*position:relative;*/
        display: flex;
        width: 100%;
        border: solid #00a65a 1px;
        margin-bottom: 2px;
        padding: 15px;
        background-color: #e6ffcc;
        border-radius: 10px;
    }

    .label-station {
        font-weight: normal;
        font-size: 20px;
        margin: auto;
    }

    #setMasterStation {
        display: none
    }

    #setMasterStationEdit {
        display: none
    }

    #mdassignscdata {
        padding-right: 0 !important;
    }

    .summaryBulanan {
        border: 1px solid darkgreen;
        margin-bottom: 5px;
    }

    table.summaryBulanan tr {
        border: 1px solid darkgreen;
        padding: 2px
    }

    table.summaryBulanan td {
        border: 1px solid darkgreen;
        padding: 2px
    }

    .rainfallSummary {
        display: none;
    }


    /*.angry-grid {
        display: grid;
        grid-template-rows: 3fr 2fr 2fr;
        grid-template-columns: 1fr 1fr;
        gap: 0px;
        height: 100%;
        margin-left: 10%;
        margin-right: 10%;
    }

    #item-0 {*/
    /*chart utama*/
    /*grid-row-start: 1;
        grid-row-end: 2;
        grid-column-start: 1;
        grid-column-end: 3;
    }

    #item-1 {
        grid-row-start: 2;
        grid-row-end: 3;
        grid-column-start: 1;
        grid-column-end: 2;
        margin-left: 10%;
        margin-right: 10%;
    }

    #item-2 {
        grid-row-start: 2;
        grid-row-end: 3;
        grid-column-start: 2;
        grid-column-end: 3;
        margin-left: 10%;
        margin-right: 10%;
    }

    #item-3 {
        grid-row-start: 3;
        grid-row-end: 4;
        grid-column-start: 1;
        grid-column-end: 2;
        margin-left: 10%;
        margin-right: 10%;
    }

    #item-4 {
        grid-row-start: 3;
        grid-row-end: 4;
        grid-column-start: 2;
        grid-column-end: 3;
        margin-left: 10%;
        margin-right: 10%;
    }*/
    /*
    .wrapper {
        display: grid;
        grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
        grid-column-gap: 6px;
        grid-row-gap: 6px;
    }*/
</style>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Bootstrap Select -->
    <link rel="stylesheet" href="../Source/adminlte/bootstrap-select.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.dataTables.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../Source/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="../Style/Loading.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Source/adminlte/plugins/iCheck/all.css">
    <link rel="stylesheet" href="../Source/adminlte/dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/morris.js/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../Source/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/select2/dist/css/select2.min.css">
    <!-- Openlayers 4 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/openlayers4/ol.css">

    <link href="../Style/Loading.css" rel="stylesheet" />

    <%--    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../Source/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>--%>
    <script src="../Source/jquery-1.11.1.js"></script>

    <!-- CanvasJS -->
    <script src="../Plugin/canvasjs-commercial-3.6.6/canvasjs.min.js" type="text/javascript"></script>
    <%--<script src="../Source/canvasjs-2.1/canvasjs.min.js"></script>--%>

    <script src="../Source/underscore/underscore.js"></script>

    <!-- DataTables -->
    <%--<script src="../Plugin/adminlte/bower_components/datatables.net/js/dataTables.buttons.min.js"></script>--%>
    <%--<script src="../Plugin/adminlte/bower_components/datatables.net/js/buttons.flash.min.js"></script>--%>
    <script src="../Plugin/adminlte/bower_components/datatables.net/js/jszip.min.js"></script>
    <script src="../Plugin/adminlte/bower_components/datatables.net/js/pdfmake.min.js"></script>
    <script src="../Plugin/adminlte/bower_components/datatables.net/js/vfs_fonts.js"></script>
    <%--<script src="../Plugin/adminlte/bower_components/datatables.net/js/buttons.html5.min.js"></script>--%>
    <%--<script src="../Plugin/adminlte/bower_components/datatables.net/js/buttons.print.min.js"></script>--%>
    <script src="https://unpkg.com/jspdf@1.3.3/dist/jspdf.min.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@3.2.11/dist/jspdf.plugin.autotable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.0/FileSaver.min.js" integrity="sha512-csNcFYJniKjJxRWRV1R7fvnXrycHP6qDR21mgz1ZP55xY5d+aHLfo9/FcGDQLfn2IfngbAHd8LdfsagcCqgTcQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="../Plugin/jqueryPrint/html2canvas.min.js"></script>
    <script src="../Script/Language.js"></script>



    <script src="../Script/WLR_DataZonaChart.js"></script>
    <%--<script src="../Script/helper/helper.js"></script>--%>
    <script src="../Script/helperWLR.js"></script>

</head>
<body>

    <div class="loading" style="display: none; z-index: 99999999;"></div>
    <%--<br>--%>
    <%--<div id='reportMain'>--%>
    <%--        <div id='divChart' style='height: 400px; width: 800px; margin: 0px auto;'>
        </div>
        <br>
        <div style="display:flex">
                            <div id='divChart2a'  style='height: 200px; width: 45%; margin: 0px auto;' ></div>                       
                            <div id='divChart2b'  style='height: 200px; width: 45%; margin: 0px auto;' ></div>                       

        </div>

                <br>
        <div style="display:flex">
                            <div id='divChart2c'  style='height: 200px; width: 45%; margin: 0px auto;' ></div>                       
                            <div id='divChart2d'  style='height: 200px; width: 45%; margin: 0px auto;' ></div>                       

        </div>
      

        </div>--%>




    <div id='button'>
        <div class="col-md-12">
            <table style="height: 8px; width: 100%; border-collapse: collapse;" border="0">
                <tbody>
                    <tr style="height: 8px;">
                        <td style="width: 100%; height: 8px;">
                            <button id="printcharting" class="btn btn-info" type="button" >Print</button>
                        </td>
                        <%--<td> <button id="savepdf" class="btn btn-info" type="button">Save PDF</button></td>--%>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>




    <div id='reportMain' style="width=595px; height=842px;">
        <div id='reportDetail1' class="col-md-12">

            <%--        <div id='reportHeader' class="col-md-12">
            <p>Water Management</p>
            <p><strong>Analisa Tata Kelola Air per Zona</strong></p>
            <font face="Arial Narrow" size="12">
            <table style="height: 54px; width: 100%; border-collapse: collapse;" border="0">
            <tbody>
            <tr style="height: 12px;">
            <td style="width: 14.0151%; height: 12px;">WM Area&nbsp;</td>
            <td style="width: 2.22536%; height: 12px;">:</td>
            <td style="width: 61.3152%; height: 12px;">THIP Pulau A&nbsp;</td>
            <td style="width: 22.4442%; height: 12px;" colspan="3">Curah hujan :</td>
            </tr>
            <tr style="height: 12px;">
            <td style="width: 14.0151%; height: 12px;">Zona</td>
            <td style="width: 2.22536%; height: 12px;">:</td>
            <td style="width: 61.3152%; height: 12px;">A01-A (4,253 Ha.). 6.22</td>
            <td style="width: 7.48201%; height: 12px;">- HH</td>
            <td style="width: 2.51535%; height: 12px;">:</td>
            <td style="width: 12.4468%; height: 12px;">10</td>
            </tr>
            <tr style="height: 12px;">
            <td style="width: 14.0151%; height: 12px;">Periode&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 2.22536%; height: 12px;">:</td>
            <td style="width: 61.3152%; height: 12px;">Aug-W3, 2023</td>
            <td style="width: 7.48201%; height: 12px;">- CH</td>
            <td style="width: 2.51535%; height: 12px;">:</td>
            <td style="width: 12.4468%; height: 12px;">75mm</td>
            </tr>
            </tr>
            </tbody>
            </table>
            <hr style="border-top: 3px double black;border-bottom: none;">
            </font>
        </div>--%>
            <div id="konten">
                <table style="height: 54px; width: 100%; border-collapse: collapse;" border="0">
                    <tbody>
                        <tr style="height: 12px;">
                            <td style="width: 100%; height: 12px;">Water Management</td>
                        </tr>
                        <tr style="height: 12px;">
                            <td style="width: 100%; height: 12px;"><strong>Analisa Tata Kelola Air per Zona</strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <%--            <div id="kontenHead2">
                <font face="Arial Narrow" size="12">
                    <table style="height: 54px; width: 100%; border-collapse: collapse;" border="0">
                        <tbody>
                            <tr style="height: 12px;">
                                <td style="width: 14.0151%; height: 12px;">WM Area</td>
                                <td style="width: 2.22536%; height: 12px;">:</td>
                                <td style="width: 61.3152%; height: 12px;">THIP Pulau A</td>
                                <td style="width: 22.4442%; height: 12px;" colspan="3">Curah hujan :</td>
                            </tr>
                            <tr style="height: 12px;">
                                <td style="width: 14.0151%; height: 12px;">Zona</td>
                                <td style="width: 2.22536%; height: 12px;">:</td>
                                <td style="width: 61.3152%; height: 12px;">A01-A (4,253 Ha.). 6.22</td>
                                <td style="width: 7.48201%; height: 12px;">- HH</td>
                                <td style="width: 2.51535%; height: 12px;">:</td>
                                <td style="width: 12.4468%; height: 12px;">10</td>
                            </tr>
                            <tr style="height: 12px;">
                                <td style="width: 14.0151%; height: 12px;">Periode</td>
                                <td style="width: 2.22536%; height: 12px;">:</td>
                                <td style="width: 61.3152%; height: 12px;">Aug-W3, 2023</td>
                                <td style="width: 7.48201%; height: 12px;">- CH</td>
                                <td style="width: 2.51535%; height: 12px;">:</td>
                                <td style="width: 12.4468%; height: 12px;">75mm</td>
                            </tr>
                        </tbody>
                    </table>

                </font>
            </div>--%>

            <div id="kontenHead2"></div>
            <hr style="border-top: 3px double black; border-bottom: none; padding: 0px; margin-top: 5px; margin-bottom: 5px;">
            <%--            <div id="konten0">
                <font face="Arial Narrow" size="12">
                    <table style="height: 54px; width: 100%; border-collapse: collapse;" border="1">
                        <tbody>
                            <tr>
                                <td colspan="2"><b>A.  Kontrol TMAS</b></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div id="divChart" style="height: 250px; width: 100%; border: solid blue 0px;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><b>B.  Sample TMAT Piezometer</b></td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="divChart2a" style="height: 200px; width: 250px; border: solid blue 0px;"></div>
                                </td>
                                <td>
                                    <div id="divChart2b" style="height: 200px; width: 250px; border: solid blue 0px;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="divChart2c" style="height: 200px; width: 250px; border: solid blue 0px;"></div>
                                </td>
                                <td>
                                    <div id="divChart2d" style="height: 200px; width: 250px; border: solid blue 0px;"></div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2"><b>C.  Sample TMAT - AWL</b></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div id="divChartIOT" style="height: 250px; width: 100%; border: solid blue 0px;"></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </font>

            </div>--%>

            <div id="konten2">
                <font face="Arial Narrow" size="12">
                    <table style="height: 54px; width: 100%; border-collapse: collapse;" border="0">
                        <tbody>
                            <tr>
                                <td colspan="2"><b>A.  Kontrol TMAS</b></td>
                            </tr>
                            <%--<tr><b>A.  Kontrol TMAS</b></tr>--%>
                            <tr>
                                <td colspan="2">
                                    <div id="divChart" style="height: 280px; width: 100%; border: solid blue 0px;"></div>
                                </td>
                            </tr>
                            <tr id="trChart2">
                                <td colspan="2"><b>B.  Sample TMAT Piezometer</b></td>
                            </tr>
                            <%--<tr hidden>--%>
                            <tr id="trChart2a">
                                <td>
                                    <div id="divChart2a" style="height: 150px; width: 350px; border: solid blue 0px;"></div>
                                </td>
                                <td>
                                    <div id="divChart2b" style="height: 150px; width: 350px; border: solid blue 0px;"></div>
                                </td>
                            </tr>
                            <tr id="trChart2c">
                                <td>
                                    <div id="divChart2c" style="height: 150px; width: 350px; border: solid blue 0px;"></div>
                                </td>
                                <td>
                                    <div id="divChart2d" style="height: 150px; width: 350px; border: solid blue 0px;"></div>
                                </td>
                            </tr>

                            <tr id="trChart3">
                                <td colspan="2"><b>C.  Sample TMAT - AWL</b></td>
                            </tr>
                            <tr id="trChart3a">
                                <td colspan="2">
                                    <div id="divChartIOT" style="height: 280px; width: 100%; border: solid blue 0px;"></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </font>

            </div>

            <%--                <div class="row">
                    <div class="col-md-12"></div>
                    <div class="col-md-12"><b>A.  Kontrol TMAS</b></div>
                    <div class="col-md-12">
                        <div id="divChart" style="height: 250px; width: 100%; border:solid blue 0px;"></div>
                    </div>
                </div>--%>
            <%--                <div class="row">
                    <div class="col-md-12"></div>
                    <div class="col-md-12"><b>B.  Sample TMAT Piezometer</b></div>
                    <div class="col-md-6">
                         <div id="divChart2a" style="height: 200px; width: 300px; border:solid blue 0px;"></div>
                    </div>
                    <div class="col-md-6">
                         <div id="divChart2b" style="height: 200px; width: 300px; border:solid blue 0px;"></div>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-12"></div>
                    <div class="col-md-6">
                         <div id="divChart2c" style="height: 200px; width: 300px; border:solid blue 0px;"></div>
                    </div>
                    <div class="col-md-6">
                         <div id="divChart2d" style="height: 200px; width: 300px; border:solid blue 0px;"></div>
                    </div>
                </div>--%>

            <%--            <div class="row">
                <div class="col-md-12"></div>
                <div class="col-md-12"><b>C.  Sample TMAT - AWL</b></div>
                <div class="col-md-12">
                    <div id="divChartIOT" style="height: 300px; width: 100%; border: solid blue 0px;"></div>
                </div>
            </div>--%>
        </div>


    </div>



    <%--<canvas id="canvas" width="480" height="320"></canvas>--%>


    <%--<div style="display: block; padding: 80px">
      <div style="width: 100%; margin-bottom: 20px">
        <div style="background-color: aqua; width: 100%; height: 400px">
          <div id="chartContainer" style="height: 100%; width: 100%"></div>
        </div>
      </div>
      <div id="chart"
        style="
          width: 100%;
          margin-bottom: 20px;
          display: flex;
          justify-content: space-between;
          align-items: center;
        "
      >

      </div>
    </div>--%>

    <%--<div class="angry-grid">

                    <div id="item-0">
                        <p>A.	Kontrol TMAS</p>
                        <p>&nbsp;</p>
                        <div id='divChart'></div> 
                        <p>&nbsp;</p>
                    </div>
                    <div id="item-1">
                        <p>B.	Sample TMAT Piezometer</p>
                        <p>&nbsp;</p>
                        <div id='divChart2a'></div> 
                        <p>&nbsp;</p>
                    </div>
                    <div id="item-2">
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <div id='divChart2b'></div> 
                        <p>&nbsp;</p>
                    </div>
                    <div id="item-3">
                        <p>&nbsp;</p>
                        <div id='divChart2c'></div> 
                        <p>&nbsp;</p>
                    </div>
                    <div id="item-4">
                        <p>&nbsp;</p>
                        <div id='divChart2d'></div> 
                        <p>&nbsp;</p>
                    </div>




    </div>--%>


    <%--        <div class="row">
            <div class="col-md-12" id='divChart'></div>
            <div class="col-md-12">
                <div class="col-md-6" id='divChart2a'></div>
                <div class="col-md-6" id='divChart2b'></div>
            </div>
        </div>--%>



    <%--    <div class="row" >
        <div id='divChart'></div> 
    </div>--%>

    <%--  <div class="row" >
    <div class="col-md-12">
        
        <div class="col-md-6">

            <div id='divChart2a' ></div> 
        </div>
        
        <div class="col-md-6">

            <div id='divChart2b' ></div> 
        </div>
    </div>
    </div>--%>




    <%--                <div class="flex-container" >
                        <div class="flex-child">
                            <div id='divChart2a' ></div>                       
                        </div>
                        <div class="flex-child">
                            <div id='divChart2b' ></div>                       
                        </div>                    
                </div>
                <br>
      
                <div class="flex-container" >
                        <div class="flex-child">
                            <div id='divChart2c' ></div>                       
                        </div>
                        <div class="flex-child">
                            <div id='divChart2d' ></div>                       
                        </div>                    
    --%>

    <%--    <div id='divGroupChart2' style='height: 800px; width: 800px; margin: 0px auto;'>
        <div id='divChart2a' style='height: 200px; width: 200px;'>
        </div>
        <div id='divChart2b' style='height: 200px; width: 200px;'>
        </div>
        <div id='divChart2c' style='height: 200px; width: 200px;'>
        </div>
        <div id='divChart2d' style='height: 200px; width: 200px;'>
        </div>
    </div>--%>

    <%--    <div id='divGroupChart2' class="grid-container">
      <div class="grid-item"><div id='divChart2a' style='height: 200px; width: 200px;'></div> </div>
      <div class="grid-item"><div id='divChart2b' style='height: 200px; width: 200px;'></div> </div>
      <div class="grid-item"><div id='divChart2c' style='height: 200px; width: 200px;'></div> </div>
      <div class="grid-item"><div id='divChart2d' style='height: 200px; width: 200px;'></div> </div>
    </div>--%>




    <%--            <div class="modal-body">
                <div class="flex-container" style='height: 800px; width: 800px;'>
                    <div class="flex-child">
                        <div class="flex-child">
                            <div id='divChart2a' style='height: 200px; width: 200px;'></div>                    
                        </div>
                        <div class="flex-child">
                            <div id='divChart2b' style='height: 200px; width: 200px;'></div>                       
                        </div>                    
                    </div>
                    <div class="flex-child">
                        <div class="flex-child">
                            <div id='divChart2c' style='height: 200px; width: 200px;'></div>                       
                        </div>
                        <div class="flex-child">
                            <div id='divChart2d' style='height: 200px; width: 200px;'></div>                       
                        </div>                    
                    </div>
                </div>
            </div>--%>






    <%--<div id='divCounting' style="height: 100px; width: 100%; margin: 0px auto; align: left;"></div>--%>
    <%--<button id='btnClose' onclick="return closeWindow();">
	Close Window
        
</button>--%>
</body>


<script type="text/javascript">
</script>
</html>
