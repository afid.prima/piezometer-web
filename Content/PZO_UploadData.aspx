﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PZO_UploadData.aspx.cs" Inherits="PZO_PiezometerOnlineMap.Content.PZO_UploadData" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Map | Piezometer</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Bootstrap Select -->
    <link rel="stylesheet" href="../Source/adminlte/bootstrap-select.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.dataTables.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../Source/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="../Source/adminlte/plugins/iCheck/all.css">
    <link rel="stylesheet" href="../Source/adminlte/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../Source/css/jquery-confirm.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="../Style/Loading.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Source/DataTable/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.bootstrap4.min.css" />

    <style>
        .table-text-center th, .table-text-center td {
            text-align: center;
        }
    </style>
</head>
<body class="hold-transition skin-green-light sidebar-mini fixed">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="#" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>PZO</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Piezometer</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    <li>
                        <a href="PZO_Dashboard.aspx">
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <%--<li>
                        <a href="PZO_Map.aspx">
                            <i class="fa fa-map"></i>
                            <span>Online Map</span>
                        </a>
                    </li>--%>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-map"></i><span>Online Map</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a id="map_default" href="PZO_Map.aspx"><i class="fa fa-circle-o"></i>Default Map</a></li>
                            <li><a id="map_analysis" href="PZO_MapAnalysis.aspx"><i class="fa fa-circle-o"></i>Analysis Map</a></li>
                            <%--<li><a id="olmTMAS" href="PZO_NewAnalysisTMAS.aspx"><i class="fa fa-circle-o"></i>Analysis Map for TMAS</a></li>--%>
                        </ul>
                    </li>
                    <li>
                        <a href="PZO_DownloadData.aspx">
                            <i class="fa fa-files-o"></i>
                            <span>Download Data</span>
                        </a>
                    </li>
                   <%-- <li class="active">
                        <a href="#">
                            <i class="fa fa-files-o"></i>
                            <span>Upload Data</span>
                        </a>
                    </li>--%>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-gear"></i><span>Maintenance Data</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a id="master_hobo" href="PZO_MasterPiezo.aspx"><i class="fa fa-circle-o"></i>Master Piezo</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="?act=LogOut">
                            <i class="fa fa-sign-out"></i>
                            <span id="txtLogout" runat="server">Keluar</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!--<section class="content-header">
            </section>-->

            <!-- Main content -->
            <section class="content">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#uphobo" data-toggle="tab">Upload File Hobo</a></li>
                        <li style="display: none"><a href="#upexcel" data-toggle="tab">Upload File Excel</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="uphobo">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Company</label>
                                        <select id="selectFilterCompany" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2" style="display: none">
                                    <div class="form-group">
                                        <label>Block</label>
                                        <select id="selectFilterBlock" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2" id="divPeriode">
                                    <label>Periode</label>
                                    <div class="form-group">
                                        <select id="selectFilterPeriodeHOBO" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Year</label>
                                        <div class="form-group">
                                            <select id="selectFilterYearHOBO" class="form-control selectpicker" data-live-search="true">
                                                <option value="2019">2019</option>
                                                <option selected value="2020">2020</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <%--                               <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="visibility: hidden; margin-bottom: 12px;">Year</label>
                                        <div class="form-group">
                                            <input id="fileUpload" type="file" name="upload_file" required="" accept=".hobo" />
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="visibility: hidden; margin-bottom: 5px;">Periode</label>
                                        <div class="form-group">
                                            <button type="button" id="btnSearchHOBODoc" class="btn btn-success pull-right">Search</button>
                                        </div>
                                    </div>
                                </div>
                                <br />
                            </div>
                            <div class="row">
                                <div class="col-md-2" id="divPeriodeReport">
                                    <label>Periode Report</label>
                                    <div class="form-group">
                                        <select id="selectFilterPeriodeReport" class="form-control selectpicker" data-live-search="true">
                                            <option value="1">PERIODE 1 (Jan-Mar)</option>
                                            <option value="4">PERIODE 2 (Apr-Jun)</option>
                                            <option value="7">PERIODE 3 (Jul-Sep)</option>
                                            <option value="10">PERIODE 4 (Okt-Des)</option>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <div class="col-md-12">
                                    <div class="col-md-2" style="display: flex">
                                        <div class="form-group">
                                            <label style="visibility: hidden; margin-bottom: 5px;">Periode</label>
                                            <div class="form-group">
                                                <button type="button" id="btnDownloadReport" class="btn btn-success pull-right">Download Report</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="divTableHOBODoc"></div>

                            <div class="modal fade" data-backdrop="static" id="mdlImportData" tabindex="-1" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Detail Excel</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div id="divTableDetailExcelDoc"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <%--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>--%>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                        <div class="tab-pane" id="upexcel">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Company</label>
                                        <select id="selectFilterCompanyExcel" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Block</label>
                                        <select id="selectFilterBlockExcel" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2" id="divPeriodeExcel">
                                    <label>Periode</label>
                                    <div class="form-group">
                                        <select id="selectFilterPeriodeExcel" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Year</label>
                                        <div class="form-group">
                                            <select id="selectFilterYearExcel" class="form-control selectpicker" data-live-search="true">
                                                <option value="2019">2019</option>
                                                <option selected value="2020">2020</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label style="visibility: hidden; margin-bottom: 12px;">Periode</label>
                                    <div class="form-group">
                                        <input id="fileUploadExcel" type="file" name="upload_file" required="" accept=".xlsx" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label style="visibility: hidden; margin-bottom: 12px;">Periode</label>
                                    <div class="form-group">
                                        <button type="button" id="btnsaveFileUploadExcel" class="btn btn-success pull-right">Save</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <label style="visibility: hidden; margin-bottom: 12px;">Periode</label>
                                    <div class="form-group">
                                        <button type="button" id="btnSearchExcelDoc" class="btn btn-success pull-right">Search</button>
                                    </div>
                                </div>
                                <div class="col-md-2" style="margin-left: -45px;">
                                    <label style="visibility: hidden; margin-bottom: 12px;">Periode</label>
                                    <div class="form-group">
                                        <button type="button" id="btnDownloadExcelDoc" class="btn btn-success pull-right">Download Report</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
    <div class="loading" style="display: none; z-index: 99999999;"></div>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../Source/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../Source/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Bootstrap Select -->
    <script src="../Source/adminlte/bootstrap-select.js"></script>
    <!-- DataTables -->
    <script src="../Source/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/dataTables.buttons.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.flash.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/jszip.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/pdfmake.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/vfs_fonts.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.html5.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.print.min.js"></script>
    <%--  <script src="../Source/DataTable/js/dataTables.bootstrap4.min.js"></script>
    <script src="../Source/DataTable/js/dataTables.fixedHeader.js"></script>--%>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"></script>

    <!-- daterangepicker -->
    <script src="../Source/adminlte/bower_components/moment/min/moment.min.js"></script>
    <script src="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../Source/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../Source/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../Source/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../Source/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../Source/adminlte/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../Source/adminlte/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../Source/adminlte/dist/js/demo.js"></script>
    <!-- This Page Main JS -->
    <script src="../Source/adminlte/plugins/iCheck/icheck.min.js"></script>
    <script src="../Source/js/jquery-confirm.js"></script>
    <script src="../Script/ol4/uploaddata.js"></script>

</body>
</html>

