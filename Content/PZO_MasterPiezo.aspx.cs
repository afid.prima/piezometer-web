﻿using PZO_PiezometerOnlineMap.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PZO_PiezometerOnlineMap.Content
{
    public partial class PZO_MasterPiezo : System.Web.UI.Page
    {
        private LastUpdateFile lastUpdateFile = new LastUpdateFile();
        public string masterPiezo_js { get; set; }
        public string graphMasterPiezoTMAT_js { get; set; }
        public string formTMATIOT_js { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            string filePath;
            string fileVersion;
            filePath = Server.MapPath("~/Script/ol4/masterPiezo.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            masterPiezo_js = $"../Script/ol4/masterPiezo.js?ver={fileVersion}";

            filePath = Server.MapPath("~/Script/masterPiezo/graphMasterPiezoTMAT.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            graphMasterPiezoTMAT_js = $"../Script/masterPiezo/graphMasterPiezoTMAT.js?ver={fileVersion}";

            filePath = Server.MapPath("~/Script/masterPiezo/formTMATIOT.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            formTMATIOT_js = $"../Script/masterPiezo/formTMATIOT.js?ver={fileVersion}";

            if (Session["uID"] == null || Session["uID"].ToString() == "")
            {
                LogOut();
            }
            else
            {
                userid.Value = Session["uID"].ToString();
            }
        }
        protected void LogOut()
        {
            if (!string.IsNullOrEmpty(Session["PZO5"] as string))
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            //Session.Abandon();

        }
    }
}