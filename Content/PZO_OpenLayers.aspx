﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PZO_OpenLayers.aspx.cs" Inherits="PZO_PiezometerOnlineMap.Content.PZO_OpenLayers" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Map | Piezometer</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link href="../Source/adminlte/bower_components/openlayers4.6.5/ol.css" rel="stylesheet" />
</head>
<body>
    <div>
        <button type="button" id="btnshowpopup" class="btn btn-success">Show</button>
    </div>

    <div id="mymodal" class="modal">
        <div class="modal-dialog col col-lg-8">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">OpenLayers Map</h4>
                    <div id="coordinate"></div>
                </div>
                <div class="modal-body">
                    <div id="map" style="width: 500px; height: 500px; display : inline-block;" class="map"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" aria-hidden="true">OK</button>
                </div>
            </div>
        </div>
    </div>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../Source/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../Source/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../Source/adminlte/bower_components/openlayers4.6.5/ol.js"></script>
    <script>
        var map;
        var selectInteraction, translateInteraction;
        $(function () {
            var rome = new ol.Feature({
                geometry: new ol.geom.Point(ol.proj.fromLonLat([12.5, 41.9]))
            });

            var london = new ol.Feature({
                geometry: new ol.geom.Point(ol.proj.fromLonLat([-0.12755, 51.507222]))
            });

            var madrid = new ol.Feature({
                geometry: new ol.geom.Point(ol.proj.fromLonLat([-3.683333, 40.4]))
            });

            rome.setStyle(new ol.style.Style({
                image: new ol.style.Icon(/** @type {olx.style.IconOptions} */({
                    color: '#8959A8',
                    crossOrigin: 'anonymous',
                    src: 'https://openlayers.org/en/v4.6.5/examples/data/dot.png'
                }))
            }));

            london.setStyle(new ol.style.Style({
                image: new ol.style.Icon(/** @type {olx.style.IconOptions} */({
                    color: '#4271AE',
                    crossOrigin: 'anonymous',
                    src: 'https://openlayers.org/en/v4.6.5/examples/data/dot.png'
                }))
            }));

            madrid.setStyle(new ol.style.Style({
                image: new ol.style.Icon(/** @type {olx.style.IconOptions} */({
                    color: [113, 140, 0],
                    crossOrigin: 'anonymous',
                    src: 'https://openlayers.org/en/v4.6.5/examples/data/dot.png'
                }))
            }));


            var vectorSource = new ol.source.Vector({
                features: [rome, london, madrid]
            });

            var vectorLayer = new ol.layer.Vector({
                source: vectorSource
            });

            map = new ol.Map({
                layers: [
                  new ol.layer.Tile({
                      source: new ol.source.OSM()
                  }), vectorLayer
                ],
                target: 'map',
                controls: ol.control.defaults({
                    attributionOptions: {
                        collapsible: false
                    }
                }),
                view: new ol.View({
                    center: [0, 0],
                    //projection: "EPSG:4326",
                    zoom: 2
                })
            });

            selectInteraction = new ol.interaction.Select({
                layers: [vectorLayer],
                condition: ol.events.condition.click
            });
            map.addInteraction(selectInteraction);

            translateInteraction = new ol.interaction.Translate({
                features: selectInteraction.getFeatures()
            });
            map.getInteractions().extend([selectInteraction, translateInteraction]);

            translateInteraction.on('translating', evt => {
                evt.features.forEach(feature => {
                    $('#coordinate').html(feature.getGeometry().getCoordinates()[0] + "," + feature.getGeometry().getCoordinates()[1]);
                        console.log(feature.getGeometry().getCoordinates()[0] + " - " + feature.getGeometry().getCoordinates()[1]);
                })
            });

            translateInteraction.on('translateend', evt => {
                evt.features.forEach(feature => {
                    //console.log(feature.getGeometry().getCoordinates()[0] + " - " + feature.getGeometry().getCoordinates()[1]);
                })
            });

            $('#btnshowpopup').click(function () {
                $('#mymodal').modal('show');
                map.updateSize();
            })

        })
    </script>
</body>
</html>
