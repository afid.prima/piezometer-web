﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PZO_Dashboard.aspx.cs" Inherits="PZO_PiezometerOnlineMap.Content.PZO_Dashboard" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Map | Piezometer</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Bootstrap Select -->
    <link rel="stylesheet" href="../Source/adminlte/bootstrap-select.css">
    <!-- DataTables -->
    <link href="../Source/adminlte/bower_components/datatables.net-1.10.16/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.dataTables.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../Source/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="../Source/adminlte/plugins/iCheck/all.css">
    <link rel="stylesheet" href="../Source/adminlte/dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/morris.js/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../Source/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- Loading Style -->
    <link href="../Style/Loading.css" rel="stylesheet" />
    <link href="../Source/adminlte/bower_components/jquery-ui/1.12.1/jquery-ui.css" rel="stylesheet" />
    <%--<style>
        .modal-dialog {
            position: fixed;
            width: 400px;
            margin: 0;
            padding: 10px;
        }

    </style>--%>
</head>
<body class="hold-transition skin-green-light sidebar-mini fixed">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="#" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>PZO</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Piezometer</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <%--  <div class="navbar-static-top">                          
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="hidden-xs" id="userHeader" runat="server"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header" style="height: 50px;">
                                        <p id="userHeaderP" runat="server">
                                           
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="?WLR=LogOut" id="btnLogOut" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>--%>
            </nav>
        </header>
        <%--      <!-- Logo -->
            <a href="#" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>PZO</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Piezometer</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                  <ul class="nav navbar-nav" style="position: absolute; right: 0;">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="hidden-xs" id="userHeader" runat="server">Alexander Pierce</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header" style="height: 50px;">
                                        <p id="userHeaderP" runat="server">
                                            Alexander Pierce - Web Developer
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div>
                                            <a href="?WLR=LogOut" id="btnLogOut" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
            </nav>--%>

        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header" id="liheader" runat="server">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="#">
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <%--<li>
                        <a href="PZO_Map.aspx">
                            <i class="fa fa-map"></i>
                            <span>Online Map</span>
                        </a>
                    </li>--%>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-map"></i><span>Online Map</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a id="map_default" href="PZO_Map.aspx"><i class="fa fa-circle-o"></i>Default Map</a></li>
                            <li><a id="map_analysis" href="PZO_MapAnalysis.aspx"><i class="fa fa-circle-o"></i>Analysis Map</a></li>
                            <%--<li><a id="olmTMAS" href="PZO_NewAnalysisTMAS.aspx"><i class="fa fa-circle-o"></i>Analysis Map for TMAS</a></li>
                            <li><a id="olmTMASxx" href="PZO_MapByPT.aspx"><i class="fa fa-circle-o"></i>Online Map by PT</a></li> --%>
                        </ul>
                    </li>
                    <li id="liDownloadData">
                        <a href="PZO_DownloadData.aspx">
                            <i class="fa fa-files-o"></i>
                            <span>Download Data</span>
                        </a>
                    </li>
                   <%-- <li id="liUploadData">
                        <a href="PZO_UploadData.aspx">
                            <i class="fa fa-files-o"></i>
                            <span>Upload Data</span>
                        </a>
                    </li>--%>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-gear"></i><span>Maintenance Data</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a id="master_hobo" href="PZO_MasterPiezo.aspx"><i class="fa fa-circle-o"></i>Master Piezo</a></li>
                        </ul>
                    </li>
                    <li>
                        <a id="linklogout" href="?_PZO_Map=LogOut">
                            <i class="fa fa-sign-out"></i>
                            <span id="txtLogout" runat="server">Keluar</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <%--<!-- Content Header (Page header) -->
            <section class="content-header">
            </section>--%>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <button type="button" id="btnPrevWeek" class="btn btn-success"><i class="fa fa-angle-left"></i></button>
                                <label id="lblTitleWeek">Week Name </label>
                                <button type="button" id="btnNextWeek" class="btn btn-success"><i class="fa fa-angle-right"></i></button>
                                <button type="button" id="btnThisWeek" class="btn btn-success">This Week</button>
                                <button type="button" id="btnGenerateData" class="btn btn-success">Generate Data</button>
                                <label id="lblLastData"></label>
                                <div style="display: none;">
                                    <h3 class="box-title">Filter</h3>
                                    &nbsp;&nbsp;
                                <select id="selectFilterCompany" class="selectpicker" data-live-search="true" data-width="auto">
                                </select>
                                    <select id="selectFilterEstate" class="selectpicker" data-live-search="true" data-width="auto">
                                    </select>
                                    <button type="button" id="btnSubmitFilter" class="btn btn-success">Go</button>
                                </div>
                                <div class="loading" style="display: none; z-index: 99999999;"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Progress This Week</h3>
                                <span style="margin-left: 6px;">
                                    <label>
                                        <input type="radio" id="rbSummaryKondisi" name="tipedashboard" class="flat-green" checked="checked">
                                        Summary Kondisi
                                    </label>
                                </span>
                                <span style="margin-left: 6px;">
                                    <label>
                                        <input type="radio" id="rbByPiezoMaster" name="tipedashboard" class="flat-green">
                                        Progress By Piezo Master
                                    </label>
                                </span>
                                <span style="margin-left: 6px;">
                                    <label>
                                        <input type="radio" id="rbByProgressPercentage" name="tipedashboard" class="flat-green">
                                        Progress By Percentage
                                    </label>
                                </span>
                                <%--<div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>--%>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-9">

                                        <p class="text-center">
                                            <button type="button" id="btnCloseSubChart" class="btn btn-success"><i class="fa fa-angle-left"></i></button>
                                            &nbsp;<strong><label id="lblCompanyCode" style="display: none;"></label><label id="lblWeekName" style="display: none;"></label><label id="lblDateRange"></label></strong>
                                        </p>

                                        <div id="mainchart" class="chart">
                                            <!-- Piezometer Canvas -->
                                            <div id="divChartPiezometer"></div>
                                        </div>
                                        <div id="subchart" class="chart">
                                            <!-- Piezometer Sub Canvas -->
                                            <div id="divSubChartPiezometer"></div>
                                        </div>
                                        <!-- /.chart-responsive -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3">
                                        <p class="text-center">
                                            <strong>Goal Completion</strong>
                                        </p>
                                        <div style="max-height: 400px; overflow: auto;">
                                            <div id="goalcompletion"></div>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <div style="max-height: 400px; overflow: auto;">
                                    <div id="time" style="margin-left: 825px;">
                                        <label><b>Tanggal/Waktu : </b></label>
                                        <label id="lblTime"><b></b></label>
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- ./box-body -->
                            <%-- <div class="box-footer">
                                <div class="row">
                                    <div class="col-sm-3 col-xs-6">
                                        <div class="description-block border-right">
                                            <span class="description-percentage text-green"><i class="fa fa-caret-up"></i>17%</span>
                                            <h5 class="description-header">$35,210.43</h5>
                                            <span class="description-text">TOTAL REVENUE</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-3 col-xs-6">
                                        <div class="description-block border-right">
                                            <span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i>0%</span>
                                            <h5 class="description-header">$10,390.90</h5>
                                            <span class="description-text">TOTAL COST</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-3 col-xs-6">
                                        <div class="description-block border-right">
                                            <span class="description-percentage text-green"><i class="fa fa-caret-up"></i>20%</span>
                                            <h5 class="description-header">$24,813.53</h5>
                                            <span class="description-text">TOTAL PROFIT</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-3 col-xs-6">
                                        <div class="description-block">
                                            <span class="description-percentage text-red"><i class="fa fa-caret-down"></i>18%</span>
                                            <h5 class="description-header">1200</h5>
                                            <span class="description-text">GOAL COMPLETIONS</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-footer -->--%>
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Data Pengukuran</h3>
                                &nbsp;&nbsp;
                                <%--<button type="button" id="btnPrevWeek" class="btn btn-success"><i class="fa fa-angle-left"></i></button>
                                <label id="lblTitleWeek"> Week 1 Maret 2018 </label>
                                <button type="button" id="btnNextWeek" class="btn btn-success"><i class="fa fa-angle-right"></i></button>
                                <button type="button" id="btnThisWeek" class="btn btn-success">This Week</button>--%>
                                <%--<div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>--%>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div id="divChartDataPengukuran"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box">
                            <div class="box-header with-border">
                                <h3 class="box-title">Sumber Data</h3>

                                <%--<div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>--%>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div id="divChartSumberData"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>

    <div id="dialog" title="Basic dialog" style="display: none;">
        
        <div style="float: right;margin-bottom:5px;">
            
            <div style="float: right; margin-left: 5px;">
                <button type='button' id='btnRODPZORPT2a' class='btn btn-success btnRODPZORPT2a'>RPT2a - Summary Hasil Inspeksi</button>
            </div>

            <div style="float: right; margin-left: 5px;">
                <button type='button' id='btnRODPZORPT2' class='btn btn-success btnRODPZORPT2'>RPT2 - Summary Inspeksi</button>
            </div>

            <div style="float: right; margin-left: 5px;">
                <button type='button' id='btnWeeklyReportV2' class='btn btn-success btnWeeklyReportV2' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing">RPT1a - Weekly Report V2</button>
            </div>


    <%--        <div style="float: right; margin-left: 5px;">
                <button type='button' id='btnWeeklyReport' class='btn btn-success' data-loading-text="<i class='fa fa-spinner fa-spin'></i> Processing">Weekly Report</button>
            </div>--%>
<%--            <div style="clear: both"></div>

            <div style="float: right;">
                <button type='button' id='btnGoToMap' class='btn btn-success'>Go To Map</button>
            </div>--%>

        </div>


        <div style="float: right;margin-bottom:5px;">

            <div style="float: right; margin-left: 5px;">
                <button type='button' id='btnRODPZORPT4' class='btn btn-success btnRODPZORPT4'>RPT4 - Laporan Piezometer Tahunan</button>
            </div>
            <div style="float: right; margin-left: 5px;">
                <button type='button' id='btnRODPZORPT3' class='btn btn-success btnRODPZORPT3'>RPT3 - Laporan List User Download</button>
            </div>
<%--            <div style="float: right; margin-left: 5px;">
                <button type='button' id='btnRODPZORPT2a' class='btn btn-success btnRODPZORPT2a'>RPT2a - Summary Hasil Inspeksi</button>
            </div>--%>

        </div>


            <div style="float: right;">
                <button type='button' id='btnGoToMap' class='btn btn-success'>Go To Map</button>
            </div>
            <div style="clear: both"></div>
            <div id="chartDataPengukuran_Popup" style="height: 400px; width: 100%;"></div>

    </div>
    <%-- <div id="content-popover" style="position:fixed; left:50%; top:50%;">
    </div>

    <div class="modal fade" id="popupchart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="position:relative;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Chart</h4>
                </div>
                <div class="modal-body" >
                    <div id="popupchart_content"></div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>--%>
    <form id="Form1" runat="server">
        <asp:HiddenField ID="userid" runat="server" />
    </form>

    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../Source/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <%-- <!-- jQuery UI 1.11.4 -->
    <script src="../Source/adminlte/bower_components/jquery-ui/1.12.1/jquery-ui.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>--%>
    <!-- Bootstrap 3.3.7 -->
    <script src="../Source/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Bootstrap Select -->
    <script src="../Source/adminlte/bootstrap-select.js"></script>
    <!-- CanvasJS -->
    <script src="../Source/adminlte/bower_components/canvasjs/jquery.canvasjs.min.js"></script>
    <!-- ChartJS -->
    <script src="../Source/adminlte/bower_components/chart.js/Chart.js"></script>
    <!-- DataTables -->
    <script src="../Source/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../Source/adminlte/bower_components/jquery-ui/1.12.1/jquery-ui.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>


    <!-- iCheck 1.0.1 -->
    <script src="../Source/adminlte/plugins/iCheck/icheck.min.js"></script>

    <!-- daterangepicker -->
    <script src="../Source/adminlte/bower_components/moment/min/moment.min.js"></script>
    <script src="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../Source/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../Source/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../Source/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../Source/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../Source/adminlte/dist/js/adminlte.min.js"></script>
    <%-- <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../Source/adminlte/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../Source/adminlte/dist/js/demo.js"></script>--%>

    <!-- JS For Dashboard Menu -->
    <script src="../Script/ol4/dashboard.js"></script>
</body>
</html>

