﻿using System;
using System.Data;
using System.Web.UI;
using PASS.Mapper;
using PZO_PiezometerOnlineMap.Class;
using PZO_PiezometerOnlineMap.Mapper;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace PZO_PiezometerOnlineMap.Content
{
    public partial class PZO_MapByPT : System.Web.UI.Page
    {
        #region Variable Declaration

        private PZO_ProjectMapper mapper = new PZO_ProjectMapper();
        private LastUpdateFile lastUpdateFile = new LastUpdateFile();
        public string downloaddata_js { get; set; }

        private PZO_ProjectMapperTambahan mapper2 = new PZO_ProjectMapperTambahan();
        private OLM_OnlineMapper olmMapper = new OLM_OnlineMapper();
        private Cryptography crypto = new Cryptography();

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            string filePath;
            string fileVersion;
            filePath = Server.MapPath("~/Script/ol4/downloaddata.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            downloaddata_js = $"../Script/ol4/downloaddata.js?ver={fileVersion}";

            if (Session["uID"].ToString() != null && Session["uID"].ToString() != "")
            {
                txtLogout.InnerText = "Keluar" + " " + "(" + Session["uname"].ToString() + ")";
                //liheader.InnerText = Session["uname"].ToString();
            }
            string menu = Request.Params["act"];
            if (menu == "LogOut")
            {
                LogOut();
            }
            try
            {
                bool isValid = true;
                //set company dan estate
                if (Request.Params["Company"] != null && Request.Params["Estate"] != null)
                {
                    mapped_estate.Value = mapper.GetMappedEstate(Request.Params["Estate"]);
                    estate.Value = Request.Params["Estate"];
                    company.Value = Request.Params["Company"];
                    Session["Company"] = company.Value;
                    Session["Estate"] = estate.Value;
                }
                else if (Session["Company"] != null && Session["Estate"] != null)
                {
                    mapped_estate.Value = mapper.GetMappedEstate(Session["Estate"].ToString());
                    estate.Value = Session["Estate"].ToString();
                    company.Value = Session["Company"].ToString();
                }
                else
                {
                    mapped_estate.Value = mapper.GetMappedEstate("THP1");
                    estate.Value = "THP1";
                    company.Value = "PT.THIP";
                    Session["Company"] = company.Value;
                    Session["Estate"] = estate.Value;
                    //isValid = false;
                }

                //setidwm
                if (Request.Params["IDWM"] != null)
                {
                    idwm.Value = Request.Params["IDWM"];
                    Session["Idwm"] = idwm.Value;
                }
                else if (Session["IDWM"] != null)
                {
                    idwm.Value = Session["IDWM"].ToString();
                }
                else
                {
                    idwm.Value = "1";
                    Session["IDWM"] = idwm.Value;
                }

                if (isValid)
                {
                    DataSet ds = mapper.GetEstateDetail(mapped_estate.Value);
                    DataTable dt = ds.Tables[0];
                    DataTable dtUTMProjection = ds.Tables[1];
                    DataTable dtWeek = new DataTable();
                    DataTable dtWeekX = new DataTable();
                    DataTable dtListWeek = mapper.GetListWeek();
                    DataTable dtWaterDeep = mapper2.GetWaterDepthIndicator();
                    DataTable dtwmarea = mapper2.getAllWMArea();
                    utmprojection.Value = dtUTMProjection.Rows[0]["UTMProjection"].ToString();
                    rsid.Value = dt.Rows[0]["Projection"].ToString();
                    extent.Value = dt.Rows[0]["CoordinateExtent"].ToString();
                    numzoom.Value = dt.Rows[0]["NumZoom"].ToString();
                    legend.Value = "";
                    estatezone.Value = mapper.GetZone(mapped_estate.Value);

                    listyear.Value = ObjectToJSON(dtListWeek.DefaultView.ToTable(true, "Year"));
                    listmonth.Value = ObjectToJSON(dtListWeek.DefaultView.ToTable(true, "Year", "Month", "MonthName"));
                    listweek.Value = ObjectToJSON(dtListWeek);
                    listAllWMArea.Value = ObjectToJSON(dtwmarea);
                    listWaterDeep.Value = ObjectToJSON(dtWaterDeep);

                    //jika masuk query
                    if (Request.Params["masukQuery"] == null || Request.Params["masukQuery"] == "")
                    {
                        masukQuery.Value = "false";
                    }
                    else
                    {
                        masukQuery.Value = Request.Params["masukQuery"];
                    }

                    //getWeekDari
                    if (Request.Params["IDWEEK"] == null || Request.Params["IDWEEK"] == "")
                    {
                        dtWeek = mapper.GetWeekFromCurrentDate();
                    }
                    else
                    {
                        dtWeek = mapper.GetWeekFromParameter(Request.Params["IDWEEK"]);
                    }

                    //getWeek Sampai
                    if (Request.Params["IDWEEKX"] == null || Request.Params["IDWEEKX"] == "")
                    {
                        dtWeekX = mapper.GetWeekFromCurrentDate();
                    }
                    else
                    {
                        dtWeekX = mapper.GetWeekFromParameter(Request.Params["IDWEEKX"]);
                    }

                    //getDeepWater
                    if (Request.Params["KONDISI"] == null || Request.Params["KONDISI"] == "")
                    {
                        kondisi.Value = "4";
                    }
                    else
                    {
                        kondisi.Value = Request.Params["KONDISI"];
                    }

                    //getFilterOperatro
                    if (Request.Params["FILTER"] == null || Request.Params["FILTER"] == "")
                    {
                        filter.Value = "1";
                        minFilter.Value = "";
                        operatorfilter.Value = "";
                    }
                    else
                    {
                        minFilter.Value = "";
                        operatorfilter.Value = "<";
                        berkali.Value = "true";
                        if (Request.Params["FILTER"] == "1")
                        {
                            minFilter.Value = Request.Params["MINF"];
                            operatorfilter.Value = Request.Params["OPERATORF"];
                            berkali.Value = Request.Params["BERKALI"];
                        }
                        else if (Request.Params["FILTER"] == "4")
                        {
                            berkali.Value = Request.Params["BERKALI"];
                        }
                        filter.Value = Request.Params["FILTER"];
                    }

                    year.Value = dtWeek.Rows[0]["Year"].ToString();
                    month.Value = dtWeek.Rows[0]["Month"].ToString();
                    monthname.Value = dtWeek.Rows[0]["MonthName"].ToString();
                    week.Value = dtWeek.Rows[0]["Week"].ToString();
                    monthnamealias.Value = dtWeek.Rows[0]["MonthName"].ToString();

                    yearX.Value = dtWeekX.Rows[0]["Year"].ToString();
                    monthX.Value = dtWeekX.Rows[0]["Month"].ToString();
                    monthnameX.Value = dtWeekX.Rows[0]["MonthName"].ToString();
                    weekX.Value = dtWeekX.Rows[0]["Week"].ToString();
                    monthnamealiasX.Value = dtWeekX.Rows[0]["MonthName"].ToString();


                    idweek.Value = dtWeek.Rows[0]["ID"].ToString();
                    idweekX.Value = dtWeekX.Rows[0]["ID"].ToString();
                    currentextent.Value = Request.Params["Extent"] != null ? Request.Params["Extent"] : "";
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + "Incorrect Parameters" + "')", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + "No Data" + " (" + ex.Message + ")" + "')", true);
            }
        }

        private string ObjectToJSON(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        protected void LogOut()
        {
            if (!string.IsNullOrEmpty(Session["PZO5"] as string))
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            //Session.Abandon();

        }
    }
}