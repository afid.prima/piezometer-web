﻿using PZO_PiezometerOnlineMap.Class;
using PZO_PiezometerOnlineMap.Mapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PZO_PiezometerOnlineMap.Content
{
    public partial class PZO_DownloadData : System.Web.UI.Page
    {

        private PZO_ProjectMapperTambahan mapper2 = new PZO_ProjectMapperTambahan();
        private LastUpdateFile lastUpdateFile = new LastUpdateFile();
        public string downloaddata_js { get; set; }
        public string chartTmasTmat_js { get; set; }
        public string graphTMAT_js { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Menambahkan header CORS
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");

            // Untuk menangani preflight requests (OPTIONS)
            if (Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.Response.StatusCode = 204;
                HttpContext.Current.Response.End();
            }

            string filePath;
            string fileVersion;
            filePath = Server.MapPath("~/Script/ol4/downloaddata.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            downloaddata_js = $"../Script/ol4/downloaddata.js?ver={fileVersion}";

            filePath = Server.MapPath("~/Script/ol4/chartTmasTmat.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            chartTmasTmat_js = $"../Script/ol4/chartTmasTmat.js?ver={fileVersion}";

            filePath = Server.MapPath("~/Script/ol4/graphTMAT.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            graphTMAT_js = $"../Script/ol4/graphTMAT.js?ver={fileVersion}";

            if (Session["uID"] == null)
            {
                LogOut();
            }
            if (Session["uID"].ToString() != null && Session["uID"].ToString() != "")
            {

                DataTable dtWaterDeep = mapper2.GetWaterDepthIndicator();
                DataTable dtAllGisUser = mapper2.GetAllUserGis();
                userid.Value = Session["uID"].ToString();
                listWaterDeep.Value = ObjectToJSON(dtWaterDeep);
                listGisUser.Value = ObjectToJSON(dtAllGisUser);
                txtLogout.InnerText = "Keluar" + " " + "(" + Session["uname"].ToString() + ")";
                //liheader.InnerText = Session["uname"].ToString();
            }
            else
            {
                LogOut();
            }
            string menu = Request.Params["act"];
            if (menu == "LogOut")
            {
                LogOut();
            }

        }
        private string ObjectToJSON(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        protected void LogOut()
        {
            if (!string.IsNullOrEmpty(Session["PZO5"] as string))
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            //Session.Abandon();

        }
    }
}