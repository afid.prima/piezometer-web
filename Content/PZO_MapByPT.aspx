﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PZO_MapByPT.aspx.cs" Inherits="PZO_PiezometerOnlineMap.Content.PZO_MapByPT" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Map | Piezometer</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Bootstrap Select -->
    <link rel="stylesheet" href="../Source/adminlte/bootstrap-select.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.dataTables.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../Source/adminlte/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="../Source/adminlte/plugins/iCheck/all.css">
    <link rel="stylesheet" href="../Source/adminlte/dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/morris.js/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../Source/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/select2/dist/css/select2.min.css">
    <!-- Openlayers 4 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/openlayers4/ol.css">
    <link rel="stylesheet" href="../Source/DataTable/css/dataTables.bootstrap4.min.css" />



    <style>
        #map .ol-zoom .ol-zoom-out {
            margin-top: 204px;
        }

        #map .ol-zoomslider {
            background-color: transparent;
            top: 2.3em;
        }

        #map .ol-touch .ol-zoom .ol-zoom-out {
            margin-top: 212px;
        }

        #map .ol-touch .ol-zoomslider {
            top: 2.75em;
        }

        #map .ol-zoom-in.ol-has-tooltip:hover [role=tooltip],
        #map .ol-zoom-in.ol-has-tooltip:focus [role=tooltip] {
            top: 3px;
        }

        #map .ol-zoom-out.ol-has-tooltip:hover [role=tooltip],
        #map .ol-zoom-out.ol-has-tooltip:focus [role=tooltip] {
            top: 232px;
        }

        .scale-line {
            position: absolute;
            top: 60px;
            margin-left: 50px;
        }

        .ol-scale-line {
            position: relative;
            bottom: 0px;
        }

        .mouse-position {
            width: 200px;
            text-align: center;
            position: absolute;
            top: 60px;
            margin-left: 200px;
            font-size: small;
            color: white;
        }

        .ol-mouse-position {
            position: relative;
            bottom: 0px;
        }

        .geometry {
            margin: 4px;
            float: left;
        }

            .geometry label {
                cursor: pointer;
                float: left;
                width: 100px;
                margin: 4px;
                background-color: #EFEFEF;
                border-radius: 4px;
                border: 1px solid #D0D0D0;
                overflow: auto;
            }

                .geometry label span {
                    text-align: center;
                    font-size: 12px;
                    padding: 5px 0px;
                    display: block;
                }

                .geometry label input {
                    position: absolute;
                    top: -20px;
                }

            .geometry input:checked + span {
                background-color: #404040;
                color: #F7F7F7;
            }

            .geometry .yellow {
                background-color: #FFCC00;
                color: #333;
            }

            .geometry .blue {
                background-color: #00BFFF;
                color: #333;
            }

            .geometry .pink {
                background-color: #FF99FF;
                color: #333;
            }

            .geometry .green {
                background-color: #03C9A9;
                color: #333;
            }

            .geometry .purple {
                background-color: #B399FF;
                color: #333;
            }

        .ol-custom-overviewmap,
        .ol-custom-overviewmap.ol-uncollapsible {
            bottom: auto;
            right: auto;
            left: 10px;
            top: 300px;
        }

            .ol-custom-overviewmap:not(.ol-collapsed) {
                border: 1px solid black;
            }

            .ol-custom-overviewmap .ol-overviewmap-map {
                border: none;
                width: 300px;
            }

            .ol-custom-overviewmap .ol-overviewmap-box {
                border: 2px solid red;
            }

            .ol-custom-overviewmap:not(.ol-collapsed) button {
                bottom: auto;
                left: auto;
                right: 1px;
                top: 1px;
            }

        .ol-info {
            bottom: auto;
            right: auto;
            left: 50px;
            top: 80px;
            width: 250px;
            background-color: rgba(255, 255, 255, 0.5);
            border-radius: 5px;
            position: absolute;
            z-index: 99;
            font-family: Calibri;
            font-size: 12px;
            font-weight: bold;
        }

        .ol-outstanding-editing {
            bottom: auto;
            right: auto;
            left: 50px;
            top: 240px;
            width: 250px;
            background-color: rgba(255, 255, 255, 0.5);
            border-radius: 5px;
            position: absolute;
            z-index: 99;
            font-family: Calibri;
            font-size: 12px;
            font-weight: bold;
        }

        /*Arrow Toggle*/
        .arrow-down {
            width: 50px;
            height: 50px;
            left: 195px;
            top: -8px;
            background: transparent;
            position: absolute;
        }

            .arrow-down.active {
                background: transparent;
            }

            .arrow-down:before, .arrow-down:after {
                content: "";
                display: block;
                width: 15px;
                height: 1px;
                background: white;
                position: absolute;
                top: 20px;
                transition: transform .5s;
            }

            .arrow-down:before {
                right: 21px;
                border-top-left-radius: 10px;
                border-bottom-left-radius: 10px;
                transform: rotate(45deg);
            }

            .arrow-down:after {
                right: 10px;
                transform: rotate(-45deg);
            }

            .arrow-down.active:before {
                transform: rotate(-45deg);
            }

            .arrow-down.active:after {
                transform: rotate(45deg);
            }

        #myToolbox {
            position: relative;
        }


        #myMapLegend {
            position: relative;
        }

        #myDetail {
            position: relative;
        }

        #dialogloading {
            position: relative;
        }

        .modal-dialog {
            position: fixed;
            width: 400px;
            margin: 0;
            padding: 10px;
        }

        .modal-dialogdetail {
            position: fixed;
            width: 600px;
            margin: 300px;
            padding: 10px;
        }

        /* For Preview Image */
        .modalpreviewimage {
            display: none;
            position: absolute;
            top: 0px;
            left: 0px;
            background-color: black;
            z-index: 99999999;
            opacity: 0.8;
            filter: alpha(opacity=60);
            -moz-opacity: 0.8;
            min-height: 100%;
        }

        #divImage {
            display: none;
            z-index: 999999999;
            position: fixed;
            top: 0;
            left: 0;
            background-color: White;
            height: 550px;
            width: 600px;
            padding: 3px;
            border: solid 1px black;
        }
    </style>

    <style>
        .tooltip {
            position: relative;
            background: rgba(0, 0, 0, 0.5);
            border-radius: 4px;
            color: white;
            padding: 4px 8px;
            opacity: 0.7;
            white-space: nowrap;
        }

        .tooltip-measure {
            opacity: 1;
            font-weight: bold;
        }

        .tooltip-static {
            background-color: #ffcc33;
            color: black;
            border: 1px solid white;
        }

            .tooltip-measure:before,
            .tooltip-static:before {
                border-top: 6px solid rgba(0, 0, 0, 0.5);
                border-right: 6px solid transparent;
                border-left: 6px solid transparent;
                content: "";
                position: absolute;
                bottom: -6px;
                margin-left: -7px;
                left: 50%;
            }

            .tooltip-static:before {
                border-top-color: #ffcc33;
            }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green-light sidebar-mini fixed">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="#" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>PZO</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Piezometer</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-static-top">
                    <%--<ul class="nav navbar-nav">
                        <li>
                            <a href="#">
                                <img src="../Image/toolbar/icon_fullscreen2.png" width='18px' /></a>
                        </li>
                        <li>
                            <a href="#">
                                <img src='../Image/toolbar/icon_overview2.png' width='18px' /></a>
                        </li>
                        <li>
                            <a href="#">
                                <img src='../Image/toolbar/icon_drawing.png' width='18px' /></a>
                        </li>
                        <li>
                            <a href="#">
                                <img src='../Image/toolbar/icon_measure.png' width='18px' /></a>
                        </li>
                        <li>
                            <a href="#">
                                <img src='../Image/toolbar/icon_legend.png' width='18px' /></a>
                        </li>
                        <li>
                            <a href="#">
                                <img src='../Image/toolbar/icon_fullextent.png' width='18px' /></a>
                        </li>
                    </ul>--%>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <span style="margin-left: 50px;"></span>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="#">
                                    <i class="fa fa-map-marker"></i>
                                    <span style="margin-left: 10px;"><span id="titleEstate">Kebun THP 1</span></span>
                                </a>
                        </ul>
                        <ul class="nav navbar-nav">
                            <span style="margin-left: 15px;"></span>
                        </ul>
                        <ul class="nav navbar-nav">
                            <%--<li>
                                <a id="toolsprevdate" href="#">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>--%>
                            <li>
                                <a href="#">
                                    <i class="fa fa-calendar"></i>
                                    <span class="hidden-xs"><span id="lbldate"></span></span>
                                </a>
                            </li>
                            <%--<li>
                                <a id="toolsnextdate" href="#">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>--%>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    <li>
                        <a href="PZO_Dashboard.aspx">
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="treeview active">
                        <a href="#">
                            <i class="fa fa-map"></i>
                            <span>Online Map</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                       <ul class="treeview-menu">
                            <li><a id="map_default" href="PZO_Map.aspx"><i class="fa fa-circle-o"></i>Default Map</a></li>
                            <li><a id="map_analysis" href="PZO_MapAnalysis.aspx"><i class="fa fa-circle-o"></i>Analysis Map</a></li>
                            <li><a id="olmTMAS" href="PZO_NewAnalysisTMAS.aspx"><i class="fa fa-circle-o"></i>Analysis Map for TMAS</a></li>
                            <li><a id="peta_perbandingan" href="PZO_MapByPT.aspx"><i class="fa fa-circle-o"></i>Online Map by PT</a></li> 
                       </ul>
                    </li>
                    <li>
                        <a href="PZO_DownloadData.aspx">
                            <i class="fa fa-files-o"></i>
                            <span>Download Data</span>
                        </a>
                    </li>
                    <li class="treeview active">
                        <a href="#">
                            <i class="fa fa-briefcase"></i><span>Toolbox</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a id="toolFullscreen" href="#"><i class="fa fa-expand"></i>Fullscreen</a></li>
                            <li><a id="toolOverview" href="#"><i class="fa fa-tablet"></i>Overview</a></li>
                            <li><a id="toolDrawing" href="#"><i class="fa fa-pencil"></i>Drawing</a></li>
                            <li><a id="toolMeasure" href="#"><i class="fa fa-cut"></i>Measure</a></li>
                            <li><a id="toolLegend" href="#"><i class="fa fa-list-alt"></i>Legend</a></li>
                            <li><a id="toolFullExtent" href="#"><i class="fa fa-globe"></i>Full Extent</a></li>
                        </ul>
                    </li>
                     <li>
                        <a href="PZO_MapAnalysis.aspx?act=LogOut">
                            <i class="fa fa-sign-out"></i>
                            <span id="txtLogout" runat="server">Keluar</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">



            <%-- <!-- Content Header (Page header) -->
            <section class="content-header">
            </section>--%>

            <!-- Main content -->
            <section class="content">
                  <div class="row">
                                <div class="col-md-8" style="height: 1050px;">
                                    <div>
                                        <label id="lblLastWeek" style="margin-left:15px;"></label>
                                        <table id="tblPetaLastWeek" class="table table-striped table-bordered" style="width: 100%">
                                            <thead>
                                                <tr>
                                                    <td style="width: 20%; text-align: center;">Klasifikasi</td>
                                                    <td style="width: 20%; text-align: center;">Total Block</td>
                                                    <td style="width: 20%; text-align: center;">%</td>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                        <label id="lblKetLastWeek" style="margin-left:15px;"></label>
                                        <button type="button" id="lblUrlLastWeek" style="margin-right:15px;" class="btn btn-success pull-right"></button>
                                    </div>
                                </div>
                                <div class="col-md-6" style="height: 1050px;">
                                    <div>
                                        <label id="lblThisWeek" style="margin-left:15px;"></label>
                                        <table id="tblPetaThisWeek" class="table table-striped table-bordered" style="width: 100%">
                                            <thead>
                                                <tr>
                                                    <td style="width: 20%; text-align: center;">Klasifikasi</td>
                                                    <td style="width: 20%; text-align: center;">Total Block</td>
                                                    <td style="width: 20%; text-align: center;">%</td>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                        <label id="lblKetThisWeek" style="margin-left:15px;"></label>
                                        <button type="button" id="lblUrlThisWeek" style="margin-right:15px;" class="btn btn-success pull-right"></button>
                                    </div>
                                </div>
                            </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!--<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>-->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark control-sidebar-open">
            <!-- Create the tabs -->
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                <li class="active"><a href="#control-sidebar-filter-tab" data-toggle="tab"><i class="fa fa-filter"></i></a></li>
                <li><a href="#control-sidebar-layer-tab" data-toggle="tab"><i class="fa fa-clone"></i></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                
                <div class="tab-pane" id="control-sidebar-layer-tab">
                    <h3 class="control-sidebar-heading">Layer</h3>
                    <label class="control-sidebar-subheading">
                        Vector Layer
                    </label>
                    <label>
                        <input type="checkbox" id="chkLayerBlock" class="flat-red" checked>
                        Block</label>

                    <br />
                    <label>
                        <input type="checkbox" id="chkLayerPiezometer" class="flat-red" checked>
                        Piezometer</label>
                </div>
                <div class="tab-pane active" id="control-sidebar-filter-tab">
                     <br>
                    <div id="dynamicCompanyFilter">
                        <label class="control-sidebar-subheading">
                            Company
                        </label>
                         <select id="selectFilterCompanyForPerbandingan" class="form-control selectpicker" data-live-search="true">
                          </select>
                     
                        
                    </div>
                
                    <br>
                    <div class="form-group">
                        <label>Periode</label>
                        <div class="container-fluid">
                            <button type="button" id="btnPrevWeekPetaPerbandinganxx" class="btn btn-success"><i class="fa fa-angle-left"></i></button>
                            <label id="lblTitleWeekPetaPerbandinganxx">Week Name </label>
                            <button type="button" id="btnNextWeekPetaPerbandinganxx" class="btn btn-success"><i class="fa fa-angle-right"></i></button>
                            <input type="hidden" id="txtWeekIdPetaPerbandinganxx" />
                            <button type="button" id="btnThisWeekPetaPerbandinganxx" class="btn btn-success" style="position: static;">This Week</button>
                        </div>
                        <!--<div class="container-fluid">
                            <button type="button" id="btnPrevWeekGrapTmasTmat" class="btn btn-success"><i class="fa fa-angle-left"></i></button>
                            <label id="lblTitleWeekGrapTmasTmat">Week Name </label>
                            <button type="button" id="btnNextWeekGrapTmasTmat" class="btn btn-success"><i class="fa fa-angle-right"></i></button>
                            <input type="hidden" id="txtWeekIdGrapTmasTmat" />
                            <button type="button" id="btnThisWeekGrapTmasTmat" class="btn btn-success" style="position: static;">This Week</button>
                        </div>-->
                    </div>
                    <br />
                      
                        <div class="form-group">
                            <label></label>
                            <div class="container-fluid">
                                <button type="button" id="btnSubmitPerbedaanPetaGo" class="btn btn-success pull-left">Go</button>
                            </div>
                        </div>
                   
                 
                </div>
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <%--<div class="control-sidebar-bg"></div>--%>
    </div>
    <!-- ./wrapper -->

    <!-- Modal -->
    <div class="modal fade" id="myToolbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" id="btnCloseMyToolbox" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myToolboxTitle"></h4>
                </div>
                <div class="modal-body">
                    <div id="myDrawingToolbox">
                        <select id="selectDrawingMode" class="selectpicker" data-width="100px">
                            <option value="Point">Point</option>
                            <option value="LineString">Polyline</option>
                            <option value="Polygon">Polygon</option>
                        </select>
                        <button type="button" id="btnStartDrawing" class="btn btn-success">Start</button>
                        <button type="button" id="btnStopDrawing" class="btn btn-danger" style="display: none;">Stop</button>
                        <button type="button" id="btnClearDrawing" class="btn btn-success">Clear</button>
                    </div>
                    <div id="myMeasureToolbox">
                        <select id="selectMeasureMode" class="selectpicker" data-width="150px">
                            <option value="LineString">Length (LineString)</option>
                            <option value="Polygon">Area (Polygon)</option>
                        </select>
                        <button type="button" id="btnStartMeasure" class="btn btn-success">Start</button>
                        <button type="button" id="btnStopMeasure" class="btn btn-danger" style="display: none;">Stop</button>
                        <button type="button" id="btnClearMeasure" class="btn btn-success">Clear</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myMapLegend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" id="btnCloseMyMapLegend" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myMapLegendTitle">Map Legend</h4>
                </div>
                <div class="modal-body">
                    <img src="http://172.30.1.32:8080/geoserver/PZO/wms?REQUEST=GetLegendGraphic&amp;VERSION=1.0.0&amp;FORMAT=image/png&amp;WIDTH=20&amp;HEIGHT=20&amp;LAYER=PZO:pzo_block_week" alt="img" id="legendlayer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialogdetail" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" id="btnCloseMyDetail" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myDetailTitle">Detail</h4>
                </div>
                <div class="modal-body">
                    <div id="divloadingdetail" style="display: none; text-align: center;">
                        <img alt="" src="../Image/loading-ripple.gif" width="50" />
                    </div>
                    <div id="divcontentdetail">
                        <div class="row">
                            <div id="divmasterdetail"></div>
                        </div>
                        <div class="row">
                            <ul class="nav nav-tabs">
                                <li role="presentation"><a data-toggle="tab" href="#table">Table</a></li>
                                <li role="presentation" class="active"><a data-toggle="tab" href="#chart">Chart</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="table" class="tab-pane fade">
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="divtablehistory"></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="chart" class="tab-pane fade in active">
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="divcharthistory" style="width: 100%; height: 400px"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="btn-group" data-toggle="buttons">
                                                <label id="allData" class="btn btn-success active">
                                                    <input type="radio" name="allData"><span>All</span></label>
                                                <label id="thn" class="btn btn-success">
                                                    <input type="radio" name="thn"><span>1 Tahun</span></label>
                                                <label id="6bln" class="btn btn-success">
                                                    <input type="radio" name="6bln"><span>6 Bulan</span></label>
                                                <label id="3bln" class="btn btn-success">
                                                    <input type="radio" name="3bln"><span>3 Bulan</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divinfo"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="divBackground" class="modalpreviewimage">
    </div>
    <div id="divImage">
        <table style="height: 100%; width: 100%">
            <tr>
                <td valign="middle" align="center">
                    <img id="imgLoader" alt="" src="../Image/loading.gif" />
                    <img id="imgFull" alt="" src="" style="display: none; height: 500px; width: 590px" />
                </td>
            </tr>
            <tr>
                <td align="center" valign="bottom">
                    <input id="btnClose" type="button" value="close" onclick="HidePreview()" />
                </td>
            </tr>
        </table>
    </div>
    <div id="dialogloading" class="modal fade" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialogdetail col-md-3">
            <div class="box box-default">
                <h3 class="box-title">Loading</h3>
                <div class="overlay">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
    
    <form id="Form1" runat="server">
        <asp:HiddenField ID="userid" runat="server" />
        <asp:HiddenField ID="userrole" runat="server" />
        <asp:HiddenField ID="username" runat="server" />
        <asp:HiddenField ID="utmprojection" runat="server" />
        <asp:HiddenField ID="rsid" runat="server" />
        <asp:HiddenField ID="extent" runat="server" />
        <asp:HiddenField ID="numzoom" runat="server" />
        <asp:HiddenField ID="legend" runat="server" />
        <asp:HiddenField ID="idwm" runat="server" />
        <asp:HiddenField ID="company" runat="server" />
        <asp:HiddenField ID="estate" runat="server" />
        <asp:HiddenField ID="mapped_estate" runat="server" />
        <asp:HiddenField ID="date" runat="server" />
        <asp:HiddenField ID="idweek" runat="server" />
        <asp:HiddenField ID="year" runat="server" />
        <asp:HiddenField ID="month" runat="server" />
        <asp:HiddenField ID="monthname" runat="server" />
        <asp:HiddenField ID="week" runat="server" />
        <asp:HiddenField ID="monthnamealias" runat="server" />

        <asp:HiddenField ID="idweekX" runat="server" />
        <asp:HiddenField ID="yearX" runat="server" />
        <asp:HiddenField ID="monthX" runat="server" />
        <asp:HiddenField ID="monthnameX" runat="server" />
        <asp:HiddenField ID="weekX" runat="server" />
        <asp:HiddenField ID="monthnamealiasX" runat="server" />
        <asp:HiddenField ID="masukQuery" runat="server" />

        <asp:HiddenField ID="filter" runat="server" />
        <asp:HiddenField ID="kondisi" runat="server" />
        <asp:HiddenField ID="minFilter" runat="server" />
        <asp:HiddenField ID="berkali" runat="server" />
        <asp:HiddenField ID="operatorfilter" runat="server" />
        <asp:HiddenField ID="listAllWMArea" runat="server" />

        <asp:HiddenField ID="currentextent" runat="server" />
        <asp:HiddenField ID="typemodule" runat="server" />
        <asp:HiddenField ID="estshortname" runat="server" />
        <asp:HiddenField ID="estname" runat="server" />
        <asp:HiddenField ID="estatezone" runat="server" />
        <asp:HiddenField ID="listyear" runat="server" />
        <asp:HiddenField ID="listmonth" runat="server" />
        <asp:HiddenField ID="listweek" runat="server" />
        <asp:HiddenField ID="listWaterDeep" runat="server" />
    </form>

    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../Source/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../Source/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Bootstrap Select -->
    <script src="../Source/adminlte/bootstrap-select.js"></script>
    <!-- DataTables -->
    <script src="../Source/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/dataTables.buttons.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.flash.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/jszip.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/pdfmake.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/vfs_fonts.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.html5.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.print.min.js"></script>    
    <%-- <script src="../Source/DataTable/js/dataTables.bootstrap4.min.js"></script>
    <script src="../Source/DataTable/js/dataTables.fixedHeader.js"></script>--%>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="../Source/adminlte/plugins/iCheck/icheck.min.js"></script>
    <!-- daterangepicker -->
    <script src="../Source/adminlte/bower_components/moment/min/moment.min.js"></script>
    <script src="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../Source/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../Source/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../Source/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../Source/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../Source/adminlte/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../Source/adminlte/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../Source/adminlte/dist/js/demo.js"></script>
    <!-- Openlayers 4 -->
    <script src="../Source/adminlte/bower_components/openlayers4.6.5/ol.js"></script>
    <!-- Piezometer -->
    <script src="../Script/ol4/piezometerAnalisis.js"></script>
    <%--Tambahan afid--%>
    <script src="../Source/jszip/jszip.min.js"></script>
    <script src="../Source/shpwrite/shpwrite.js"></script>
    <script src="../Source/FileSaver/FileSaverRestu.js"></script>
    <script src="../Source/canvasjs/canvasjs.min.js"></script>
    <script src="../Script/tambahanjs/WebServices.js"></script>
    <script src="../Script/tambahanjs/FunctionTambahan.js"></script>
    <!-- Map Event Handler -->
    <script src="../Script/ol4/function.js"></script>
    <!-- Reqwest -->
    <script src="../Source/Reqwest/reqwest.js" type="text/javascript"></script>


    <!-- This Page Main JS -->

    <script src="../Source/js/jquery-confirm.js"></script>
    <script src="../Source/canvasjs-commercial-3.6.6/canvasjs.min.js"></script>
    <script src="../Source/underscore/underscore.js"></script>
    
    <script src="<%= downloaddata_js %>"></script>
</body>
</html>