﻿using System;
using System.Net;
using System.Web;

namespace PZO_PiezometerOnlineMap.Content
{
    /// <summary>
    /// Summary description for GambarProxy
    /// </summary>
    public class GambarProxy : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string imageUrl = context.Request.QueryString["url"];
            if (string.IsNullOrEmpty(imageUrl))
            {
                context.Response.StatusCode = 400;
                context.Response.Write("URL is required");
                return;
            }

            // Pastikan URL menggunakan HTTP
            if (imageUrl.StartsWith("https://"))
            {
                imageUrl = imageUrl.Replace("https://", "http://");
            }

            using (var webClient = new WebClient())
            {
                try
                {
                    byte[] imageBytes = webClient.DownloadData(imageUrl);
                    context.Response.ContentType = "image/png"; // Sesuaikan content type sesuai dengan jenis gambar
                    context.Response.BinaryWrite(imageBytes);
                }
                catch (Exception ex)
                {
                    context.Response.StatusCode = 500;
                    context.Response.Write("Error loading image: " + ex.Message);
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}