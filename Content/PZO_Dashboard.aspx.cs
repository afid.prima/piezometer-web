﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PZO_PiezometerOnlineMap.Content
{
    public partial class PZO_Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uID"] == null) Response.Redirect("~/Default.aspx");

            if (Session["uID"].ToString() != null && Session["uID"].ToString() != "")
            {           
                txtLogout.InnerText = "Keluar"+" "+"("+Session["uname"].ToString()+")";
                userid.Value = Session["uID"].ToString();
                //liheader.InnerText = Session["uname"].ToString();
            }
            string menu = Request.Params["_PZO_Map"];
            if (menu == "LogOut")
            {
                LogOut();
            }

        }
        protected void LogOut()
        {
            if (!string.IsNullOrEmpty(Session["PZO5"] as string))
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            //Session.Abandon();
            
        }
    }
}