﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WLR_DataZona.ascx.cs" Inherits="PZO_PiezometerOnlineMap.Content.WLR_DataZona" %>
<%--<link href="https://unpkg.com/ionicons@4.4.8/dist/css/ionicons.min.css" rel="stylesheet">--%>
<style>
    #tgldragdropRambu {
        list-style-type: none;
        cursor: move;
    }

        #tgldragdropRambu li {
            display: inline-block;
            padding: 0 10px;
            font-size: 16px;
            line-height: 30px;
            border-radius: 15px;
            margin: 4px;
        }

    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }

    .horizontal {
        display: inline;
        list-style-type: none;
        padding-right: 20px;
    }

    .popover {
        max-width: 2000px;
    }
</style>

<script src="../Source/canvasjs-2.1/canvasjs.min.js"></script>
<script src="../Source/jspdf/jspdf1.3.2.js"></script>
<script src="../Source/jspdf/jspdf1.3.2.debug.js"></script>
<script src="../Source/jspdf/html2canvas.js"></script>
<script src="../Source/jspdf/canvas2image.js"></script>
<script src="../Source/jspdf/customfont.min.js"></script>
<%--<script src="../Source/jspdf/jspdf.js"></script>--%>
<script src="<%= scriptUrlDataZona %>"></script>
<form id="Form1" runat="server">
    <asp:HiddenField ID="uAcc" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="uID" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="ListZona" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="ListWMArea" runat="server" ClientIDMode="Static" />
</form>
<%--<progress></progress>--%>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Wm Area</label>
                            <select id="selectFilterWMArea" class="form-control selectpicker" data-live-search="true">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Graph</label>
                            <select id="selectFilterZona" class="form-control selectpicker" data-live-search="true">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Rambu Air</label>
                            <select id="selectFilterRambuAir" class="form-control selectpicker" data-live-search="true" style="width: 100%" multiple></select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Date Range</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control pull-right" id="tglSelected" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box collapsed-box">
            <div class="box-header with-border">
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse"
                        data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
                <i class="fa fa-cogs"></i>

                <h3 class="box-title">Setting Graph
                </h3>
            </div>
            <div class="box-body with-border">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Rambu Refrensi:</label>
                            <div class="input-group">
                                <div id='divRambuRefrensi'></div>
                            </div>
                        </div>
                        <div style="margin-top: 150px;">
                            <textarea id="keteranganGrafik" placeholder="input keterangan grafik" style="width: 531px; height: 50px;" oninput="getDescription();"></textarea>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Curah Hujan:</label>
                            <select id="selectFilterCurahHujan" class="select2" style="width: 100%" multiple="multiple"></select>
                            <br />
                            <br />
                            <label>TMAS Target:</label>
                            <br />
                            <input type="text" class="form-control pull-right" id="tmasTarget">                                                                                 
                        </div>
                    </div>                    
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">                                                            
                                    <div class="col-md-12">
                                        <div class="form-group">
                                              <label>Zona</label>
                                               <select id="DropdownFilterZona" class="form-control selectpicker" style=""></select>
                                        </div>
                                    </div>                                
                                    <div class="col-md-12" style="margin-right:30px;">
                                        <div class="form-group">
                                            <label>Nama Graph</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-area-chart"></i></div>
                                                <input type="text" class="form-control pull-right" id="namaGraph">
                                            </div>
                                        </div>
                                    </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="file" id="btnUploadImg" accept="image/*" />
                                            <div id="idproggres"><progress></progress></div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Note:</label>
                                    <div class="input-group">
                                        <a style="height: 30px; padding-top: 3px" class="btn btn-block btn-social btn-success" id="addNote"><i class="fa  fa-plus"></i>Add Note</a>
                                        <a style="height: 30px; padding-top: 3px" class="btn btn-block btn-social btn-primary" id="addGraphColor"><i class="fa  fa-plus"></i>Add Graphic Color</a>
                                        <!--fariz-->
                                    </div>
                                </div>
                            </div>
                        </div>                     
                        <ul class="nav nav-tabs pull-right">
                            <li class="active"><a href="#tab_1-1" id='l1' data-toggle="tab">Table Note</a></li>
                            <li><a href="#tab_2-2" id='l2' data-toggle="tab">Table Color</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab_1-1">
                                <table id="tableNote" class="table table-striped table-bordered" style="width: 100%">
                                    <thead style="font-size: 12px">
                                        <th align="center">idnote</th>
                                        <th align="center" style="width: 40%">Tanggal</th>
                                        <th align="center" style="width: 55%">Note</th>
                                        <th align="center" style="width: 5%">Status</th>
                                        <th align="center">idstatus</th>
                                    </thead>
                                </table>

                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2-2">
                                <table id="tempColor" class="table table-striped table-bordered" style="width: 100%">
                                    <thead style="font-size: 12px">
                                        <th align="center" style="width: 40%">Nama Klasifikasi</th>
                                        <th align="center" style="width: 55%">Nilai Minimum</th>
                                        <th align="center" style="width: 55%">Nilai Maksimum</th>
                                        <th align="center" style="width: 5%">Kode Warna</th>
                                    <%--    <th align="center" style="width: 5%">Warna</th>--%>
                                        <th align="center" style="width: 5%">Status</th>
                                        <th align="center">idstatus</th>
                                    </thead>
                                </table>

                            </div>
                            <!-- /.tab-pane -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row" id="divActionBtn">
                    <div class="col-md-12">
                        <div class="form-group" style="margin-top: 10px;">
                            <button type="submit" class="btn btn-primary" id="btnHapusGraph">Hapus</button>
                            <button type="submit" class="btn btn-primary pull-right" id="btnSaveGraph">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box collapsed-box">
            <div class="box-header with-border">
                <!-- tools box -->
                <div class="pull-right box-tools">
                    <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse"
                        data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
                <i class="fa fa-cogs"></i>

                <h3 class="box-title">Group Report
                </h3>
            </div>
            <div class="box-body with-border">
                <div class="row">                  
                    <div class="col-md-3">
                        <div class="form-group">
                        <label>Week</label>
                        <select id="SelectFilterWeek" class="form-control selectpicker" data-live-search="true">
                        </select>
                        </div>
                    </div>     
                    <div class="col-md-3">
                        <div class="form-group">
                        <label>Graph</label>
                        <select id="selectFilterGraph" class="form-control selectpicker" data-live-search="true">
                        </select>
                        </div>
                    </div>     
                     <div class="col-md-6">
                        <div class="form-group" style="margin-top: 25px;"> 
                              <button type="button" id="saveGraphParamater" class="btn btn-primary" onclick="">Save graph parameter</button>
                        </div>                          
                    </div>                    
                </div>
                <button type="button" id="linkReportPemantauanTMAS" class="btn btn-primary" onclick="DownloadReportPemantauanTMAS()">Pemantauan TMAS</button>
                <button type='button' id='linkGroup' class='btn btn-primary' onclick='DownloadReportAnalisaPemantauanTMAS()'>Analisa Perbandingan TMAT & TMAS</button>
            </div>          
        </div>
    </div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body with-border">
                <div class="box-header with-border">
                    <div>
                        <h3 class="box-title">Chart</h3>                        
                        <div class="pull-right box-tools" id="loremipsum">  
                            <%--<button type="button" class="btn btn-info btn-sm" id="downloadImages" style="margin-left: 50px;"><i class="fa fa-info"></i></button>  --%>
                            <button type="button" id="btnExportPdf" data-toggle="popover" data-placement="left" data-container="body" class="btn btn-primary btn-sm pull-right" title="Pilih Tipe Download" <%--style="margin-right: 30px;"--%> ;>                                
                                export to pdf
                            </button>                                
                        </div>
                    </div>
                    <div class="pull-right box-tools" id="divDownload">
                    </div>
                </div>                
                <div id='divChart' style='height: 400px; width: 100%; margin: 0px auto;'>
                </div>
                <div id="export-pdf" >
                    <div id="divDeskripsiGrafik" style="margin-left: 75px; font-size: 12px; width: 920px; font-style: normal;"></div>
                    <div id="divKetChart" class="canvasjs-chart-canvas" style='max-width: 920px; margin: 0px auto;'>
                       <%-- <img src="../Image/circle.png" id="daily" /> Daily <br />
                        <img src="../Image/square-shape-shadow.png" id="weekly" /> Weekly <br />
                        <img src="../Image/caret-arrow-up.png" /> Random--%>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>
<div id="formMeasure"></div>
<div id="formMeasureHistory"></div>
<div id="formChangeColor"></div>
<div id="formNote"></div>
<div id="graphColorSetting"></div>


