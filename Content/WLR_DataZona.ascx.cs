﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PZO_PiezometerOnlineMap.Class;
using PZO_PiezometerOnlineMap.Mapper;

namespace PZO_PiezometerOnlineMap.Content
{
    public partial class WLR_DataZona : System.Web.UI.UserControl
    {
        #region Variable Declaration
        private WLR_Mapper mapper = new WLR_Mapper();
        private LastUpdateFile lastUpdateFile = new LastUpdateFile();
        public string scriptUrlDataZona { get; set; }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet dtWMArea = mapper.ListWMArea();
            DataSet dtUser = new DataSet();
            if (Session["uID"].ToString() != "Guest")
            {
                dtUser = mapper.UserAcces(Session["uID"].ToString());
                uAcc.Value = ObjectToJSON(dtUser);
            }

            ListWMArea.Value = ObjectToJSON(dtWMArea);
            uID.Value = Session["uID"].ToString();

            string filePath;
            string fileVersion;
            filePath = Server.MapPath("~/Script/dataZona.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            scriptUrlDataZona = $"../Script/dataZona.js?v={fileVersion}";

        }

        private string ObjectToJSON(DataSet data)
        {
            string jsonDataStr = JsonConvert.SerializeObject(data, Formatting.None);
            JObject jsonData = JObject.Parse(jsonDataStr).ToObject<JObject>();
            return jsonData["Table"].ToString();
        }
    }
}