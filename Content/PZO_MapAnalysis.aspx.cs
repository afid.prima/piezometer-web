﻿using System;
using System.Data;
using System.Web.UI;
using PASS.Mapper;
using PZO_PiezometerOnlineMap.Class;
using PZO_PiezometerOnlineMap.Mapper;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace PZO_PiezometerOnlineMap.Content
{
    public partial class PZO_MapAnalysis : System.Web.UI.Page
    {
        #region Variable Declaration

        private PZO_ProjectMapper mapper = new PZO_ProjectMapper();
        private PZO_ProjectMapperTambahan mapper2 = new PZO_ProjectMapperTambahan();
        private OLM_OnlineMapper olmMapper = new OLM_OnlineMapper();
        private Cryptography crypto = new Cryptography();
        private LastUpdateFile lastUpdateFile = new LastUpdateFile();

        public string scriptPiezometerAnalisis { get; set; }
        public string scriptFunctionTambahan { get; set; }
        public string scriptFunction { get; set; }
        public string scriptMap_LayerBase { get; set; }
        public string scriptMap_SideBar { get; set; }
        public string scriptMap_Waterway { get; set; }
        public string scriptMap_PiezometerAnalysis { get; set; }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uID"] == null) Response.Redirect("~/Default.aspx");

            if (Session["uID"].ToString() != null && Session["uID"].ToString() != "")
            {
                txtLogout.InnerText = "Keluar" + " " + "(" + Session["uname"].ToString() + ")";
                //liheader.InnerText = Session["uname"].ToString();
            }
            string menu = Request.Params["act"];
            if (menu == "LogOut")
            {
                LogOut();
            }
            //if (Session["group"] != null)
            //{
            //    UserControl ucJob = null;
            //    //string menu = Request.Params["HMS"];

            //    switch (menu)
            //    {
            //        case "LogOut":
            //            LogOut();
            //            break;
            //        case "WLR_DataZona":
            //            ucJob = (UserControl)LoadControl("WLR_DataZona.ascx");
            //            phjob.Controls.Add(ucJob);
            //            break;
            //        //default:
            //        //    ucJob = (UserControl)LoadControl("HMS_EnterSystem.ascx");
            //        //    phjob.Controls.Add(ucJob);
            //        //    break;
            //    }
            //}
            //else
            //{
            //    Response.Redirect("~/default.aspx");
            //}

            try
            {
                DataTable dtListWeek = mapper.GetListWeek();
                DataTable dtExtendCoordinate = mapper2.GetAllExtendCoordinate();
                listyear.Value = ObjectToJSON(dtListWeek.DefaultView.ToTable(true, "Year"));
                listmonth.Value = ObjectToJSON(dtListWeek.DefaultView.ToTable(true, "Year", "Month", "MonthName"));
                listweek.Value = ObjectToJSON(dtListWeek);
                DataTable dtWeek = new DataTable();

                dtWeek = mapper.GetWeekFromCurrentDate();

                userid.Value = Session["uID"].ToString();
                year.Value = dtWeek.Rows[0]["Year"].ToString();
                month.Value = dtWeek.Rows[0]["Month"].ToString();
                monthname.Value = dtWeek.Rows[0]["MonthName"].ToString();
                week.Value = dtWeek.Rows[0]["Week"].ToString();
                monthnamealias.Value = dtWeek.Rows[0]["MonthName"].ToString();

                extentCoordinate.Value = ObjectToJSON(dtExtendCoordinate);
                idweek.Value = dtWeek.Rows[0]["ID"].ToString();
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + "No Data" + " (" + ex.Message + ")" + "')", true);
            }

            string filePath;
            string fileVersion;
            filePath = Server.MapPath("~/Script/ol4/piezometerAnalisis.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            scriptPiezometerAnalisis = $"../Script/ol4/piezometerAnalisis.js?v={fileVersion}";
            
            filePath = Server.MapPath("~/Script/tambahanjs/FunctionTambahan.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            scriptFunctionTambahan = $"../Script/tambahanjs/FunctionTambahan.js?v={fileVersion}";

            filePath = Server.MapPath("~/Script/ol4/function.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            scriptFunction = $"../Script/ol4/function.js?v={fileVersion}";

            filePath = Server.MapPath("~/Script/analysisMap/Map_LayerBase.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            scriptMap_LayerBase = $"../Script/analysisMap/Map_LayerBase.js?v={fileVersion}";

            filePath = Server.MapPath("~/Script/analysisMap/Map_SideBar.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            scriptMap_SideBar = $"../Script/analysisMap/Map_SideBar.js?v={fileVersion}";

            filePath = Server.MapPath("~/Script/analysisMap/Map_Waterway.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            scriptMap_Waterway = $"../Script/analysisMap/Map_Waterway.js?v={fileVersion}";

            filePath = Server.MapPath("~/Script/analysisMap/Map_PiezometerAnalysis.js");
            fileVersion = lastUpdateFile.cekLastUpdateJs(filePath);
            scriptMap_PiezometerAnalysis = $"../Script/analysisMap/Map_PiezometerAnalysis.js?v={fileVersion}";
        }

        private string ObjectToJSON(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
        protected void LogOut()
        {
            if (!string.IsNullOrEmpty(Session["PZO5"] as string))
            {
                Session.Abandon();
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            //Session.Abandon();

        }
    }
}