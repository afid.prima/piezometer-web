﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PZO_MainMenu.aspx.cs" Inherits="PZO_PiezometerOnlineMap.Content.PZO_MainMenu" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Online Map | Piezometer</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Bootstrap Select -->
  <link rel="stylesheet" href="../Source/adminlte/bootstrap-select.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../Source/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../Source/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../Source/adminlte/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../Source/adminlte/plugins/iCheck/square/blue.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="PZO_MainMenu.aspx"><b>OnlineMap</b>Piezometer</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><img alt="GAMA Plantation" src="../Image/Logo/Logo_GAMA.jpg" style="height: 70px;" /></p>

    <form action="#" method="post">
      <div class="form-group has-feedback">
        <select id="selectFilterCompany" class="form-control selectpicker" data-live-search="true"></select>
      </div>
      <div class="form-group has-feedback">
          <select id="selectFilterEstate" class="form-control selectpicker" data-live-search="true"></select>
      </div>
      <div class="row">
        <div class="col-xs-8">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button id="btngo" type="button" class="btn btn-primary btn-block btn-flat">Go</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../Source/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Bootstrap Select -->
<script src="../Source/adminlte/bootstrap-select.js"></script>
<script>
    $(function () {
        $('.selectpicker').selectpicker({});

        ListCompany();

        $("#selectFilterCompany").change(function () {
            ListEstate($("#selectFilterCompany").val());
            //$('.selectpicker').selectpicker('refresh');
        })

        $("#btngo").click(function () {
            window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + $("#selectFilterCompany").val() + "&Estate=" + $("#selectFilterEstate").val();
        })
    });

    function ListCompany() {
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/ListCompany',
            data: '{}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var dataValue = response.d[0].split(";");
                var dataText = response.d[1].split(";");
                var dataLength = response.d[0].split(";").length;

                $('#selectFilterCompany').find('option').remove();
                $("#selectFilterCompany").append($('<option>', {
                    value: "",
                    text: "All Company"
                }));
                for (var i = 0; i < dataLength; i++) {
                    $("#selectFilterCompany").append($('<option>', {
                        value: dataValue[i],
                        text: dataText[i]
                    }));
                }

                $("#selectFilterCompany").val("").change();

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("function ListCompany : " + xhr.statusText);
            }
        });
    }

    function ListEstate(companycode) {
        console.log(companycode);
        $.ajax({
            type: 'POST',
            url: '../Service/MapService.asmx/ListEstate',
            data: '{CompanyCode: "' + companycode + '"}',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                var dataValue = response.d[0].split(";");
                var dataText = response.d[1].split(";");
                var dataLength = response.d[0].split(";").length;

                $('#selectFilterEstate').find('option').remove();
                for (var i = 0; i < dataLength; i++) {
                    $("#selectFilterEstate").append($('<option>', {
                        value: dataValue[i],
                        text: dataText[i]
                    }));
                }

                $("#selectFilterEstate").val(dataValue[0]);
                $('.selectpicker').selectpicker('refresh');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("function ListEstate : " + xhr.statusText);
            }
        });
    }
</script>
</body>
</html>


<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Main Menu</title>
    <link rel="stylesheet" href="../Source/Jquery/jquery-ui.min.css?v=t1" type="text/css" />
    <link rel="stylesheet" href="../Style/Global.css" type="text/css" />
    <script type="text/javascript" src="../Source/Jquery/jquery-1.11.2.min.js?v=t1"></script>
    <script type="text/javascript" src="../Source/Jquery/jquery-ui.min.js?v=t1"></script>
    <link rel="Stylesheet" href="../Source/AutoCompleteDropdown/chosen.css" type="text/css" />;
    <script type="text/javascript" src="../Source/AutoCompleteDropdown/chosen.jquery.js"></script>
    <style type="text/css">
        .main
        {
            margin: 0pt auto; 	
	        max-width: 500px;
	        height: 300px;		
	        border: 3px solid;
	        border-color:#389C56;
	        border-radius: 5px;
	        background-color:#d0f2d0;
        }
        
        .content
        {
            width:100%;
            margin:0 auto;        
            height: 78%;    
            line-height: 0.7em;  
        }
    </style>
    <script type="text/javascript">
        $(window).load(function () {
            ListCompany();

            var today = new Date();
            var dd = today.getDate();
            var ddto = today.getDate();
            var mm = (today.getMonth() + 1);
            var yyyy = today.getFullYear();
            if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } today = mm + '/' + dd + '/' + yyyy;

            $("#inputFilterDate").val(today);
            $("#inputFilterDate").datepicker({
                defaultDate: 0,
                changeMonth: true,
                numberOfMonths: 1
            });

            $("#selectFilterCompany").change(function () {
                ListEstate($("#selectFilterCompany").val());
            })

            $("#btngo").click(function () {
                window.location = "PZO_Map.aspx?typeModule=OLM&Company=" + $("#selectFilterCompany").val() + "&Estate=" + $("#selectFilterEstate").val();
            })
        })


        function ListCompany() {
            $.ajax({
                type: 'POST',
                url: '../Service/MapService.asmx/ListCompany',
                data: '{}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var dataValue = response.d[0].split(";");
                    var dataText = response.d[1].split(";");
                    var dataLength = response.d[0].split(";").length;

                    $('#selectFilterCompany').find('option').remove();
                    for (var i = 0; i < dataLength; i++) {
                        $("#selectFilterCompany").append($('<option>', {
                            value: dataValue[i],
                            text: dataText[i]
                        }));
                    }

                    $("#selectFilterCompany").val(dataValue[0]).change();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("function ListCompany : " + xhr.statusText);
                }
            });
        }

        function ListEstate(companycode) {
            $.ajax({
                type: 'POST',
                url: '../Service/MapService.asmx/ListEstate',
                data: '{CompanyCode: "' + companycode + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var dataValue = response.d[0].split(";");
                    var dataText = response.d[1].split(";");
                    var dataLength = response.d[0].split(";").length;

                    $('#selectFilterEstate').find('option').remove();
                    for (var i = 0; i < dataLength; i++) {
                        $("#selectFilterEstate").append($('<option>', {
                            value: dataValue[i],
                            text: dataText[i]
                        }));
                    }

                    $("#selectFilterEstate").val(dataValue[0]);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("function ListEstate : " + xhr.statusText);
                }
            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="main">
            <div style="margin:20px 0 0 20px;">
                 <img alt="Precise Agri" src="../Image/Logo/Logo_GAMA.jpg" style="height: 70px;" />
            </div>
            <div style="margin: 50px 50px 0 50px;">
                Company<br />
                <select id="selectFilterCompany" style="left: 0px; position: relative; font-size: 12px; width: 250px;"
                            class="chzn-select">
                        </select><br />
                Estate<br />
                <select id="selectFilterEstate" style="left: 0px; position: relative; font-size: 12px; width: 200px;"
                            class="chzn-select">
                        </select><%--<br /><br />--%>
                <%--Date<br />
                <input type="text" id="inputFilterDate" name="inputFilterDate" style="width:100px;" class="classfont"/>--%>
                <%--<input type="button" class="classbutton classfont" value="Go" id="btngo"/>
            </div>
        </div>
    </div>
    </form>
</body>
</html>--%>
