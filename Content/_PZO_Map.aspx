﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="_PZO_Map.aspx.cs" Inherits="PZO_PiezometerOnlineMap.Content.PZO_Map" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="../Source/Openlayers2/theme/default/style.css?v=t2"
        type="text/css" />
    <link rel="stylesheet" href="../Source/Jquery/jquery-ui.min.css?v=t1" type="text/css" />
    <link href="../Source/adminlte/bower_components/select2/dist/css/select2.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Style/Map.css?v=t1" type="text/css" />
    <link rel="stylesheet" href="../Style/Global.css?v=t1" type="text/css" />
    <link rel="Stylesheet" href="../Style/AMSStyle.css" type="text/css" />
    <link rel="Stylesheet" href="../Source/DataTable/css/jquery.dataTables.css" type="text/css" />

    <title>Online Map</title>
</head>
<body>
    <div id="divtop">
        <div id="toolsfullscreen" class="tools" style="left: 5px;" title="Full Screen">
            <img id="imgfullsceen" src="../Image/toolbar/Fullscreen-32.png" height="24" width="24"
                alt="img" />
            &nbsp;&nbsp;
        </div>
        <div id="toolsoverview" class="tools" style="left: 6px;" title="Overview Map">
            <img id="imgoverview" src="../Image/toolbar/Overview.png" height="24" width="24"
                alt="img" />
        </div>
        <div id="toolsdrawing" class="tools" style="left: 7px;" title="Drawing">
            <img id="imgdrawing" src="../Image/toolbar/draw.png" height="24" width="24" alt="img" />
        </div>
        <div id="toolsmeasurement" class="tools" style="left: 8px;" title="Measurement">
            <img id="imgmeasurement" src="../Image/toolbar/measurement.png" height="24" width="24"
                alt="img" />
        </div>
        <div id="toolslegend" class="tools" style="left: 9px;" title="Legend">
            <img id="imglegend" src="../Image/toolbar/legendToolbar.png" height="24" width="24"
                alt="img" />
        </div>
        <div id="toolsextent" class="tools" style="left: 10px;" title="Full Extent">
            <img id="imgextent" src="../Image/toolbar/fullextent.png" height="24" width="24"
                alt="img" />
        </div>
        <div id="toolsdrag" class="tools" style="left: 11px;" title="Drag Mode">
            <img id="imgdrag" src="../Image/toolbar/drag.png" height="24" width="24" alt="img" />
        </div>
        <div id="toolszoomprev" class="tools" style="left: 12px;" title="Zoom Previous">
            <span id="spanzoomprev" title="Zoom Previous"></span>
        </div>
        <div id="toolszoomnext" class="tools" style="left: 13px;" title="Zoom Next">
            <span id="spanzoomnext" title="Zoom Next"></span>
        </div>
        <%--<div id="toolsreport" class="tools" style="left: 14px; display: block;" title="Report">
            <img id="imgreport" src="../Image/toolbar/report.jpg" height="24" width="24" alt="img" />
        </div>--%>
        <div id="toolsprojectdetail" class="tools" style="left: 15px; visibility: hidden"
            title="Project Detail">
            <img id="imgprojectdetail" src="../Image/toolbar/ProjectInfo.png" height="24" width="24"
                alt="img" />
        </div>
        <div id="operatingunit" class="divtools  classfont" style="left: 330px; font-size: 14px;">
            <div id="lbloperatingunit" style="top: 3px; position: relative;">
            </div>
        </div>
        <div id="toolsprevdate" class="divtools" style="left: 550px; cursor: pointer;">
            <img id="prevdate" src="../Image/toolbar/previous.png" height="24" width="24" alt="img" />
        </div>
        <div id="titledate" class="divtools  classfont" style="width: 120px; left: 595px; font-size: 14px;">
            <div id="lbldate" style="top: 3px; position: relative;"></div>
        </div>
        <div id="toolsnextdate" class="divtools" style="left: 735px; cursor: pointer;">
            <img id="nextdate" src="../Image/toolbar/next.png" height="24" width="24" alt="img" />
        </div>
    </div>
    <div id="map">
        <img id="sidebarlayer" src="../Image/layer/layer.png" alt="img" height="100" class="sidebar" />
        <img id="sidebarfilter" src="../Image/layer/filter.png" alt="img" height="100" class="sidebar"
            style="top: 141px;" />
        <div id="layercontent" title="Layer" class="dialog classfont" style="visibility: hidden; opacity: 1; font-size: 13px;">
            <table id="tablelayer">
                <tr>
                    <td>Active Layer
                    </td>
                </tr>
                <tr>
                    <td>
                        <select id="selectlistLayer" style="left: 0px; position: relative; font-size: 12px; width: 150px;"
                            class="classfont">
                        </select>
                    </td>
                </tr>
            </table>
            <div id="divlistlayer">
            </div>
        </div>
        <div id="filtercontent" title="Filter" class="dialog classfont" style="visibility: hidden; opacity: 1; font-size: 13px;">
            <div style="display: table">
                <div style="display: table-row">
                    <div style="display: table-cell">
                        Filter :
                    </div>
                </div>
                <div style="display: table-row; height: 5px;">
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell">
                        Company
                    </div>
                    <div style="display: table-cell; width: 50px;">
                    </div>
                    <div style="display: table-cell">
                        <select id="selectFilterCompany" style="left: 0px; position: relative; font-size: 10px; width: 250px;"
                            class="classfont">
                        </select>
                    </div>
                </div>
                <div style="display: table-row; height: 5px;">
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell">
                        Estate
                    </div>
                    <div style="display: table-cell; width: 50px;">
                    </div>
                    <div style="display: table-cell">
                        <select id="selectFilterEstate" style="left: 0px; position: relative; font-size: 12px; width: 250px;"
                            class="classfont">
                        </select>
                    </div>
                </div>
                <div style="display: table-row; height: 5px;">
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell">
                        Week
                    </div>
                    <div style="display: table-cell; width: 50px;">
                    </div>
                    <div style="display: table-cell">
                        <select id="selectFilterYear" style="left: 0px; position: relative; font-size: 12px; width: 68px;"
                            class="classfont">
                        </select>
                        <select id="selectFilterMonth" style="left: 0px; position: relative; font-size: 12px; width: 100px;"
                            class="classfont">
                        </select>
                        <select id="selectFilterWeek" style="left: 0px; position: relative; font-size: 12px; width: 75px;"
                            class="classfont">
                        </select>
                        <input type="text" id="inputFilterDate" name="inputFilterDate" style="width: 100px; display: none;" />
                    </div>
                </div>
                <div style="display: table-row; height: 5px;">
                </div>
                <div style="display: table-row">
                    <div style="display: table-cell">
                    </div>
                    <div style="display: table-cell; width: 50px;">
                    </div>
                    <div style="display: table-cell">
                        <input type="button" class="classbutton classfont" value="Refresh" id="btnrefresh" />
                    </div>
                </div>
                <br />
            </div>
        </div>
    </div>
    <%--Dialog--%>
    <div id="dialogoverview" title="Overview Map" class="dialog" style="visibility: hidden; opacity: 1;">
    </div>
    <div id="dialogdrawing" title="Drawing" class="dialog classfont" style="visibility: hidden; opacity: 1; font-size: 14px;">
        <div class="tools">
            <img src="../Image/toolbar/DrawPoint.png" alt="img" id="drawpoint" height="24" width="24" />
        </div>
        <div class="tools" style="left: 4px;">
            <img src="../Image/toolbar/DrawPolyline.png" alt="img" id="drawpolyline" height="24"
                width="24" />
        </div>
        <div class="tools" style="left: 8px;">
            <img src="../Image/toolbar/DrawPolygon.png" alt="img" id="drawpolygon" height="24"
                width="24" />
        </div>
        <div class="tools" style="left: 12px;">
            <img src="../Image/toolbar/DrawRectangle.png" alt="img" id="drawrectangle" height="24"
                width="24" />
        </div>
        <div class="tools" style="left: 16px;" onclick="ClearGraphic()">
            <img src="../Image/toolbar/StopDraw.png" alt="img" id="stopdraw" height="24" width="24" />
        </div>
        <br />
        <br />
        <br />
        <input type="checkbox" name="allow-pan" value="allow-pan" id="allowpancheckbox" checked="checked"
            onclick="AllowPan(this);" />
        Allow pan while drawing
    </div>
    <div id="dialogmeasurement" title="Measurment" class="dialog classfont" style="visibility: hidden; font-size: 14px; opacity: 1;">
        <div class="tools">
            <img src="../Image/toolbar/Mline.png" alt="imv" id='Mline' />
        </div>
        <div class="tools" style="left: 4px;">
            <img src="../Image/toolbar/Mpoligon.png" alt="img" id='MPolygon' />
        </div>
        <br />
        <br />
        <br />
        <div id="output">
        </div>
    </div>
    <div id="dialoglegend" title="Legend" class="dialog" style="visibility: hidden; opacity: 1;">
        <%--<img id='imgdialoglegend' alt="Draw" />--%><br />
        <img src="http://172.30.1.32:8080/geoserver/PZO/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=PZO:pzo_block_week" alt="img" id='legendlayer' />
        <br />
        <br />
    </div>
    <div id="dialogloginhistory" style='visibility: hidden; opacity: 1;' title="Login History"
        class="dialog classfont">
        <div id="sorthistory" style="font-size: 12px; font-family: Calibri;">
            <table>
                <tr>
                    <td>User
                    </td>
                    <td colspan='3'>
                        <input type="text" id="textuserhistory" name="textuserhistory" />
                    </td>
                </tr>
                <tr>
                    <td>Date
                    </td>
                    <td>
                        <input type="text" id="datefrom" name="datefrom" style="width: 100px;" />
                    </td>
                    <td>to
                        <input type="text" id="dateto" name="dateto" style="width: 100px;" />
                    </td>
                    <td>
                        <input type="button" id="btnfilterhistory" name="btnfilterhistory" value="Search"
                            class="classbutton" style="width: 70px;" />
                    </td>
                </tr>
            </table>
        </div>
        <hr />
        <div id='loginhistorycontent'>
        </div>
    </div>
    <div id="dialogchangepassword" style='visibility: hidden; opacity: 1;' title="Change Password"
        class="dialog classfont">
        <table style='font-family: Verdana; font-size: 12px'>
            <tr>
                <td style="height: 25px;">Old Password
                </td>
                <td>
                    <input type="password" id="oldpassword" name="oldpassword" style="font-family: Verdana;" />
                </td>
            </tr>
            <tr>
                <td style="height: 25px;">New Password
                </td>
                <td>
                    <input type="password" id="newpassword" name="newpassword" style="font-family: Verdana;" />
                </td>
            </tr>
            <tr>
                <td style="height: 25px;">Confirm New Password
                </td>
                <td>
                    <input type="password" id="conformnewpassword" name="conformnewpassword" style="font-family: Verdana;" />
                </td>
            </tr>
        </table>
        <hr />
        <input type="button" id="btnpasswordchange" name="btnpasswordchange" value="Change"
            style='width: 60px' class="classbutton" />
        <input type="button" id="btncancelchange" name="btncancelchange" value="Cancel" style='width: 60px'
            class="classbutton" />
    </div>
    <div id="dialogprojectdetail" title="Project Detail" class="dialog" style="visibility: hidden;">
    </div>
    <div id="dialoginfo" title="Info" class="dialog classfont" style="visibility: hidden; font-size: 14px; opacity: 1;">
        <div id="divfeaturename" style="font-size: 14px; font-weight: bold;">
        </div>
        <div id="divinfo">
        </div>
    </div>
    <div id="dialogloading" title="File Nursery History" class="classfont" style="visibility: hidden; opacity: 1; font-size: 13px; background: #FFFFFF !important; border: 2px solid #40C256 !important;">
        <table>
            <tr>
                <td>
                    <img src="../Image/toolbar/Loading.gif" height="50px" width="50px" />
                </td>
                <td>Please Wait...
                </td>
            </tr>
        </table>
    </div>
    <div id="dialogwms" title="Change WMS Layer" class="dialog classfont" style="visibility: hidden; opacity: 1; font-size: 14px;">
        <table>
            <tr>
                <td>WMS URL
                </td>
                <td>
                    <input type="text" id="textwmsurl" style="width: 300px;" />
                </td>
            </tr>
            <tr>
                <td>WMS Name
                </td>
                <td>
                    <input type="text" id="textwmsname" style="width: 100px;" />
                </td>
            </tr>
            <tr style="height: 50px;">
                <td colspan="2">
                    <input type="button" id="btnwmsok" value="Change" class="classbutton classfont" />
                    <input type="button" id="btnwmscancel" value="Cancel" class="classbutton classfont" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divBackground" class="modal">
    </div>
    <div id="divImage">
        <table style="height: 100%; width: 100%">
            <tr>
                <td valign="middle" align="center">
                    <img id="imgLoader" alt="" src="../../images/loading.gif" />
                    <img id="imgFull" alt="" src="" style="display: none; height: 500px; width: 590px" />
                </td>
            </tr>
            <tr>
                <td align="center" valign="bottom">
                    <input id="btnClose" type="button" value="close" onclick="HidePreview()" />
                </td>
            </tr>
        </table>
    </div>
    <div id="dialogsurveypiezometer" title="Piezometer" class="dialog classfont" style="visibility: hidden; opacity: 1;">
        <div id='ContentPiezoHeader' style="font-size: 15px; color: Black;">
        </div>
        <br />
        <div id='surveypiezometer'>
        </div>
    </div>
    <form id="Form1" runat="server">
        <asp:HiddenField ID="usergroup" runat="server" />
        <asp:HiddenField ID="userid" runat="server" />
        <asp:HiddenField ID="pzoAcc" runat="server" />
        <asp:HiddenField ID="projectid" runat="server" />
        <asp:HiddenField ID="projectname" runat="server" />
        <asp:HiddenField ID="userrole" runat="server" />
        <asp:HiddenField ID="username" runat="server" />
        <asp:HiddenField ID="utmprojection" runat="server" />
        <asp:HiddenField ID="rsid" runat="server" />
        <asp:HiddenField ID="extent" runat="server" />
        <asp:HiddenField ID="numzoom" runat="server" />
        <asp:HiddenField ID="legend" runat="server" />
        <asp:HiddenField ID="company" runat="server" />
        <asp:HiddenField ID="estate" runat="server" />
        <asp:HiddenField ID="mapped_estate" runat="server" />
        <asp:HiddenField ID="date" runat="server" />
        <asp:HiddenField ID="idweek" runat="server" />
        <asp:HiddenField ID="year" runat="server" />
        <asp:HiddenField ID="month" runat="server" />
        <asp:HiddenField ID="monthname" runat="server" />
        <asp:HiddenField ID="week" runat="server" />
        <asp:HiddenField ID="monthnamealias" runat="server" />
        <asp:HiddenField ID="currentextent" runat="server" />
        <asp:HiddenField ID="typemodule" runat="server" />
        <asp:HiddenField ID="estshortname" runat="server" />
        <asp:HiddenField ID="estname" runat="server" />
        <asp:HiddenField ID="tableblock" runat="server" />
        <asp:HiddenField ID="selectedestateforfeedback" runat="server" />
        <asp:HiddenField ID="estatezone" runat="server" />
        <asp:HiddenField ID="listyear" runat="server" />
        <asp:HiddenField ID="listmonth" runat="server" />
        <asp:HiddenField ID="listweek" runat="server" />
        <asp:HiddenField ID="isPieChart" runat="server" />
    </form>
    <script type="text/javascript" src="../Source/Openlayers2/OpenLayers.js?v=t1"></script>
    <script type="text/javascript" src="../Source/Jquery/jquery-1.11.2.min.js?v=t1"></script>
    <script type="text/javascript" src="../Source/Jquery/jquery-ui.min.js?v=t1"></script>
    <script type="text/javascript" src="../Source/Jquery/jquery.iframe-transport.js?v=t1"></script>
    <script type="text/javascript" src="../Source/Jquery/jquery.fileupload.js?v=t1"></script>
    <script type="text/javascript" src="../Script/WebService.js?v=t1"></script>
    <script type="text/javascript" src="../Script/MapOnLoad.js?v=t1"></script>
    <script type="text/javascript" src="../Script/MapFunction.js?v=t1"></script>
    <script type="text/javascript" src="../Script/MapMain.js?v=t1"></script>
    <script type="text/javascript" src="../Source/Page/oneSimpleTablePaging-1.0.js?v=t1"></script>
    <script type="text/javascript" src="../Source/Converter/Converter.js?v=t1"></script>
    <script type="text/javascript" src="../Source/DialogExtend/DialogExtend.js"></script>
    <script type="text/javascript" src="../Source/Jquery/AutoComplete.js"></script>
    <script type="text/javascript" src="../Source/DataTable/js/jquery.dataTables.js"></script>
    <script src="../Source/adminlte/bower_components/select2/dist/js/select2.js"></script>

    <style type="text/css">
        .modal {
            display: none;
            position: absolute;
            top: 0px;
            left: 0px;
            background-color: black;
            z-index: 100;
            opacity: 0.8;
            filter: alpha(opacity=60);
            -moz-opacity: 0.8;
            min-height: 100%;
        }

        #divImage {
            display: none;
            z-index: 1000;
            position: fixed;
            top: 0;
            left: 0;
            background-color: White;
            height: 550px;
            width: 600px;
            padding: 3px;
            border: solid 1px black;
        }
    </style>
</body>
</html>
