﻿using System;
using System.Data;
using System.Web.UI;
using PASS.Mapper;
using PZO_PiezometerOnlineMap.Class;
using PZO_PiezometerOnlineMap.Mapper;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace PZO_PiezometerOnlineMap.Content
{
    public partial class PZO_Map : System.Web.UI.Page
    {
        #region Variable Declaration

        private PZO_ProjectMapper mapper = new PZO_ProjectMapper();
        private OLM_OnlineMapper olmMapper = new OLM_OnlineMapper();
        private Cryptography crypto = new Cryptography();

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uID"] == null)
            {
                //Response.Redirect("~/Default.aspx");
            }          
            try
            {
                mapped_estate.Value = mapper.GetMappedEstate(Request.Params["Estate"]);
                estate.Value = Request.Params["Estate"];

                DataSet ds = mapper.GetEstateDetail(mapped_estate.Value);
                DataTable dt = ds.Tables[0];
                DataTable dtUTMProjection = ds.Tables[1];
                DataTable dtWeek = new DataTable();
                DataTable dtListWeek = mapper.GetListWeek();
                utmprojection.Value = dtUTMProjection.Rows[0]["UTMProjection"].ToString();
                rsid.Value = dt.Rows[0]["Projection"].ToString();
                extent.Value = dt.Rows[0]["CoordinateExtent"].ToString();
                numzoom.Value = dt.Rows[0]["NumZoom"].ToString();
                legend.Value = "";
                company.Value = Request.Params["Company"];
                estatezone.Value = mapper.GetZone(mapped_estate.Value);
                date.Value = "1st Week - Oct 2016";

                listyear.Value = ObjectToJSON(dtListWeek.DefaultView.ToTable(true, "Year"));
                listmonth.Value = ObjectToJSON(dtListWeek.DefaultView.ToTable(true, "Year", "Month", "MonthName"));
                listweek.Value = ObjectToJSON(dtListWeek);

                if (Request.Params["IDWEEK"] == null || Request.Params["IDWEEK"] == "")
                {
                    dtWeek = mapper.GetWeekFromCurrentDate();
                }
                else
                {
                    dtWeek = mapper.GetWeekFromParameter(Request.Params["IDWEEK"]);
                }

                year.Value = dtWeek.Rows[0]["Year"].ToString();
                month.Value = dtWeek.Rows[0]["Month"].ToString();
                monthname.Value = dtWeek.Rows[0]["MonthName"].ToString();
                week.Value = dtWeek.Rows[0]["Week"].ToString();
                monthnamealias.Value = dtWeek.Rows[0]["MonthName"].ToString();

                idweek.Value = dtWeek.Rows[0]["ID"].ToString();
                currentextent.Value = Request.Params["Extent"] != null ? Request.Params["Extent"] : "";
                typemodule.Value = Request.QueryString["typeModule"].ToString();
                tableblock.Value = dt.Rows[0]["TableBlock"].ToString();
                isPieChart.Value = "true";
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + "No Data" + " (" + ex.Message + ")" + "')", true);
            }
        }

        private string ObjectToJSON(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
      
    }
}