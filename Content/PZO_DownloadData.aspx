﻿﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PZO_DownloadData.aspx.cs" Inherits="PZO_PiezometerOnlineMap.Content.PZO_DownloadData" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Map | Piezometer</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Bootstrap Select -->
    <link rel="stylesheet" href="../Source/adminlte/bootstrap-select.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.dataTables.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../Source/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="../Source/adminlte/plugins/iCheck/all.css">
    <link rel="stylesheet" href="../Source/adminlte/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../Source/css/jquery-confirm.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="../Style/Loading.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Source/DataTable/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.bootstrap4.min.css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/select2/dist/css/select2.css">


    <%--custom library--%>
    <link href="../Source/adminlte/bower_components/jquery-ui/1.12.1/jquery-ui.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    <link rel="stylesheet" href="../Source/alwan/css/alwan.min.css">


    <style>
        .table-text-center th, .table-text-center td {
            text-align: center;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #4338ca;
            border: none;
            border-radius: 4px;
            cursor: default;
            float: left;
            margin-right: 5px;
            margin-top: 5px;
            padding: 0 5px;
        }

        .box {
            background: #ecf0f5;
            border-top: none;
        }

        .connectedSortable li {
            display: inline-block;
            padding: 0 10px;
            font-size: 16px;
            line-height: 30px;
            border-radius: 6px;
            margin: 4px;
            cursor: move;
            border: none;
        }

        .square-j::after {
            position: absolute;
            top: -5px;
            right: -3px;
            width: 12px;
            height: 12px;
            background: #fff;
            content: " ";
            /* padding: 1px; */
            z-index: 10;
            border-radius: 10px;
        }
    </style>
</head>
<body class="hold-transition skin-green-light sidebar-mini fixed">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="#" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>PZO</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Piezometer</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    <li>
                        <a href="PZO_Dashboard.aspx">
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <%--<li>
                        <a href="PZO_Map.aspx">
                            <i class="fa fa-map"></i>
                            <span>Online Map</span>
                        </a>
                    </li>--%>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-map"></i><span>Online Map</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a id="map_default" href="PZO_Map.aspx"><i class="fa fa-circle-o"></i>Default Map</a></li>
                            <li><a id="map_analysis" href="PZO_MapAnalysis.aspx"><i class="fa fa-circle-o"></i>Analysis Map</a></li>
                            <%--<li><a id="olmTMAS" href="PZO_NewAnalysisTMAS.aspx"><i class="fa fa-circle-o"></i>Analysis Map for TMAS</a></li>
                            <li><a id="mapbypt" href="PZO_MapByPT.aspx"><i class="fa fa-circle-o"></i>Online Map by PT</a></li>--%>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="#">
                            <i class="fa fa-files-o"></i>
                            <span>Download Data</span>
                        </a>
                    </li>
                    <%--<li id="liUploadData">
                        <a href="PZO_UploadData.aspx">
                            <i class="fa fa-files-o"></i>
                            <span>Upload Data</span>
                        </a>
                    </li>--%>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-gear"></i><span>Maintenance Data</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a id="master_hobo" href="PZO_MasterPiezo.aspx"><i class="fa fa-circle-o"></i>Master Master Piezo</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="?act=LogOut">
                            <i class="fa fa-sign-out"></i>
                            <span id="txtLogout" runat="server">Keluar</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!--<section class="content-header">
            </section>-->

            <!-- Main content -->
            <section class="content">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#data" data-toggle="tab">Data</a></li>
                        <%--<li><a href="#laporanklhk" data-toggle="tab">Laporan KLHK</a></li>--%>
                        <li id="tabgenerateklhk"><a href="#generatedataklhk" data-toggle="tab">Generate Data KLHK</a></li>
                        <%--<li id="tabblockrehab"><a href="#blockrehab" data-toggle="tab">Add Block Rehab</a></li>--%>
                        <li id="tabPencatatanPerubahanTMAT"><a href="#blockPencatatanTMAT" data-toggle="tab">Pencatatan Perubahan TMAT</a></li>
                        <%--<li id="tabGrapTmatTmas"><a href="#grapTmatTmas" data-toggle="tab">Graph TMAT dan TMAS</a></li>--%>
                        <li id="tabPetaperbandingan"><a href="#petaPerbadingan" data-toggle="tab">Peta Perbandingan Kondisi</a></li>
                        <li id="tabPetaKondisiPiezo"><a href="#petaKondisiPiezo" data-toggle="tab">Peta Kondisi Piezo</a></li>
                        <li id="tabGraphTmat"><a href="#graphTmatj" data-toggle="tab">Graph TMAT</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="data">
                            <div class="row">
                                <div class="form-group" style="margin-left: 15px; display: none;">
                                    <label>Filter Option</label>
                                    <input type="radio" value="filter By Date" id="rbFilterByDate" name="rbOption" class="flat-green" checked="checked" />
                                    Filter By Date
                                       
                                    <input type="radio" value="filter By Week" id="rbFilterByWeek" name="rbOption" class="flat-green" />
                                    Filter By Date   
                               
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Company</label>
                                        <select id="selectFilterCompany" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Estate</label>
                                        <select id="selectFilterEstate" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3" id="divPeriode">
                                    <label>Periode</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control pull-right" id="periode" />
                                        <br />
                                        <br />
                                        <button type="button" id="btnSubmitFilter" class="btn btn-success pull-right">Go</button>
                                    </div>
                                </div>
                                <div class="col-md-3" id="divWeekDropDown" style="display: none;">
                                    <label>Week 1</label>
                                    <div class="form-group">
                                        <select id="weekDropdown" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                        <br />
                                        <br />
                                        <%--<button type="button" id="btnSubmitFilter" class="btn btn-success pull-right">Go</button>--%>
                                    </div>
                                </div>
                                <div class="col-md-3" id="divWeekDropDown2" style="display: none;">
                                    <label>Week 2</label>
                                    <div class="form-group">
                                        <select id="weekDropdown2" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                        <br />
                                        <br />
                                        <button type="button" id="btnSubmitFilterWeek" class="btn btn-success pull-right">Go</button>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div id="divTableData"></div>
                        </div>
                        <div class="tab-pane" id="laporanklhk">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Company</label>
                                        <select id="selectFilterCompanyForKLHK" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Estate</label>
                                        <select id="selectFilterEstateForKLHK" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Periode</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control pull-right" id="periodeForKLHK">
                                            <br />
                                            <br />
                                            <button type="button" id="btnSubmitFilterForKLHK" class="btn btn-success pull-right">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br />
                            <div id="divTableKLHK"></div>
                        </div>

                        <div class="tab-pane" id="generatedataklhk">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Company</label>
                                        <select id="selectFilterCompanyForGenerateKLHK" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Estate</label>
                                        <select id="selectFilterEstateForGenerateKLHK" multiple class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Periode</label>
                                        <div class="form-group">
                                            <select id="selectFilterPeriodeForGenerateKLHK" class="form-control selectpicker" data-live-search="true">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Year</label>
                                        <div class="form-group">
                                            <select id="selectFilterYearForGenerateKLHK" class="form-control selectpicker" data-live-search="true">
                                                <option value="2019">2019</option>
                                                <option selected value="2020">2020</option>
                                            </select>
                                            <br />
                                            <br />
                                            <button type="button" id="btnSubmitFilterForGenerateKLHK" class="btn btn-success pull-right">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-2" id="a" style="display: flex; margin-right: -26px;">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <button style="width: 150px;" type="button" id="btnDownloadTemplateKLHK" class="btn btn-success pull-right">
                                                    Download Template</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2" id="d" style="display: flex; margin-right: -26px;">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <button style="width: 150px;" type="button" id="btnSaveGenerateManualKLHK" class="btn btn-success pull-right">Approve</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2" id="h" style="display: flex; margin-right: -26px;">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <button style="width: 150px;" type="button" id="btnCekNullDataKLHK" class="btn btn-success pull-right">Check Data</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2" id="c" style="display: flex; margin-right: -26px;">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <button style="width: 150px;" type="button" id="btnUploadKLHK" class="btn btn-success pull-right">Upload</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2" id="e" style="display: flex; margin-right: -26px;">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <button style="width: 150px;" type="button" id="btnUpdateKLHK" class="btn btn-success pull-right">Update</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2" id="b">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <button style="width: 150px;" type="button" id="btnGenerateKLHK" class="btn btn-success pull-right">Generate</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6" id="f">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Status : </label>
                                            <label id="statusGenerate"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" id="g">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Time : </label>
                                            <label id="statusDate"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divTableGenerateKLHK"></div>

                            <div class="modal fade" data-backdrop="static" id="mdlImportData" tabindex="-1" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Import From Excel</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form id="formImportHoliday" class="form-horizontal" role="form" method="post">
                                                <div class="form-group">
                                                    <div class="col-xs-8 col-xs-offset-2">
                                                        <input id="fileUpload" type="file" name="upload_file" required="" accept=".xlsx" />
                                                        <br />
                                                        <button id="btnUpload" class="btn btn-success" type="button"><span class="glyphicon glyphicon-upload"></span>Import From Excel</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <%--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>--%>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <div class="modal fade" data-backdrop="static" id="mdlCheckData" tabindex="-1" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Check Data TMAT</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div id="divTableDetailNullData"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <%--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>--%>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </div>
                        <div class="tab-pane" id="blockrehab">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Company</label>
                                        <select id="selectFilterCompanyBlockRehab" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label style="visibility: hidden">Company</label>
                                        <br />
                                        <button type="button" id="btnSubmitFilterForBlockRehab" class="btn btn-success pull-right">Go</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label style="visibility: hidden">Company</label>
                                        <br />
                                        <button type="button" id="btnAddBlockRehab" class="btn btn-success pull-right">Add</button>
                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label style="visibility: hidden">Company</label>
                                        <br />
                                        <button type="button" id="btnClearBlockRehab" class="btn btn-success pull-right">Clear</button>
                                    </div>
                                </div>
                                <br />

                            </div>
                            <br />
                            <div id="divTableBlockRehab"></div>
                            <div class="modal fade" data-backdrop="static" id="mdlblockRehab" tabindex="-1" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Add Block Rehab</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Company</label>
                                                        <select id="selectFilterAddCompanyBlockRehab" class="form-control selectpicker" data-live-search="true">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Estate</label>
                                                        <select id="selectFilterAddEstateBlockRehab" multiple class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Block</label>
                                                        <select id="selectFilterAddBlockRehab" multiple class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label style="visibility: hidden">Company</label>
                                                        <br />
                                                        <button type="button" id="btnSaveBlockRehab" class="btn btn-success pull-right">Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <%--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>--%>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                        </div>
                        <div class="tab-pane" id="blockPencatatanTMAT">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>WM Area</label>
                                        <select id="selectFilterEstatePulauPerubahanTMAT" multiple class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                        </select>
                                        <br />
                                        <br />
                                        <label id="lblDataTidakLengkap">Data Tidak Lengkap : 0</label><br />
                                        <label id="lblTotalData">Total Data : 0</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Estate</label>
                                        <select id="selectFilterEstatePencatatanPerubahanTMAT" multiple class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Zona</label>
                                        <select id="selectFilterZonaPencatatanPerubahanTMAT" multiple class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Periode</label>
                                        <div class="container-fluid">
                                            <button type="button" id="btnPrevWeekPencatatanPerubahan" class="btn btn-success"><i class="fa fa-angle-left"></i></button>
                                            <label id="lblTitleWeek">Week Name </label>
                                            <button type="button" id="btnNextWeekPencatatanPerubahan" class="btn btn-success"><i class="fa fa-angle-right"></i></button>
                                            <input type="hidden" id="txtWeekId" />
                                            <button type="button" id="btnThisWeek" class="btn btn-success" style="position: static;">This Week</button>

                                            <%--<button type="button" id="btnSubmitFilterForPencatatanPerubahanTMAT" class="btn btn-success pull-right" style="margin-left: 5px;">Go</button>
                                            <button type="button" id="btnSubmitFilterForPencatatanPerubahanTMATPDF" class="btn btn-success pull-right">PDF</button>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3" style="">
                                    <div class="form-group">
                                        <label>Class</label>
                                        <div class="form-group" style="padding-left: 15px;">
                                            <input type="radio" id="checkAll" name="check" value="2" checked="checked">
                                            <label for="checkAll">All</label>

                                            <input type="radio" id="checkStandar" name="check" value="0">
                                            <label for="checkStandar">Standar</label>

                                            <input type="radio" id="checkklhk" name="check" value="1">
                                            <label for="checkklhk">Klhk</label>
                                            <button type="button" id="btnSubmitFilterForPencatatanPerubahanTMAT" class="btn btn-success pull-right" style="margin-left: 5px;">Go</button>
                                            <button type="button" id="btnSubmitFilterForPencatatanPerubahanTMATPDF" class="btn btn-success pull-right">PDF</button>
                                        </div>
                                    </div>
                                </div>



                            </div>


                            <div class="col-md-1">
                                <div class="form-group">
                                    <label style="visibility: hidden">Company</label>
                                    <br />
                                    <%--<button type="button" id="btnSubmitFilterForPencatatanPerubahanTMAT" class="btn btn-success pull-right">Go</button>
                                        <button type="button" id="btnSubmitFilterForPencatatanPerubahanTMATPDF" class="btn btn-success pull-right">PDF</button>--%>
                                </div>
                            </div>
                            <div class="col-md-12" id="tempatTable">
                            </div>
                        </div>

                        <div class="tab-pane" id="petaPerbadingan">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Company</label>
                                        <select id="selectFilterCompanyForPerbandingan" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Periode</label>
                                        <div class="container-fluid">
                                            <button type="button" id="btnPrevWeekPetaPerbandingan" class="btn btn-success"><i class="fa fa-angle-left"></i></button>
                                            <label id="lblTitleWeekPetaPerbandingan">Week Name </label>
                                            <button type="button" id="btnNextWeekPetaPerbandingan" class="btn btn-success"><i class="fa fa-angle-right"></i></button>
                                            <input type="hidden" id="txtWeekIdPetaPerbandingan" />
                                            <button type="button" id="btnThisWeekPetaPerbandingan" class="btn btn-success" style="position: static;">This Week</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label></label>
                                        <div class="container-fluid">
                                            <button type="button" id="btnSubmitPerbedaanPetaGo" class="btn btn-success pull-right">Go</button>
                                            <button type="button" id="btnCaptureReportMap" style="margin-right: 15px;" class="btn btn-success pull-right">Capture Report</button>
                                            <button type="button" id="btnDownloadTable" style="margin-right: 15px;" class="btn btn-success pull-right">Capture Table Only</button>
                                            <button type="button" id="btnDownloadPeta" style="margin-right: 15px;" class="btn btn-success pull-right">Capture Map Only</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 4px;">
                                <div class="col-md-6">
                                    <button type="button" id="btnGeneratePetaPerbandingan" class="btn btn-success" style="position: static;">Generate</button>
                                </div>
                                <div class="col-md-6">
                                    <p style="text-align: right; color: red; font-size: 11px; width: 100%;">*jika peta tidak muncul coba akses melalui <a href="http://app.gis-div.com/PZO/Content/PZO_DownloadData.aspx">link ini</a> dan buka menggunakan google chrome terbaru</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" style="background-color: white;">
                                    <button type="button" id="btnDownloadPetaLastWeek" class="btn btn-success">Capture Map Last Week</button>
                                    <button type="button" id="btnDownloadTableLastWeek" class="btn btn-success pull-right">Capture Table Last Week</button>
                                </div>
                                <div class="col-md-6" style="background-color: white;">
                                    <button type="button" id="btnDownloadPetaThisWeek" class="btn btn-success">Capture Map This Week</button>
                                    <button type="button" id="btnDownloadTableThisWeek" class="btn btn-success pull-right">Capture Table This Week</button>
                                </div>
                            </div>
                            <div id="divReportPetaPerbadingan">
                                <div class="row">
                                    <div class="col-md-6" style="background-color: white;">
                                        <p id="lblLastWeek" style="font-weight: bold; font-size: 25px; z-index: 999; position: relative;"></p>
                                    </div>
                                    <div class="col-md-6" style="background-color: white;">
                                        <p id="lblThisWeek" style="font-weight: bold; font-size: 25px; z-index: 999; position: relative;"></p>
                                    </div>
                                </div>
                                <div class="row" id="divMap">
                                    <div class="col-md-6" style="background-color: white;">
                                        <div id="imgLastWeek"></div>
                                    </div>
                                    <div class="col-md-6" style="background-color: white;">
                                        <div id="imgThisWeek"></div>
                                    </div>
                                </div>
                                <div class="row" id="divSummary">
                                    <div class="col-md-6" style="background-color: white;">
                                        <div class="row" id="summaryLastWeek" style="background-color: white;">
                                            <div class="col-md-6" style="margin-top: 10px; background-color: white;" id="tblPetaLastWeek"></div>
                                            <div class="col-md-6" style="margin-top: 10px; background-color: white;" id="tblSummaryLastWeek"></div>
                                            <%--<div class="col-md-3" style="margin-top: 10px;">
                                                <p id="lblKetLastWeek" style="text-align: center; font-weight: bold; font-size: 20px; font-family: Arial Narrow;"></p>
                                            </div>--%>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="background-color: white;">
                                        <div class="row" id="summaryThisWeek" style="background-color: white;">
                                            <div class="col-md-6" style="margin-top: 10px; background-color: white;" id="tblPetaThisWeek"></div>
                                            <div class="col-md-6" style="margin-top: 10px; background-color: white;" id="tblSummaryThisWeek"></div>
                                            <%--<div class="col-md-3" style="margin-top: 10px;">
                                                <p id="lblKetThisWeek" style="text-align: center; font-weight: bold; font-size: 20px; font-family: Arial Narrow;"></p>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="petaKondisiPiezo">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Company</label>
                                        <select id="selectFilterCompanyForKondisiPiezo" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Periode</label>
                                        <div class="container-fluid">
                                            <button type="button" id="btnPrevWeekPetaKondisiPiezo" class="btn btn-success"><i class="fa fa-angle-left"></i></button>
                                            <label id="lblTitleWeekPetaKondisiPiezo">Week Name</label>
                                            <button type="button" id="btnNextWeekPetaKondisiPiezo" class="btn btn-success"><i class="fa fa-angle-right"></i></button>
                                            <input type="hidden" id="txtWeekIdPetaKondisiPiezo" />
                                            <button type="button" id="btnThisWeekPetaKondisiPiezo" class="btn btn-success" style="position: static;">This Week</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Kondisi</label>
                                        <select id="selectFilterKondisiForKondisiPiezo" class="form-control selectpicker" data-live-search="true">
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Berturut-turut</label>
                                        <select id="selectFilterBerturutTurutForKondisiPiezo" class="form-control selectpicker" data-live-search="true">
                                            <option value="4">>= 4 Minggu</option>
                                            <option value="5">>= 5 Minggu</option>
                                            <option value="6">>= 6 Minggu</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row" >
                                <div class="col-md-6">
                                    <p style="text-align: left; color: red; font-size: 11px; width: 100%;">*jika peta tidak muncul coba akses melalui <a href="http://app.gis-div.com/PZO/Content/PZO_DownloadData.aspx">link ini</a> dan buka menggunakan google chrome terbaru</p>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="container-fluid" style="text-align:right;">
                                            <button type="button" id="btnGoKondisiPiezo" class="btn btn-success pull-right">Go</button>
                                            <button type="button" id="btnCaptureReportMapKondisiPiezo" style="margin-right: 15px;" class="btn btn-success pull-right">Capture Report</button>
                                            <button type="button" id="btnDownloadTablKondisiPiezoe" style="margin-right: 15px;" class="btn btn-success pull-right">Capture Table Only</button>
                                            <button type="button" id="btnDownloadPetaKondisiPiezo" style="margin-right: 15px;" class="btn btn-success pull-right">Capture Map Only</button>
                                            <button type="button" id="btnGenerateKondisiPiezo" style="margin-right: 15px;" class="btn btn-success pull-right">Generate</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divReportKondisiPiezo" style="background-color: white;">
                                <div class="row">
                                    <div class="col-md-12" style="background-color: white;">
                                        <p id="lblThisWeekKondisiPiezo" style="font-weight: bold; font-size: 25px; z-index: 999; position: relative;"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="divMapKondisiPiezo" class="col-md-6" style="background-color: white;">
                                        <div id="imgThisWeekKondisiPiezo"></div>
                                    </div>
                                    <div id="divSummaryKondisiPiezo" class="col-md-6" style="background-color: white;">
                                        <div class="row" id="summaryKondisiPiezoThisWeek" style="background-color: white;margin-left: 5px;margin-right: 5px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane" id="grapTmatTmas">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>WM Area</label>
                                        <select id="selectFilterEstatePulauGrapTmasTmat" multiple class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                        </select>
                                        <%--<br />
                                        <br />
                                        <label id="lblDataTidakLengkap">Data Tidak Lengkap : 0</label><br />
                                        <label id="lblTotalData">Total Data : 0</label>--%>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Estate</label>
                                        <select id="selectFilterEstateGrapTmasTmat" multiple class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Zona</label>
                                        <select id="selectFilterZonaGrapTmasTmat" class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Periode</label>
                                        <div class="container-fluid">
                                            <button type="button" id="btnPrevWeekGrapTmasTmat" class="btn btn-success"><i class="fa fa-angle-left"></i></button>
                                            <label id="lblTitleWeekGrapTmasTmat">Week Name </label>
                                            <button type="button" id="btnNextWeekGrapTmasTmat" class="btn btn-success"><i class="fa fa-angle-right"></i></button>
                                            <input type="hidden" id="txtWeekIdGrapTmasTmat" />
                                            <button type="button" id="btnThisWeekGrapTmasTmat" class="btn btn-success" style="position: static;">This Week</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2" style="margin-left: -35px">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label>
                                                <input type="radio" id="chkAllPiezo" name="radioButtonFilter" class="iradio_futurico" checked>
                                                All Piezo
                                           
                                            </label>
                                            <a href="#" id="lblGraphTmasTmatAllPzo"><u>Qty (%) </u></a>
                                            <br>
                                            <label>
                                                <input type="radio" id="chkBlackOnly" name="radioButtonFilter" class="iradio_futurico">
                                                Selected
                                           
                                            </label>
                                            <a href="#" id="lblGraphTmasTmatSelected"><u>Qty (%) </u><i class="fa fa-search"></i></a>
                                            <br>
                                            <label>
                                                <input type="radio" id="chkBlack" name="radioButtonFilter" class="iradio_futurico">
                                                Un Selected
                                           
                                            </label>
                                            <a href="#" id="lblGraphTmasTmatUnSelected"><u>Qty (%)</u> <i class="fa fa-search"></i></a>
                                            <br>
                                            <label id="lblBlackList" style="color: blue; cursor: pointer; display: none;">
                                                <u>0 Piezometer Black List</u>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <div class="form-group" style="padding-top: 25px;">
                                            <button type="button" id="btnGoGrapTmasTmat" class="btn btn-success">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" style="height: 1050px;">
                                    <br>
                                    <div id="divGraph"></div>
                                    <br>
                                    <div id="divGraph2" style="margin-top: 400px;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="graphTmatj">
                            <div class="box ">
                                <div class="box-header with-border">
                                    <!-- tools box -->
                                    <div class="pull-right box-tools">
                                        <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse"
                                            data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <i class="fa fa-cogs"></i>

                                    <h3 class="box-title">Pilih Graph
                                    </h3>
                                </div>
                                <div class="box-body with-border">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>WM Area</label>
                                                <select id="selectFilterWmArea_j" class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Graph</label>
                                                <select id="selectFilterGraph_j" class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Date Range</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                    <input type="text" class="form-control pull-right" id="tglSelectedGraph" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Per Day / Week</label>
                                                        <select id="selectFilterPer" class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                                            <option value="" disabled>Nothing selected</option>
                                                            <option value="1">Day</option>
                                                            <option value="2">Week</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div style="margin-bottom: 25px"></div>
                                                        <button id="btnDeleteGraphTmat" class="btn btn-danger btn-sm btn-block">Delete</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box collapsed-box">
                                <div class="box-header with-border">
                                    <!-- tools box -->
                                    <div class="pull-right box-tools">
                                        <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse"
                                            data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <i class="fa fa-cogs"></i>

                                    <h3 class="box-title">Setting Graph
                                    </h3>
                                </div>
                                <div class="box-body with-border">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Zona</label>
                                                <select id="selectFilterZona_j" multiple class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Estate</label>
                                                <select id="selectFilterEstate_j" multiple class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Piezometer</label>
                                                <select id="selectFilterPizometer_J" multiple class="form-control selectpicker" data-live-search="true" data-actions-box="true" data-selected-text-format="count">
                                                </select>
                                                <%--    <select id="selectFilterPizometer_J" class="select2" style="width: 100%" multiple="multiple">
                                                </select>
                                                <div style="display:flex;align-items:center;">
                                                    <input type="checkbox" id="selectAllFilterPizometer_J" />
                                                    <div style="margin-left:5px;">select all</div>
                                                </div>
                                                <div id="alertSelectFilterPizometer_J" style="font-size: 13px; color: #dc2626;"></div>--%>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Curah Hujan</label>
                                                <select id="selectFilterCurahHujan_j" class="select2" style="width: 100%" multiple="multiple">
                                                </select>
                                                <div id="selectGraphDetailIotError" style="font-size: 13px; color: #dc2626;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Piezometer Refrensi:</label>
                                                <div class="input-group">
                                                    <div id='divPiezometerRefrensi' style="min-height: 192px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Nama Graph </label>
                                                <input id="namaGraph_j" placeholder="Masukan nama graph" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div style="margin-bottom: 25px"></div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <button id="btnCreateGraphTmat" class="btn btn-primary btn-sm btn-block">Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%--<div style="width: 100%; height: 20px; background-color: #ecf0f5; border-radius: 10px; margin-bottom: 10px"></div>--%>
                            <br />
                            <div id='divChart' style='height: 400px; width: 100%; margin: 0px auto;'>
                            </div>
                            <br />
                            <div id="indicatorLabelColor" style="display: flex; align-items: center; justify-content: center">
                                <%--    <div class="square-j" style="background: red; width: 12px; height: 12px; margin-right: 5px">
                                </div>
                                <div class="title-note-j" style="font-size: 14px; font-weight: bold;">
                                    title
                                </div>--%>
                            </div>

                        </div>

                        <div id="divTablePencatatanTMAT"></div>
                    </div>


                    <div class="modal fade" data-backdrop="static" id="mdlPencatatanTMAT" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Pencatatan Perubahan TMAT</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Company</label>
                                                <select id="selectFilterAddCompanyPencatatanPerubahanTMAT" class="form-control selectpicker" data-live-search="true">
                                                </select>
                                            </div>
                                        </div>


                                        <div class="col-md-3" id="divPeriodePencatatanPerubahanTMAT">
                                            <label>Periode</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control pull-right" id="periode" />
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <%--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>--%>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="mdlJabarBlock" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header" style="background-color: cornflowerblue">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h3 class="modal-title" style="margin-left: 10px; color: white">Penjabaran Block</h3>
                                    <br />
                                    <h3 class="modal-title" id="lblketKetinggian" style="margin-left: 10px; color: white">Ketinggian</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="col-md-12" id="tempatTablePenjabaran">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <%--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>--%>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="modal fade" tabindex="-1" role="dialog" id="mdlChartTMAT" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog" style="width: 800px">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="idHeadChartTMAT">List Piezometer</h4>
                                </div>
                                <div class="modal-body table-responsive" style="height: 550px; overflow-x: hidden; overflow-y: scroll;">
                                    <table id="tblListPzoChart" class="table table-striped table-bordered" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <td style="width: 20%; text-align: center;">Estate</td>
                                                <td style="width: 20%; text-align: center;">Block</td>
                                                <td style="width: 20%; text-align: center;">Piezometer</td>
                                                <td style="width: 20%; text-align: center;">Jenis</td>
                                                <td style="width: 10%; text-align: center;">Elevasi</td>
                                                <td style="width: 10%; text-align: center;">TMAT</td>
                                                <td style="width: 30%; text-align: center;">Check untuk black list<input type="checkbox" id="chkChartTMATAll"></td>
                                                <td style="width: 10%; text-align: center;">Checked By</td>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button id="btnSaveChartTMAT" type="submit" class="btn btn-primary">Save changes</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <%--<div class="modal fade" data-backdrop="static" id="mdlCheckData" tabindex="-1" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Check Data TMAT</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div id="divTableDetailNullData"></div>
                                        </div>
                                        <div class="modal-footer">
                                            <%--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>--%>
                </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

    </div>

   

    <div class="modal fade" tabindex="-1" role="dialog" id="mdlChartTMATBlackList" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 100%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="idHeadChartTMATBlackList">List Piezometer Black List</h4>
                </div>
                <div class="modal-body table-responsive" style="height: 550px; overflow-x: hidden; overflow-y: scroll;">
                    <table id="tblListPzoChartBlackList" class="table table-striped table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <td style="width: 20%; text-align: center;">Estate</td>
                                <td style="width: 20%; text-align: center;">Block</td>
                                <td style="width: 20%; text-align: center;">Piezometer</td>
                                <td style="width: 20%; text-align: center;">Jenis</td>
                                <td style="width: 20%; text-align: center;">Longitude</td>
                                <td style="width: 20%; text-align: center;">Latitude</td>
                                <td style="width: 10%; text-align: center;">Elevasi</td>
                                <td style="width: 10%; text-align: center;" id="week1">week1</td>
                                <td style="width: 10%; text-align: center;" id="week2">week2</td>
                                <td style="width: 10%; text-align: center;" id="week3">week3</td>
                                <td style="width: 10%; text-align: center;" id="week4">week4</td>
                                <td style="width: 10%; text-align: center;" id="week5">week5</td>
                                <td style="width: 10%; text-align: center;">Check untuk black list</td>
                                <td style="width: 10%; text-align: center;">Checked By</td>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button id="btnSaveChartTMATBlackList" type="submit" class="btn btn-primary">Save changes</button>
                </div>

            </div>
        </div>
    </div>

    <div class="loading" style="display: none; z-index: 99999999;"></div>
    <div id="formChangeColor"></div>

    <form id="Form1" runat="server">
        <asp:HiddenField ID="userid" runat="server" />
        <asp:HiddenField ID="listGisUser" runat="server" />
        <asp:HiddenField ID="listWaterDeep" runat="server" />
    </form>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../Source/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../Source/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Bootstrap Select -->
    <script src="../Source/adminlte/bootstrap-select.js"></script>
    <!-- Select2 -->
    <script src="../Source/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
    <!-- DataTables -->
    <script src="../Source/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/dataTables.buttons.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.flash.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/jszip.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/pdfmake.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/vfs_fonts.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.html5.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.print.min.js"></script>
    <%--  <script src="../Source/DataTable/js/dataTables.bootstrap4.min.js"></script>
    <script src="../Source/DataTable/js/dataTables.fixedHeader.js"></script>--%>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="../Source/DataTable/js/DataTableColumnTitle.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"></script>

    <!-- daterangepicker -->
    <script src="../Source/adminlte/bower_components/moment/min/moment.min.js"></script>
    <script src="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../Source/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../Source/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../Source/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../Source/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../Source/adminlte/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../Source/adminlte/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../Source/adminlte/dist/js/demo.js"></script>
    <!-- This Page Main JS -->
    <script src="../Source/adminlte/plugins/iCheck/icheck.min.js"></script>
    <script src="../Source/js/jquery-confirm.js"></script>
    <script src="../Source/canvasjs-commercial-3.6.6/canvasjs.min.js"></script>
    <script src="../Source/underscore/underscore.js"></script>
    <script src="../Source/dom-to-image/dom-to-image.js"></script>

    <%--custom library--%>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
    <script src="../Source/alwan/js/alwan.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../Source/jquery-ui.js"></script>


    <script>
        $(function () {

            // each calendar picker needs a unique class/id so we can target it
            $(".startdate").datepicker({ dateFormat: "yy-mm-dd", minDate: 0 });
            $(".enddate").datepicker({ dateFormat: "yy-mm-dd", minDate: 0 });
        });

        var fixHelperModified = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        };

        $("#sort tbody").sortable({
            helper: fixHelperModified

        }).disableSelection();
    </script>



    <script src="<%= downloaddata_js %>"></script>
    <script src="<%= chartTmasTmat_js %>"></script>
    <script src="<%= graphTMAT_js %>"></script>

</body>
</html>

