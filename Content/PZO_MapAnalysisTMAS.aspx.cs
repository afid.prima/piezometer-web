﻿using System;
using System.Data;
using System.Web.UI;
using PASS.Mapper;
using PZO_PiezometerOnlineMap.Class;
using PZO_PiezometerOnlineMap.Mapper;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace PZO_PiezometerOnlineMap.Content
{
    public partial class PZO_MapAnalysisTMAS : System.Web.UI.Page
    {
        #region Variable Declaration

        private PZO_ProjectMapper mapper = new PZO_ProjectMapper();
        private PZO_ProjectMapperTambahan mapper2 = new PZO_ProjectMapperTambahan();
        private OLM_OnlineMapper olmMapper = new OLM_OnlineMapper();
        private Cryptography crypto = new Cryptography();

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uID"] == null) Response.Redirect("~/Default.aspx");
            string value = Session["uID"] as string;
            string pzoAccess = Session["PZO5"] as string;
            if (value != null && value != "")
            {
                txtLogout.InnerText = "Keluar" + " " + "(" + Session["uname"].ToString() + ")";
                //liheader.InnerText = Session["uname"].ToString();
            }else if(value == "")
            {
                txtLogout.InnerText = "Keluar";
            }
            try
            {
                bool isValid = true;
                if (Request.Params["Company"] != null && Request.Params["Estate"] != null)
                {
                    mapped_estate.Value = mapper.GetMappedEstate(Request.Params["Estate"]);
                    estate.Value = Request.Params["Estate"];
                    company.Value = Request.Params["Company"];
                    Session["Company"] = company.Value;
                    Session["Estate"] = estate.Value;
                }
                else if (Session["Company"] != null && Session["Estate"] != null)
                {
                    mapped_estate.Value = mapper.GetMappedEstate(Session["Estate"].ToString());
                    estate.Value = Session["Estate"].ToString();
                    company.Value = Session["Company"].ToString();
                }
                else
                {
                    mapped_estate.Value = mapper.GetMappedEstate("THP1");
                    estate.Value = "THP1";
                    company.Value = "PT.THIP";
                    Session["Company"] = company.Value;
                    Session["Estate"] = estate.Value;
                    //isValid = false;
                }

                if (Request.Params["Idwm"] != null)
                {
                    idwm.Value = Request.Params["Idwm"];
                    Session["Idwm"] = idwm.Value;
                }
                else if (Session["Idwm"] != null)
                {
                    idwm.Value = Session["Idwm"].ToString();
                }
                else
                {
                    idwm.Value = "1";
                    Session["Idwm"] = idwm.Value;
                }
                if (value == null)
                {

                }
                else
                {
                    userid.Value = value;
                }
                if (pzoAccess != null)
                {
                    pzoAcc.Value = Session["PZO5"].ToString();
                }

                if (isValid)
                {
                    DataSet ds = mapper.GetEstateDetail(mapped_estate.Value);
                    DataTable dt = ds.Tables[0];
                    DataTable dtUTMProjection = ds.Tables[1];
                    DataTable dtWeek = new DataTable();
                    DataTable dtListWeek = mapper.GetListWeekNew();
                    DataTable dtwmarea = mapper2.getAllWMArea();
                    
                    utmprojection.Value = dtUTMProjection.Rows[0]["UTMProjection"].ToString();
                    rsid.Value = dt.Rows[0]["Projection"].ToString();
                    extent.Value = dt.Rows[0]["CoordinateExtent"].ToString();
                    numzoom.Value = dt.Rows[0]["NumZoom"].ToString();
                    legend.Value = "";
                    estatezone.Value = mapper.GetZone(mapped_estate.Value);

                    listyear.Value = ObjectToJSON(dtListWeek.DefaultView.ToTable(true, "Year"));
                    listmonth.Value = ObjectToJSON(dtListWeek.DefaultView.ToTable(true, "Year", "Month", "MonthName"));
                    listweek.Value = ObjectToJSON(dtListWeek);
                    listAllWMArea.Value = ObjectToJSON(dtwmarea);

                    if (Request.Params["IDWEEK"] == null || Request.Params["IDWEEK"] == "")
                    {
                        dtWeek = mapper.GetWeekFromCurrentDate();
                    }
                    else
                    {
                        dtWeek = mapper.GetWeekFromParameter(Request.Params["IDWEEK"]);
                    }

                    year.Value = dtWeek.Rows[0]["Year"].ToString();
                    month.Value = dtWeek.Rows[0]["Month"].ToString();
                    monthname.Value = dtWeek.Rows[0]["MonthName"].ToString();
                    week.Value = dtWeek.Rows[0]["Week"].ToString();
                    monthnamealias.Value = dtWeek.Rows[0]["MonthName"].ToString();

                    idweek.Value = dtWeek.Rows[0]["ID"].ToString();
                    currentextent.Value = Request.Params["Extent"] != null ? Request.Params["Extent"] : "";
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + "Incorrect Parameters" + "')", true);
                }
                if(Request.Params["act"]== "LogOut")
                {
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx");
                }
                
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + "No Data" + " (" + ex.Message + ")" + "')", true);
            }
        }

        private string ObjectToJSON(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }
    }
}