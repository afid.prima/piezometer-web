﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PZO_MapAnalysis.aspx.cs" Inherits="PZO_PiezometerOnlineMap.Content.PZO_MapAnalysis" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Map | Piezometer</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Bootstrap Select -->
    <link rel="stylesheet" href="../Source/adminlte/bootstrap-select.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.dataTables.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../Source/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="../Style/Loading.css" rel="stylesheet" />
    <%--<link rel="stylesheet" href="../Source/adminlte/plugins/iCheck/all.css">--%>
    <link rel="stylesheet" href="../Source/adminlte/dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/morris.js/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../Source/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/select2/dist/css/select2.min.css">
    <!-- Openlayers 4 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/openlayers4/ol.css">
    <link rel="stylesheet" href="../Style/Map.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green-light sidebar-mini fixed" style='overflow-y: hidden; overflow-x: hidden;'>


    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="#" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>PZO</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Piezometer</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-static-top">
                    <%--<ul class="nav navbar-nav">
                        <li>
                            <a href="#">
                                <img src="../Image/toolbar/icon_fullscreen2.png" width='18px' /></a>
                        </li>
                        <li>
                            <a href="#">
                                <img src='../Image/toolbar/icon_overview2.png' width='18px' /></a>
                        </li>
                        <li>
                            <a href="#">
                                <img src='../Image/toolbar/icon_drawing.png' width='18px' /></a>
                        </li>
                        <li>
                            <a href="#">
                                <img src='../Image/toolbar/icon_measure.png' width='18px' /></a>
                        </li>
                        <li>
                            <a href="#">
                                <img src='../Image/toolbar/icon_legend.png' width='18px' /></a>
                        </li>
                        <li>
                            <a href="#">
                                <img src='../Image/toolbar/icon_fullextent.png' width='18px' /></a>
                        </li>
                    </ul>--%>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Control Sidebar Toggle Button -->
                            <li>
                                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <span style="margin-left: 50px;"></span>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="#" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModalModule">
                                    <i class="fa fa-map-marker"></i>
                                    <span style="margin-left: 10px;"><span id="titleEstate"></span></span>
                                </a>
                        </ul>
                        <ul class="nav navbar-nav">
                            <span style="margin-left: 15px;"></span>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li>
                                <a id="toolsprevdate" href="#">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="hidden-xs"><span id="lbldate"></span></span>
                                </a>
                            </li>
                            <li>
                                <a id="toolsnextdate" href="#">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    <li>
                        <a href="PZO_Dashboard.aspx">
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="treeview active">
                        <a href="#">
                            <i class="fa fa-map"></i>
                            <span>Online Map</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a id="map_default" href="PZO_Map.aspx"><i class="fa fa-circle-o"></i>Default Map</a></li>
                            <li><a id="map_analysis" href="PZO_MapAnalysis.aspx"><i class="fa fa-circle-o"></i>Analysis Map</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="PZO_DownloadData.aspx">
                            <i class="fa fa-files-o"></i>
                            <span>Download Data</span>
                        </a>
                    </li>
                    <li class="treeview active">
                        <a href="#">
                            <i class="fa fa-briefcase"></i><span>Toolbox</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a id="toolFullscreen" href="#"><i class="fa fa-expand"></i>Fullscreen</a></li>
                            <li><a id="toolOverview" href="#"><i class="fa fa-tablet"></i>Overview</a></li>
                            <li><a id="toolDrawing" href="#"><i class="fa fa-pencil"></i>Drawing</a></li>
                            <li><a id="toolMeasure" href="#"><i class="fa fa-cut"></i>Measure</a></li>
                            <li><a id="toolLegend" href="#"><i class="fa fa-list-alt"></i>Legend</a></li>
                            <li><a id="toolFullExtent" href="#"><i class="fa fa-globe"></i>Full Extent</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="PZO_Map.aspx?act=LogOut">
                            <i class="fa fa-sign-out"></i>
                            <span id="txtLogout" runat="server">Keluar</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <%-- <!-- Content Header (Page header) -->
            <section class="content-header">
            </section>--%>

            <!-- Main content -->
            <section class="content">
                <div id="map"></div>
                <div id="scale-line" class="scale-line"></div>
                <div id="mouse-position" class="mouse-position"></div>
            </section>
            <!-- /.content -->
            <%--<section class="content">
                <asp:PlaceHolder ID="phjob" runat="server"></asp:PlaceHolder>
                <!-- /.row -->
            </section>--%>

        </div>
        <!-- /.content-wrapper -->
        <!--<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>-->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark control-sidebar-open">
            <!-- Create the tabs -->
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                <li class="active"><a href="#control-sidebar-layer-tab" data-toggle="tab"><i class="fa fa-clone"></i></a></li>
                <li><a href="#control-sidebar-waterway-tab" data-toggle="tab"><i class="fa fa-tint"></i></a></li>
                <li><a href="#control-sidebar-filter-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- Layer tab content -->
                <div class="tab-pane active" id="control-sidebar-layer-tab">
                    <button type="button" class="btn btn-primary2 btnslideshow" data-toggle="collapse" data-target="#divlayer1" onclick="Layer1Click()" id="btnlayer1">Base Map</button>
                    <div id="divlayer1" class="collapse in">
                        <table style="width: 100%; font-size: 13px;" border="0">
                            <tr id="trnonthip">
                                <td style="width: 20px; padding-top: 10px;">
                                    <input type="checkbox" id="chkbasemap2" checked="checked" />
                                    <label class="labelstyle" for="chkbasemap2" style="top: -10px; position: relative; cursor: pointer;"></label>
                                </td>
                                <td style="padding-top: 10px;">
                                    <select id="selectbasemap2" style="width: 100%;"></select>
                                </td>
                                <td align="right" style="padding-top: 10px;">
                                    <div id="sliderbasemap2" style="width: 40px;"></div>
                                </td>
                            </tr>
                            <tr id="trnonthip2">
                                <td style="width: 20px; padding-top: 10px;">
                                    <input type="checkbox" id="chkbasemap22" checked="checked" />
                                    <label class="labelstyle" for="chkbasemap22" style="top: -10px; position: relative; cursor: pointer;"></label>
                                </td>
                                <td style="padding-top: 10px;">
                                    <select id="selectbasemap22" style="width: 100%;"></select>
                                </td>
                                <td align="right" style="padding-top: 10px;">
                                    <div id="sliderbasemap22" style="width: 40px;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20px; padding-top: 10px;">
                                    <input type="checkbox" id="chkbasemap" checked="checked" />
                                    <label class="labelstyle" for="chkbasemap" style="top: -10px; position: relative; cursor: pointer;"></label>
                                </td>
                                <td style="padding-top: 10px;">
                                    <select id="selectbasemap" style="width: 100%;"></select></td>
                                <td align="right" style="padding-top: 10px;">
                                    <div id="sliderbasemap" style="width: 40px;"></div>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </div>

                    <button type="button" class="btn btn-primary2 btnslideshow" data-toggle="collapse" data-target="#divlayer2" onclick="Layer2Click()" id="btnlayer2">Layer</button>
                    <div id="divlayer2" class="collapse in">
                        <table style="width: 100%; font-size: 13px;" border="0">
                            <tr>
                                <td style="padding-top: 10px;" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="color: white;">Standard Layer</td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chkjalan" />
                                    <label class="labelstyle" for="chkjalan" style="top: 4px; position: relative; cursor: pointer;">Jalan</label>
                                </td>
                                <td align="right">
                                    <div id="sliderjalan" style="width: 40px;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chkboundary" />
                                    <label class="labelstyle" for="chkboundary" style="top: 4px; position: relative; cursor: pointer;">Boundary</label>
                                </td>
                                <td align="right">
                                    <div id="sliderboundary" style="width: 40px;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chkafdeling" />
                                    <label class="labelstyle" for="chkafdeling" style="top: 4px; position: relative; cursor: pointer;">Afdeling</label>
                                </td>
                                <td align="right">
                                    <div id="sliderafdeling" style="width: 40px;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chkblock" />
                                    <label class="labelstyle" for="chkblock" style="top: 4px; position: relative; cursor: pointer;">Block</label>
                                </td>
                                <td align="right">
                                    <div id="sliderblock" style="width: 40px;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chklanduse" />
                                    <label class="labelstyle" for="chklanduse" style="top: 4px; position: relative; cursor: pointer;">Landuse</label>
                                </td>
                                <td align="right">
                                    <div id="sliderlanduse" style="width: 40px;"></div>
                                </td>
                            </tr>


                            <tr>
                                <td colspan="3" style="color: white;">Palm Layer</td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chkdeadpalm" />
                                    <label class="labelstyle" for="chkdeadpalm" style="top: 4px; position: relative; cursor: pointer;">Dead Palm</label>
                                </td>
                                <td align="right">
                                    <div id="sliderdeadpalm" style="width: 40px;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chkoilpalm" />
                                    <label class="labelstyle" for="chkoilpalm" style="top: 4px; position: relative; cursor: pointer;">Oil Palm</label>
                                </td>
                                <td align="right">
                                    <div id="slideroilpalm" style="width: 40px;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="color: white;">Label</td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chklabelboundary" />
                                    <label class="labelstyle" for="chklabelboundary" style="top: 4px; position: relative; cursor: pointer;">Boundary</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chklabelafdeling" />
                                    <label class="labelstyle" for="chklabelafdeling" style="top: 4px; position: relative; cursor: pointer;">Afdeling</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="checkbox" id="chklabelblock" />
                                    <label class="labelstyle" for="chklabelblock" style="top: 4px; position: relative; cursor: pointer;">Block</label>
                                </td>
                            </tr>


                        </table>
                        <br />
                    </div>
                </div>
                <!-- Filter tab content -->
                <div class="tab-pane" id="control-sidebar-waterway-tab">
                    <button type="button" class="btn btn-primary2 btnslideshow" data-toggle="collapse" data-target="#divlayer3" onclick="Layer4Click()" id="btnlayer4">Layer</button>
                    <div id="divlayer3" class="collapse in">
                        <table style="width: 100%; font-size: 13px;" border="0">
                            <tr>
                                <td style="padding-top: 10px;" colspan="2"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input type="checkbox" id="chkwaterlabel" />
                                    <label class="labelstyle" for="chkwaterlabel" style="top: 4px; position: relative; cursor: pointer;">Label Parit</label>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="center"></td>
                                <td>
                                    <input type="checkbox" id="chkwater" />
                                    <label class="labelstyle" for="chkwater" style="top: 4px; position: relative; cursor: pointer;">Parit</label>
                                </td>
                                <td align="right">
                                    <div id="sliderwater" style="width: 40px;"></div>
                                </td>
                            </tr>
                        </table>
                        <div id="divtableparit"></div>
                        <br />
                    </div>

                    <button type="button" class="btn btn-primary2 btnslideshow" data-toggle="collapse" data-target="#divlayer4" onclick="Layer5Click()" id="btnlayer5">Info</button>
                    <div id="divlayer4" class="collapse in">
                        <br />
                        <div id="divtableinfo"></div>
                        <br />
                    </div>
                </div>
                <div class="tab-pane" id="control-sidebar-filter-tab">
                    <button type="button" class="btn btn-primary2 btnslideshow" data-toggle="collapse" data-target="#divlayer5" onclick="Layer5Click()" id="btnlayer5">Zona Analysis</button>
                    <div id="divlayer5" class="collapse in">
                        <table style="width: 100%; font-size: 13px;" border="0">
                            <tr>
                                <td style="padding-top: 10px;" colspan="2"></td>
                            </tr>
                            <tr>
                                <td>
                                    <select id="selectFilterZonaSelected" class="form-control selectpicker" data-live-search="true"></select>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top: 5px;" colspan="2"></td>
                            </tr>
                            <tr>
                                <td>
                                    <button type="button" id="btnPdf" class="btn btn-block btn-success">PDF</button>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </div>

                    <button type="button" class="btn btn-primary2 btnslideshow" data-toggle="collapse" data-target="#divlayer6" onclick="Layer5Click()" id="btnlayer6">Setting Zona Analysis</button>
                    <div id="divlayer6" class="collapse in">
                        <table style="width: 100%; font-size: 13px;" border="0">
                            <tr>
                                <td style="padding-top: 10px;" colspan="2"></td>
                            </tr>
                            <tr>
                                <td style="color: white;">Wm Area</td>
                            </tr>
                            <tr>
                                <td style="padding-top: 5px;" colspan="2">
                                    <select id="selectFilterWMArea" class="form-control selectpicker" data-live-search="true"></select>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: white; padding-top: 5px;" colspan="2">Zona Pemantauan TMAS</td>
                            </tr>
                            <tr>
                                <td style="padding-top: 5px;" colspan="2">
                                    <select id="selectFilterZona" class="form-control selectpicker" data-live-search="true"></select>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: white; padding-top: 5px;" colspan="2">Piezometer Pilihan</td>
                            </tr>
                            <tr>
                                <td style="padding-top: 5px;" colspan="2">
                                    <select id="selectFilterPzo" class="form-control selectpicker" data-live-search="true" data-actions-box="true" multiple data-selected-text-format="count"></select>
                                </td>
                            </tr>

                            <tr>
                                <td style="color: white; padding-top: 5px;" colspan="2">AWL - TMAT</td>
                            </tr>
                            <tr>
                                <td style="padding-top: 5px;" colspan="2">
                                    <select id="selectFilterAWL" class="form-control selectpicker" data-live-search="true"></select>
                                </td>
                            </tr>


                            <tr>
                                <td style="color: white; padding-top: 5px;" colspan="2">Nama Zona Analysis</td>
                            </tr>
                            <tr>
                                <td style="padding-top: 5px;" colspan="2">
                                    <input type="text" class="form-control" id="namaZona" />
                                </td>
                            </tr>


                            <tr>
                                <td style="color: white; padding-top: 5px;padding-right: 5px;width:50%;">
                                    <button type="button" id="btnSave" class="btn btn-block btn-success">Save</button></td>
                                <td style="color: white; padding-top: 5px;padding-left: 5px;width:50%;">
                                    <button type="button" id="btnDeleted" class="btn btn-block btn-danger">Deleted</button></td>
                            </tr>

                            <tr>
                                <td style="padding-top: 5px;" colspan="2">
                                    <button type="button" id="btnZoom" class="btn btn-block btn-info">Zoom To Zona</button></td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- Modal -->
    <div class="modal fade" id="myToolbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm modal-side modal-top-left" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" id="btnCloseMyToolbox" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myToolboxTitle"></h4>
                </div>
                <div class="modal-body">
                    <div id="myDrawingToolbox">
                        <select id="selectDrawingMode" class="selectpicker" data-width="100px">
                            <option value="Point">Point</option>
                            <option value="LineString">Polyline</option>
                            <option value="Polygon">Polygon</option>
                        </select>
                        <button type="button" id="btnStartDrawing" class="btn btn-success">Start</button>
                        <button type="button" id="btnStopDrawing" class="btn btn-danger" style="display: none;">Stop</button>
                        <button type="button" id="btnClearDrawing" class="btn btn-success">Clear</button>
                    </div>
                    <div id="myMeasureToolbox">
                        <select id="selectMeasureMode" class="selectpicker" data-width="150px">
                            <option value="LineString">Length (LineString)</option>
                            <option value="Polygon">Area (Polygon)</option>
                        </select>
                        <button type="button" id="btnStartMeasure" class="btn btn-success">Start</button>
                        <button type="button" id="btnStopMeasure" class="btn btn-danger" style="display: none;">Stop</button>
                        <button type="button" id="btnClearMeasure" class="btn btn-success">Clear</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myMapLegend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" id="btnCloseMyMapLegend" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myMapLegendTitle">Map Legend</h4>
                </div>
                <div class="modal-body">
                    <img src="http://map.gis-div.com:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=pub_pzo:pzo_block_week_unitpemantauan" alt="img" id="legendlayer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialogdetail" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" id="btnCloseMyDetail" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myDetailTitle">Detail</h4>
                </div>
                <div class="modal-body">
                    <div id="divloadingdetail" style="display: none; text-align: center;">
                        <img alt="" src="../Image/loading-ripple.gif" width="50" />
                    </div>
                    <div id="divcontentdetail">
                        <div class="row">
                            <div id="divmasterdetail"></div>
                        </div>
                        <div class="row">
                            <ul class="nav nav-tabs">
                                <li role="presentation"><a data-toggle="tab" href="#table">Table</a></li>
                                <li role="presentation" class="active"><a data-toggle="tab" href="#chart">Chart</a></li>
                            </ul>
                            <div class="tab-content" style='overflow-y: scroll; overflow-x: hidden; height: 470px;'>
                                <div id="table" class="tab-pane fade">
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="divtablehistory"></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="chart" class="tab-pane fade in active">
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="divcharthistory" style="width: 100%; height: 400px"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="btn-group" data-toggle="buttons">
                                                <label id="allData" class="btn btn-success active">
                                                    <input type="radio" name="allData"><span>All</span></label>
                                                <label id="thn" class="btn btn-success">
                                                    <input type="radio" name="thn"><span>1 Tahun</span></label>
                                                <label id="6bln" class="btn btn-success">
                                                    <input type="radio" name="6bln"><span>6 Bulan</span></label>
                                                <label id="3bln" class="btn btn-success">
                                                    <input type="radio" name="3bln"><span>3 Bulan</span></label>
                                                <label id="6min" class="btn btn-success">
                                                    <input type="radio" name="6min"><span>6 Minggu</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="divinfo"></div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade" id="exampleModalModule" tabindex="-1" role="dialog" aria-labelledby="dialog_confirm_mapLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Pilih PT yang akan muncul</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-outline mb-4">
                        <label style="font-size: 15px;">Company</label>
                        <select id="selectfiltercompany" class="form-control selectpicker" data-live-search="true"></select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success pull-right" id="btnGo">OK</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div id="divBackground" class="modalpreviewimage">
    </div>
    <div id="divImage">
        <table style="height: 100%; width: 100%">
            <tr>
                <td valign="middle" align="center">
                    <img id="imgLoader" alt="" src="../../images/loading.gif" />
                    <img id="imgFull" alt="" src="" style="display: none; height: 500px; width: 590px" />
                </td>
            </tr>
            <tr>
                <td align="center" valign="bottom">
                    <input id="btnClose" type="button" value="close" onclick="HidePreview()" />
                </td>
            </tr>
        </table>
    </div>
    <div class="loading" style="z-index: 99999999;"></div>
    <form id="Form1" runat="server">
        <asp:HiddenField ID="idweek" runat="server" />
        <asp:HiddenField ID="year" runat="server" />
        <asp:HiddenField ID="month" runat="server" />
        <asp:HiddenField ID="monthname" runat="server" />
        <asp:HiddenField ID="week" runat="server" />
        <asp:HiddenField ID="monthnamealias" runat="server" />
        <asp:HiddenField ID="userid" runat="server" />
        <asp:HiddenField ID="listyear" runat="server" />
        <asp:HiddenField ID="listmonth" runat="server" />
        <asp:HiddenField ID="listweek" runat="server" />
        <asp:HiddenField ID="extentCoordinate" runat="server" />
    </form>

    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../Source/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../Source/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Bootstrap Select -->
    <script src="../Source/adminlte/bootstrap-select.js"></script>
    <!-- DataTables -->
    <script src="../Source/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/dataTables.buttons.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.flash.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/jszip.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/pdfmake.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/vfs_fonts.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.html5.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.print.min.js"></script>
    <%--   <script src="../Source/DataTable/js/dataTables.bootstrap4.min.js"></script>
    <script src="../Source/DataTable/js/dataTables.fixedHeader.js"></script>--%>
    <script src="../Script/dataTables.fixedHeader.min.js"></script>
    <script src="../Script/dataTables.bootstrap4.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <%--<script src="../Source/adminlte/plugins/iCheck/icheck.min.js"></script>--%>
    <!-- daterangepicker -->
    <script src="../Source/adminlte/bower_components/moment/min/moment.min.js"></script>
    <script src="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../Source/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../Source/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../Source/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../Source/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../Source/adminlte/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../Source/adminlte/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../Source/adminlte/dist/js/demo.js"></script>
    <!-- Openlayers 4 -->
    <script src="../Source/adminlte/bower_components/openlayers4.6.5/ol.js"></script>
    <script src="../Source/canvasjs/canvasjs.min.js"></script>
    <script src="../Source/xml2json.js"></script>
    <script src="../Source/underscore/underscore.js"></script>
    <!-- Piezometer -->
    <script src="<%= scriptPiezometerAnalisis %>"></script>
    <script src="<%= scriptFunctionTambahan %>"></script>
    <script src="<%= scriptFunction %>"></script>
    <script src="<%= scriptMap_LayerBase %>"></script>
    <script src="<%= scriptMap_SideBar %>"></script>
    <script src="<%= scriptMap_Waterway %>"></script>
    <script src="<%= scriptMap_PiezometerAnalysis %>"></script>
    <!-- Reqwest -->
    <script src="../Source/Reqwest/reqwest.js" type="text/javascript"></script>

</body>
</html>
