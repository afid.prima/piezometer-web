﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PZO_MasterPiezo.aspx.cs" Inherits="PZO_PiezometerOnlineMap.Content.PZO_MasterPiezo" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Online Map | Piezometer</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Bootstrap Select -->
    <link rel="stylesheet" href="../Source/adminlte/bootstrap-select.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.dataTables.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../Source/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="../Source/adminlte/plugins/iCheck/all.css">
    <link rel="stylesheet" href="../Source/adminlte/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="../Source/css/jquery-confirm.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="../Style/Loading.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Source/DataTable/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.3/css/fixedHeader.bootstrap4.min.css" />

    <style>
        .table-text-center th, .table-text-center td {
            text-align: center;
        }
    </style>
    <style>
        .dataTables_wrapper {
            overflow-x: auto;
        }
    </style>
</head>
<body class="hold-transition skin-green-light sidebar-mini fixed">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="#" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>PZO</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Piezometer</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    <li>
                        <a href="PZO_Dashboard.aspx">
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <%--<li>
                        <a href="PZO_Map.aspx">
                            <i class="fa fa-map"></i>
                            <span>Online Map</span>
                        </a>
                    </li>--%>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-map"></i><span>Online Map</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a id="map_default" href="PZO_Map.aspx"><i class="fa fa-circle-o"></i>Default Map</a></li>
                            <li><a id="map_analysis" href="PZO_MapAnalysis.aspx"><i class="fa fa-circle-o"></i>Analysis Map</a></li>
                            <%--<li><a id="olmTMAS" href="PZO_NewAnalysisTMAS.aspx"><i class="fa fa-circle-o"></i>Analysis Map for TMAS</a></li>--%>
                        </ul>
                    </li>
                    <li>
                        <a href="PZO_DownloadData.aspx">
                            <i class="fa fa-files-o"></i>
                            <span>Download Data</span>
                        </a>
                    </li>
                    <%-- <li>
                        <a href="PZO_UploadData.aspx">
                            <i class="fa fa-files-o"></i>
                            <span>Upload Data</span>
                        </a>
                    </li>--%>
                    <li class="treeview open menu-open active">
                        <a href="#">
                            <i class="fa fa-gear"></i><span>Maintenance Data</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu active">
                            <li><a id="master_hobo" href="PZO_MasterPiezo.aspx"><i class="fa fa-circle-o"></i>Master Piezometer</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="?act=LogOut">
                            <i class="fa fa-sign-out"></i>
                            <span id="txtLogout" runat="server">Keluar</span>
                        </a>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!--<section class="content-header">
            </section>-->
            <section class="content-header">
                <h1>Master Piezo</h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="nav-tabs-custom">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="nav-tabs-custom  tab-success" id="tabCountry">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#masterPiezo" data-toggle="tab" runat="server">Master Piezometer</a></li>
                                        <li><a href="#summaryPiezo" data-toggle="tab" runat="server">Summary Piezometer</a></li>
                                        <li class="pull-right">
                                            <select id="selectFiltering" class="form-control selectpicker" data-live-search="true">
                                                <option selected="selected" value="1">All</option>
                                                <option value="2">KLHK</option>
                                            </select></li>
                                        <li class="pull-right">
                                            <label style="width: 50px; margin-left: 10px;">Type</label>
                                        </li>
                                        <li class="pull-right">
                                            <select id="selectFilteringEstate" class="form-control selectpicker" data-live-search="true"></select>
                                        </li>
                                        <li class="pull-right">
                                            <label style="width: 50px; margin-left: 10px;">Kebun</label>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="chart tab-pane active" id="masterPiezo">
                                            <div style="overflow: auto;">
                                                <font size="1">
                                                    <table id="tableMasterPiezo" class="table table-striped table-bordered table-text-center" style="width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <td>No</td>
                                                                <td>Block Pemantauaan</td>
                                                                <td>Estate</td>
                                                                <td>Afdeling</td>
                                                                <td>Block</td>
                                                                <td>Jenis</td>
                                                                <td>PiezoStation ID</td>
                                                                <td>PiezoStation ID Since</td>
                                                                <td>Block Active Since</td>
                                                                <td>Distance Form SK,m</td>
                                                                <td>Device Id</td>
                                                                <%--<td>Status History</td>--%>
                                                                <td>Status</td>
                                                                <td>Unit Pemantauaan</td>
                                                                <td>TMAT IOT</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </font>
                                            </div>
                                        </div>
                                        <div class="chart tab-pane" id="summaryPiezo">
                                            <div style="overflow: auto;">
                                                <table class="table table-striped table-bordered table-text-center" style="width: 100%">
                                                    <thead>
                                                        <tr>
                                                            <td></td>
                                                            <td>∑ UP Unit Pemantauan</td>
                                                            <td>∑ UP Unregister</td>
                                                            <td>∑ UP Non-Aktif</td>
                                                            <td>∑ UP Aktif</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Titik Penetepan KLH-Loger</td>
                                                            <td id="lblUnitpemantauanKLHLoger"></td>
                                                            <td id="lblKLHlogerUnregistrasi"></td>
                                                            <td id="lblKLHlogerNonAktif"></td>
                                                            <td id="lblKLHlogerAktif"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Titik Penaatan KLH-Manual</td>
                                                            <td id="lblKlhManualUnitPemantauan"></td>
                                                            <td id="lblKlhManualUnreg"></td>
                                                            <td id="lblKlhManualNonAktif"></td>
                                                            <td id="lblKlhManualAktif"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Piezometer Kebun-Manual</td>
                                                            <td id="lblKebunUnitPemantauan"></td>
                                                            <td id="lblKebunUnreg"></td>
                                                            <td id="lblKebunNonAktif"></td>
                                                            <td id="lblKebunAktif"></td>
                                                        </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td>Total</td>
                                                            <td id="lbltotalUnitpemantauan"></td>
                                                            <td id="lbltotalUnreg"></td>
                                                            <td id="lbltotalNonAktif"></td>
                                                            <td id="lbltotalAktif"></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                        </div>
                    </div>
                </div>
            </section>

            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
    <div class="loading" style="display: none; z-index: 99999999;"></div>

    <div class="modal fade" data-backdrop="static" id="mdlCheckData" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Data Unit Pemantauan</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ID Pemantauan</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="idPemantauaan" disabled="disabled">
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Block</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="block" disabled="disabled">
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Alias</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="alias">
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Jenis Piezometer</label>
                                    <div class="col-sm-9">
                                        <select id="jenisPiezo" class="form-control selectpicker" data-live-search="true">
                                            <option value="0">--Select---</option>
                                            <option value="1">KLH-Manual</option>
                                            <option value="2">KLH-Logger</option>
                                            <option value="3">Kebun-Manual</option>
                                            <option value="4">Kebun-Logger</option>
                                        </select>
                                        <input class="form-control" id="jenisPiezoPrev">
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Is Active</label>
                                    <div class="col-sm-9">
                                        <input type="checkbox" class="minimal" id="isActive">
                                        <input class="form-control" id="isActivePrev">
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Keterangan</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="keterangn">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="tableMdlBlock" class="table table-striped table-bordered table-text-center display nowrap" style="width: 100%">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Block</td>
                                    <td>Is Default</td>
                                    <td>Is Active</td>
                                    <td>Status</td>
                                    <td>Lepas</td>
                                    <td>idMappingBlockUnitPemantauan</td>
                                    <td>Is Active2</td>
                                </tr>
                            </thead>
                        </table>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right" id="btnMdlSave">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" data-backdrop="static" id="mdlCheckDataIOT" style="overflow-y: scroll;" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">TMAT IOT</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label" style="font-size: 13px;">Piezometer Id</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" id="midPzo" disabled="disabled">
                                    </div>
                                    <label class="col-sm-1 control-label" style="font-size: 13px;">Block</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" id="midBlockPemantauan" disabled="disabled">
                                    </div>
                                    <label class="col-sm-1 control-label" style="font-size: 13px;">Jenis</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" id="midJenis" disabled="disabled">
                                    </div>
                                    <label class="col-sm-1 control-label" style="font-size: 13px;">Status</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" id="midStatus" disabled="disabled">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div style="overflow-y: scroll;">
                                <font size="1" face="Arial">
                                    <table id="tableIOT" class="table table-striped table-bordered table-text-center display nowrap" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <td>idx</td>
                                                <td>Type</td>
                                                <td>Device</td>
                                                <td>Start</td>
                                                <td>End</td>
                                                <td>Kalibrasi</td>
                                                <td><a href="#" id="btnTambah"><i class="fa fa-fw fa-plus"></i>Tambah Conncetion</a></td>
                                            </tr>
                                        </thead>
                                    </table>
                                </font>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-9"></div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <button type="button" id="btnPrevWeek" class="btn btn-success btn-sm prevnext"><i class="fa fa-angle-left"></i></button>
                                            <label style="margin-left: 10px;">Date Range</label>
                                            <button style="margin-left: 10px;" type="button" id="btnNextWeek" class="btn btn-success btn-sm prevnext"><i class="fa fa-angle-right"></i></button>
                                            <button style="margin-left: 10px;" type="button" id="btnThis" class="btn btn-success btn-sm prevnext">This Day</button>
                                            <div class="input-group" style="width: 65.5%;">
                                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                <input type="text" class="form-control pull-right" id="mTglSelected" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <%--<label class="col-sm-1 control-label" style="font-size: 13px;">Date Range</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control pull-right" id="mTglSelected" readonly>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id='mDivChart' style='height: 400px; width: 100%; margin: 0px auto;'></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right" id="btnMdlSaveIOT">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" data-backdrop="static" id="mdlCalibrationReference" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Calibration Reference</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group" style="margin-bottom: 0%;">
                                    <div class="col-md-4">
                                        <label class="control-label" style="font-size: 13px;">Piezometer Id</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label" style="font-size: 13px;">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label" style="font-size: 13px;" id="midPzo1">PUL-10</label>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 0%;">
                                    <div class="col-md-4">
                                        <label class="control-label" style="font-size: 13px;">Kebun</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label" style="font-size: 13px;">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label" style="font-size: 13px;" id="midEst1">THP10/PUL</label>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 0%;">
                                    <div class="col-md-4">
                                        <label class="control-label" style="font-size: 13px;">Block</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label" style="font-size: 13px;">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label" style="font-size: 13px;" id="midBlockPemantauan1">23-16</label>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 0%;">
                                    <div class="col-md-4">
                                        <label class="control-label" style="font-size: 13px;">Jenis</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label" style="font-size: 13px;">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label" style="font-size: 13px;" id="midJenis1">KLH-Manual</label>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 0%;">
                                    <div class="col-md-4">
                                        <label class="control-label" style="font-size: 13px;">Status</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label" style="font-size: 13px;">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label" style="font-size: 13px;" id="midStatus1">Aktif</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-size: 15px;"><u>Piezometer</u></label>
                            <div class="form-horizontal">
                                <div class="form-group" style="margin-bottom: 0%;">
                                    <div class="col-md-4">
                                        <label class="control-label" style="font-size: 13px;">Tanggal</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label" style="font-size: 13px;">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label" style="font-size: 13px;" id="midTgl1">12/07/2024, 10:21:11</label>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 0%;">
                                    <div class="col-md-4">
                                        <label class="control-label" style="font-size: 13px;">TMAT</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label" style="font-size: 13px;">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label" style="font-size: 13px;" id="midTmat1">54cm</label>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 0%;">
                                    <div class="col-md-4" style="visibility: hidden">
                                        <label class="control-label" style="font-size: 13px;">TMAT</label>
                                    </div>
                                    <div class="col-md-1" style="visibility: hidden">
                                        <label class="control-label" style="font-size: 13px;">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="img"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-size: 15px;"><u>AWL TMAT</u></label>
                            <div class="form-horizontal">
                                <div class="form-group" style="margin-bottom: 0%;">
                                    <div class="col-md-4">
                                        <label class="control-label" style="font-size: 13px;">ID Perangkat</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label" style="font-size: 13px;">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label" style="font-size: 13px;" id="midIdperangkat1"></label>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 0%;">
                                    <div class="col-md-4">
                                        <label class="control-label" style="font-size: 13px;">Nama Station</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label" style="font-size: 13px;">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label" style="font-size: 13px;" id="midStation1"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12" id="tblCalibrate">
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group" style="margin-bottom: 0%;">
                                    <div class="col-md-4">
                                        <label class="control-label" style="font-size: 13px;">Selisih (Kalibrasi Baru)</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label" style="font-size: 13px;">:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label" style="font-size: 13px;" id="midSelisih1">5.59</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success pull-right" id="setNewCalibrate" onclick="setNewCalibrate()" style="margin-left: 2%;">
                                Set New Calibrate</button>
                            <button type="button" class="btn pull-right" data-dismiss="modal">Close</button>
                        </div>
                    </div>


                    <%-- <div class="row">
                        <div class="col-md-12">
                            <div style="overflow-y: scroll;">
                                <font size="1" face="Arial">
                                <table id="tableIOT" class="table table-striped table-bordered table-text-center display nowrap" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <td>idx</td>
                                            <td>Type</td>
                                            <td>Device</td>
                                            <td>Start</td>
                                            <td>End</td>
                                            <td>Kalibrasi</td>
                                            <td><a href="#" id="btnTambah"><i class="fa fa-fw fa-plus"></i>Tambah Conncetion</a></td>
                                        </tr>
                                    </thead>
                                </table>
                                </font>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-9"></div>
                                    <label class="col-sm-1 control-label" style="font-size: 13px;">Date Range</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control pull-right" id="mTglSelected" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id='mDivChart' style='height: 400px; width: 100%; margin: 0px auto;'></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right" id="btnMdlSaveIOT">Save</button>
                        </div>
                    </div>--%>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <form id="Form1" runat="server">
        <asp:HiddenField ID="userid" runat="server" />
    </form>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../Source/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- jQuery 3 -->
    <script src="../Source/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../Source/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Bootstrap Select -->
    <script src="../Source/adminlte/bootstrap-select.js"></script>
    <!-- DataTables -->
    <script src="../Source/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/dataTables.buttons.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.flash.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/jszip.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/pdfmake.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/vfs_fonts.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.html5.min.js"></script>
    <script src="../Source/adminlte/bower_components/datatables.net-1.10.16/datatables-button-1.5.1/buttons.print.min.js"></script>
    <%--  <script src="../Source/DataTable/js/dataTables.bootstrap4.min.js"></script>
    <script src="../Source/DataTable/js/dataTables.fixedHeader.js"></script>--%>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.3/js/dataTables.fixedHeader.min.js"></script>

    <!-- daterangepicker -->
    <script src="../Source/adminlte/bower_components/moment/min/moment.min.js"></script>
    <script src="../Source/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../Source/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../Source/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../Source/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../Source/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../Source/adminlte/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../Source/adminlte/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../Source/adminlte/dist/js/demo.js"></script>
    <!-- This Page Main JS -->
    <script src="../Source/adminlte/plugins/iCheck/icheck.min.js"></script>
    <script src="../Source/js/jquery-confirm.js"></script>
    <script src="../Source/canvasjs-commercial-3.6.6/canvasjs.min.js"></script>
    <script src="../Source/underscore/underscore.js"></script>
    <script src="<%= masterPiezo_js %>"></script>
    <script src="<%= graphMasterPiezoTMAT_js %>"></script>
    <script src="<%= formTMATIOT_js %>"></script>
    <script>
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
    </script>
</body>
</html>
