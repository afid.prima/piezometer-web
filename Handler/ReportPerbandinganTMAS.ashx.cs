﻿using DevExpress.XtraCharts;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using PZO_PiezometerOnlineMap.Mapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace PZO_PiezometerOnlineMap.Handler
{
    /// <summary>
    /// Summary description for ReportPerbandinganTMAS
    /// </summary>
    public class ReportPerbandinganTMAS : IHttpHandler
    {
        PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();

        public void ProcessRequest(HttpContext context)
        {


            var zona = context.Request.Params["zona"];
            var rambu_air = context.Request.Params["rambu_air"];
            int minRangeZone = Convert.ToInt32(context.Request.Params["minRangeZone"]);
            int maxRangeZone = Convert.ToInt32(context.Request.Params["maxRangeZona"]);
            int year = Convert.ToInt32(context.Request.Params["year"]);
            int month = Convert.ToInt32(context.Request.Params["month"]);            
            var week = context.Request.Params["week"];
            string estCode = context.Request.Params["estcode"];
            if (week.Contains(','))
            {
                week = week.Replace(",", "");
            }
            //DataSet ds = mapper.PerbandinganCurahHujan(zona, rambu_air, minRangeZone, maxRangeZone, year, month, week);
            if (!zona.Equals(""))
            {
                if (!year.Equals("") && !month.Equals("") && !week.Equals(""))
                {
                    XtraReport report = XtraReport.FromFile(@"" + context.Server.MapPath("..") + "\\ReportFile\\RPT_LaporanAnalisaPerbandinganTMAT.repx", true);
                    string inputmin = minRangeZone.ToString();
                    string inputmax = maxRangeZone.ToString();

                    if (inputmin == null) inputmin = "0";
                    else if (inputmin.Equals("")) inputmin = "0";
                    if (inputmax == null) inputmax = "0";
                    else if (inputmax.Equals("")) inputmax = "0";

                    float minX = float.Parse(inputmin);
                    float maxX = float.Parse(inputmax);
                    float minPrimaryY = (float)0;
                    float maxPrimaryY = (float)10;
                    DataSet ds = mapper.PerbandinganCurahHujan(zona, rambu_air, minRangeZone, maxRangeZone, year, month, week, estCode);
                    DataTable dtHeader = ds.Tables[2];
                    report.DataSource = ds;
                    report.Parameters["zone"].Value = zona;
                    report.Parameters["pulau"].Value = dtHeader.Rows[0]["pulau"];
                    report.Parameters["periode"].Value = dtHeader.Rows[0]["periode"];
                    report.Parameters["rambu"].Value = dtHeader.Rows[0]["rambu"];

                    DetailReportBand detailReport = report.Bands["DetailReport"] as DetailReportBand;

                    GroupFooterBand footerband = report.Bands["GroupFooter1"] as GroupFooterBand;
                    XRChart chartControl = footerband.Controls["xrChart1"] as XRChart;
                    Series series1 = chartControl.Series["TMAS Normal"] as Series;
                    Series seriesTmas = chartControl.Series["TMAS"] as Series;

                    if (minX == 0 && maxX == 0)
                    {
                        series1.Visible = false;
                    }

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        row["minX"] = minX;
                        row["maxX"] = maxX;
                        if (ds.Tables[0].Rows.IndexOf(row) == 0)
                        {
                            minPrimaryY = float.Parse(row["tmas"].ToString());
                            maxPrimaryY = float.Parse(row["tmas"].ToString());
                        }
                        else
                        {
                            float tmas = float.Parse(row["tmas"].ToString());
                            if (tmas < minPrimaryY)
                            {
                                minPrimaryY = tmas;
                            }
                            if (tmas > maxPrimaryY)
                            {
                                maxPrimaryY = tmas;
                            }
                        }
                    }

                    float range = (float)0.05;
                    int gridalign = 2;
                    XYDiagram diagram = (XYDiagram)chartControl.Diagram;
                    diagram.AxisY.WholeRange.SideMarginsValue = 0;
                    float spacing = (float)0.5;

                    if (maxPrimaryY != 0 && minPrimaryY != 0)
                    {
                        range = (float)0.05;
                        gridalign = 1;
                        diagram.AxisY.NumericScaleOptions.GridSpacing = range;
                        diagram.AxisY.NumericScaleOptions.CustomGridAlignment = gridalign;
                        spacing = (float)0.05;
                    }

                    if (minPrimaryY == 0) minPrimaryY = 0;
                    if (maxPrimaryY == 0) maxPrimaryY = 2;
                    minPrimaryY = minPrimaryY - spacing;
                    maxPrimaryY = maxPrimaryY + spacing;


                    diagram.AxisY.WholeRange.SetMinMaxValues(minPrimaryY, maxPrimaryY);

                    detailReport.DataSource = ds.Tables[1];

                    report.AllControls<XRChart>().ToList().ForEach(chart =>
                    {
                        chart.DataSource = ds.Tables[0];
                    });
                    foreach (Band band in report.Bands)
                    {
                        if (band is DetailReportBand)
                        {
                            ((DetailReportBand)band).DataMember = "Table1";
                        }
                    }

                    DownloadPDF(context ,report,  "RPT_LaporanAnalisaPerbandinganTMAT.pdf");
                }
            }
        }
        private void DownloadPDF(HttpContext context, XtraReport xr, string namepdf)
        {
            string prefixfilenames = DateTime.Now.ToString().Replace("/", "");
            prefixfilenames = prefixfilenames.Replace(":", "");
            prefixfilenames = prefixfilenames.Replace(" ", "_");

            string filename = prefixfilenames + "_" + namepdf;

            byte[] bytesInStream = null;
            string fullpath = context.Server.MapPath("..") + "\\ReportFile\\" + filename;
            MemoryStream ms = new MemoryStream();
            PdfExportOptions pdfOptions = xr.ExportOptions.Pdf;
            pdfOptions.ConvertImagesToJpeg = false;
            pdfOptions.ImageQuality = PdfJpegImageQuality.High;
            xr.ExportToPdf(fullpath, pdfOptions);            // simpler way of converting to array
            ms.Close();

            context.Response.ClearContent();
            context.Response.ClearHeaders();
            context.Response.ContentType = "application/pdf";
            context.Response.AddHeader("content-disposition", "attachment; filename=" + filename);
            context.Response.TransmitFile(fullpath);
            context.Response.Flush();
            context.Response.Close();
            FileInfo fo = new FileInfo(fullpath);
            if (fo.Exists)
            {
                fo.Delete();
            }
        }
        //report.Parameters["date"].Value = newTanggal;
        //report.Parameters["dateminsatu"].Value = tanggalHeader;

        //string PDFFILE = "RPT_ProgressHarianCurahHujan";
        //string extention = "PDF";
        //report.DataSource = ds.Tables[0];
        //foreach (Band band in report.Bands)
        //{
        //    if (band is DetailReportBand)
        //    {
        //        ((DetailReportBand)band).DataMember = "Table1";
        //    }
        //}

        //DownloadFile(report, PDFFILE, extention, context);



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        


    }
}

