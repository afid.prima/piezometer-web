﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using DevExpress.XtraCharts;
using System.Data;
using System.Globalization;
using PASS.Mapper;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Drawing;
using PZO_PiezometerOnlineMap.Mapper;

namespace PZO_PiezometerOnlineMap.Handler
{
        
    public class PZO_AnalisaPerbandinganTMAT : IHttpHandler
    {

        private PZO_PiezometerMapper mapper = new PZO_PiezometerMapper();
        private PZO_ProjectMapperTambahan mapperTambahan = new PZO_ProjectMapperTambahan();
        private Color[] listColor = {Color.Blue, Color.Red, Color.Orange, Color.Navy, Color.Aqua, Color.Black,
                                    Color.BlueViolet, Color.Brown, Color.Cyan, Color.DarkMagenta, Color.DarkOrange,
                                    Color.DarkTurquoise, Color.DeepSkyBlue, Color.DeepPink};

        public void ProcessRequest(HttpContext context)
        {
            string month = context.Request.Params["month"];
            string year = context.Request.Params["year"];
            string week = context.Request.Params["week"];
            string wmacode = context.Request.Params["wmaCode"];
            string raCode = context.Request.Params["raCode"];
            string minRange = context.Request.Params["minRange"];
            string maxRange = context.Request.Params["maxRange"];
            string estCode = context.Request.Params["estCode"];

            generateReport(context, wmacode, raCode, week, month, year, float.Parse(minRange), float.Parse(maxRange), estCode);

        }



        public void generateReport(HttpContext context, string wmcode, string racode, string minggu, string bulan, string tahun, float minX, float maxX, string estCode)
        {

            XtraReport report =
               XtraReport.FromFile(@"" + context.Server.MapPath("..") + "\\ReportFile\\RPT_LaporanAnalisaPerbandinganTMAT.repx", true);

            float minPrimaryY = (float)0;
            float maxPrimaryY = (float)10;
            string[] listRambu = racode.Split(new Char[] { ',' });

            DataSet ds = mapperTambahan.GetPerbandinganTMATTMAS2(wmcode, racode, int.Parse(tahun), int.Parse(bulan), int.Parse(minggu), maxX, minX, estCode);
            DataTable dtHeader = ds.Tables[2];
            report.DataSource = ds;
            report.Parameters["zone"].Value = wmcode;
            report.Parameters["pulau"].Value = dtHeader.Rows[0]["pulau"];
            report.Parameters["periode"].Value = dtHeader.Rows[0]["periode"];
            report.Parameters["rambu"].Value = racode;

            DetailReportBand detailReport = report.Bands["DetailReport"] as DetailReportBand;

            GroupFooterBand footerband = report.Bands["GroupFooter1"] as GroupFooterBand;
            XRChart chartControl = footerband.Controls["xrChart1"] as XRChart;
            Series series1 = chartControl.Series["TMAS Normal"] as Series;

            for (int i = 0; i < listRambu.Length; i++)
            {
                string rambu = listRambu[i];
                DataColumnCollection columns = ds.Tables[0].Columns;
                if (columns.Contains(rambu))
                {
                    Series series2 = new Series(rambu, ViewType.Line);
                    series2.DataSource = ds.Tables[0];
                    series2.ValueScaleType = ScaleType.Numerical;
                    series2.ArgumentScaleType = ScaleType.DateTime;
                    series2.ArgumentDataMember = "hari";
                    series2.ValueDataMembers.AddRange(new string[] { rambu });
                    DataFilter df = new DataFilter((rambu + "_V"), "System.String", DataFilterCondition.Equal, "TRUE");
                    series2.DataFilters.Add(df);
                    series2.Label.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
                    series2.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;

                    LineSeriesView view = (LineSeriesView)series2.View;
                    view.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True;
                    view.LineMarkerOptions.Size = 4;
                    view.LineMarkerOptions.Kind = MarkerKind.Circle;
                    view.Color = listColor[i];
                    chartControl.Series.Add(series2);
                }
            }



            if (minX == 0 && maxX == 0)
            {
                series1.Visible = false;
            }

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row["minX"] = minX;
                row["maxX"] = maxX;
                foreach (string rambu in listRambu)
                {
                    DataColumnCollection columns = ds.Tables[0].Columns;
                    if (columns.Contains(rambu))
                    {
                        
                        float tmas = float.Parse(row[rambu].ToString());
                        if (minPrimaryY == 0 && tmas > 0)
                        {
                            minPrimaryY = float.Parse(row[rambu].ToString());
                            maxPrimaryY = float.Parse(row[rambu].ToString());
                        }
                        else
                        {
                            if (row[rambu].ToString() != null)
                            {
                                if (tmas < minPrimaryY && tmas > 0)
                                {
                                    minPrimaryY = tmas;
                                }
                                if (tmas > maxPrimaryY)
                                {
                                    maxPrimaryY = tmas;
                                }
                            }
                        }
                    }
                }

            }

            float range = (float)0.05;
            int gridalign = 2;
            XYDiagram diagram = (XYDiagram)chartControl.Diagram;
            diagram.AxisY.WholeRange.SideMarginsValue = 0;
            float spacing = (float)0.5;

            if (maxPrimaryY != 0 && minPrimaryY != 0)
            {
                range = (float)0.05;
                gridalign = 1;
                diagram.AxisY.NumericScaleOptions.GridSpacing = range;
                diagram.AxisY.NumericScaleOptions.CustomGridAlignment = gridalign;
                spacing = (float)0.05;
            }

            if (minPrimaryY == 0) minPrimaryY = 0;
            if (maxPrimaryY == 0) maxPrimaryY = 2;
            minPrimaryY = minPrimaryY - spacing;
            maxPrimaryY = maxPrimaryY + spacing;


            diagram.AxisY.WholeRange.SetMinMaxValues(minPrimaryY, maxPrimaryY);

            detailReport.DataSource = ds.Tables[1];

            report.AllControls<XRChart>().ToList().ForEach(chart =>
            {
                chart.DataSource = ds.Tables[0];
            });

            foreach (Band band in report.Bands)
            {
                if (band is DetailReportBand)
                {
                    ((DetailReportBand)band).DataMember = "Table1";
                }
            }

            string prefixfilenames = DateTime.Now.ToString().Replace("/", "");
            prefixfilenames = prefixfilenames.Replace(":", "");
            prefixfilenames = prefixfilenames.Replace(" ", "_");

            string filename = "RPT_LaporanAnalisaPerbandinganTMAT_" + wmcode + "_" + prefixfilenames + ".pdf";

            string fullPath = Path.Combine(context.Server.MapPath("~/content/RPT/PDF_Report"), filename);
            Directory.CreateDirectory(context.Server.MapPath("~/content/RPT/PDF_Report/"));

            PdfExportOptions pdfOptions = report.ExportOptions.Pdf;
            pdfOptions.ConvertImagesToJpeg = false;
            pdfOptions.ImageQuality = PdfJpegImageQuality.High;
            report.ExportToPdf(fullPath, pdfOptions);

            DownloadFile(context, fullPath, filename);
        }

        private void DownloadFile(HttpContext context, string path, string filename)
        {
            filename = filename.Replace(" ", "%20");

            context.Response.Clear();
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            context.Response.ContentType = "application/octet-stream";
            context.Response.TransmitFile(path);
            context.Response.Flush();
            context.Response.Close();
            context.Response.End();


            FileInfo file = new FileInfo(path);
            if (file.Exists)
                file.Delete();
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}