﻿using DevExpress.XtraReports.UI;
using PASS.Mapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace PZO_PiezometerOnlineMap.Handler
{
    /// <summary>
    /// Summary description for GetReportHandler
    /// </summary>
    public class GetReportHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string param = context.Request.Params["reporttype"];
            string filename = context.Request.Params["filename"];
            string filepath = context.Server.MapPath("..") + "\\Attachment\\" + filename;

            if (param == "weekly")
            {
                DownloadFile(context, filepath, filename);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        private void DownloadFile(HttpContext context, string path, string filename)
        {
            filename = filename.Replace(" ", "%20");

            context.Response.Clear();
            context.Response.ClearHeaders();
            context.Response.ClearContent();
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            context.Response.ContentType = "application/octet-stream";
            context.Response.TransmitFile(path);
            context.Response.Flush();
            context.Response.Close();
            context.Response.End();
        }

    }
}