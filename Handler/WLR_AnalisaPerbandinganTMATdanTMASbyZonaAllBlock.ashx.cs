﻿using DevExpress.XtraCharts;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using PZO_PiezometerOnlineMap.Mapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace PZO_PiezometerOnlineMap.Handler
{
    /// <summary>
    /// Summary description for WLR_AnalisaPerbandinganTMATdanTMASbyZona
    /// </summary>
    public class WLR_AnalisaPerbandinganTMATdanTMASbyZonaAllBlock : IHttpHandler
    {

        private PZO_ProjectMapperTambahan mapper = new PZO_ProjectMapperTambahan();
        DataSet currentDS = new DataSet();

        DataTable dtColor = new DataTable();


        public void ProcessRequest(HttpContext context)
        {
            string month = context.Request.Params["month"];
            string year = context.Request.Params["year"];
            string week = context.Request.Params["week"];
            string zona = context.Request.Params["zona"];
            //string piezoId = context.Request.Params["pzoid"];

            dtColor.Columns.Add("ColorName");
            dtColor.Columns.Add("ColorCode");

            for (int i = 1; i <= 8; i++)
            {
                DataRow row = dtColor.NewRow();
                switch (i)
                {
                    case 1:
                        row["ColorName"] = "Banjir";
                        row["ColorCode"] = "#80000000";
                        break;
                    case 2:
                        row["ColorName"] = "Tergenang";
                        row["ColorCode"] = "#802f74b7";
                        break;
                    case 3:
                        row["ColorName"] = "Agak Tergenang";
                        row["ColorCode"] = "#8001b0f3";
                        break;
                    case 4:
                        row["ColorName"] = "Normal";
                        row["ColorCode"] = "#80008000";
                        break;
                    case 5:
                        row["ColorName"] = "Agak Kering";
                        row["ColorCode"] = "#80FFFF00";
                        break;
                    case 6:
                        row["ColorName"] = "Kering";
                        row["ColorCode"] = "#80FF0000";
                        break;
                    case 7:
                        row["ColorName"] = "No Data";
                        row["ColorCode"] = "#80FFFFFF";
                        break;
                    case 8:
                        row["ColorName"] = "Rusak / Hilang";
                        row["ColorCode"] = "#80808080";
                        break;
                    default:
                        row["ColorName"] = "Unidentified";
                        row["ColorCode"] = "#80000000";
                        break;
                }

                dtColor.Rows.Add(row);
            }


            generateReport(context, zona, week, month, year);

        }



        public void generateReport(HttpContext context, string zona, string minggu, string bulan, string tahun)
        {
            XtraReport report =
               XtraReport.FromFile(@"" + context.Server.MapPath("..") + "\\ReportFile\\RPT_LaporanAnalisaTMATdanTMASbyZonaAllBlock_2.repx", true);

            DataSet ds = mapper.GetReportAnalisaTMATdanTMASbyZonaAllBlock(zona, int.Parse(minggu), int.Parse(bulan), int.Parse(tahun));

            DataTable dtHeaderChart = ds.Tables[4];

            DetailBand detail = report.Bands["Detail"] as DetailBand;
            XRLabel label1 = detail.Controls["xrLabel38"] as XRLabel;
            XRLabel label2 = detail.Controls["xrLabel39"] as XRLabel;
            XRLabel label3 = detail.Controls["xrLabel40"] as XRLabel;
            XRLabel label4 = detail.Controls["xrLabel41"] as XRLabel;
            XRLabel label5 = detail.Controls["xrLabel42"] as XRLabel;
            XRLabel label6 = detail.Controls["xrLabel43"] as XRLabel;

            //label1.Text = dtHeaderChart.Rows[0]["detail"] != null ? dtHeaderChart.Rows[0]["detail"].ToString() : "";
            //label2.Text = dtHeaderChart.Rows[1]["detail"] != null ? dtHeaderChart.Rows[1]["detail"].ToString() : "";
            //label3.Text = dtHeaderChart.Rows[2]["detail"] != null ? dtHeaderChart.Rows[2]["detail"].ToString() : "";
            //label4.Text = dtHeaderChart.Rows[3]["detail"] != null ? dtHeaderChart.Rows[3]["detail"].ToString() : "";
            //label5.Text = dtHeaderChart.Rows[4]["detail"] != null ? dtHeaderChart.Rows[4]["detail"].ToString() : "";
            //label6.Text = dtHeaderChart.Rows[5]["detail"] != null ? dtHeaderChart.Rows[5]["detail"].ToString() : "";

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Uri uri = new Uri(row["image_url"].ToString());
                row["image_url"] = uri.AbsoluteUri;
            }

            DataTable masterGraphBlock = ds.Tables[5].AsDataView().ToTable(true, "estName", "Block", "ElevasiTanah", "ElevasiType");

            //report.Parameters["periodepeta"].Value = ds.Tables[0].Rows[0]["mapperiode"].ToString();

            ds.Tables[0].Columns.Add("lastValue", typeof(System.String));
            ds.Tables[0].Columns.Add("lastValueTitle", typeof(System.String));
            ds.Tables[2].Columns.Add("default_Min", typeof(System.Double));
            ds.Tables[2].Columns.Add("default_Max", typeof(System.Double));
            ds.Tables[2].Columns.Add("default_V", typeof(System.String));

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string stationName = row["stationName"].ToString();
                string stationView = row["stationName"].ToString() + "_V";
                string graphName = row["namaGraph"].ToString();

                float targetTmas = float.Parse(row["TmasTarget"].ToString());
                float targetMin = float.Parse(row["minTarget"].ToString());
                float targetmax = float.Parse(row["maxTarget"].ToString());
                string showTarget = row["target_V"].ToString();
                string valueLast = "";
                string datelast = "";


                foreach (DataRow dRow in ds.Tables[2].Rows)
                {
                    DataColumnCollection columns = ds.Tables[2].Columns;
                    if (columns.Contains(stationName) && columns.Contains(stationView))
                    {
                        string graphView = dRow[stationView].ToString();
                        string graphValue = dRow[stationName].ToString();
                        string dategraph = dRow["tanggal"].ToString();
                        if (graphView.Equals("TRUE"))
                        {
                            valueLast = graphValue;
                            string date = dategraph;
                            DateTime myDate;
                            if (!DateTime.TryParse(date, out myDate))
                            {
                                string[] d = date.Split(new Char[' ']);
                                string[] dx = d[0].Split(new Char['-']);
                                datelast = "Tgl " + dx[2] + "/" + dx[1];
                            }
                            else
                            {
                                datelast = "Tgl " + myDate.ToString("dd/MM");
                            }
                        }
                        dRow["default_V"] = "TRUE";
                        dRow["default_Min"] = targetMin;
                        dRow["default_Max"] = targetmax;
                    }
                }

                if (datelast.Equals("")) datelast = "Tgl ";
                if (valueLast != null && showTarget.Equals("TRUE"))
                {
                    if (!valueLast.Equals(""))
                    {
                        float val = float.Parse(valueLast);
                        float result = val - targetTmas;
                        valueLast = valueLast + " (" + result.ToString("0.00") + ")";
                    }
                }

                row["lastValue"] = valueLast;
                row["lastValueTitle"] = datelast;
            }
            currentDS = ds;

            DataSet dsreport = new DataSet();
            dsreport.Merge(ds.Tables[0]);
            masterGraphBlock.TableName = "Table1";
            dsreport.Merge(masterGraphBlock);
            report.DataSource = dsreport;

            foreach (Band band in report.Bands)
            {
                if (band is DetailReportBand)
                {
                    ((DetailReportBand)band).DataMember = "Table1";
                }
            }



            DetailReportBand detailReport = report.Bands["DetailReport"] as DetailReportBand;
            DetailBand detail1 = detailReport.Bands["Detail1"] as DetailBand;
            detail1.BeforePrint += detail1_BeforePrint;
            XRChart chart2 = detail1.Controls["xrChart2"] as XRChart;
            chart2.BeforePrint += chart2_BeforePrint;


            string prefixfilenames = DateTime.Now.ToString().Replace("/", "");
            prefixfilenames = prefixfilenames.Replace(":", "");
            prefixfilenames = prefixfilenames.Replace(" ", "_");

            string filename = "RPT_LaporanAnalisaTMATdanTMASbyZona_" + zona + "_" + prefixfilenames + ".pdf";

            string fullPath = Path.Combine(context.Server.MapPath("~/content/RPT/PDF_Report"), filename);
            Directory.CreateDirectory(context.Server.MapPath("~/content/RPT/PDF_Report/"));

            PdfExportOptions pdfOptions = report.ExportOptions.Pdf;
            pdfOptions.ConvertImagesToJpeg = false;
            pdfOptions.ImageQuality = PdfJpegImageQuality.High;
            report.ExportToPdf(fullPath, pdfOptions);

            DownloadFile(context, fullPath, filename);
        }

        void detail1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DetailBand detail1 = (DetailBand)sender;
            XtraReportBase reportbase = detail1.Report;
            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            string current_est = dataRow["estName"].ToString();
            string current_block = dataRow["Block"].ToString();
            string current_elev = dataRow["ElevasiTanah"].ToString();
            string current_elev_type = dataRow["ElevasiType"].ToString();

            string[] estCollection = current_est.Split('-');

            XRLabel lblEst = detail1.Controls["xrLabel11"] as XRLabel;
            XRLabel lblBlock = detail1.Controls["xrLabel33"] as XRLabel;
            XRLabel lblElev = detail1.Controls["xrLabel12"] as XRLabel;

            lblEst.Text = estCollection[0];
            lblBlock.Text = current_block;
            lblElev.Text = current_elev + " mdpl " + "( " + current_elev_type + " )";

        }

        void chart2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //My Code Start From Here

            XRChart chart2 = (XRChart)sender;
            XtraReportBase reportbase = chart2.Report;

            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            string current_est = dataRow["estName"].ToString();
            string current_block = dataRow["Block"].ToString();

            //string elevasiTanah = mapper.getElevasiTanahbyBlock(current_block);
            DataTable tableGraph2 = currentDS.Tables[5];
            DataTable dtklasifikasi = currentDS.Tables[6];
            DataTable dtRambuUtama = currentDS.Tables[7];
            string a = currentDS.Tables[0].Rows[0]["stationName"].ToString();
            //CREATE LINE CHART SERIES
            chart2.Series.Clear();

            List<Series> listLine = new List<Series>();

            //Rambu Block

            Series series2 = new Series("TMAT", ViewType.Line);
            series2.DataSource = tableGraph2;
            series2.ValueScaleType = ScaleType.Numerical;
            series2.ArgumentScaleType = ScaleType.Auto;
            series2.ArgumentDataMember = "WeekName";
            series2.ValueDataMembers.AddRange(new string[] { "KetinggianMldp" });
            DataFilter df = new DataFilter("estName", "System.String", DataFilterCondition.Equal, current_est);
            DataFilter dfblock = new DataFilter("Block", "System.String", DataFilterCondition.Equal, current_block);
            series2.DataFilters.Add(df);
            series2.DataFilters.Add(dfblock);
            series2.Label.LineVisibility = DevExpress.Utils.DefaultBoolean.True;

            //XRChart chartControl = chart2.Controls["xrChart2"] as XRChart;
            //chartControl.CustomDrawSeriesPoint += chartControl2_CustomDrawSeriesPoint;

            series2.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;


            LineSeriesView view = (LineSeriesView)series2.View;
            view.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True;

            view.LineMarkerOptions.Kind = MarkerKind.Circle;
            view.LineStyle.Thickness = 2;
            view.LineMarkerOptions.Size = 3;
            Color cc = System.Drawing.ColorTranslator.FromHtml("#c0504e");
            view.Color = cc;

            chart2.Series.Add(series2);


            Series ss = (Series)series2.Clone();
            double ketinggian = 0.00;
            //Rambu Utama
            //Series series3 = new Series("Rambu Utama", ViewType.Line);
            //series3.DataSource = dtRambuUtama;
            //series3.ValueScaleType = ScaleType.Numerical;
            //series3.ArgumentScaleType = ScaleType.Auto;
            //series3.ArgumentDataMember = "WeekName";
            //series3.ValueDataMembers.AddRange(new string[] { "TMAS" });
            //DataFilter df = new DataFilter("estName", "System.String", DataFilterCondition.Equal, current_est);
            //DataFilter dfblock = new DataFilter("Block", "System.String", DataFilterCondition.Equal, current_block);
            //listLine.Add(ss);

            //END OF LINE CHART

            //chart2.Series.Clear();


            XYDiagram diagram = (XYDiagram)chart2.Diagram;



            for (int i = 0; i < listLine.Count(); i++)
            {
                chart2.Series.Add(listLine[i]);
            }
            //for (int i = 0; i < dtRambuUtama.Rows.Count; i++)
            //{
            //    double num = Convert.ToDouble(dtRambuUtama.Rows[i]["TMAS"]);
            //    minSecondaryY = Math.Min(minSecondaryY, num);
            //    maxSecondaryY = Math.Max(maxSecondaryY, num);
            //}

            //float jarak = 5;
            //double space = 0.05;
            //if (minSecondaryY > 0.05)
            //{
            //    minSecondaryY = minSecondaryY - space;
            //}
            //sY.WholeRange.SetMinMaxValues(6.06, maxSecondaryY+space);
            //sY.NumericScaleOptions.GridSpacing = 0.10;

            //diagram.AxisY.NumericScaleOptions.GridSpacing = 2;

            double minPrimaryY = 1000.00;
            double maxPrimaryY = -99.00;
            //float sumCH = 0;
            foreach (DataRow row in tableGraph2.Rows)
            {
                //float sumRain = 0;
                //sumRain = float.Parse((row["rainInches"].ToString() == null ? "0" : row["rainInches"].ToString()));
                //sumCH = sumCH + sumRain;
                string estate = row["estName"].ToString();
                string block = row["Block"].ToString();

                if (estate.Equals(current_est) && block.Equals(current_block))
                {
                    if (row["KetinggianMldp"] != null && row["KetinggianMldp"].ToString() != "")
                    {
                       ketinggian = Convert.ToDouble(row["KetinggianMldp"].ToString());
                    }                  
                    if (maxPrimaryY == -99.00 && ketinggian > -99.00)
                    {
                        minPrimaryY = ketinggian;
                        maxPrimaryY = ketinggian;
                    }
                    else
                    {
                        if (row["KetinggianMldp"].ToString() != null)
                        {
                            if (ketinggian < minPrimaryY && ketinggian > 0)
                            {
                                minPrimaryY = ketinggian;
                            }
                            if (ketinggian > maxPrimaryY)
                            {
                                maxPrimaryY = ketinggian;
                            }
                        }
                    }
                }

            }
            //Start Line Rambu Utama


            Series seriesb = new Series("Dt Rambu Utama", ViewType.Line);
            seriesb.DataSource = dtRambuUtama;



            ////rambu TMAS
            LineSeriesView view2 = (LineSeriesView)seriesb.View;
            view2.AxisY = diagram.AxisY;
            seriesb.ValueScaleType = ScaleType.Numerical;
            seriesb.ArgumentScaleType = ScaleType.Auto;
            seriesb.ArgumentDataMember = "WeekName";
            seriesb.ValueDataMembers.AddRange(new string[] { "TMAS" });
            seriesb.Label.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
            seriesb.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;

            view2.LineMarkerOptions.Kind = MarkerKind.Circle;
            view2.LineStyle.Thickness = 2;
            view2.LineMarkerOptions.Size = 3;
            Color cc2 = System.Drawing.ColorTranslator.FromHtml("#48d9ef");
            view2.Color = cc2;
            chart2.Series.Add(seriesb);



            //START AREA CHART

            foreach (DataRow row in dtklasifikasi.Rows)
            {
                string area = row["IndicatorAlias"].ToString();
                string color = "";

                foreach (DataRow col in dtColor.Rows)
                {
                    string n = col["ColorName"].ToString();
                    string c = col["ColorCode"].ToString();
                    if (n.Equals(area))
                    {
                        color = c;
                        Color _color = System.Drawing.ColorTranslator.FromHtml(color);
                        DataColumnCollection columns = tableGraph2.Columns;
                        string code = area + "_Min";

                        if (columns.Contains(code))
                        {
                            Series seriesArea = new Series(area, ViewType.RangeArea);
                            seriesArea.DataSource = tableGraph2;
                            seriesArea.DataFilters.Add(dfblock);
                            seriesArea.ValueScaleType = ScaleType.Numerical;
                            seriesArea.ArgumentScaleType = ScaleType.Auto;
                            seriesArea.ArgumentDataMember = "WeekName";
                            seriesArea.ValueDataMembers.AddRange(new string[] { area + "_Min", area + "_Max" });
                            seriesArea.Label.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
                            seriesArea.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;

                            AreaSeriesView views = (AreaSeriesView)seriesArea.View;
                            views.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True;
                            views.Color = _color;
                            chart2.Series.Add(seriesArea);
                        }
                    }
                }
            }


            //END AREA CHART
            double range = 0.01;
            double gridalign = 0.02;
            diagram.AxisY.WholeRange.SideMarginsValue = 0;
            diagram.AxisX.WholeRange.SideMarginsValue = 0.1;
            diagram.AxisX.WholeRange.AlwaysShowZeroLevel = false;
            //diagram.AxisY.Label.TextPattern = "{V:00}";
            //diagram.AxisY.Reverse = true;
            diagram.AxisX.MinorCount = 1;
            diagram.AxisY.MinorCount = 1;
            diagram.Margins.All = 0;
            Font f = new Font("Arial Narrow", 8, FontStyle.Regular);
            diagram.AxisX.Label.Font = f;
            diagram.AxisY.Label.Font = f;
            chart2.Padding = 0;
            chart2.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
            chart2.Legend.Font = f;

            GridLinesX gridX = (GridLinesX)diagram.AxisX.GridLines;
            gridX.Color = Color.Black;
            gridX.MinorVisible = false;
            gridX.Visible = true;
            double spacing = 0.15;
            if (maxPrimaryY != -99 && minPrimaryY != 1000)
            {
                double avg = (maxPrimaryY - minPrimaryY) / 0.05;
                range = 0.15;//(int)Math.Round((double)avg);
                if (range == 0)
                    range = 0.10;
                gridalign = 0.01;
                diagram.AxisY.NumericScaleOptions.GridSpacing = range;
                diagram.AxisY.NumericScaleOptions.CustomGridAlignment = gridalign;
            }
            else
            {
                range = 0.10;
                gridalign = 0.01;
                diagram.AxisY.NumericScaleOptions.GridSpacing = range;
                diagram.AxisY.NumericScaleOptions.CustomGridAlignment = gridalign;
            }
            if (minPrimaryY == 1000) minPrimaryY = 0;
            if (maxPrimaryY == -99 || maxPrimaryY == 0) maxPrimaryY = 60;
            minPrimaryY = minPrimaryY - spacing - 0.05;
            maxPrimaryY = maxPrimaryY + spacing + 0.15;
            diagram.AxisY.Label.TextPattern = "{V:n2}";
            diagram.AxisY.WholeRange.SetMinMaxValues(minPrimaryY, maxPrimaryY);
            //diagram.AxisY.NumericScaleOptions.GridSpacing = 2;
            //diagram.AxisY.Reverse = true;


        }

        void chartControl_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRChart chart1 = (XRChart)sender;

            DataTable dtStation = currentDS.Tables[1];
            DataTable dtGraphDetail = currentDS.Tables[2];
            DataTable dtklasifikasi = currentDS.Tables[3];

            string showTargetChart = dtStation.Rows[0]["target_V"].ToString();

            //CREATE LINE CHART SERIES
            chart1.Series.Clear();

            List<Series> listLine = new List<Series>();
            foreach (DataRow item in dtStation.Rows)
            {
                string rambu = item["stationName"].ToString();
                string color = item["colorField"].ToString();
                Color _color = System.Drawing.ColorTranslator.FromHtml(color);

                DataColumnCollection columns = dtGraphDetail.Columns;
                if (columns.Contains(rambu))
                {
                    Series series2 = new Series(rambu, ViewType.Line);
                    series2.DataSource = dtGraphDetail;
                    series2.ValueScaleType = ScaleType.Numerical;
                    series2.ArgumentScaleType = ScaleType.DateTime;
                    series2.ArgumentDataMember = "tanggal";
                    series2.ValueDataMembers.AddRange(new string[] { rambu });
                    DataFilter df = new DataFilter((rambu + "_V"), "System.String", DataFilterCondition.Equal, "TRUE");
                    series2.DataFilters.Add(df);
                    series2.Label.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
                    series2.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;

                    LineSeriesView view = (LineSeriesView)series2.View;
                    view.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True;

                    view.LineMarkerOptions.Kind = MarkerKind.Circle;
                    view.LineStyle.Thickness = 2;
                    view.LineMarkerOptions.Size = 3;
                    view.Color = _color;

                    chart1.Series.Add(series2);
                    Series ss = (Series)series2.Clone();
                    listLine.Add(ss);
                }
            }
            //END OF LINE CHART


            //CREATE BAR CHART SERIES
            XYDiagram diagram = (XYDiagram)chart1.Diagram;

            Series seriesb = new Series("Rata-Rata Curah Hujan", ViewType.Bar);
            seriesb.DataSource = dtGraphDetail;


            seriesb.ValueScaleType = ScaleType.Numerical;
            seriesb.ArgumentScaleType = ScaleType.DateTime;
            seriesb.ArgumentDataMember = "tanggal";
            seriesb.ValueDataMembers.AddRange(new string[] { "rainInches" });
            seriesb.Label.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
            seriesb.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;

            BarSeriesView bar = (BarSeriesView)seriesb.View;
            bar.FillStyle.FillMode = FillMode.Gradient;
            RectangleGradientFillOptions baseOps = (RectangleGradientFillOptions)bar.FillStyle.Options;
            baseOps.Color2 = System.Drawing.Color.Green;
            baseOps.GradientMode = RectangleGradientMode.LeftToRight;
            bar.Color = System.Drawing.Color.Lime;

            //END OF BAR CHART 

            chart1.Series.Clear();

            //START AREA CHART
            foreach (DataRow row in dtklasifikasi.Rows)
            {
                string area = row["NamaKlasifikasi"].ToString();
                string color = row["colorField"].ToString();
                string namaGraph = row["namaGraph"].ToString();
                string idGraph = row["idGraph"].ToString();

                Color _color = System.Drawing.ColorTranslator.FromHtml(color);
                DataColumnCollection columns = dtGraphDetail.Columns;
                string code = idGraph + "_" + area + "_Min";
                if (columns.Contains(code))
                {
                    Series seriesArea = new Series(area, ViewType.RangeArea);
                    seriesArea.DataSource = dtGraphDetail;
                    seriesArea.ValueScaleType = ScaleType.Numerical;
                    seriesArea.ArgumentScaleType = ScaleType.DateTime;
                    seriesArea.ArgumentDataMember = "tanggal";
                    seriesArea.ValueDataMembers.AddRange(new string[] { (idGraph + "_" + area + "_Min"), (idGraph + "_" + area + "_Max") });
                    seriesArea.Label.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
                    seriesArea.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;

                    AreaSeriesView view = (AreaSeriesView)seriesArea.View;
                    view.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True;
                    view.Color = _color;
                    chart1.Series.Add(seriesArea);
                }
            }
            //END AREA CHART


            //START DEFAULT AREA CHART  #B3F2C2
            if (showTargetChart.Equals("TRUE"))
            {

                string area = "TMAS Target";
                string color = "#B3F2C2";
                Color _color = System.Drawing.ColorTranslator.FromHtml(color);

                Series seriesX = new Series(area, ViewType.RangeArea);
                seriesX.DataSource = dtGraphDetail;
                seriesX.ValueScaleType = ScaleType.Numerical;
                seriesX.ArgumentScaleType = ScaleType.DateTime;
                seriesX.ArgumentDataMember = "tanggal";
                seriesX.ValueDataMembers.AddRange(new string[] { ("default_Min"), ("default_Max") });
                seriesX.Label.LineVisibility = DevExpress.Utils.DefaultBoolean.False;
                seriesX.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;

                AreaSeriesView viewx = (AreaSeriesView)seriesX.View;
                viewx.MarkerVisibility = DevExpress.Utils.DefaultBoolean.True;
                viewx.Color = _color;
                chart1.Series.Add(seriesX);
            }


            chart1.Series.Add(seriesb);
            for (int i = 0; i < listLine.Count(); i++)
            {
                chart1.Series.Add(listLine[i]);
            }

            float minPrimaryY = (float)0;
            float maxPrimaryY = (float)10;
            DateTime minDate = new DateTime();
            DateTime maxDate = new DateTime();
            float sumCH = 0;
            foreach (DataRow row in dtGraphDetail.Rows)
            {
                if (dtGraphDetail.Rows.IndexOf(row) == 0)
                {
                    minDate = Convert.ToDateTime(row["tanggal"].ToString());
                }
                if (dtGraphDetail.Rows.IndexOf(row) + 1 == dtGraphDetail.Rows.Count)
                {
                    maxDate = Convert.ToDateTime(row["tanggal"].ToString());
                }

                float sumRain = 0;
                sumRain = float.Parse((row["rainInches"].ToString() == null ? "0" : row["rainInches"].ToString()));
                sumCH = sumCH + sumRain;

                foreach (DataRow rRambu in dtStation.Rows)
                {
                    string rambu = rRambu["stationName"].ToString();
                    DataColumnCollection columns = dtGraphDetail.Columns;
                    if (columns.Contains(rambu))
                    {
                        float tmas = float.Parse(row[rambu].ToString());
                        if (minPrimaryY == 0 && tmas > 0)
                        {
                            minPrimaryY = float.Parse(row[rambu].ToString());
                            maxPrimaryY = float.Parse(row[rambu].ToString());
                        }
                        else
                        {
                            if (row[rambu].ToString() != null)
                            {
                                if (tmas < minPrimaryY && tmas > 0)
                                {
                                    minPrimaryY = tmas;
                                }
                                if (tmas > maxPrimaryY)
                                {
                                    maxPrimaryY = tmas;
                                }
                            }
                        }
                    }
                }

            }

            if (sumCH == 0)
            {
                seriesb.Visible = false;
            }

            float range = (float)0.05;
            int gridalign = 2;
            diagram.AxisY.WholeRange.SideMarginsValue = 0;
            diagram.AxisX.WholeRange.SideMarginsValue = 1;
            diagram.AxisX.WholeRange.AlwaysShowZeroLevel = false;
            diagram.AxisY.Label.TextPattern = "{V:0.00}";
            diagram.AxisX.Label.TextPattern = "{A:dd-MMM}";
            diagram.AxisX.MinorCount = 6;
            diagram.AxisY.MinorCount = 1;
            Font f = new Font("Arial Narrow", 8, FontStyle.Regular);
            diagram.AxisY.Label.Font = f;
            diagram.AxisX.Label.Font = f;
            chart1.Legend.Font = f;

            GridLinesX gridX = (GridLinesX)diagram.AxisX.GridLines;
            gridX.Color = Color.Black;
            gridX.MinorVisible = false;
            gridX.Visible = true;
            float spacing = (float)0.1;

            if (maxPrimaryY != 10 && minPrimaryY != 0)
            {
                float avg = (maxPrimaryY - minPrimaryY) / 7;
                range = (float)0.05;
                if (range == 0)
                    range = 1;
                gridalign = 1;
                diagram.AxisY.NumericScaleOptions.GridSpacing = range;
                diagram.AxisY.NumericScaleOptions.CustomGridAlignment = gridalign;
                spacing = (float)0.05;
            }
            else
            {
                float avg = 2 / 7;
                range = (float)0.05;
                if (range == 0)
                    range = 1;
                gridalign = 1;
                diagram.AxisY.NumericScaleOptions.GridSpacing = range;
                diagram.AxisY.NumericScaleOptions.CustomGridAlignment = gridalign;
                spacing = (float)0.05;
            }

            if (minPrimaryY == 0) minPrimaryY = 1;
            if (maxPrimaryY == 0 || maxPrimaryY == 10) maxPrimaryY = 3;
            minPrimaryY = minPrimaryY - spacing;
            maxPrimaryY = maxPrimaryY + spacing;

            diagram.AxisY.WholeRange.SetMinMaxValues(minPrimaryY, maxPrimaryY);
            diagram.AxisX.WholeRange.SetMinMaxValues(minDate, maxDate);
        }
        void chartControl2_CustomDrawSeriesPoint(object sender, CustomDrawSeriesPointEventArgs e)
        {
            XRChart chart2 = (XRChart)sender;
            XtraReportBase reportbase = chart2.Report;

            DataRowView dataRow = (DataRowView)reportbase.GetCurrentRow();
            string current_est = dataRow["estName"].ToString();
            string current_block = dataRow["Block"].ToString();

            //string elevasiTanah = mapper.getElevasiTanahbyBlock(current_block);
            DataTable tableGraph2 = currentDS.Tables[5];
            string value = e.LabelText.ToString();
            for (int i = 0; i < tableGraph2.Rows.Count; i++)
            {
                if (value != tableGraph2.Rows[i]["Ketinggian"].ToString())
                {
                    value = tableGraph2.Rows[i]["Ketinggian"].ToString();
                    string estate = tableGraph2.Rows[i]["estName"].ToString();
                    string block = tableGraph2.Rows[i]["Block"].ToString();
                    if (estate.Equals(current_est) && block.Equals(current_block))
                    {

                        e.LabelText = tableGraph2.Rows[i]["Ketinggian"].ToString();
                    }
                }

            }

            //foreach (DataRow row in tableGraph2.Rows)
            //{
            //    //float sumRain = 0;
            //    //sumRain = float.Parse((row["rainInches"].ToString() == null ? "0" : row["rainInches"].ToString()));
            //    //sumCH = sumCH + sumRain;
            //    string estate = row["estName"].ToString();
            //    string block = row["Block"].ToString();

            //    if (estate.Equals(current_est) && block.Equals(current_block))
            //    {
            //        e.LabelText = row["Ketinggian"].ToString();
            //    }

            //}
        }
        private void DownloadFile(HttpContext context, string path, string filename)
        {
            filename = filename.Replace(" ", "%20");
            try
            {
                context.Response.Clear();
                context.Response.ClearHeaders();
                context.Response.ClearContent();
                context.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
                context.Response.ContentType = "application/octet-stream";
                context.Response.TransmitFile(path);
                context.Response.Flush();
                context.Response.Close();
                context.Response.End();
            }
            finally
            {
                FileInfo file = new FileInfo(path);
                if (file.Exists)
                    File.Delete(path);
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}