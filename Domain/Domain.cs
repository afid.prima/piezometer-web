﻿#region .NET Base Class Namespace Imports
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
#endregion

namespace PZO_PiezometerOnlineMap.Domain
{
    public class Domain
    {
        

    }

    public class PZO_Domain
    {
        private String m_id;
        private String m_estCode;
        private String m_afdeling;
        private String m_block;
        private String m_kodetmat;
        private String m_merk;
        private String m_type;
        private String m_kondisi;
        private String m_serialnumber;
        private String m_baterai;
        private String m_lastcheck;
        private String m_remarks;
        
        public String ID
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public String EstCode
        {
            get { return m_estCode; }
            set { m_estCode = value; }
        }
        public String Afdeling
        {
            get { return m_afdeling; }
            set { m_afdeling = value; }
        }
        public String Block
        {
            get { return m_block; }
            set { m_block = value; }
        }
        public String KodeTMAT
        {
            get { return m_kodetmat; }
            set { m_kodetmat = value; }
        }
        public String Merk
        {
            get { return m_merk; }
            set { m_merk = value; }
        }
        public String Type
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public String Kondisi
        {
            get { return m_kondisi; }
            set { m_kondisi = value; }
        }
        public String SerialNumber
        {
            get { return m_serialnumber; }
            set { m_serialnumber = value; }
        }
        public String Baterai
        {
            get { return m_baterai; }
            set { m_baterai = value; }
        }
        public String lastCheck
        {
            get { return m_lastcheck; }
            set { m_lastcheck = value; }
        }
        public String Remark
        {
            get { return m_remarks; }
            set { m_remarks = value; }
        }

    }

    public class Absent
    {
        private String m_estCode;
        private String m_companyCode;
        private String m_month;
        private String m_year;


        public List<subAbsent> ListPiezo { get; set; }
        public List<WeekList> ListWeek { get; set; }
        public List<BlockRehab> ListBlock { get; set; }


        public String estCode
        {
            get { return m_estCode; }
            set { m_estCode = value; }
        }
        public String companyCode
        {
            get { return m_companyCode; }
            set { m_companyCode = value; }
        }
        public String month
        {
            get { return m_month; }
            set { m_month = value; }
        }
        public String year
        {
            get { return m_year; }
            set { m_year = value; }
        }
    }

    public class subAbsent
    {
        private string m_PieRecordID;
        private String m_Date;
        private string m_HasilGenerate;
        private string m_CreatedBy;
        private string m_CreatedDate;

        //User
        public string PieRecordID
        {
            get { return m_PieRecordID; }
            set { m_PieRecordID = value; }
        }

        public string Date
        {
            get { return m_Date; }
            set { m_Date = value; }
        }

        public string HasilGenerate
        {
            get { return m_HasilGenerate; }
            set { m_HasilGenerate = value; }
        }

        //Remarks
        public string CreatedBy
        {
            get { return m_CreatedBy; }
            set { m_CreatedBy = value; }
        }

        public string CreatedDate
        {
            get { return m_CreatedDate; }
            set { m_CreatedDate = value; }
        }


    }


    public class WeekList
    {
        private String m_Date;

        public string Date
        {
            get { return m_Date; }
            set { m_Date = value; }
        }

    }


    public class BlockRehab
    {
        private String m_Block;

        public string Block
        {
            get { return m_Block; }
            set { m_Block = value; }
        }
    }

}