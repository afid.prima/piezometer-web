﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PASS.Mapper;

namespace PZO_PiezometerOnlineMap.Class
{
    public class BulkData
    {
        public BulkData()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public bool BulkInsertDataTable(string tableName, DataTable dataTable)
        {
            bool isSuccess;
            try
            {
                SqlConnection SqlConnectionObj = new SqlConnection(COR_General.connStringGISAPP);
                SqlConnectionObj.Open();
                SqlBulkCopy bulkCopy = new SqlBulkCopy(SqlConnectionObj, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null);
                bulkCopy.DestinationTableName = tableName;
                bulkCopy.WriteToServer(dataTable);
                isSuccess = true;
                SqlConnectionObj.Close();
            }
            catch
            {
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}