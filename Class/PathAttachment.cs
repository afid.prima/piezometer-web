﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace PZO_PiezometerOnlineMap.Class
{
    public class PathAttachment
    {
        public string pathfile()
        {
            string address = WebConfigurationManager.AppSettings["pathattachment"];
            return address;
        }
    }
}