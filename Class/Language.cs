﻿using System;
using System.Web;
using System.Web.SessionState;
using PASS.Domain;
using PASS.Mapper;

namespace PZO_PiezometerOnlineMap.Class
{
    public class Language
    {
        private COR_LanguageDomain objLangDomain = new COR_LanguageDomain();
        private COR_LanguageMapper objlangMapper = new COR_LanguageMapper();

        public string Translate(string _codes, string _session)
        {
            HttpSessionState Session = HttpContext.Current.Session;
            String activeLang = "";
            String Code = "";
            String terjemahan = "";

            if (Session["LangTracer"] != null)
            {
                if (Session["LangTracer"].ToString() == "On")
                {
                    activeLang = _session.ToString();
                    Code = _codes.ToString();
                    terjemahan = "";
                    objLangDomain.LangCode = activeLang;
                    objLangDomain.ListCode = Code;
                    if (objlangMapper.GetLanguageDescriptionDeveloperMode(ref objLangDomain))
                    {
                        terjemahan = objLangDomain.ListDesc.ToString();
                    }
                    else
                    {
                        objLangDomain.LangCode = "ENG";
                        objLangDomain.ListCode = Code;

                        if (objlangMapper.GetLanguageDescriptionDeveloperMode(ref objLangDomain) == true)
                        {
                            terjemahan = objLangDomain.ListDesc.ToString();
                        }
                    }
                }
                else
                {
                    activeLang = _session.ToString();
                    Code = _codes.ToString();
                    terjemahan = "";
                    objLangDomain.LangCode = activeLang;
                    objLangDomain.ListCode = Code;
                    if (objlangMapper.GetLanguageDescription(ref objLangDomain))
                    {
                        terjemahan = objLangDomain.ListDesc.ToString();
                    }
                    else
                    {
                        objLangDomain.LangCode = "ENG";
                        objLangDomain.ListCode = Code;

                        if (objlangMapper.GetLanguageDescription(ref objLangDomain) == true)
                        {
                            terjemahan = objLangDomain.ListDesc.ToString();
                        }
                    }
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Default.aspx");
            }

            return terjemahan;
        }
    }
}