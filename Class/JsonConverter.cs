﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace PZO_PiezometerOnlineMap.Class
{
    public class JsonConverter
    {
        public string ObjectToJSON(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in dt.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return jsSerializer.Serialize(parentRow);
        }

        public List<object> JSONToObject(string JSON)
        {
            JavaScriptSerializer jsDeserializer = new JavaScriptSerializer();
            return jsDeserializer.Deserialize<List<object>>(JSON);
        }
    }
}