﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using System.Web.Configuration;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.IO;

/// <summary>
/// Summary description for Email
/// </summary>
namespace PASS.Email
{
    public class Email
    {
        public Email()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string PopulateBody(string subject, string userName, string notification, string detail, string closing, bool isApprovalEmail)
        {
            string body = string.Empty;
            string htmltemplate = string.Empty;
            if (isApprovalEmail)
                htmltemplate = "EmailTemplateApprovalIndoPalm.htm";
            else
                htmltemplate = "EmailTemplateIndoPalm.htm";

            using (StreamReader reader = new StreamReader(ConfigurationManager.AppSettings["pathcontent"] + @"HTML\" + htmltemplate))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{Email Header}", subject);
            body = body.Replace("{Username}", userName);
            body = body.Replace("{Opening}", notification);
            body = body.Replace("{Content}", detail);
            body = body.Replace("{Closing}", closing);

            return body;
        }

        public string PopulateBodyForUserRegistrationVerification(string EmailHeader, string userName, string detail, string opening, string closing, string Code)
        {
            string body = string.Empty;

            using (StreamReader reader = new StreamReader(ConfigurationManager.AppSettings["pathcontent"] + "EmailTemplateIndoPalm.htm"))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{Email Header}", EmailHeader);
            body = body.Replace("{Username}", userName);
            body = body.Replace("{Content}", detail);
            body = body.Replace("{Opening}", opening);
            body = body.Replace("{Closing}", closing);
            body = body.Replace("{Email Code}", Code);
            return body;
        }

        public void SendHtmlFormattedEmail(HttpFileCollection files, string recepientEmail, string subject, string body)
        {
            body = body.Replace("{Footer}", "This message was sent to " + recepientEmail);
            using (MailMessage mailMessage = new MailMessage())
            {
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
                LinkedResource imagelink = new LinkedResource(WebConfigurationManager.AppSettings["imgsource"] + "logologin4.png", "image/png");
                imagelink.ContentId = "ImageID";
                imagelink.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                htmlView.LinkedResources.Add(imagelink);
                mailMessage.AlternateViews.Add(htmlView);

                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UsernameEmailIndoPalm"]);
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail));

                if (files != null)
                {
                    if (files.Count > 0)
                    {
                        for (int i = 0; i < files.Count; i++)
                        {
                            HttpPostedFile file = files[i];
                            if (file.ContentLength > 0)
                            {
                                mailMessage.Attachments.Add(new Attachment(file.InputStream, Path.GetFileName(file.FileName), file.ContentType));
                            }
                        }
                    }
                }

                SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["HostEmailIndoPalm"]);
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSLEmailIndoPalm"]);
                smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["UsernameEmailIndoPalm"], ConfigurationManager.AppSettings["PasswordEmailIndoPalm"]);
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);

                ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                X509Certificate certificate,
                X509Chain chain,
                SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };

                smtp.Send(mailMessage);
            }
        }
    }
}