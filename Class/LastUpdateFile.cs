﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PZO_PiezometerOnlineMap.Class
{
    public class LastUpdateFile
    {
        public string cekLastUpdateJs(string filePath)
        {
            DateTime lastModified = File.GetLastWriteTimeUtc(filePath);
            return lastModified.ToString("yyyyMMddHHmmss");
        }
    }
}