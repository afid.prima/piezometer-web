﻿#region .NET Base Class Namespace Imports
using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Web.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using PZO_PiezometerOnlineMap.Class;
#endregion

#region Custom Namespace
using PASS.Domain;
using PASS.Mapper;

using PASS.Email;
#endregion

namespace PPB_Project
{
    public partial class Default : System.Web.UI.Page
    {
        #region Custom Variable Declaration

        private Cryptography crypto = new Cryptography();
        private Language lang = new Language();
        private COR_LanguageMapper LanguageObj = new COR_LanguageMapper();
        private COR_LanguageDomain LanguageObjDomain = new COR_LanguageDomain();
        private COR_UserMapper userObjMapper = new COR_UserMapper();
        private COR_userDomain userObjDomain = new COR_userDomain();
        private Email email = new Email();

        #endregion

        #region EventHandler

        protected void Page_Load(object sender, EventArgs e)
        {

            Session["LangTracer"] = "Off";
            Session["MdlAccess"] = "Off";
            if (Session["uID"] == null)
            {
                Session["uID"] = "";
                Response.Redirect("~/Default.aspx");
            }
            if (Session["language"] == null)
            {
                Session["language"] = "INA";
            }

            language();

        }

        protected void ddllanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["language"] = ddllanguage.SelectedValue;
            language();
        }

        protected void btnLogin_OnClick(object sender, EventArgs e)
        {
            if (txtBoxUsername.Text != "" && txtBoxPassword.Text != "")
            {
                userObjDomain.userName = txtBoxUsername.Text.ToLower();
                userObjDomain.userPass = crypto.Encrypt(txtBoxPassword.Text);
                DataSet UserLogin = userObjMapper.ListUserForLogin(userObjDomain);

                if (UserLogin.Tables[0].Rows.Count == 1)
                {
                    if (UserLogin.Tables[0].Rows[0]["IPCheck"].ToString() == "1")
                    {
                        if (SameIP())
                        {
                            rememberme();
                            LoginProcess(UserLogin);
                        }
                        else
                        {
                            updatePnlWarningPopup.Update();
                            //mdlWarningPopup.Show();
                        }
                    }
                    else
                    {
                        rememberme();
                        LoginProcess(UserLogin);
                    }
                }
                else if (UserLogin.Tables[0].Rows.Count > 1)
                {
                    lblErrorLogin.Text = lang.Translate("DEF00024", Session["language"].ToString());
                }
                else if (UserLogin.Tables[0].Rows.Count < 1)
                {
                    lblErrorLogin.Text = lang.Translate("DEF00020", Session["language"].ToString());
                }
            }
            else if (txtBoxUsername.Text == "" && txtBoxPassword.Text != "")
            {
                lblErrorLogin.Text = lang.Translate("DEF00021", Session["language"].ToString());
            }
            else if (txtBoxUsername.Text != "" && txtBoxPassword.Text == "")
            {
                lblErrorLogin.Text = lang.Translate("DEF00022", Session["language"].ToString());
            }
            else if (txtBoxUsername.Text == "" && txtBoxPassword.Text == "")
            {
                lblErrorLogin.Text = lang.Translate("DEF00023", Session["language"].ToString());
            }
        }

        protected void btnRegister_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("register.aspx");
        }

        protected void btnAboutUs_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("aboutus.aspx");
        }

        protected void btnSend_OnClick(object sender, EventArgs e)
        {
            try
            {
                lblWarningSend.Text = "";
                if (txtEmail.Text == "")
                {
                    lblWarningSend.Text = "Please input your indo-palm email address !";
                }
                else if (txtEmail.Text.Split('@')[1].ToString() != "indo-palm.co.id")
                {
                    lblWarningSend.Text = "Please input a valid indo-palm email system !";
                }
                else
                {
                    DataTable dt = userObjMapper.GetUserDetailByEmail(txtEmail.Text).Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        string subject = "Forgot Your Password ?";
                        string username = dt.Rows[0]["UserFullName"].ToString();
                        string notif = @"We were told that you forgot your password and we are pleased to inform you by sending this email containing your password. <br />" +
                                        "Here is your password.";
                        string content = crypto.Decrypt(dt.Rows[0]["UserPass"].ToString());
                        string closing = "";

                        string body = email.PopulateBody(subject, username, notif, content, closing, false);
                        body = body.Replace("{Email Code}", "EDEFN001");
                        email.SendHtmlFormattedEmail(null, txtEmail.Text, subject, body);

                        lblWarningSend.Text = "Email sent";
                    }
                    else
                    {
                        lblWarningSend.Text = "Please input a valid indo-palm email system !";
                    }
                }
            }
            catch (Exception ex)
            {
                lblWarningSend.Text = ex.Message;
            }
        }

        protected void btnCloseWarning_Click(object sender, EventArgs e)
        {
            //mdlWarningPopup.Hide();
            Response.Redirect("~/Default.aspx");
        }
        protected void btnGuest_OnClick(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now;
            string day = (dt.Day).ToString();
            day = day.Length == 1 ? "0" + day : day;
            string month = dt.Month.ToString();
            month = month.Length == 1 ? "0" + month : month;
            string year = dt.Year.ToString();
            Session["PZO5"] = "";
            Session["uID"] = "Guest";
            Session["uname"] = "Guest";
            Response.Redirect("~/Content/PZO_Dashboard.aspx");
        }

        #endregion

        #region Custom Method

        protected void LoginProcess(DataSet UserLogin)
        {
            string userName = "", pass = "";
            foreach (DataRow row in UserLogin.Tables[0].Rows)
            {
                userName = row[1].ToString();
                pass = row[2].ToString().ToLower();
                Session["fullname"] = row["UserFullName"].ToString();
                Session["uname"] = userName;
                if (row["UserID"].ToString() != null && row["UserID"].ToString() != "")
                {
                    Session["uID"] = row["UserID"].ToString();
                }
                else
                {
                    Session["uID"] = "";
                }

                Session["group"] = row["usrGroupCode"].ToString();
                Session["dept"] = row["DeptCode"].ToString();
            }

            userObjDomain.userID = Session["uID"].ToString();

            DataSet mdlaccess = userObjMapper.ListUserModuleAccessByUserID(userObjDomain, "INV");
            DataSet mdlPzoAcc = userObjMapper.ListUserModuleAccessByUserID(userObjDomain, "PZO");
            if (mdlPzoAcc.Tables[0].Rows.Count > 0)
            {
                Session["PZO5"] = mdlPzoAcc.Tables[0].Rows[0]["mdlAccCode"].ToString();
            }
            else
            {
                Session["PZO5"] = "";
            }
            if (mdlaccess.Tables[0].Rows.Count > 0)
                Session["INV"] = mdlaccess.Tables[0].Rows[0]["mdlAccCode"].ToString();
            else
                Session["INV"] = "";

            Session["language"] = ddllanguage.SelectedValue;

            inputhistory(Session["uname"].ToString());

            DateTime dt = DateTime.Now;
            string day = (dt.Day - 1).ToString();
            day = day.Length == 1 ? "0" + day : day;
            string month = dt.Month.ToString();
            month = month.Length == 1 ? "0" + month : month;
            string year = dt.Year.ToString();
            //Response.Redirect("~/Content/PPB_Map.aspx?typeModule=OLM&Company=PT.THIP&Estate=AGT&Date=" + month + "/" + day + "/" + year);
            Response.Redirect("~/Content/PZO_Dashboard.aspx");

        }

        private void bindLanguage(string region)
        {
            DataTable dt = new DataTable();
            DataSet dtset = new DataSet();
            int i;

            ddllanguage.Items.Clear();
            dtset = LanguageObj.ListData();
            dt = dtset.Tables[0];
            for (i = 0; i < dt.Rows.Count; i++)
            {
                ddllanguage.Items.Add(new ListItem(dt.Rows[i]["langDESC"].ToString(), dt.Rows[i]["langCODE"].ToString()));
            }

            ddllanguage.Text = region;
        }

        protected String getLanguage(String code)
        {
            return lang.Translate(code, Session["language"].ToString());
        }

        protected void rememberme()
        {
            if (chkSaveLanguage.Checked == true)
            {
                HttpCookie INV_Cookies = new HttpCookie("INV_Cookies");
                INV_Cookies.Values["username"] = Request.Form["username"];
                INV_Cookies.Values["password"] = Request.Form["password"];
                INV_Cookies.Values["language"] = ddllanguage.SelectedValue;
                INV_Cookies.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(INV_Cookies);
            }
        }

        protected void loadcache()
        {
            if (Request.Cookies["PPB_Cookies"] != null)
            {
                String username = Server.HtmlEncode(Request.Cookies["PPB_Cookies"].Values["username"]).ToString();
                String password = Server.HtmlEncode(Request.Cookies["PPB_Cookies"].Values["password"]).ToString();
                String language = Server.HtmlEncode(Request.Cookies["PPB_Cookies"].Values["language"]).ToString();

                String encryptedpassword = crypto.Encrypt(password);

                userObjDomain.userName = username;
                userObjDomain.userPass = encryptedpassword;
                DataSet UserLogin = userObjMapper.ListUserForLogin(userObjDomain);

                if (UserLogin.Tables[0].Rows.Count == 1)
                {
                    foreach (DataRow row in UserLogin.Tables[0].Rows)
                    {
                        int userID = int.Parse(row[0].ToString());
                        String userName = row["UserName"].ToString();
                        Session["uname"] = userName;
                        Session["uID"] = row["UserID"].ToString();
                        Session["group"] = row["usrGroupCode"].ToString();
                        Session["dept"] = row["DeptCode"].ToString();
                        String pass = row["UserPass"].ToString().ToLower();

                        Session["language"] = language;
                        inputhistory(Session["uname"].ToString());

                        DateTime dt = DateTime.Now;
                        string day = (dt.Day - 1).ToString();
                        day = day.Length == 1 ? "0" + day : day;
                        string month = dt.Month.ToString();
                        month = month.Length == 1 ? "0" + month : month;
                        string year = dt.Year.ToString();
                        Response.Redirect("~/Content/PPB_Map.aspx?typeModule=OLM&Company=PT.THIP&Estate=AGT&Date=" + month + "/" + day + "/" + year);
                    }
                }
            }
        }

        protected void inputhistory(String _username)
        {
            String username = _username.ToString();

            String ipadd = "";
            String hostpc = "";

            if (username != "")
            {
                ipadd = getIPAddress();
            }

            //userObjMapper.AddLoginHistory(username, DateTime.Now, hostpc, ipadd);
        }

        protected String getIPAddress()
        {
            String ipAddr = "";

            try
            {
                if (System.Web.HttpContext.Current.Request.UserHostAddress != null)
                {
                    ipAddr = System.Web.HttpContext.Current.Request.UserHostAddress;
                }
            }
            catch (System.Net.Sockets.SocketException ex2)
            {
                ex2.ToString();
                ipAddr = "";
            }

            return ipAddr;
        }

        protected Boolean SameIP()
        {
            String myIPAddress = getIPAddress();

            DataSet listDomain = userObjMapper.ListDomainGroupForSecurityPurpose();
            Boolean same = false;

            if (listDomain.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in listDomain.Tables[0].Rows)
                {
                    if (myIPAddress.Contains(row["IPAddress"].ToString()))
                    {
                        same = true;
                    }
                }
            }

            return same;
        }

        protected void language()
        {
            lblHeaderPoweredBy.Text = lang.Translate("DEF00001", Session["language"].ToString());
            lblHeaderLogin.Text = lang.Translate("DEF00002", Session["language"].ToString());
            lblHeaderUsername.Text = lang.Translate("DEF00003", Session["language"].ToString());
            txtBoxUsername.ToolTip = lang.Translate("DEF00004", Session["language"].ToString());
            lblHeaderPassword.Text = lang.Translate("DEF00005", Session["language"].ToString());
            txtBoxPassword.ToolTip = lang.Translate("DEF00006", Session["language"].ToString());
            btnForgotPassword.Text = lang.Translate("DEF00007", Session["language"].ToString());
            btnForgotPassword.ToolTip = lang.Translate("DEF00007_tooltip", Session["language"].ToString());
            chkSaveLanguage.ToolTip = lang.Translate("DEF00008", Session["language"].ToString());
            btnLogin.Text = lang.Translate("DEF00019", Session["language"].ToString());
            btnLogin.ToolTip = lang.Translate("DEF00009", Session["language"].ToString());

            //btnAboutUs.Text = lang.Translate("DEF00017", Session["language"].ToString());
            //btnRegister.Text = lang.Translate("DEF00018", Session["language"].ToString());
            RegularExpressionValidator.ErrorMessage = lang.Translate("DEF00010", Session["language"].ToString());

            lblHeaderForgot.Text = lang.Translate("DEF00011", Session["language"].ToString());
            lblForgotAttention.Text = lang.Translate("DEF00012", Session["language"].ToString());
            lblForgotQuestion.Text = lang.Translate("DEF00013", Session["language"].ToString());

            lblHeaderSaveLanguage.Text = lang.Translate("DEF00016", Session["language"].ToString());

            lblWarningAccess.Text = lang.Translate("DEF00026", Session["language"].ToString());
            btnCloseWarning.Text = lang.Translate("DEF00027", Session["language"].ToString());

            btnSend.Text = "Send";
            //btnGuest.Text = "GUEST";
            btnLogin.Text = "MASUK";
            btnSend.ToolTip = "Click here to send email";
        }

        #endregion
    }
}
