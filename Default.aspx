﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="PPB_Project.Default" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Piezo Meter Web :: Welcome</title>
    <link rel="stylesheet" href="Style/login.css?ver=1.1" type="text/css" />
    <link rel="stylesheet" href="Style/modalpopup.css" type="text/css" />
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="Plugin/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <!-- AdminLTE -->
    <link rel="stylesheet" href="Plugin/adminlte/dist/css/AdminLTE.min.css" />
    <script type="text/javascript" src="Plugin/jquery-1.11.1.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="Plugin/jquery-ui.js"></script>
   
    <script type="text/javascript" src="Script/Default.js"></script>
    <script type="text/javascript" src="Script/Language.js"></script>
    <script src="Plugin/ping.js" type="text/javascript"></script>
    <script src="Script/handler/login.js" type="text/javascript"></script>
   
    <!-- jQuery 3 -->
    <script type="text/javascript" src="Plugin/jquery-ui.js"></script>
  
     <!-- Jquery Validator -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 3.3.7 -->
    <script type="text/javascript" src="Plugin/jquery-ui.js"></script>
    <!-- iCheck -->
    <script type="text/javascript" src="Plugin/iCheck/icheck.min.js"></script>
    <!-- Bootstrap Select -->
    <script type="text/javascript" src="Plugin/adminlte/bootstrap-select.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script type="text/javascript" src="Plugin/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/JavaScript">
        //addEvent(window, 'load', initCorners);
        function initCorners() {
            var settings =
            {
                tl: { radius: 8 },
                tr: { radius: 8 },
                bl: { radius: 8 },
                br: { radius: 8 },
                antiAlias: true
            }
            curvyCorners(settings, "#container_form");
            curvyCorners(settings, "#myBox");
            curvyCorners(settings, "#myBox2");
            curvyCorners(settings, "#myBox3");
        }
    </script>
        <script>
            $.widget.bridge('uibutton', $.ui.button);
            localStorage.setItem("iothistorylogin", "0");
            $(function () {

                $('#btncek').click(function () {
                    $("#loader").show();
                    $.ajax({
                        type: 'POST',
                        url: 'Service/MapService.asmx/GetStatusServer',
                        data: JSON.stringify({
                        }),
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        async: false,
                        success: function (response) {
                            ping('http://app.gis-div.com/pcsweb/').then(function (delta) {
                                $("#jaringan").text(String(delta));
                                if (parseInt(String(delta)) <= 100) {
                                    $("#statusjaringan").text("(BAIK)");
                                    $("#statusjaringan").css("color", "green");
                                } else if (parseInt(String(delta)) > 100 && parseInt(String(delta)) <= 200) {
                                    $("#statusjaringan").text("(CUKUP)");
                                    $("#statusjaringan").css("color", "yellow");
                                }
                                else {
                                    $("#statusjaringan").text("(BURUK)");
                                    $("#statusjaringan").css("color", "red");
                                }
                            }).catch(function (err) {
                                $("#jaringan").text('Time Out');
                            });

                            let statuscpu, statuscpu2;
                            let statusmemory, statusmemory2;
                            $("#cpu").text(response.d[0]);
                            $("#memory").text(response.d[1]);

                            //$("#cpu2").text(parseInt(response.d[2]));
                            //let valmemory = (((63.2) - (parseFloat(response.d[3]) / 1024).toFixed(1)) / 63.2 * 100).toFixed(0);
                            //$("#memory2").text(valmemory);

                            if (parseInt(response.d[0]) <= 50) {
                                statuscpu = 0;
                            }
                            else if (parseInt(response.d[0]) > 50 && parseInt(response.d[0]) <= 80) {
                                statuscpu = 1;
                            }
                            else {
                                statuscpu = 2;
                            }

                            if (parseInt(response.d[2]) <= 50) {
                                statuscpu2 = 0;
                            }
                            else if (parseInt(response.d[2]) > 50 && parseInt(response.d[2]) <= 80) {
                                statuscpu2 = 1;
                            }
                            else {
                                statuscpu2 = 2;
                            }

                            if (parseInt(response.d[1]) <= 50) {
                                statusmemory = 0;
                            }
                            else if (parseInt(response.d[1]) > 50 && parseInt(response.d[1]) <= 90) {
                                statusmemory = 1;
                            }
                            else {
                                statusmemory = 2;
                            }

                            //if (valmemory <= 50) {
                            //	statusmemory2 = 0;
                            //}
                            //else if (valmemory > 50 && valmemory <= 90) {
                            //	statusmemory2 = 1;
                            //}
                            //else {
                            //	statusmemory2 = 2;
                            //                  }

                            if (statuscpu == 2 || statusmemory == 2) {
                                $("#statusserver").text("(BURUK)");
                                $("#statusserver").css("color", "red");
                            }
                            else if (statuscpu == 1 || statusmemory == 1) {
                                $("#statusserver").text("(CUKUP)");
                                $("#statusserver").css("color", "yellow");
                            }
                            else {
                                $("#statusserver").text("(BAIK)");
                                $("#statusserver").css("color", "green");
                            }

                            //if (statuscpu2 == 2 || statusmemory2 == 2) {
                            //                      $("#statusserver2").text("(BURUK)");
                            //                      $("#statusserver2").css("color", "red");
                            //                  }
                            //else if (statuscpu2 == 1 || statusmemory2 == 1) {
                            //                      $("#statusserver2").text("(CUKUP)");
                            //                      $("#statusserver2").css("color", "yellow");
                            //                  }
                            //                  else {
                            //	$("#statusserver2").text("(BAIK)");
                            //	$("#statusserver2").css("color", "green");
                            //}

                            $('#ModalStatus').modal({
                                backdrop: 'static', keyboard: false
                            });

                            $("#loader").hide();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log("function GetStatusServer : " + xhr.statusText);
                            $("#loader").hide();
                        }
                    });
                });
            })
        </script>
    <style type="text/css">
        .securityauth {
            width: 400px;
            height: 400px;
            background-color: #d0f2d0;
            line-height: 1em;
            color: black;
            text-align: left;
            padding: 10px;
            margin-left: 10px;
            margin-right: 10px;
        }

        .items {
            width: 635px; /*height: 500px;*/
            background-color: #d0f2d0;
            line-height: 1em;
            color: black;
            text-align: left;
            padding: 10px;
            margin-left: 10px;
            margin-right: 10px;
        }

        .items_mdl3 {
            width: 735px;
            background-color: #e2f9ee;
            line-height: 1em;
            color: black;
            text-align: left;
            padding: 10px;
            margin-left: 10px;
            margin-right: 10px;
        }

        .item_tx {
            width: 400px;
            background-color: #e2f9ee;
            line-height: 1em;
            color: black;
            text-align: left;
            padding: 3px;
        }

        .item_tx_mdl3 {
            width: 520px;
            background-color: #e2f9ee;
            line-height: 1em;
            color: black;
            text-align: left;
            padding: 3px;
        }

        .itemsPopup {
            width: 330px;
            background-color: #d0f2d0;
            line-height: 1em;
            color: black;
            text-align: left;
            padding: 10px;
            margin-left: 10px;
            margin-right: 10px;
        }
    </style>
    <style type="text/css">
        .Animation2 {
            display: none;
            position: absolute;
            width: 200px;
            background-color: #FFFFFF;
            z-index: 10000;
        }

        input.blur {
            color: #999;
        }

        #myBox {
            margin: 5px auto;
            width: 345px;
            padding: 5px;
            text-align: left;
            background-color: transparent;
        }

        #myBox2 {
            margin: 5px auto;
            width: 345px;
            padding: 5px;
            text-align: left;
            background-color: #e2f9ee;
        }

        #myBox3 {
            margin: 5px auto;
            width: 345px;
            padding: 5px;
            text-align: left;
            background-color: #e2f9ee;
        }

        .modalBackground {
            background-color: Gray;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }

        #vertmenu {
            display: none;
            width: 410px;
            position: absolute;
            left: 0;
            background-color: #d0f2d0;
        }

        .style1 {
            width: 100px;
        }

        .style2 {
            width: 400px;
        }

        .style3 {
            height: 34px;
        }
    </style>
    <style type="text/css">
        .testbutton {
            display: block;
            background: url(Image/Login/simple_button.png) repeat-x;
            width: 100%;
            text-align: center;
            padding: 5px 5px 5px 5px; /*border:3px solid #608925; */
            border: 1px solid #006400; /*text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.8);*/
            font-weight: bold;
            border-radius: 5px; /*color:#Black;*/
            text-decoration: none;
            opacity: .85;
            margin-bottom: 20px;
        }

            .testbutton:hover {
                background: #e2f9ee;
                cursor: pointer;
                color: #000000;
            }

            .testbutton:active {
                background-position: 0 -200px; /*padding:11px 0 9px;*/
                text-shadow: 0 1px 0 rgba(0, 0, 0, 0.8);
            }
    </style>
    <style type="text/css">
        .button {
            border-top: 1px solid #ffffff;
            background: #d0f2d0;
            background: -webkit-gradient(linear, left top, left bottom, from(#d0f2d0), to(#d0f2d0));
            background: -webkit-linear-gradient(top, #d0f2d0, #d0f2d0);
            background: -moz-linear-gradient(top, #d0f2d0, #d0f2d0);
            background: -ms-linear-gradient(top, #d0f2d0, #d0f2d0);
            background: -o-linear-gradient(top, #d0f2d0, #d0f2d0);
            padding: 5px 10px;
            border-radius: 11px;
            -webkit-box-shadow: rgba(0,0,0,1) 0 1px 0;
            -moz-box-shadow: rgba(0,0,0,1) 0 1px 0;
            box-shadow: rgba(0,0,0,1) 0 1px 0;
            text-shadow: rgba(0,0,0,.4) 0 1px 0;
            color: #000000;
            font-size: 11px;
            font-family: Helvetica, serif;
            text-decoration: none;
            vertical-align: middle;
        }

            .button:hover {
                background: #e2f9ee;
                cursor: pointer;
                color: #000000;
            }

            .button:active {
                background: #e2f9ee;
            }

        #imgLogoClient {
            width: 250px;
        }

        .down {
            position: fixed;
            top: 33%;
            left: 49%;
            width: 30em;
            height: auto;
            margin-top: -9em;
            margin-left: -15em;
            padding-bottom: 20px;
            border-radius: 3px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
    </style>
    <style>
        a {
            color: #00a65a;
            text-decoration: none;
        }

        .boxshadow {
            width: 400px;
            height: auto;
            padding-bottom: 20px;
            border-radius: 3px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        .textshadow li {
            list-style: none;
            text-shadow: 2px 2px 0px #FFFFFF, 1px 3px 0px rgba(0,0,0,0.15);
        }

            .textshadow li:hover {
                font-size: 12pt;
                font-weight: bolder;
            }

            .textshadow li a {
                color: #00a65a;
                text-decoration: none;
            }

        input#password {
            text-security: disc;
            -webkit-text-security: disc;
            -mox-text-security: disc;
        }

        .popover {
            max-width: 100%;
        }

        .tile {
            border: 1px solid rgba(0, 0, 0, 0.2);
            border-radius: 5px;
            text-align: left;
            padding: 10px;
        }

            .tile:hover {
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                cursor: pointer;
            }

        .conceal {
            font-family: 'text-security-disc';
        }

        .loader {
            position: fixed;
            z-index: 999999;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: black;
            opacity: 0.5;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        /* body {
            background-image: url(Image/oil_palm.jpg);
            z-index: 1;
            position: relative;
        } */

        .dropbtn {
            background-color: rgba(0,0,0,0);
            color: black;
            padding: 12px;
            font-size: 12px;
            border: none;
            cursor: pointer;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
            font-size: 12px;
            top: 25px;
        }

            .dropdown-content a {
                color: black;
                padding: 12px 16px;
                text-decoration: none;
                display: block;
            }

                .dropdown-content a:hover {
                    background-color: #ddd;
                }

        /*.dropdown:hover .dropdown-content {display: block;}*/

        .dropdown:hover .dropbtn {
            background-color: rgba(0,0,0,0);
        }

        .atas {
            margin-bottom: 10px;
        }

        .gone {
            display: none;
        }

        .rapih {
            margin-left: 15px;
        }
    </style>
    <script type="text/javascript">
        window.history.forward();
        function noBack() { window.history.forward(); }
    </script>
    <script type="text/javascript">

        function btnCloseModal() {
            $find("modal1").hide();
        }

        function btnCloseReferenceModal() {
            $find("modal2").hide();
            $find("modal1").show();
        }

<%--        function ShowPopupForgotPassword() {
            var txtEmail = document.getElementById("<%= txtEmail.ClientID %>");
            txtEmail.value = "";
            $find("modalForgot").show();
            return false;
        }--%>

        function ClosePopupForgotPassword() {
            $find("modalForgot").hide();
            return false;
        }

    </script>
</head>
<body>
    <form id="Form1" runat="server" autocomplete="off" defaultbutton="btnLogin">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <!--new-->
        <%--   <div class="col-md-6 boxshadow" style="position: fixed; top: 33%; left: 49%; width: 30em; height: auto; margin-top: -9em; margin-left: -15em;">
            <div style="text-align: center;">
                <br>
                <img src="Image/login/logologin4.png" width="250">
                <br>
                <br>
                <b>Water Level And Rain Fall</b>
                <br>
                <br>

                <p><span id="Register">Belum punya akun Enterprise GIS?</span>&nbsp;<a id="Register2" href="https://app.gis-div.com/EntGIS/register2.aspx">Daftar</a></p>
                <br>
            </div>
            <form method="post" action="./" id="formLogin" role="form" autocomplete="off" novalidate="novalidate">
                <div class="aspNetHidden">
                    <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="hiCq75U3OrkDFD6f8fkBBH1BiivKB9JPMFSRH1o1MLYje/lhlhWzwMVUu8Dc9DJfsgSoTHLH1M7AVs+WbGvwvAASJHc9U7Ot25YNr/sSofqPwAlWK8BBIeAP6uAiYmv3thcD1QeZ6ABoQihMyfjccFIN8G78eI9vweJ6uhvK+Ug=">
                </div>

                <div class="aspNetHidden">

                    <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="E169DFF1">
                    <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="su/jBzGJwBwbt2mEc2lXPpDZGjQ8QD3WYuqcDgiQS9zfL/mNxmiIzk4BZL5uGUKnVJO0x6UikWh4ZczoBSf4fQ9f/zW15t0aRHAjNtvSPoZFXOef6gCeVSdCUus2dpzldfd2+rqMOGIC0dfc9ZSX3/dItMBCF8G4ti6u2o5pwj1IUUsrBpmnKYozBrZnrXyj">
                </div>
                <div class="box-body" style="padding: 15px;">
                    <div class="form-group">
                        <label for="username" id="lblusername">Nama Pengguna</label>
                        <asp:TextBox ID="txtBoxUsernamex" runat="server" class="form-control" TabIndex="1" EnableTheming="False" EnableViewState="False" aria-invalid="false"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="password" id="lblpassword">Kata Sandi</label>
                        <asp:TextBox ID="TextBox1" runat="server" TextMode="Password" class="form-control" TabIndex="2" EnableTheming="False" EnableViewState="False"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="text-align: left;">
                            <span style="display: none">
                                <span class="flag-green">
                                    <input id="chkRememberMe" type="checkbox" name="chkRememberMe"></span>
                                Ingat Saya
                            </span>
                        </div>
                        <div class="col-md-6" style="text-align: right; color: #00a65a; cursor: pointer; margin-left: 54%" onclick="_setCustomFunctions.Test()"><span id="forgetpassword">Lupa Kata Sandi?</span></div>
                    </div>
                    <div class="form-group">
                        <table style="width: 100%;" border="0">
                            <tbody>
                                <tr>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="box-footer" style="padding: 15px;">
                    <input type="submit" name="btnLogin" value="Masuk" id="btnLogin" class="btn btn-success col-md-12" fdprocessedid="0nt04q">
                    <br>
<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddllanguage_SelectedIndexChanged"></asp:DropDownList>n dan Server</button>
                </div>
                <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddllanguage_SelectedIndexChanged"></asp:DropDownList>
                <div class="dropdown">
                    <span class="dropbtn" onclick="ButtonLanguageClick();">
                        <img id="imglanguage" style="width: 20px;" src="Image/flag/in.png">
                        &nbsp;&nbsp; <span id="spanlanguage">Indonesia</span></span>
                    <div class="dropdown-content">
                        <a href="#" onclick="LanguageClick('INA');">
                            <img src="Image/flag/in.png" style="width: 20px;">
                            &nbsp;&nbsp;Indonesia</a>
                        <a href="#" onclick="LanguageClick('ENG');">
                            <img src="Image/flag/en.png" style="width: 20px;">
                            &nbsp;&nbsp;English</a>
                    </div>
                </div>
            </form>
        </div>--%>
        <!--end new-->

        <div class="col-md-12" style="padding-top: 50px;">
            <div class="col-md-6 down">







                <div>
                    <br />
                    <table>
                        <tr>
                            <td>
                               
                            </td>
                            <td>
                                <table width="100%" style="height: 100%;display: none;">
                                    <tr>
                                        <td align="right" style="width: auto; vertical-align: middle">
                                            <span style="font-family: Calibri; font-size: 9pt; margin-right: 16px">
                                                <asp:Label ID="lblHeaderPoweredBy" runat="server"></asp:Label>
                                            </span>
                                            <br />
                                            <span style="margin-right: 12px;">
                                                <img alt="Precise Agri" src="Image/Login/login-logo-v2.jpg" style="height: 30px; width: 110px" />
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                 <div style="text-align: center;">
                
                <img src="Image/login/logologin4.png" width="250" />
                <br />
                <br />
                <b>Piezo Meter</b>
                <br />
                <br />
                <%--<h3><b>KPN Enterprise GIS</b></h3>--%>
                <p><span id="Register">Belum punya akun Enterprise GIS ? </span>&nbsp<a id="Register2" href="https://app.gis-div.com/EntGIS/register2.aspx">Daftar</a></p>
                <br />
            </div>
                    <div id="container_form" class="rounded3" <%--onkeydown="javascript:EnterToClick();"--%>>

                        <div class="box-body" style="padding: 15px;">
                            <div class="form-group">

                                <label>
                                    <asp:Label ID="lblHeaderUsername" runat="server" EnableTheming="False" EnableViewState="False"></asp:Label></label>
                                <asp:TextBox ID="txtBoxUsername" CssClass="form-control" runat="server" TabIndex="1" EnableTheming="False"
                                    EnableViewState="False"></asp:TextBox>
                                <asp:Label ID="myLabel2" runat="server" Text="User tidak terdaftar pada EUCLID" Style="color: crimson" Visible="false" />
                            </div>
                            <div class="form-group">
                                <label>
                                    <asp:Label ID="lblHeaderPassword" runat="server" EnableTheming="False" EnableViewState="False"></asp:Label></label>
                                <asp:TextBox ID="txtBoxPassword" CssClass="form-control" runat="server" TextMode="Password" TabIndex="2"
                                    EnableTheming="False" EnableViewState="False">
                                </asp:TextBox>
                                <asp:Label ID="myLabel" runat="server" Text="Username Atau Password Salah." Style="color: crimson" Visible="false" />
                            </div>
                            <div class="form-group">
                                <div class="col-md-6" style="text-align: left;">
                                    <span style="display: none">
                                        <asp:CheckBox runat="server" ID="chkRememberMe" CssClass="flag-green" />
                                        Ingat Saya
                            </span>
                                </div>


                                <div class="col-md-6" runat="server" style="text-align: right; color: #00a65a; cursor: pointer; margin-left:54%" onclick="_setCustomFunctions.Test()"><span id="forgetpassword">Lupa Kata Sandi ?</span></div>
                                   
                                
                                    <span id="forgetpassword_lama" style="display: none;">
                                        <asp:UpdatePanel ID="updatepanel1" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                &nbsp;&nbsp;
                                                <asp:Button ID="btnForgotPassword" runat="server" OnClientClick="return ShowPopupForgotPassword();"
                                                    BackColor="#d0f2d0" BorderWidth="0px" Font-Names="Calibri" EnableTheming="False"
                                                    EnableViewState="False" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <table style="width: 100%;" border="0">
                                    <tr>
                                        <td>
                                            <%--<div runat="server" style="text-align: right; color: #00a65a; cursor: pointer;" data-toggle="modal" data-target="#modallupapassword">Lupa Kata Sandi ?</div>--%>
                                </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="box-footer" style="padding: 15px;">



                            <asp:Button ID="btnLogin" class="atas btn btn-success col-md-12" Font-Names="Calibri" Font-Size="10pt"
                                OnClick="btnLogin_OnClick" runat="server" />
                            <%--<asp:Button ID="btnGuest" class="btn btn-primary  col-md-12" Font-Names="Calibri" Font-Size="10pt"
                                runat="server" OnClick="btnGuest_OnClick" />--%>
                            <button type="button" class="btn btn-primary  col-md-12" id="btncek">Cek Jaringan dan Server</button>

                        </div>
                        <table cellpadding="0" cellspacing="0" style="display: none;">
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddllanguage" CssClass="rapih" runat="server" AutoPostBack="True"
                                        Font-Size="12pt" Height="27px" Width="100%" OnSelectedIndexChanged="ddllanguage_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td style="padding-left: 20px">
                                    <asp:CheckBox ID="chkSaveLanguage" runat="server" EnableTheming="False" EnableViewState="False" />
                                </td>
                                <td>
                                    <span style="font-size: 8pt;margin-left: 4px;">
                                        <asp:Label ID="lblHeaderSaveLanguage" runat="server" EnableViewState="False"></asp:Label>
                                    </span>
                                </td>
                            </tr>
                        </table>

                    <div class="dropdown">
                        <span class="dropbtn" onclick="ButtonLanguageClick();">
                            <img id="imglanguage" style="width: 20px;" />
                            &nbsp;&nbsp; <span id="spanlanguage"></span></span>
                        <div class="dropdown-content">
                            <a href="#" onclick="LanguageClick('INA');">
                                <img src="Image/flag/in.png" style="width: 20px;" />
                                &nbsp;&nbsp;Indonesia</a>
                            <a href="#" onclick="LanguageClick('ENG');">
                                <img src="Image/flag/en.png" style="width: 20px;" />
                                &nbsp;&nbsp;English</a>
                        </div>
                    </div>

                        <table class="gone" cellpadding="0" cellspacing="0" style="width: 500px; height: 107px" border="1">
                            <tr>
                                <td style="height: 21px"></td>
                            </tr>
                            <tr>
                                <td style="height: 21px;" colspan="2">
                                    <span style="font-family: Calibri; font-size: 12pt; font-weight: bold; margin-left: 10px">
                                        <asp:Label ID="lblHeaderLogin" runat="server" EnableTheming="False" EnableViewState="False">
                                        </asp:Label></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 15px"></td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-family: Calibri; font-size: 10pt; margin-left: 10px"></span>
                                </td>
                                <td style="height: 43px; text-align: center;">
                                    <div id="myBox">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px">
                                    <span style="font-family: Calibri; font-size: 10pt; margin-left: 10px"></span>
                                </td>
                                <td style="height: 43px; width: 400px; text-align: center">
                                    <div id="myBox2">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100px"></td>
                                <td style="height: 43px; width: 400px; text-align: left"></td>
                            </tr>
                            <tr style="height: 10px; vertical-align: top">
                                <td class="style1"></td>
                                <td style="text-align: left; color: red;" class="style2">
                                    <asp:Label ID="lblErrorLogin" runat="server" EnableTheming="False" Font-Size="9pt"
                                        Text="" EnableViewState="False"></asp:Label>&nbsp;
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator" runat="server" ControlToValidate="txtBoxUsername"
                            Font-Bold="true" Font-Size="9pt" ForeColor="Red" ValidationExpression="[a-zA-Z0-9._-]+"
                            EnableTheming="False" EnableViewState="False"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>


                    </div>

                    <table>
                        <tr>
                            <td style="width: 14px; height: 0px"></td>
                            <td style="padding-left: 15px"></td>
                            <%--<td>
                    <asp:Button ID="btnRegister" class="testbutton" Font-Names="Calibri" Font-Size="10pt"
                        runat="server" OnClick="btnRegister_OnClick" />
                </td>--%>
                            <td style="width: 400px; padding-left: 370px"></td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
     
        <asp:Panel ID="panelMdlForgot" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="updatePnlMdlForgot" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="modalBody700px">
                        <div class="modalHeader">
                            <asp:Label ID="lblHeaderForgot" runat="server"></asp:Label>
                        </div>
                        <span style="margin-left: 10px; font-weight: bold">
                            <div style="position: absolute; right: 10px; top: 10px">
                                <asp:Button ID="btnCloseMdlForgot" runat="server" Text="X" CssClass="modalButtonX"
                                    OnClientClick="return ClosePopupForgotPassword();" />
                            </div>
                        </span>
                        <br />
                        <br />
                        <div class="modalInnerBody700px">
                            <table style="width: 580px; height: 1px">
                                <tr>
                                    <td style="width: 200px">&nbsp;
                                    </td>
                                    <td style="width: 380px; height: 40px; vertical-align: middle">
                                        <div id="Div9" class="modalItemBackground">
                                            <asp:Label ID="lblForgotAttention" runat="server" Font-Names="calibri" Font-Size="10pt"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px">
                                        <asp:Label ID="lblForgotQuestion" runat="server" Font-Names="calibri" Font-Size="10pt"></asp:Label>
                                    </td>
                                    <td style="width: 380px; height: 40px; vertical-align: middle">
                                        <div id="Div11" class="modalItemBackground">
                                            <asp:TextBox runat="server" ID="txtEmail" Width="356px" Font-Names="calibri" Font-Size="10pt"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 200px">&nbsp;
                                    </td>
                                    <td style="width: 380px; height: 40px; vertical-align: middle">
                                        <div id="Div12" class="modalItemBackground">
                                            <asp:Button ID="btnSend" runat="server" CssClass="modalButton" OnClick="btnSend_OnClick" />
                                            <asp:Label ID="lblWarningSend" runat="server" CssClass="modalLabelWarning"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <br />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
      
        <asp:Panel ID="pnlWarningPopup" runat="server" Style="display: none;">
            <asp:UpdatePanel ID="updatePnlWarningPopup" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="background-color: #d0f2d0; padding: 10px; width: 310px">
                        <div style="border-width: 1px; border-color: Black; border-style: solid">
                            <div style="background-color: white; width: 308px">
                                <table>
                                    <tr>
                                        <td>
                                            <span style="font-family: Calibri; font-size: 10pt">
                                                <img src="images/admin/warning2.jpg" alt="" height="75px" />
                                            </span>
                                        </td>
                                        <td>
                                            <span style="font-family: Calibri; font-size: 10pt; text-align: justify">
                                                <asp:Label ID="lblWarningAccess" runat="server" Text=""></asp:Label>
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="background-color: #E4E3E3; width: 308px; height: 25px">
                                <span style="margin-left: 140px">
                                    <asp:Button ID="btnCloseWarning" runat="server" Text="" Font-Names="calibri" Font-Size="9pt"
                                        BorderStyle="groove" BackColor="#d0f2d0" OnClick="btnCloseWarning_Click" />
                                </span>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
      
 </form>
    
    <div class="modal fade" id="ModalStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="position: absolute;">
        <div class="modal-dialog modal-dialog-centered" role="document" style="width: 550px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleStatus" style="font-weight: bold">STATUS JARINGAN DAN SERVER</h5>
                </div>
                <div class="modal-body">
                    <table border="1" class="table table-bordered">
                        <tr style="background-color: #7fc0e1; font-weight: bold">
                            <td colspan="2">Status Jaringan <span id="statusjaringan"></span></td>
                        </tr>
                        <tr>
                            <td>Ping (ms)</td>
                            <td align="right"><span id="jaringan"></span></td>
                        </tr>
                        <tr style="background-color: #7fc0e1; font-weight: bold">
                            <td colspan="2">Status Server (Database) <span id="statusserver" /></td>
                        </tr>
                        <tr>
                            <td>Pemakain CPU (%)</td>
                            <td align="right"><span id="cpu"></span></td>
                        </tr>
                        <tr>
                            <td>Pemakain Memory (%)</td>
                            <td align="right"><span id="memory"></span></td>
                        </tr>
                        <%--<tr style="background-color:#7fc0e1; font-weight:bold;">
                    <td colspan="2">Status Server (Aplikasi) <span id="statusserver2" /></td>                        
                </tr>
                <tr>
                    <td>Pemakain CPU (%)</td>
                    <td align="right"><span id="cpu2"></span></td>
                </tr> 
                <tr>
                    <td>Pemakain Memory (%)</td>
                    <td align="right"><span id="memory2"></span></td>
                </tr>   --%>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="btncancelTambahTarget" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" data-backdrop="static" id="mdlAdd">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Reset Password</h4>
                    </div>
                    <div class="modal-body">
                        <form id="formAdd" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Email Anda</label>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" name="email" />
                                </div>
                                <img id="checked" class="img-circle" alt="User Image" style="position: fixed; width: 1.4em; height: 1.4em; left: 90%; top: 37%; visibility: hidden;" />
                            </div>
                            <div class="form-group">
                                <div class="col-xs-8 col-xs-offset-3">
                                    <button id="btnUpdate" type="button" class="btn btn-primary">Kirim</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
</body>
</html>

