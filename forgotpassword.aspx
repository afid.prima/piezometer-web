﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="forgotpassword.aspx.cs" Inherits="WLR_WaterLevelAndRainFall.forgotpassword" %>

<!DOCTYPE html>

<html>
<head>
    <title>PZO | Change Password</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="Plugin/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
   
    <!-- AdminLTE -->
    <link href="Plugin/adminlte/dist/css/AdminLTE.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link href="Plugin/adminlte/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->
    <link href="Plugin/jquery-confirm/jquery-confirm.min.css" rel="stylesheet" />
    <link href="Plugin/adminlte/bower_components/Ionicons/css/ionicons.min.css" rel="stylesheet" />
    <!-- Bootstrap Select -->
    <link href="Plugin/adminlte/bower_components/bootstrap-select/bootstrap-select.css" rel="stylesheet" />
    <!-- Theme style -->

    <link rel="stylesheet" href="Plugin/adminlte/dist/css/skins/_all-skins.min.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="Plugin/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="Plugin/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <link href="Plugin/toast/jquery.toast.min.css" rel="stylesheet" />

    <%--<link href="../../Plugin/adminlte/bower_components/bootstrap-treeview/bootstrap-treeview.css" rel="stylesheet" />--%>
    <!-- Morris chart -->
    <link rel="stylesheet" href="Plugin/adminlte/bower_components/morris.js/morris.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="Plugin/iCheck/all.css">
    
    <link href="Plugin/jquery-ui/jquery-ui.css" rel="stylesheet" />
    <link href="Plugin/jstree/style.min.css" rel="stylesheet" />

    <!-- Google Font -->
  
    <%--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">--%>
    <style>
        a {
            color: #00a65a; 
            text-decoration: none;
        }
        .boxshadow-small {
            height: 50px;
            border-radius: 3px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .boxshadow {
            width: 400px;
            height: auto;
            padding-bottom:20px;
            border-radius: 3px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .textshadow li {
            list-style:none;
            text-shadow: 2px 2px 0px #FFFFFF, 1px 3px 0px rgba(0,0,0,0.15);
        }
        .textshadow li:hover {
           font-size:12pt;
           font-weight:bolder;
        }
        .textshadow li a {
            color: #00a65a; 
            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class="col-md-12" style="padding-top: 50px">
        <div class="col-md-12 boxshadow" style="
    position: fixed;
    top: 40%;
    left: 49%;
    width: 32em;
    height: auto;
    margin-top: -9em;
    margin-left: -15em;
    ">
            <div style="text-align: center;">
                <br />
                <img src="image/Login/logologin4.png" width="100"/>
                <br />
                <label class="col-xs-12 control-label"><small style="font-family: Times New Roman, Times, serif;font-style: italic;font-size: initial;">Bersama Kita <label style="color: #00a65a;"> Bisa </label></small> </label>
            </div>
                <div class="box-body" style="padding: 15px;">
                    <form role="form" id="formVerification" runat="server">

                    <div class="box-footer" style="padding: 15px;text-align:center;">
                        <label id="lblNotification"></label>
                        <br />
                        <div class="form-group col-md-12" id="Password">
                            <label class="col-md-7 control-label" style="text-align: initial;">Password Baru</label>
                            <div class="col-md-5">
                                <%--<input type="text" class="form-control" name="code" />--%>
                                <asp:TextBox runat="server" CssClass="form-control" ID="password" type="password" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group col-md-12" id="UlangiPassword">
                            <label class="col-md-7 control-label" style="text-align: initial">Ulangi Password Baru</label>
                            <div class="col-md-5">
                                <%--<input type="text" class="form-control" name="code" />--%>
                                <asp:TextBox runat="server" CssClass="form-control" ID="ulangipassword" type="password"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div>
                            <button class="btn btn-primary col-md-12" id="BtnChangePassword" >Perbaharui</button>
                        </div>
                        
                        <%--<div class="form-group col-md-12" id="lblDefaultPassword"><label><small>Default Password : <label style="color: #E53935;">123456</label></small></label>
                            </div>--%>
                </div>
                    </form>

                </div>
                <!-- /.box-body -->

        </div>
    </div>

    <!-- jQuery 3 -->
    <script src="Plugin/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="Plugin/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="Plugin/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
    <script src="Plugin/iCheck/icheck.min.js?ver=1.1"></script>
    <script src="Plugin/jquery-confirm/jquery-confirm.min.js?ver=1.1"></script>
    <!-- Main Jquery -->
    <script src="Script/handler/forgotpassword.js"></script>
</body>
</html>